var t = require("../../../../@babel/runtime/helpers/typeof");

(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uni_modules/uni-transition/components/uni-transition/uni-transition" ], {
    "28c8": function(t, n, i) {
        i.r(n);
        var e = i("e7e3"), o = i.n(e);
        for (var r in e) "default" !== r && function(t) {
            i.d(n, t, function() {
                return e[t];
            });
        }(r);
        n.default = o.a;
    },
    "78a3": function(t, n, i) {
        i.r(n);
        var e = i("bd35"), o = i("28c8");
        for (var r in o) "default" !== r && function(t) {
            i.d(n, t, function() {
                return o[t];
            });
        }(r);
        var a = i("f0c5"), s = Object(a.a)(o.default, e.b, e.c, !1, null, null, null, !1, e.a, void 0);
        n.default = s.exports;
    },
    bd35: function(t, n, i) {
        i.d(n, "b", function() {
            return e;
        }), i.d(n, "c", function() {
            return o;
        }), i.d(n, "a", function() {});
        var e = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    e7e3: function(n, i, e) {
        Object.defineProperty(i, "__esModule", {
            value: !0
        }), i.default = void 0;
        var o = e("5b03");
        function r(t) {
            return function(t) {
                if (Array.isArray(t)) return a(t);
            }(t) || function(t) {
                if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
            }(t) || function(t, n) {
                if (t) {
                    if ("string" == typeof t) return a(t, n);
                    var i = Object.prototype.toString.call(t).slice(8, -1);
                    return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(t) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? a(t, n) : void 0;
                }
            }(t) || function() {
                throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            }();
        }
        function a(t, n) {
            (null == n || n > t.length) && (n = t.length);
            for (var i = 0, e = new Array(n); i < n; i++) e[i] = t[i];
            return e;
        }
        function s(t, n) {
            var i = Object.keys(t);
            if (Object.getOwnPropertySymbols) {
                var e = Object.getOwnPropertySymbols(t);
                n && (e = e.filter(function(n) {
                    return Object.getOwnPropertyDescriptor(t, n).enumerable;
                })), i.push.apply(i, e);
            }
            return i;
        }
        function c(t) {
            for (var n = 1; n < arguments.length; n++) {
                var i = null != arguments[n] ? arguments[n] : {};
                n % 2 ? s(Object(i), !0).forEach(function(n) {
                    u(t, n, i[n]);
                }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : s(Object(i)).forEach(function(n) {
                    Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(i, n));
                });
            }
            return t;
        }
        function u(t, n, i) {
            return n in t ? Object.defineProperty(t, n, {
                value: i,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : t[n] = i, t;
        }
        var l = {
            name: "uniTransition",
            emits: [ "click", "change" ],
            props: {
                show: {
                    type: Boolean,
                    default: !1
                },
                modeClass: {
                    type: [ Array, String ],
                    default: function() {
                        return "fade";
                    }
                },
                duration: {
                    type: Number,
                    default: 300
                },
                styles: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                customClass: {
                    type: String,
                    default: ""
                }
            },
            data: function() {
                return {
                    isShow: !1,
                    transform: "",
                    opacity: 1,
                    animationData: {},
                    durationTime: 300,
                    config: {}
                };
            },
            watch: {
                show: {
                    handler: function(t) {
                        t ? this.open() : this.isShow && this.close();
                    },
                    immediate: !0
                }
            },
            computed: {
                stylesObject: function() {
                    var t = c(c({}, this.styles), {}, {
                        "transition-duration": this.duration / 1e3 + "s"
                    }), n = "";
                    for (var i in t) {
                        n += this.toLine(i) + ":" + t[i] + ";";
                    }
                    return n;
                },
                transformStyles: function() {
                    return "transform:" + this.transform + ";opacity:" + this.opacity + ";" + this.stylesObject;
                }
            },
            created: function() {
                this.config = {
                    duration: this.duration,
                    timingFunction: "ease",
                    transformOrigin: "50% 50%",
                    delay: 0
                }, this.durationTime = this.duration;
            },
            methods: {
                init: function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    t.duration && (this.durationTime = t.duration), this.animation = (0, o.createAnimation)(Object.assign(this.config, t));
                },
                onClick: function() {
                    this.$emit("click", {
                        detail: this.isShow
                    });
                },
                step: function(n) {
                    var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    if (this.animation) {
                        for (var e in n) try {
                            var o;
                            "object" === t(n[e]) ? (o = this.animation)[e].apply(o, r(n[e])) : this.animation[e](n[e]);
                        } catch (t) {
                            console.error("方法 ".concat(e, " 不存在"));
                        }
                        return this.animation.step(i), this;
                    }
                },
                run: function(t) {
                    this.animation && this.animation.run(t);
                },
                open: function() {
                    var t = this;
                    clearTimeout(this.timer), this.transform = "", this.isShow = !0;
                    var n = this.styleInit(!1), i = n.opacity, e = n.transform;
                    void 0 !== i && (this.opacity = i), this.transform = e, this.$nextTick(function() {
                        t.timer = setTimeout(function() {
                            t.animation = (0, o.createAnimation)(t.config, t), t.tranfromInit(!1).step(), t.animation.run(), 
                            t.$emit("change", {
                                detail: t.isShow
                            });
                        }, 20);
                    });
                },
                close: function(t) {
                    var n = this;
                    this.animation && this.tranfromInit(!0).step().run(function() {
                        n.isShow = !1, n.animationData = null, n.animation = null;
                        var t = n.styleInit(!1), i = t.opacity, e = t.transform;
                        n.opacity = i || 1, n.transform = e, n.$emit("change", {
                            detail: n.isShow
                        });
                    });
                },
                styleInit: function(t) {
                    var n = this, i = {
                        transform: ""
                    }, e = function(t, e) {
                        "fade" === e ? i.opacity = n.animationType(t)[e] : i.transform += n.animationType(t)[e] + " ";
                    };
                    return "string" == typeof this.modeClass ? e(t, this.modeClass) : this.modeClass.forEach(function(n) {
                        e(t, n);
                    }), i;
                },
                tranfromInit: function(t) {
                    var n = this, i = function(t, i) {
                        var e = null;
                        "fade" === i ? e = t ? 0 : 1 : (e = t ? "-100%" : 0, "zoom-in" === i && (e = t ? .8 : 1), 
                        "zoom-out" === i && (e = t ? 1.2 : 1), "slide-right" === i && (e = t ? "100%" : 0), 
                        "slide-bottom" === i && (e = t ? "100%" : 0)), n.animation[n.animationMode()[i]](e);
                    };
                    return "string" == typeof this.modeClass ? i(t, this.modeClass) : this.modeClass.forEach(function(n) {
                        i(t, n);
                    }), this.animation;
                },
                animationType: function(t) {
                    return {
                        fade: t ? 1 : 0,
                        "slide-top": "translateY(".concat(t ? 0 : "-100%", ")"),
                        "slide-right": "translateX(".concat(t ? 0 : "100%", ")"),
                        "slide-bottom": "translateY(".concat(t ? 0 : "100%", ")"),
                        "slide-left": "translateX(".concat(t ? 0 : "-100%", ")"),
                        "zoom-in": "scaleX(".concat(t ? 1 : .8, ") scaleY(").concat(t ? 1 : .8, ")"),
                        "zoom-out": "scaleX(".concat(t ? 1 : 1.2, ") scaleY(").concat(t ? 1 : 1.2, ")")
                    };
                },
                animationMode: function() {
                    return {
                        fade: "opacity",
                        "slide-top": "translateY",
                        "slide-right": "translateX",
                        "slide-bottom": "translateY",
                        "slide-left": "translateX",
                        "zoom-in": "scale",
                        "zoom-out": "scale"
                    };
                },
                toLine: function(t) {
                    return t.replace(/([A-Z])/g, "-$1").toLowerCase();
                }
            }
        };
        i.default = l;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uni_modules/uni-transition/components/uni-transition/uni-transition-create-component", {
    "uni_modules/uni-transition/components/uni-transition/uni-transition-create-component": function(t, n, i) {
        i("543d").createComponent(i("78a3"));
    }
}, [ [ "uni_modules/uni-transition/components/uni-transition/uni-transition-create-component" ] ] ]);