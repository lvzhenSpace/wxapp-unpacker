(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/index" ], {
    "04a8": function(n, t, o) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0, o("26cb");
            var e = {
                components: {},
                data: function() {
                    return {
                        scrollTop: 0,
                        popupSetting: {
                            number: ""
                        },
                        isShowPopupKey: !0,
                        isShowCouponPopupKey: !1,
                        danmuSetting: {},
                        danmuList: [],
                        refreshCounter: 0,
                        getNextPageCounter: 0
                    };
                },
                computed: {
                    page: function() {
                        return this.$store.getters.setting.box_home;
                    },
                    isShowPopup: function() {
                        var t = n.getStorageSync("popup_" + this.popupSetting.number);
                        return this.popupSetting.is_enabled && this.isShowPopupKey && !t;
                    },
                    isShowCouponPopup: function() {
                        var t = n.getStorageSync("coupon_popup" + this.$store.getters.setting.coupon_popup.number);
                        return this.$store.getters.setting.coupon_popup.is_enabled && this.isShowCouponPopupKey && !t;
                    }
                },
                watch: {},
                onLoad: function(n) {
                    var t = this;
                    this.$http("/coupon/popup-list").then(function(n) {
                        0 !== n.data.list.length && (t.isShowCouponPopupKey = !0);
                    });
                    var o = n.gdt_vid, e = n.weixinadinfo;
                    if (o) {
                        var u = o.split(".")[0];
                        this.$store.commit("SET_CLICK_ID", u);
                    }
                    e && this.$store.commit("SET_WX_INFO", e), this.$visitor.record("box_index"), this.getPopup(), 
                    this.getDanmu();
                },
                onPullDownRefresh: function() {
                    this.getPopup(), this.getDanmu();
                },
                onReachBottom: function() {
                    this.getNextPageCounter++;
                },
                onShow: function() {},
                methods: {
                    getDanmu: function() {
                        var n = this;
                        this.$http("/danmus/home").then(function(t) {
                            n.danmuSetting = t.data.setting, n.danmuList = t.data.list;
                        });
                    },
                    getPopup: function() {
                        var n = this;
                        this.$http("/setting/popup", "GET").then(function(t) {
                            n.popupSetting = t.data.setting;
                        });
                    },
                    closePopup: function() {
                        n.setStorageSync("popup_" + this.popupSetting.number, 1), this.isShowPopupKey = !1;
                    },
                    closeCouponPopup: function() {
                        n.setStorageSync("coupon_popup" + this.$store.getters.setting.coupon_popup.number, 1), 
                        this.isShowCouponPopupKey = !1;
                    }
                },
                onPageScroll: function(n) {
                    this.scrollTop = n.scrollTop;
                }
            };
            t.default = e;
        }).call(this, o("543d").default);
    },
    "3bb7": function(n, t, o) {},
    "92e7": function(n, t, o) {
        (function(n) {
            o("f868"), e(o("66fd"));
            var t = e(o("a233"));
            function e(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = o, n(t.default);
        }).call(this, o("543d").createPage);
    },
    9628: function(n, t, o) {
        o.r(t);
        var e = o("04a8"), u = o.n(e);
        for (var i in e) "default" !== i && function(n) {
            o.d(t, n, function() {
                return e[n];
            });
        }(i);
        t.default = u.a;
    },
    a233: function(n, t, o) {
        o.r(t);
        var e = o("f4cf"), u = o("9628");
        for (var i in u) "default" !== i && function(n) {
            o.d(t, n, function() {
                return u[n];
            });
        }(i);
        o("e90c");
        var p = o("f0c5"), r = Object(p.a)(u.default, e.b, e.c, !1, null, "6f1fa7ea", null, !1, e.a, void 0);
        t.default = r.exports;
    },
    e90c: function(n, t, o) {
        var e = o("3bb7");
        o.n(e).a;
    },
    f4cf: function(n, t, o) {
        o.d(t, "b", function() {
            return u;
        }), o.d(t, "c", function() {
            return i;
        }), o.d(t, "a", function() {
            return e;
        });
        var e = {
            HomeNavbar: function() {
                return o.e("components/HomeNavbar/HomeNavbar").then(o.bind(null, "fa00"));
            },
            PageRender: function() {
                return Promise.all([ o.e("common/vendor"), o.e("components/PageRender/PageRender") ]).then(o.bind(null, "e85b"));
            },
            Popup: function() {
                return o.e("components/Popup/Popup").then(o.bind(null, "01fc"));
            },
            Danmus: function() {
                return o.e("components/Danmus/Danmus").then(o.bind(null, "7d24"));
            },
            CouponPopup: function() {
                return o.e("components/CouponPopup/CouponPopup").then(o.bind(null, "6b0f"));
            }
        }, u = function() {
            var n = this;
            n.$createElement;
            n._self._c, n._isMounted || (n.e0 = function(t) {
                n.isShowCouponPopupKey = !1;
            });
        }, i = [];
    }
}, [ [ "92e7", "common/runtime", "common/vendor" ] ] ]);