(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/setAddress/index" ], {
    "0330": function(t, e, i) {
        i.d(e, "b", function() {
            return o;
        }), i.d(e, "c", function() {
            return r;
        }), i.d(e, "a", function() {
            return n;
        });
        var n = {
            AreaSelect: function() {
                return i.e("components/AreaSelect/AreaSelect").then(i.bind(null, "f9f5"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    },
    "0b10": function(t, e, i) {
        i.r(e);
        var n = i("0330"), o = i("7e50");
        for (var r in o) "default" !== r && function(t) {
            i.d(e, t, function() {
                return o[t];
            });
        }(r);
        i("df50");
        var a = i("f0c5"), c = Object(a.a)(o.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        e.default = c.exports;
    },
    "0f8f": function(t, e, i) {
        (function(t) {
            i("f868"), n(i("66fd"));
            var e = n(i("0b10"));
            function n(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = i, t(e.default);
        }).call(this, i("543d").createPage);
    },
    "24a0": function(t, e, i) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = i("049e"), o = {
                data: function() {
                    return {
                        tags: [ "家", "公司", "学校" ],
                        uuid: "",
                        form: {
                            consignee: "",
                            phone: "",
                            province: "",
                            city: "",
                            district: "",
                            address: "",
                            tag: "",
                            is_default: 0
                        }
                    };
                },
                computed: {
                    defaultValue: function() {
                        return [ this.form.province, this.form.city, this.form.district ];
                    },
                    defaultAddressValue: function() {
                        return this.form.province ? [ this.form.province, this.form.city, this.form.district ] : null;
                    },
                    pickerSelected: function() {
                        return this.form.province ? this.form.province + "-" + this.form.city + "-" + this.form.district : "";
                    }
                },
                onLoad: function(e) {
                    if (t.setNavigationBarTitle({
                        title: e.uuid ? "编辑收货地址" : "新建收货地址"
                    }), e.uuid) {
                        this.uuid = e.uuid, t.setNavigationBarTitle({
                            title: "编辑收货地址"
                        });
                        var i = this.$navigator.getParameters() || {};
                        this.form = {
                            consignee: i.consignee,
                            phone: i.phone,
                            province: i.province,
                            city: i.city,
                            district: i.district,
                            address: i.address,
                            tag: i.tag,
                            is_default: i.is_default
                        };
                    }
                },
                methods: {
                    handleClick: function(t) {
                        this.form.tag = t.currentTarget.dataset.tag;
                    },
                    switchChange: function(t) {
                        this.form.is_default = t ? 1 : 0;
                    },
                    change: function(t) {
                        var e = t.detail.value;
                        this.form.province = e[0], this.form.city = e[1], this.form.district = e[2];
                    },
                    areaChange: function(t) {
                        var e = t.labelArr;
                        this.form.province = e[0], this.form.city = e[1], this.form.district = e[2];
                    },
                    handleSubmit: function() {
                        var e = this.form;
                        e.consignee ? /^1\d{10}$/.test(e.phone) ? this.pickerSelected ? e.address ? (t.showLoading({
                            title: "加载中"
                        }), this.uuid ? (0, n.updateAddress)(e, this.uuid).then(function(e) {
                            t.navigateBack({
                                delta: 1
                            });
                        }) : (0, n.createAddress)(e).then(function(e) {
                            t.navigateBack({
                                delta: 1
                            });
                        })) : t.showToast({
                            title: "请填写详细地址",
                            icon: "none"
                        }) : t.showToast({
                            title: "请选择所在地区",
                            icon: "none"
                        }) : t.showToast({
                            title: "请填写正确的手机号",
                            icon: "none"
                        }) : t.showToast({
                            title: "请填写收货人姓名",
                            icon: "none"
                        });
                    }
                }
            };
            e.default = o;
        }).call(this, i("543d").default);
    },
    "3b0b": function(t, e, i) {},
    "7e50": function(t, e, i) {
        i.r(e);
        var n = i("24a0"), o = i.n(n);
        for (var r in n) "default" !== r && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(r);
        e.default = o.a;
    },
    df50: function(t, e, i) {
        var n = i("3b0b");
        i.n(n).a;
    }
}, [ [ "0f8f", "common/runtime", "common/vendor" ] ] ]);