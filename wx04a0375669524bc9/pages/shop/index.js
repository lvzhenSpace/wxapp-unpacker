(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/shop/index" ], {
    "200f": function(n, e, t) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        e.default = {
            components: {},
            data: function() {
                return {
                    scrollTop: 0,
                    refreshCounter: 0,
                    getNextPageCounter: 0
                };
            },
            computed: {
                page: function() {
                    return this.$store.getters.setting.shop_home;
                }
            },
            watch: {},
            onLoad: function() {
                this.$visitor.record("box_index");
            },
            onPullDownRefresh: function() {
                var n = this;
                this.$showPullRefresh().then(function(e) {
                    n.refreshCounter++;
                });
            },
            onShow: function() {},
            methods: {},
            onReachBottom: function() {
                this.getNextPageCounter++;
            },
            onPageScroll: function(n) {
                this.scrollTop = n.scrollTop;
            }
        };
    },
    "28c1": function(n, e, t) {
        t.d(e, "b", function() {
            return r;
        }), t.d(e, "c", function() {
            return u;
        }), t.d(e, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/PageRender/PageRender") ]).then(t.bind(null, "e85b"));
            }
        }, r = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    "3c39": function(n, e, t) {
        t.r(e);
        var o = t("200f"), r = t.n(o);
        for (var u in o) "default" !== u && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(u);
        e.default = r.a;
    },
    d075: function(n, e, t) {
        t.r(e);
        var o = t("28c1"), r = t("3c39");
        for (var u in r) "default" !== u && function(n) {
            t.d(e, n, function() {
                return r[n];
            });
        }(u);
        var c = t("f0c5"), f = Object(c.a)(r.default, o.b, o.c, !1, null, "428ccfc2", null, !1, o.a, void 0);
        e.default = f.exports;
    },
    f7f8: function(n, e, t) {
        (function(n) {
            t("f868"), o(t("66fd"));
            var e = o(t("d075"));
            function o(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = t, n(e.default);
        }).call(this, t("543d").createPage);
    }
}, [ [ "f7f8", "common/runtime", "common/vendor" ] ] ]);