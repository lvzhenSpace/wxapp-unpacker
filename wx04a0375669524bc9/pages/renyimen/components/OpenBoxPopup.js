(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/renyimen/components/OpenBoxPopup" ], {
    "2bed": function(e, t, n) {
        (function(e) {
            function n(e, t) {
                var n;
                if ("undefined" == typeof Symbol || null == e[Symbol.iterator]) {
                    if (Array.isArray(e) || (n = o(e)) || t && e && "number" == typeof e.length) {
                        n && (e = n);
                        var i = 0, r = function() {};
                        return {
                            s: r,
                            n: function() {
                                return i >= e.length ? {
                                    done: !0
                                } : {
                                    done: !1,
                                    value: e[i++]
                                };
                            },
                            e: function(e) {
                                throw e;
                            },
                            f: r
                        };
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }
                var a, u = !0, s = !1;
                return {
                    s: function() {
                        n = e[Symbol.iterator]();
                    },
                    n: function() {
                        var e = n.next();
                        return u = e.done, e;
                    },
                    e: function(e) {
                        s = !0, a = e;
                    },
                    f: function() {
                        try {
                            u || null == n.return || n.return();
                        } finally {
                            if (s) throw a;
                        }
                    }
                };
            }
            function o(e, t) {
                if (e) {
                    if ("string" == typeof e) return i(e, t);
                    var n = Object.prototype.toString.call(e).slice(8, -1);
                    return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(e) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? i(e, t) : void 0;
                }
            }
            function i(e, t) {
                (null == t || t > e.length) && (t = e.length);
                for (var n = 0, o = new Array(t); n < t; n++) o[n] = e[n];
                return o;
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = {
                props: {
                    boxImg: String,
                    buttonTitle: String,
                    order: Object,
                    tryMode: Boolean,
                    tryInfo: Object,
                    info: Object,
                    isNavbarEnable: !1,
                    istry: Boolean,
                    payTotal: Number,
                    skus: Array,
                    isOpen: Boolean
                },
                data: function() {
                    return {
                        dataMsg: null,
                        showResult: !0,
                        status: 0,
                        isShowReturnSale: !1,
                        isReturnSaleSuccess: !1,
                        package: {},
                        defaultBoxImage: "https://cdn2.hquesoft.com/box/openbox.png",
                        isNotOpen: !1,
                        selectedIds: [],
                        total_money_price: 0,
                        total_score_price: 0,
                        total_score_score: 0,
                        tryskus: [],
                        myInfo: {}
                    };
                },
                mounted: function() {
                    this.initData(), this.getMyInfo();
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    },
                    rewardJikaTimes: function() {
                        return this.package.reward && this.package.reward.jika_times;
                    },
                    rewardLotteryTicket: function() {
                        return this.package.reward && this.package.reward.lottery_ticket;
                    }
                },
                methods: {
                    getMyInfo: function() {
                        var e = this;
                        this.$http("/task/level_list?no_list=1").then(function(t) {
                            e.myInfo = t.data.my_task_level_info;
                        }).catch(function(e) {
                            console.log(e);
                        });
                    },
                    viewFreeOrderDetail: function() {
                        this.info.money_price ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : e.navigateTo({
                            url: "/pages/myScore/index"
                        });
                    },
                    randomSort: function(e, t) {
                        return Math.random() > .5 ? -1 : 1;
                    },
                    initData: function() {},
                    lockInfo: function() {
                        this.$http("/yifanshang/rooms-lock-info", "GET", {
                            room_id: this.info.room.id
                        }).then(function(e) {});
                    },
                    onekeyrecycle_try: function() {
                        e.redirectTo({
                            url: "/pages/page/index?uuid=635180d9968bf"
                        });
                    },
                    onekeyrecycle: function() {
                        var t = this;
                        e.showLoading({
                            title: "分解中...",
                            success: function(o) {
                                var i, r = n(t.skus);
                                try {
                                    for (r.s(); !(i = r.n()).done; ) {
                                        var a = i.value;
                                        t.selectedIds.push(a.id);
                                    }
                                } catch (e) {
                                    r.e(e);
                                } finally {
                                    r.f();
                                }
                                t.$http("/asset/return-sale/confirm", "post", {
                                    ids: t.selectedIds
                                }).then(function(n) {
                                    t.isReturnSaleSuccess = 0, e.showToast({
                                        title: "分解成功"
                                    }), t.refresh(), t.isSelectMode = !1, t.$emit("refresh");
                                });
                            }
                        }), this.close();
                    },
                    startOpenAnimate: function() {
                        var e = this;
                        setTimeout(function() {
                            e.status = 1, setTimeout(function() {
                                e.isOpen = !0, e.$playAudio("open"), setTimeout(function() {
                                    e.showResult = !0;
                                }, 300);
                            }, 200);
                        }, 100);
                    },
                    handleRefresh: function() {
                        this.isNotOpen = !1, this.initData();
                    },
                    goLotteryDetail: function() {
                        e.navigateTo({
                            url: "/pages/lottery/detail?uuid=" + this.rewardLotteryTicket.uuid
                        });
                    },
                    goJikaDetail: function() {
                        e.navigateTo({
                            url: "/pages/jika/detail?uuid=" + this.rewardJikaTimes.uuid
                        });
                    },
                    returnSale: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    handleOk: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    goBack: function() {
                        e.navigateBack({
                            delta: 1
                        });
                    },
                    close: function() {
                        this.$emit("close"), this.$emit("refresh");
                    },
                    handleGuanBiClick: function() {
                        this.$emit("close"), this.$emit("refresh");
                    },
                    checkSku: function(t) {
                        "score" === t.sku_type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "coupon" === t.sku_type ? e.navigateTo({
                            url: "/pages/myCoupons/index"
                        }) : "redpack" === t.sku_type && e.navigateTo({
                            url: "/pages/myRedpack/index"
                        });
                    }
                }
            };
            t.default = r;
        }).call(this, n("543d").default);
    },
    "9b86": function(e, t, n) {
        n.r(t);
        var o = n("2bed"), i = n.n(o);
        for (var r in o) "default" !== r && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    },
    b192: function(e, t, n) {
        n.r(t);
        var o = n("e976"), i = n("9b86");
        for (var r in i) "default" !== r && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("f963");
        var a = n("f0c5"), u = Object(a.a)(i.default, o.b, o.c, !1, null, "8e4a464e", null, !1, o.a, void 0);
        t.default = u.exports;
    },
    b9d6: function(e, t, n) {},
    e976: function(e, t, n) {
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    f963: function(e, t, n) {
        var o = n("b9d6");
        n.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/renyimen/components/OpenBoxPopup-create-component", {
    "pages/renyimen/components/OpenBoxPopup-create-component": function(e, t, n) {
        n("543d").createComponent(n("b192"));
    }
}, [ [ "pages/renyimen/components/OpenBoxPopup-create-component" ] ] ]);