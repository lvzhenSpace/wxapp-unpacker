(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/renyimen/detail" ], {
    "1e48": function(t, n, i) {
        (function(t) {
            i("f868"), e(i("66fd"));
            var n = e(i("9de6"));
            function e(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = i, t(n.default);
        }).call(this, i("543d").createPage);
    },
    "35c1": function(t, n, i) {
        i.r(n);
        var e = i("d574"), o = i.n(e);
        for (var u in e) "default" !== u && function(t) {
            i.d(n, t, function() {
                return e[t];
            });
        }(u);
        n.default = o.a;
    },
    4793: function(t, n, i) {
        i.d(n, "b", function() {
            return o;
        }), i.d(n, "c", function() {
            return u;
        }), i.d(n, "a", function() {
            return e;
        });
        var e = {
            PriceDisplay: function() {
                return i.e("components/PriceDisplay/PriceDisplay").then(i.bind(null, "f149"));
            },
            uniPopup: function() {
                return i.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(i.bind(null, "50c2"));
            },
            BoxSkuPopup: function() {
                return i.e("components/BoxSkuPopup/BoxSkuPopup").then(i.bind(null, "16ac"));
            },
            OpenBoxPopupTheme2: function() {
                return i.e("components/OpenBoxPopupTheme2/OpenBoxPopupTheme2").then(i.bind(null, "3444"));
            },
            OpenBoxPopup: function() {
                return i.e("components/OpenBoxPopup/OpenBoxPopup").then(i.bind(null, "4eb2"));
            },
            FreeTicketFloatBtn: function() {
                return i.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(i.bind(null, "6af9"));
            },
            Danmus: function() {
                return i.e("components/Danmus/Danmus").then(i.bind(null, "7d24"));
            }
        }, o = function() {
            var t = this, n = (t.$createElement, t._self._c, t.__map(t.info.sku_level, function(n, i) {
                return {
                    $orig: t.__get_orig(n),
                    g0: n.odds.toFixed(2)
                };
            }));
            t.$mp.data = Object.assign({}, {
                $root: {
                    l0: n
                }
            });
        }, u = [];
    },
    "9de6": function(t, n, i) {
        i.r(n);
        var e = i("4793"), o = i("35c1");
        for (var u in o) "default" !== u && function(t) {
            i.d(n, t, function() {
                return o[t];
            });
        }(u);
        i("c9c5");
        var s = i("f0c5"), a = Object(s.a)(o.default, e.b, e.c, !1, null, "e412b840", null, !1, e.a, void 0);
        n.default = a.exports;
    },
    c136: function(t, n, i) {},
    c9c5: function(t, n, i) {
        var e = i("c136");
        i.n(e).a;
    },
    d574: function(t, n, i) {
        (function(t) {
            function e(t, n) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var e = Object.getOwnPropertySymbols(t);
                    n && (e = e.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), i.push.apply(i, e);
                }
                return i;
            }
            function o(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var i = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? e(Object(i), !0).forEach(function(n) {
                        u(t, n, i[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : e(Object(i)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(i, n));
                    });
                }
                return t;
            }
            function u(t, n, i) {
                return n in t ? Object.defineProperty(t, n, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[n] = i, t;
            }
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var s = {
                components: {
                    OpenBoxPopup: function() {
                        i.e("pages/renyimen/components/OpenBoxPopup").then(function() {
                            return resolve(i("b192"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    PayCard: function() {
                        Promise.all([ i.e("common/vendor"), i.e("pages/renyimen/components/PayCard") ]).then(function() {
                            return resolve(i("86cc"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    RecordList: function() {
                        i.e("pages/renyimen/components/RecordList").then(function() {
                            return resolve(i("381b"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    imagepop: function() {
                        i.e("pages/renyimen/components/imagepop").then(function() {
                            return resolve(i("2263"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    bangDan: function() {
                        Promise.all([ i.e("common/vendor"), i.e("pages/renyimen/components/bangDan") ]).then(function() {
                            return resolve(i("9361"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    wanfa: function() {
                        i.e("pages/renyimen/components/wanfa").then(function() {
                            return resolve(i("0dcb"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    jiangli: function() {
                        i.e("pages/renyimen/components/jiangli").then(function() {
                            return resolve(i("5ebd"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        noclick: !0,
                        huadong: !1,
                        movableX: 0,
                        movableXNew: 0,
                        swiperHeight: 0,
                        duocishu: null,
                        duocishuNum: 0,
                        limitNum: 0,
                        nowTeamTotal: null,
                        emitTotal: null,
                        limitCountNum: 0,
                        lianjiCiShu: 1,
                        emitWordTotal: 1,
                        current: 0,
                        types: [ "pending", "resale_pending" ],
                        typeTextList: [ "连击池", "秘宝池" ],
                        bgImgs: [ "url(https://img121.7dun.com/20230207NewImg/wuxianshang/rDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/srDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/ssrDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/urDes.png)", "url()" ],
                        uuid: "",
                        isInit: !1,
                        isPayPopup: !1,
                        payTotal: 1,
                        info: {
                            min_lucky_score: ""
                        },
                        order: {},
                        pageUuid: "",
                        isNotFound: !1,
                        isShowResultPopup: !1,
                        danmuSetting: {},
                        danmuList: [],
                        config: {},
                        detailImageList: [],
                        levelFilter: 0,
                        isTryMode: !1,
                        isShowRankList: !1,
                        wining_list: [],
                        isimagepop: !1,
                        is_doubleBoxCard: 0,
                        skuinfo: {},
                        skusList: [],
                        skus_ultimate: {},
                        openSkus: [],
                        isOpenPopup: !1,
                        istry: !1,
                        isOpen: !1
                    };
                },
                computed: o(o({}, (0, i("26cb").mapGetters)([ "userInfo" ])), {}, {
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/fudai/detail_bg.png";
                    },
                    boxImage: function() {
                        return this.info.image_3d || "https://cdn2.hquesoft.com/box/fudai/box.png";
                    },
                    skuLevel: function() {
                        return this.info.sku_level || [];
                    },
                    skus: function() {
                        var t = this;
                        return this.levelFilter ? this.info.skus.filter(function(n) {
                            return n.level === t.levelFilter;
                        }) : this.info.skus || [];
                    },
                    payInfo: function() {
                        return {
                            page_uuid: this.pageUuid,
                            title: this.info.title,
                            pay_total: this.payTotal,
                            total_list: this.info.total_list,
                            money_price: this.info.money_price,
                            score_price: this.info.score_price
                        };
                    },
                    titleBgColor: function() {
                        return " ";
                    },
                    titleColor: function() {
                        return "#ffffff";
                    },
                    setTitleText: function() {
                        t.setNavigationBarTitle({
                            title: this.info.title
                        });
                    },
                    discountTips: function() {
                        for (var t = this.info.total_list || [], n = t.length - 1; n >= 0; n--) if (t[n].is_discount) {
                            var i = t[n].total + "连开优惠";
                            return t[n].money_discount && (i += t[n].money_discount / 100 + "元"), t[n].score_discount && (i += t[n].score_discount + this.scoreAlias), 
                            i;
                        }
                        return !1;
                    },
                    width_value: function() {
                        return this.userInfo.lucky_score / this.info.min_lucky_score * 100 > 100 ? 100 : this.userInfo.lucky_score / this.info.min_lucky_score * 100 < 0 ? 0 : Math.trunc(this.userInfo.lucky_score / this.info.min_lucky_score * 100);
                    }
                }),
                watch: {},
                filters: {},
                onLoad: function(t) {
                    this.uuid = t.uuid;
                },
                onUnload: function() {},
                onPullDownRefresh: function() {
                    this.$showPullRefresh(), this.initData(), this.setTitleText();
                },
                onShow: function() {
                    var n = this;
                    this.$nextTick(function() {
                        n.current = 0, n.movableX = 0;
                    }), t.showLoading({
                        title: "加载中"
                    }), this.initData().then(function(i) {
                        n.isInit = !0, t.hideLoading(), n.$http("/renyi/change", "POST", {
                            renyi_id: n.info.id
                        }).then(function(t) {
                            n.pageUuid = t.data.page_uuid, n.fetchList();
                        });
                    }).catch(function(t) {
                        n.isNotFound = !0;
                    });
                },
                onReachBottom: function() {},
                methods: {
                    doudong: function() {
                        var t = this;
                        setTimeout(function() {
                            t.huadong || (t.movableXNew -= 15, setTimeout(function() {
                                t.huadong || (t.movableXNew += 30, setTimeout(function() {
                                    t.huadong || (t.movableXNew -= 30, setTimeout(function() {
                                        t.huadong || (t.movableXNew += 30, setTimeout(function() {
                                            t.huadong || (t.movableXNew = 10);
                                        }, 100));
                                    }, 100));
                                }, 100));
                            }, 100));
                        }, 100);
                    },
                    onMovableChange: function(t) {
                        "touch" === t.detail.source && (this.movableXNew = t.detail.x);
                    },
                    onTouchEndChange: function() {
                        var t = this;
                        this.movableX = this.movableXNew, this.$nextTick(function() {
                            t.movableX < 80 ? (t.movableX = 0, t.current = 0) : (t.movableX = 146, t.current = 1);
                        });
                    },
                    setSwiperHeight: function() {
                        var n = this;
                        setTimeout(function() {
                            0 == n.current ? t.createSelectorQuery().in(n).select("#content-wrap").boundingClientRect(function(t) {
                                n.swiperHeight = t.height;
                            }).exec(function(t) {
                                n.swiperHeight = t[0].height;
                            }) : t.createSelectorQuery().in(n).select("#content-wrap1").boundingClientRect(function(t) {
                                n.swiperHeight = t.height + 30;
                            }).exec(function(t) {
                                n.swiperHeight = t[0].height + 30;
                            });
                        }, 200);
                    },
                    handleRecordList: function() {
                        this.isShowRankList = !0;
                    },
                    handleRecordListClose: function() {
                        this.isShowRankList = !1;
                    },
                    handleMaxClick: function() {
                        t.vibrateShort({}), this.emitWordTotal - this.limitNum <= 0 || (this.lianjiCiShu = this.emitWordTotal - this.limitNum);
                    },
                    handleJianClick: function() {
                        t.vibrateShort({}), 1 != this.lianjiCiShu && 0 != this.lianjiCiShu ? this.lianjiCiShu = this.lianjiCiShu - 1 : t.showToast({
                            title: "不能再少了哦~",
                            icon: "none"
                        });
                    },
                    handleJiaClick: function() {
                        t.vibrateShort({}), this.lianjiCiShu != this.emitWordTotal - this.limitNum ? this.lianjiCiShu = this.lianjiCiShu + 1 : t.showToast({
                            title: "已达最大次数~",
                            icon: "none"
                        });
                    },
                    handleCountJian: function() {
                        t.vibrateShort({}), 1 != this.duocishuNum ? this.duocishuNum = this.duocishuNum - 1 : t.showToast({
                            title: "不能再少了哦~",
                            icon: "none"
                        });
                    },
                    handleCountJia: function() {
                        t.vibrateShort({}), this.duocishuNum != this.duocishu ? this.duocishuNum = this.duocishuNum + 1 : t.showToast({
                            title: "已达最大次数~",
                            icon: "none"
                        });
                    },
                    handleBaoPay: function() {
                        var n = this;
                        t.showLoading({
                            mask: !0
                        }), this.$http("/renyi/buy_word", "POST", {
                            renyi_id: this.info.id,
                            total: this.duocishuNum
                        }).then(function(i) {
                            t.hideLoading(), setTimeout(function() {
                                n.$playAudio("open"), setTimeout(function() {
                                    n.openSkus = i.data, n.isOpenPopup = !0, n.istry = !1, n.isOpen = !0;
                                }, 500);
                            }, 400);
                        });
                    },
                    goOpenBack: function() {
                        this.isOpenPopup = !1, this.istry = !0, this.isOpen = !1, this.limitCx();
                    },
                    limitCx: function() {
                        var t = this;
                        this.$http("/renyis/".concat(this.uuid, "/limit-cx"), "GET", {}).then(function(n) {
                            t.lianjiCiShu = 1, t.limitCountNum = n.data.info.count, t.limitNum = n.data.info.num, 
                            t.emitTotal = n.data.team.emit_total, t.nowTeamTotal = n.data.team.now_team_total, 
                            t.duocishuNum = n.data.info.use_count, t.duocishu = n.data.info.use_count;
                        });
                    },
                    currentTransition: function(t) {},
                    currentChange: function(t) {
                        var n = t.currentTarget.dataset.current;
                        n !== this.current && (this.current = n, 0 == this.current ? this.movableX = 0 : this.movableX = 146);
                    },
                    currentChange2: function(t) {
                        var n = this, i = t.detail.current;
                        this.$nextTick(function() {
                            n.setSwiperHeight();
                        }), i !== this.current && (this.current = i, 0 == this.current ? this.movableX = 0 : this.movableX = 146);
                    },
                    handleTry: function() {
                        t.showLoading({
                            title: "刷新中..."
                        }), this.initData().then(function(n) {
                            t.hideLoading(), t.showToast({
                                title: "刷新完成~",
                                icon: "none"
                            });
                        });
                    },
                    fetchList: function() {
                        if (this.isLoading) return !1;
                        this.isLoading = !0;
                    },
                    addClick: function() {
                        t.vibrateShort({}), 1 === this.payTotal ? this.payTotal = this.payTotal + 2 : 3 === this.payTotal ? this.payTotal = this.payTotal + 7 : t.showToast({
                            title: "最多十抽哦~",
                            icon: "none"
                        });
                    },
                    minusClick: function() {
                        t.vibrateShort({}), 3 === this.payTotal ? this.payTotal = this.payTotal - 2 : 10 === this.payTotal ? this.payTotal = this.payTotal - 7 : 20 === this.payTotal ? this.payTotal = this.payTotal - 10 : t.showToast({
                            title: "最少一抽哦~",
                            icon: "none"
                        });
                    },
                    useFreeTicket: function() {
                        var n = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/renyi/order/confirm", "POST", {
                            page_uuid: this.pageUuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(i) {
                            t.hideLoading();
                            var e = i.data;
                            e.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : n.paySuccess(e.order);
                        });
                    },
                    getDanmu: function() {},
                    showDetailImagePopup: function(t, n) {
                        t.odds = n, this.skuinfo = t, this.isimagepop = !0;
                    },
                    hideDetailImagePopup: function() {
                        this.$refs.detailPopup.close();
                    },
                    isimagepopfun: function() {
                        this.isimagepop = !1;
                    },
                    refresh: function() {
                        this.$store.dispatch("getUserInfo");
                    },
                    initData: function() {
                        var n = this;
                        return this.$http("/renyis/".concat(this.uuid), "GET", {}).then(function(i) {
                            t.hideLoading(), n.lianjiCiShu = 1, n.info = i.data.info, n.config = i.data.config, 
                            n.info.sku_level = n.info.sku_level.sort().reverse(), n.skusList = i.data.info.skus.filter(function(t) {
                                return 5 === t.level;
                            }), n.emitWordTotal = i.data.info.emit_word_total, n.emitTotal = i.data.info.emit_total, 
                            n.nowTeamTotal = i.data.info.now_team_total, "[]" != JSON.stringify(i.data.limit) && (n.limitCountNum = i.data.limit.count, 
                            n.limitNum = i.data.limit.num, n.duocishu = i.data.limit.use_count, n.duocishuNum = i.data.limit.use_count), 
                            n.$nextTick(function() {
                                n.setSwiperHeight();
                            });
                            var e = 1;
                            n.skus_ultimate = n.skusList[0], setInterval(function() {
                                n.skus_ultimate = n.skusList[e], ++e >= n.skusList.length && (e = 0);
                            }, 3e3), n.getDanmu(), t.setNavigationBarTitle({
                                title: n.info.title
                            });
                        });
                    },
                    paySuccess: function(t) {
                        var n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], i = arguments.length > 2 ? arguments[2] : void 0;
                        this.isTryMode = n, this.order = t, this.is_doubleBoxCard = i, this.isPayPopup = !1, 
                        this.isShowResultPopup = !0, this.refresh(), this.initData();
                    },
                    goBack: function() {
                        this.isShowResultPopup = !1;
                    },
                    hidePayPopup: function() {
                        this.isPayPopup = !1;
                    },
                    pay: function(t) {
                        this.payTotal = t, this.isPayPopup = !0;
                    },
                    getLevelIcon: function(t) {
                        return this.skuLevel.find(function(n) {
                            return n.level === t;
                        }).icon;
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = s;
        }).call(this, i("543d").default);
    }
}, [ [ "1e48", "common/runtime", "common/vendor" ] ] ]);