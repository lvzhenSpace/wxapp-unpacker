(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/detail" ], {
    "047f": function(t, e, n) {
        n.d(e, "b", function() {
            return r;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var r = function() {
            var t = this, e = (t.$createElement, t._self._c, 1 == t.userInfo.vip_status ? t.$tool.formatDate(t.userInfo.vip_end_at, "yyyy-MM-dd") : null);
            t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, o = [];
    },
    3112: function(t, e, n) {
        (function(t) {
            function r(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var r = Object.getOwnPropertySymbols(t);
                    e && (r = r.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, r);
                }
                return n;
            }
            function o(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? r(Object(n), !0).forEach(function(e) {
                        i(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            function i(t, e, n) {
                return e in t ? Object.defineProperty(t, e, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[e] = n, t;
            }
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var u = {
                name: "center",
                components: {},
                data: function() {
                    return {
                        code: "",
                        init: !1,
                        isAnimated: !0
                    };
                },
                computed: o(o({}, (0, n("26cb").mapGetters)([ "deviceInfo", "userInfo", "token", "personalSettings" ])), {}, {
                    isLogin: function() {
                        return !!this.token;
                    },
                    userName: function() {
                        return this.token ? this.userInfo.name : "点击登录";
                    },
                    avatar: function() {
                        return this.token ? this.userInfo.headimg : "/static/toux.png";
                    },
                    config: function() {
                        return this.$store.getters.setting.user_center;
                    },
                    items: function() {
                        return this.config && this.config.items || [];
                    }
                }),
                onLoad: function() {
                    var e = this;
                    t.login({
                        success: function(t) {
                            e.code = t.code;
                        },
                        fail: function(t) {}
                    }), this.$visitor.record("center");
                },
                onShow: function() {
                    this.$store.dispatch("getUserInfo");
                },
                methods: {
                    toDetail: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    },
                    toDayTask: function() {
                        this.isAnimated = !1, t.navigateTo({
                            url: "/pages/dayTask/index"
                        });
                    },
                    toBuyVip: function() {
                        t.navigateTo({
                            url: "/pages/buyVip/index"
                        });
                    }
                }
            };
            e.default = u;
        }).call(this, n("543d").default);
    },
    "5a06": function(t, e, n) {
        n.r(e);
        var r = n("3112"), o = n.n(r);
        for (var i in r) "default" !== i && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(i);
        e.default = o.a;
    },
    "5f29": function(t, e, n) {
        var r = n("6183");
        n.n(r).a;
    },
    6183: function(t, e, n) {},
    bab8: function(t, e, n) {
        n.r(e);
        var r = n("047f"), o = n("5a06");
        for (var i in o) "default" !== i && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        n("5f29");
        var u = n("f0c5"), a = Object(u.a)(o.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        e.default = a.exports;
    },
    f9e8: function(t, e, n) {
        (function(t) {
            n("f868"), r(n("66fd"));
            var e = r(n("bab8"));
            function r(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    }
}, [ [ "f9e8", "common/runtime", "common/vendor" ] ] ]);