(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/index" ], {
    1076: function(e, t, n) {
        (function(e) {
            n("f868"), o(n("66fd"));
            var t = o(n("6a47"));
            function o(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, e(t.default);
        }).call(this, n("543d").createPage);
    },
    4613: function(e, t, n) {
        var o = n("4a35");
        n.n(o).a;
    },
    "4a35": function(e, t, n) {},
    6294: function(e, t, n) {
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            Banner: function() {
                return n.e("components/Banner/Banner").then(n.bind(null, "3d80"));
            },
            FloatBtn: function() {
                return n.e("components/FloatBtn/FloatBtn").then(n.bind(null, "9d70"));
            }
        }, i = function() {
            var e = this, t = (e.$createElement, e._self._c, e.config.is_show_vip && 1 == e.userInfo.vip_status ? e.$tool.formatDate(e.userInfo.vip_end_at, "yyyy-MM-dd") : null), n = e._f("0")(e.myLevel.my_level_score), o = e.level_title ? e._f("0")(e.myLevel.my_level_score) : null;
            e.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    f0: n,
                    f1: o
                }
            });
        }, r = [];
    },
    "6a47": function(e, t, n) {
        n.r(t);
        var o = n("6294"), i = n("d624");
        for (var r in i) "default" !== r && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("4613");
        var a = n("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = c.exports;
    },
    bc25: function(e, t, n) {
        (function(e) {
            function o(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function i(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? o(Object(n), !0).forEach(function(t) {
                        r(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : o(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            function r(e, t, n) {
                return t in e ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = n, e;
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var a = {
                name: "center",
                components: {},
                data: function() {
                    return {
                        level_title: !0,
                        need_level_score: 0,
                        next_score: 0,
                        heightEle1: 0,
                        heightEle2: 0,
                        heightEle3: 0,
                        progress: 0,
                        myLevel: {},
                        version: "1.0.8",
                        init: !1,
                        orderStat: {},
                        count_score_redpack: 0,
                        myInfo: {},
                        vipShow: !1
                    };
                },
                computed: i(i({}, (0, n("26cb").mapGetters)([ "deviceInfo", "userInfo", "token", "personalSettings" ])), {}, {
                    isLogin: function() {
                        return !!this.token;
                    },
                    isscoreRedpack: function() {
                        return this.token && this.scoreRedpack ? this.count_score_redpack = this.scoreRedpack[0] : this.count_score_redpack = 0;
                    },
                    userName: function() {
                        return this.token ? this.userInfo.name : "点击登录";
                    },
                    avatar: function() {
                        return this.token ? this.userInfo.headimg : "/static/toux.png";
                    },
                    config: function() {
                        return this.$store.getters.setting.user_center;
                    },
                    items: function() {
                        return this.config && this.config.items || [];
                    },
                    isAnimated: function() {
                        return !0;
                    },
                    floatBtn: function() {
                        return {
                            is_show: this.config.is_show_float_btn,
                            link: this.config.float_btn_link,
                            image: this.config.float_btn_image,
                            is_animate: this.config.float_btn_is_animate
                        };
                    }
                }),
                onLoad: function() {
                    this.$visitor.record("center");
                },
                onShow: function() {
                    var t = this;
                    this.getMyInfo(), this.getLevelList(), this.isLogin && (this.$store.dispatch("getUserInfo").then(function(n) {
                        t.$http("/order-stat").then(function(e) {
                            t.orderStat = e.data.info;
                        }), t.userInfo && "微信用户" == t.userInfo.name && e.showModal({
                            title: "检测到您使用的是微信默认昵称!请先修改",
                            confirmText: "去修改",
                            showCancel: !1,
                            success: function(e) {
                                e.confirm && t.toProfilePage(), console.log("用户点击确定");
                            }
                        });
                    }), this.$store.dispatch("getCountScoreRedpack").then(function(e) {
                        t.count_score_redpack = e.data.info[0];
                    }));
                },
                methods: {
                    getLevelList: function() {
                        var e = this;
                        this.$http("/task/level_list").then(function(t) {
                            e.myLevel = t.data, e.need_level_score = t.data.next_task_level_info.need_level_score - t.data.my_level_score, 
                            e.progress = t.data.my_level_score / t.data.next_task_level_info.need_level_score * 100, 
                            e.next_score = t.data.next_task_level_info.need_level_score, 50 == t.data.my_task_level && 0 == t.data.next_task_level_info.length && (e.next_score = t.data.my_level_score, 
                            e.progress = 100, e.level_title = !1);
                        });
                    },
                    getMyInfo: function() {
                        var e = this;
                        this.$http("/task/level_list?no_list=1").then(function(t) {
                            e.myInfo = t.data.my_task_level_info, e.vipShow = !0;
                        }).catch(function(e) {
                            console.log(e);
                        });
                    },
                    toProfilePage: function() {
                        e.navigateTo({
                            url: "/pages/myProfile/index"
                        });
                    },
                    toDetail: function(t) {
                        e.navigateTo({
                            url: t
                        });
                    },
                    toDayTask: function() {
                        this.isAnimated = !1, e.navigateTo({
                            url: "/pages/dayTask/index"
                        });
                    },
                    toBuyVip: function() {
                        e.navigateTo({
                            url: "/pages/buyVip/index"
                        });
                    },
                    memberDetail: function() {
                        e.navigateTo({
                            url: "/member/pages/memberGrade/memberGrade"
                        });
                    }
                }
            };
            t.default = a;
        }).call(this, n("543d").default);
    },
    d624: function(e, t, n) {
        n.r(t);
        var o = n("bc25"), i = n.n(o);
        for (var r in o) "default" !== r && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    }
}, [ [ "1076", "common/runtime", "common/vendor" ] ] ]);