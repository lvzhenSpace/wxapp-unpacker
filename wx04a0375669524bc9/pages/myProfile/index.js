(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myProfile/index" ], {
    "0307": function(e, t, n) {
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            GetPhonePopup: function() {
                return n.e("components/GetPhonePopup/GetPhonePopup").then(n.bind(null, "4a88"));
            }
        }, i = function() {
            var e = this;
            e.$createElement;
            e._self._c, e._isMounted || (e.e0 = function(t) {
                e.isShowPhonePopup = !1;
            });
        }, r = [];
    },
    "29ff": function(e, t, n) {
        n.r(t);
        var o = n("ad86"), i = n.n(o);
        for (var r in o) "default" !== r && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    },
    "3a0b": function(e, t, n) {},
    6755: function(e, t, n) {
        var o = n("a763");
        n.n(o).a;
    },
    "8e23": function(e, t, n) {
        var o = n("3a0b");
        n.n(o).a;
    },
    a763: function(e, t, n) {},
    ad86: function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }(n("a34a"));
            function i(e, t, n, o, i, r, a) {
                try {
                    var c = e[r](a), u = c.value;
                } catch (e) {
                    return void n(e);
                }
                c.done ? t(u) : Promise.resolve(u).then(o, i);
            }
            function r(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function a(e, t, n) {
                return t in e ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = n, e;
            }
            var c = {
                components: {},
                data: function() {
                    return {
                        gender: [ "男", "女", "保密" ],
                        genderIndex: 0,
                        code: "",
                        isShowPhonePopup: !1,
                        form: {
                            headimg: "",
                            name: "",
                            phone: "",
                            city: "",
                            gender: "",
                            email: "",
                            birthday: ""
                        }
                    };
                },
                computed: function(e) {
                    for (var t = 1; t < arguments.length; t++) {
                        var n = null != arguments[t] ? arguments[t] : {};
                        t % 2 ? r(Object(n), !0).forEach(function(t) {
                            a(e, t, n[t]);
                        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(t) {
                            Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                        });
                    }
                    return e;
                }({}, (0, n("26cb").mapGetters)([ "userInfo", "token" ])),
                watch: {
                    userInfo: function() {
                        this.initForm();
                    }
                },
                created: function() {
                    var t = this;
                    e.login({
                        success: function(e) {
                            t.code = e.code;
                        },
                        fail: function(e) {}
                    });
                },
                onLoad: function() {
                    var e = this;
                    return function(e) {
                        return function() {
                            var t = this, n = arguments;
                            return new Promise(function(o, r) {
                                var a = e.apply(t, n);
                                function c(e) {
                                    i(a, o, r, c, u, "next", e);
                                }
                                function u(e) {
                                    i(a, o, r, c, u, "throw", e);
                                }
                                c(void 0);
                            });
                        };
                    }(o.default.mark(function t() {
                        return o.default.wrap(function(t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                e.$store.dispatch("getUserInfo"), e.initForm();

                              case 2:
                              case "end":
                                return t.stop();
                            }
                        }, t);
                    }))();
                },
                methods: {
                    onChooseAvatar: function(t) {
                        var n = "wechat-" + (e.getSystemInfoSync().appName || "") + "-miniapp-" + (e.getSystemInfoSync().platform || ""), o = this;
                        wx.uploadFile({
                            url: "https://api.peiapp.7dun.com/image",
                            filePath: t.detail.avatarUrl,
                            name: "image",
                            header: {
                                Authorization: this.token || "",
                                "Client-Type": n,
                                "Client-Name": "default",
                                "content-type": "multipart/form-data"
                            },
                            success: function(e) {
                                e.data, console.log("image=>", JSON.parse(e.data).data.image.url), o.form.headimg = JSON.parse(e.data).data.image.url;
                            }
                        });
                    },
                    bindInviter: function() {
                        var t = this;
                        e.showModal({
                            title: "请输入邀请码",
                            editable: !0,
                            success: function(n) {
                                if (n.confirm) {
                                    if (!n.content) return !1;
                                    t.$http("/user/bind-inviter", "POST", {
                                        invite_code: n.content
                                    }).then(function(n) {
                                        e.showToast({
                                            title: "绑定成功~",
                                            icon: "none"
                                        }), t.$store.dispatch("getUserInfo");
                                    });
                                }
                            }
                        });
                    },
                    handleDeleteAccount: function() {
                        var t = this;
                        e.showModal({
                            title: "确定要删除帐号吗?",
                            content: "删除帐号后所有帐号资料将无法找回，请谨慎操作!",
                            confirmText: "仍然删除",
                            cancelText: "暂不删除",
                            success: function(n) {
                                n.confirm && (e.showLoading({
                                    title: "申请中..."
                                }), t.$http("/user", "DELETE").then(function(t) {
                                    e.hideLoading(), e.showModal({
                                        title: "申请成功",
                                        content: "已为您申请删除帐号，待客服审核通过后帐号即会被删除~"
                                    });
                                }));
                            }
                        });
                    },
                    handleLogout: function() {
                        var t = this;
                        e.showModal({
                            title: "确定要退出登录吗?",
                            confirmText: "退出登录",
                            cancelText: "暂不",
                            success: function(n) {
                                n.confirm && (t.$store.dispatch("logout"), e.showToast({
                                    title: "已退出登陆，跳转中~",
                                    icon: "none"
                                }), setTimeout(function() {
                                    e.switchTab({
                                        url: "/pages/index/index"
                                    });
                                }, 1800));
                            }
                        });
                    },
                    startGetPhone: function() {
                        this.isShowPhonePopup = !0;
                    },
                    getPhoneNumberSuccess: function(t) {
                        var n = this;
                        this.$http("/phone-update/with-code", "POST", {
                            phone: t.phone,
                            phone_code: t.phone_code
                        }).then(function(t) {
                            n.$store.dispatch("getUserInfo"), e.showToast({
                                title: "手机号更新成功~",
                                icon: "none"
                            }), n.isShowPhonePopup = !1;
                        });
                    },
                    initForm: function() {
                        this.form.headimg = this.userInfo.headimg, this.form.name = this.userInfo.name, 
                        this.form.phone = this.userInfo.phone, this.form.city = this.userInfo.city, this.form.gender = this.userInfo.gender, 
                        this.form.email = this.userInfo.email, this.form.birthday = this.userInfo.birthday, 
                        this.genderIndex = "男" === this.userInfo.gender ? 0 : 1;
                    },
                    updateProfile: function(t) {
                        console.log(t), e.showLoading({
                            title: "更新中"
                        }), console.log(this.form), this.$http({
                            url: "/user",
                            method: "PUT",
                            data: this.form
                        }).then(function(t) {
                            e.hideLoading(), e.showToast({
                                title: "更新成功，跳转中~",
                                icon: "none"
                            }), setTimeout(function() {
                                e.switchTab({
                                    url: "/pages/index/index"
                                });
                            }, 1300);
                        }).catch(function(e) {});
                    },
                    selectBirthday: function(e) {
                        this.form.birthday = e.detail.value;
                    },
                    selectGender: function(e) {
                        this.genderIndex = e.detail.value, this.form.gender = this.gender[this.genderIndex];
                    },
                    getPhoneNumber: function(t) {
                        var n = this;
                        t.detail.encryptedData && (console.log("eeee", t), this.$http("/phone-update/with-miniapp", "POST", {
                            encrypt_data: t.detail.encryptedData,
                            iv: t.detail.iv,
                            code: this.code
                        }).then(function(t) {
                            n.$store.dispatch("getUserInfo"), e.showModal({
                                title: "手机号更新成功~",
                                icon: "none"
                            }), e.login({
                                success: function(e) {
                                    n.code = e.code;
                                },
                                fail: function(e) {}
                            });
                        }));
                    }
                },
                onShow: function() {},
                onPageScroll: function(e) {
                    this.scrollTop = e.scrollTop;
                }
            };
            t.default = c;
        }).call(this, n("543d").default);
    },
    b25c: function(e, t, n) {
        (function(e) {
            n("f868"), o(n("66fd"));
            var t = o(n("da51"));
            function o(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, e(t.default);
        }).call(this, n("543d").createPage);
    },
    da51: function(e, t, n) {
        n.r(t);
        var o = n("0307"), i = n("29ff");
        for (var r in i) "default" !== r && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("6755"), n("8e23");
        var a = n("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, "de66658a", null, !1, o.a, void 0);
        t.default = c.exports;
    }
}, [ [ "b25c", "common/runtime", "common/vendor" ] ] ]);