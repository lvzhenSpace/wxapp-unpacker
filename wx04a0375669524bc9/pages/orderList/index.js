(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/orderList/index" ], {
    "40a1": function(t, e, n) {},
    6897: function(t, e, n) {
        var i = n("40a1");
        n.n(i).a;
    },
    8661: function(t, e, n) {
        n.d(e, "b", function() {
            return r;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            },
            ReturnSalePopupOld: function() {
                return n.e("components/ReturnSalePopupOld/ReturnSalePopupOld").then(n.bind(null, "bd359"));
            }
        }, r = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__map(t.types, function(e, n) {
                return {
                    $orig: t.__get_orig(e),
                    l0: t.__map(t.dataList[n].list, function(e, n) {
                        return {
                            $orig: t.__get_orig(e),
                            g0: t.selectedIds.indexOf(e.id)
                        };
                    })
                };
            }));
            t._isMounted || (t.e0 = function(e) {
                t.isShowPay = !1;
            }, t.e1 = function(e) {
                t.isShowReturnSalePopup = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l1: e
                }
            });
        }, o = [];
    },
    cd03: function(t, e, n) {
        (function(t) {
            n("f868"), i(n("66fd"));
            var e = i(n("dfd7"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    },
    dfd7: function(t, e, n) {
        n.r(e);
        var i = n("8661"), r = n("ea80");
        for (var o in r) "default" !== o && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(o);
        n("6897");
        var a = n("f0c5"), s = Object(a.a)(r.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = s.exports;
    },
    ea80: function(t, e, n) {
        n.r(e);
        var i = n("f965"), r = n.n(i);
        for (var o in i) "default" !== o && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(o);
        e.default = r.a;
    },
    f965: function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = o(n("a34a")), r = o(n("ae4c"));
            function o(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            function a(t, e, n, i, r, o, a) {
                try {
                    var s = t[o](a), c = s.value;
                } catch (t) {
                    return void n(t);
                }
                s.done ? e(c) : Promise.resolve(c).then(i, r);
            }
            function s(t) {
                return function(t) {
                    if (Array.isArray(t)) return c(t);
                }(t) || function(t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
                }(t) || function(t, e) {
                    if (t) {
                        if ("string" == typeof t) return c(t, e);
                        var n = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? c(t, e) : void 0;
                    }
                }(t) || function() {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function c(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i;
            }
            var u = {
                components: {
                    OrderItem: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/orderList/components/OrderItem") ]).then(function() {
                            return resolve(n("e803"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    IActionSheet: function() {
                        n.e("components/ActionSheet/index").then(function() {
                            return resolve(n("9dcb"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    PayCard: function() {
                        n.e("pages/orderList/components/PayCard").then(function() {
                            return resolve(n("4626"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        order: {},
                        reasons: [],
                        list: [],
                        visible: !1,
                        dataList: [],
                        current: 0,
                        currentUuid: "6056e83ef3251",
                        types: [ "pay_pending", "deliver_pending", "delivered", "all" ],
                        typeTextList: [ "待付款", "待发货", "待收货", "全部" ],
                        arrList: [ "暂无待付款的订单", "暂无待发货的订单", "暂无待收货的订单", "暂无订单列表" ],
                        isSelectMode: !1,
                        selectedIds: [],
                        isShowPay: !1,
                        isShowReturnSalePopup: !1,
                        isRefresh: !1,
                        statusTotal: {}
                    };
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    }
                },
                onLoad: function(e) {
                    var n = this;
                    this.current = this.types.indexOf(e.status || "pending"), this.initData(), this.$api.emit("order.cancel_reason.list").then(function(t) {
                        n.reasons = t.data.list;
                    }), t.$on("postCommentSuccess", function(t) {
                        n.initData(), n.getOrderList(), console.log("postCommentSuccess");
                    });
                },
                onShow: function() {
                    this.refresh();
                },
                onUnload: function() {
                    t.$off("postCommentSuccess", function(t) {
                        console.log("移除postCommentSuccess 自定义事件");
                    });
                },
                methods: {
                    initTotalData: function() {
                        var t = this;
                        this.$http("/order-stat").then(function(e) {
                            t.statusTotal = e.data.info;
                        });
                    },
                    successDeliver: function() {
                        this.cancelSelect(), this.current = 2, this.refresh(), this.isShowPay = !1, t.showModal({
                            title: "发货成功",
                            content: "已成功提交发货请求，请注意查收快递哦~"
                        });
                    },
                    cancelSelect: function() {
                        this.isSelectMode = !1, this.selectedIds = [];
                    },
                    selectOrSubmit: function() {
                        if (this.isSelectMode) {
                            if (0 == this.selectedIds.length) return t.showModal({
                                title: "请选择盒子",
                                content: "选择一个或多个盒子后才能提交发货哦~"
                            }), !1;
                            this.isShowPay = !0;
                        } else this.isSelectMode = !0;
                    },
                    checkItem: function(t) {
                        var e = this.selectedIds.indexOf(t.id);
                        e > -1 ? this.selectedIds.splice(e, 1) : this.selectedIds.push(t.id);
                    },
                    refresh: function() {
                        this.isRefresh = !0, this.cleanData(), this.getOrderList(), this.initTotalData();
                    },
                    scrolltolower: function() {
                        this.dataList[this.current].page++, this.getOrderList();
                    },
                    initData: function() {
                        console.log(666);
                        var t = [];
                        this.types.forEach(function(e) {
                            t.push({
                                list: [],
                                type: e,
                                page: 1,
                                per_page: 50,
                                total: 0,
                                init: !1,
                                loading: !1
                            });
                        }), this.dataList = t;
                    },
                    cleanData: function() {
                        this.dataList.forEach(function(t) {
                            t.page = 1, t.init = !1;
                        });
                    },
                    cancelOrder: function(e) {
                        var n = this;
                        this.visibleChange(), t.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.close", this.order.uuid).then(function(e) {
                            var i = n.dataList[n.current];
                            n.$api.emit("order.list", {
                                status: i.type,
                                page: 1,
                                per_page: i.per_page
                            }).then(function(e) {
                                var i;
                                t.hideLoading(), n.initData(), (i = n.dataList[n.current].list).push.apply(i, s(e.data.list)), 
                                n.dataList[n.current].total = e.data.item_total, n.dataList[n.current].init = !0, 
                                t.showToast({
                                    title: "已提交取消请求~",
                                    icon: "none"
                                });
                            });
                        }).catch(function(e) {
                            t.hideLoading();
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    payOrder: function(e) {
                        var n = this;
                        return function(t) {
                            return function() {
                                var e = this, n = arguments;
                                return new Promise(function(i, r) {
                                    var o = t.apply(e, n);
                                    function s(t) {
                                        a(o, i, r, s, c, "next", t);
                                    }
                                    function c(t) {
                                        a(o, i, r, s, c, "throw", t);
                                    }
                                    s(void 0);
                                });
                            };
                        }(i.default.mark(function o() {
                            var a;
                            return i.default.wrap(function(i) {
                                for (;;) switch (i.prev = i.next) {
                                  case 0:
                                    return t.showLoading({
                                        mask: !0
                                    }), i.next = 3, n.$api.emit("order.pay_config", e, {
                                        pay_type: "wechat",
                                        sub_type: "miniapp"
                                    });

                                  case 3:
                                    a = i.sent, t.hideLoading(), r.default.pay({
                                        pay_config: a.data.pay_config,
                                        success: function() {
                                            t.showToast({
                                                title: "支付成功",
                                                icon: "none"
                                            }), n.refresh();
                                        },
                                        faild: function() {
                                            t.showToast({
                                                title: "支付失败",
                                                icon: "none"
                                            });
                                        }
                                    });

                                  case 6:
                                  case "end":
                                    return i.stop();
                                }
                            }, o);
                        }))();
                    },
                    actions: function(e) {
                        var n = this;
                        switch (e.action) {
                          case "立即支付":
                            this.payOrder(e.order.uuid);
                            break;

                          case "取消订单":
                            this.order = e.order, this.visibleChange();
                            break;

                          case "返售":
                            this.currentUuid = e.order.uuid, this.isShowReturnSalePopup = !0;
                            break;

                          case "删除订单":
                            t.showModal({
                                title: "提示",
                                content: "确认要删除订单吗?",
                                success: function(i) {
                                    i.confirm && (t.showLoading({
                                        title: "加载中",
                                        mask: !0
                                    }), n.$api.emit("order.destory", e.order.uuid).then(function(t) {
                                        n.initData(), n.getOrderList();
                                    }));
                                }
                            });
                            break;

                          case "提醒发货":
                            break;

                          case "确认收货":
                            t.showModal({
                                title: "提示",
                                content: "订单确认收货?",
                                success: function(i) {
                                    i.confirm && (t.showLoading({
                                        title: "加载中",
                                        mask: !0
                                    }), n.$api.emit("order.complete", e.order.uuid).then(function(e) {
                                        t.hideLoading(), n.refresh();
                                    }));
                                }
                            });
                            break;

                          case "去评价":
                            t.navigateTo({
                                url: "/pages/writeComment/index?uuid=" + e.order.uuid
                            });
                            break;

                          case "核销码":
                            t.navigateTo({
                                url: "/pages/orderCode/index?uuid=" + e.order.uuid
                            });
                        }
                    },
                    currentChange: function(t) {
                        var e = t.currentTarget.dataset.current;
                        if (e !== this.current) {
                            this.current = e;
                            var n = this.dataList[this.current];
                            console.log(n), n.init || this.getOrderList();
                        }
                    },
                    currentChange2: function(t) {
                        var e = t.detail.current;
                        e !== this.current && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    getOrderList: function() {
                        var t = this.dataList[this.current];
                        t.loading || (t.loading = !0, this.$api.emit("order.list", {
                            status: t.type,
                            page: t.page,
                            per_page: t.per_page
                        }).then(function(e) {
                            var n;
                            t.loading = !1, 1 === e.data.current_page ? t.list = e.data.list : (n = t.list).push.apply(n, s(e.data.list)), 
                            t.total = e.data.item_total, t.init = !0;
                        }));
                    }
                }
            };
            e.default = u;
        }).call(this, n("543d").default);
    }
}, [ [ "cd03", "common/runtime", "common/vendor" ] ] ]);