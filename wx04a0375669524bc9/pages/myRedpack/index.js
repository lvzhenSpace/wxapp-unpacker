(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myRedpack/index" ], {
    "04c8": function(t, n, e) {},
    "067d": function(t, n, e) {
        e.r(n);
        var r = e("64b1"), o = e.n(r);
        for (var a in r) "default" !== a && function(t) {
            e.d(n, t, function() {
                return r[t];
            });
        }(a);
        n.default = o.a;
    },
    "105e": function(t, n, e) {
        var r = e("04c8");
        e.n(r).a;
    },
    2472: function(t, n, e) {
        (function(t) {
            e("f868"), r(e("66fd"));
            var n = r(e("b45a"));
            function r(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, t(n.default);
        }).call(this, e("543d").createPage);
    },
    "64b1": function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var r = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }(e("a34a"));
            function o(t) {
                return function(t) {
                    if (Array.isArray(t)) return a(t);
                }(t) || function(t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
                }(t) || function(t, n) {
                    if (t) {
                        if ("string" == typeof t) return a(t, n);
                        var e = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === e && t.constructor && (e = t.constructor.name), "Map" === e || "Set" === e ? Array.from(t) : "Arguments" === e || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(e) ? a(t, n) : void 0;
                    }
                }(t) || function() {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function a(t, n) {
                (null == n || n > t.length) && (n = t.length);
                for (var e = 0, r = new Array(n); e < n; e++) r[e] = t[e];
                return r;
            }
            function i(t, n, e, r, o, a, i) {
                try {
                    var u = t[a](i), c = u.value;
                } catch (t) {
                    return void e(t);
                }
                u.done ? n(c) : Promise.resolve(c).then(r, o);
            }
            e("f783");
            var u = {
                components: {},
                data: function() {
                    return {
                        list: [],
                        total: 0,
                        page: 1,
                        per_page: 20,
                        init: !1,
                        loading: !1,
                        type: "all",
                        setting: {},
                        assetInfo: {
                            balance: 0,
                            withdraw_pending: 0,
                            withdraw_completed: 0
                        },
                        withdraw: 0
                    };
                },
                computed: {
                    navBar: function() {
                        return this.$store.getters.deviceInfo.customBar;
                    },
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    }
                },
                watch: {
                    type: function() {
                        this.refresh();
                    }
                },
                onLoad: function() {
                    var n = this;
                    return function(t) {
                        return function() {
                            var n = this, e = arguments;
                            return new Promise(function(r, o) {
                                var a = t.apply(n, e);
                                function u(t) {
                                    i(a, r, o, u, c, "next", t);
                                }
                                function c(t) {
                                    i(a, r, o, u, c, "throw", t);
                                }
                                u(void 0);
                            });
                        };
                    }(r.default.mark(function e() {
                        return r.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                t.showLoading({
                                    title: "加载中"
                                }), n.getRedpackRecord().then(function(n) {
                                    t.hideLoading();
                                }), n.getlist(), n.getSetting(), n.$store.dispatch("getUserInfo");

                              case 5:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                onShow: function() {
                    var t = this;
                    this.$http("/assets/redpack").then(function(n) {
                        t.assetInfo = n.data.info;
                    });
                },
                onReachBottom: function() {
                    this.getRedpackRecord();
                },
                methods: {
                    getlist: function() {
                        var t = this;
                        this.$http("/status-total/limit", "GET", {}).then(function(n) {
                            t.withdraw = n.data.withdraw;
                        });
                    },
                    getRedpackRecord: function() {
                        var t = this;
                        if (!this.loading) return this.loading = !0, this.$http("/asset-records/redpack", "GET", {
                            type: this.type,
                            page: this.page,
                            per_page: this.per_page
                        }).then(function(n) {
                            var e;
                            t.page++, (e = t.list).push.apply(e, o(n.data.list)), t.loading = !1, t.init = !0;
                        });
                    },
                    getSetting: function() {
                        var t = this;
                        return this.$http("/setting/redpack", "GET").then(function(n) {
                            t.setting = n.data.setting;
                        });
                    },
                    refresh: function() {
                        this.page = 1, this.list = [], this.getRedpackRecord();
                    }
                }
            };
            n.default = u;
        }).call(this, e("543d").default);
    },
    aa33: function(t, n, e) {
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return a;
        }), e.d(n, "a", function() {
            return r;
        });
        var r = {
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "83c6"));
            }
        }, o = function() {
            var t = this, n = (t.$createElement, t._self._c, t.$tool.formatPrices(t.assetInfo.balance)), e = t.$tool.formatPrices(t.assetInfo.withdraw_pending), r = t.$tool.formatPrices(t.assetInfo.withdraw_completed), o = t.__map(t.list, function(n, e) {
                return {
                    $orig: t.__get_orig(n),
                    g3: 1 === n.type ? t.$tool.formatPrice(n.value) : null
                };
            });
            t._isMounted || (t.e0 = function(n) {
                t.type = "all";
            }, t.e1 = function(n) {
                t.type = "in";
            }, t.e2 = function(n) {
                t.type = "out";
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    g0: n,
                    g1: e,
                    g2: r,
                    l0: o
                }
            });
        }, a = [];
    },
    b45a: function(t, n, e) {
        e.r(n);
        var r = e("aa33"), o = e("067d");
        for (var a in o) "default" !== a && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        e("105e");
        var i = e("f0c5"), u = Object(i.a)(o.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        n.default = u.exports;
    }
}, [ [ "2472", "common/runtime", "common/vendor" ] ] ]);