(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/fudai/detail" ], {
    "18bb": function(t, n, i) {},
    "1acc": function(t, n, i) {
        i.d(n, "b", function() {
            return o;
        }), i.d(n, "c", function() {
            return s;
        }), i.d(n, "a", function() {
            return e;
        });
        var e = {
            PriceDisplay: function() {
                return i.e("components/PriceDisplay/PriceDisplay").then(i.bind(null, "f149"));
            },
            uniPopup: function() {
                return i.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(i.bind(null, "50c2"));
            },
            BoxSkuPopup: function() {
                return i.e("components/BoxSkuPopup/BoxSkuPopup").then(i.bind(null, "16ac"));
            },
            OpenBoxPopupTheme2: function() {
                return i.e("components/OpenBoxPopupTheme2/OpenBoxPopupTheme2").then(i.bind(null, "3444"));
            },
            FreeTicketFloatBtn: function() {
                return i.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(i.bind(null, "6af9"));
            },
            Danmus: function() {
                return i.e("components/Danmus/Danmus").then(i.bind(null, "7d24"));
            }
        }, o = function() {
            var t = this, n = (t.$createElement, t._self._c, t.__map(t.info.sku_level, function(n, i) {
                return {
                    $orig: t.__get_orig(n),
                    g0: n.odds.toFixed(2)
                };
            }));
            t._isMounted || (t.e0 = function(n) {
                t.isShowRankList = !0;
            }, t.e1 = function(n) {
                t.isShowRankList = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l0: n
                }
            });
        }, s = [];
    },
    3576: function(t, n, i) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = i("26cb");
            function o(t, n) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var e = Object.getOwnPropertySymbols(t);
                    n && (e = e.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), i.push.apply(i, e);
                }
                return i;
            }
            function s(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var i = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? o(Object(i), !0).forEach(function(n) {
                        u(t, n, i[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : o(Object(i)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(i, n));
                    });
                }
                return t;
            }
            function u(t, n, i) {
                return n in t ? Object.defineProperty(t, n, {
                    value: i,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[n] = i, t;
            }
            var a = {
                components: {
                    PayCard: function() {
                        Promise.all([ i.e("common/vendor"), i.e("pages/fudai/components/PayCard") ]).then(function() {
                            return resolve(i("1a1f"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    RecordList: function() {
                        i.e("pages/fudai/components/RecordList").then(function() {
                            return resolve(i("cb34"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    imagepop: function() {
                        i.e("pages/fudai/components/imagepop").then(function() {
                            return resolve(i("1bd5"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        bgImgs: [ "url(https://img121.7dun.com/20230207NewImg/wuxianshang/rDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/srDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/ssrDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/urDes.png)", "url()" ],
                        imgList: [ {
                            img: "https://img121.7dun.com/20230207NewImg/wuxianshang/zhongshang.png"
                        }, {
                            img: "https://img121.7dun.com/20230207NewImg/wuxianshang/zhongshang.png"
                        }, {
                            img: "https://img121.7dun.com/20230207NewImg/wuxianshang/zhongshang.png"
                        }, {
                            img: "https://img121.7dun.com/20230207NewImg/wuxianshang/zhongshang.png"
                        }, {
                            img: "https://img121.7dun.com/20230207NewImg/wuxianshang/zhongshang.png"
                        } ],
                        uuid: "",
                        isInit: !1,
                        isPayPopup: !1,
                        payTotal: 1,
                        info: {
                            min_lucky_score: ""
                        },
                        order: {},
                        pageUuid: "",
                        isNotFound: !1,
                        isShowResultPopup: !1,
                        danmuSetting: {},
                        danmuList: [],
                        config: {},
                        detailImageList: [],
                        levelFilter: 0,
                        isTryMode: !1,
                        isShowRankList: !1,
                        wining_list: [],
                        isimagepop: !1,
                        is_doubleBoxCard: 0,
                        skuinfo: {},
                        skusList: [],
                        skus_ultimate: {},
                        color_list: [ {
                            level: 0,
                            color: "#00aa00"
                        }, {
                            level: 1,
                            color: "#aa55ff"
                        }, {
                            level: 2,
                            color: "#ffff00"
                        }, {
                            level: 3,
                            color: "#ffff00"
                        }, {
                            level: 4,
                            color: "#ff0000"
                        } ]
                    };
                },
                computed: s(s({}, (0, e.mapGetters)([ "userInfo" ])), {}, {
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/fudai/detail_bg.png";
                    },
                    boxImage: function() {
                        return this.info.image_3d || "https://cdn2.hquesoft.com/box/fudai/box.png";
                    },
                    skuLevel: function() {
                        return this.info.sku_level || [];
                    },
                    skus: function() {
                        var t = this;
                        return this.levelFilter ? this.info.skus.filter(function(n) {
                            return n.level === t.levelFilter;
                        }) : this.info.skus || [];
                    },
                    payInfo: function() {
                        return {
                            page_uuid: this.pageUuid,
                            title: this.info.title,
                            pay_total: this.payTotal,
                            total_list: this.info.total_list,
                            money_price: this.info.money_price,
                            score_price: this.info.score_price
                        };
                    },
                    titleBgColor: function() {
                        return " ";
                    },
                    titleColor: function() {
                        return "#ffffff";
                    },
                    setTitleText: function() {
                        t.setNavigationBarTitle({
                            title: this.info.title
                        });
                    },
                    discountTips: function() {
                        for (var t = this.info.total_list || [], n = t.length - 1; n >= 0; n--) if (t[n].is_discount) {
                            var i = t[n].total + "连开优惠";
                            return t[n].money_discount && (i += t[n].money_discount / 100 + "元"), t[n].score_discount && (i += t[n].score_discount + this.scoreAlias), 
                            i;
                        }
                        return !1;
                    },
                    width_value: function() {
                        return this.userInfo.lucky_score / this.info.min_lucky_score * 100 > 100 ? 100 : this.userInfo.lucky_score / this.info.min_lucky_score * 100 < 0 ? 0 : Math.trunc(this.userInfo.lucky_score / this.info.min_lucky_score * 100);
                    }
                }),
                watch: {},
                filters: {},
                onLoad: function(n) {
                    this.uuid = n.uuid, t.loadFontFace({
                        family: "BigjianNew",
                        source: 'url("'.concat("https://img121.7dun.com/member_grade/BigjianNew.TTF", '")'),
                        success: function() {},
                        fail: function(t) {}
                    });
                },
                onUnload: function() {},
                created: function() {},
                onPullDownRefresh: function() {
                    this.$showPullRefresh(), this.initData(), this.setTitleText();
                },
                onShow: function() {
                    var n = this;
                    t.showLoading({
                        title: "加载中"
                    }), this.initData().then(function(i) {
                        n.isInit = !0, t.hideLoading(), n.$http("/fudai/change", "POST", {
                            fudai_id: n.info.id
                        }).then(function(t) {
                            n.pageUuid = t.data.page_uuid, n.fetchList();
                        });
                    }).catch(function(t) {
                        n.isNotFound = !0;
                    });
                },
                onReachBottom: function() {},
                methods: {
                    handleTry: function() {
                        t.showLoading({
                            title: "刷新中..."
                        }), this.initData().then(function(n) {
                            t.hideLoading(), t.showToast({
                                title: "刷新完成~",
                                icon: "none"
                            });
                        });
                    },
                    fetchList: function() {
                        var t = this;
                        return !this.isLoading && (this.isLoading = !0, this.$http("/fudais/".concat(this.info.uuid, "/records"), "GET", {
                            page: 1,
                            per_page: 100
                        }).then(function(n) {
                            t.isInit = !0;
                            for (var i = 0; i < n.data.list.length; i++) t.wining_list.length < 12 && 5 == n.data.list[i].level && t.wining_list.push(n.data.list[i]);
                        }).catch(function(n) {
                            t.isInit = !1;
                        }));
                    },
                    setLevelFilter: function(t) {
                        this.levelFilter === t ? this.levelFilter = 0 : this.levelFilter = t;
                    },
                    addClick: function() {
                        t.vibrateShort({}), 1 === this.payTotal ? this.payTotal = this.payTotal + 2 : 3 === this.payTotal ? this.payTotal = this.payTotal + 7 : t.showToast({
                            title: "最多十抽哦~",
                            icon: "none"
                        });
                    },
                    minusClick: function() {
                        t.vibrateShort({}), 3 === this.payTotal ? this.payTotal = this.payTotal - 2 : 10 === this.payTotal ? this.payTotal = this.payTotal - 7 : 20 === this.payTotal ? this.payTotal = this.payTotal - 10 : t.showToast({
                            title: "最少一抽哦~",
                            icon: "none"
                        });
                    },
                    gomybox: function() {
                        t.reLaunch({
                            url: "/pages/myBox/index"
                        });
                    },
                    useFreeTicket: function() {
                        var n = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/fudai/order/confirm", "POST", {
                            page_uuid: this.pageUuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(i) {
                            t.hideLoading();
                            var e = i.data;
                            e.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : n.paySuccess(e.order);
                        });
                    },
                    getDanmu: function() {
                        var t = this;
                        this.$http("/danmus/fudai_detail?node_id=".concat(this.info.id)).then(function(n) {
                            t.danmuSetting = n.data.setting, t.danmuList = n.data.list;
                        });
                    },
                    showDetailImagePopup: function(t, n) {
                        t.odds = n, this.skuinfo = t, this.isimagepop = !0;
                    },
                    hideDetailImagePopup: function() {
                        this.$refs.detailPopup.close();
                    },
                    isimagepopfun: function() {
                        this.isimagepop = !1;
                    },
                    refresh: function() {
                        this.$store.dispatch("getUserInfo");
                    },
                    initData: function() {
                        var n = this;
                        return this.$http("/fudais/".concat(this.uuid), "GET", {}).then(function(i) {
                            t.hideLoading(), n.info = i.data.info, n.config = i.data.config, n.info.sku_level = n.info.sku_level.sort().reverse(), 
                            n.skusList = i.data.info.skus.filter(function(t) {
                                return 5 === t.level;
                            });
                            var e = 1;
                            n.skus_ultimate = n.skusList[0], setInterval(function() {
                                n.skus_ultimate = n.skusList[e], ++e >= n.skusList.length && (e = 0);
                            }, 3e3), n.getDanmu(), t.setNavigationBarTitle({
                                title: n.info.title
                            });
                        });
                    },
                    paySuccess: function(t) {
                        var n = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], i = arguments.length > 2 ? arguments[2] : void 0;
                        this.isTryMode = n, this.order = t, this.is_doubleBoxCard = i, this.isPayPopup = !1, 
                        this.isShowResultPopup = !0, this.refresh();
                    },
                    goBack: function() {
                        this.isShowResultPopup = !1;
                    },
                    hidePayPopup: function() {
                        this.isPayPopup = !1;
                    },
                    pay: function(t) {
                        this.payTotal = t, this.isPayPopup = !0;
                    },
                    getLevelIcon: function(t) {
                        return this.skuLevel.find(function(n) {
                            return n.level === t;
                        }).icon;
                    },
                    getLevelTitle: function(t) {
                        return this.skuLevel.find(function(n) {
                            return n.level === t;
                        }).title;
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = a;
        }).call(this, i("543d").default);
    },
    "3b3c": function(t, n, i) {
        var e = i("18bb");
        i.n(e).a;
    },
    "49d8": function(t, n, i) {
        i.r(n);
        var e = i("3576"), o = i.n(e);
        for (var s in e) "default" !== s && function(t) {
            i.d(n, t, function() {
                return e[t];
            });
        }(s);
        n.default = o.a;
    },
    5013: function(t, n, i) {
        (function(t) {
            i("f868"), e(i("66fd"));
            var n = e(i("f6e3"));
            function e(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = i, t(n.default);
        }).call(this, i("543d").createPage);
    },
    f6e3: function(t, n, i) {
        i.r(n);
        var e = i("1acc"), o = i("49d8");
        for (var s in o) "default" !== s && function(t) {
            i.d(n, t, function() {
                return o[t];
            });
        }(s);
        i("3b3c");
        var u = i("f0c5"), a = Object(u.a)(o.default, e.b, e.c, !1, null, "f2ba7b64", null, !1, e.a, void 0);
        n.default = a.exports;
    }
}, [ [ "5013", "common/runtime", "common/vendor" ] ] ]);