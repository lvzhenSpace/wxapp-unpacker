(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/fudai/components/PayCard" ], {
    "1a1f": function(t, o, e) {
        e.r(o);
        var n = e("7751"), i = e("e4ea");
        for (var s in i) "default" !== s && function(t) {
            e.d(o, t, function() {
                return i[t];
            });
        }(s);
        e("ae47");
        var u = e("f0c5"), r = Object(u.a)(i.default, n.b, n.c, !1, null, "6d5b6d43", null, !1, n.a, void 0);
        o.default = r.exports;
    },
    7751: function(t, o, e) {
        e.d(o, "b", function() {
            return i;
        }), e.d(o, "c", function() {
            return s;
        }), e.d(o, "a", function() {
            return n;
        });
        var n = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            },
            UsableCouponPopup: function() {
                return e.e("components/UsableCouponPopup/UsableCouponPopup").then(e.bind(null, "9d28"));
            }
        }, i = function() {
            var t = this, o = (t.$createElement, t._self._c, t.order.coupon_discount ? t.$tool.formatPrice(t.order.coupon_discount) : null), e = t.order.redpack ? t.$tool.formatPrice(t.order.redpack) : null, n = t.order.max_useable_score && t.form.is_use_score ? t._f("priceToFixed")(t.order.score_discount) : null;
            t.$mp.data = Object.assign({}, {
                $root: {
                    g0: o,
                    g1: e,
                    f0: n
                }
            });
        }, s = [];
    },
    ae47: function(t, o, e) {
        var n = e("d550");
        e.n(n).a;
    },
    bef0: function(t, o, e) {
        (function(t) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var n = e("26cb"), i = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }(e("ae4c"));
            function s(t, o) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    o && (n = n.filter(function(o) {
                        return Object.getOwnPropertyDescriptor(t, o).enumerable;
                    })), e.push.apply(e, n);
                }
                return e;
            }
            function u(t) {
                for (var o = 1; o < arguments.length; o++) {
                    var e = null != arguments[o] ? arguments[o] : {};
                    o % 2 ? s(Object(e), !0).forEach(function(o) {
                        r(t, o, e[o]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : s(Object(e)).forEach(function(o) {
                        Object.defineProperty(t, o, Object.getOwnPropertyDescriptor(e, o));
                    });
                }
                return t;
            }
            function r(t, o, e) {
                return o in t ? Object.defineProperty(t, o, {
                    value: e,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[o] = e, t;
            }
            var a = {
                components: {},
                data: function() {
                    return {
                        payTotal: -1,
                        order: {},
                        price: 0,
                        form: {
                            is_use_redpack: 1,
                            is_use_score: 1,
                            is_doubleBoxCard: 0
                        },
                        total_prices: 0,
                        total_score_price: 0,
                        currentCoupon: {},
                        isCouponPopup: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        usableCouponsList: [],
                        unusableCouponsList: [],
                        isInit: !1,
                        isLoading: !1,
                        isSubmiting: !1,
                        isCheckUserStatement: !1,
                        doubleBoxCard: {},
                        count: 10,
                        buzaiBool: !1,
                        pingtaiBool: !1,
                        fahuoBoll: !1,
                        fahuoCurrent: 0,
                        fahuoList: [ "发货须知", "售后须知" ]
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: u(u({}, (0, n.mapGetters)([ "userInfo" ])), {}, {
                    isuserInfo: function() {}
                }),
                watch: {
                    payTotal: function() {
                        this.initOrder();
                    }
                },
                created: function() {
                    this.payTotal = this.info.pay_total;
                    var o = this;
                    t.getStorage({
                        key: "fudai_scoreStorage",
                        success: function(t) {
                            o.form.is_use_score = t.data ? 1 : 0, setTimeout(function() {
                                o.initOrder();
                            }, 100);
                        }
                    }), t.getStorage({
                        key: "isCheckUserStatementBool",
                        success: function(t) {
                            o.isCheckUserStatement = t.data;
                        }
                    }), t.getStorage({
                        key: "isBuzaiBool",
                        success: function(t) {
                            o.buzaiBool = t.data;
                        }
                    });
                },
                methods: {
                    handleFaHuoClick: function() {
                        this.fahuoBoll = !0;
                    },
                    handleHuoClose: function() {
                        this.fahuoBoll = !1;
                    },
                    handleHuoClick: function(t) {
                        t !== this.fahuoCurrent && (this.fahuoCurrent = t);
                    },
                    uncheck: function() {
                        this.isCheckUserStatement = !this.isCheckUserStatement;
                    },
                    handleDesClick: function() {
                        this.buzaiBool = !this.buzaiBool, t.setStorage({
                            key: "isBuzaiBool",
                            data: !0,
                            success: function() {}
                        });
                    },
                    handleTuiClick: function() {
                        this.pingtaiBool = !1;
                    },
                    load: function() {
                        this.count += 10, this.showPopup();
                    },
                    showPopup: function() {
                        for (var t = [], o = [], e = 0; e < this.count; e++) void 0 !== this.usableCoupons[e] && t.push(this.usableCoupons[e]), 
                        void 0 !== this.unusableCoupons[e] && o.push(this.unusableCoupons[e]);
                        this.usableCouponsList = t.filter(function(t, o, e) {
                            return e.findIndex(function(o) {
                                return o.id === t.id;
                            }) === o;
                        }), this.unusableCouponsList = o.filter(function(t, o, e) {
                            return e.findIndex(function(o) {
                                return o.id === t.id;
                            }) === o;
                        }), this.isCouponPopup = !0;
                    },
                    hidePopup: function() {
                        this.count = 10, this.usableCouponsList = [], this.unusableCouponsList = [], this.isCouponPopup = !1;
                    },
                    couponChange: function(t) {
                        t.id === this.currentCoupon.id || (this.currentCoupon = t, this.initOrder());
                    },
                    initOrder: function() {
                        var o = this;
                        t.showLoading(), this.$http("/fudai/order/preview", "POST", u({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(e) {
                            o.isInit = !0, o.order = e.data.order, o.unusableCoupons = e.data.order.coupons.unusable, 
                            o.usableCoupons = e.data.order.coupons.usable, t.hideLoading();
                        }).catch(function(t) {
                            o.isInit = !1, o.cancel();
                        }), this.$http("/user/cards").then(function(t) {
                            o.doubleBoxCard = t.data.double_box_card;
                        });
                    },
                    switchChange: function(t) {
                        null == t.detail.value[0] ? this.form.is_use_redpack = 0 : this.form.is_use_redpack = 1, 
                        this.initOrder();
                    },
                    scoreSwitchChange: function(o) {
                        this.form.is_use_score = !(o.detail.value.length > 1), null == o.detail.value[0] ? (this.form.is_use_score = 0, 
                        t.setStorage({
                            key: "fudai_scoreStorage",
                            data: !1,
                            success: function() {}
                        })) : (this.form.is_use_score = 1, t.setStorage({
                            key: "fudai_scoreStorage",
                            data: !0,
                            success: function() {}
                        })), this.initOrder();
                    },
                    doubleBoxCardswitchChange: function(t) {
                        1 === this.form.is_doubleBoxCard ? this.form.is_doubleBoxCard = 0 : this.form.is_doubleBoxCard = 1, 
                        this.initOrder();
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    createOrder: function() {
                        var o = this;
                        if (this.isLoading) return !1;
                        this.isLoading = !0, t.showLoading({
                            title: "提交中"
                        }), this.$http("/fudai/order/confirm", "POST", u({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(e) {
                            t.hideLoading(), o.isSubmiting = !1;
                            var n = e.data;
                            n.is_need_pay ? i.default.pay(u(u({}, n), {}, {
                                success: function() {
                                    o.$emit("success", n.order, !1, n.is_doubleBoxCard);
                                },
                                fail: function() {
                                    t.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), o.isLoading = !1, o.$http("/orders/".concat(n.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : o.$emit("success", n.order, !1, n.is_doubleBoxCard);
                        }).catch(function(t) {
                            o.isLoading = !1;
                        });
                    },
                    handleBtnClick: function() {
                        if (!this.isCheckUserStatement) return t.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1;
                    },
                    handleJixuClick: function() {
                        if (this.pingtaiBool = !1, this.isLoading) return !1;
                        this.createOrder();
                    },
                    submit: function() {
                        if (t.setStorage({
                            key: "isCheckUserStatementBool",
                            data: !0,
                            success: function() {}
                        }), 0 == this.buzaiBool) this.pingtaiBool = !0; else {
                            if (this.pingtaiBool = !1, this.isLoading) return !1;
                            this.createOrder();
                        }
                    }
                },
                onPageScroll: function(t) {}
            };
            o.default = a;
        }).call(this, e("543d").default);
    },
    d550: function(t, o, e) {},
    e4ea: function(t, o, e) {
        e.r(o);
        var n = e("bef0"), i = e.n(n);
        for (var s in n) "default" !== s && function(t) {
            e.d(o, t, function() {
                return n[t];
            });
        }(s);
        o.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/fudai/components/PayCard-create-component", {
    "pages/fudai/components/PayCard-create-component": function(t, o, e) {
        e("543d").createComponent(e("1a1f"));
    }
}, [ [ "pages/fudai/components/PayCard-create-component" ] ] ]);