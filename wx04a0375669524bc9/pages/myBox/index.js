(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/index" ], {
    "03f9": function(t, e, n) {
        n.r(e);
        var i = n("0adc"), s = n.n(i);
        for (var o in i) "default" !== o && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(o);
        e.default = s.a;
    },
    "0adc": function(t, e, n) {
        (function(t) {
            function i(t) {
                return function(t) {
                    if (Array.isArray(t)) return s(t);
                }(t) || function(t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
                }(t) || function(t, e) {
                    if (t) {
                        if ("string" == typeof t) return s(t, e);
                        var n = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? s(t, e) : void 0;
                    }
                }(t) || function() {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function s(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i;
            }
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = {
                components: {
                    PackageSku: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/myBox/components/PackageSku") ]).then(function() {
                            return resolve(n("94dc"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    PayCard: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/myBox/components/PayCard") ]).then(function() {
                            return resolve(n("c366"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        popoverBool: !1,
                        popover: {
                            thumb: null,
                            title: null
                        },
                        guashouBool: !0,
                        order: {},
                        showSelect: !0,
                        reasons: [],
                        visible: !1,
                        dataList: [],
                        current: 0,
                        currentUuid: "6056e83ef3251",
                        currentItem: {},
                        types: [ "pending", "resale_pending", "picked", "all" ],
                        typeTextList: [ "待处理", "提货中", "已提货", "全部" ],
                        boxList: [ "赏品", "宝箱" ],
                        boxIndex: 0,
                        boxShow: !1,
                        allBox: [],
                        arrBox: [ "暂无待处理的商品", "暂无提货中的商品", "暂无已提货的商品", "暂无商品" ],
                        isSelectMode: !1,
                        selectType: "deliver",
                        selectedIds: [],
                        isShowPay: !1,
                        isShowReturnSalePopup: !1,
                        isShowResalePopup: !1,
                        statusTotal: {},
                        isReturnSaleSuccess: !1,
                        boxTitle: !0,
                        popup: !1,
                        boxskus: [],
                        myInfo: {}
                    };
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order || {};
                    },
                    marketConfig: function() {
                        return this.$store.getters.setting.market || {};
                    },
                    exchangeConfig: function() {
                        return this.$store.getters.setting.exchange || {};
                    }
                },
                onLoad: function(e) {
                    t.showLoading({
                        title: "加载中"
                    }), this.getBoxList(), this.current = this.types.indexOf(e.status || "pending");
                },
                onShow: function() {
                    var e = this;
                    this.$http("/task/level_list?no_list=1").then(function(t) {
                        e.myInfo = t.data.my_task_level_info, t.data.my_task_level_info.level > 4 ? (e.types = [ "pending", "resale_pending", "picked", "all" ], 
                        e.typeTextList = [ "待处理", "提货中", "已提货", "全部" ]) : (e.types = [ "pending", "picked", "all" ], 
                        e.typeTextList = [ "待处理", "已提货", "全部" ]), e.initData();
                    }).catch(function(t) {
                        console.log(t);
                    });
                    var n = this;
                    t.getStorage({
                        key: "guashouStorage",
                        success: function(t) {
                            n.guashouBool = t.data;
                        },
                        fail: function(t) {}
                    });
                },
                onUnload: function() {},
                methods: {
                    handlePayCardCancel: function() {
                        this.isShowPay = !1;
                    },
                    handleReturnCancel: function() {
                        this.isShowReturnSalePopup = !1;
                    },
                    handleIKnowClick: function() {
                        this.popoverBool = !1;
                    },
                    getMyInfo: function() {
                        var t = this;
                        this.$http("/task/level_list?no_list=1").then(function(e) {
                            t.myInfo = e.data.my_task_level_info, e.data.my_task_level_info.level > 10 ? (t.types = [ "pending", "resale_pending", "picked", "all" ], 
                            t.typeTextList = [ "待处理", "提货中", "已提货", "全部" ]) : (t.types = [ "pending", "picked", "all" ], 
                            t.typeTextList = [ "待处理", "已提货", "全部" ]);
                        }).catch(function(t) {
                            console.log(t);
                        });
                    },
                    handleGuaClose: function() {
                        this.guashouBool = !1;
                    },
                    handleGuaShouClick: function() {
                        t.setStorage({
                            key: "guashouStorage",
                            data: !1,
                            success: function() {
                                console.log("success");
                            }
                        }), t.navigateTo({
                            url: "/pages/guashou/index"
                        });
                    },
                    showaward: function(t) {
                        this.popup = !0, this.boxskus = this.allBox[t].skus;
                    },
                    close: function() {
                        this.popup = !1;
                    },
                    getBoxList: function() {
                        var t = this;
                        this.$http("/shangdai/records?status=0").then(function(e) {
                            t.allBox = e.data.list;
                        });
                    },
                    boxchange: function(t) {
                        this.refresh(), this.boxIndex = t, 1 == t ? (this.boxShow = !0, this.showSelect = !1) : (this.boxShow = !1, 
                        this.showSelect = !0);
                    },
                    openBox: function(e) {
                        var n = this;
                        this.$http("/shangdai/open/".concat(e), "POST").then(function(t) {
                            setTimeout(function() {
                                n.$playAudio("open"), setTimeout(function() {
                                    n.popoverBool = !0, n.popover = t.data.shangdai_sku;
                                }, 500);
                            }, 400), n.getBoxList();
                        }).catch(function(e) {
                            t.showToast({
                                title: "开启失败",
                                icon: "none",
                                duration: 2e3
                            });
                        });
                    },
                    hideResalePopup: function() {
                        this.isShowResalePopup = !1;
                    },
                    toPage: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    },
                    successDeliver: function(e) {
                        this.cancelSelect(), "custom_gateway" === e.deliver_type ? (this.refresh(), this.isShowPay = !1, 
                        t.showToast({
                            title: "提交成功",
                            icon: "none"
                        })) : (this.current = 2, this.refresh(), this.isShowPay = !1, t.showModal({
                            title: "发货成功",
                            content: "已成功提交发货请求，请注意查收快递哦~"
                        }));
                    },
                    selectAll: function() {
                        if (!this.dataList[0]) return !1;
                        this.selectedIds = [];
                        for (var t = 0; t < this.dataList[0].list.length; t++) {
                            var e = this.dataList[0].list[t];
                            "deliver" === this.selectType ? "virtual_asset" === e.sku_type_text || e.is_presell || this.selectedIds.push(e.id) : "return_sale" === this.selectType ? !this.orderConfig.is_ban_return_sale && "virtual_asset" != e.sku_type_text && e.is_return_saleable && this.selectedIds.push(e.id) : "resale" === this.selectType && "virtual_asset" != e.sku_type_text && this.selectedIds.push(e.id);
                        }
                    },
                    cancelSelect: function() {
                        this.isSelectMode = !1, this.selectedIds = [];
                    },
                    selectOrSubmit: function() {
                        if (this.isSelectMode) {
                            if (0 == this.selectedIds.length) return t.showModal({
                                title: "请选择物品",
                                content: "选择一件或多件物品后才能提交发货哦~"
                            }), !1;
                            this.isShowPay = !0;
                        } else this.isSelectMode = !0;
                    },
                    batchReturnSale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能分解哦~"
                        }), !1;
                        t.showModal({
                            title: "确认分解",
                            content: "确认要批量分解吗?",
                            success: function(n) {
                                n.confirm && (e.isReturnSaleSuccess = !0, t.showLoading({
                                    title: "分解中..."
                                }), e.$http("/asset/return-sale/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(n) {
                                    t.showToast({
                                        title: "分解成功"
                                    }), e.refresh(), e.isSelectMode = !1, e.isReturnSaleSuccess = !1;
                                }));
                            }
                        });
                    },
                    batchResale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能挂售哦~"
                        }), !1;
                        t.showModal({
                            title: "确认挂售到交易市场",
                            content: "统一以建议挂售价挂售到交易市场",
                            success: function(n) {
                                n.confirm && (t.showLoading({
                                    title: "挂售中..."
                                }), e.$http("/market/resale/batch/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(n) {
                                    t.hideLoading(), e.refresh(), e.isSelectMode = !1, t.showToast({
                                        title: "转售成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.navigateTo({
                                            url: "/pages/resale/index"
                                        });
                                    }, 1500);
                                }));
                            }
                        });
                    },
                    batchExchange: function() {
                        return 0 == this.selectedIds.length ? (t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能置换哦~"
                        }), !1) : this.selectedIds.length >= 10 ? (t.showModal({
                            title: "超出限制了",
                            content: "单次置换限制在10件以内~"
                        }), !1) : (this.setStorage("exchange_package_sku_ids", this.selectedIds), void t.navigateTo({
                            url: "/pages/exchange/productList"
                        }));
                    },
                    enterSelectMode: function(t) {
                        this.selectedIds = [], this.isSelectMode = !0, this.selectType = t;
                    },
                    checkItem: function(t) {
                        var e = this.selectedIds.indexOf(t.id);
                        e > -1 ? this.selectedIds.splice(e, 1) : this.selectedIds.push(t.id);
                    },
                    refresh: function() {
                        this.cleanData(), this.getOrderList(), this.getBoxList(), this.initBoxTotalData();
                    },
                    scrolltolower: function() {
                        this.dataList[this.current].page++, this.getOrderList(), this.getBoxList();
                    },
                    initData: function() {
                        var t = [];
                        this.types.forEach(function(e) {
                            t.push({
                                list: [],
                                type: e,
                                page: 1,
                                per_page: 50,
                                total: 0,
                                init: !1,
                                loading: !1
                            });
                        }), this.dataList = t, this.refresh();
                    },
                    cleanData: function() {
                        this.dataList.forEach(function(t) {
                            t.page = 1, t.init = !1;
                        });
                    },
                    initBoxTotalData: function() {
                        var e = this;
                        this.$http("/status-total/package-sku").then(function(n) {
                            e.statusTotal = n.data.info, t.hideLoading();
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    actions: function(e) {
                        switch (e.action) {
                          case "返售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowReturnSalePopup = !0;
                            break;

                          case "核销码":
                            t.navigateTo({
                                url: "/pages/orderCode/index?uuid=" + e.order.uuid
                            });
                            break;

                          case "查看订单":
                            t.navigateTo({
                                url: "/pages/orderDetail/index?uuid=" + e.order.pick_order.uuid
                            });
                            break;

                          case "转售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowResalePopup = !0;
                        }
                    },
                    currentChange: function(t) {
                        var e = t.currentTarget.dataset.current;
                        (this.myInfo.level > 4 ? 0 == e || 3 == e ? (this.boxTitle = !0, this.boxIndex = 0, 
                        this.boxShow = !1, this.showSelect = !0) : (this.boxTitle = !1, this.boxShow = !1) : 0 == e || 2 == e ? (this.boxTitle = !0, 
                        this.boxIndex = 0, this.boxShow = !1, this.showSelect = !0) : (this.boxTitle = !1, 
                        this.boxShow = !1), e !== this.current) && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    currentChange2: function(t) {
                        var e = t.detail.current;
                        (this.myInfo.level > 4 ? 0 == e || 3 == e ? (this.boxTitle = !0, this.boxIndex = 0, 
                        this.showSelect = !0) : (this.boxIndex = 0, this.boxTitle = !1, this.boxShow = !1) : 0 == e || 2 == e ? (this.boxTitle = !0, 
                        this.boxIndex = 0, this.showSelect = !0) : (this.boxIndex = 0, this.boxTitle = !1, 
                        this.boxShow = !1), e !== this.current) && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    getOrderList: function() {
                        var t = this.dataList[this.current];
                        t.loading || (t.loading = !0, this.$http("/asset/package-skus", "GET", {
                            status: t.type,
                            page: t.page,
                            per_page: t.per_page
                        }).then(function(e) {
                            var n;
                            t.loading = !1, 1 === e.data.current_page ? t.list = e.data.list : (n = t.list).push.apply(n, i(e.data.list)), 
                            t.total = e.data.item_total, t.init = !0;
                        }));
                    }
                }
            };
            e.default = o;
        }).call(this, n("543d").default);
    },
    "62a6": function(t, e, n) {
        n.d(e, "b", function() {
            return s;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            },
            ReturnSalePopup: function() {
                return n.e("components/ReturnSalePopup/ReturnSalePopup").then(n.bind(null, "000e"));
            },
            ResalePopup: function() {
                return n.e("components/ResalePopup/ResalePopup").then(n.bind(null, "b08f"));
            }
        }, s = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__map(t.types, function(e, n) {
                return {
                    $orig: t.__get_orig(e),
                    l0: t.boxShow ? null : t.__map(t.dataList[n].list, function(e, n) {
                        return {
                            $orig: t.__get_orig(e),
                            g0: t.selectedIds.indexOf(e.id)
                        };
                    })
                };
            }));
            t._isMounted || (t.e0 = function(e) {
                t.isReturnSaleSuccess = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l1: e
                }
            });
        }, o = [];
    },
    7396: function(t, e, n) {
        (function(t) {
            n("f868"), i(n("66fd"));
            var e = i(n("835f"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    },
    "835f": function(t, e, n) {
        n.r(e);
        var i = n("62a6"), s = n("03f9");
        for (var o in s) "default" !== o && function(t) {
            n.d(e, t, function() {
                return s[t];
            });
        }(o);
        n("c323");
        var a = n("f0c5"), r = Object(a.a)(s.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = r.exports;
    },
    c323: function(t, e, n) {
        var i = n("fcbd");
        n.n(i).a;
    },
    fcbd: function(t, e, n) {}
}, [ [ "7396", "common/runtime", "common/vendor" ] ] ]);