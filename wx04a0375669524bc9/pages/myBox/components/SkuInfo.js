(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/components/SkuInfo" ], {
    "2dbf": function(t, n, o) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                mixins: [ function(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }(o("fdc7")).default ],
                props: {
                    info: {
                        type: Object
                    },
                    myInfo: {
                        type: Object
                    },
                    disableClick: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                filters: {},
                computed: {
                    boxDepotConfig: function() {
                        return this.$store.getters.setting.box_depot || {};
                    }
                },
                methods: {
                    getMyInfo: function() {
                        var t = this;
                        this.$http("/task/level_list?no_list=1").then(function(n) {
                            t.myInfo = n.data.my_task_level_info;
                        }).catch(function(t) {
                            console.log(t);
                        });
                    },
                    toProductDetail: function(n) {
                        if (this.disableClick) return !1;
                        var o = this.info;
                        "product" === o.product_type ? t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + o.product_uuid
                        }) : "box" === o.product_type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + o.product_uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, o("543d").default);
    },
    "7d9e": function(t, n, o) {
        var e = o("c852");
        o.n(e).a;
    },
    b250: function(t, n, o) {
        o.r(n);
        var e = o("b96c"), a = o("ba4a");
        for (var u in a) "default" !== u && function(t) {
            o.d(n, t, function() {
                return a[t];
            });
        }(u);
        o("7d9e");
        var c = o("f0c5"), i = Object(c.a)(a.default, e.b, e.c, !1, null, "03ad5924", null, !1, e.a, void 0);
        n.default = i.exports;
    },
    b96c: function(t, n, o) {
        o.d(n, "b", function() {
            return e;
        }), o.d(n, "c", function() {
            return a;
        }), o.d(n, "a", function() {});
        var e = function() {
            var t = this, n = (t.$createElement, t._self._c, t.info.attrs && t.info.attrs.length ? t._f("productAttrsToString")(t.info.attrs) : null);
            t.$mp.data = Object.assign({}, {
                $root: {
                    f0: n
                }
            });
        }, a = [];
    },
    ba4a: function(t, n, o) {
        o.r(n);
        var e = o("2dbf"), a = o.n(e);
        for (var u in e) "default" !== u && function(t) {
            o.d(n, t, function() {
                return e[t];
            });
        }(u);
        n.default = a.a;
    },
    c852: function(t, n, o) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/myBox/components/SkuInfo-create-component", {
    "pages/myBox/components/SkuInfo-create-component": function(t, n, o) {
        o("543d").createComponent(o("b250"));
    }
}, [ [ "pages/myBox/components/SkuInfo-create-component" ] ] ]);