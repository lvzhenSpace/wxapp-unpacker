(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/components/PackageSku" ], {
    5174: function(e, t, n) {
        var i = n("d7be");
        n.n(i).a;
    },
    "6f06": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = {
                mixins: [ function(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }(n("fdc7")).default ],
                props: {
                    order: {
                        type: Object
                    },
                    isSelected: {
                        type: Boolean
                    },
                    isSelectMode: {
                        type: Boolean
                    },
                    selectType: {
                        type: String
                    },
                    myInfo: {
                        type: Object
                    }
                },
                components: {
                    SkuInfo: function() {
                        n.e("pages/myBox/components/SkuInfo").then(function() {
                            return resolve(n("b250"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        hours: "",
                        minutes: "",
                        seconds: "",
                        closeTimeVisible: !1,
                        timer: null
                    };
                },
                computed: {
                    info: function() {
                        return this.order;
                    },
                    orderConfig: function() {
                        return this.$store.getters.setting.order || {};
                    },
                    marketConfig: function() {
                        return this.$store.getters.setting.market || {};
                    },
                    isSelectable: function() {
                        return "pending" === this.info.union_status && ("deliver" === this.selectType ? "virtual_asset" !== this.info.sku_type_text && !this.info.is_presell : "return_sale" !== this.selectType || this.isReturnSaleable);
                    },
                    isVirtualAsset: function() {
                        return "virtual_asset" === this.info.sku_type_text;
                    },
                    isReturnSaleable: function() {
                        return this.orderConfig.is_return_sale_enable && !this.isVirtualAsset && this.info.is_return_saleable;
                    },
                    isResaleable: function() {
                        return !this.isVirtualAsset && this.info.is_resaleable;
                    }
                },
                filters: {
                    dateformat: function(e) {
                        return this.$tool.formatDate(e, "MM-dd hh:mm");
                    }
                },
                destroyed: function() {},
                methods: {
                    handleLock: function() {
                        var t = this;
                        e.showLoading({
                            title: "转入中~",
                            icon: "none"
                        }), this.$http("/package-skus/".concat(this.info.uuid, "/lock"), "POST").then(function(n) {
                            e.showToast({
                                title: "已转入保险箱~",
                                icon: "none"
                            }), t.$emit("refresh");
                        });
                    },
                    handleUnlock: function() {
                        var t = this;
                        e.showLoading({
                            title: "解锁中~",
                            icon: "none"
                        }), this.$http("/package-skus/".concat(this.info.uuid, "/unlock"), "POST").then(function(n) {
                            e.showToast({
                                title: "已解锁~",
                                icon: "none"
                            }), t.$emit("refresh");
                        });
                    },
                    handleVirtualAssetPick: function() {
                        var t = this;
                        e.showModal({
                            title: "确认领取此奖品吗?",
                            success: function(n) {
                                n.confirm && t.$http("/package-skus/".concat(t.info.uuid, "/virtual-asset/pick"), "POST").then(function(n) {
                                    e.showToast({
                                        title: "领取成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function() {
                                        e.navigateTo({
                                            url: "/pages/myBox/detail?uuid=" + t.order.uuid
                                        });
                                    }, 1500);
                                });
                            }
                        });
                    },
                    checkResaleDetail: function() {
                        e.navigateTo({
                            url: "/pages/resale/detail?uuid=" + this.order.resale.uuid
                        });
                    },
                    fillNumber: function(e) {
                        return e < 10 ? "0" + e : e;
                    },
                    handleClick: function() {
                        if (this.isSelectMode) return "virtual_asset" !== this.order.sku_type_text && !!this.isSelectable && (this.$emit("check"), 
                        !1);
                        e.navigateTo({
                            url: "/pages/myBox/detail?uuid=" + this.order.uuid
                        });
                    },
                    handleClick2: function(e) {
                        this.$emit("action", {
                            order: this.order,
                            action: e.currentTarget.dataset.type
                        });
                    },
                    handleCheck: function() {
                        this.$emit("check");
                    },
                    handleCoverChip: function() {
                        e.navigateTo({
                            url: "/pages/coverChip/index?sku_id=" + this.order.skus[0].sku_id
                        });
                    }
                }
            };
            t.default = i;
        }).call(this, n("543d").default);
    },
    "94dc": function(e, t, n) {
        n.r(t);
        var i = n("a3ce"), o = n("f5c9");
        for (var s in o) "default" !== s && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(s);
        n("5174");
        var a = n("f0c5"), u = Object(a.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        t.default = u.exports;
    },
    a3ce: function(e, t, n) {
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return o;
        }), n.d(t, "a", function() {});
        var i = function() {
            var e = this, t = (e.$createElement, e._self._c, this.$tool.formatDate(e.order.created_at, "MM-dd hh:mm"));
            e.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, o = [];
    },
    d7be: function(e, t, n) {},
    f5c9: function(e, t, n) {
        n.r(t);
        var i = n("6f06"), o = n.n(i);
        for (var s in i) "default" !== s && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(s);
        t.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/myBox/components/PackageSku-create-component", {
    "pages/myBox/components/PackageSku-create-component": function(e, t, n) {
        n("543d").createComponent(n("94dc"));
    }
}, [ [ "pages/myBox/components/PackageSku-create-component" ] ] ]);