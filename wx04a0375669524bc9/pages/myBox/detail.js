(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/detail" ], {
    "1a5f": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = {
                mixins: [ function(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }(e("fdc7")).default ],
                components: {
                    IActionSheet: function() {
                        e.e("components/ActionSheet/index").then(function() {
                            return resolve(e("9dcb"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        deliverRecord: null,
                        info: {},
                        uuid: ""
                    };
                },
                filters: {
                    hidePhoneDetail: function(n) {
                        return n ? n.substring(0, 3) + "****" + n.substring(7, 11) : "";
                    }
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    }
                },
                onLoad: function(n) {
                    this.uuid = n.uuid;
                },
                onShow: function() {
                    this.getOrderInfo();
                },
                methods: {
                    checkChip: function() {
                        n.navigateTo({
                            url: "/pages/myChip/index"
                        });
                    },
                    checkRedpack: function() {
                        n.navigateTo({
                            url: "/pages/myRedpack/index"
                        });
                    },
                    checkScore: function() {
                        n.navigateTo({
                            url: "/pages/myScore/index"
                        });
                    },
                    openContact: function() {},
                    setCopyText: function(t) {
                        n.setClipboardData({
                            data: t,
                            success: function(t) {
                                n.showToast({
                                    title: "复制成功"
                                });
                            }
                        });
                    },
                    getOrderInfo: function() {
                        var t = this;
                        this.uuid, n.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$http("/asset/package-skus/".concat(this.uuid)).then(function(e) {
                            t.info = e.data.info, n.hideLoading();
                        }).catch(function(n) {});
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    payNow: function() {
                        n.navigateTo({
                            url: "/pages/payCenter/index?uuid=" + this.order.uuid
                        });
                    }
                }
            };
            t.default = i;
        }).call(this, e("543d").default);
    },
    "34ad": function(n, t, e) {
        var i = e("ac73");
        e.n(i).a;
    },
    "5b82": function(n, t, e) {
        (function(n) {
            e("f868"), i(e("66fd"));
            var t = i(e("b2c6"));
            function i(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    a3e5: function(n, t, e) {
        e.d(t, "b", function() {
            return o;
        }), e.d(t, "c", function() {
            return u;
        }), e.d(t, "a", function() {
            return i;
        });
        var i = {
            SkuItem: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/SkuItem/SkuItem") ]).then(e.bind(null, "a6d5"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    ac73: function(n, t, e) {},
    b2c6: function(n, t, e) {
        e.r(t);
        var i = e("a3e5"), o = e("b3ce");
        for (var u in o) "default" !== u && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        e("34ad");
        var c = e("f0c5"), a = Object(c.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        t.default = a.exports;
    },
    b3ce: function(n, t, e) {
        e.r(t);
        var i = e("1a5f"), o = e.n(i);
        for (var u in i) "default" !== u && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        t.default = o.a;
    }
}, [ [ "5b82", "common/runtime", "common/vendor" ] ] ]);