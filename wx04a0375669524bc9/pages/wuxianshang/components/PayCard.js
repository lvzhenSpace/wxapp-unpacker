(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/wuxianshang/components/PayCard" ], {
    2930: function(o, e, t) {},
    "6f93": function(o, e, t) {
        t.d(e, "b", function() {
            return i;
        }), t.d(e, "c", function() {
            return s;
        }), t.d(e, "a", function() {
            return n;
        });
        var n = {
            PriceDisplay: function() {
                return t.e("components/PriceDisplay/PriceDisplay").then(t.bind(null, "f149"));
            },
            UsableCouponPopup: function() {
                return t.e("components/UsableCouponPopup/UsableCouponPopup").then(t.bind(null, "9d28"));
            }
        }, i = function() {
            var o = this, e = (o.$createElement, o._self._c, o.order.coupon_discount ? o.$tool.formatPrice(o.order.coupon_discount) : null), t = o.order.redpack ? o.$tool.formatPrice(o.order.redpack) : null, n = o.order.max_useable_score && o.form.is_use_score ? o._f("priceToFixed")(o.order.score_discount) : null;
            o.$mp.data = Object.assign({}, {
                $root: {
                    g0: e,
                    g1: t,
                    f0: n
                }
            });
        }, s = [];
    },
    "82ce": function(o, e, t) {
        t.r(e);
        var n = t("b7d6"), i = t.n(n);
        for (var s in n) "default" !== s && function(o) {
            t.d(e, o, function() {
                return n[o];
            });
        }(s);
        e.default = i.a;
    },
    b7d6: function(o, e, t) {
        (function(o) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = t("26cb"), i = function(o) {
                return o && o.__esModule ? o : {
                    default: o
                };
            }(t("ae4c"));
            function s(o, e) {
                var t = Object.keys(o);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(o);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(o, e).enumerable;
                    })), t.push.apply(t, n);
                }
                return t;
            }
            function u(o) {
                for (var e = 1; e < arguments.length; e++) {
                    var t = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? s(Object(t), !0).forEach(function(e) {
                        r(o, e, t[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(t)) : s(Object(t)).forEach(function(e) {
                        Object.defineProperty(o, e, Object.getOwnPropertyDescriptor(t, e));
                    });
                }
                return o;
            }
            function r(o, e, t) {
                return e in o ? Object.defineProperty(o, e, {
                    value: t,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : o[e] = t, o;
            }
            var a = {
                components: {},
                data: function() {
                    return {
                        payTotal: -1,
                        order: {},
                        price: 0,
                        form: {
                            is_use_redpack: 1,
                            is_use_score: 1,
                            is_doubleBoxCard: 0
                        },
                        total_prices: 0,
                        total_score_price: 0,
                        currentCoupon: {},
                        isCouponPopup: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        usableCouponsList: [],
                        unusableCouponsList: [],
                        isInit: !1,
                        isLoading: !1,
                        isSubmiting: !1,
                        isCheckUserStatement: !1,
                        doubleBoxCard: {},
                        count: 10,
                        buzaiBool: !1,
                        pingtaiBool: !1,
                        fahuoBoll: !1,
                        fahuoCurrent: 0,
                        fahuoList: [ "发货须知", "售后须知" ]
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: u(u({}, (0, n.mapGetters)([ "userInfo" ])), {}, {
                    isuserInfo: function() {
                        console.log(this.userInfo);
                    }
                }),
                watch: {
                    payTotal: function() {
                        this.initOrder();
                    }
                },
                created: function() {
                    this.payTotal = this.info.pay_total;
                    var e = this;
                    o.getStorage({
                        key: "wxs_scoreStorage",
                        success: function(o) {
                            e.form.is_use_score = o.data ? 1 : 0, setTimeout(function() {
                                e.initOrder();
                            }, 100);
                        }
                    }), o.getStorage({
                        key: "isCheckUserStatementBool",
                        success: function(o) {
                            e.isCheckUserStatement = o.data;
                        }
                    }), o.getStorage({
                        key: "isBuzaiBool",
                        success: function(o) {
                            e.buzaiBool = o.data;
                        }
                    });
                },
                methods: {
                    handleFaHuoClick: function() {
                        this.fahuoBoll = !0;
                    },
                    handleHuoClose: function() {
                        this.fahuoBoll = !1;
                    },
                    handleHuoClick: function(o) {
                        o !== this.fahuoCurrent && (this.fahuoCurrent = o);
                    },
                    uncheck: function() {
                        this.isCheckUserStatement = !this.isCheckUserStatement;
                    },
                    handleDesClick: function() {
                        this.buzaiBool = !this.buzaiBool, o.setStorage({
                            key: "isBuzaiBool",
                            data: !0,
                            success: function() {}
                        });
                    },
                    handleTuiClick: function() {
                        this.pingtaiBool = !1;
                    },
                    load: function() {
                        this.count += 10, this.showPopup();
                    },
                    showPopup: function() {
                        for (var o = [], e = [], t = 0; t < this.count; t++) void 0 !== this.usableCoupons[t] && o.push(this.usableCoupons[t]), 
                        void 0 !== this.unusableCoupons[t] && e.push(this.unusableCoupons[t]);
                        this.usableCouponsList = o.filter(function(o, e, t) {
                            return t.findIndex(function(e) {
                                return e.id === o.id;
                            }) === e;
                        }), this.unusableCouponsList = e.filter(function(o, e, t) {
                            return t.findIndex(function(e) {
                                return e.id === o.id;
                            }) === e;
                        }), this.isCouponPopup = !0;
                    },
                    hidePopup: function() {
                        this.count = 10, this.usableCouponsList = [], this.unusableCouponsList = [], this.isCouponPopup = !1;
                    },
                    couponChange: function(o) {
                        o.id === this.currentCoupon.id || (this.currentCoupon = o, this.initOrder());
                    },
                    initOrder: function() {
                        var e = this;
                        o.showLoading(), this.$http("/fudai/order/preview", "POST", u({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(t) {
                            e.isInit = !0, e.order = t.data.order, e.unusableCoupons = t.data.order.coupons.unusable, 
                            e.usableCoupons = t.data.order.coupons.usable, console.log("order=>", t.data.order), 
                            o.hideLoading();
                        }).catch(function(o) {
                            e.isInit = !1, e.cancel();
                        }), this.$http("/user/cards").then(function(o) {
                            e.doubleBoxCard = o.data.double_box_card, console.log(o.data.double_box_card);
                        });
                    },
                    switchChange: function(o) {
                        null == o.detail.value[0] ? this.form.is_use_redpack = 0 : this.form.is_use_redpack = 1, 
                        this.initOrder();
                    },
                    scoreSwitchChange: function(e) {
                        console.log(e), this.form.is_use_score = !(e.detail.value.length > 1), null == e.detail.value[0] ? (this.form.is_use_score = 0, 
                        o.setStorage({
                            key: "wxs_scoreStorage",
                            data: !1,
                            success: function() {
                                console.log("success");
                            }
                        })) : (this.form.is_use_score = 1, o.setStorage({
                            key: "wxs_scoreStorage",
                            data: !0,
                            success: function() {
                                console.log("success");
                            }
                        })), console.log(this.form.is_use_score), this.initOrder();
                    },
                    doubleBoxCardswitchChange: function(o) {
                        1 === this.form.is_doubleBoxCard ? this.form.is_doubleBoxCard = 0 : this.form.is_doubleBoxCard = 1, 
                        this.initOrder();
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    createOrder: function() {
                        var e = this;
                        if (this.isLoading) return !1;
                        this.isLoading = !0, o.showLoading({
                            title: "提交中"
                        }), this.$http("/fudai/order/confirm", "POST", u({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(t) {
                            o.hideLoading(), e.isSubmiting = !1;
                            var n = t.data;
                            console.log(n), n.is_need_pay ? i.default.pay(u(u({}, n), {}, {
                                success: function() {
                                    e.$emit("success", n.order, !1, n.is_doubleBoxCard);
                                },
                                fail: function() {
                                    o.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.isLoading = !1, e.$http("/orders/".concat(n.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : e.$emit("success", n.order, !1, n.is_doubleBoxCard);
                        }).catch(function(o) {
                            e.isLoading = !1;
                        });
                    },
                    handleBtnClick: function() {
                        if (!this.isCheckUserStatement) return o.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1;
                    },
                    handleJixuClick: function() {
                        if (this.pingtaiBool = !1, this.isLoading) return !1;
                        this.createOrder();
                    },
                    submit: function() {
                        if (o.setStorage({
                            key: "isCheckUserStatementBool",
                            data: !0,
                            success: function() {}
                        }), 0 == this.buzaiBool) this.pingtaiBool = !0; else {
                            if (this.pingtaiBool = !1, this.isLoading) return !1;
                            this.createOrder();
                        }
                    }
                },
                onPageScroll: function(o) {}
            };
            e.default = a;
        }).call(this, t("543d").default);
    },
    d0da: function(o, e, t) {
        var n = t("2930");
        t.n(n).a;
    },
    e027: function(o, e, t) {
        t.r(e);
        var n = t("6f93"), i = t("82ce");
        for (var s in i) "default" !== s && function(o) {
            t.d(e, o, function() {
                return i[o];
            });
        }(s);
        t("d0da");
        var u = t("f0c5"), r = Object(u.a)(i.default, n.b, n.c, !1, null, "011c0b61", null, !1, n.a, void 0);
        e.default = r.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/wuxianshang/components/PayCard-create-component", {
    "pages/wuxianshang/components/PayCard-create-component": function(o, e, t) {
        t("543d").createComponent(t("e027"));
    }
}, [ [ "pages/wuxianshang/components/PayCard-create-component" ] ] ]);