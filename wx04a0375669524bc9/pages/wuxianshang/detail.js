(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/wuxianshang/detail" ], {
    "15ca": function(t, n, e) {},
    "36df": function(t, n, e) {
        e.r(n);
        var i = e("53bc"), o = e.n(i);
        for (var a in i) "default" !== a && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(a);
        n.default = o.a;
    },
    "46cb": function(t, n, e) {
        e.r(n);
        var i = e("d7d8"), o = e("36df");
        for (var a in o) "default" !== a && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        e("a449");
        var s = e("f0c5"), r = Object(s.a)(o.default, i.b, i.c, !1, null, "ff3fbe62", null, !1, i.a, void 0);
        n.default = r.exports;
    },
    "53bc": function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var i = e("26cb");
            function o(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(t);
                    n && (i = i.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), e.push.apply(e, i);
                }
                return e;
            }
            function a(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var e = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? o(Object(e), !0).forEach(function(n) {
                        s(t, n, e[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : o(Object(e)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(e, n));
                    });
                }
                return t;
            }
            function s(t, n, e) {
                return n in t ? Object.defineProperty(t, n, {
                    value: e,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[n] = e, t;
            }
            var r = {
                components: {
                    PayCard: function() {
                        Promise.all([ e.e("common/vendor"), e.e("pages/wuxianshang/components/PayCard") ]).then(function() {
                            return resolve(e("e027"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    RecordList: function() {
                        e.e("pages/wuxianshang/components/RecordList").then(function() {
                            return resolve(e("f850"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    imagepop: function() {
                        e.e("pages/wuxianshang/components/imagepop").then(function() {
                            return resolve(e("a4bd"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    bangDan: function() {
                        Promise.all([ e.e("common/vendor"), e.e("pages/wuxianshang/components/bangDan") ]).then(function() {
                            return resolve(e("f22a"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    wanfa: function() {
                        e.e("pages/wuxianshang/components/wanfa").then(function() {
                            return resolve(e("ea91"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    jiangli: function() {
                        e.e("pages/wuxianshang/components/jiangli").then(function() {
                            return resolve(e("cecb"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        bgImgs: [ "url(https://img121.7dun.com/20230207NewImg/wuxianshang/rDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/srDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/ssrDes.png)", "url(https://img121.7dun.com/20230207NewImg/wuxianshang/urDes.png)", "url()" ],
                        currentswiper: 0,
                        numIndex: 1,
                        timers: null,
                        times: 0,
                        lastList: null,
                        levelList: [],
                        lordRewardThreeBool: !1,
                        bangDanObj: {},
                        bangDanBool: !1,
                        gameplayBool: !1,
                        lordRewardBool: !1,
                        lordRewardThree: {},
                        lordRewardTwo: {},
                        lordRewardOne: {},
                        lordRightObj: {},
                        lord: null,
                        lord_endtime: "",
                        uuid: "",
                        isInit: !1,
                        isPayPopup: !1,
                        payTotal: 1,
                        info: {
                            min_lucky_score: ""
                        },
                        order: {},
                        pageUuid: "",
                        isNotFound: !1,
                        isShowResultPopup: !1,
                        danmuSetting: {},
                        danmuList: [],
                        config: {},
                        detailImageList: [],
                        levelFilter: 0,
                        isTryMode: !1,
                        isShowRankList: !1,
                        wining_list: [],
                        isimagepop: !1,
                        is_doubleBoxCard: 0,
                        skuinfo: {},
                        skusList: [],
                        skus_ultimate: {},
                        color_list: [ {
                            level: 0,
                            color: "#00aa00"
                        }, {
                            level: 1,
                            color: "#aa55ff"
                        }, {
                            level: 2,
                            color: "#ffff00"
                        }, {
                            level: 3,
                            color: "#ffff00"
                        }, {
                            level: 4,
                            color: "#ff0000"
                        } ]
                    };
                },
                computed: a(a({}, (0, i.mapGetters)([ "userInfo" ])), {}, {
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/fudai/detail_bg.png";
                    },
                    boxImage: function() {
                        return this.info.image_3d || "https://cdn2.hquesoft.com/box/fudai/box.png";
                    },
                    skuLevel: function() {
                        return this.info.sku_level || [];
                    },
                    skus: function() {
                        var t = this;
                        return this.levelFilter ? this.info.skus.filter(function(n) {
                            return n.level === t.levelFilter;
                        }) : this.info.skus || [];
                    },
                    payInfo: function() {
                        return {
                            page_uuid: this.pageUuid,
                            title: this.info.title,
                            pay_total: this.payTotal,
                            total_list: this.info.total_list,
                            money_price: this.info.money_price,
                            score_price: this.info.score_price
                        };
                    },
                    titleBgColor: function() {
                        return " ";
                    },
                    titleColor: function() {
                        return "#ffffff";
                    },
                    setTitleText: function() {
                        t.setNavigationBarTitle({
                            title: this.info.title
                        });
                    },
                    discountTips: function() {
                        for (var t = this.info.total_list || [], n = t.length - 1; n >= 0; n--) if (t[n].is_discount) {
                            var e = t[n].total + "连开优惠";
                            return t[n].money_discount && (e += t[n].money_discount / 100 + "元"), t[n].score_discount && (e += t[n].score_discount + this.scoreAlias), 
                            e;
                        }
                        return !1;
                    },
                    width_value: function() {
                        return this.userInfo.lucky_score / this.info.min_lucky_score * 100 > 100 ? 100 : this.userInfo.lucky_score / this.info.min_lucky_score * 100 < 0 ? 0 : Math.trunc(this.userInfo.lucky_score / this.info.min_lucky_score * 100);
                    }
                }),
                watch: {},
                filters: {},
                onLoad: function(n) {
                    this.uuid = n.uuid, t.loadFontFace({
                        family: "BigjianNew",
                        source: 'url("'.concat("https://img121.7dun.com/member_grade/BigjianNew.TTF", '")'),
                        success: function() {},
                        fail: function(t) {}
                    });
                },
                onUnload: function() {},
                created: function() {},
                onPullDownRefresh: function() {
                    this.$showPullRefresh(), this.initData(), this.setTitleText();
                },
                onShow: function() {
                    var n = this;
                    t.showLoading({
                        title: "加载中"
                    }), this.newHttp(), this.initData().then(function(e) {
                        n.isInit = !0, t.hideLoading(), n.$http("/fudai/change", "POST", {
                            fudai_id: n.info.id
                        }).then(function(t) {
                            n.pageUuid = t.data.page_uuid, n.fetchList();
                        });
                    }).catch(function(t) {
                        n.isNotFound = !0;
                    });
                },
                onReachBottom: function() {},
                methods: {
                    handleBackClick: function() {
                        t.switchTab({
                            url: "/pages/index/index"
                        });
                    },
                    openBangDanClick: function() {
                        var t = this;
                        this.$http("/fudais/".concat(this.uuid, "/lord-rank"), "GET", {
                            page: 1,
                            per_page: 10
                        }).then(function(n) {
                            n.data.last_list.forEach(function(n) {
                                1 == n.ranking && (t.lastList = n);
                            }), t.bangDanBool = !0, t.bangDanObj = n.data;
                        });
                    },
                    closeBangDanClick: function() {
                        this.bangDanBool = !1;
                    },
                    openGameplayClick: function() {
                        this.gameplayBool = !0;
                    },
                    closeGameplayClick: function() {
                        this.gameplayBool = !1;
                    },
                    openJiangLiClick: function() {
                        this.lordRewardBool = !0;
                    },
                    closeJiangLiClick: function() {
                        this.lordRewardBool = !1;
                    },
                    handleTry: function() {
                        t.showLoading({
                            title: "刷新中..."
                        }), this.initData().then(function(n) {
                            t.hideLoading(), t.showToast({
                                title: "刷新完成~",
                                icon: "none"
                            });
                        });
                    },
                    fetchList: function() {
                        var t = this;
                        return !this.isLoading && (this.isLoading = !0, this.$http("/fudais/".concat(this.info.uuid, "/records"), "GET", {
                            page: 1,
                            per_page: 100
                        }).then(function(n) {
                            t.isInit = !0;
                            for (var e = 0; e < n.data.list.length; e++) t.wining_list.length < 12 && 5 == n.data.list[e].level && t.wining_list.push(n.data.list[e]);
                        }).catch(function(n) {
                            t.isInit = !1;
                        }));
                    },
                    setLevelFilter: function(t) {
                        this.levelFilter === t ? this.levelFilter = 0 : this.levelFilter = t;
                    },
                    addClick: function() {
                        t.vibrateShort({}), 1 === this.payTotal ? this.payTotal = this.payTotal + 2 : 3 === this.payTotal ? this.payTotal = this.payTotal + 7 : t.showToast({
                            title: "最多十抽哦~",
                            icon: "none"
                        });
                    },
                    minusClick: function() {
                        t.vibrateShort({}), 3 === this.payTotal ? this.payTotal = this.payTotal - 2 : 10 === this.payTotal ? this.payTotal = this.payTotal - 7 : 20 === this.payTotal ? this.payTotal = this.payTotal - 10 : t.showToast({
                            title: "最少一抽哦~",
                            icon: "none"
                        });
                    },
                    gomybox: function() {
                        t.reLaunch({
                            url: "/pages/myBox/index"
                        });
                    },
                    useFreeTicket: function() {
                        var n = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/fudai/order/confirm", "POST", {
                            page_uuid: this.pageUuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(e) {
                            t.hideLoading();
                            var i = e.data;
                            i.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : n.paySuccess(i.order);
                        });
                    },
                    getDanmu: function() {
                        var t = this;
                        this.$http("/danmus/fudai_detail?node_id=".concat(this.info.id)).then(function(n) {
                            t.danmuSetting = n.data.setting, t.danmuList = n.data.list;
                        });
                    },
                    showDetailImagePopup: function(t, n) {
                        t.odds = n, this.skuinfo = t, this.isimagepop = !0;
                    },
                    hideDetailImagePopup: function() {
                        this.$refs.detailPopup.close();
                    },
                    isimagepopfun: function() {
                        this.isimagepop = !1;
                    },
                    refresh: function() {
                        this.$store.dispatch("getUserInfo");
                    },
                    newHttp: function() {
                        var t = this;
                        return this.$http("/fudais/".concat(this.uuid), "GET", {}).then(function(n) {
                            t.levelList = n.data.info.sku_level.filter(function(t, n, e) {
                                return e.findIndex(function(n) {
                                    return n.level === t.level;
                                }) === n;
                            });
                            var e = 1;
                            if (n.data && "[]" != JSON.stringify(n.data.lord_info)) {
                                t.lord = n.data.lord_info, n.data.lord_info && n.data.lord_info.lord_skus[0] && (t.lordRightObj = t.lord && t.lord.lord_skus[0]), 
                                t.lord && t.lord.lord_skus[0] && t.lord.lord_skus.forEach(function(n) {
                                    1 == n.ranking && (t.lordRewardOne = n), 2 == n.ranking && (t.lordRewardTwo = n, 
                                    t.lordRewardThreeBool = !1), 3 == n.ranking && (t.lordRewardThreeBool = !0, t.lordRewardThree = n);
                                }), setInterval(function() {
                                    t.lordRightObj = t.lord.lord_skus[e], ++e >= t.lord.lord_skus.length && (e = 0);
                                }, 3e3);
                                var i = new Date(n.data.lord_info.end_at.replace(/-/g, "/")).getTime();
                                t.lord_endtime = function(t) {
                                    var n = new Date().getTime(), e = new Date(n), i = new Date(t), o = (e.getFullYear(), 
                                    e.getMonth(), e.getDate()), a = (e.getHours(), e.getMinutes(), e.getSeconds(), i.getFullYear(), 
                                    i.getMonth(), i.getDate()), s = i.getHours(), r = i.getMinutes();
                                    return i.getSeconds(), r < 10 && (r = "0" + r), s < 10 && (s = "0" + s), a - o >= 0 && a - o < 1 ? "今日 " + s + ":" + r : a - o >= 1 && a - o < 2 ? "明日 " + s + ":" + r : a + "日 " + s + ":" + r;
                                }(i);
                            }
                        });
                    },
                    newHttp60: function() {
                        var t = this;
                        this.times < 60 || this.$http("/fudais/".concat(this.uuid), "GET", {}).then(function(n) {
                            var e = 1;
                            if (n.data && "[]" != JSON.stringify(n.data.lord_info)) {
                                clearTimeout(t.timers), t.times = 0, t.lord = n.data.lord_info, n.data.lord_info && n.data.lord_info.lord_skus[0] && (t.lordRightObj = t.lord && t.lord.lord_skus[0]), 
                                t.lord && t.lord.lord_skus[0] && t.lord.lord_skus.forEach(function(n) {
                                    1 == n.ranking && (t.lordRewardOne = n), 2 == n.ranking && (t.lordRewardTwo = n, 
                                    t.lordRewardThreeBool = !1), 3 == n.ranking && (t.lordRewardThreeBool = !0, t.lordRewardThree = n);
                                }), setInterval(function() {
                                    t.lordRightObj = t.lord.lord_skus[e], ++e >= t.lord.lord_skus.length && (e = 0);
                                }, 3e3);
                                var i = new Date(n.data.lord_info.end_at.replace(/-/g, "/")).getTime();
                                t.lord_endtime = function(t) {
                                    var n = new Date().getTime(), e = new Date(n), i = new Date(t), o = (e.getFullYear(), 
                                    e.getMonth(), e.getDate()), a = (e.getHours(), e.getMinutes(), e.getSeconds(), i.getFullYear(), 
                                    i.getMonth(), i.getDate()), s = i.getHours(), r = i.getMinutes();
                                    return i.getSeconds(), r < 10 && (r = "0" + r), s < 10 && (s = "0" + s), a - o >= 0 && a - o < 1 ? "今日 " + s + ":" + r : a - o >= 1 && a - o < 2 ? "明日 " + s + ":" + r : a + "日 " + s + ":" + r;
                                }(i), console.log("最终时间", t.lord_endtime);
                            }
                        });
                    },
                    initData: function() {
                        var n = this;
                        return this.$http("/fudais/".concat(this.uuid), "GET", {}).then(function(e) {
                            t.hideLoading(), n.info = e.data.info, n.config = e.data.config, n.info.sku_level = n.info.sku_level.sort().reverse(), 
                            n.skusList = e.data.info.skus.filter(function(t) {
                                return 5 === t.level;
                            });
                            var i = 1;
                            n.numIndex = 1, n.skus_ultimate = n.skusList[0], setInterval(function() {
                                n.skus_ultimate = n.skusList[i], n.numIndex++, ++i >= n.skusList.length && (i = 0), 
                                n.numIndex == n.skusList.length && (n.numIndex = n.skusList.length), n.numIndex > n.skusList.length && (n.numIndex = 1);
                            }, 3e3), n.getDanmu(), t.setNavigationBarTitle({
                                title: n.info.title
                            });
                        });
                    },
                    changeswiper: function(t) {
                        this.currentswiper = t.detail.current;
                    },
                    handleLeftClick: function() {
                        this.currentswiper = this.currentswiper - 1, this.currentswiper <= -1 && (this.currentswiper = this.skusList.length - 1);
                    },
                    handleRightClick: function() {
                        this.currentswiper = this.currentswiper + 1, this.currentswiper >= this.skusList.length && (this.currentswiper = 0);
                    },
                    paySuccess: function(t) {
                        var n = this, e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], i = arguments.length > 2 ? arguments[2] : void 0;
                        this.isTryMode = e, this.order = t, this.is_doubleBoxCard = i, this.isPayPopup = !1, 
                        this.isShowResultPopup = !0, this.refresh(), this.newHttp60(), this.timers = setInterval(function() {
                            n.times++;
                        }, 1e3);
                    },
                    goBack: function() {
                        this.isShowResultPopup = !1;
                    },
                    hidePayPopup: function() {
                        this.newHttp(), this.isPayPopup = !1;
                    },
                    pay: function(t) {
                        this.payTotal = t, this.isPayPopup = !0;
                    },
                    getLevelIcon: function(t) {
                        return this.skuLevel.find(function(n) {
                            return n.level === t;
                        }).icon;
                    },
                    getLevelTitle: function(t) {
                        return this.skuLevel.find(function(n) {
                            return n.level === t;
                        }).title;
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = r;
        }).call(this, e("543d").default);
    },
    "947c": function(t, n, e) {
        (function(t) {
            e("f868"), i(e("66fd"));
            var n = i(e("46cb"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, t(n.default);
        }).call(this, e("543d").createPage);
    },
    a449: function(t, n, e) {
        var i = e("15ca");
        e.n(i).a;
    },
    d7d8: function(t, n, e) {
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return a;
        }), e.d(n, "a", function() {
            return i;
        });
        var i = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            },
            uniPopup: function() {
                return e.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(e.bind(null, "50c2"));
            },
            BoxSkuPopup: function() {
                return e.e("components/BoxSkuPopup/BoxSkuPopup").then(e.bind(null, "16ac"));
            },
            OpenBoxPopupTheme2: function() {
                return e.e("components/OpenBoxPopupTheme2/OpenBoxPopupTheme2").then(e.bind(null, "3444"));
            },
            FreeTicketFloatBtn: function() {
                return e.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(e.bind(null, "6af9"));
            },
            Danmus: function() {
                return e.e("components/Danmus/Danmus").then(e.bind(null, "7d24"));
            }
        }, o = function() {
            var t = this, n = (t.$createElement, t._self._c, t.__map(t.info.sku_level, function(n, e) {
                return {
                    $orig: t.__get_orig(n),
                    g0: n.odds.toFixed(2)
                };
            }));
            t._isMounted || (t.e0 = function(n) {
                t.isShowRankList = !0;
            }, t.e1 = function(n) {
                t.isShowRankList = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l0: n
                }
            });
        }, a = [];
    }
}, [ [ "947c", "common/runtime", "common/vendor" ] ] ]);