(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myAddress/index" ], {
    "568d": function(e, t, n) {
        var i = n("e1df");
        n.n(i).a;
    },
    6845: function(e, t, n) {
        n.r(t);
        var i = n("9dfc"), s = n.n(i);
        for (var c in i) "default" !== c && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(c);
        t.default = s.a;
    },
    "9dfc": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = n("049e"), s = {
                components: {
                    NoData: function() {
                        n.e("components/NoData/NoData").then(function() {
                            return resolve(n("83c6"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        list: [],
                        select: !1,
                        init: !1
                    };
                },
                filters: {
                    hidePhoneDetail: function(e) {
                        return e.substring(0, 3) + "****" + e.substring(7, 11);
                    }
                },
                onLoad: function(e) {
                    e.select && (this.select = !0);
                },
                onShow: function() {
                    var t = this;
                    e.showLoading({
                        title: "加载中"
                    }), (0, i.getAddressList)().then(function(n) {
                        t.list = n.data.addresses, t.init = !0, e.hideLoading();
                    });
                },
                methods: {
                    fetchData: function() {
                        var t = this;
                        (0, i.getAddressList)().then(function(n) {
                            t.list = n.data.addresses, t.init = !0, e.hideLoading();
                        });
                    },
                    selectAddress: function(t) {
                        this.select && (e.$emit("selectAddress", this.list[t.currentTarget.dataset.index]), 
                        e.navigateBack({
                            delta: 1
                        }));
                    },
                    handleEdit: function(e) {
                        var t = this.list[e];
                        this.$navigator.navigateTo({
                            url: "/pages/setAddress/index?uuid=".concat(t.uuid) + "&consignee=".concat(t.consignee) + "&phone=".concat(t.phone) + "&province=".concat(t.province) + "&city=".concat(t.city) + "&district=".concat(t.district) + "&address=".concat(t.address) + "&tag=".concat(t.tag) + "&is_default=".concat(t.is_default)
                        });
                    },
                    handleDelete: function(t) {
                        var n = this, s = t.currentTarget.dataset.index;
                        e.showModal({
                            title: "提示",
                            content: "删除该收货地址?",
                            success: function(t) {
                                t.confirm && (e.showLoading({
                                    title: "加载中"
                                }), (0, i.deleteAddress)(n.list[s].uuid).then(function(t) {
                                    n.list.splice(s, 1), e.hideLoading();
                                }));
                            }
                        });
                    },
                    handleNewAddress: function() {
                        e.navigateTo({
                            url: "/pages/setAddress/index"
                        });
                    },
                    $getAddress: function(e) {
                        console.log("main get address =======>"), my.authorize({
                            scopes: "scope.addressList",
                            success: function(t) {
                                my.tb.chooseAddress({
                                    addAddress: "show",
                                    searchAddress: "hide",
                                    locateAddress: "hide"
                                }, function(t) {
                                    e.success && e.success(t);
                                }, function(t) {
                                    e.fail && e.fail(t);
                                });
                            }
                        });
                    },
                    handleImportAddress: function() {
                        var t = this;
                        e.chooseAddress({
                            success: function(e) {
                                if (e.error) return !1;
                                (0, i.createAddress)({
                                    consignee: e.userName,
                                    phone: e.telNumber,
                                    province: e.provinceName,
                                    city: e.cityName,
                                    district: e.countyName,
                                    address: e.detailInfo,
                                    tag: "",
                                    is_default: 0
                                }).then(function(e) {
                                    t.fetchData();
                                });
                            }
                        });
                    }
                }
            };
            t.default = s;
        }).call(this, n("543d").default);
    },
    b621: function(e, t, n) {
        (function(e) {
            n("f868"), i(n("66fd"));
            var t = i(n("d0c4"));
            function i(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, e(t.default);
        }).call(this, n("543d").createPage);
    },
    c378: function(e, t, n) {
        n.d(t, "b", function() {
            return s;
        }), n.d(t, "c", function() {
            return c;
        }), n.d(t, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            }
        }, s = function() {
            var e = this, t = (e.$createElement, e._self._c, e.__map(e.list, function(t, n) {
                return {
                    $orig: e.__get_orig(t),
                    f0: e._f("hidePhoneDetail")(t.phone)
                };
            }));
            e.$mp.data = Object.assign({}, {
                $root: {
                    l0: t
                }
            });
        }, c = [];
    },
    d0c4: function(e, t, n) {
        n.r(t);
        var i = n("c378"), s = n("6845");
        for (var c in s) "default" !== c && function(e) {
            n.d(t, e, function() {
                return s[e];
            });
        }(c);
        n("568d");
        var a = n("f0c5"), o = Object(a.a)(s.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        t.default = o.exports;
    },
    e1df: function(e, t, n) {}
}, [ [ "b621", "common/runtime", "common/vendor" ] ] ]);