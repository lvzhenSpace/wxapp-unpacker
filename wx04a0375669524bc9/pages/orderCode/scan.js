(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/orderCode/scan" ], {
    "6e12": function(n, o, e) {
        (function(n) {
            e("f868"), t(e("66fd"));
            var o = t(e("a6bb"));
            function t(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(o.default);
        }).call(this, e("543d").createPage);
    },
    "871c": function(n, o, e) {},
    "8b32": function(n, o, e) {
        var t = e("871c");
        e.n(t).a;
    },
    a6bb: function(n, o, e) {
        e.r(o);
        var t = e("c2a8"), r = e("c796");
        for (var i in r) "default" !== i && function(n) {
            e.d(o, n, function() {
                return r[n];
            });
        }(i);
        e("8b32");
        var c = e("f0c5"), u = Object(c.a)(r.default, t.b, t.c, !1, null, null, null, !1, t.a, void 0);
        o.default = u.exports;
    },
    b1f1: function(n, o, e) {
        (function(n) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        code: "",
                        order: {}
                    };
                },
                onLoad: function(n) {},
                onShow: function() {},
                computed: {
                    skus: function() {
                        return this.order.skus;
                    },
                    address: function() {
                        return this.order.address;
                    }
                },
                methods: {
                    scan: function() {
                        var o = this;
                        n.scanCode({
                            success: function(n) {
                                o.code = n.result, o.initOrder();
                            }
                        });
                    },
                    submitDisableTips: function() {
                        n.showToast({
                            title: "订单不在待发货状态，无法核销",
                            icon: "none"
                        });
                    },
                    submit: function() {
                        var o = this;
                        n.showLoading({
                            title: "核销中"
                        }), this.$http("/order/scan/confirm", "POST", {
                            code: this.code
                        }).then(function(e) {
                            n.hideLoading(), o.initOrder(), n.setNavigationBarColor({
                                backgroundColor: "#2BA246",
                                frontColor: "#ffffff"
                            }), n.showToast({
                                title: "核销成功",
                                icon: "none"
                            });
                        });
                    },
                    initOrder: function() {
                        var o = this;
                        n.showLoading({
                            title: "加载中"
                        }), this.$http("/order/scan", "POST", {
                            code: this.code
                        }).then(function(e) {
                            n.hideLoading(), o.order = e.data.info;
                        });
                    },
                    reset: function() {
                        this.code = "", this.order = {}, n.setNavigationBarColor({
                            backgroundColor: "#FFCC00",
                            frontColor: "#000000"
                        });
                    }
                },
                onShareAppMessage: function() {
                    return {};
                }
            };
            o.default = e;
        }).call(this, e("543d").default);
    },
    c2a8: function(n, o, e) {
        e.d(o, "b", function() {
            return r;
        }), e.d(o, "c", function() {
            return i;
        }), e.d(o, "a", function() {
            return t;
        });
        var t = {
            SkuItem: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/SkuItem/SkuItem") ]).then(e.bind(null, "a6d5"));
            },
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, r = function() {
            var n = this, o = (n.$createElement, n._self._c, n.order.id ? n._f("priceToFixed")(n.order.coupon_discount) : null), e = n.order.id ? n._f("priceToFixed")(n.order.redpack_discount) : null, t = n.order.id ? n._f("priceToFixed")(n.order.carriage) : null;
            n.$mp.data = Object.assign({}, {
                $root: {
                    f0: o,
                    f1: e,
                    f2: t
                }
            });
        }, i = [];
    },
    c796: function(n, o, e) {
        e.r(o);
        var t = e("b1f1"), r = e.n(t);
        for (var i in t) "default" !== i && function(n) {
            e.d(o, n, function() {
                return t[n];
            });
        }(i);
        o.default = r.a;
    }
}, [ [ "6e12", "common/runtime", "common/vendor" ] ] ]);