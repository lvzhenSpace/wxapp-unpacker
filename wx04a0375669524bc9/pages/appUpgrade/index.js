(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/appUpgrade/index" ], {
    "25a9": function(e, t, n) {},
    "46c8": function(e, t, n) {
        n.r(t);
        var r = n("dbd2"), a = n("590b");
        for (var o in a) "default" !== o && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(o);
        n("a4f2");
        var i = n("f0c5"), c = Object(i.a)(a.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        t.default = c.exports;
    },
    "590b": function(e, t, n) {
        n.r(t);
        var r = n("a3d2"), a = n.n(r);
        for (var o in r) "default" !== o && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(o);
        t.default = a.a;
    },
    a3d2: function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }(n("a34a"));
            function a(e, t) {
                return function(e) {
                    if (Array.isArray(e)) return e;
                }(e) || function(e, t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(e)) {
                        var n = [], r = !0, a = !1, o = void 0;
                        try {
                            for (var i, c = e[Symbol.iterator](); !(r = (i = c.next()).done) && (n.push(i.value), 
                            !t || n.length !== t); r = !0) ;
                        } catch (e) {
                            a = !0, o = e;
                        } finally {
                            try {
                                r || null == c.return || c.return();
                            } finally {
                                if (a) throw o;
                            }
                        }
                        return n;
                    }
                }(e, t) || function(e, t) {
                    if (e) {
                        if ("string" == typeof e) return o(e, t);
                        var n = Object.prototype.toString.call(e).slice(8, -1);
                        return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(e) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? o(e, t) : void 0;
                    }
                }(e, t) || function() {
                    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function o(e, t) {
                (null == t || t > e.length) && (t = e.length);
                for (var n = 0, r = new Array(t); n < t; n++) r[n] = e[n];
                return r;
            }
            function i(e, t, n, r, a, o, i) {
                try {
                    var c = e[o](i), u = c.value;
                } catch (e) {
                    return void n(e);
                }
                c.done ? t(u) : Promise.resolve(u).then(r, a);
            }
            function c(e) {
                return function() {
                    var t = this, n = arguments;
                    return new Promise(function(r, a) {
                        var o = e.apply(t, n);
                        function c(e) {
                            i(o, r, a, c, u, "next", e);
                        }
                        function u(e) {
                            i(o, r, a, c, u, "throw", e);
                        }
                        c(void 0);
                    });
                };
            }
            var u = "__localFilePath__", l = null;
            var s = {
                data: function() {
                    return {
                        installForBeforeFilePath: "",
                        installed: !1,
                        installing: !1,
                        downloadSuccess: !1,
                        downloading: !1,
                        downLoadPercent: 0,
                        downloadedSize: 0,
                        packageFileSize: 0,
                        tempFilePath: "",
                        title: "更新日志",
                        contents: "",
                        is_mandatory: !1,
                        subTitle: "发现新版本",
                        downLoadBtnTextiOS: "立即跳转更新",
                        downLoadBtnText: "立即下载更新",
                        downLoadingText: "安装包下载中，请稍后"
                    };
                },
                onLoad: function(t) {
                    var n = t.local_storage_key;
                    if (!n) return console.error("local_storage_key为空，请检查后重试"), void e.navigateBack();
                    var r = e.getStorageSync(n);
                    if (!r) return console.error("安装包信息为空，请检查后重试"), void e.navigateBack();
                    var a = [ "version", "url", "type" ];
                    for (var o in r) if (-1 !== a.indexOf(o) && !r[o]) return console.error("参数 ".concat(o, " 必填，请检查后重试")), 
                    void e.navigateBack();
                    Object.assign(this, r), this.checkLocalStoragePackage();
                },
                onBackPress: function() {
                    if (this.is_mandatory) return !0;
                    l && l.abort();
                },
                computed: {
                    isWGT: function() {
                        return "wgt" === this.type;
                    },
                    isiOS: function() {
                        return !this.isWGT && this.platform.includes("iOS");
                    }
                },
                methods: {
                    checkLocalStoragePackage: function() {
                        var t = e.getStorageSync(u);
                        if (t) {
                            var n = t.version, r = t.savedFilePath;
                            t.installed || 0 !== function() {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "0", t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "0";
                                e = String(e).split("."), t = String(t).split(".");
                                for (var n = Math.min(e.length, t.length), r = 0, a = 0; a < n; a++) {
                                    var o = Number(e[a]), i = Number(t[a]);
                                    if (o > i) {
                                        r = 1;
                                        break;
                                    }
                                    if (o < i) {
                                        r = -1;
                                        break;
                                    }
                                }
                                if (0 === r && e.length !== t.length) for (var c = e.length > t.length, u = c ? e : t, l = n; l < u.length; l++) {
                                    var s = Number(u[l]);
                                    if (s > 0) {
                                        r = c ? 1 : -1;
                                        break;
                                    }
                                }
                                return r;
                            }(n, this.version) ? this.deleteSavedFile(r) : (this.downloadSuccess = !0, this.installForBeforeFilePath = r, 
                            this.tempFilePath = r);
                        }
                    },
                    closeUpdate: function() {
                        var t = this;
                        return c(r.default.mark(function n() {
                            return r.default.wrap(function(n) {
                                for (;;) switch (n.prev = n.next) {
                                  case 0:
                                    if (!t.downloading) {
                                        n.next = 5;
                                        break;
                                    }
                                    if (!t.is_mandatory) {
                                        n.next = 3;
                                        break;
                                    }
                                    return n.abrupt("return", e.showToast({
                                        title: "下载中，请稍后……",
                                        icon: "none",
                                        duration: 500
                                    }));

                                  case 3:
                                    return e.showModal({
                                        title: "是否取消下载？",
                                        cancelText: "否",
                                        confirmText: "是",
                                        success: function(t) {
                                            t.confirm && (l && l.abort(), e.navigateBack());
                                        }
                                    }), n.abrupt("return");

                                  case 5:
                                    if (!t.downloadSuccess || !t.tempFilePath) {
                                        n.next = 10;
                                        break;
                                    }
                                    return n.next = 8, t.saveFile(t.tempFilePath, t.version);

                                  case 8:
                                    return e.navigateBack(), n.abrupt("return");

                                  case 10:
                                    e.navigateBack();

                                  case 11:
                                  case "end":
                                    return n.stop();
                                }
                            }, n);
                        }))();
                    },
                    downloadPackage: function() {
                        var t = this;
                        this.downloading = !0, (l = e.downloadFile({
                            url: this.url,
                            success: function(e) {
                                200 == e.statusCode && (t.downloadSuccess = !0, t.tempFilePath = e.tempFilePath, 
                                t.is_mandatory && t.installPackage());
                            },
                            complete: function() {
                                t.downloading = !1, t.downLoadPercent = 0, t.downloadedSize = 0, t.packageFileSize = 0, 
                                l = null;
                            }
                        })).onProgressUpdate(function(e) {
                            t.downLoadPercent = e.progress, t.downloadedSize = (e.totalBytesWritten / Math.pow(1024, 2)).toFixed(2), 
                            t.packageFileSize = (e.totalBytesExpectedToWrite / Math.pow(1024, 2)).toFixed(2);
                        });
                    },
                    installPackage: function() {},
                    restart: function() {
                        this.installed = !1;
                    },
                    saveFile: function(t, n) {
                        return c(r.default.mark(function o() {
                            var i, c, l, s;
                            return r.default.wrap(function(r) {
                                for (;;) switch (r.prev = r.next) {
                                  case 0:
                                    return r.next = 2, e.saveFile({
                                        tempFilePath: t
                                    });

                                  case 2:
                                    if (i = r.sent, c = a(i, 2), l = c[0], s = c[1], !l) {
                                        r.next = 8;
                                        break;
                                    }
                                    return r.abrupt("return");

                                  case 8:
                                    e.setStorageSync(u, {
                                        version: n,
                                        savedFilePath: s.savedFilePath
                                    });

                                  case 9:
                                  case "end":
                                    return r.stop();
                                }
                            }, o);
                        }))();
                    },
                    deleteSavedFile: function(t) {
                        return e.removeStorageSync(u), e.removeSavedFile({
                            filePath: t
                        });
                    },
                    jumpToAppStore: function() {
                        plus.runtime.openURL(this.url);
                    }
                }
            };
            t.default = s;
        }).call(this, n("543d").default);
    },
    a4f2: function(e, t, n) {
        var r = n("25a9");
        n.n(r).a;
    },
    abaf: function(e, t, n) {
        (function(e) {
            n("f868"), r(n("66fd"));
            var t = r(n("46c8"));
            function r(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, e(t.default);
        }).call(this, n("543d").createPage);
    },
    dbd2: function(e, t, n) {
        n.d(t, "b", function() {
            return r;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {});
        var r = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    }
}, [ [ "abaf", "common/runtime", "common/vendor" ] ] ]);