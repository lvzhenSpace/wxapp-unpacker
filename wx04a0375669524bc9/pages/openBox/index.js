(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/openBox/index" ], {
    "240b": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = a(e("a34a")), i = a(e("fdc7")), u = e("26cb");
            function a(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            function r(n, t, e, o, i, u, a) {
                try {
                    var r = n[u](a), s = r.value;
                } catch (n) {
                    return void e(n);
                }
                r.done ? t(s) : Promise.resolve(s).then(o, i);
            }
            function s(n) {
                return function() {
                    var t = this, e = arguments;
                    return new Promise(function(o, i) {
                        var u = n.apply(t, e);
                        function a(n) {
                            r(u, o, i, a, s, "next", n);
                        }
                        function s(n) {
                            r(u, o, i, a, s, "throw", n);
                        }
                        a(void 0);
                    });
                };
            }
            function c(n, t) {
                var e = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(n);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(n, t).enumerable;
                    })), e.push.apply(e, o);
                }
                return e;
            }
            function d(n) {
                for (var t = 1; t < arguments.length; t++) {
                    var e = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? c(Object(e), !0).forEach(function(t) {
                        f(n, t, e[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(e)) : c(Object(e)).forEach(function(t) {
                        Object.defineProperty(n, t, Object.getOwnPropertyDescriptor(e, t));
                    });
                }
                return n;
            }
            function f(n, t, e) {
                return t in n ? Object.defineProperty(n, t, {
                    value: e,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : n[t] = e, n;
            }
            e("b426");
            var p = {
                name: "openBox",
                mixins: [ i.default ],
                components: {
                    PayCard: function() {
                        Promise.all([ e.e("common/vendor"), e.e("pages/openBox/components/PayCard") ]).then(function() {
                            return resolve(e("16ae"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        boxUuid: "",
                        roomUuid: "",
                        skuIndex: "",
                        pageUuid: "",
                        skuList: [],
                        boxInfo: {},
                        roomInfo: {},
                        isShowPayCard: !1,
                        isShowExcludeCard: !1,
                        isShowOpenCard: !1,
                        showBoxCard: {
                            score_price: 0,
                            my_balance: 0
                        },
                        excludeBoxCard: {
                            score_price: 0,
                            my_balance: 0
                        },
                        order: {},
                        yaoyiyaoFlag: !1,
                        isPay: !1,
                        isInit: !1,
                        isOpenPopup: !1,
                        animateStatus: 0,
                        visible: !1,
                        loading: !1,
                        tipsList: {},
                        tips: "",
                        isShowTips: !1,
                        isSharePopup: !1,
                        orderUuid: "",
                        danmuList: [],
                        danmuSetting: {},
                        buyTotal: 1,
                        remainTime: 0,
                        isShowSkuList: !1,
                        baseConfig: {},
                        isShowHiddenSkuRank: !1
                    };
                },
                computed: d(d({}, (0, u.mapGetters)([ "userInfo" ])), {}, {
                    defaultBoxImage: function() {
                        return this.isInit ? "https://cdn2.hquesoft.com/box/openbox.png" : "";
                    },
                    config: function() {
                        return this.$store.getters.setting.open_box;
                    },
                    payCardInfo: function() {
                        return {
                            box: this.boxInfo,
                            room: this.roomInfo,
                            sku_index: this.skuIndex,
                            page_uuid: this.pageUuid
                        };
                    },
                    deviceHeight: function() {
                        return this.$device.screenHeight;
                    },
                    isSmallDevice: function() {
                        return this.$device.screenHeight < 750;
                    },
                    instance: function() {
                        return this.boxInfo.instance;
                    },
                    posterInfo: function() {
                        return {
                            money_price: this.boxInfo.money_price,
                            score_price: this.boxInfo.score_price,
                            title: this.boxInfo.title,
                            path: this.getShareConfig().path,
                            thumb: this.boxInfo.thumb
                        };
                    },
                    freeTipLimit: function() {
                        return null === this.boxInfo.free_tips_limit ? 2 : this.boxInfo.free_tips_limit;
                    },
                    bgImage: function() {
                        return this.isInit ? this.boxInfo.room_bg_image || this.config.bg_image || "https://cdn2.hquesoft.com/box/bg.png" : "";
                    },
                    isDarkMode: function() {
                        return 2 === this.boxInfo.bg_color_mode;
                    },
                    boxTips: function() {
                        return this.boxInfo.tips || "官方正品，非质量问题不支持退换";
                    },
                    share: function() {
                        return {
                            title: this.boxInfo.title,
                            thumb: this.boxInfo.thumb,
                            desc: "",
                            path: "/pages/boxDetail/index?uuid=" + this.boxInfo.uuid + "&invite_node=box-" + this.boxInfo.uuid
                        };
                    }
                }),
                onLoad: function(t) {
                    var e = this;
                    return s(o.default.mark(function i() {
                        return o.default.wrap(function(o) {
                            for (;;) switch (o.prev = o.next) {
                              case 0:
                                e.roomUuid = t.roomUuid, e.skuIndex = parseInt(t.index), e.boxUuid = t.uuid, n.showLoading({
                                    title: "加载中",
                                    mask: !0
                                }), e.initRoom().then(function(n) {
                                    e.isInit = !0, "wholeRoom" === t.buyMode ? (e.changeone(), e.buyTotal = 0, e.isShowPayCard = !0) : t.buyMode && (e.buyTotal = parseInt(t.buyMode), 
                                    e.isShowPayCard = !0), e.getDanmu();
                                }), n.hideLoading(), n.$on("startShare", function() {
                                    e.isSharePopup = !0;
                                });

                              case 7:
                              case "end":
                                return o.stop();
                            }
                        }, i);
                    }))();
                },
                onShow: function() {
                    var t = this;
                    this.$store.dispatch("getUserInfo"), this.initData(), n.onAccelerometerChange(function(n) {
                        if (1 == t.boxInfo.open_mode && !t.yaoyiyaoFlag && 0 !== t.boxInfo.free_tips_limit) {
                            var e = .9;
                            (Math.abs(n.x) > e && Math.abs(n.y) > e || Math.abs(n.x) > e && Math.abs(n.z) > e) && (t.yaoyiyaoFlag || (t.$playAudio("yao"), 
                            t.getFreeTips(), t.yaoyiyaoFlag = !0, setTimeout(function() {
                                t.yaoyiyaoFlag = !1;
                            }, 2500)));
                        }
                    });
                },
                onUnload: function() {
                    n.stopAccelerometer();
                },
                onHide: function() {
                    n.stopAccelerometer();
                },
                methods: {
                    useFreeTicket: function() {
                        var t = this;
                        n.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/box-order/confirm", "POST", {
                            room_id: this.payCardInfo.room.id,
                            sku_index: this.payCardInfo.sku_index,
                            page_uuid: this.payCardInfo.page_uuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(e) {
                            n.hideLoading();
                            var o = e.data;
                            o.is_need_pay ? n.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : t.paySuccess(o.order);
                        });
                    },
                    getDanmu: function() {
                        var n = this;
                        this.$http("/danmus/box_open?node_id=".concat(this.boxInfo.id)).then(function(t) {
                            n.danmuSetting = t.data.setting, n.danmuList = t.data.list;
                        });
                    },
                    showPreviewTips: function() {
                        n.showToast({
                            title: "此款正在预售中，暂不可购买哦~",
                            icon: "none"
                        });
                    },
                    isExcluded: function(n) {
                        return -1 !== (this.tipsList.exclude || []).findIndex(function(t) {
                            return t.sku_id === n.id;
                        });
                    },
                    isShowed: function(n) {
                        return -1 !== (this.tipsList.show || []).findIndex(function(t) {
                            return t.sku_id === n.id;
                        });
                    },
                    initRoom: function() {
                        var t = this;
                        return this.$http("/rooms/".concat(this.roomUuid, "/skus/").concat(this.skuIndex)).then(function(e) {
                            t.boxInfo = e.data.box, t.roomInfo = e.data.room, t.skuList = e.data.sku_list, t.pageUuid = e.data.page_uuid, 
                            t.baseConfig = e.data.config, n.setNavigationBarTitle({
                                title: t.boxInfo.title
                            });
                        });
                    },
                    initData: function() {
                        var n = this;
                        this.$http("/user/cards").then(function(t) {
                            n.showBoxCard = t.data.show_box_card, n.excludeBoxCard = t.data.exclude_box_card;
                        });
                    },
                    previewSmallBoxThumb: function(t) {
                        var e = this.skuList.map(function(n) {
                            return n.thumb;
                        });
                        n.previewImage({
                            urls: e,
                            current: t
                        });
                    },
                    showTips: function(n) {
                        this.tips = n, this.isShowTips = !0;
                    },
                    buyNow: function() {
                        this.buyTotal = 1, this.isShowPayCard = !0;
                    },
                    useCard: function(t) {
                        var e = this, i = "温馨提示", u = "确定要使用1张透视卡吗~";
                        "exclude_box" === t && (i = "温馨提示", u = "确定要使用1张排除卡吗~"), n.showModal({
                            title: i,
                            content: u,
                            success: function() {
                                var i = s(o.default.mark(function i(u) {
                                    return o.default.wrap(function(o) {
                                        for (;;) switch (o.prev = o.next) {
                                          case 0:
                                            u.confirm && (n.showLoading({
                                                title: "提交中"
                                            }), e.$http("/box/use-card", "POST", {
                                                card_type: t,
                                                page_uuid: e.pageUuid
                                            }).then(function(t) {
                                                e.showTips(t.data.tips), e.isShowExcludeCard = !1, e.isShowOpenCard = !1, e.tipsList = t.data.tips_list, 
                                                n.hideLoading(), e.initData();
                                            }));

                                          case 1:
                                          case "end":
                                            return o.stop();
                                        }
                                    }, i);
                                }));
                                return function(n) {
                                    return i.apply(this, arguments);
                                };
                            }()
                        });
                    },
                    getFreeTips: function() {
                        var n = this;
                        this.$http("/box/use-card", "POST", {
                            card_type: "free_tips",
                            page_uuid: this.pageUuid
                        }).then(function(t) {
                            n.showTips(t.data.tips), n.isShowExcludeCard = !1, n.isShowOpenCard = !1, n.tipsList = t.data.tips_list;
                        });
                    },
                    handleOk: function() {
                        n.redirectTo({
                            url: "/pages/orderList/index"
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    changeone: function(n) {
                        var t = this;
                        this.animateStatus = 1, this.$http("/box/change-sku", "POST", {
                            room_id: this.roomInfo.id,
                            sku_index: this.skuIndex
                        }).then(function(n) {
                            t.skuIndex = n.data.sku_index, t.roomUuid = n.data.room_uuid, t.pageUuid = n.data.page_uuid, 
                            t.initRoom(), t.tipsList = {};
                        }), setTimeout(function(n) {
                            t.animateStatus = 2;
                        }, 800);
                    },
                    paySuccess: function(t) {
                        if (2 === this.boxInfo.open_mode) return n.redirectTo({
                            url: "/pages/orderList/index"
                        }), !0;
                        this.isShowPayCard = !1, n.showToast({
                            title: "支付成功, 开盒中",
                            icon: "none"
                        }), this.order = t, this.isOpenPopup = !0;
                    },
                    goBack: function() {
                        n.navigateBack({
                            delta: 1
                        });
                    }
                }
            };
            t.default = p;
        }).call(this, e("543d").default);
    },
    5907: function(n, t, e) {
        var o = e("ad45");
        e.n(o).a;
    },
    "89d4": function(n, t, e) {
        e.d(t, "b", function() {
            return i;
        }), e.d(t, "c", function() {
            return u;
        }), e.d(t, "a", function() {
            return o;
        });
        var o = {
            TextNavBar: function() {
                return e.e("components/TextNavBar/TextNavBar").then(e.bind(null, "24d6"));
            },
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            },
            OpenBoxPopup: function() {
                return e.e("components/OpenBoxPopup/OpenBoxPopup").then(e.bind(null, "4eb2"));
            },
            SharePopup: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/SharePopup/SharePopup") ]).then(e.bind(null, "4b48"));
            },
            Danmus: function() {
                return e.e("components/Danmus/Danmus").then(e.bind(null, "7d24"));
            },
            uniPopup: function() {
                return e.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(e.bind(null, "50c2"));
            },
            BoxSkuPopup: function() {
                return e.e("components/BoxSkuPopup/BoxSkuPopup").then(e.bind(null, "16ac"));
            },
            FreeTicketFloatBtn: function() {
                return e.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(e.bind(null, "6af9"));
            },
            HiddenSkuRank: function() {
                return e.e("components/HiddenSkuRank/HiddenSkuRank").then(e.bind(null, "5ada"));
            }
        }, i = function() {
            var n = this, t = (n.$createElement, n._self._c, n.boxInfo ? n._f("bigNumberDisplay")(n.userInfo.score) : null), e = n.boxInfo ? n.__map(n.skuList, function(t, e) {
                var o = n.__get_orig(t), i = n.isExcluded(t);
                return {
                    $orig: o,
                    m0: i,
                    m1: i ? null : n.isShowed(t)
                };
            }) : null;
            n._isMounted || (n.e0 = function(t) {
                n.isSharePopup = !0;
            }, n.e1 = function(t) {
                n.isShowHiddenSkuRank = !0;
            }, n.e2 = function(t) {
                n.isShowExcludeCard = !0;
            }, n.e3 = function(t) {
                n.isShowOpenCard = !0;
            }, n.e4 = function(t) {
                return n.$refs.detailPopup.open("bottom");
            }, n.e5 = function(t) {
                n.isShowTips = !1;
            }, n.e6 = function(t) {
                n.isShowExcludeCard = !1;
            }, n.e7 = function(t) {
                n.isShowOpenCard = !1;
            }, n.e8 = function(t) {
                n.isShowPayCard = !1;
            }, n.e9 = function(t) {
                n.isSharePopup = !1;
            }, n.e10 = function(t) {
                return n.$refs.detailPopup.close();
            }, n.e11 = function(t) {
                n.isShowHiddenSkuRank = !1;
            }), n.$mp.data = Object.assign({}, {
                $root: {
                    f0: t,
                    l0: e
                }
            });
        }, u = [];
    },
    "955b": function(n, t, e) {
        e.r(t);
        var o = e("240b"), i = e.n(o);
        for (var u in o) "default" !== u && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        t.default = i.a;
    },
    "9d86": function(n, t, e) {
        (function(n) {
            e("f868"), o(e("66fd"));
            var t = o(e("ed5a"));
            function o(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    ad45: function(n, t, e) {},
    ed5a: function(n, t, e) {
        e.r(t);
        var o = e("89d4"), i = e("955b");
        for (var u in i) "default" !== u && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        e("5907");
        var a = e("f0c5"), r = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = r.exports;
    }
}, [ [ "9d86", "common/runtime", "common/vendor" ] ] ]);