(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myScore/buy" ], {
    "0c44": function(t, e, n) {
        var o = n("4461");
        n.n(o).a;
    },
    4461: function(t, e, n) {},
    5562: function(t, e, n) {
        n.r(e);
        var o = n("8d80"), r = n("c0ee");
        for (var i in r) "default" !== i && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(i);
        n("0c44");
        var c = n("f0c5"), u = Object(c.a)(r.default, o.b, o.c, !1, null, "35812ca0", null, !1, o.a, void 0);
        e.default = u.exports;
    },
    7593: function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = i(n("a34a")), r = i(n("ae4c"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            function c(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    e && (o = o.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function u(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? c(Object(n), !0).forEach(function(e) {
                        a(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : c(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            function a(t, e, n) {
                return e in t ? Object.defineProperty(t, e, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[e] = n, t;
            }
            function s(t, e, n, o, r, i, c) {
                try {
                    var u = t[i](c), a = u.value;
                } catch (t) {
                    return void n(t);
                }
                u.done ? e(a) : Promise.resolve(a).then(o, r);
            }
            var f = {
                components: {},
                data: function() {
                    return {
                        isInit: !1,
                        list: [],
                        setting: {}
                    };
                },
                computed: {
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    }
                },
                onLoad: function() {
                    var e = this;
                    return function(t) {
                        return function() {
                            var e = this, n = arguments;
                            return new Promise(function(o, r) {
                                var i = t.apply(e, n);
                                function c(t) {
                                    s(i, o, r, c, u, "next", t);
                                }
                                function u(t) {
                                    s(i, o, r, c, u, "throw", t);
                                }
                                c(void 0);
                            });
                        };
                    }(o.default.mark(function n() {
                        return o.default.wrap(function(n) {
                            for (;;) switch (n.prev = n.next) {
                              case 0:
                                e.$store.dispatch("getUserInfo"), t.showLoading({
                                    title: "加载中"
                                }), e.$http("/asset/score-skus").then(function(n) {
                                    e.list = n.data.list, e.setting = n.data.setting, e.isInit = !0, t.hideLoading();
                                });

                              case 3:
                              case "end":
                                return n.stop();
                            }
                        }, n);
                    }))();
                },
                methods: {
                    submitBuy: function(e) {
                        var n = this;
                        t.showLoading({
                            title: "请求中"
                        }), this.$http("/asset/score-order/confirm", "POST", {
                            sku_id: e.id
                        }).then(function(e) {
                            t.hideLoading();
                            var o = e.data;
                            o.is_need_pay ? r.default.pay(u(u({}, o), {}, {
                                success: function() {
                                    t.showToast({
                                        title: "充值成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.redirectTo({
                                            url: "/pages/myScore/index"
                                        });
                                    }, 1500);
                                },
                                fail: function() {
                                    t.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), n.$http("/orders/".concat(o.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : (t.showToast({
                                title: "充值成功，即将跳转~",
                                icon: "none"
                            }), setTimeout(function(e) {
                                t.navigateTo({
                                    url: "/pages/myScore/index"
                                });
                            }, 1500));
                        });
                    }
                }
            };
            e.default = f;
        }).call(this, n("543d").default);
    },
    "8d80": function(t, e, n) {
        n.d(e, "b", function() {
            return r;
        }), n.d(e, "c", function() {
            return i;
        }), n.d(e, "a", function() {
            return o;
        });
        var o = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            }
        }, r = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    9526: function(t, e, n) {
        (function(t) {
            n("f868"), o(n("66fd"));
            var e = o(n("5562"));
            function o(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    },
    c0ee: function(t, e, n) {
        n.r(e);
        var o = n("7593"), r = n.n(o);
        for (var i in o) "default" !== i && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        e.default = r.a;
    }
}, [ [ "9526", "common/runtime", "common/vendor" ] ] ]);