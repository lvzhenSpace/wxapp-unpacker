(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myScore/index" ], {
    "1c80": function(t, e, n) {
        n.d(e, "b", function() {
            return a;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {
            return r;
        });
        var r = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            }
        }, a = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    2505: function(t, e, n) {
        n.r(e);
        var r = n("4e71"), a = n.n(r);
        for (var o in r) "default" !== o && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(o);
        e.default = a.a;
    },
    "2ad9": function(t, e, n) {
        (function(t) {
            n("f868"), r(n("66fd"));
            var e = r(n("8b60"));
            function r(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    },
    "4e71": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var r = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }(n("a34a")), a = n("f783");
            function o(t) {
                return function(t) {
                    if (Array.isArray(t)) return u(t);
                }(t) || function(t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
                }(t) || function(t, e) {
                    if (t) {
                        if ("string" == typeof t) return u(t, e);
                        var n = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? u(t, e) : void 0;
                    }
                }(t) || function() {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function u(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, r = new Array(e); n < e; n++) r[n] = t[n];
                return r;
            }
            function i(t, e, n, r, a, o, u) {
                try {
                    var i = t[o](u), c = i.value;
                } catch (t) {
                    return void n(t);
                }
                i.done ? e(c) : Promise.resolve(c).then(r, a);
            }
            function c(t) {
                return function() {
                    var e = this, n = arguments;
                    return new Promise(function(r, a) {
                        var o = t.apply(e, n);
                        function u(t) {
                            i(o, r, a, u, c, "next", t);
                        }
                        function c(t) {
                            i(o, r, a, u, c, "throw", t);
                        }
                        u(void 0);
                    });
                };
            }
            var s = {
                components: {},
                data: function() {
                    return {
                        list: [],
                        total: 0,
                        page: 1,
                        per_page: 20,
                        init: !1,
                        loading: !1
                    };
                },
                computed: {
                    navBar: function() {
                        return this.$store.getters.deviceInfo.customBar;
                    },
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    },
                    setting: function() {
                        return this.$store.getters.setting.score || {};
                    },
                    isBuyEnabled: function() {
                        return this.setting.is_buy_enabled;
                    }
                },
                onLoad: function() {
                    var e = this;
                    return c(r.default.mark(function n() {
                        var a, u;
                        return r.default.wrap(function(n) {
                            for (;;) switch (n.prev = n.next) {
                              case 0:
                                return e.$store.dispatch("getUserInfo"), t.showLoading({
                                    title: "加载中"
                                }), n.next = 4, e.getScoreRecord();

                              case 4:
                                u = n.sent, t.hideLoading(), e.init = !0, (a = e.list).push.apply(a, o(u.data.list)), 
                                e.total = u.data.item_total, t.setNavigationBarTitle({
                                    title: "我的" + e.scoreAlias
                                });

                              case 10:
                              case "end":
                                return n.stop();
                            }
                        }, n);
                    }))();
                },
                onReachBottom: function() {
                    var t = this;
                    return c(r.default.mark(function e() {
                        var n, a;
                        return r.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                if (!t.loading) {
                                    e.next = 2;
                                    break;
                                }
                                return e.abrupt("return");

                              case 2:
                                return t.loading = !0, t.page++, e.next = 6, t.getScoreRecord();

                              case 6:
                                a = e.sent, t.loading = !1, (n = t.list).push.apply(n, o(a.data.list));

                              case 9:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                methods: {
                    getScoreRecord: function() {
                        var t = this;
                        return c(r.default.mark(function e() {
                            return r.default.wrap(function(e) {
                                for (;;) switch (e.prev = e.next) {
                                  case 0:
                                    return e.next = 2, (0, a.getScoreRecord)({
                                        page: t.page,
                                        per_page: t.per_page
                                    });

                                  case 2:
                                    return e.abrupt("return", e.sent);

                                  case 3:
                                  case "end":
                                    return e.stop();
                                }
                            }, e);
                        }))();
                    },
                    scoreRedPack: function() {
                        t.navigateTo({
                            url: "/pages/scoreRedPack/index"
                        });
                    }
                }
            };
            e.default = s;
        }).call(this, n("543d").default);
    },
    "8b60": function(t, e, n) {
        n.r(e);
        var r = n("1c80"), a = n("2505");
        for (var o in a) "default" !== o && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(o);
        n("fec2");
        var u = n("f0c5"), i = Object(u.a)(a.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        e.default = i.exports;
    },
    e8a0: function(t, e, n) {},
    fec2: function(t, e, n) {
        var r = n("e8a0");
        n.n(r).a;
    }
}, [ [ "2ad9", "common/runtime", "common/vendor" ] ] ]);