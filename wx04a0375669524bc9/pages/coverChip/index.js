(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/coverChip/index" ], {
    "378e": function(n, t, e) {
        (function(n) {
            e("f868"), o(e("66fd"));
            var t = o(e("a59e"));
            function o(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    "396f": function(n, t, e) {
        e.d(t, "b", function() {
            return o;
        }), e.d(t, "c", function() {
            return i;
        }), e.d(t, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    "7d27": function(n, t, e) {
        e.r(t);
        var o = e("b61e"), i = e.n(o);
        for (var u in o) "default" !== u && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        t.default = i.a;
    },
    a59e: function(n, t, e) {
        e.r(t);
        var o = e("396f"), i = e("7d27");
        for (var u in i) "default" !== u && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        e("b664");
        var a = e("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = c.exports;
    },
    b61e: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        skuId: "",
                        info: {}
                    };
                },
                onLoad: function(n) {
                    this.skuId = n.sku_id;
                },
                onShow: function() {
                    var t = this;
                    n.showLoading({
                        title: "加载中"
                    }), this.$http("/cover-chip/info", "POST", {
                        sku_id: this.skuId
                    }).then(function(e) {
                        t.info = e.data, n.hideLoading();
                    });
                },
                methods: {
                    submitCover: function() {
                        n.showLoading({
                            title: "兑换中，请稍候"
                        }), this.$http("/cover-chip", "POST", {
                            sku_id: this.skuId
                        }).then(function(t) {
                            n.showToast({
                                title: "兑换成功，即将跳转~",
                                icon: "none"
                            }), setTimeout(function() {
                                n.redirectTo({
                                    url: "/pages/myChip/index"
                                });
                            }, 1300);
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    b664: function(n, t, e) {
        var o = e("da15");
        e.n(o).a;
    },
    da15: function(n, t, e) {}
}, [ [ "378e", "common/runtime", "common/vendor" ] ] ]);