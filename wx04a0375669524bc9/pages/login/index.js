(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/login/index" ], {
    "0a42": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = n("326d");
            function i(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function a(e, t, n) {
                return t in e ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = n, e;
            }
            var r = {
                name: "login",
                components: {
                    GetPhonePopup: function() {
                        n.e("components/GetPhonePopup/GetPhonePopupForWechat").then(function() {
                            return resolve(n("ef4e"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        code: "",
                        params: {},
                        isShowPhonePopup: !1,
                        dataUuid: "",
                        isLoginLoading: !1,
                        isCheckUserStatement: !0,
                        isuser_info: !1
                    };
                },
                created: function() {
                    this.getCode();
                },
                onShow: function() {
                    var t = (0, o.$getStorage)("token");
                    t && (console.log("token====>" + t), setTimeout(function(t) {
                        e.navigateBack({
                            delta: 1
                        });
                    }, 100));
                },
                computed: {
                    logo: function() {
                        return this.$store.getters.setting.login_page.logo || "";
                    },
                    bgImage: function() {
                        return this.$store.getters.setting.login_page.bg_image || "";
                    }
                },
                methods: {
                    getCode: function() {
                        var t = this;
                        e.login({
                            success: function(e) {
                                console.log("logon ====>", e), t.code = e.code;
                            },
                            fail: function(e) {
                                console.log(e);
                            }
                        });
                    },
                    getUserInfoWithMiniapp: function() {
                        var t = this;
                        return this.isCheckUserStatement ? !this.isLoginLoading && (this.isLoginLoading = !0, 
                        void wx.getUserProfile({
                            desc: "用于完善会员资料",
                            success: function(e) {
                                console.log(e), (0, o.$getStorage)("click_id") ? t.params = {
                                    code: t.code,
                                    encrypt_data: e.encryptedData,
                                    iv: e.iv,
                                    click_id: (0, o.$getStorage)("click_id")
                                } : t.params = {
                                    code: t.code,
                                    encrypt_data: e.encryptedData,
                                    iv: e.iv
                                }, t.submitLogin(t.params);
                            },
                            complete: function() {
                                t.isLoginLoading = !1;
                            }
                        })) : (e.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1);
                    },
                    getPhoneNumberSuccess: function(e) {
                        this.params = function(e) {
                            for (var t = 1; t < arguments.length; t++) {
                                var n = null != arguments[t] ? arguments[t] : {};
                                t % 2 ? i(Object(n), !0).forEach(function(t) {
                                    a(e, t, n[t]);
                                }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : i(Object(n)).forEach(function(t) {
                                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                                });
                            }
                            return e;
                        }({
                            data_uuid: this.dataUuid
                        }, e), (0, o.$getStorage)("click_id") && (this.params.click_id = (0, o.$getStorage)("click_id")), 
                        (0, o.$getStorage)("weixinadinfo") && (this.params.weixinadinfo = (0, o.$getStorage)("weixinadinfo")), 
                        this.isuser_info = !0, this.submitLogin(this.params);
                    },
                    stopLogin: function() {
                        (0, o.$setStorage)("stop_login_time", new Date().getTime()), e.switchTab({
                            url: "/pages/index/index"
                        });
                    },
                    submitLogin: function(t) {
                        var n = this;
                        return e.showLoading({
                            title: "登录中"
                        }), (0, o.$getStorage)("click_id") && (t.click_id = (0, o.$getStorage)("click_id")), 
                        this.$store.dispatch("login", {
                            type: "with-wechat",
                            params: t
                        }).then(function(t) {
                            e.hideLoading(), t.data.is_need_phone_number || t.data.is_need_phone ? (n.isShowPhonePopup = !0, 
                            n.dataUuid = t.data.data_uuid, n.getCode()) : t.data.token && (n.$store.dispatch("getUserInfo"), 
                            t.data.is_first_login ? e.redirectTo({
                                url: "/pages/myProfile/index"
                            }) : e.navigateBack({
                                delta: 1
                            }));
                        });
                    }
                }
            };
            t.default = r;
        }).call(this, n("543d").default);
    },
    1027: function(e, t, n) {
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            UserStatement: function() {
                return n.e("components/UserStatement/UserStatement").then(n.bind(null, "8c17"));
            },
            GetPhonePopup: function() {
                return n.e("components/GetPhonePopup/GetPhonePopup").then(n.bind(null, "4a88"));
            }
        }, i = function() {
            var e = this;
            e.$createElement;
            e._self._c, e._isMounted || (e.e0 = function(t) {
                e.isShowPhonePopup = !1;
            });
        }, a = [];
    },
    "4b4b": function(e, t, n) {
        var o = n("8d1e");
        n.n(o).a;
    },
    "8d1e": function(e, t, n) {},
    a200: function(e, t, n) {
        n.r(t);
        var o = n("0a42"), i = n.n(o);
        for (var a in o) "default" !== a && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = i.a;
    },
    e90e: function(e, t, n) {
        n.r(t);
        var o = n("1027"), i = n("a200");
        for (var a in i) "default" !== a && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(a);
        n("4b4b");
        var r = n("f0c5"), c = Object(r.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = c.exports;
    },
    ef07: function(e, t, n) {
        (function(e) {
            n("f868"), o(n("66fd"));
            var t = o(n("e90e"));
            function o(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, e(t.default);
        }).call(this, n("543d").createPage);
    }
}, [ [ "ef07", "common/runtime", "common/vendor" ] ] ]);