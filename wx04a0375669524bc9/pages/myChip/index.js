(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myChip/index" ], {
    "1a3d": function(n, t, e) {
        e.d(t, "b", function() {
            return o;
        }), e.d(t, "c", function() {
            return r;
        }), e.d(t, "a", function() {
            return u;
        });
        var u = {
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "83c6"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    },
    3501: function(n, t, e) {},
    "5cd3": function(n, t, e) {
        e.r(t);
        var u = e("1a3d"), o = e("fe73");
        for (var r in o) "default" !== r && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(r);
        e("e0d6");
        var a = e("f0c5"), i = Object(a.a)(o.default, u.b, u.c, !1, null, null, null, !1, u.a, void 0);
        t.default = i.exports;
    },
    "697d": function(n, t, e) {
        (function(n) {
            e("f868"), u(e("66fd"));
            var t = u(e("5cd3"));
            function u(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    e0d6: function(n, t, e) {
        var u = e("3501");
        e.n(u).a;
    },
    f3a5: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var u = function(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }(e("a34a"));
            function o(n, t, e, u, o, r, a) {
                try {
                    var i = n[r](a), c = i.value;
                } catch (n) {
                    return void e(n);
                }
                i.done ? t(c) : Promise.resolve(c).then(u, o);
            }
            var r = {
                components: {},
                data: function() {
                    return {
                        list: []
                    };
                },
                computed: {
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    }
                },
                onLoad: function() {
                    var t = this;
                    return function(n) {
                        return function() {
                            var t = this, e = arguments;
                            return new Promise(function(u, r) {
                                var a = n.apply(t, e);
                                function i(n) {
                                    o(a, u, r, i, c, "next", n);
                                }
                                function c(n) {
                                    o(a, u, r, i, c, "throw", n);
                                }
                                i(void 0);
                            });
                        };
                    }(u.default.mark(function e() {
                        return u.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                n.showLoading({
                                    title: "加载中"
                                }), t.$http("/chip-assets").then(function(e) {
                                    t.list = e.data.list, n.hideLoading();
                                });

                              case 2:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                methods: {}
            };
            t.default = r;
        }).call(this, e("543d").default);
    },
    fe73: function(n, t, e) {
        e.r(t);
        var u = e("f3a5"), o = e.n(u);
        for (var r in u) "default" !== r && function(n) {
            e.d(t, n, function() {
                return u[n];
            });
        }(r);
        t.default = o.a;
    }
}, [ [ "697d", "common/runtime", "common/vendor" ] ] ]);