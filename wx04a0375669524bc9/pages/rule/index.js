(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/rule/index" ], {
    2216: function(e, n, t) {
        var o = t("f0fe");
        t.n(o).a;
    },
    6797: function(e, n, t) {
        t.r(n);
        var o = t("aa31"), i = t.n(o);
        for (var r in o) "default" !== r && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(r);
        n.default = i.a;
    },
    "6dc8": function(e, n, t) {
        t.d(n, "b", function() {
            return i;
        }), t.d(n, "c", function() {
            return r;
        }), t.d(n, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/PageRender/PageRender") ]).then(t.bind(null, "e85b"));
            },
            SharePopup: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/SharePopup") ]).then(t.bind(null, "4b48"));
            }
        }, i = function() {
            var e = this;
            e.$createElement;
            e._self._c, e._isMounted || (e.e0 = function(n) {
                e.showShare = !1;
            });
        }, r = [];
    },
    "93a0": function(e, n, t) {
        t.r(n);
        var o = t("6dc8"), i = t("6797");
        for (var r in i) "default" !== r && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(r);
        t("2216");
        var a = t("f0c5"), u = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        n.default = u.exports;
    },
    aa31: function(e, n, t) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = t("7fce"), i = {
                components: {
                    PosterTheme1: (t("f93e"), function() {
                        Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/components/posterTheme1") ]).then(function() {
                            return resolve(t("4965"));
                        }.bind(null, t)).catch(t.oe);
                    }),
                    SharePopup: function() {
                        Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/SharePopup") ]).then(function() {
                            return resolve(t("4b48"));
                        }.bind(null, t)).catch(t.oe);
                    }
                },
                name: "rule",
                data: function() {
                    return {
                        isInit: !1,
                        type: "",
                        page: {},
                        canvasStatus: !1,
                        sharerInfo: {},
                        all_money: 0,
                        userInfo: (0, o.getStorageSync)("userInfo") || {},
                        showShare: !1,
                        title: "初级推荐官",
                        direct_inviter: 0,
                        indirect_inviter: 0
                    };
                },
                computed: {
                    posterInfo: function() {
                        var e = this.$store.getters.userInfo || {}, n = this.getShareConfig(!1);
                        return {
                            path: "/pages/index/index?type=share&&inviter=" + e.uuid,
                            app_url: n.app_url,
                            thumb: "https://img121.7dun.com/member_grade/posterImg.png"
                        };
                    }
                },
                watch: {},
                created: function() {
                    e.loadFontFace({
                        family: "BebasNeue",
                        source: 'url("https://img121.7dun.com/newRecommend2/BebasNeue-1.otf")',
                        success: function() {
                            console.log("success");
                        }
                    });
                },
                onLoad: function(e) {
                    this.type = e.type, this.initData(), this.getDistribution();
                },
                methods: {
                    tograde: function() {
                        e.pageScrollTo({
                            scrollTop: 800,
                            duration: 300
                        });
                    },
                    initData: function() {
                        var e = this;
                        this.$http("/setting/" + this.type + "_rule").then(function(n) {
                            e.page = n.data.setting, e.isInit = !0;
                        });
                    },
                    close: function() {
                        this.showShare = !1, this.$emit("close");
                    },
                    saveImg: function() {
                        e.downloadFile({
                            url: "https://img121.7dun.com/member_grade/peipeiOnline.jpg",
                            success: function(n) {
                                e.showToast({
                                    title: "已保存到相册",
                                    icon: "none"
                                });
                            }
                        });
                    },
                    handlePosterImg: function() {
                        this.showShare = !0;
                    },
                    getDistribution: function() {
                        var e = this;
                        this.$http("/agent/brokerage").then(function(n) {
                            e.sharerInfo = n.data, e.all_money = n.data.all.money / 100, e.title = n.data.now_agent_level, 
                            e.direct_inviter = n.data.direct_inviter, e.indirect_inviter = n.data.indirect_inviter;
                        });
                    },
                    goinvite: function() {
                        e.navigateTo({
                            url: "/pages/myInvitees/index"
                        });
                    }
                }
            };
            n.default = i;
        }).call(this, t("543d").default);
    },
    cb8f: function(e, n, t) {
        (function(e) {
            t("f868"), o(t("66fd"));
            var n = o(t("93a0"));
            function o(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = t, e(n.default);
        }).call(this, t("543d").createPage);
    },
    f0fe: function(e, n, t) {}
}, [ [ "cb8f", "common/runtime", "common/vendor" ] ] ]);