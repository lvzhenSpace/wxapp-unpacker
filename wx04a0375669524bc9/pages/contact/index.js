(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/contact/index" ], {
    "63e2": function(n, t, e) {
        var a = e("69d0");
        e.n(a).a;
    },
    "69d0": function(n, t, e) {},
    9423: function(n, t, e) {
        e.r(t);
        var a = e("ea4f"), u = e.n(a);
        for (var o in a) "default" !== o && function(n) {
            e.d(t, n, function() {
                return a[n];
            });
        }(o);
        t.default = u.a;
    },
    "9edd": function(n, t, e) {
        e.d(t, "b", function() {
            return u;
        }), e.d(t, "c", function() {
            return o;
        }), e.d(t, "a", function() {
            return a;
        });
        var a = {
            PageRender: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/PageRender/PageRender") ]).then(e.bind(null, "e85b"));
            }
        }, u = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    cf7f: function(n, t, e) {
        (function(n) {
            e("f868"), a(e("66fd"));
            var t = a(e("da5b"));
            function a(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    da5b: function(n, t, e) {
        e.r(t);
        var a = e("9edd"), u = e("9423");
        for (var o in u) "default" !== o && function(n) {
            e.d(t, n, function() {
                return u[n];
            });
        }(o);
        e("63e2");
        var i = e("f0c5"), c = Object(i.a)(u.default, a.b, a.c, !1, null, null, null, !1, a.a, void 0);
        t.default = c.exports;
    },
    ea4f: function(n, t, e) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        t.default = {
            name: "rule",
            data: function() {
                return {
                    isInit: !1,
                    type: "",
                    page: {}
                };
            },
            computed: {},
            watch: {},
            onLoad: function(n) {
                this.type = n.type, this.initData();
            },
            methods: {
                initData: function() {
                    var n = this;
                    this.$http("/setting/contact_page").then(function(t) {
                        n.page = t.data.setting, n.isInit = !0;
                    });
                }
            }
        };
    }
}, [ [ "cf7f", "common/runtime", "common/vendor" ] ] ]);