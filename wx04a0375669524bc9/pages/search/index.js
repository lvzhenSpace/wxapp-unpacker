(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/search/index" ], {
    1606: function(t, e, n) {
        n.d(e, "b", function() {
            return r;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            ProductList: function() {
                return n.e("components/ProductList/ProductList").then(n.bind(null, "e5e7"));
            },
            NoMore: function() {
                return n.e("components/NoMore/NoMore").then(n.bind(null, "d411"));
            },
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "83c6"));
            }
        }, r = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    1922: function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = r(n("a34a"));
            function r(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            function o(t, e, n, i, r, o, a) {
                try {
                    var c = t[o](a), s = c.value;
                } catch (t) {
                    return void n(t);
                }
                c.done ? e(s) : Promise.resolve(s).then(i, r);
            }
            function a(t) {
                return function(t) {
                    if (Array.isArray(t)) return c(t);
                }(t) || function(t) {
                    if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
                }(t) || function(t, e) {
                    if (t) {
                        if ("string" == typeof t) return c(t, e);
                        var n = Object.prototype.toString.call(t).slice(8, -1);
                        return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? Array.from(t) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? c(t, e) : void 0;
                    }
                }(t) || function() {
                    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }();
            }
            function c(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i;
            }
            var s = {
                mixins: [ r(n("fdc7")).default ],
                components: {
                    ProductList: function() {
                        n.e("pages/search/components/list").then(function() {
                            return resolve(n("cfb6"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    SortBar: function() {
                        n.e("pages/search/components/sortBar").then(function() {
                            return resolve(n("9250"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        sort: "",
                        key: "",
                        visible: !1,
                        category: [],
                        list: [],
                        page: 1,
                        per_page: 20,
                        total: 0,
                        activeCategory: "",
                        init: !1,
                        title: "全部商品",
                        action: "",
                        itemType: "all",
                        loading: !1
                    };
                },
                computed: {
                    isShowSortBar: function() {
                        return !("search" === this.action && !this.key);
                    },
                    guessList: function() {
                        return this.list.slice(0, 6);
                    }
                },
                watch: {
                    key: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    },
                    sort: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    },
                    itemType: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    }
                },
                onLoad: function(e) {
                    var n = this;
                    this.activeCategory = e.category_id || 0, this.title = e.title, this.action = e.action, 
                    this.itemType = e.type || "all", this.activeCategory && !this.title ? this.$http("/categories/" + this.activeCategory).then(function(e) {
                        n.title = e.data.info.title, t.setNavigationBarTitle({
                            title: n.title
                        });
                    }) : t.setNavigationBarTitle({
                        title: this.title
                    }), this.getProductList(), this.$visitor.record("search");
                },
                methods: {
                    setSort: function(t) {
                        this.sort = t;
                    },
                    setType: function(t) {
                        this.itemType = t;
                    },
                    activeChange: function(t) {
                        var e = t.currentTarget.dataset.id;
                        this.activeCategory = e == this.activeCategory ? "" : e, this.visible = !1, this.getProductList();
                    },
                    handleChange: function(t) {
                        var e = t.currentTarget.dataset.index, n = this.category[e];
                        0 !== n.sub_categories.length ? this.category[e].openSubCategory = !this.category[e].openSubCategory : (this.activeCategory = n.id, 
                        this.visible = !1, this.getProductList());
                    },
                    handleClick: function(e) {
                        t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + e.uuid
                        });
                    },
                    getProductList: function() {
                        var e = this;
                        this.loading = !0, t.showLoading({
                            mask: !0
                        }), this.$http("/search", "POST", {
                            category_id: this.activeCategory,
                            page: this.page,
                            key: this.key,
                            sort: this.sort,
                            item_type: this.itemType
                        }).then(function(n) {
                            var i;
                            e.init = !0, t.hideLoading(), (i = e.list).push.apply(i, a(n.data.list)), e.loading = !1, 
                            e.page++;
                        });
                    },
                    showDrawer: function() {
                        this.visible = !0;
                    },
                    drawerClose: function() {
                        this.visible = !1;
                    }
                },
                onReachBottom: function() {
                    var t = this;
                    return function(t) {
                        return function() {
                            var e = this, n = arguments;
                            return new Promise(function(i, r) {
                                var a = t.apply(e, n);
                                function c(t) {
                                    o(a, i, r, c, s, "next", t);
                                }
                                function s(t) {
                                    o(a, i, r, c, s, "throw", t);
                                }
                                c(void 0);
                            });
                        };
                    }(i.default.mark(function e() {
                        return i.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                if (!t.loading) {
                                    e.next = 2;
                                    break;
                                }
                                return e.abrupt("return");

                              case 2:
                                t.getProductList();

                              case 3:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                }
            };
            e.default = s;
        }).call(this, n("543d").default);
    },
    5670: function(t, e, n) {},
    a9c9: function(t, e, n) {
        n.r(e);
        var i = n("1922"), r = n.n(i);
        for (var o in i) "default" !== o && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(o);
        e.default = r.a;
    },
    b816: function(t, e, n) {
        var i = n("5670");
        n.n(i).a;
    },
    ce4c: function(t, e, n) {
        n.r(e);
        var i = n("1606"), r = n("a9c9");
        for (var o in r) "default" !== o && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(o);
        n("b816");
        var a = n("f0c5"), c = Object(a.a)(r.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = c.exports;
    },
    fb69: function(t, e, n) {
        (function(t) {
            n("f868"), i(n("66fd"));
            var e = i(n("ce4c"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    }
}, [ [ "fb69", "common/runtime", "common/vendor" ] ] ]);