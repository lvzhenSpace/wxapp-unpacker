(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/orderPreview/index" ], {
    1506: function(e, t, r) {
        r.d(t, "b", function() {
            return i;
        }), r.d(t, "c", function() {
            return o;
        }), r.d(t, "a", function() {
            return n;
        });
        var n = {
            SelectAddress: function() {
                return r.e("components/SelectAddress/SelectAddress").then(r.bind(null, "990a"));
            },
            SkuItem: function() {
                return Promise.all([ r.e("common/vendor"), r.e("components/SkuItem/SkuItem") ]).then(r.bind(null, "a6d5"));
            },
            PriceDisplay: function() {
                return r.e("components/PriceDisplay/PriceDisplay").then(r.bind(null, "f149"));
            },
            UsableCouponPopup: function() {
                return r.e("components/UsableCouponPopup/UsableCouponPopup").then(r.bind(null, "9d28"));
            },
            NoData: function() {
                return r.e("components/NoData/NoData").then(r.bind(null, "83c6"));
            }
        }, i = function() {
            var e = this, t = (e.$createElement, e._self._c, e.init && e.currentCoupon.id ? e._f("priceToFixed")(e.order.coupon_discount) : null), r = e.init && e.order.cover_discount ? e._f("priceToFixed")(e.order.cover_discount) : null, n = e.init ? e._f("priceToFixed")(e.order.redpack) : null, i = e.init && e.order.max_useable_score && e.order.score_discount ? e._f("priceToFixed")(e.order.score_discount) : null, o = e.init && 2 !== e.order.carriage_type ? e._f("priceToFixed")(e.order.carriage) : null, s = e.init ? e._f("priceToFixed")(e.order.price) : null;
            e._isMounted || (e.e0 = function(t) {
                e.couponsVisible = !0;
            }, e.e1 = function(t) {
                e.couponsVisible = !0;
            }, e.e2 = function(t) {
                e.couponsVisible = !1;
            }), e.$mp.data = Object.assign({}, {
                $root: {
                    f0: t,
                    f1: r,
                    f2: n,
                    f3: i,
                    f4: o,
                    f5: s
                }
            });
        }, o = [];
    },
    "40fe": function(e, t, r) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = s(r("fdc7")), i = s(r("ae4c")), o = r("26cb");
            function s(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            function u(e, t) {
                var r = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), r.push.apply(r, n);
                }
                return r;
            }
            function d(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var r = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? u(Object(r), !0).forEach(function(t) {
                        c(e, t, r[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : u(Object(r)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(r, t));
                    });
                }
                return e;
            }
            function c(e, t, r) {
                return t in e ? Object.defineProperty(e, t, {
                    value: r,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = r, e;
            }
            var a = {
                mixins: [ n.default ],
                components: {
                    SplitLine: function() {
                        r.e("components/SplitLine/index").then(function() {
                            return resolve(r("dc06"));
                        }.bind(null, r)).catch(r.oe);
                    },
                    IButton: function() {
                        r.e("components/Button/index").then(function() {
                            return resolve(r("1f9a"));
                        }.bind(null, r)).catch(r.oe);
                    }
                },
                data: function() {
                    return {
                        address: {},
                        order: {
                            is_use_redpack: "unselect"
                        },
                        isNeedAddress: 0,
                        skus: [],
                        seckillId: "",
                        groupPriceUuid: "",
                        products: [],
                        remark: "",
                        init: !1,
                        couponsVisible: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        currentCoupon: {},
                        current: 0,
                        animationData: {},
                        animationData2: {},
                        coverType: "",
                        _source: ""
                    };
                },
                computed: d(d({}, (0, o.mapGetters)([ "token" ])), {}, {
                    totalPrice: function() {
                        return this.order.price;
                    }
                }),
                onLoad: function(t) {
                    var r = this;
                    this.seckillId = t.seckillId, this.groupPriceUuid = t.groupPriceUuid, this.coverType = t.coverType, 
                    this.skus = JSON.parse(t.skus), this._source = t._source, e.$on("selectAddress", function(e) {
                        r.address = e, r.getOrderInfo();
                    });
                },
                onUnload: function() {
                    e.$off("selectAddress", function(e) {
                        console.log("移除 selectAddress 自定义事件");
                    });
                },
                onShow: function() {
                    this.token && !this.address.id && this.getOrderInfo();
                },
                methods: {
                    redpackSwitchChange: function(e) {
                        this.order.is_use_redpack = e.detail.value ? 1 : 0, this.getOrderInfo();
                    },
                    scoreSwitchChange: function(e) {
                        this.order.is_use_score = e.detail.value ? 1 : 0, this.getOrderInfo();
                    },
                    couponChange: function(e) {
                        e.id === this.currentCoupon.id ? (this.currentCoupon = {}, this.getOrderInfo()) : (this.currentCoupon = e, 
                        this.getOrderInfo());
                    },
                    createOrder: function() {
                        e.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.store", {
                            payment: "miniapp",
                            skus: this.skus,
                            coupon_id: this.currentCoupon && this.currentCoupon.id,
                            seckill_id: this.seckillId,
                            group_price_uuid: this.groupPriceUuid,
                            address_id: this.address && this.address.id,
                            remark: this.remark,
                            cover_type: this.coverType,
                            is_use_redpack: this.order.is_use_redpack,
                            is_use_score: this.order.is_use_score,
                            _source: this._source
                        }).then(function(t) {
                            var r = t.data, n = t.data.order;
                            if (!r.is_need_pay) return e.showToast({
                                title: "支付成功，正在跳转~",
                                icon: "none"
                            }), setTimeout(function() {
                                e.redirectTo({
                                    url: "/pages/orderDetail/index?uuid=" + n.uuid
                                });
                            }, 1500), !1;
                            i.default.pay(d(d({}, r), {}, {
                                pay_type: r.pay_type,
                                success: function() {
                                    e.showToast({
                                        title: "支付成功",
                                        icon: "none"
                                    }), setTimeout(function() {
                                        e.redirectTo({
                                            url: "/pages/orderList/index?status=deliver_pending"
                                        });
                                    }, 1500);
                                },
                                fail: function() {
                                    e.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.redirectTo({
                                        url: "/pages/orderDetail/index?uuid=" + n.uuid
                                    });
                                }
                            })), e.hideLoading();
                        }).catch(function(t) {
                            e.hideLoading();
                        });
                    },
                    payOrder: function() {
                        var t = this;
                        !this.isNeedAddress || this.address.id ? e.requestSubscribeMessage({
                            tmplIds: [ this.miniappSubscribeIds.order_delivered, this.miniappSubscribeIds.order_paid ],
                            complete: function(e) {
                                t.createOrder();
                            }
                        }) : e.showToast({
                            title: "未添加收货地址",
                            icon: "none"
                        });
                    },
                    getOrderInfo: function() {
                        var t = this;
                        e.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.preview.store", {
                            skus: this.skus,
                            address_id: this.address && this.address.id,
                            seckill_id: this.seckillId,
                            group_price_uuid: this.groupPriceUuid,
                            coupon_id: this.currentCoupon.id,
                            cover_type: this.coverType,
                            is_use_redpack: this.order.is_use_redpack,
                            is_use_score: this.order.is_use_score ? 1 : 0
                        }).then(function(r) {
                            e.hideLoading(), t.init = !0, t.order = r.data.order, t.products = r.data.order.skus, 
                            t.unusableCoupons = r.data.order.coupons.unusable, t.usableCoupons = r.data.order.coupons.usable, 
                            t.address = r.data.address, t.isNeedAddress = r.data.is_need_address, r.data.tips && e.showModal({
                                content: r.data.tips,
                                title: "温馨提示"
                            });
                        });
                    }
                }
            };
            t.default = a;
        }).call(this, r("543d").default);
    },
    "4fb6": function(e, t, r) {
        var n = r("e95a");
        r.n(n).a;
    },
    5673: function(e, t, r) {
        r.r(t);
        var n = r("1506"), i = r("6400");
        for (var o in i) "default" !== o && function(e) {
            r.d(t, e, function() {
                return i[e];
            });
        }(o);
        r("4fb6");
        var s = r("f0c5"), u = Object(s.a)(i.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        t.default = u.exports;
    },
    6400: function(e, t, r) {
        r.r(t);
        var n = r("40fe"), i = r.n(n);
        for (var o in n) "default" !== o && function(e) {
            r.d(t, e, function() {
                return n[e];
            });
        }(o);
        t.default = i.a;
    },
    dd3b: function(e, t, r) {
        (function(e) {
            r("f868"), n(r("66fd"));
            var t = n(r("5673"));
            function n(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = r, e(t.default);
        }).call(this, r("543d").createPage);
    },
    e95a: function(e, t, r) {}
}, [ [ "dd3b", "common/runtime", "common/vendor" ] ] ]);