(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/lotteryTicket/components/TicketItem" ], {
    "0dbb": function(e, t, n) {
        n.r(t);
        var o = n("e238"), i = n("377a");
        for (var s in i) "default" !== s && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(s);
        n("4e2b");
        var c = n("f0c5"), a = Object(c.a)(i.default, o.b, o.c, !1, null, "c34eadca", null, !1, o.a, void 0);
        t.default = a.exports;
    },
    "377a": function(e, t, n) {
        n.r(t);
        var o = n("91ae"), i = n.n(o);
        for (var s in o) "default" !== s && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(s);
        t.default = i.a;
    },
    "4e2b": function(e, t, n) {
        var o = n("8e70");
        n.n(o).a;
    },
    "8e70": function(e, t, n) {},
    "91ae": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }(n("a34a"));
            function i(e, t, n, o, i, s, c) {
                try {
                    var a = e[s](c), r = a.value;
                } catch (e) {
                    return void n(e);
                }
                a.done ? t(r) : Promise.resolve(r).then(o, i);
            }
            var s = {
                components: {},
                data: function() {
                    return {
                        isAddressListening: !1
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: {
                    sku: function() {
                        return this.info.sku;
                    }
                },
                onLoad: function() {
                    return function(e) {
                        return function() {
                            var t = this, n = arguments;
                            return new Promise(function(o, s) {
                                var c = e.apply(t, n);
                                function a(e) {
                                    i(c, o, s, a, r, "next", e);
                                }
                                function r(e) {
                                    i(c, o, s, a, r, "throw", e);
                                }
                                a(void 0);
                            });
                        };
                    }(o.default.mark(function e() {
                        return o.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                beforeDestroy: function() {
                    e.$off("selectAddress");
                },
                methods: {
                    toDetail: function() {
                        1 === this.info.status && e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    checkPrize: function() {
                        if (!this.sku) return e.showModal({
                            title: "无奖品",
                            content: "未查到任何奖品~"
                        }), !1;
                        "score" === this.sku.type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "redpack" === this.sku.type ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : "coupon" === this.sku.type ? e.navigateTo({
                            url: "/pages/couponDetail/index?uuid=" + this.sku.coupon.uuid
                        }) : "custom" === this.sku.type && e.navigateTo({
                            url: "/pages/orderDetail/index?uuid=" + this.info.order.uuid
                        });
                    },
                    getPrize: function() {
                        var t = this;
                        "custom" !== this.sku.type || this.sku.is_picked || e.showModal({
                            title: "领取实物奖品",
                            content: "是否填写邮寄地址发货？",
                            success: function(n) {
                                n.confirm && (e.showToast({
                                    title: "yes"
                                }), t.isAddressListening || (e.$once("selectAddress", function(n) {
                                    t.isAddressListening = !1, n.id && t.$http("/lottery-tickets/".concat(t.info.uuid, "/pick-prize"), "POST", {
                                        address_id: n.id
                                    }).then(function(n) {
                                        t.$emit("refresh"), e.showToast({
                                            title: "领取成功~",
                                            icon: "none"
                                        });
                                    });
                                }), t.isAddressListening = !0), e.navigateTo({
                                    url: "/pages/myAddress/index?select=true"
                                }));
                            }
                        });
                    }
                },
                filters: {}
            };
            t.default = s;
        }).call(this, n("543d").default);
    },
    e238: function(e, t, n) {
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {});
        var o = function() {
            var e = this, t = (e.$createElement, e._self._c, e.$tool.formatDate(e.info.created_at, "MM-dd hh:mm"));
            e.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/lotteryTicket/components/TicketItem-create-component", {
    "pages/lotteryTicket/components/TicketItem-create-component": function(e, t, n) {
        n("543d").createComponent(n("0dbb"));
    }
}, [ [ "pages/lotteryTicket/components/TicketItem-create-component" ] ] ]);