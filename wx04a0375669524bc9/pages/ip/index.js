(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/ip/index" ], {
    2765: function(n, t, e) {},
    "2d3f": function(n, t, e) {
        var a = e("4b84");
        e.n(a).a;
    },
    "4b84": function(n, t, e) {},
    "5cb9": function(n, t, e) {
        (function(n) {
            e("f868"), a(e("66fd"));
            var t = a(e("a830"));
            function a(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = e, n(t.default);
        }).call(this, e("543d").createPage);
    },
    "5d58": function(n, t, e) {
        e.d(t, "b", function() {
            return o;
        }), e.d(t, "c", function() {
            return i;
        }), e.d(t, "a", function() {
            return a;
        });
        var a = {
            Banner: function() {
                return e.e("components/Banner/Banner").then(e.bind(null, "3d80"));
            },
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "83c6"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    "75bd": function(n, t, e) {
        var a = e("2765");
        e.n(a).a;
    },
    a4b5: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var a = function(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }(e("a34a"));
            function o(n, t, e, a, o, i, u) {
                try {
                    var r = n[i](u), c = r.value;
                } catch (n) {
                    return void e(n);
                }
                r.done ? t(c) : Promise.resolve(c).then(a, o);
            }
            var i = {
                components: {},
                data: function() {
                    return {
                        ipList: [],
                        setting: {}
                    };
                },
                computed: {
                    banner: function() {
                        return this.setting.banner || [];
                    }
                },
                onLoad: function(n) {
                    var t = this;
                    return function(n) {
                        return function() {
                            var t = this, e = arguments;
                            return new Promise(function(a, i) {
                                var u = n.apply(t, e);
                                function r(n) {
                                    o(u, a, i, r, c, "next", n);
                                }
                                function c(n) {
                                    o(u, a, i, r, c, "throw", n);
                                }
                                r(void 0);
                            });
                        };
                    }(a.default.mark(function n() {
                        return a.default.wrap(function(n) {
                            for (;;) switch (n.prev = n.next) {
                              case 0:
                                t.$http("/ip/categories").then(function(n) {
                                    t.ipList = n.data.list;
                                }), t.$http("/setting/ip_list").then(function(n) {
                                    t.setting = n.data.setting;
                                });

                              case 2:
                              case "end":
                                return n.stop();
                            }
                        }, n);
                    }))();
                },
                methods: {
                    toList: function(t) {
                        n.navigateTo({
                            url: "/pages/search/index?type=all&category_id=".concat(t.id, "&title=").concat(t.title)
                        });
                    }
                }
            };
            t.default = i;
        }).call(this, e("543d").default);
    },
    a830: function(n, t, e) {
        e.r(t);
        var a = e("5d58"), o = e("be0a");
        for (var i in o) "default" !== i && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(i);
        e("75bd"), e("2d3f");
        var u = e("f0c5"), r = Object(u.a)(o.default, a.b, a.c, !1, null, "8cf08886", null, !1, a.a, void 0);
        t.default = r.exports;
    },
    be0a: function(n, t, e) {
        e.r(t);
        var a = e("a4b5"), o = e.n(a);
        for (var i in a) "default" !== i && function(n) {
            e.d(t, n, function() {
                return a[n];
            });
        }(i);
        t.default = o.a;
    }
}, [ [ "5cb9", "common/runtime", "common/vendor" ] ] ]);