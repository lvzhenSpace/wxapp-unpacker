(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/code/index" ], {
    "20b1": function(e, o, t) {},
    "27f6": function(e, o, t) {},
    "62cb": function(e, o, t) {
        t.r(o);
        var n = t("cb6c"), c = t.n(n);
        for (var i in n) "default" !== i && function(e) {
            t.d(o, e, function() {
                return n[e];
            });
        }(i);
        o.default = c.a;
    },
    "707e": function(e, o, t) {
        t.r(o);
        var n = t("ad63e"), c = t("62cb");
        for (var i in c) "default" !== i && function(e) {
            t.d(o, e, function() {
                return c[e];
            });
        }(i);
        t("f7f0"), t("ecb6");
        var a = t("f0c5"), d = Object(a.a)(c.default, n.b, n.c, !1, null, "650d2821", null, !1, n.a, void 0);
        o.default = d.exports;
    },
    8139: function(e, o, t) {
        (function(e) {
            t("f868"), n(t("66fd"));
            var o = n(t("707e"));
            function n(e) {
                return e && e.__esModule ? e : {
                    default: e
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = t, e(o.default);
        }).call(this, t("543d").createPage);
    },
    ad63e: function(e, o, t) {
        t.d(o, "b", function() {
            return n;
        }), t.d(o, "c", function() {
            return c;
        }), t.d(o, "a", function() {});
        var n = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
    },
    cb6c: function(e, o, t) {
        (function(e) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var t = {
                components: {},
                data: function() {
                    return {
                        code: "",
                        info: {}
                    };
                },
                computed: {},
                onLoad: function(e) {
                    this.code = e.code || "";
                },
                methods: {
                    useCode: function() {
                        var o = this;
                        e.showLoading({
                            title: "兑换中"
                        }), this.$http("/code/use", "POST", {
                            code: this.code
                        }).then(function(t) {
                            e.hideLoading(), e.showToast({
                                title: "兑换成功，跳转中~",
                                icon: "none"
                            }), setTimeout(function(t) {
                                "score" == o.info.code_type ? e.navigateTo({
                                    url: "/pages/myScore/index"
                                }) : "redpack" == o.info.code_type ? e.navigateTo({
                                    url: "/pages/myRedpack/index"
                                }) : "show_card" == o.info.code_type || "exclude_card" == o.info.code_type ? e.navigateTo({
                                    url: "/pages/myCard/index"
                                }) : "chip" == o.info.code_type ? e.navigateTo({
                                    url: "/pages/myChip/index"
                                }) : "product" == o.info.code_type && e.navigateTo({
                                    url: "/pages/myBox/index"
                                });
                            }, 1500);
                        });
                    },
                    submit: function() {
                        var o = this;
                        if (this.code.length < 4 || this.code.length > 10) return e.showToast({
                            title: "兑换码长度应在4到10个字符之间~",
                            icon: "none"
                        }), !1;
                        e.showLoading({
                            title: "校验中"
                        }), this.$http("/code/check", "POST", {
                            code: this.code
                        }).then(function(t) {
                            e.hideLoading();
                            var n = t.data;
                            if (o.info = n, "coupon" == n.code_type) e.navigateTo({
                                url: "/pages/couponDetail/index?uuid=" + n.coupon.uuid + "&code=" + o.code
                            }); else {
                                var c = "";
                                "score" === n.code_type ? c = "此兑换码可兑换".concat(n.score).concat(o.scoreAlias) : "redpack" === n.code_type ? c = "此兑换码可兑换".concat(n.redpack / 100, "元余额") : "exclude_card" === n.code_type ? c = "此兑换码可兑换".concat(n.prize_total, "张排除卡") : "show_card" === n.code_type ? c = "此兑换码可兑换".concat(n.prize_total, "张透视卡") : "chip" === n.code_type ? c = "此兑换码可兑换".concat(n.prize_total, '块"') + n.chip.title + '"' : "product" === n.code_type && (c = '兑换 "'.concat(n.product.title, '" 1件')), 
                                e.showModal({
                                    title: "兑换提示",
                                    content: c,
                                    confirmText: "立即兑换",
                                    success: function(e) {
                                        e.confirm && o.useCode();
                                    }
                                });
                            }
                        });
                    }
                }
            };
            o.default = t;
        }).call(this, t("543d").default);
    },
    ecb6: function(e, o, t) {
        var n = t("20b1");
        t.n(n).a;
    },
    f7f0: function(e, o, t) {
        var n = t("27f6");
        t.n(n).a;
    }
}, [ [ "8139", "common/runtime", "common/vendor" ] ] ]);