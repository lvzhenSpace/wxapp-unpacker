(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/resale/detail" ], {
    "2ce3": function(t, e, n) {
        (function(t) {
            n("f868"), i(n("66fd"));
            var e = i(n("8e66"));
            function i(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = n, t(e.default);
        }).call(this, n("543d").createPage);
    },
    "2cf5": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }(n("ae4c"));
            function o(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(t);
                    e && (i = i.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, i);
                }
                return n;
            }
            function a(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? o(Object(n), !0).forEach(function(e) {
                        r(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : o(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            function r(t, e, n) {
                return e in t ? Object.defineProperty(t, e, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[e] = n, t;
            }
            var c = {
                components: {},
                data: function() {
                    return {
                        uuid: "",
                        info: {
                            user: {}
                        },
                        user: {}
                    };
                },
                filters: {},
                computed: {
                    skus: function() {
                        return this.info.package_skus || [];
                    },
                    sku: function() {
                        return this.info.package_skus && this.info.package_skus[0] || {};
                    },
                    share: function() {
                        return {
                            title: this.info.user.name + "向你转售了一个商品，快来看看吧~"
                        };
                    },
                    detailImages: function() {
                        return this.sku.detail_images;
                    }
                },
                onLoad: function(t) {
                    this.uuid = t.uuid;
                },
                onShow: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        var t = this;
                        this.$http("/asset/resales/".concat(this.uuid)).then(function(e) {
                            t.info = e.data.info, t.user = e.data.info.user;
                        });
                    },
                    buy: function() {
                        var e = this;
                        t.showLoading({
                            title: "购买中"
                        }), this.$http("/asset/resale-order/confirm", "POST", {
                            resale_id: this.info.id
                        }).then(function(n) {
                            t.hideLoading();
                            var o = n.data;
                            n.data.is_need_pay ? i.default.pay(a(a({}, o), {}, {
                                success: function() {
                                    e.initData(), t.showToast({
                                        title: "购买成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.redirectTo({
                                            url: "/pages/myBox/index"
                                        });
                                    }, 1500);
                                },
                                fail: function() {
                                    t.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.$http("/orders/".concat(o.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : (t.showToast({
                                title: "购买成功~",
                                icon: "none"
                            }), e.initData());
                        });
                    },
                    cancel: function() {
                        var e = this;
                        t.showModal({
                            title: "提示",
                            content: "确认要取消此转售吗?",
                            success: function(n) {
                                n.confirm && (t.showLoading({
                                    title: "取消中"
                                }), e.$http("/asset/resales/".concat(e.uuid), "PUT", {
                                    type: "close"
                                }).then(function(n) {
                                    t.hideLoading(), t.showToast({
                                        title: "取消成功~",
                                        icon: "none"
                                    }), e.initData();
                                }));
                            }
                        });
                    },
                    toResaleIndex: function() {
                        t.navigateTo({
                            url: "/pages/resale/index"
                        });
                    }
                }
            };
            e.default = c;
        }).call(this, n("543d").default);
    },
    "2ee5": function(t, e, n) {},
    "30c0": function(t, e, n) {
        var i = n("2ee5");
        n.n(i).a;
    },
    "3ac3": function(t, e, n) {
        n.r(e);
        var i = n("2cf5"), o = n.n(i);
        for (var a in i) "default" !== a && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        e.default = o.a;
    },
    "8e66": function(t, e, n) {
        n.r(e);
        var i = n("c859"), o = n("3ac3");
        for (var a in o) "default" !== a && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        n("30c0"), n("b93c");
        var r = n("f0c5"), c = Object(r.a)(o.default, i.b, i.c, !1, null, "5140eb88", null, !1, i.a, void 0);
        e.default = c.exports;
    },
    a9c5: function(t, e, n) {},
    b93c: function(t, e, n) {
        var i = n("a9c5");
        n.n(i).a;
    },
    c859: function(t, e, n) {
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "f149"));
            }
        }, o = function() {
            var t = this, e = (t.$createElement, t._self._c, t.info.id ? t.$tool.showShortTime(t.info.created_at) : null);
            t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, a = [];
    }
}, [ [ "2ce3", "common/runtime", "common/vendor" ] ] ]);