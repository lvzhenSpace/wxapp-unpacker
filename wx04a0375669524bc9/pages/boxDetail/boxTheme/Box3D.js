(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/boxDetail/boxTheme/Box3D" ], {
    "0bc5": function(t, o, e) {
        e.d(o, "b", function() {
            return n;
        }), e.d(o, "c", function() {
            return i;
        }), e.d(o, "a", function() {});
        var n = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    "129c": function(t, o, e) {
        var n = e("db86");
        e.n(n).a;
    },
    "2bd0": function(t, o, e) {
        (function(t) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var e = {
                props: {
                    room: {
                        type: Object
                    },
                    info: {
                        type: Object
                    }
                },
                data: function() {
                    return {
                        selected: -1,
                        isShowClickTips: !1
                    };
                },
                created: function() {
                    this.isShowClickTips = !t.getStorageSync("disableShowClickTips");
                },
                computed: {
                    config: function() {
                        return this.$store.getters.setting.box_room;
                    },
                    boxBottomImage: function() {
                        return this.info.big_box_bottom || this.config.box_bottom || "https://cdn2.hquesoft.com/box/bigBox/3d-bg.png";
                    },
                    boxTopImage: function() {
                        return this.info.big_box_top || this.config.box_up || "https://cdn2.hquesoft.com/box/bigBox/3d-bg-up.png";
                    },
                    smallBoxImage: function() {
                        return this.info.small_box_image || this.config.small_box || "https://cdn2.hquesoft.com/box/bigBox/small-box.png";
                    }
                },
                methods: {
                    selectItem: function(o) {
                        this.isShowClickTips && (this.isShowClickTips = !1, t.setStorageSync("disableShowClickTips", !0)), 
                        this.selected !== o ? (this.selected = o, this.$playAudio("check")) : this.$emit("select", o);
                    }
                }
            };
            o.default = e;
        }).call(this, e("543d").default);
    },
    "3aec": function(t, o, e) {
        e.r(o);
        var n = e("0bc5"), i = e("ebc6");
        for (var c in i) "default" !== c && function(t) {
            e.d(o, t, function() {
                return i[t];
            });
        }(c);
        e("129c");
        var s = e("f0c5"), a = Object(s.a)(i.default, n.b, n.c, !1, null, "a4bcb8da", null, !1, n.a, void 0);
        o.default = a.exports;
    },
    db86: function(t, o, e) {},
    ebc6: function(t, o, e) {
        e.r(o);
        var n = e("2bd0"), i = e.n(n);
        for (var c in n) "default" !== c && function(t) {
            e.d(o, t, function() {
                return n[t];
            });
        }(c);
        o.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/boxDetail/boxTheme/Box3D-create-component", {
    "pages/boxDetail/boxTheme/Box3D-create-component": function(t, o, e) {
        e("543d").createComponent(e("3aec"));
    }
}, [ [ "pages/boxDetail/boxTheme/Box3D-create-component" ] ] ]);