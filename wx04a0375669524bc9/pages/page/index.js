(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/page/index" ], {
    "03c2": function(n, e, t) {
        (function(n) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var t = {
                components: {},
                data: function() {
                    return {
                        uuid: "",
                        info: {},
                        refreshCounter: 1
                    };
                },
                computed: {
                    page: function() {
                        return this.info.content || {};
                    }
                },
                watch: {},
                onLoad: function(n) {
                    this.uuid = n.uuid, this.initData();
                },
                onShow: function() {
                    this.refreshCounter++;
                },
                methods: {
                    initData: function() {
                        var e = this;
                        n.showLoading({
                            title: "加载中"
                        }), this.$http("/pages/".concat(this.uuid)).then(function(t) {
                            console.log(t), e.info = t.data.info, n.hideLoading();
                        });
                    }
                },
                onPageScroll: function(n) {}
            };
            e.default = t;
        }).call(this, t("543d").default);
    },
    "1e4e": function(n, e, t) {
        (function(n) {
            t("f868"), o(t("66fd"));
            var e = o(t("f8e1"));
            function o(n) {
                return n && n.__esModule ? n : {
                    default: n
                };
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = t, n(e.default);
        }).call(this, t("543d").createPage);
    },
    "38a3": function(n, e, t) {},
    a3fe: function(n, e, t) {
        var o = t("38a3");
        t.n(o).a;
    },
    ad0c: function(n, e, t) {
        t.r(e);
        var o = t("03c2"), a = t.n(o);
        for (var u in o) "default" !== u && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(u);
        e.default = a.a;
    },
    dca4: function(n, e, t) {
        t.d(e, "b", function() {
            return a;
        }), t.d(e, "c", function() {
            return u;
        }), t.d(e, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/PageRender/PageRender") ]).then(t.bind(null, "e85b"));
            }
        }, a = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    f8e1: function(n, e, t) {
        t.r(e);
        var o = t("dca4"), a = t("ad0c");
        for (var u in a) "default" !== u && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(u);
        t("a3fe");
        var i = t("f0c5"), c = Object(i.a)(a.default, o.b, o.c, !1, null, "7a35360e", null, !1, o.a, void 0);
        e.default = c.exports;
    }
}, [ [ "1e4e", "common/runtime", "common/vendor" ] ] ]);