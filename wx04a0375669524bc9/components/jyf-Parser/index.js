var t = require("../../@babel/runtime/helpers/typeof");

(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/jyf-Parser/index" ], {
    "0f89": function(e, n, i) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = i("961b").parseHtmlSync, r = getApp().parserCache = {}, a = i("abb8");
            var s = i("f60d");
            function l(t) {
                if (0 != t.indexOf("http")) return t;
                for (var e = "", n = 0; n < t.length && (e += Math.random() >= .5 ? t[n].toUpperCase() : t[n].toLowerCase(), 
                "/" != t[n] || "/" == t[n - 1] || "/" == t[n + 1]); n++) ;
                return e += t.substring(n + 1);
            }
            var c = {
                name: "parser",
                data: function() {
                    return {
                        showAnimation: "",
                        controls: {},
                        nodes: []
                    };
                },
                components: {
                    trees: function() {
                        i.e("components/jyf-Parser/trees").then(function() {
                            return resolve(i("e236"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                props: {
                    html: {
                        type: null,
                        default: null
                    },
                    autocopy: {
                        type: Boolean,
                        default: !0
                    },
                    autopause: {
                        type: Boolean,
                        default: !0
                    },
                    autopreview: {
                        type: Boolean,
                        default: !0
                    },
                    autosetTitle: {
                        type: Boolean,
                        default: !0
                    },
                    domain: {
                        type: String,
                        default: null
                    },
                    imgMode: {
                        type: String,
                        default: "default"
                    },
                    lazyLoad: {
                        type: Boolean,
                        default: !1
                    },
                    selectable: {
                        type: Boolean,
                        default: !1
                    },
                    tagStyle: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    showWithAnimation: {
                        type: Boolean,
                        default: !1
                    },
                    useAnchor: {
                        type: Boolean,
                        default: !1
                    },
                    useCache: {
                        type: Boolean,
                        default: !1
                    }
                },
                watch: {
                    html: function(t) {
                        this.setContent(t, void 0, !0);
                    }
                },
                mounted: function() {
                    this.imgList = [], this.imgList.each = function(t) {
                        for (var e = 0; e < this.length; e++) {
                            var n = t(this[e], e, this);
                            n && (this.includes(n) ? this[e] = l(n) : this[e] = n);
                        }
                    }, this.setContent(this.html, void 0, !0);
                },
                methods: {
                    setContent: function(n, i, c) {
                        var d = this;
                        if ("object" == t(i)) for (var u in i) this[u = u.replace(/-(\w)/g, function(t, e) {
                            return e.toUpperCase();
                        })] = i[u];
                        if (this.showWithAnimation && (this.showAnimation = "transition:400ms ease 0ms;transition-property:transform,opacity;transform-origin:50% 50% 0;-webkit-transition:400ms ease 0ms;-webkit-transform:;-webkit-transition-property:transform,opacity;-webkit-transform-origin:50% 50% 0;opacity: 1"), 
                        n) if ("string" == typeof n) {
                            if (this.useCache) {
                                var f = function(t) {
                                    for (var e = t.length, n = 5381; e--; ) n += (n << 5) + t.charCodeAt(e);
                                    return n;
                                }(n);
                                r[f] ? this.nodes = r[f] : (this.nodes = o(n, this), r[f] = this.nodes);
                            } else this.nodes = o(n, this);
                            this.$emit("parse", this.nodes);
                        } else if ("[object Array]" == Object.prototype.toString.call(n)) {
                            if (this.nodes = c ? [] : n, n.length && "Parser" != n[0].PoweredBy) {
                                var h = {
                                    _imgNum: 0,
                                    _videoNum: 0,
                                    _audioNum: 0,
                                    _domain: this.domain,
                                    _protocol: this.domain ? this.domain.includes("://") ? this.domain.split("://")[0] : "http" : void 0,
                                    _STACK: [],
                                    CssHandler: new a(this.tagStyle)
                                };
                                h.CssHandler.getStyle(""), function t(e) {
                                    for (var n, i = 0; n = e[i++]; ) if ("text" != n.type) {
                                        for (var o in n.attrs = n.attrs || {}, n.attrs) s.trustAttrs[o] ? "string" != typeof n.attrs[o] && (n.attrs[o] = n.attrs[o].toString()) : n.attrs[o] = void 0;
                                        s.LabelAttrsHandler(n, h), s.blockTags[n.name] ? n.name = "div" : s.trustTags[n.name] || (n.name = "span"), 
                                        n.children && n.children.length ? (h._STACK.push(n), t(n.children), h._STACK.pop()) : n.children = void 0;
                                    }
                                }(n), this.nodes = n;
                            }
                        } else {
                            if ("object" != t(n) || !n.nodes) return this.$emit("error", {
                                source: "parse",
                                errMsg: "传入的nodes数组格式不正确！应该传入的类型是array，实际传入的类型是：" + t(n.nodes)
                            });
                            this.nodes = n.nodes, console.warn("Parser 类型错误：object 类型已废弃，请直接将 html 设置为 object.nodes （array 类型）");
                        } else {
                            if (c) return;
                            this.nodes = [];
                        }
                        this.$nextTick(function() {
                            d.imgList.length = 0, d.videoContexts = [];
                            (function t(n) {
                                for (var i = function() {
                                    var i = n[o];
                                    if ("trees" == i.$options.name) for (r = !1, a = i.nodes.length; s = i.nodes[--a]; ) s.c || ("img" == s.name ? (s.attrs.src && s.attrs.i && (-1 == d.imgList.indexOf(s.attrs.src) ? d.imgList[s.attrs.i] = s.attrs.src : d.imgList[s.attrs.i] = l(s.attrs.src)), 
                                    r || (r = !0, d.lazyLoad && e.createIntersectionObserver ? (i._observer && i._observer.disconnect(), 
                                    i._observer = e.createIntersectionObserver(i), i._observer.relativeToViewport({
                                        top: 1e3,
                                        bottom: 1e3
                                    }).observe("._img", function(t) {
                                        i.imgLoad = !0, i._observer.disconnect(), i._observer = null;
                                    })) : i.imgLoad = !0)) : "video" == s.name ? ((c = e.createVideoContext(s.attrs.id, i)).id = s.attrs.id, 
                                    d.videoContexts.push(c)) : "audio" == s.name && s.attrs.autoplay ? wx.createAudioContext(s.attrs.id, i).play() : "title" == s.name && d.autosetTitle && "text" == s.children[0].type && s.children[0].text && e.setNavigationBarTitle({
                                        title: s.children[0].text
                                    }));
                                    i.$children.length && t(i.$children);
                                }, o = n.length; o--; ) {
                                    var r, a, s, c;
                                    i();
                                }
                            })(d.$children), e.createSelectorQuery().in(d).select("._contain").boundingClientRect(function(t) {
                                d.$emit("ready", t);
                            }).exec();
                        });
                    },
                    getText: function() {
                        var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], e = "", n = function n(i) {
                            if ("text" == i.type) return e += i.text;
                            if (t && (("p" == i.name || "div" == i.name || "tr" == i.name || "li" == i.name || /h[1-6]/.test(i.name)) && e && "\n" != e[e.length - 1] || "br" == i.name) && (e += "\n"), 
                            i.children) for (var o = 0; o < i.children.length; o++) n(i.children[o]);
                            t && ("p" == i.name || "div" == i.name || "tr" == i.name || "li" == i.name || /h[1-6]/.test(i.name)) && e && "\n" != e[e.length - 1] ? e += "\n" : t && "td" == i.name && (e += "\t");
                        }, i = this.nodes && this.nodes.length ? this.nodes : this.html[0] && (this.html[0].name || this.html[0].type) ? this.html : [];
                        if (!i.length) return "";
                        for (var o = 0; o < this.data.html.length; o++) n(this.data.html[o]);
                        return e;
                    },
                    navigateTo: function(t) {
                        var n = this, i = function(i, o) {
                            var r = e.createSelectorQuery().in(o || n);
                            r.select(i).boundingClientRect(), r.selectViewport().scrollOffset(), r.exec(function(n) {
                                if (!n || !n[0]) return t.fail ? t.fail({
                                    errMsg: "Label Not Found"
                                }) : null;
                                e.pageScrollTo({
                                    scrollTop: n[1].scrollTop + n[0].top,
                                    success: t.success,
                                    fail: t.fail
                                });
                            });
                        };
                        t.id ? i("._contain >>> #" + t.id + ", ._contain >>> ." + t.id) : i("._contain");
                    },
                    getVideoContext: function(t) {
                        if (!t) return this.videoContexts;
                        for (var e = this.videoContexts.length; e--; ) if (this.videoContexts[e].id == t) return this.videoContexts[e];
                        return null;
                    }
                }
            };
            n.default = c;
        }).call(this, i("543d").default);
    },
    "2d60": function(t, e, n) {
        n.r(e);
        var i = n("66b6"), o = n("504b");
        for (var r in o) "default" !== r && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(r);
        n("31de");
        var a = n("f0c5"), s = Object(a.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = s.exports;
    },
    "31de": function(t, e, n) {
        var i = n("9cdf");
        n.n(i).a;
    },
    "504b": function(t, e, n) {
        n.r(e);
        var i = n("0f89"), o = n.n(i);
        for (var r in i) "default" !== r && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(r);
        e.default = o.a;
    },
    "66b6": function(t, e, n) {
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var i = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    "9cdf": function(t, e, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/jyf-Parser/index-create-component", {
    "components/jyf-Parser/index-create-component": function(t, e, n) {
        n("543d").createComponent(n("2d60"));
    }
}, [ [ "components/jyf-Parser/index-create-component" ] ] ]);