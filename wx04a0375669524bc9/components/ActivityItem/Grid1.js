(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ActivityItem/Grid1" ], {
    "01ff": function(t, n, e) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            props: {
                info: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                tag: {
                    type: String
                },
                theme: {
                    type: String,
                    default: function() {
                        return "default-theme";
                    }
                }
            },
            data: function() {
                return {};
            },
            computed: {
                tagString: function() {
                    return this.info && this.info.tags && this.info.tags[0] || " ";
                }
            },
            methods: {}
        };
        n.default = o;
    },
    "0664": function(t, n, e) {
        e.r(n);
        var o = e("7b76"), i = e("8718");
        for (var c in i) "default" !== c && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(c);
        e("6147");
        var r = e("f0c5"), u = Object(r.a)(i.default, o.b, o.c, !1, null, "3d2c11a6", null, !1, o.a, void 0);
        n.default = u.exports;
    },
    "278b": function(t, n, e) {},
    6147: function(t, n, e) {
        var o = e("278b");
        e.n(o).a;
    },
    "7b76": function(t, n, e) {
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return c;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, i = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
    },
    8718: function(t, n, e) {
        e.r(n);
        var o = e("01ff"), i = e.n(o);
        for (var c in o) "default" !== c && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(c);
        n.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ActivityItem/Grid1-create-component", {
    "components/ActivityItem/Grid1-create-component": function(t, n, e) {
        e("543d").createComponent(e("0664"));
    }
}, [ [ "components/ActivityItem/Grid1-create-component" ] ] ]);