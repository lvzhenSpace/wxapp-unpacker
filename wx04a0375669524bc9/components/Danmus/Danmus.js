(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/Danmus/Danmus" ], {
    "30d3": function(t, n, e) {
        var o = e("6fc2");
        e.n(o).a;
    },
    "6fc2": function(t, n, e) {},
    "7d24": function(t, n, e) {
        e.r(n);
        var o = e("f0cb"), r = e("992b");
        for (var a in r) "default" !== a && function(t) {
            e.d(n, t, function() {
                return r[t];
            });
        }(a);
        e("30d3");
        var c = e("f0c5"), u = Object(c.a)(r.default, o.b, o.c, !1, null, "0937ce10", null, !1, o.a, void 0);
        n.default = u.exports;
    },
    "992b": function(t, n, e) {
        e.r(n);
        var o = e("bb2f"), r = e.n(o);
        for (var a in o) "default" !== a && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        n.default = r.a;
    },
    bb2f: function(t, n, e) {
        (function(t) {
            function e(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    n && (o = o.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), e.push.apply(e, o);
                }
                return e;
            }
            function o(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var o = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? e(Object(o), !0).forEach(function(n) {
                        r(t, n, o[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(o)) : e(Object(o)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(o, n));
                    });
                }
                return t;
            }
            function r(t, n, e) {
                return n in t ? Object.defineProperty(t, n, {
                    value: e,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[n] = e, t;
            }
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var a = {
                name: "Danmus",
                props: {
                    list: Array,
                    uuid: String,
                    setting: Object
                },
                data: function() {
                    return {
                        danmus: [],
                        customBar: 64,
                        isShow: !1
                    };
                },
                watch: {
                    list: {
                        handler: function(t) {
                            t && this.start();
                        },
                        deep: !0
                    }
                },
                created: function() {
                    this.customBar = this.$store.getters.deviceInfo.customBar, this.start();
                },
                methods: {
                    start: function() {
                        var t = this;
                        this.danmus = [], this.isShow = !1;
                        for (var n = this.list, e = [], r = 0, a = 0; a < n.length; a++) {
                            var c = this.customBar + a % 4 * 42, u = 9 + 3 * Math.random(), i = 0 + 2 * Math.random() + a;
                            r = r > u + i ? r : u + i, e.push(o(o({}, n[a]), {}, {
                                style: "background: ".concat(n[a].bg_color, ";top:").concat(c, "px;animation-duration:").concat(u, "s;animation-delay:").concat(i, "s;")
                            }));
                        }
                        this.$forceUpdate(), setTimeout(function() {
                            t.danmus = e, t.isShow = !0;
                        }, 100), r -= 2.5, setTimeout(function() {
                            t.start(), t.$forceUpdate();
                        }, parseInt(1e3 * r));
                    },
                    clickItem: function(n) {
                        if (n.uuid === this.uuid) return !1;
                        "box" === n.type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + n.uuid
                        });
                    }
                }
            };
            n.default = a;
        }).call(this, e("543d").default);
    },
    f0cb: function(t, n, e) {
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return r;
        }), e.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/Danmus/Danmus-create-component", {
    "components/Danmus/Danmus-create-component": function(t, n, e) {
        e("543d").createComponent(e("7d24"));
    }
}, [ [ "components/Danmus/Danmus-create-component" ] ] ]);