(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/UsableCouponPopup/components/CouponItem" ], {
    1860: function(n, t, o) {
        var e = o("18c2");
        o.n(e).a;
    },
    "18c2": function(n, t, o) {},
    "48f2": function(n, t, o) {
        o.r(t);
        var e = o("c6a1"), u = o.n(e);
        for (var c in e) "default" !== c && function(n) {
            o.d(t, n, function() {
                return e[n];
            });
        }(c);
        t.default = u.a;
    },
    "5b36": function(n, t, o) {
        o.r(t);
        var e = o("750c"), u = o("48f2");
        for (var c in u) "default" !== c && function(n) {
            o.d(t, n, function() {
                return u[n];
            });
        }(c);
        o("1860");
        var a = o("f0c5"), r = Object(a.a)(u.default, e.b, e.c, !1, null, "113018e4", null, !1, e.a, void 0);
        t.default = r.exports;
    },
    "750c": function(n, t, o) {
        o.d(t, "b", function() {
            return e;
        }), o.d(t, "c", function() {
            return u;
        }), o.d(t, "a", function() {});
        var e = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    c6a1: function(n, t, o) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var e = function(n) {
            return n && n.__esModule ? n : {
                default: n
            };
        }(o("c850"));
        var u = {
            name: "CouponItem",
            props: {
                backgroundColor: {
                    type: String,
                    default: "#ffffff"
                },
                coupon: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                isSelected: {
                    type: Boolean,
                    default: function() {
                        return !1;
                    }
                },
                active: {
                    type: Number,
                    default: 1
                },
                activeText: {
                    type: String,
                    default: "立即使用"
                },
                unActiveText: {
                    type: String,
                    default: "已使用"
                }
            },
            computed: {
                baseCoupon: function() {
                    return this.coupon.base_coupon || {};
                },
                style: function() {
                    return "background-color:" + this.backgroundColor;
                },
                validDateStr: function() {
                    var n = this.coupon.usable_start_at, t = this.coupon.usable_end_at;
                    if (!t) return "";
                    var o = (0, e.default)(n), u = (0, e.default)(t), c = new Date();
                    if (o > c || u < c) return o.format("YYYY.MM.DD") + " - " + u.format("MM.DD");
                    var a = u.diff(c, "days");
                    return a < 1 ? u.diff(c, "h") + 1 + "小时后过期" : a + 1 + "天后过期";
                }
            },
            methods: {
                click: function() {
                    this.$emit("click", this.coupon);
                }
            }
        };
        t.default = u;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/UsableCouponPopup/components/CouponItem-create-component", {
    "components/UsableCouponPopup/components/CouponItem-create-component": function(n, t, o) {
        o("543d").createComponent(o("5b36"));
    }
}, [ [ "components/UsableCouponPopup/components/CouponItem-create-component" ] ] ]);