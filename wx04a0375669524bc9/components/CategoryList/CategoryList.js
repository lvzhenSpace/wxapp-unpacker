(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/CategoryList/CategoryList" ], {
    "0d9b": function(t, e, n) {
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    },
    "1b87": function(t, e, n) {
        n.r(e);
        var o = n("f5c6"), a = n.n(o);
        for (var i in o) "default" !== i && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        e.default = a.a;
    },
    "1d69": function(t, e, n) {},
    "509e1": function(t, e, n) {
        var o = n("1d69");
        n.n(o).a;
    },
    cc21: function(t, e, n) {
        n.r(e);
        var o = n("0d9b"), a = n("1b87");
        for (var i in a) "default" !== i && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(i);
        n("509e1");
        var c = n("f0c5"), r = Object(c.a)(a.default, o.b, o.c, !1, null, "7a69af76", null, !1, o.a, void 0);
        e.default = r.exports;
    },
    f5c6: function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: {
                    refreshCounter: {
                        type: Number
                    },
                    module: {
                        type: Object
                    }
                },
                data: function() {
                    return {
                        list: []
                    };
                },
                watch: {
                    refreshCounter: function() {
                        this.initData();
                    }
                },
                mounted: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        var t = this;
                        if ("custom" === this.module.list_mode) {
                            var e = this.module.list || [ 0 ];
                            this.$http("/normal/categories", "GET", {
                                per_page: 10,
                                level_mode: "all",
                                ids: e
                            }).then(function(e) {
                                t.list = e.data.list;
                            });
                        } else this.$http("/normal/categories", "GET", {
                            per_page: 10
                        }).then(function(e) {
                            t.list = e.data.list;
                        });
                    },
                    handleClick: function(e) {
                        1 === e.level ? t.navigateTo({
                            url: "/pages/category/index?category_id=".concat(e.id, "&title=").concat(e.title)
                        }) : 2 === e.level && t.navigateTo({
                            url: "/pages/search/index?category_id=".concat(e.id, "&title=").concat(e.title)
                        });
                    },
                    more: function(e) {
                        t.navigateTo({
                            url: "/pages/ip/index"
                        });
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/CategoryList/CategoryList-create-component", {
    "components/CategoryList/CategoryList-create-component": function(t, e, n) {
        n("543d").createComponent(n("cc21"));
    }
}, [ [ "components/CategoryList/CategoryList-create-component" ] ] ]);