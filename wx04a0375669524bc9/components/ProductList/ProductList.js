(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ProductList/ProductList" ], {
    "09ba": function(t, e, n) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var o = {
            props: {
                ids: {
                    type: Array
                },
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                refreshCounter: Number,
                getNextPageCounter: Number
            },
            data: function() {
                return {
                    page: 1,
                    list: []
                };
            },
            mounted: function() {
                this.initData();
            },
            computed: {
                isScroll: function() {
                    return "scroll" == this.module.theme;
                },
                grid: function() {
                    return this.module.grid || "grid3";
                },
                wrapMode: function() {
                    return this.module.wrap_mode || "wrap";
                }
            },
            watch: {
                ids: function() {
                    this.initData();
                },
                refreshCounter: function() {
                    this.initData();
                },
                getNextPageCounter: function(t) {
                    "all" === this.module.list_content && this.getNextPage();
                }
            },
            methods: {
                initData: function() {
                    var t = this;
                    "all" === this.module.list_content ? (this.page = 1, this.$http("/shop/products", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(e) {
                        t.list = e.data.list;
                    })) : this.ids && this.ids.length > 0 && this.$http("/shop/products", "GET", {
                        per_page: 80,
                        ids: this.ids
                    }).then(function(e) {
                        t.list = e.data.list;
                    });
                },
                clickItem: function() {
                    this.$playAudio("click");
                },
                getNextPage: function() {
                    var t = this;
                    this.page++, this.$http("/shop/products", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(e) {
                        t.list = t.list.concat(e.data.list);
                    });
                }
            }
        };
        e.default = o;
    },
    "3ddf9": function(t, e, n) {
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {
            return o;
        });
        var o = {
            ProductItem: function() {
                return n.e("components/ProductItem/ProductItem").then(n.bind(null, "4ed5"));
            }
        }, i = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    4081: function(t, e, n) {
        var o = n("eb96");
        n.n(o).a;
    },
    e5e7: function(t, e, n) {
        n.r(e);
        var o = n("3ddf9"), i = n("f2b0");
        for (var u in i) "default" !== u && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(u);
        n("4081");
        var r = n("f0c5"), c = Object(r.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        e.default = c.exports;
    },
    eb96: function(t, e, n) {},
    f2b0: function(t, e, n) {
        n.r(e);
        var o = n("09ba"), i = n.n(o);
        for (var u in o) "default" !== u && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(u);
        e.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ProductList/ProductList-create-component", {
    "components/ProductList/ProductList-create-component": function(t, e, n) {
        n("543d").createComponent(n("e5e7"));
    }
}, [ [ "components/ProductList/ProductList-create-component" ] ] ]);