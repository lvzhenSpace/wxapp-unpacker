(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxList/BoxList" ], {
    "3af9": function(t, n, e) {
        e.r(n);
        var o = e("66d3"), i = e.n(o);
        for (var c in o) "default" !== c && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(c);
        n.default = i.a;
    },
    5533: function(t, n, e) {},
    "66d3": function(t, n, e) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            props: {
                ids: {
                    type: Array
                },
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                refreshCounter: Number,
                getNextPageCounter: Number
            },
            components: {
                Row1: function() {
                    e.e("components/BoxItem/Row1").then(function() {
                        return resolve(e("270c"));
                    }.bind(null, e)).catch(e.oe);
                },
                Grid1: function() {
                    e.e("components/BoxItem/Grid1").then(function() {
                        return resolve(e("8c4d"));
                    }.bind(null, e)).catch(e.oe);
                },
                Grid2: function() {
                    e.e("components/BoxItem/Grid2").then(function() {
                        return resolve(e("0878"));
                    }.bind(null, e)).catch(e.oe);
                },
                Grid3: function() {
                    e.e("components/BoxItem/Grid3").then(function() {
                        return resolve(e("0034"));
                    }.bind(null, e)).catch(e.oe);
                }
            },
            data: function() {
                return {
                    page: 1,
                    list: []
                };
            },
            mounted: function() {
                this.initData();
            },
            computed: {
                isScroll: function() {
                    return "scroll" == this.module.display;
                },
                grid: function() {
                    return this.module.grid || "grid3";
                },
                wrapMode: function() {
                    return this.module.wrap_mode || "wrap";
                }
            },
            watch: {
                ids: function() {
                    this.initData();
                },
                refreshCounter: function() {
                    this.initData();
                },
                getNextPageCounter: function(t) {
                    "all" === this.module.list_content && this.getNextPage();
                }
            },
            methods: {
                initData: function() {
                    var t = this;
                    "all" === this.module.list_content ? (this.page = 1, this.$http("/boxes", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(n) {
                        t.list = n.data.list;
                    })) : this.ids && this.ids.length > 0 && this.$http("/boxes", "GET", {
                        per_page: 100,
                        ids: this.ids
                    }).then(function(n) {
                        t.list = n.data.list;
                    });
                },
                clickItem: function() {
                    this.$playAudio("click");
                },
                getNextPage: function() {
                    var t = this;
                    this.page++, this.$http("/boxes", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(n) {
                        t.list = t.list.concat(n.data.list);
                    });
                }
            }
        };
        n.default = o;
    },
    68397: function(t, n, e) {
        var o = e("5533");
        e.n(o).a;
    },
    afc6: function(t, n, e) {
        e.r(n);
        var o = e("cf11"), i = e("3af9");
        for (var c in i) "default" !== c && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(c);
        e("68397");
        var a = e("f0c5"), u = Object(a.a)(i.default, o.b, o.c, !1, null, "31ad2db6", null, !1, o.a, void 0);
        n.default = u.exports;
    },
    cf11: function(t, n, e) {
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return i;
        }), e.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxList/BoxList-create-component", {
    "components/BoxList/BoxList-create-component": function(t, n, e) {
        e("543d").createComponent(e("afc6"));
    }
}, [ [ "components/BoxList/BoxList-create-component" ] ] ]);