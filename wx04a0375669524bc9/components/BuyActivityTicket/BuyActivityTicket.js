(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BuyActivityTicket/BuyActivityTicket" ], {
    1079: function(t, n, i) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var i = {
                components: {},
                data: function() {
                    return {
                        info: {},
                        isInit: !1,
                        scorePrice: 10,
                        total: 1
                    };
                },
                props: {
                    nodeType: {
                        type: String
                    },
                    nodeUuid: {
                        type: String
                    }
                },
                computed: {},
                watch: {},
                onLoad: function(t) {},
                created: function() {
                    this.initData();
                },
                methods: {
                    switchChange: function(t) {
                        this.isUseAdvisePrice = t.detail.value ? 1 : 0;
                    },
                    initData: function() {
                        var n = this;
                        t.showLoading(), this.$http("/activity/buy-ticket/preview", "post", {
                            node_type: this.nodeType,
                            node_uuid: this.nodeUuid
                        }).then(function(i) {
                            n.isInit = !0, n.info = i.data.info, n.scorePrice = n.info.score_price, t.hideLoading();
                        });
                    },
                    cancel: function() {
                        this.$emit("cancel");
                    },
                    submit: function() {
                        var n = this;
                        this.total || t.showToast({
                            title: "请输入兑换次数",
                            icon: "none"
                        }), t.showLoading(), this.$http("/activity/buy-ticket", "post", {
                            node_type: this.nodeType,
                            node_uuid: this.nodeUuid,
                            total: this.total
                        }).then(function(i) {
                            t.hideLoading(), n.$emit("success");
                        });
                    },
                    toPage: function(n) {
                        t.navigateTo({
                            url: n
                        });
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = i;
        }).call(this, i("543d").default);
    },
    "1f4b": function(t, n, i) {
        i.d(n, "b", function() {
            return e;
        }), i.d(n, "c", function() {
            return o;
        }), i.d(n, "a", function() {});
        var e = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    "64b2": function(t, n, i) {
        var e = i("c1af");
        i.n(e).a;
    },
    "9fc05": function(t, n, i) {
        i.r(n);
        var e = i("1079"), o = i.n(e);
        for (var c in e) "default" !== c && function(t) {
            i.d(n, t, function() {
                return e[t];
            });
        }(c);
        n.default = o.a;
    },
    c1af: function(t, n, i) {},
    dc4b: function(t, n, i) {
        i.r(n);
        var e = i("1f4b"), o = i("9fc05");
        for (var c in o) "default" !== c && function(t) {
            i.d(n, t, function() {
                return o[t];
            });
        }(c);
        i("64b2");
        var a = i("f0c5"), u = Object(a.a)(o.default, e.b, e.c, !1, null, "4eb9b52c", null, !1, e.a, void 0);
        n.default = u.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BuyActivityTicket/BuyActivityTicket-create-component", {
    "components/BuyActivityTicket/BuyActivityTicket-create-component": function(t, n, i) {
        i("543d").createComponent(i("dc4b"));
    }
}, [ [ "components/BuyActivityTicket/BuyActivityTicket-create-component" ] ] ]);