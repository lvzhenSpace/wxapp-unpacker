(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SharePopup/SharePopup" ], {
    "0d02": function(e, t, n) {
        n.r(t);
        var o = n("983f"), r = n.n(o);
        for (var i in o) "default" !== i && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        t.default = r.a;
    },
    "4b48": function(e, t, n) {
        n.r(t);
        var o = n("558c"), r = n("0d02");
        for (var i in r) "default" !== i && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(i);
        n("5e0e");
        var c = n("f0c5"), a = Object(c.a)(r.default, o.b, o.c, !1, null, "4ab59b4e", null, !1, o.a, void 0);
        t.default = a.exports;
    },
    "558c": function(e, t, n) {
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    },
    "5e0e": function(e, t, n) {
        var o = n("aaa3");
        n.n(o).a;
    },
    "983f": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = n("f93e");
            function r(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function i(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? r(Object(n), !0).forEach(function(t) {
                        c(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            function c(e, t, n) {
                return t in e ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = n, e;
            }
            var a = {
                components: {
                    PosterTheme1: function() {
                        Promise.all([ n.e("common/vendor"), n.e("components/SharePopup/components/posterTheme1") ]).then(function() {
                            return resolve(n("4965"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    poster: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    }
                },
                computed: {
                    calcInfo: function() {
                        return i(i({}, this.info), {}, {
                            qrcode: o.BASE_URL + "/miniapp/qrcode?path=" + encodeURIComponent(this.info.path)
                        });
                    },
                    isCanShareToTimeLine: function() {
                        return !1;
                    },
                    isCanShareWithPoster: function() {
                        return !0;
                    }
                },
                data: function() {
                    return {
                        image: "",
                        isShowPoster: !1,
                        isAskShareType: !0
                    };
                },
                methods: {
                    shareToWechat: function() {
                        return this.close(), 0 == this.poster && this.$http("/task/add_share", "POST").then(function(e) {}).catch(function(e) {
                            console.log(e);
                        }), !1;
                    },
                    shareToTimeLine: function() {
                        var t = this;
                        e.share({
                            provider: "weixin",
                            scene: "WXSceneTimeline",
                            type: 0,
                            href: this.info.app_url,
                            title: this.info.title,
                            imageUrl: this.info.thumb,
                            summary: this.info.desc || this.info.title,
                            success: function(e) {
                                t.close();
                            },
                            fail: function(t) {
                                console.log("fail:" + JSON.stringify(t)), e.showToast({
                                    title: "分享未成功~"
                                });
                            }
                        });
                    },
                    generatePoster: function() {
                        console.log(this.info), this.isAskShareType = !1, this.isShowPoster = !0;
                    },
                    getPosterUrl: function(e) {
                        this.image = e;
                    },
                    close: function() {
                        this.$emit("close");
                    },
                    saveImg: function() {
                        e.saveImageToPhotosAlbum({
                            filePath: this.image,
                            success: function(t) {
                                e.showToast({
                                    title: "已保存到相册",
                                    icon: "none"
                                });
                            }
                        });
                    }
                }
            };
            t.default = a;
        }).call(this, n("543d").default);
    },
    aaa3: function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SharePopup/SharePopup-create-component", {
    "components/SharePopup/SharePopup-create-component": function(e, t, n) {
        n("543d").createComponent(n("4b48"));
    }
}, [ [ "components/SharePopup/SharePopup-create-component" ] ] ]);