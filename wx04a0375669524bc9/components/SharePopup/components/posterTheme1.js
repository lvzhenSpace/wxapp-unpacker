(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SharePopup/components/posterTheme1" ], {
    "1acc8": function(e, t, n) {},
    4965: function(e, t, n) {
        n.r(t);
        var o = n("7cf2"), r = n("4c18");
        for (var a in r) "default" !== a && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(a);
        n("aad1");
        var c = n("f0c5"), i = Object(c.a)(r.default, o.b, o.c, !1, null, "367b6ca8", null, !1, o.a, void 0);
        t.default = i.exports;
    },
    "4c18": function(e, t, n) {
        n.r(t);
        var o = n("f42d"), r = n.n(o);
        for (var a in o) "default" !== a && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = r.a;
    },
    "7cf2": function(e, t, n) {
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    },
    aad1: function(e, t, n) {
        var o = n("1acc8");
        n.n(o).a;
    },
    f42d: function(e, t, n) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = c(n("a34a")), r = c(n("7fce")), a = n("cedb");
        function c(e) {
            return e && e.__esModule ? e : {
                default: e
            };
        }
        function i(e, t, n, o, r, a, c) {
            try {
                var i = e[a](c), u = i.value;
            } catch (e) {
                return void n(e);
            }
            i.done ? t(u) : Promise.resolve(u).then(o, r);
        }
        var u = {
            data: function() {
                return {
                    canvasId: "default_PosterCanvasId",
                    poster: null,
                    posterPath: null
                };
            },
            components: {},
            computed: {
                displayPrice: function() {
                    var e = "";
                    return this.info && this.info.money_price && (e = this.info.money_price / 100 + "元"), 
                    this.info && this.info.score_price && (e += (e ? "+" : "") + this.info.score_price + this.scoreAlias), 
                    e || (e = ""), e;
                }
            },
            props: {
                info: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            watch: {
                info: function() {
                    this.generatePoster();
                }
            },
            mounted: function() {
                this.generatePoster();
            },
            methods: {
                generatePoster: function() {
                    var e = this;
                    return function(e) {
                        return function() {
                            var t = this, n = arguments;
                            return new Promise(function(o, r) {
                                var a = e.apply(t, n);
                                function c(e) {
                                    i(a, o, r, c, u, "next", e);
                                }
                                function u(e) {
                                    i(a, o, r, c, u, "throw", e);
                                }
                                c(void 0);
                            });
                        };
                    }(o.default.mark(function t() {
                        var n;
                        return o.default.wrap(function(t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                return t.prev = 0, t.next = 3, (0, a.getSharePoster)({
                                    _this: e,
                                    type: "testShareType",
                                    posterCanvasId: e.canvasId,
                                    delayTimeScale: 20,
                                    background: {
                                        height: 1e3,
                                        width: 700,
                                        backgroundColor: "#ffffff"
                                    },
                                    drawArray: function(t) {
                                        var n = t.bgObj;
                                        return t.type, t.bgScale, n.width, n.width, n.height, new Promise(function(t, o) {
                                            t([ {
                                                type: "image",
                                                url: e.info.thumb,
                                                dx: 0,
                                                dy: 0,
                                                infoCallBack: function(e) {
                                                    return {
                                                        dWidth: n.width,
                                                        dHeight: n.width + 225,
                                                        mode: "aspectFit"
                                                    };
                                                }
                                            }, {
                                                type: "image",
                                                url: e.info.qrcode,
                                                dx: 190,
                                                dy: n.width - 105,
                                                infoCallBack: function(e) {
                                                    return n.width, e.height, {
                                                        dWidth: 156,
                                                        dHeight: 130,
                                                        roundRectSet: {
                                                            r: 70
                                                        }
                                                    };
                                                }
                                            } ]);
                                        });
                                    },
                                    setCanvasWH: function(t) {
                                        var n = t.bgObj;
                                        t.type, t.bgScale, e.poster = n;
                                    }
                                });

                              case 3:
                                n = t.sent, e.poster.finalPath = n.poster.tempFilePath, e.posterPath = n.poster.tempFilePath, 
                                e.$emit("getPosterUrl", e.posterPath), t.next = 13;
                                break;

                              case 9:
                                t.prev = 9, t.t0 = t.catch(0), r.default.hideLoading(), r.default.showToast(JSON.stringify(t.t0));

                              case 13:
                              case "end":
                                return t.stop();
                            }
                        }, t, null, [ [ 0, 9 ] ]);
                    }))();
                }
            }
        };
        t.default = u;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SharePopup/components/posterTheme1-create-component", {
    "components/SharePopup/components/posterTheme1-create-component": function(e, t, n) {
        n("543d").createComponent(n("4965"));
    }
}, [ [ "components/SharePopup/components/posterTheme1-create-component" ] ] ]);