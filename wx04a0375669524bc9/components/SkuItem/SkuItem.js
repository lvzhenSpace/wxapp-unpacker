(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SkuItem/SkuItem" ], {
    "26e0": function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = {
                mixins: [ function(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }(e("fdc7")).default ],
                props: {
                    info: {
                        type: Object
                    },
                    disableClick: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                filters: {},
                methods: {
                    toProductDetail: function(n) {
                        if (this.disableClick) return !1;
                        var e = this.info;
                        "product" === e.product_type ? t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + e.product_uuid
                        }) : "box" === e.product_type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + e.product_uuid
                        });
                    }
                }
            };
            n.default = o;
        }).call(this, e("543d").default);
    },
    "54d5": function(t, n, e) {
        var o = e("9f6a");
        e.n(o).a;
    },
    5959: function(t, n, e) {
        e.d(n, "b", function() {
            return u;
        }), e.d(n, "c", function() {
            return a;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, u = function() {
            var t = this, n = (t.$createElement, t._self._c, t.info.attrs && t.info.attrs.length ? t._f("productAttrsToString")(t.info.attrs) : null);
            t.$mp.data = Object.assign({}, {
                $root: {
                    f0: n
                }
            });
        }, a = [];
    },
    "5fc7": function(t, n, e) {
        e.r(n);
        var o = e("26e0"), u = e.n(o);
        for (var a in o) "default" !== a && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        n.default = u.a;
    },
    "9f6a": function(t, n, e) {},
    a6d5: function(t, n, e) {
        e.r(n);
        var o = e("5959"), u = e("5fc7");
        for (var a in u) "default" !== a && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(a);
        e("54d5");
        var c = e("f0c5"), i = Object(c.a)(u.default, o.b, o.c, !1, null, "c0abeae6", null, !1, o.a, void 0);
        n.default = i.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SkuItem/SkuItem-create-component", {
    "components/SkuItem/SkuItem-create-component": function(t, n, e) {
        e("543d").createComponent(e("a6d5"));
    }
}, [ [ "components/SkuItem/SkuItem-create-component" ] ] ]);