(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/AreaSelect/AreaSelect" ], {
    "0534": function(e, n, t) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var r = {
            data: function() {
                return {
                    cityPickerValueDefault: [ 0, 0, 1 ],
                    labelArr: []
                };
            },
            props: {
                value: {
                    type: Array
                }
            },
            components: {
                simpleAddress: function() {
                    Promise.all([ t.e("common/vendor"), t.e("components/AreaSelect/simple-address") ]).then(function() {
                        return resolve(t("7ed7"));
                    }.bind(null, t)).catch(t.oe);
                }
            },
            computed: {
                pickerText: function() {
                    return this.labelArr && this.labelArr.join("-");
                }
            },
            mounted: function() {
                this.value && (this.labelArr = this.value);
            },
            watch: {
                labelArr: function() {
                    this.cityPickerValueDefault = this.$refs.simpleAddress.queryIndex(this.labelArr, "label").index;
                }
            },
            methods: {
                openAddres: function() {
                    this.$refs.simpleAddress.open();
                },
                onConfirm: function(e) {
                    this.labelArr = e.labelArr, this.$emit("change", e);
                }
            }
        };
        n.default = r;
    },
    2860: function(e, n, t) {},
    3810: function(e, n, t) {
        var r = t("2860");
        t.n(r).a;
    },
    "5db8": function(e, n, t) {
        t.r(n);
        var r = t("0534"), o = t.n(r);
        for (var c in r) "default" !== c && function(e) {
            t.d(n, e, function() {
                return r[e];
            });
        }(c);
        n.default = o.a;
    },
    c52c: function(e, n, t) {
        t.d(n, "b", function() {
            return r;
        }), t.d(n, "c", function() {
            return o;
        }), t.d(n, "a", function() {});
        var r = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    f9f5: function(e, n, t) {
        t.r(n);
        var r = t("c52c"), o = t("5db8");
        for (var c in o) "default" !== c && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(c);
        t("3810");
        var a = t("f0c5"), l = Object(a.a)(o.default, r.b, r.c, !1, null, "e91bd068", null, !1, r.a, void 0);
        n.default = l.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/AreaSelect/AreaSelect-create-component", {
    "components/AreaSelect/AreaSelect-create-component": function(e, n, t) {
        t("543d").createComponent(t("f9f5"));
    }
}, [ [ "components/AreaSelect/AreaSelect-create-component" ] ] ]);