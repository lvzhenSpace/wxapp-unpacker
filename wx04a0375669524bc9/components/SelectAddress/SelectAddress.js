(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SelectAddress/SelectAddress" ], {
    "0ca5": function(e, n, t) {
        t.r(n);
        var s = t("6ca5"), o = t.n(s);
        for (var d in s) "default" !== d && function(e) {
            t.d(n, e, function() {
                return s[e];
            });
        }(d);
        n.default = o.a;
    },
    "15e9": function(e, n, t) {},
    "27ed": function(e, n, t) {
        t.d(n, "b", function() {
            return s;
        }), t.d(n, "c", function() {
            return o;
        }), t.d(n, "a", function() {});
        var s = function() {
            var e = this, n = (e.$createElement, e._self._c, e.address && e.address.id && e.isShowPhone ? e._f("hidePhoneDetail")(e.address && e.address.phone) : null);
            e.$mp.data = Object.assign({}, {
                $root: {
                    f0: n
                }
            });
        }, o = [];
    },
    "6ca5": function(e, n, t) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var t = {
                props: {
                    value: {
                        type: Object
                    },
                    isShowPhone: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                data: function() {
                    return {
                        address: {}
                    };
                },
                filters: {
                    hidePhoneDetail: function(e) {
                        return e ? e.substring(0, 3) + "****" + e.substring(7, 11) : "";
                    }
                },
                watch: {
                    address: function() {
                        this.$emit("input", this.address);
                    },
                    value: function(e) {
                        this.address = e;
                    }
                },
                mounted: function(n) {
                    var t = this;
                    this.address = this.value || {}, e.$on("selectAddress", function(e) {
                        t.address = e;
                    });
                },
                beforeDestroy: function() {
                    e.$off("selectAddress", function(e) {
                        console.log("移除 selectAddress 自定义事件");
                    });
                },
                methods: {
                    handleSelectAddress: function() {
                        e.navigateTo({
                            url: "/pages/myAddress/index?select=true"
                        });
                    }
                }
            };
            n.default = t;
        }).call(this, t("543d").default);
    },
    "990a": function(e, n, t) {
        t.r(n);
        var s = t("27ed"), o = t("0ca5");
        for (var d in o) "default" !== d && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(d);
        t("f524");
        var c = t("f0c5"), a = Object(c.a)(o.default, s.b, s.c, !1, null, "647e14c4", null, !1, s.a, void 0);
        n.default = a.exports;
    },
    f524: function(e, n, t) {
        var s = t("15e9");
        t.n(s).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SelectAddress/SelectAddress-create-component", {
    "components/SelectAddress/SelectAddress-create-component": function(e, n, t) {
        t("543d").createComponent(t("990a"));
    }
}, [ [ "components/SelectAddress/SelectAddress-create-component" ] ] ]);