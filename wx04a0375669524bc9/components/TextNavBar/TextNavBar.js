(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/TextNavBar/TextNavBar" ], {
    "24d6": function(t, e, n) {
        n.r(e);
        var o = n("ed07"), a = n("d201");
        for (var c in a) "default" !== c && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(c);
        n("b4d7");
        var r = n("f0c5"), i = Object(r.a)(a.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        e.default = i.exports;
    },
    4722: function(t, e, n) {},
    "6e54": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: {
                    title: String,
                    darkMode: Boolean,
                    bgColor: String,
                    titleColor: String
                },
                data: function() {
                    return {
                        customBar: 64,
                        contentStyle: 64,
                        titleStyle: 64
                    };
                },
                computed: {
                    deviceInfo: function() {
                        return this.$store.getters.deviceInfo;
                    }
                },
                created: function() {
                    this.customBar = this.deviceInfo.customBar, this.contentStyle = "height:".concat(this.deviceInfo.customBar, "px;padding-top:").concat(this.deviceInfo.statusBar, "px;background:").concat(this.bgColor, ";"), 
                    this.titleStyle = "color:".concat(this.titleColor);
                },
                methods: {
                    goBack: function() {
                        console.log("goback", getCurrentPages().length), 1 == getCurrentPages().length ? t.switchTab({
                            url: "/pages/index/index"
                        }) : t.navigateBack();
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    b4d7: function(t, e, n) {
        var o = n("4722");
        n.n(o).a;
    },
    d201: function(t, e, n) {
        n.r(e);
        var o = n("6e54"), a = n.n(o);
        for (var c in o) "default" !== c && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(c);
        e.default = a.a;
    },
    ed07: function(t, e, n) {
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/TextNavBar/TextNavBar-create-component", {
    "components/TextNavBar/TextNavBar-create-component": function(t, e, n) {
        n("543d").createComponent(n("24d6"));
    }
}, [ [ "components/TextNavBar/TextNavBar-create-component" ] ] ]);