(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/LotteryResultPopup/LotteryResultPopup" ], {
    "1f53": function(t, n, o) {
        var e = o("f48e");
        o.n(e).a;
    },
    "565b": function(t, n, o) {
        o.r(n);
        var e = o("6e7d"), u = o("7a26");
        for (var c in u) "default" !== c && function(t) {
            o.d(n, t, function() {
                return u[t];
            });
        }(c);
        o("1f53");
        var a = o("f0c5"), r = Object(a.a)(u.default, e.b, e.c, !1, null, "50143e10", null, !1, e.a, void 0);
        n.default = r.exports;
    },
    "6e7d": function(t, n, o) {
        o.d(n, "b", function() {
            return e;
        }), o.d(n, "c", function() {
            return u;
        }), o.d(n, "a", function() {});
        var e = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    "7a26": function(t, n, o) {
        o.r(n);
        var e = o("c835"), u = o.n(e);
        for (var c in e) "default" !== c && function(t) {
            o.d(n, t, function() {
                return e[t];
            });
        }(c);
        n.default = u.a;
    },
    c835: function(t, n, o) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = {
                components: {},
                data: function() {
                    return {};
                },
                props: {
                    skus: {
                        type: Array
                    }
                },
                computed: {
                    sku: function() {
                        return this.skus[0] || {};
                    }
                },
                watch: {},
                onLoad: function(t) {},
                created: function() {},
                methods: {
                    toBoxDetail: function() {
                        t.navigateTo({
                            url: "/pages/myBox/detail?uuid=" + this.sku.uuid
                        });
                    },
                    toMyBox: function() {
                        t.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    cancel: function() {
                        this.$emit("cancel");
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = o;
        }).call(this, o("543d").default);
    },
    f48e: function(t, n, o) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/LotteryResultPopup/LotteryResultPopup-create-component", {
    "components/LotteryResultPopup/LotteryResultPopup-create-component": function(t, n, o) {
        o("543d").createComponent(o("565b"));
    }
}, [ [ "components/LotteryResultPopup/LotteryResultPopup-create-component" ] ] ]);