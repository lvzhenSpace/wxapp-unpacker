(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/PageRender" ], {
    "4a0b": function(e, t, n) {
        n.r(t);
        var o = n("83f4"), r = n.n(o);
        for (var a in o) "default" !== a && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = r.a;
    },
    "83f4": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = n("26cb");
            function r(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function a(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? r(Object(n), !0).forEach(function(t) {
                        i(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            function i(e, t, n) {
                return t in e ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : e[t] = n, e;
            }
            var u = {
                components: {
                    DefaultTheme: function() {
                        n.e("components/PageRender/themes/DefaultTheme").then(function() {
                            return resolve(n("9238"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    HomepageTheme: function() {
                        Promise.all([ n.e("common/vendor"), n.e("components/PageRender/themes/HomepageTheme") ]).then(function() {
                            return resolve(n("04b2"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                props: {
                    page: {
                        type: Object,
                        default: function() {
                            return {
                                title: "",
                                modules: [],
                                isSharePopup: !1
                            };
                        }
                    },
                    homeNum: Number,
                    theme: {
                        type: String
                    },
                    fullPageMode: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    },
                    refreshCounter: Number,
                    getNextPageCounter: Number
                },
                data: function() {
                    return {
                        isSharePopup: !1
                    };
                },
                computed: a(a({}, (0, o.mapGetters)([ "userInfo" ])), {}, {
                    renderTheme: function() {
                        return this.page && this.page.base && this.page.base.theme || this.theme || "default";
                    },
                    title: function() {
                        return this.page && this.page.title || "未设置";
                    },
                    navColor: function() {
                        return this.page && this.page.nav_color || "#FFFFFF";
                    },
                    isShowFloatBtn: function() {
                        return this.page && this.page.is_float_btn;
                    },
                    floatBtn: function() {
                        return this.page && this.page.float_btn || {};
                    },
                    posterInfo: function() {
                        return {
                            money_price: "",
                            score_price: "",
                            title: this.title,
                            path: this.getShareConfig().path,
                            thumb: "https://img121.7dun.com/member_grade/posterImg.png"
                        };
                    }
                }),
                mounted: function() {
                    this.setNav();
                },
                watch: {
                    title: function() {
                        this.setNav();
                    },
                    navColor: function() {
                        this.setNav();
                    }
                },
                methods: {
                    setNav: function() {
                        if (!this.fullPageMode) return !1;
                        e.setNavigationBarTitle({
                            title: this.title
                        }), e.setNavigationBarColor({
                            backgroundColor: this.navColor,
                            frontColor: "#ffffff"
                        });
                    },
                    invite: function() {
                        "invite" == this.floatBtn.link.url && (this.isSharePopup = !0);
                    },
                    hideSharePopup: function() {
                        this.isSharePopup = !1;
                    }
                }
            };
            t.default = u;
        }).call(this, n("543d").default);
    },
    dc24: function(e, t, n) {
        n.d(t, "b", function() {
            return r;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            FloatBtn: function() {
                return n.e("components/FloatBtn/FloatBtn").then(n.bind(null, "9d70"));
            },
            SharePopup: function() {
                return Promise.all([ n.e("common/vendor"), n.e("components/SharePopup/SharePopup") ]).then(n.bind(null, "4b48"));
            }
        }, r = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    },
    e85b: function(e, t, n) {
        n.r(t);
        var o = n("dc24"), r = n("4a0b");
        for (var a in r) "default" !== a && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(a);
        n("fa29");
        var i = n("f0c5"), u = Object(i.a)(r.default, o.b, o.c, !1, null, "18cee4e2", null, !1, o.a, void 0);
        t.default = u.exports;
    },
    e8c7: function(e, t, n) {},
    fa29: function(e, t, n) {
        var o = n("e8c7");
        n.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/PageRender-create-component", {
    "components/PageRender/PageRender-create-component": function(e, t, n) {
        n("543d").createComponent(n("e85b"));
    }
}, [ [ "components/PageRender/PageRender-create-component" ] ] ]);