(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/modules/ImageList" ], {
    "196c": function(e, n, t) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            props: {
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            data: function() {
                return {};
            },
            computed: {
                list: function() {
                    return this.module.list || [];
                },
                style: function() {
                    return this.module.style || {};
                },
                perRow: function() {
                    return {
                        grid1: 1,
                        grid2: 2,
                        grid3: 3,
                        grid4: 4,
                        grid5: 5,
                        scroll: 1
                    }[this.module.grid];
                },
                imageWidth: function() {
                    return (750 - 2 * (this.style.margin || 0) - (this.module.spacing || 0) * (this.perRow - 1)) / this.perRow;
                }
            },
            mounted: function() {},
            watch: {},
            methods: {}
        };
        n.default = o;
    },
    "35fe": function(e, n, t) {
        t.r(n);
        var o = t("b424"), u = t("d6b8");
        for (var r in u) "default" !== r && function(e) {
            t.d(n, e, function() {
                return u[e];
            });
        }(r);
        t("f376");
        var c = t("f0c5"), i = Object(c.a)(u.default, o.b, o.c, !1, null, "4fbdc8a4", null, !1, o.a, void 0);
        n.default = i.exports;
    },
    b424: function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return u;
        }), t.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    cbfa: function(e, n, t) {},
    d6b8: function(e, n, t) {
        t.r(n);
        var o = t("196c"), u = t.n(o);
        for (var r in o) "default" !== r && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(r);
        n.default = u.a;
    },
    f376: function(e, n, t) {
        var o = t("cbfa");
        t.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/modules/ImageList-create-component", {
    "components/PageRender/modules/ImageList-create-component": function(e, n, t) {
        t("543d").createComponent(t("35fe"));
    }
}, [ [ "components/PageRender/modules/ImageList-create-component" ] ] ]);