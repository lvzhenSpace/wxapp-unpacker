(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/modules/PureImageList" ], {
    "04c9": function(e, n, t) {
        t.r(n);
        var o = t("380a"), r = t("ed72");
        for (var u in r) "default" !== u && function(e) {
            t.d(n, e, function() {
                return r[e];
            });
        }(u);
        t("093b");
        var i = t("f0c5"), c = Object(i.a)(r.default, o.b, o.c, !1, null, "5ed1ef5c", null, !1, o.a, void 0);
        n.default = c.exports;
    },
    "093b": function(e, n, t) {
        var o = t("30f80");
        t.n(o).a;
    },
    "30f80": function(e, n, t) {},
    "380a": function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return r;
        }), t.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    },
    be06: function(e, n, t) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            props: {
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            data: function() {
                return {};
            },
            computed: {
                list: function() {
                    return this.module.images || [];
                },
                style: function() {
                    return this.module.style || {};
                },
                perRow: function() {
                    return {
                        grid1: 1,
                        grid2: 2,
                        grid3: 3,
                        grid4: 4,
                        grid5: 5,
                        scroll: 1
                    }[this.module.grid || "grid1"];
                },
                imageWidth: function() {
                    return (750 - 2 * (this.style.margin || 0) - (this.module.spacing || 0) * (this.perRow - 1)) / this.perRow;
                }
            },
            mounted: function() {},
            watch: {},
            methods: {}
        };
        n.default = o;
    },
    ed72: function(e, n, t) {
        t.r(n);
        var o = t("be06"), r = t.n(o);
        for (var u in o) "default" !== u && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(u);
        n.default = r.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/modules/PureImageList-create-component", {
    "components/PageRender/modules/PureImageList-create-component": function(e, n, t) {
        t("543d").createComponent(t("04c9"));
    }
}, [ [ "components/PageRender/modules/PureImageList-create-component" ] ] ]);