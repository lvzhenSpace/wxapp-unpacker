(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/themes/HomepageTheme" ], {
    "04b2": function(e, n, t) {
        t.r(n);
        var i = t("c369"), o = t("bd0f");
        for (var c in o) "default" !== c && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(c);
        t("a5f9");
        var u = t("f0c5"), a = Object(u.a)(o.default, i.b, i.c, !1, null, "b1418e7e", null, !1, i.a, void 0);
        n.default = a.exports;
    },
    "9b5e": function(e, n, t) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0, t("7fce");
            var i = {
                components: {
                    ImageList: function() {
                        t.e("components/PageRender/modules/ImageList").then(function() {
                            return resolve(t("35fe"));
                        }.bind(null, t)).catch(t.oe);
                    },
                    PureImageList: function() {
                        t.e("components/PageRender/modules/PureImageList").then(function() {
                            return resolve(t("04c9"));
                        }.bind(null, t)).catch(t.oe);
                    },
                    VideoItem: function() {
                        t.e("components/PageRender/modules/Video").then(function() {
                            return resolve(t("ecbf"));
                        }.bind(null, t)).catch(t.oe);
                    }
                },
                props: {
                    homeNum: {
                        type: Number,
                        default: 0
                    },
                    refreshCount: {
                        type: Number
                    },
                    page: {
                        type: Object,
                        default: function() {
                            return {
                                modules: []
                            };
                        }
                    },
                    refreshCounter: Number,
                    getNextPageCounter: Number
                },
                data: function() {
                    return {
                        topheight: 0,
                        numberType: 0,
                        homeList: [ {
                            selected: 0,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/20230207NewImg/quanbu.png",
                            selectedIconPath: "https://img121.7dun.com/20230207NewImg/quanbu-on.png",
                            activity_type: "fudai"
                        }, {
                            selected: 1,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/20230207NewImg/wuxianshang.png",
                            selectedIconPath: "https://img121.7dun.com/20230207NewImg/wuxianshang-on.png",
                            activity_type: "fudai"
                        }, {
                            selected: 2,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/20230207NewImg/yifanshang.png",
                            selectedIconPath: "https://img121.7dun.com/20230207NewImg/yifanshang-on.png",
                            activity_type: "yifanshang"
                        }, {
                            selected: 3,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/20230207NewImg/newEdit/peilashang.png",
                            selectedIconPath: "https://img121.7dun.com/20230207NewImg/newEdit/peilashang-on.png",
                            activity_type: "yifanshang"
                        }, {
                            selected: 4,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/20230207NewImg/newEdit/lianjishang.png",
                            selectedIconPath: "https://img121.7dun.com/20230207NewImg/newEdit/lianjishang-on.png",
                            activity_type: "renyi"
                        } ]
                    };
                },
                computed: {
                    deviceInfo: function() {
                        return this.$store.getters.deviceInfo;
                    },
                    pageModules: function() {
                        return this.page.modules || [];
                    },
                    swiperList: function() {
                        return this.pageModules[0] && this.pageModules[0].list || [];
                    },
                    swiperModule: function() {
                        return this.pageModules[0] || {
                            style: {}
                        };
                    },
                    modules: function() {
                        return this.pageModules.slice(1);
                    },
                    title: function() {
                        return this.page.title || "未设置";
                    },
                    floatBtn: function() {
                        return this.page && this.page.float_btn || {};
                    }
                },
                mounted: function() {
                    this.topheight = this.deviceInfo.customBar;
                },
                watch: {
                    title: function(e) {}
                },
                methods: {
                    goShareActivity: function() {
                        "invite" == this.floatBtn.link.url || e.navigateTo({
                            url: this.floatBtn.link.path
                        });
                    },
                    hanldeActivityTypeClick: function(e) {
                        this.numberType = e;
                    }
                }
            };
            n.default = i;
        }).call(this, t("543d").default);
    },
    a5f9: function(e, n, t) {
        var i = t("d65c");
        t.n(i).a;
    },
    bd0f: function(e, n, t) {
        t.r(n);
        var i = t("9b5e"), o = t.n(i);
        for (var c in i) "default" !== c && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(c);
        n.default = o.a;
    },
    c369: function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return c;
        }), t.d(n, "a", function() {
            return i;
        });
        var i = {
            Banner: function() {
                return t.e("components/Banner/Banner").then(t.bind(null, "3d80"));
            },
            cardTitle: function() {
                return t.e("components/cardTitle/cardTitle").then(t.bind(null, "1ac7"));
            },
            BoxList: function() {
                return t.e("components/BoxList/BoxList").then(t.bind(null, "afc6"));
            },
            ProductList: function() {
                return t.e("components/ProductList/ProductList").then(t.bind(null, "e5e7"));
            },
            ActivityList: function() {
                return t.e("components/ActivityList/ActivityList").then(t.bind(null, "bc81"));
            },
            CouponList: function() {
                return t.e("components/CouponList/CouponList").then(t.bind(null, "c258"));
            },
            IPList: function() {
                return t.e("components/IPList/IPList").then(t.bind(null, "01f1"));
            },
            CategoryList: function() {
                return t.e("components/CategoryList/CategoryList").then(t.bind(null, "cc21"));
            },
            SigninCard: function() {
                return t.e("components/SigninCard/SigninCard").then(t.bind(null, "ec3b"));
            },
            HTML: function() {
                return t.e("components/HTML/HTML").then(t.bind(null, "79f5"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
    },
    d65c: function(e, n, t) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/themes/HomepageTheme-create-component", {
    "components/PageRender/themes/HomepageTheme-create-component": function(e, n, t) {
        t("543d").createComponent(t("04b2"));
    }
}, [ [ "components/PageRender/themes/HomepageTheme-create-component" ] ] ]);