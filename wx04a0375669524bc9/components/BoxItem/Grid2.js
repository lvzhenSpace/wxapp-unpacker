(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Grid2" ], {
    "0878": function(n, t, e) {
        e.r(t);
        var o = e("5d57"), i = e("d6ab");
        for (var u in i) "default" !== u && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        e("c187");
        var c = e("f0c5"), a = Object(c.a)(i.default, o.b, o.c, !1, null, "493c82cb", null, !1, o.a, void 0);
        t.default = a.exports;
    },
    "5d57": function(n, t, e) {
        e.d(t, "b", function() {
            return i;
        }), e.d(t, "c", function() {
            return u;
        }), e.d(t, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, i = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    6152: function(n, t, e) {},
    "75ee": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        n.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    c187: function(n, t, e) {
        var o = e("6152");
        e.n(o).a;
    },
    d6ab: function(n, t, e) {
        e.r(t);
        var o = e("75ee"), i = e.n(o);
        for (var u in o) "default" !== u && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        t.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Grid2-create-component", {
    "components/BoxItem/Grid2-create-component": function(n, t, e) {
        e("543d").createComponent(e("0878"));
    }
}, [ [ "components/BoxItem/Grid2-create-component" ] ] ]);