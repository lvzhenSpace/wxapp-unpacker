(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Grid3" ], {
    "0034": function(n, t, e) {
        e.r(t);
        var o = e("d36a"), i = e("363d");
        for (var u in i) "default" !== u && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        e("47931");
        var a = e("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, "2f53a721", null, !1, o.a, void 0);
        t.default = c.exports;
    },
    "363d": function(n, t, e) {
        e.r(t);
        var o = e("b87c"), i = e.n(o);
        for (var u in o) "default" !== u && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        t.default = i.a;
    },
    47931: function(n, t, e) {
        var o = e("e0df");
        e.n(o).a;
    },
    b87c: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        n.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    d36a: function(n, t, e) {
        e.d(t, "b", function() {
            return i;
        }), e.d(t, "c", function() {
            return u;
        }), e.d(t, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, i = function() {
            this.$createElement;
            this._self._c;
        }, u = [];
    },
    e0df: function(n, t, e) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Grid3-create-component", {
    "components/BoxItem/Grid3-create-component": function(n, t, e) {
        e("543d").createComponent(e("0034"));
    }
}, [ [ "components/BoxItem/Grid3-create-component" ] ] ]);