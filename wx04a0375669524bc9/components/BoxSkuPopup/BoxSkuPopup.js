(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxSkuPopup/BoxSkuPopup" ], {
    "0846": function(t, n, e) {
        e.d(n, "b", function() {
            return u;
        }), e.d(n, "c", function() {
            return i;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, u = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    "16ac": function(t, n, e) {
        e.r(n);
        var o = e("0846"), u = e("8332");
        for (var i in u) "default" !== i && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(i);
        e("d050");
        var r = e("f0c5"), a = Object(r.a)(u.default, o.b, o.c, !1, null, "2371eb5a", null, !1, o.a, void 0);
        n.default = a.exports;
    },
    8332: function(t, n, e) {
        e.r(n);
        var o = e("aa45"), u = e.n(o);
        for (var i in o) "default" !== i && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(i);
        n.default = u.a;
    },
    a1e4: function(t, n, e) {},
    aa45: function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        current: 0
                    };
                },
                props: {
                    skuList: {
                        type: Array
                    },
                    detailImageList: {
                        type: Array
                    },
                    setting: {}
                },
                computed: {
                    isShowDetail: function() {
                        return this.detailImageList && this.detailImageList.length;
                    },
                    isShowSkuList: function() {
                        return this.skuList && this.skuList.length;
                    },
                    isShowHeaderTab: function() {
                        return this.isShowDetail && this.isShowSkuList;
                    }
                },
                watch: {},
                onLoad: function(t) {},
                created: function() {},
                methods: {
                    currentChange: function(t) {
                        var n = t.currentTarget.dataset.current;
                        this.current = n;
                    },
                    currentChange2: function(t) {
                        var n = t.detail.current;
                        this.current = n;
                    },
                    close: function() {
                        this.$emit("close");
                    },
                    previewSkuThumb: function(n) {
                        t.previewImage({
                            urls: this.skuList.map(function(t) {
                                return t.thumb;
                            }),
                            current: n
                        });
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    d050: function(t, n, e) {
        var o = e("a1e4");
        e.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxSkuPopup/BoxSkuPopup-create-component", {
    "components/BoxSkuPopup/BoxSkuPopup-create-component": function(t, n, e) {
        e("543d").createComponent(e("16ac"));
    }
}, [ [ "components/BoxSkuPopup/BoxSkuPopup-create-component" ] ] ]);