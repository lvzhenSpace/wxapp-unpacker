(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/HiddenSkuRank/HiddenSkuRank" ], {
    "10ab": function(t, n, a) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var a = {
                components: {},
                data: function() {
                    return {
                        isInit: !1,
                        list: [],
                        total: 0,
                        page: 1,
                        perPage: 20,
                        tag: "all"
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: {},
                watch: {},
                created: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        t.showLoading({
                            title: "加载中"
                        }), this.fetchList().then(function(n) {
                            t.hideLoading();
                        });
                    },
                    fetchList: function() {
                        var t = this;
                        return !this.isLoading && (this.isLoading = !0, this.$http("/boxes/".concat(this.info.uuid, "/records"), "GET", {
                            page: this.page,
                            per_page: this.perPage
                        }).then(function(n) {
                            t.isInit = !0, t.list = t.list.concat(n.data.list), t.isLoading = !1, t.page++, 
                            t.total = n.data.item_total;
                        }).catch(function(n) {
                            t.isInit = !1;
                        }));
                    },
                    cancel: function() {
                        this.$emit("close");
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = a;
        }).call(this, a("543d").default);
    },
    "18d4": function(t, n, a) {},
    "34e1": function(t, n, a) {
        a.d(n, "b", function() {
            return o;
        }), a.d(n, "c", function() {
            return i;
        }), a.d(n, "a", function() {
            return e;
        });
        var e = {
            NoData: function() {
                return a.e("components/NoData/NoData").then(a.bind(null, "83c6"));
            }
        }, o = function() {
            var t = this, n = (t.$createElement, t._self._c, t.__map(t.list, function(n, a) {
                return {
                    $orig: t.__get_orig(n),
                    g0: t.$tool.formatDate(n.created_at, "MM/dd hh:mm")
                };
            }));
            t.$mp.data = Object.assign({}, {
                $root: {
                    l0: n
                }
            });
        }, i = [];
    },
    "5ada": function(t, n, a) {
        a.r(n);
        var e = a("34e1"), o = a("caad");
        for (var i in o) "default" !== i && function(t) {
            a.d(n, t, function() {
                return o[t];
            });
        }(i);
        a("7c88");
        var c = a("f0c5"), u = Object(c.a)(o.default, e.b, e.c, !1, null, "3fa442c3", null, !1, e.a, void 0);
        n.default = u.exports;
    },
    "7c88": function(t, n, a) {
        var e = a("18d4");
        a.n(e).a;
    },
    caad: function(t, n, a) {
        a.r(n);
        var e = a("10ab"), o = a.n(e);
        for (var i in e) "default" !== i && function(t) {
            a.d(n, t, function() {
                return e[t];
            });
        }(i);
        n.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/HiddenSkuRank/HiddenSkuRank-create-component", {
    "components/HiddenSkuRank/HiddenSkuRank-create-component": function(t, n, a) {
        a("543d").createComponent(a("5ada"));
    }
}, [ [ "components/HiddenSkuRank/HiddenSkuRank-create-component" ] ] ]);