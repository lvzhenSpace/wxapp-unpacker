(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/UserGroupCheck/UserGroupCheck" ], {
    "498b": function(e, n, t) {
        var o = t("d4f4");
        t.n(o).a;
    },
    "500e": function(e, n, t) {
        t.r(n);
        var o = t("ac14"), i = t.n(o);
        for (var r in o) "default" !== r && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(r);
        n.default = i.a;
    },
    6925: function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return i;
        }), t.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    "7a46": function(e, n, t) {
        t.r(n);
        var o = t("6925"), i = t("500e");
        for (var r in i) "default" !== r && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(r);
        t("498b");
        var a = t("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, "36aa1e83", null, !1, o.a, void 0);
        n.default = c.exports;
    },
    ac14: function(e, n, t) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var t = {
                components: {},
                data: function() {
                    return {
                        info: {},
                        isPassed: 0,
                        options: {},
                        isInit: !1
                    };
                },
                props: {
                    userGroupId: {
                        type: Number
                    },
                    title: {
                        type: String
                    }
                },
                computed: {},
                watch: {},
                created: function() {
                    this.initOrder();
                },
                methods: {
                    toCheck: function(n) {
                        var t = {
                            phone: "/pages/myProfile/index",
                            vip: "/pages/buyVip/index",
                            birthday: "/pages/myProfile/index",
                            score: "/pages/myScore/index",
                            register_time: "/pages/center/detail",
                            level_score: "/pages/center/detail",
                            invitee_total: "/pages/myInvitees/index",
                            psy_money_total: "/pages/index/index"
                        }[n];
                        e.navigateTo({
                            url: t
                        });
                    },
                    initOrder: function() {
                        var n = this;
                        if (!this.userGroupId) return !1;
                        e.showLoading(), this.$http("/user-group/check", "POST", {
                            id: this.userGroupId
                        }).then(function(t) {
                            console.log(t), n.isInit = !0, n.options = t.data.options, n.isPassed = t.data.is_passed, 
                            e.hideLoading();
                        });
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    toPage: function(n) {
                        e.navigateTo({
                            url: n
                        });
                    }
                }
            };
            n.default = t;
        }).call(this, t("543d").default);
    },
    d4f4: function(e, n, t) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/UserGroupCheck/UserGroupCheck-create-component", {
    "components/UserGroupCheck/UserGroupCheck-create-component": function(e, n, t) {
        t("543d").createComponent(t("7a46"));
    }
}, [ [ "components/UserGroupCheck/UserGroupCheck-create-component" ] ] ]);