(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/FreeTicketFloatBtn/FreeTicketFloatBtn" ], {
    "0335": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: {
                    uuid: String,
                    nodeType: String
                },
                data: function() {
                    return {
                        inviteTotal: 0,
                        usedTotal: 0,
                        stock: 0,
                        title: ""
                    };
                },
                computed: {
                    isAnimate: function() {
                        return !0;
                    }
                },
                created: function() {
                    var e = this;
                    this.initData(), t.$on("refreshFreeTicketTotal", function() {
                        e.initData();
                    });
                },
                methods: {
                    handleClick: function() {
                        t.navigateTo({
                            url: "/pages/activityTicket/record?uuid=" + this.uuid + "&node_type=" + this.nodeType
                        });
                    },
                    handleUseFreeTime: function() {
                        var e = this;
                        t.showModal({
                            title: "操作提示",
                            content: "确定使用一次免费抽奖机会吗？",
                            confirmText: "确定使用",
                            cancelText: "暂不",
                            success: function(t) {
                                t.confirm && (e.$emit("useFreeTicket"), setTimeout(function() {
                                    e.initData();
                                }, 1e3));
                            }
                        });
                    },
                    initData: function() {
                        var t = this;
                        if (!this.uuid) return !1;
                        this.$http("/activity/ticket-total", "GET", {
                            node_type: this.nodeType,
                            uuid: this.uuid
                        }).then(function(e) {
                            t.inviteTotal = e.data.invite_total, t.usedTotal = e.data.used_total, t.stock = e.data.stock, 
                            t.title = e.data.title;
                        });
                    }
                },
                watch: {
                    uuid: function(t) {
                        this.initData();
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    "25dd": function(t, e, n) {
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var i = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    5388: function(t, e, n) {},
    "55d8": function(t, e, n) {
        var i = n("5388");
        n.n(i).a;
    },
    "6af9": function(t, e, n) {
        n.r(e);
        var i = n("25dd"), o = n("b644");
        for (var a in o) "default" !== a && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        n("55d8");
        var c = n("f0c5"), u = Object(c.a)(o.default, i.b, i.c, !1, null, "d302dce4", null, !1, i.a, void 0);
        e.default = u.exports;
    },
    b644: function(t, e, n) {
        n.r(e);
        var i = n("0335"), o = n.n(i);
        for (var a in i) "default" !== a && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        e.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/FreeTicketFloatBtn/FreeTicketFloatBtn-create-component", {
    "components/FreeTicketFloatBtn/FreeTicketFloatBtn-create-component": function(t, e, n) {
        n("543d").createComponent(n("6af9"));
    }
}, [ [ "components/FreeTicketFloatBtn/FreeTicketFloatBtn-create-component" ] ] ]);