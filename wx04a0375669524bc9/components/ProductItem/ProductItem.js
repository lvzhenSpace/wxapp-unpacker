(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ProductItem/ProductItem" ], {
    "27f4": function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    grid: {
                        type: String,
                        default: function() {
                            return "grid2";
                        }
                    },
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    },
                    isShowTags: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    "321a": function(t, n, e) {},
    3547: function(t, n, e) {
        var o = e("321a");
        e.n(o).a;
    },
    "4ed5": function(t, n, e) {
        e.r(n);
        var o = e("98df"), u = e("7efe");
        for (var r in u) "default" !== r && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(r);
        e("3547");
        var c = e("f0c5"), i = Object(c.a)(u.default, o.b, o.c, !1, null, "cba160fe", null, !1, o.a, void 0);
        n.default = i.exports;
    },
    "7efe": function(t, n, e) {
        e.r(n);
        var o = e("27f4"), u = e.n(o);
        for (var r in o) "default" !== r && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(r);
        n.default = u.a;
    },
    "98df": function(t, n, e) {
        e.d(n, "b", function() {
            return u;
        }), e.d(n, "c", function() {
            return r;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "f149"));
            }
        }, u = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ProductItem/ProductItem-create-component", {
    "components/ProductItem/ProductItem-create-component": function(t, n, e) {
        e("543d").createComponent(e("4ed5"));
    }
}, [ [ "components/ProductItem/ProductItem-create-component" ] ] ]);