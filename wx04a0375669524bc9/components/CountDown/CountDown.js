(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/CountDown/CountDown" ], {
    "2e89": function(e, t, n) {
        n.r(t);
        var o = n("affb"), i = n.n(o);
        for (var c in o) "default" !== c && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(c);
        t.default = i.a;
    },
    "3e9f": function(e, t, n) {},
    "9cef": function(e, t, n) {
        var o = n("3e9f");
        n.n(o).a;
    },
    affb: function(e, t, n) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = {
            name: "CountDown",
            components: {
                DefaultTheme: function() {
                    n.e("components/CountDown/theme/Default").then(function() {
                        return resolve(n("d035"));
                    }.bind(null, n)).catch(n.oe);
                },
                ProductDetailTheme1: function() {
                    n.e("components/CountDown/theme/ProductDetailTheme1").then(function() {
                        return resolve(n("19a4"));
                    }.bind(null, n)).catch(n.oe);
                },
                ZhuliTheme: function() {
                    n.e("components/CountDown/theme/ZhuliTheme").then(function() {
                        return resolve(n("edb0"));
                    }.bind(null, n)).catch(n.oe);
                }
            },
            props: {
                start: {
                    type: String
                },
                end: {
                    type: String
                },
                endText: {
                    type: String,
                    default: "活动已结束"
                },
                theme: {
                    type: String,
                    default: "default"
                }
            },
            data: function() {
                return {
                    status: "pending",
                    timer: null,
                    startSecondTime: 0,
                    endSecondTime: 0,
                    timeObj: {
                        day: 0,
                        hour: 0,
                        minute: 0,
                        second: 0
                    }
                };
            },
            computed: {},
            mounted: function() {
                this.calcTime();
            },
            watch: {
                start: {
                    handler: function(e) {
                        var t = this;
                        clearInterval(this.timer), e && (this.startSecondTime = this.getSecondTime(e), this.endSecondTime = this.getSecondTime(this.end), 
                        this.calcTime(), this.timer = setInterval(function() {
                            t.calcTime();
                        }, 1e3));
                    },
                    immediate: !0
                },
                status: {
                    handler: function(e) {
                        this.$emit("change", e);
                    },
                    immediate: !0
                }
            },
            destroyed: function() {
                clearInterval(this.timer);
            },
            methods: {
                getSecondTime: function(e) {
                    if ("string" == typeof e) var t = e.split(/[- :]/), n = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]); else n = new Date();
                    return n.getTime() / 1e3;
                },
                calcTime: function() {
                    var e = this.getSecondTime();
                    if (this.endSecondTime < e) return this.status = "expired", void clearInterval(this.timer);
                    if (this.startSecondTime < e) {
                        this.status = "working";
                        var t = this.endSecondTime - e;
                        this.timeObj = this.formatTimeDiff(t);
                    } else if (this.startSecondTime > e) {
                        this.status = "pending";
                        var n = this.startSecondTime - e;
                        this.timeObj = this.formatTimeDiff(n);
                    }
                },
                formatTimeDiff: function(e) {
                    var t = parseInt(e / 86400);
                    return {
                        day: t,
                        hour: parseInt(e / 3600) - 24 * t,
                        minute: parseInt(e % 3600 / 60),
                        second: parseInt(e % 60)
                    };
                }
            }
        };
        t.default = o;
    },
    cdfb: function(e, t, n) {
        n.r(t);
        var o = n("e493"), i = n("2e89");
        for (var c in i) "default" !== c && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(c);
        n("9cef");
        var a = n("f0c5"), r = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = r.exports;
    },
    e493: function(e, t, n) {
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/CountDown/CountDown-create-component", {
    "components/CountDown/CountDown-create-component": function(e, t, n) {
        n("543d").createComponent(n("cdfb"));
    }
}, [ [ "components/CountDown/CountDown-create-component" ] ] ]);