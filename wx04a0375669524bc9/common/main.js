(global.webpackJsonp = global.webpackJsonp || []).push([ [ "common/main" ], {
    3626: function(t, e, o) {
        var n = o("e383");
        o.n(n).a;
    },
    "560a": function(t, e, o) {
        (function(t, e) {
            o("f868");
            var n = p(o("66fd")), r = p(o("8477")), a = p(o("56a5")), u = p(o("e7d7")), i = p(o("23b6")), c = p(o("22cd")), l = p(o("5969")), f = p(o("fdc7")), d = p(o("e2b1"));
            function p(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            function s(t, e) {
                var o = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), o.push.apply(o, n);
                }
                return o;
            }
            function y(t, e, o) {
                return e in t ? Object.defineProperty(t, e, {
                    value: o,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[e] = o, t;
            }
            wx.__webpack_require_UNI_MP_PLUGIN__ = o, n.default.mixin(l.default), n.default.mixin(f.default);
            n.default.component("tab-bar", function() {
                Promise.all([ o.e("common/vendor"), o.e("components/TabBar/tabBar") ]).then(function() {
                    return resolve(o("8163"));
                }.bind(null, o)).catch(o.oe);
            }), n.default.prototype.$api = d.default;
            var v = o("a6f5");
            n.default.prototype.$upload = v;
            var h = o("7385");
            n.default.prototype.$navigator = h;
            var m = o("2251");
            n.default.prototype.$visitor = m, n.default.prototype.$tool = i.default, n.default.prototype.$device = c.default, 
            n.default.prototype.$store = u.default, n.default.prototype.$http = a.default, n.default.config.productionTip = !1, 
            n.default.prototype.$showPullRefresh = function() {
                return t.showLoading({
                    title: "刷新中"
                }), setTimeout(function(e) {
                    t.stopPullDownRefresh();
                }, 200), setTimeout(function(e) {
                    t.hideLoading();
                }, 500), u.default.dispatch("getSetting");
            }, t.$showMsg = function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "请求出错", o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2e3;
                t.showToast({
                    title: e,
                    icon: "none",
                    duration: o
                });
            };
            var b = t.$on;
            t.$on = function(e, o) {
                try {
                    t.$off(e);
                } catch (t) {}
                b(e, o);
            };
            var g = {
                click: "https://cdn2.hquesoft.com/box/audio/click.mp3",
                check: "https://cdn2.hquesoft.com/box/audio/check.mp3",
                yao: "https://cdn2.hquesoft.com/box/audio/yao.mp3",
                open: "https://cdn2.hquesoft.com/box/audio/open.mp3"
            }, $ = {
                click: null,
                check: null,
                yao: null,
                open: null
            };
            setTimeout(function() {
                for (var e in g) $[e] = t.createInnerAudioContext(), $[e].src = g[e];
            }, 800), n.default.prototype.$playAudio = function(e) {
                $[e] || ($[e] = t.createInnerAudioContext(), $[e].src = g[e] || e), $[e].play();
            };
            var O = null;
            n.default.prototype.$playBgm = function(e, o) {
                O || (O = t.createInnerAudioContext()), e && (O.src = e, O.loop = !0), O.play(), 
                u.default.dispatch("setIsBgmPlay", !0);
            }, n.default.prototype.$stopBgm = function() {
                O && O.stop(), u.default.dispatch("setIsBgmPlay", !1);
            }, n.default.prototype.$switchBgm = function() {
                u.default.getters.isBgmPlay ? n.default.prototype.$stopBgm() : n.default.prototype.$playBgm();
            }, r.default.mpType = "app";
            var w = new n.default(function(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var o = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? s(Object(o), !0).forEach(function(e) {
                        y(t, e, o[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(o)) : s(Object(o)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(o, e));
                    });
                }
                return t;
            }({
                store: u.default
            }, r.default));
            e(w).$mount();
        }).call(this, o("543d").default, o("543d").createApp);
    },
    8477: function(t, e, o) {
        o.r(e);
        var n = o("e219");
        for (var r in n) "default" !== r && function(t) {
            o.d(e, t, function() {
                return n[t];
            });
        }(r);
        o("3626");
        var a = o("f0c5"), u = Object(a.a)(n.default, void 0, void 0, !1, null, null, null, !1, void 0, void 0);
        e.default = u.exports;
    },
    a7af: function(t, e, o) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var n = o("326d"), r = (o("f93e"), function(t) {
            return t && t.__esModule ? t : {
                default: t
            };
        }(o("2c6a")));
        var a = {
            onLaunch: function(t) {
                var e = this;
                this.$store.dispatch("setEnterScene", t.scene), t.query && t.query.inviter && ((0, 
                n.$setStorage)("inviter", t.query.inviter), (0, n.$setStorage)("invite_node", t.query.invite_node)), 
                this.$store.dispatch("getSetting"), setTimeout(function() {
                    e.$store.dispatch("getSetting");
                }, 500), setTimeout(function() {
                    r.default.checkUpdate();
                }, 2e3);
            },
            onHide: function() {
                console.log("App Hide");
            }
        };
        e.default = a;
    },
    e219: function(t, e, o) {
        o.r(e);
        var n = o("a7af"), r = o.n(n);
        for (var a in n) "default" !== a && function(t) {
            o.d(e, t, function() {
                return n[t];
            });
        }(a);
        e.default = r.a;
    },
    e383: function(t, e, o) {}
}, [ [ "560a", "common/runtime", "common/vendor" ] ] ]);