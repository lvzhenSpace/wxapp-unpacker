(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-loading/u-loading" ], {
    "0178": function(e, n, t) {
        t.r(n);
        var o = t("ca2f"), c = t("153b");
        for (var i in c) [ "default" ].indexOf(i) < 0 && function(e) {
            t.d(n, e, function() {
                return c[e];
            });
        }(i);
        t("33cf");
        var a = t("f0c5"), u = Object(a.a)(c.default, o.b, o.c, !1, null, "1983c2a8", null, !1, o.a, void 0);
        n.default = u.exports;
    },
    "153b": function(e, n, t) {
        t.r(n);
        var o = t("a793"), c = t.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(i);
        n.default = c.a;
    },
    "33cf": function(e, n, t) {
        var o = t("8a05");
        t.n(o).a;
    },
    "8a05": function(e, n, t) {},
    a793: function(e, n, t) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            name: "u-loading",
            props: {
                mode: {
                    type: String,
                    default: "circle"
                },
                color: {
                    type: String,
                    default: "#c7c7c7"
                },
                size: {
                    type: [ String, Number ],
                    default: "34"
                },
                show: {
                    type: Boolean,
                    default: !0
                }
            },
            computed: {
                cricleStyle: function() {
                    var e = {};
                    return e.width = this.size + "rpx", e.height = this.size + "rpx", "circle" == this.mode && (e.borderColor = "#e4e4e4 #e4e4e4 #e4e4e4 ".concat(this.color ? this.color : "#c7c7c7")), 
                    e;
                }
            }
        };
        n.default = o;
    },
    ca2f: function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return c;
        }), t.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            var e = (this._self._c, this.show ? this.__get_style([ this.cricleStyle ]) : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    s0: e
                }
            });
        }, c = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-loading/u-loading-create-component", {
    "uview-ui/components/u-loading/u-loading-create-component": function(e, n, t) {
        t("543d").createComponent(t("0178"));
    }
}, [ [ "uview-ui/components/u-loading/u-loading-create-component" ] ] ]);