(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-row-notice/u-row-notice" ], {
    3720: function(t, n, e) {
        var o = e("e059");
        e.n(o).a;
    },
    "480f": function(t, n, e) {
        e.r(n);
        var o = e("fa78"), i = e.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(u);
        n.default = i.a;
    },
    "4daa": function(t, n, e) {
        e.r(n);
        var o = e("65ba"), i = e("480f");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(u);
        e("3720");
        var a = e("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, "211715bb", null, !1, o.a, void 0);
        n.default = c.exports;
    },
    "65ba": function(t, n, e) {
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            uIcon: function() {
                return e.e("uview-ui/components/u-icon/u-icon").then(e.bind(null, "71ff"));
            }
        }, i = function() {
            this.$createElement;
            var t = (this._self._c, this.show ? this.__get_style([ this.textStyle ]) : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    s0: t
                }
            });
        }, u = [];
    },
    e059: function(t, n, e) {},
    fa78: function(t, n, e) {
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    list: {
                        type: Array,
                        default: function() {
                            return [];
                        }
                    },
                    type: {
                        type: String,
                        default: "warning"
                    },
                    volumeIcon: {
                        type: Boolean,
                        default: !0
                    },
                    moreIcon: {
                        type: Boolean,
                        default: !1
                    },
                    closeIcon: {
                        type: Boolean,
                        default: !1
                    },
                    autoplay: {
                        type: Boolean,
                        default: !0
                    },
                    color: {
                        type: String,
                        default: ""
                    },
                    bgColor: {
                        type: String,
                        default: ""
                    },
                    show: {
                        type: Boolean,
                        default: !0
                    },
                    fontSize: {
                        type: [ Number, String ],
                        default: 26
                    },
                    volumeSize: {
                        type: [ Number, String ],
                        default: 34
                    },
                    speed: {
                        type: [ Number, String ],
                        default: 160
                    },
                    playState: {
                        type: String,
                        default: "play"
                    },
                    padding: {
                        type: [ Number, String ],
                        default: "18rpx 24rpx"
                    }
                },
                data: function() {
                    return {
                        textWidth: 0,
                        boxWidth: 0,
                        animationDuration: "10s",
                        animationPlayState: "paused",
                        showText: ""
                    };
                },
                watch: {
                    list: {
                        immediate: !0,
                        handler: function(t) {
                            var n = this;
                            this.showText = t.join("，"), this.$nextTick(function() {
                                n.initSize();
                            });
                        }
                    },
                    playState: function(t) {
                        this.animationPlayState = "play" == t ? "running" : "paused";
                    },
                    speed: function(t) {
                        this.initSize();
                    }
                },
                computed: {
                    computeColor: function() {
                        return this.color ? this.color : "none" == this.type ? "#606266" : this.type;
                    },
                    textStyle: function() {
                        var t = {};
                        return this.color ? t.color = this.color : "none" == this.type && (t.color = "#606266"), 
                        t.fontSize = this.fontSize + "rpx", t;
                    },
                    computeBgColor: function() {
                        return this.bgColor ? this.bgColor : "none" == this.type ? "transparent" : void 0;
                    }
                },
                mounted: function() {
                    var t = this;
                    this.$nextTick(function() {
                        t.initSize();
                    });
                },
                methods: {
                    initSize: function() {
                        var n = this, e = [], o = new Promise(function(e, o) {
                            t.createSelectorQuery().in(n).select("#u-notice-content").boundingClientRect().exec(function(t) {
                                n.textWidth = t[0].width, e();
                            });
                        });
                        e.push(o), Promise.all(e).then(function() {
                            n.animationDuration = "".concat(n.textWidth / t.upx2px(n.speed), "s"), n.animationPlayState = "paused", 
                            setTimeout(function() {
                                "play" == n.playState && n.autoplay && (n.animationPlayState = "running");
                            }, 10);
                        });
                    },
                    click: function(t) {
                        this.$emit("click");
                    },
                    close: function() {
                        this.$emit("close");
                    },
                    getMore: function() {
                        this.$emit("getMore");
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-row-notice/u-row-notice-create-component", {
    "uview-ui/components/u-row-notice/u-row-notice-create-component": function(t, n, e) {
        e("543d").createComponent(e("4daa"));
    }
}, [ [ "uview-ui/components/u-row-notice/u-row-notice-create-component" ] ] ]);