(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-read-more/u-read-more" ], {
    "18ba": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                name: "u-read-more",
                props: {
                    showHeight: {
                        type: [ Number, String ],
                        default: 400
                    },
                    toggle: {
                        type: Boolean,
                        default: !1
                    },
                    closeText: {
                        type: String,
                        default: "展开阅读全文"
                    },
                    openText: {
                        type: String,
                        default: "收起"
                    },
                    color: {
                        type: String,
                        default: "#2979ff"
                    },
                    fontSize: {
                        type: [ String, Number ],
                        default: 28
                    },
                    shadowStyle: {
                        type: Object,
                        default: function() {
                            return {
                                backgroundImage: "linear-gradient(-180deg, rgba(255, 255, 255, 0) 0%, #fff 80%)",
                                paddingTop: "300rpx",
                                marginTop: "-300rpx"
                            };
                        }
                    },
                    textIndent: {
                        type: String,
                        default: "2em"
                    },
                    index: {
                        type: [ Number, String ],
                        default: ""
                    }
                },
                watch: {
                    paramsChange: function(t) {
                        this.init();
                    }
                },
                computed: {
                    paramsChange: function() {
                        return "".concat(this.toggle, "-").concat(this.showHeight);
                    },
                    innerShadowStyle: function() {
                        return this.showMore ? {} : this.shadowStyle;
                    }
                },
                data: function() {
                    return {
                        isLongContent: !1,
                        showMore: !1,
                        elId: this.$u.guid()
                    };
                },
                mounted: function() {
                    this.$nextTick(function() {
                        this.init();
                    });
                },
                methods: {
                    init: function() {
                        var e = this;
                        this.$uGetRect("." + this.elId).then(function(n) {
                            n.height > t.upx2px(e.showHeight) && (e.isLongContent = !0, e.showMore = !1);
                        });
                    },
                    toggleReadMore: function() {
                        this.showMore = !this.showMore, 0 == this.toggle && (this.isLongContent = !1), this.$emit(this.showMore ? "open" : "close", this.index);
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    "432c": function(t, e, n) {
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {
            return o;
        });
        var o = {
            uIcon: function() {
                return n.e("uview-ui/components/u-icon/u-icon").then(n.bind(null, "71ff"));
            }
        }, i = function() {
            this.$createElement;
            var t = (this._self._c, this.isLongContent ? this.__get_style([ this.innerShadowStyle ]) : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    s0: t
                }
            });
        }, u = [];
    },
    "50bb": function(t, e, n) {
        var o = n("6f5a");
        n.n(o).a;
    },
    5395: function(t, e, n) {
        n.r(e);
        var o = n("18ba"), i = n.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(u);
        e.default = i.a;
    },
    "6f5a": function(t, e, n) {},
    cbb5: function(t, e, n) {
        n.r(e);
        var o = n("432c"), i = n("5395");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(u);
        n("50bb");
        var r = n("f0c5"), a = Object(r.a)(i.default, o.b, o.c, !1, null, "033da2a4", null, !1, o.a, void 0);
        e.default = a.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-read-more/u-read-more-create-component", {
    "uview-ui/components/u-read-more/u-read-more-create-component": function(t, e, n) {
        n("543d").createComponent(n("cbb5"));
    }
}, [ [ "uview-ui/components/u-read-more/u-read-more-create-component" ] ] ]);