(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-notice-bar/u-notice-bar" ], {
    "04ba": function(e, t, n) {
        n.r(t);
        var o = n("460c"), u = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        t.default = u.a;
    },
    "35fa": function(e, t, n) {},
    "460c": function(e, t, n) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = {
            name: "u-notice-bar",
            props: {
                list: {
                    type: Array,
                    default: function() {
                        return [];
                    }
                },
                type: {
                    type: String,
                    default: "warning"
                },
                volumeIcon: {
                    type: Boolean,
                    default: !0
                },
                volumeSize: {
                    type: [ Number, String ],
                    default: 34
                },
                moreIcon: {
                    type: Boolean,
                    default: !1
                },
                closeIcon: {
                    type: Boolean,
                    default: !1
                },
                autoplay: {
                    type: Boolean,
                    default: !0
                },
                color: {
                    type: String,
                    default: ""
                },
                bgColor: {
                    type: String,
                    default: ""
                },
                mode: {
                    type: String,
                    default: "horizontal"
                },
                show: {
                    type: Boolean,
                    default: !0
                },
                fontSize: {
                    type: [ Number, String ],
                    default: 28
                },
                duration: {
                    type: [ Number, String ],
                    default: 2e3
                },
                speed: {
                    type: [ Number, String ],
                    default: 160
                },
                isCircular: {
                    type: Boolean,
                    default: !0
                },
                playState: {
                    type: String,
                    default: "play"
                },
                disableTouch: {
                    type: Boolean,
                    default: !0
                },
                borderRadius: {
                    type: [ Number, String ],
                    default: 0
                },
                padding: {
                    type: [ Number, String ],
                    default: "18rpx 24rpx"
                },
                noListHidden: {
                    type: Boolean,
                    default: !0
                }
            },
            computed: {
                isShow: function() {
                    return 0 != this.show && (1 != this.noListHidden || 0 != this.list.length);
                }
            },
            methods: {
                click: function(e) {
                    this.$emit("click", e);
                },
                close: function() {
                    this.$emit("close");
                },
                getMore: function() {
                    this.$emit("getMore");
                },
                end: function() {
                    this.$emit("end");
                }
            }
        };
        t.default = o;
    },
    ab6b: function(e, t, n) {
        n.d(t, "b", function() {
            return u;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            uRowNotice: function() {
                return n.e("uview-ui/components/u-row-notice/u-row-notice").then(n.bind(null, "4daa"));
            },
            uColumnNotice: function() {
                return n.e("uview-ui/components/u-column-notice/u-column-notice").then(n.bind(null, "49b5"));
            }
        }, u = function() {
            this.$createElement;
            this._self._c;
        }, i = [];
    },
    f1e5: function(e, t, n) {
        var o = n("35fa");
        n.n(o).a;
    },
    f2de: function(e, t, n) {
        n.r(t);
        var o = n("ab6b"), u = n("04ba");
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return u[e];
            });
        }(i);
        n("f1e5");
        var a = n("f0c5"), r = Object(a.a)(u.default, o.b, o.c, !1, null, "358116f6", null, !1, o.a, void 0);
        t.default = r.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-notice-bar/u-notice-bar-create-component", {
    "uview-ui/components/u-notice-bar/u-notice-bar-create-component": function(e, t, n) {
        n("543d").createComponent(n("f2de"));
    }
}, [ [ "uview-ui/components/u-notice-bar/u-notice-bar-create-component" ] ] ]);