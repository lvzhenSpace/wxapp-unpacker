(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-switch/u-switch" ], {
    "272f": function(t, e, n) {},
    "52b7": function(t, e, n) {
        n.r(e);
        var i = n("7748"), o = n("b509");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(u);
        n("e03e");
        var a = n("f0c5"), c = Object(a.a)(o.default, i.b, i.c, !1, null, "c54dc83c", null, !1, i.a, void 0);
        e.default = c.exports;
    },
    "58fb": function(t, e, n) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                name: "u-switch",
                props: {
                    loading: {
                        type: Boolean,
                        default: !1
                    },
                    disabled: {
                        type: Boolean,
                        default: !1
                    },
                    size: {
                        type: [ Number, String ],
                        default: 50
                    },
                    activeColor: {
                        type: String,
                        default: "#2979ff"
                    },
                    inactiveColor: {
                        type: String,
                        default: "#ffffff"
                    },
                    value: {
                        type: Boolean,
                        default: !1
                    },
                    vibrateShort: {
                        type: Boolean,
                        default: !1
                    },
                    activeValue: {
                        type: [ Number, String, Boolean ],
                        default: !0
                    },
                    inactiveValue: {
                        type: [ Number, String, Boolean ],
                        default: !1
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    switchStyle: function() {
                        var t = {};
                        return t.fontSize = this.size + "rpx", t.backgroundColor = this.value ? this.activeColor : this.inactiveColor, 
                        t;
                    },
                    loadingColor: function() {
                        return this.value ? this.activeColor : null;
                    }
                },
                methods: {
                    onClick: function() {
                        var e = this;
                        this.disabled || this.loading || (this.vibrateShort && t.vibrateShort(), this.$emit("input", !this.value), 
                        this.$nextTick(function() {
                            e.$emit("change", e.value ? e.activeValue : e.inactiveValue);
                        }));
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    7748: function(t, e, n) {
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            uLoading: function() {
                return n.e("uview-ui/components/u-loading/u-loading").then(n.bind(null, "0178"));
            }
        }, o = function() {
            this.$createElement;
            var t = (this._self._c, this.__get_style([ this.switchStyle ])), e = this.$u.addUnit(this.size), n = this.$u.addUnit(this.size);
            this.$mp.data = Object.assign({}, {
                $root: {
                    s0: t,
                    g0: e,
                    g1: n
                }
            });
        }, u = [];
    },
    b509: function(t, e, n) {
        n.r(e);
        var i = n("58fb"), o = n.n(i);
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(u);
        e.default = o.a;
    },
    e03e: function(t, e, n) {
        var i = n("272f");
        n.n(i).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-switch/u-switch-create-component", {
    "uview-ui/components/u-switch/u-switch-create-component": function(t, e, n) {
        n("543d").createComponent(n("52b7"));
    }
}, [ [ "uview-ui/components/u-switch/u-switch-create-component" ] ] ]);