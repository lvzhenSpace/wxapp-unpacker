(global.webpackJsonp = global.webpackJsonp || []).push([ [ "uview-ui/components/u-icon/u-icon" ], {
    4491: function(t, e, i) {
        i.d(e, "b", function() {
            return n;
        }), i.d(e, "c", function() {
            return u;
        }), i.d(e, "a", function() {});
        var n = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__get_style([ t.customStyle ])), i = t.isImg ? t.__get_style([ t.imgStyle ]) : null, n = t.isImg ? null : t.__get_style([ t.iconStyle ]), u = "" !== t.label ? t.$u.addUnit(t.labelSize) : null, o = "" !== t.label && "right" == t.labelPos ? t.$u.addUnit(t.marginLeft) : null, l = "" !== t.label && "bottom" == t.labelPos ? t.$u.addUnit(t.marginTop) : null, a = "" !== t.label && "left" == t.labelPos ? t.$u.addUnit(t.marginRight) : null, r = "" !== t.label && "top" == t.labelPos ? t.$u.addUnit(t.marginBottom) : null;
            t.$mp.data = Object.assign({}, {
                $root: {
                    s0: e,
                    s1: i,
                    s2: n,
                    g0: u,
                    g1: o,
                    g2: l,
                    g3: a,
                    g4: r
                }
            });
        }, u = [];
    },
    "4cb3": function(t, e, i) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var n = {
            name: "u-icon",
            props: {
                name: {
                    type: String,
                    default: ""
                },
                color: {
                    type: String,
                    default: ""
                },
                size: {
                    type: [ Number, String ],
                    default: "inherit"
                },
                bold: {
                    type: Boolean,
                    default: !1
                },
                index: {
                    type: [ Number, String ],
                    default: ""
                },
                hoverClass: {
                    type: String,
                    default: ""
                },
                customPrefix: {
                    type: String,
                    default: "uicon"
                },
                label: {
                    type: [ String, Number ],
                    default: ""
                },
                labelPos: {
                    type: String,
                    default: "right"
                },
                labelSize: {
                    type: [ String, Number ],
                    default: "28"
                },
                labelColor: {
                    type: String,
                    default: "#606266"
                },
                marginLeft: {
                    type: [ String, Number ],
                    default: "6"
                },
                marginTop: {
                    type: [ String, Number ],
                    default: "6"
                },
                marginRight: {
                    type: [ String, Number ],
                    default: "6"
                },
                marginBottom: {
                    type: [ String, Number ],
                    default: "6"
                },
                imgMode: {
                    type: String,
                    default: "widthFix"
                },
                customStyle: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                width: {
                    type: [ String, Number ],
                    default: ""
                },
                height: {
                    type: [ String, Number ],
                    default: ""
                },
                top: {
                    type: [ String, Number ],
                    default: 0
                }
            },
            computed: {
                customClass: function() {
                    var t = [];
                    return t.push(this.customPrefix + "-" + this.name), "uicon" == this.customPrefix ? t.push("u-iconfont") : t.push(this.customPrefix), 
                    this.color && this.$u.config.type.includes(this.color) && t.push("u-icon__icon--" + this.color), 
                    t;
                },
                iconStyle: function() {
                    var t = {};
                    return t = {
                        fontSize: "inherit" == this.size ? "inherit" : this.$u.addUnit(this.size),
                        fontWeight: this.bold ? "bold" : "normal",
                        top: this.$u.addUnit(this.top)
                    }, this.color && !this.$u.config.type.includes(this.color) && (t.color = this.color), 
                    t;
                },
                isImg: function() {
                    return -1 !== this.name.indexOf("/");
                },
                imgStyle: function() {
                    var t = {};
                    return t.width = this.width ? this.$u.addUnit(this.width) : this.$u.addUnit(this.size), 
                    t.height = this.height ? this.$u.addUnit(this.height) : this.$u.addUnit(this.size), 
                    t;
                }
            },
            methods: {
                click: function() {
                    this.$emit("click", this.index);
                },
                touchstart: function() {
                    this.$emit("touchstart", this.index);
                }
            }
        };
        e.default = n;
    },
    "71ff": function(t, e, i) {
        i.r(e);
        var n = i("4491"), u = i("c18a");
        for (var o in u) [ "default" ].indexOf(o) < 0 && function(t) {
            i.d(e, t, function() {
                return u[t];
            });
        }(o);
        i("80ba");
        var l = i("f0c5"), a = Object(l.a)(u.default, n.b, n.c, !1, null, "07972b5e", null, !1, n.a, void 0);
        e.default = a.exports;
    },
    "80ba": function(t, e, i) {
        var n = i("aaa9");
        i.n(n).a;
    },
    aaa9: function(t, e, i) {},
    c18a: function(t, e, i) {
        i.r(e);
        var n = i("4cb3"), u = i.n(n);
        for (var o in n) [ "default" ].indexOf(o) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(o);
        e.default = u.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "uview-ui/components/u-icon/u-icon-create-component", {
    "uview-ui/components/u-icon/u-icon-create-component": function(t, e, i) {
        i("543d").createComponent(i("71ff"));
    }
}, [ [ "uview-ui/components/u-icon/u-icon-create-component" ] ] ]);