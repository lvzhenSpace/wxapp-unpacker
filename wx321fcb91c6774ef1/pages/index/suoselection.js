(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/suoselection" ], {
    "0581": function(t, e, i) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = {
                components: {
                    noticePop: function() {
                        i.e("pages/index/noticepop").then(function() {
                            return resolve(i("ed6e"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    tishiPop: function() {
                        i.e("components/pupop/tishika").then(function() {
                            return resolve(i("8e0c"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        home: 2,
                        urls: this.$configs.urls,
                        cat_index: "",
                        catList: [ {
                            name: "现货"
                        }, {
                            name: "预售"
                        } ],
                        goodstList: [],
                        page: 1,
                        status: 0,
                        allStatus: !1,
                        ordidlist: [],
                        explainData: {
                            content: ""
                        },
                        huisodata: {
                            prize_list: []
                        },
                        order_id: [],
                        chushoudata: {
                            prize_list: []
                        },
                        chuorder_id: [],
                        pay_price: "",
                        fahuoData: {},
                        fhzongshu: 0,
                        yunzongjia: 0,
                        order_idarr: [],
                        Allorder_id: [],
                        address_id: ""
                    };
                },
                onLoad: function(t) {},
                onPageScroll: function(t) {
                    this.scrollTop = t.scrollTop;
                },
                onShow: function() {
                    var e = this;
                    t.$on("changedz", function(i) {
                        e.fahuoData.address = i.local, e.fahuoData.address.address_id = i.local.id, e.address_id = i.local.id, 
                        t.$off("changedz");
                    }), this.order_idarr = [], this.getlist();
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.goodstList = [], this.getlist();
                },
                onReachBottom: function() {
                    this.page++, this.getlist(2);
                },
                onHide: function() {
                    this.page = 1, this.allStatus = !1, this.order_idarr = [];
                },
                methods: {
                    enterhome: function(t) {
                        this.home = t.home;
                    },
                    entertab: function(t) {
                        this.page = 1, this.status = t, this.getlist();
                    },
                    goexplain: function() {
                        var e = this;
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.get(this.$api.index.wenzhang, {
                            id: 7
                        }).then(function(i) {
                            t.hideLoading(), e.explainData = i.data, e.$refs.noticepop.open();
                        });
                    },
                    getPrice: function() {
                        this.yunzongjia = 0;
                        for (var t = this.fahuoData.prize_list, e = 0; e < t.length; e++) {
                            this.yunzongjia += Number(t[e].recovery_price) * Number(t[e].buyCount);
                            var i = Math.floor(100 * this.yunzongjia) / 100;
                            this.yunzongjia = i;
                        }
                    },
                    gettuihuoYulan: function() {
                        var e = this, i = {
                            order_id: this.order_id
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.recoveryPreview, i).then(function(i) {
                            e.$refs.tuihuo_pop.open(), t.hideLoading(), e.fahuoData = i.data;
                            var o = e, n = o.fahuoData.prize_list;
                            e.fhzongshu = 0;
                            for (var r = 0; r < n.length; r++) o.$set(n[r], "buyCount", 1), e.fhzongshu = Number(e.fhzongshu) + Number(n[r].buyCount);
                            n.map(function(t, e) {
                                if (null != t.recovery_price) {
                                    o.yunzongjia += Number(t.recovery_price) * Number(t.buyCount);
                                    var i = Math.floor(100 * o.yunzongjia) / 100;
                                    o.yunzongjia = i;
                                }
                            }), console.log(n), t.setStorageSync("order_idarr", e.order_idarr), "" != e.order_idarr && null != e.order_idarr || (e.order_idarr = t.getStorageSync("order_idarr"));
                        });
                    },
                    getfahuoYulan: function() {
                        var e = this, i = {
                            order_id: this.order_id
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.sendOrder, i).then(function(i) {
                            e.$refs.fahuo_pop.open(), t.hideLoading(), e.fahuoData = i.data, console.log(1, i), 
                            e.address_id = i.data.address.address_id;
                            var o = e, n = o.fahuoData.prize_list;
                            o.fhzongshu = 0;
                            for (var r = 0; r < n.length; r++) o.$set(n[r], "buyCount", 1), o.fhzongshu = Number(o.fhzongshu) + Number(n[r].buyCount);
                            n.map(function(t, e) {
                                if (null != t.recovery_price) {
                                    o.yunzongjia += Number(t.recovery_price) * Number(t.buyCount);
                                    var i = Math.floor(100 * o.yunzongjia) / 100;
                                    o.yunzongjia = i;
                                }
                            }), console.log(n), t.setStorageSync("order_idarr", o.order_idarr), "" != o.order_idarr && null != o.order_idarr || (o.order_idarr = t.getStorageSync("order_idarr"));
                        });
                    },
                    backbxgPop: function() {
                        this.bxgdata = {};
                    },
                    submit_yunfahuo: function() {
                        var e = this, i = {
                            order_id: this.Allorder_id
                        };
                        this.$Request.post(this.$api.order.sendYun, i).then(function(i) {
                            t.showToast({
                                title: i.msg,
                                icon: "none"
                            }), e.$refs.tuihuo_pop.fhpopclose(), setTimeout(function() {
                                e.order_id = [], e.order_idarr = [], t.navigateTo({
                                    url: "/luckdraw/pages/user/recoveryRecord"
                                });
                            }, 1e3);
                        });
                    },
                    getOrderno: function(t, e) {
                        var i = this, o = {
                            order_id: t
                        };
                        this.$Request.get(this.$api.order.sendOrderPayData, o).then(function(t) {
                            i.payPrice(t.data.pay_data);
                        });
                    },
                    payPrice: function(e) {
                        var i = this;
                        t.requestPayment({
                            provider: "wxpay",
                            timeStamp: e.timeStamp,
                            nonceStr: e.nonceStr,
                            package: e.package,
                            signType: e.signType,
                            paySign: e.paySign,
                            success: function(e) {
                                i.$refs.fahuo_pop.fhpopclose(), setTimeout(function() {
                                    t.navigateTo({
                                        url: "/luckdraw/pages/user/fahuo"
                                    });
                                }, 1e3);
                            },
                            fail: function(t) {
                                i.$u.toast("支付失败");
                            }
                        });
                    },
                    submit_GO: function(t) {
                        if (t) {
                            for (var e in t.buyCountarr) t.prize_data[e].num = Number(t.buyCountarr[e]);
                            this.Allorder_id = t.prize_data;
                        }
                        this.$refs.tishika_pop.open();
                    },
                    submit_fahuo: function(e) {
                        var i = this;
                        if (e) {
                            for (var o in e.buyCountarr) e.prize_data[o].num = e.buyCountarr[o];
                            this.Allorder_id = e.prize_data;
                        }
                        var n = {
                            order_id: this.Allorder_id,
                            address_id: this.address_id,
                            pay_type: 1,
                            remark: e.remark
                        };
                        t.showLoading(), this.$Request.post(this.$api.order.sendOrderSubmit, n).then(function(e) {
                            t.hideLoading(), 0 == e.data.status ? i.getOrderno(e.data.order_id, 1) : 1 == e.data.status && (t.showToast({
                                title: e.msg,
                                icon: "none"
                            }), i.$refs.fahuo_pop.fhpopclose(), setTimeout(function() {
                                t.navigateTo({
                                    url: "/luckdraw/pages/user/fahuo"
                                });
                            }, 800));
                        });
                    },
                    famall_delete: function(t) {
                        var e = this.fahuoData.prize_list;
                        e.splice(t.index, 1), "" == e && this.$refs.fahuo_pop.fhpopclose();
                    },
                    fahandleBuyCount: function(e) {
                        var i = this.fahuoData.prize_list, o = i[e.i].num, n = i[e.i].buyCount;
                        if ("min" === e.type) {
                            if (1 == n) return n = 1, this.getNumber(i, e.type), void this.getPrice();
                            n = 1 * n - 1, this.$set(i[e.i], "buyCount", n), this.getNumber(i, e.type), this.getPrice();
                        }
                        if ("add" === e.type) {
                            if (n >= o) return t.showToast({
                                title: "数量不足",
                                icon: "none"
                            }), this.getNumber(i, e.type), void this.getPrice();
                            n = 1 * n + 1, this.$set(i[e.i], "buyCount", n), this.getNumber(i, e.type), this.getPrice();
                        }
                    },
                    getNumber: function(t, e) {
                        if (1 == e) this.fhzongshu -= 1; else {
                            this.fhzongshu = 0;
                            for (var i = 0; i < t.length; i++) this.fhzongshu += Number(t[i].buyCount);
                        }
                        console.log(this.fhzongshu);
                    },
                    govalue: function(t) {
                        this.$set(this.fahuoData.prize_list[t.i], "buyCount", t.value);
                    },
                    backpopchu: function() {
                        this.$refs.chupop.close();
                    },
                    gochushou: function() {
                        var e = this;
                        if ("" != this.pay_price) {
                            var i = {
                                order_id: this.chuorder_id,
                                pay_price: this.pay_price
                            };
                            t.showLoading({
                                title: "加载中..."
                            }), this.$Request.post(this.$api.order.sellSubmit, i).then(function(i) {
                                t.hideLoading(), e.$refs.chupop.close(), t.showToast({
                                    title: i.msg,
                                    icon: "none"
                                }), e.pay_price = "", setTimeout(function() {
                                    e.order_idarr = [], e.getlist();
                                }, 800);
                            });
                        } else t.showToast({
                            title: "请输入定价",
                            icon: "none"
                        });
                    },
                    getchuYulan: function() {
                        var e = this, i = {
                            order_id: this.chuorder_id
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.sellPreview, i).then(function(i) {
                            t.hideLoading(), e.chushoudata = i.data, e.$refs.chupop.open();
                        });
                    },
                    backpop: function() {
                        this.$refs.huishoupop.close();
                    },
                    gohuishou: function() {
                        var e = this, i = {
                            order_id: this.order_id
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.exchange, i).then(function(i) {
                            t.hideLoading(), t.showToast({
                                title: i.msg,
                                icon: "none"
                            }), setTimeout(function() {
                                e.order_idarr = [], e.getlist();
                            }, 800), e.$refs.huishoupop.close();
                        });
                    },
                    getshouYulan: function() {
                        var e = this, i = {
                            order_id: this.order_id
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.exchangePreview, i).then(function(i) {
                            e.$refs.huishoupop.open(), t.hideLoading(), e.huisodata = i.data;
                        });
                    },
                    shuaxin: function() {
                        this.ordidlist = [], this.page = 1, this.getlist();
                    },
                    quanxuan: function() {
                        this.allStatus = !this.allStatus;
                        for (var t = this.goodstList, e = 0; e < t.length; e++) this.allStatus ? t[e].check = !0 : t[e].check = !1;
                        this.order_idarr = this.getCarIds();
                    },
                    getcheck: function(t) {
                        var e = this.goodstList;
                        0 == e[t].check ? e[t].check = !0 : e[t].check = !1, this.setAllSel(t), this.order_idarr = this.getCarIds();
                    },
                    setAllSel: function(t) {
                        for (var e = 0, i = 0; i < this.goodstList.length; i++) 1 == this.goodstList[i].check && (e += 1);
                        e == this.goodstList.length && e > 0 ? this.allStatus = !0 : this.allStatus = !1;
                    },
                    getCarIds: function() {
                        for (var t = this.goodstList, e = [], i = 0; i < t.length; i++) 1 == t[i].check && e.push({
                            order_id: t[i].order_id
                        });
                        return 0 != e.length && e;
                    },
                    getlist: function(e) {
                        var i = this, o = {
                            page: this.page
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.get(this.$api.order.lockList, o).then(function(o) {
                            t.stopPullDownRefresh(), t.hideLoading(), i.goodstList = 2 != e ? o.data.list : i.goodstList.concat(o.data.list);
                        });
                    },
                    goyichu: function() {
                        if ("" != this.getCarIds()) {
                            this.order_id = this.getCarIds();
                            var e = {
                                order_id: this.order_id
                            };
                            this.$Request.post(this.$api.order.lockRemove, e).then(function(e) {
                                t.navigateBack();
                            });
                        } else t.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    changeClass: function(t) {
                        this.page = 1, this.cat_index = t, this.status = this.cat_index, this.getlist();
                    }
                }
            };
            e.default = o;
        }).call(this, i("543d").default);
    },
    "0a6f": function(t, e, i) {},
    "12f9": function(t, e, i) {
        i.d(e, "b", function() {
            return n;
        }), i.d(e, "c", function() {
            return r;
        }), i.d(e, "a", function() {
            return o;
        });
        var o = {
            uniPopup: function() {
                return Promise.all([ i.e("common/vendor"), i.e("components/uni-popup/uni-popup") ]).then(i.bind(null, "ce14"));
            }
        }, n = function() {
            var t = this, e = (t.$createElement, t._self._c, t.huisodata.prize_list.length), i = t.chushoudata.prize_list.length;
            t._isMounted || (t.e0 = function(e) {
                return t.$u.throttle(t.goyichu, 1e3);
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e,
                    g1: i
                }
            });
        }, r = [];
    },
    "1add": function(t, e, i) {
        (function(t, e) {
            var o = i("4ea4");
            i("8740"), o(i("66fd"));
            var n = o(i("4a9a"));
            t.__webpack_require_UNI_MP_PLUGIN__ = i, e(n.default);
        }).call(this, i("bc2e").default, i("543d").createPage);
    },
    "22a2": function(t, e, i) {
        i.r(e);
        var o = i("0581"), n = i.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(t) {
            i.d(e, t, function() {
                return o[t];
            });
        }(r);
        e.default = n.a;
    },
    "4a9a": function(t, e, i) {
        i.r(e);
        var o = i("12f9"), n = i("22a2");
        for (var r in n) [ "default" ].indexOf(r) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(r);
        i("a891");
        var a = i("f0c5"), s = Object(a.a)(n.default, o.b, o.c, !1, null, "6e54e07a", null, !1, o.a, void 0);
        e.default = s.exports;
    },
    a891: function(t, e, i) {
        var o = i("0a6f");
        i.n(o).a;
    }
}, [ [ "1add", "common/runtime", "common/vendor" ] ] ]);