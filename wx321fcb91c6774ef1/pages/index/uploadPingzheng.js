(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/uploadPingzheng" ], {
    "00ff": function(e, n, t) {
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return i;
        }), t.d(n, "a", function() {});
        var o = function() {
            this.$createElement;
            var e = (this._self._c, this.imgs.length);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, i = [];
    },
    "1c68": function(e, n, t) {
        t.r(n);
        var o = t("00ff"), i = t("420a");
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(a);
        t("582e");
        var u = t("f0c5"), c = Object(u.a)(i.default, o.b, o.c, !1, null, "6898a466", null, !1, o.a, void 0);
        n.default = c.exports;
    },
    "420a": function(e, n, t) {
        t.r(n);
        var o = t("49ed"), i = t.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(a);
        n.default = i.a;
    },
    "49ed": function(e, n, t) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var t = {
                data: function() {
                    return {
                        urls: this.$configs.urls,
                        order_id: "",
                        imgs: []
                    };
                },
                onLoad: function(e) {
                    this.order_id = e.order_id;
                },
                methods: {
                    submit: function() {
                        var n = {
                            order_id: this.order_id,
                            imgs: this.imgs
                        };
                        this.$Request.post(this.$api.zeroBuy.addVoucher, n).then(function(n) {
                            e.showToast({
                                title: n.msg,
                                duration: 1500,
                                icon: "none",
                                success: function() {
                                    setTimeout(function() {
                                        e.navigateBack();
                                    }, 1500);
                                }
                            });
                        });
                    },
                    choice: function() {
                        var n = this;
                        e.chooseImage({
                            count: 1,
                            sourceType: [ "album", "camera" ],
                            success: function(t) {
                                var o = t.tempFilePaths[0];
                                console.log(o), console.log(n.$configs.APIHOST + n.$api.user.uploadfile), e.uploadFile({
                                    url: n.$configs.APIHOST + n.$api.user.uploadfile,
                                    filePath: o,
                                    name: "file",
                                    header: {
                                        Authorization: "Bearer " + e.getStorageSync("USERINFO")
                                    },
                                    success: function(o) {
                                        var i = JSON.parse(o.data);
                                        0 == i.code ? n.imgs.push(i.data.url) : 406 == i.code ? e.showToast({
                                            title: "请登录",
                                            duration: 1e3,
                                            icon: "none"
                                        }) : e.showToast({
                                            title: t.msg,
                                            duration: 1e3,
                                            icon: "none"
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            };
            n.default = t;
        }).call(this, t("543d").default);
    },
    "582e": function(e, n, t) {
        var o = t("81d8");
        t.n(o).a;
    },
    "81d8": function(e, n, t) {},
    b766: function(e, n, t) {
        (function(e, n) {
            var o = t("4ea4");
            t("8740"), o(t("66fd"));
            var i = o(t("1c68"));
            e.__webpack_require_UNI_MP_PLUGIN__ = t, n(i.default);
        }).call(this, t("bc2e").default, t("543d").createPage);
    }
}, [ [ "b766", "common/runtime", "common/vendor" ] ] ]);