(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/selection" ], {
    "05d8": function(t, e, i) {},
    "6dd5": function(t, e, i) {
        (function(t, o) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                components: {
                    faPop: function() {
                        i.e("components/pop/fahuo").then(function() {
                            return resolve(i("b771"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    noticePop: function() {
                        i.e("pages/index/noticepop").then(function() {
                            return resolve(i("ed6e"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    tuiPop: function() {
                        i.e("components/pop/tuihuo").then(function() {
                            return resolve(i("3d00"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    tishiPop: function() {
                        i.e("components/pupop/tishika").then(function() {
                            return resolve(i("8e0c"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        home: 2,
                        urls: this.$configs.urls,
                        cat_index: "",
                        catList: [ {
                            name: "现货"
                        }, {
                            name: "预售"
                        } ],
                        goodstList: [],
                        page: 1,
                        status: 0,
                        allStatus: !1,
                        ordidlist: [],
                        explainData: {
                            content: ""
                        },
                        huisodata: {
                            prize_list: []
                        },
                        order_id: [],
                        chushoudata: {
                            prize_list: []
                        },
                        chuorder_id: [],
                        pay_price: "",
                        fahuoData: {},
                        fhzongshu: 0,
                        yunzongjia: 0,
                        order_idarr: [],
                        Allorder_id: [],
                        address_id: "",
                        topHeight: 0
                    };
                },
                onReady: function() {
                    this.topHeight = t.getMenuButtonBoundingClientRect().top;
                },
                onLoad: function() {
                    o.hideTabBar();
                },
                onPageScroll: function(t) {
                    this.scrollTop = t.scrollTop;
                },
                onShow: function() {
                    var t = this;
                    o.$on("changedz", function(e) {
                        t.fahuoData.address = e.local, t.fahuoData.address.address_id = e.local.id, t.address_id = e.local.id, 
                        o.$off("changedz");
                    }), this.order_idarr = [], this.getlist();
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.goodstList = [], this.getlist();
                },
                onReachBottom: function() {
                    this.page++, this.getlist(2);
                },
                onHide: function() {
                    this.page = 1, this.allStatus = !1, this.order_idarr = [];
                },
                methods: {
                    enterhome: function(t) {
                        this.home = t.home;
                    },
                    entertab: function(t) {
                        this.page = 1, this.status = t, this.getlist();
                    },
                    goexplain: function() {
                        var t = this;
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.get(this.$api.index.wenzhang, {
                            id: 7
                        }).then(function(e) {
                            o.hideLoading(), t.explainData = e.data, t.$refs.noticepop.open();
                        });
                    },
                    getPrice: function() {
                        this.yunzongjia = 0;
                        for (var t = this.fahuoData.prize_list, e = 0; e < t.length; e++) {
                            this.yunzongjia += Number(t[e].recovery_price) * Number(t[e].buyCount);
                            var i = Math.floor(100 * this.yunzongjia) / 100;
                            this.yunzongjia = i;
                        }
                    },
                    gettuihuoYulan: function() {
                        var t = this, e = {
                            order_id: this.order_id
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.recoveryPreview, e).then(function(e) {
                            t.$refs.tuihuo_pop.open(), o.hideLoading(), t.fahuoData = e.data;
                            var i = t, n = i.fahuoData.prize_list;
                            t.fhzongshu = 0;
                            for (var s = 0; s < n.length; s++) i.$set(n[s], "buyCount", n[s].num), t.fhzongshu = Number(t.fhzongshu) + Number(n[s].buyCount);
                            n.map(function(t, e) {
                                if (null != t.recovery_price) {
                                    i.yunzongjia += Number(t.recovery_price) * Number(t.buyCount);
                                    var o = Math.floor(100 * i.yunzongjia) / 100;
                                    i.yunzongjia = o;
                                }
                            }), console.log(n), o.setStorageSync("order_idarr", t.order_idarr), "" != t.order_idarr && null != t.order_idarr || (t.order_idarr = o.getStorageSync("order_idarr"));
                        });
                    },
                    getfahuoYulan: function() {
                        var t = this, e = {
                            order_id: this.order_id
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.sendOrder, e).then(function(e) {
                            t.$refs.fahuo_pop.open(), o.hideLoading(), t.fahuoData = e.data, t.address_id = e.data.address ? e.data.address.address_id : 0;
                            var i = t, n = i.fahuoData.prize_list;
                            console.log("111111111111"), i.fhzongshu = 0;
                            for (var s = 0; s < n.length; s++) i.$set(n[s], "buyCount", n[s].num), i.fhzongshu = Number(i.fhzongshu) + Number(n[s].buyCount);
                            console.log(i.fhzongshu), n.map(function(t, e) {
                                if (null != t.recovery_price) {
                                    i.yunzongjia += Number(t.recovery_price) * Number(t.buyCount);
                                    var o = Math.floor(100 * i.yunzongjia) / 100;
                                    i.yunzongjia = o;
                                }
                            }), console.log(n), o.setStorageSync("order_idarr", i.order_idarr), "" != i.order_idarr && null != i.order_idarr || (i.order_idarr = o.getStorageSync("order_idarr"));
                        });
                    },
                    backbxgPop: function() {
                        this.bxgdata = {};
                    },
                    submit_yunfahuo: function() {
                        var t = this, e = {
                            order_id: this.Allorder_id
                        };
                        this.$Request.post(this.$api.order.sendYun, e).then(function(e) {
                            o.showToast({
                                title: e.msg,
                                icon: "none"
                            }), t.$refs.tuihuo_pop.fhpopclose(), setTimeout(function() {
                                t.order_id = [], t.order_idarr = [], o.navigateTo({
                                    url: "/luckdraw/pages/user/recoveryRecord"
                                });
                            }, 1e3);
                        });
                    },
                    getOrderno: function(t, e) {
                        var i = this, o = {
                            order_id: t
                        };
                        this.$Request.get(this.$api.order.sendOrderPayData, o).then(function(t) {
                            i.payPrice(t.data.pay_data);
                        });
                    },
                    payPrice: function(t) {
                        var e = this;
                        o.requestPayment({
                            provider: "wxpay",
                            timeStamp: t.timeStamp,
                            nonceStr: t.nonceStr,
                            package: t.package,
                            signType: t.signType,
                            paySign: t.paySign,
                            success: function(t) {
                                e.$refs.fahuo_pop.fhpopclose(), setTimeout(function() {
                                    o.navigateTo({
                                        url: "/luckdraw/pages/user/fahuo"
                                    });
                                }, 1e3);
                            },
                            fail: function(t) {
                                e.$u.toast("支付失败");
                            }
                        });
                    },
                    submit_GO: function(t) {
                        if (t) {
                            for (var e in t.buyCountarr) t.prize_data[e].num = Number(t.buyCountarr[e]);
                            this.Allorder_id = t.prize_data;
                        }
                        this.$refs.tishika_pop.open();
                    },
                    submit_fahuo: function(t) {
                        var e = this;
                        if (t) {
                            for (var i in t.buyCountarr) t.prize_data[i].num = t.buyCountarr[i];
                            this.Allorder_id = t.prize_data;
                        }
                        console.log(t);
                        var n = {
                            order_id: this.Allorder_id,
                            address_id: this.address_id,
                            pay_type: 1,
                            remark: t.remark
                        };
                        o.showLoading(), this.$Request.post(this.$api.order.sendOrderSubmit, n).then(function(t) {
                            o.hideLoading(), 0 == t.data.status ? e.getOrderno(t.data.order_id, 1) : 1 == t.data.status && (o.showToast({
                                title: t.msg,
                                icon: "none"
                            }), e.$refs.fahuo_pop.fhpopclose(), setTimeout(function() {
                                o.navigateTo({
                                    url: "/luckdraw/pages/user/fahuo"
                                });
                            }, 800));
                        });
                    },
                    famall_delete: function(t) {
                        var e = this.fahuoData.prize_list;
                        e.splice(t.index, 1), "" == e && this.$refs.fahuo_pop.fhpopclose();
                    },
                    fahandleBuyCount: function(t) {
                        var e = this.fahuoData.prize_list, i = e[t.i].num, n = e[t.i].buyCount;
                        if ("min" === t.type) {
                            if (1 == n) return n = 1, this.getNumber(e, t.type), void this.getPrice();
                            n = 1 * n - 1, this.$set(e[t.i], "buyCount", n), this.getNumber(e, t.type), this.getPrice();
                        }
                        if ("add" === t.type) {
                            if (n >= i) return o.showToast({
                                title: "数量不足",
                                icon: "none"
                            }), this.getNumber(e, t.type), void this.getPrice();
                            n = 1 * n + 1, this.$set(e[t.i], "buyCount", n), this.getNumber(e, t.type), this.getPrice();
                        }
                    },
                    getNumber: function(t, e) {
                        if (1 == e) this.fhzongshu -= 1; else {
                            this.fhzongshu = 0;
                            for (var i = 0; i < t.length; i++) this.fhzongshu += Number(t[i].buyCount);
                        }
                        console.log(this.fhzongshu);
                    },
                    govalue: function(t) {
                        var e = this.fahuoData.prize_list;
                        this.fhzongshu = 0;
                        for (var i = 0; i < e.length; i++) this.fhzongshu += Number(e[i].buyCount);
                        this.$set(this.fahuoData.prize_list[t.i], "buyCount", t.value);
                    },
                    backpopchu: function() {
                        this.$refs.chupop.close();
                    },
                    gochushou: function() {
                        var t = this;
                        if ("" != this.pay_price) {
                            var e = {
                                order_id: this.chuorder_id,
                                pay_price: this.pay_price
                            };
                            o.showLoading({
                                title: "加载中..."
                            }), this.$Request.post(this.$api.order.sellSubmit, e).then(function(e) {
                                o.hideLoading(), t.$refs.chupop.close(), o.showToast({
                                    title: e.msg,
                                    icon: "none"
                                }), t.pay_price = "", setTimeout(function() {
                                    t.order_idarr = [], t.getlist();
                                }, 800);
                            });
                        } else o.showToast({
                            title: "请输入定价",
                            icon: "none"
                        });
                    },
                    getchuYulan: function() {
                        var t = this, e = {
                            order_id: this.chuorder_id
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.sellPreview, e).then(function(e) {
                            o.hideLoading(), t.chushoudata = e.data, t.$refs.chupop.open();
                        });
                    },
                    backpop: function() {
                        this.$refs.huishoupop.close();
                    },
                    gohuishou: function() {
                        var t = this, e = {
                            order_id: this.order_id
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.exchange, e).then(function(e) {
                            o.hideLoading(), o.showToast({
                                title: e.msg,
                                icon: "none"
                            }), setTimeout(function() {
                                t.order_idarr = [], t.getlist();
                            }, 800), t.$refs.huishoupop.close();
                        });
                    },
                    getshouYulan: function() {
                        var t = this, e = {
                            order_id: this.order_id
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.exchangePreview, e).then(function(e) {
                            t.$refs.huishoupop.open(), o.hideLoading(), t.huisodata = e.data;
                        });
                    },
                    shuaxin: function() {
                        this.ordidlist = [], this.page = 1, this.getlist();
                    },
                    quanxuan: function() {
                        this.allStatus = !this.allStatus;
                        for (var t = this.goodstList, e = 0; e < t.length; e++) this.allStatus ? t[e].check = !0 : t[e].check = !1;
                        this.order_idarr = this.getCarIds();
                    },
                    getcheck: function(t) {
                        var e = this.goodstList;
                        0 == e[t].check ? e[t].check = !0 : e[t].check = !1, this.setAllSel(t), this.order_idarr = this.getCarIds();
                    },
                    setAllSel: function(t) {
                        for (var e = 0, i = 0; i < this.goodstList.length; i++) 1 == this.goodstList[i].check && (e += 1);
                        e == this.goodstList.length && e > 0 ? this.allStatus = !0 : this.allStatus = !1;
                    },
                    getCarIds: function() {
                        for (var t = this.goodstList, e = [], i = 0; i < t.length; i++) 1 == t[i].check && e.push({
                            order_id: t[i].order_id
                        });
                        return 0 != e.length && e;
                    },
                    getlist: function(t) {
                        var e = this, i = {
                            page: this.page,
                            status: this.status
                        };
                        o.showLoading({
                            title: "加载中..."
                        }), this.$Request.get(this.$api.order.orderList, i).then(function(i) {
                            o.stopPullDownRefresh(), o.hideLoading(), e.goodstList = 2 != t ? i.data.list : e.goodstList.concat(i.data.list);
                        });
                    },
                    gotuihuo: function() {
                        "" != this.getCarIds() ? (this.order_id = this.getCarIds(), this.gettuihuoYulan()) : o.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    enterbaoxian: function() {
                        if ("" != this.getCarIds()) {
                            this.order_id = this.getCarIds();
                            var t = {
                                order_id: this.order_id
                            };
                            this.$Request.post(this.$api.order.lockAdd, t).then(function(t) {
                                o.navigateTo({
                                    url: "/pages/index/suoselection"
                                });
                            });
                        } else o.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    gobaoxian: function() {
                        o.navigateTo({
                            url: "/pages/index/suoselection"
                        });
                    },
                    gofahuo: function() {
                        "" != this.getCarIds() ? (this.order_id = this.getCarIds(), this.getfahuoYulan()) : o.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    huishou: function() {
                        "" != this.getCarIds() ? (this.order_id = this.getCarIds(), this.getshouYulan()) : o.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    chushou: function() {
                        "" != this.getCarIds() ? (this.chuorder_id = this.getCarIds(), this.getchuYulan()) : o.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    changeClass: function(t) {
                        this.page = 1, this.cat_index = t, this.status = this.cat_index, this.getlist();
                    }
                }
            };
            e.default = n;
        }).call(this, i("bc2e").default, i("543d").default);
    },
    a2bd: function(t, e, i) {
        i.r(e);
        var o = i("6dd5"), n = i.n(o);
        for (var s in o) [ "default" ].indexOf(s) < 0 && function(t) {
            i.d(e, t, function() {
                return o[t];
            });
        }(s);
        e.default = n.a;
    },
    a3c6: function(t, e, i) {
        i.r(e);
        var o = i("f06c"), n = i("a2bd");
        for (var s in n) [ "default" ].indexOf(s) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(s);
        i("f0c8");
        var r = i("f0c5"), a = Object(r.a)(n.default, o.b, o.c, !1, null, "1449df79", null, !1, o.a, void 0);
        e.default = a.exports;
    },
    bc80: function(t, e, i) {
        (function(t, e) {
            var o = i("4ea4");
            i("8740"), o(i("66fd"));
            var n = o(i("a3c6"));
            t.__webpack_require_UNI_MP_PLUGIN__ = i, e(n.default);
        }).call(this, i("bc2e").default, i("543d").createPage);
    },
    f06c: function(t, e, i) {
        i.d(e, "b", function() {
            return n;
        }), i.d(e, "c", function() {
            return s;
        }), i.d(e, "a", function() {
            return o;
        });
        var o = {
            uniPopup: function() {
                return Promise.all([ i.e("common/vendor"), i.e("components/uni-popup/uni-popup") ]).then(i.bind(null, "ce14"));
            }
        }, n = function() {
            var t = this, e = (t.$createElement, t._self._c, t.huisodata.prize_list.length), i = t.chushoudata.prize_list.length;
            t._isMounted || (t.e0 = function(e) {
                return t.$u.throttle(t.enterbaoxian, 1e3);
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e,
                    g1: i
                }
            });
        }, s = [];
    },
    f0c8: function(t, e, i) {
        var o = i("05d8");
        i.n(o).a;
    }
}, [ [ "bc80", "common/runtime", "common/vendor" ] ] ]);