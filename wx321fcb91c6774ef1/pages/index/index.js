(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/index" ], {
    1282: function(t, e, n) {
        (function(t, e) {
            var i = n("4ea4");
            n("8740"), i(n("66fd"));
            var o = i(n("144b"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(o.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "144b": function(t, e, n) {
        n.r(e);
        var i = n("a456"), o = n("a219");
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        n("eed6");
        var s = n("f0c5"), u = Object(s.a)(o.default, i.b, i.c, !1, null, "b64eba26", null, !1, i.a, void 0);
        e.default = u.exports;
    },
    "365a": function(t, e, n) {
        (function(t, i) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = {
                components: {
                    noticePop: function() {
                        n.e("pages/index/noticepop").then(function() {
                            return resolve(n("ed6e"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    viprulePop: function() {
                        n.e("components/index/vipRulepop").then(function() {
                            return resolve(n("db64"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        home: 0,
                        noticelist: [],
                        share: !1,
                        urls: this.$configs.urls,
                        goodstList: [],
                        defaultList: {
                            banner: [],
                            hotBoxList: [],
                            cat: [],
                            news: {
                                content: ""
                            }
                        },
                        page: 1,
                        mode: 0,
                        reward: "",
                        nums: 0,
                        topHeight: 0,
                        type: 0,
                        keyword: "",
                        cat_id: "",
                        explainData: {}
                    };
                },
                onReady: function() {
                    this.topHeight = t.getMenuButtonBoundingClientRect().top;
                },
                onLoad: function() {
                    i.hideTabBar();
                },
                onShow: function() {
                    this.getlist(), this.getbox();
                },
                onHide: function() {
                    this.page = 1, this.noticelist = [];
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.defaultList = {}, this.goodstList = [], this.defaultList.hotBoxList = [], 
                    this.getlist(), this.getbox(), setTimeout(function() {
                        i.stopPullDownRefresh();
                    }, 1e3);
                },
                onReachBottom: function() {
                    this.page++, this.getbox(2);
                },
                onShareAppMessage: function(t) {
                    return t.from, {
                        title: this.arrList.box.name,
                        path: "/pages/index/index"
                    };
                },
                onShareTimeline: function(t) {
                    return t.from, {
                        title: this.arrList.box.name,
                        path: "/pages/index/index"
                    };
                },
                methods: {
                    gonotice: function() {
                        this.$refs.noticepop.open();
                    },
                    searchipt: function(t) {
                        console.log(t), this.keyword = t.detail.value, this.getbox();
                    },
                    changeClass: function(t, e) {
                        this.page = 1, this.type = t, this.cat_id = e, this.goodstList = [], this.getbox();
                    },
                    bannedck: function(t) {
                        var e = this, n = {
                            id: this.defaultList.banner[t].page_id
                        };
                        this.$Request.get(this.$api.index.wenzhang, n).then(function(t) {
                            e.explainData = t.data, Object.keys(e.explainData.length > 0) && e.$refs.viprulepop.open();
                        });
                    },
                    hideShare: function() {
                        this.$refs.shareSuccess.close();
                    },
                    getbox: function(t) {
                        var e = this, n = {
                            page: this.page,
                            cat_id: this.cat_id,
                            keyword: this.keyword
                        };
                        this.$Request.get(this.$api.index.boxList, n).then(function(n) {
                            e.goodstList = 2 != t ? n.data.list : e.goodstList.concat(n.data.list);
                        });
                    },
                    getlist: function() {
                        var t = this;
                        this.$Request.get(this.$api.index.defaultindex).then(function(e) {
                            t.defaultList = e.data, t.noticelist.push(t.defaultList.news.title);
                        });
                    },
                    toGoods: function(t, e) {
                        1 == t ? i.navigateTo({
                            url: "/indexCont/pages/index/suoshopdetail?box_id=" + e + "&pattern=1"
                        }) : i.navigateTo({
                            url: "/indexCont/pages/index/shopdetail?box_id=" + e + "&pattern=0"
                        });
                    },
                    newfl: function(t, e) {
                        i.navigateTo({
                            url: "/pages/newfl/newfl?statu=" + t + "&cat_id=" + e
                        });
                    }
                }
            };
            e.default = o;
        }).call(this, n("bc2e").default, n("543d").default);
    },
    "76c2": function(t, e, n) {},
    a219: function(t, e, n) {
        n.r(e);
        var i = n("365a"), o = n.n(i);
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        e.default = o.a;
    },
    a456: function(t, e, n) {
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            uSwiper: function() {
                return n.e("uview-ui/components/u-swiper/u-swiper").then(n.bind(null, "9ca8"));
            },
            uNoticeBar: function() {
                return n.e("uview-ui/components/u-notice-bar/u-notice-bar").then(n.bind(null, "f2de"));
            },
            uEmpty: function() {
                return n.e("uview-ui/components/u-empty/u-empty").then(n.bind(null, "636d"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    },
    eed6: function(t, e, n) {
        var i = n("76c2");
        n.n(i).a;
    }
}, [ [ "1282", "common/runtime", "common/vendor" ] ] ]);