(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/market" ], {
    "39de": function(t, n, e) {
        e.r(n);
        var i = e("4aa8"), o = e.n(i);
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(a);
        n.default = o.a;
    },
    "4aa8": function(t, n, e) {
        (function(t, i) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = {
                components: {
                    noticePop: function() {
                        e.e("pages/index/noticepop").then(function() {
                            return resolve(e("ed6e"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        urls: this.$configs.urls,
                        goodstList: [],
                        wxList: [],
                        page: 1,
                        keyword: "",
                        tabindex: 1,
                        content: "",
                        qishu: "",
                        total_consumption: "",
                        receiveData: {},
                        topHeight: 0
                    };
                },
                onLoad: function() {
                    t.hideTabBar();
                },
                onReady: function() {
                    this.topHeight = i.getMenuButtonBoundingClientRect().top;
                },
                onShow: function() {
                    this.getlist();
                },
                onHide: function() {
                    this.page = 1;
                },
                onPullDownRefresh: function() {
                    this.page = 1, 1 == this.tabindex ? (this.goodstList = [], this.getlist()) : (this.wxList = [], 
                    this.getwuxianling()), setTimeout(function() {
                        t.stopPullDownRefresh();
                    }, 1e3);
                },
                onReachBottom: function() {
                    1 == this.tabindex && (this.page++, this.getlist(2));
                },
                methods: {
                    enterguize: function() {
                        var t = this;
                        this.$Request.get(this.$api.index.wenzhang, {
                            id: 8
                        }).then(function(n) {
                            t.content = n.data.content, t.$refs.noticepop.open();
                        });
                    },
                    closeReceive: function() {
                        var t = this;
                        this.$refs.wxreceivestu.close(), setTimeout(function() {
                            t.getwuxianling();
                        }, 800);
                    },
                    entersub: function(t) {
                        var n = this, e = {
                            id: t
                        };
                        this.$Request.post(this.$api.wuxian.lingReceive, e).then(function(t) {
                            n.receiveData = t.data.data, n.$refs.wxreceivestu.open();
                        });
                    },
                    entertab: function(t) {
                        this.tabindex = t, this.page = 1, 1 == this.tabindex ? this.getlist() : this.getwuxianling();
                    },
                    searchipt: function() {
                        this.page = 1, this.goodstList = [], this.getlist();
                    },
                    toGoods: function(n) {
                        t.navigateTo({
                            url: "/indexCont/pages/wuxian/wxdetail?box_id=" + n
                        });
                    },
                    getlist: function(t) {
                        var n = this, e = {
                            page: this.page,
                            keyword: this.keyword
                        };
                        this.$Request.get(this.$api.wuxian.boxList, e).then(function(e) {
                            n.goodstList = 2 != t ? e.data.list : n.goodstList.concat(e.data.list);
                        });
                    },
                    getwuxianling: function(t) {
                        var n = this;
                        this.$Request.get(this.$api.wuxian.wuxianling).then(function(e) {
                            n.qishu = e.data.qishu, n.total_consumption = e.data.total_consumption, n.wxList = 2 != t ? e.data.data : n.wxList.concat(e.data.data);
                        });
                    }
                }
            };
            n.default = o;
        }).call(this, e("543d").default, e("bc2e").default);
    },
    "6ba4": function(t, n, e) {},
    "97ae": function(t, n, e) {
        (function(t, n) {
            var i = e("4ea4");
            e("8740"), i(e("66fd"));
            var o = i(e("abc5"));
            t.__webpack_require_UNI_MP_PLUGIN__ = e, n(o.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    abc5: function(t, n, e) {
        e.r(n);
        var i = e("fdc9"), o = e("39de");
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        e("e9ac");
        var u = e("f0c5"), s = Object(u.a)(o.default, i.b, i.c, !1, null, "39c2743e", null, !1, i.a, void 0);
        n.default = s.exports;
    },
    e9ac: function(t, n, e) {
        var i = e("6ba4");
        e.n(i).a;
    },
    fdc9: function(t, n, e) {
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return a;
        }), e.d(n, "a", function() {
            return i;
        });
        var i = {
            uImage: function() {
                return e.e("uview-ui/components/u-image/u-image").then(e.bind(null, "dfa7"));
            },
            uniPopup: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/uni-popup/uni-popup") ]).then(e.bind(null, "ce14"));
            }
        }, o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
    }
}, [ [ "97ae", "common/runtime", "common/vendor" ] ] ]);