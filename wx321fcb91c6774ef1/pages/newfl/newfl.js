(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/newfl/newfl" ], {
    "236d": function(t, i, n) {},
    "285f": function(t, i, n) {
        var e = n("236d");
        n.n(e).a;
    },
    3401: function(t, i, n) {
        (function(t) {
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var n = {
                data: function() {
                    return {
                        page: 1,
                        statu: "",
                        groupList: [],
                        cat_id: "",
                        type: 0
                    };
                },
                onLoad: function(i) {
                    this.page = 1, this.statu = i.statu, this.cat_id = i.cat_id, t.setNavigationBarTitle({
                        title: this.statu
                    }), "新品上架" == this.statu ? (this.type = 1, this.cat_id = "") : "爆款推荐" == this.statu ? (this.type = 2, 
                    this.cat_id = "") : "超值特价" == this.statu && (this.type = 3, this.cat_id = ""), this.getlist();
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.groupList = [], setTimeout(function() {
                        t.stopPullDownRefresh();
                    }, 500), this.getlist();
                },
                onReachBottom: function() {
                    this.page++, this.getlist(2);
                },
                onHide: function() {
                    this.page = 1;
                },
                methods: {
                    toGoods: function(i) {
                        t.navigateTo({
                            url: "/indexCont/pages/index/home?box_id=" + i
                        });
                    },
                    getlist: function(t) {
                        var i = this, n = {
                            page: this.page,
                            type: this.type,
                            cat_id: this.cat_id
                        };
                        this.$Request.get(this.$api.index.boxList, n).then(function(n) {
                            i.groupList = 2 == t ? i.groupList.concat(n.data.list) : n.data.list;
                        });
                    }
                }
            };
            i.default = n;
        }).call(this, n("543d").default);
    },
    9519: function(t, i, n) {
        (function(t, i) {
            var e = n("4ea4");
            n("8740"), e(n("66fd"));
            var a = e(n("b036"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, i(a.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    b036: function(t, i, n) {
        n.r(i);
        var e = n("bd2e"), a = n("f641");
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(i, t, function() {
                return a[t];
            });
        }(o);
        n("285f");
        var u = n("f0c5"), s = Object(u.a)(a.default, e.b, e.c, !1, null, "6cc16630", null, !1, e.a, void 0);
        i.default = s.exports;
    },
    bd2e: function(t, i, n) {
        n.d(i, "b", function() {
            return a;
        }), n.d(i, "c", function() {
            return o;
        }), n.d(i, "a", function() {
            return e;
        });
        var e = {
            uLazyLoad: function() {
                return n.e("uview-ui/components/u-lazy-load/u-lazy-load").then(n.bind(null, "63c2"));
            }
        }, a = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    f641: function(t, i, n) {
        n.r(i);
        var e = n("3401"), a = n.n(e);
        for (var o in e) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(i, t, function() {
                return e[t];
            });
        }(o);
        i.default = a.a;
    }
}, [ [ "9519", "common/runtime", "common/vendor" ] ] ]);