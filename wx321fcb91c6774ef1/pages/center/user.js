(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/user" ], {
    "0808": function(t, e, n) {
        n.r(e);
        var a = n("c78a"), i = n("284f");
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(o);
        n("2dd8");
        var u = n("f0c5"), s = Object(u.a)(i.default, a.b, a.c, !1, null, "0f03616f", null, !1, a.a, void 0);
        e.default = s.exports;
    },
    "284f": function(t, e, n) {
        n.r(e);
        var a = n("c72d"), i = n.n(a);
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(o);
        e.default = i.a;
    },
    "2dd8": function(t, e, n) {
        var a = n("dc2a");
        n.n(a).a;
    },
    c72d: function(t, e, n) {
        (function(t, a) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = {
                components: {
                    baLance: function() {
                        n.e("components/index/balance").then(function() {
                            return resolve(n("d67f"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        newTipsStu: !1,
                        funs: [ {
                            text: "消费记录",
                            url: "/luckdraw/pages/user/exchangeRecord"
                        }, {
                            text: "退货记录",
                            url: "/luckdraw/pages/user/recoveryRecord"
                        }, {
                            text: "发货记录",
                            url: "/luckdraw/pages/user/fahuo"
                        }, {
                            text: "地址管理",
                            url: "/luckdraw/pages/address"
                        } ],
                        urls: this.$configs.urls,
                        arrList: {
                            userInfo: {
                                header_count: {},
                                is_login: 0,
                                integral: 0,
                                card_count: 0
                            }
                        },
                        statusBarHeight: "",
                        heights: "",
                        topHeight: 0,
                        page: 1,
                        Code: "",
                        likeList: [],
                        loginStatus: !0
                    };
                },
                onLoad: function() {
                    t.hideTabBar();
                },
                onReady: function() {
                    this.topHeight = a.getMenuButtonBoundingClientRect().top;
                },
                onShow: function() {
                    t.getStorageSync("USERINFO") ? this.loginStatus = !1 : this.loginStatus = !0, this.arrData(), 
                    this.getcollection();
                },
                onHide: function() {
                    this.page = 1, this.newTipsStu = !1;
                },
                mounted: function() {
                    var e = this;
                    t.getSystemInfo({
                        success: function(t) {
                            e.statusBarHeight = t.statusBarHeight + "px", e.heights = t.statusBarHeight + 275 + "px";
                        }
                    });
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.likeList = [], this.arrData(), this.getcollection(), setTimeout(function() {
                        t.stopPullDownRefresh();
                    }, 1e3);
                },
                onReachBottom: function() {
                    this.page++, this.getcollection(2);
                },
                methods: {
                    submitbalance: function(t) {
                        var e = this, n = {
                            money: t.money,
                            pay_type: t.pay_type,
                            pay_form: "mini"
                        };
                        this.$Request.post(this.$api.user.submitOrder, n).then(function(n) {
                            console.log(n), e.payPrice(n.data.pay_data, n.data.order_id, t.pay_type);
                        });
                    },
                    payPrice: function(e, n, a) {
                        var i = this, o = "";
                        1 == a ? o = "wxpay" : 2 == a && (o = "alipay"), console.log(o), t.requestPayment({
                            provider: o,
                            orderInfo: e,
                            timeStamp: e.timeStamp,
                            nonceStr: e.nonceStr,
                            package: e.package,
                            signType: e.signType,
                            paySign: e.paySign,
                            success: function(e) {
                                console.log(e), t.showToast({
                                    title: "成功",
                                    icon: "none"
                                }), i.$refs.balance_pop.goback();
                            },
                            fail: function(t) {}
                        });
                    },
                    gobalance: function() {
                        this.$refs.balance_pop.open();
                    },
                    colldelimg: function(e, n, a) {
                        var i = this, o = {
                            box_id: e,
                            suit_id: a
                        };
                        this.$Request.post(this.$api.user.del, o).then(function(e) {
                            t.showToast({
                                title: "删除成功",
                                icon: "none"
                            }), i.likeList.splice(n, 1);
                        });
                    },
                    toGoods: function(e, n, a) {
                        0 == n ? t.navigateTo({
                            url: "/indexCont/pages/index/shopdetail?box_id=" + e + "&suit_id=" + a
                        }) : t.navigateTo({
                            url: "/indexCont/pages/wuxian/wxdetail?box_id=" + e
                        });
                    },
                    getcollection: function(t) {
                        var e = this, n = {
                            page: this.page
                        };
                        this.$Request.get(this.$api.user.getList, n).then(function(n) {
                            e.likeList = 2 == t ? e.likeList.concat(n.data.list) : n.data.list;
                        });
                    },
                    opexchangepop: function() {
                        t.showToast({
                            title: "请点击右上角客服,联系客服进行提现",
                            icon: "none",
                            duration: 2e3
                        });
                    },
                    goFuns: function(e, n) {
                        t.navigateTo({
                            url: n
                        });
                    },
                    getuploadAvatar: function() {
                        t.navigateTo({
                            url: "/luckdraw/pages/user/settingInfo"
                        });
                    },
                    shouquan: function() {
                        var e = this;
                        this.loginStatus = !1, setTimeout(function() {
                            e.loginStatus = !0;
                        }, 5e3);
                        var n = this;
                        t.login({
                            provider: "weixin",
                            scopes: "auth_base",
                            success: function(e) {
                                t.request({
                                    url: n.$configs.APIHOST + "passport/authCode",
                                    method: "POST",
                                    data: {
                                        code: e.code,
                                        invite_code: t.getStorageSync("invite_code")
                                    },
                                    success: function(e) {
                                        0 == e.data.code && (0 == e.data.data.is_mobile ? (t.setStorageSync("USERINFO", e.data.data.access_token), 
                                        n.arrData(), n.newTipsStu = !0) : 1 == e.data.data.is_mobile && (t.setStorageSync("uainfo", e.data.data), 
                                        t.navigateTo({
                                            url: "/pages/center/bingmobile"
                                        })));
                                    }
                                });
                            }
                        });
                    },
                    likedetail: function(e) {
                        t.navigateTo({
                            url: "/indexCont/pages/index/shopdetail?box_id=" + e
                        });
                    },
                    getUserProfile1: function() {
                        var e = this;
                        a.getUserProfile({
                            desc: "用于完善个人资料",
                            success: function(n) {
                                console.log(n);
                                var a = n.userInfo;
                                e.arrList.userInfo.avatar = a.avatarUrl, e.arrList.userInfo.nickname = a.nickName;
                                var i = {
                                    avatar: a.avatarUrl,
                                    nickname: a.nickName,
                                    gender: a.gender
                                };
                                console.log(i), e.$Request.post(e.$api.user.miniLogo, i).then(function(e) {
                                    t.showToast({
                                        title: e.msg,
                                        icon: "none"
                                    });
                                });
                            },
                            fail: function() {}
                        });
                    },
                    arrData: function() {
                        var t = this;
                        this.$Request.get(this.$api.user.index).then(function(e) {
                            406 != e.code && (t.arrList = e.data);
                        });
                    },
                    everyOther: function(e) {
                        4 != e ? t.navigateTo({
                            url: "/pages/center/hegui?type=" + e
                        }) : t.navigateTo({
                            url: "/pages/center/yihuishou"
                        });
                    },
                    dropShipping: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/dropShipping"
                        });
                    },
                    forGoods: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/forGoods"
                        });
                    },
                    myBill: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/myBill"
                        });
                    },
                    giftBag: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/giftBag"
                        });
                    },
                    customerService: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/customerService"
                        });
                    },
                    mytabClick: function(e) {
                        1 == e ? t.navigateTo({
                            url: "/pages/center/tuandui"
                        }) : 2 == e ? t.navigateTo({
                            url: "/luckdraw/pages/address"
                        }) : 3 == e ? t.navigateTo({
                            url: "/indexCont/pages/agreement?type=3"
                        }) : 4 == e && t.navigateTo({
                            url: "/luckdraw/pages/customerService"
                        });
                    },
                    set: function() {
                        t.navigateTo({
                            url: "../../luckdraw/pages/set"
                        });
                    },
                    chouxuan: function() {
                        t.navigateTo({
                            url: "/indexCont/pages/index/Myselection"
                        });
                    },
                    goUrl: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    }
                }
            };
            e.default = i;
        }).call(this, n("543d").default, n("bc2e").default);
    },
    c78a: function(t, e, n) {
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {
            return a;
        });
        var a = {
            uIcon: function() {
                return n.e("uview-ui/components/u-icon/u-icon").then(n.bind(null, "71ff"));
            },
            uPopup: function() {
                return n.e("uview-ui/components/u-popup/u-popup").then(n.bind(null, "6205"));
            }
        }, i = function() {
            var t = this;
            t.$createElement;
            t._self._c, t._isMounted || (t.e0 = function(e) {
                t.newTipsStu = !1;
            });
        }, o = [];
    },
    cb78: function(t, e, n) {
        (function(t, e) {
            var a = n("4ea4");
            n("8740"), a(n("66fd"));
            var i = a(n("0808"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    dc2a: function(t, e, n) {}
}, [ [ "cb78", "common/runtime", "common/vendor" ] ] ]);