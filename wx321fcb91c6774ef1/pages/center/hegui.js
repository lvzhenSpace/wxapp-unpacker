(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/hegui" ], {
    "1da0": function(t, i, e) {},
    "37c7": function(t, i, e) {
        (function(t) {
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var e = {
                data: function() {
                    return {
                        urls: this.$configs.urls,
                        Shouhuoshow: !1,
                        orderlist: [],
                        checkIndex: 0,
                        categoryList: [ {
                            name: "全部订单"
                        }, {
                            name: "待发货"
                        }, {
                            name: "待收货"
                        } ],
                        page: 1,
                        status: 0,
                        order_id: "",
                        jimaiyulan: {},
                        allStatus: !1,
                        total_recovery_price: ""
                    };
                },
                onLoad: function(t) {
                    2 == t.type ? (this.checkIndex = 1, this.status = 1, this.getlist()) : 3 == t.type && (this.checkIndex = 2, 
                    this.status = 2, this.getlist());
                },
                onShow: function() {
                    this.getlist();
                },
                onPullDownRefresh: function() {
                    this.page = 1, this.orderlist = [], this.getlist(), setTimeout(function() {
                        t.stopPullDownRefresh();
                    }, 1e3);
                },
                onReachBottom: function() {
                    this.page++, this.getlist(2);
                },
                onHide: function() {
                    this.allStatus = !1, this.$refs.jimaiStatus.close();
                },
                methods: {
                    quanxuan: function() {
                        this.allStatus = !this.allStatus;
                        for (var t = this.orderlist, i = 0; i < t.length; i++) this.allStatus ? t[i].status = !0 : t[i].status = !1;
                    },
                    bottomClick: function(t) {
                        1 == t ? this.goFahuo() : this.gethuishouyulan();
                    },
                    gethuishouyulan: function() {
                        var i = this, e = this.getCarIds();
                        if (0 != e) {
                            var s = {
                                order_id: e
                            };
                            this.$Request.post(this.$api.order.newExchangePreview, s).then(function(t) {
                                i.$refs.jimaipop.open(), i.total_recovery_price = t.data.total_recovery_price;
                            });
                        } else t.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    jimaiqueding: function() {
                        var i = this, e = {
                            order_id: this.getCarIds()
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.post(this.$api.order.batchExchange, e).then(function(e) {
                            t.hideLoading(), t.showToast({
                                title: e.msg,
                                duration: 1500,
                                icon: "none"
                            }), i.allStatus = !1, i.$refs.jimaipop.close(), i.checkIndex = 4, i.status = 4, 
                            i.page = 1, i.orderlist = [], i.getlist();
                        });
                    },
                    payConfirm: function() {
                        var i = this, e = {
                            order_id: this.order_id
                        };
                        this.$Request.post(this.$api.order.confirm, e).then(function(e) {
                            t.showToast({
                                title: e.msg,
                                duration: 1500,
                                icon: "none"
                            }), i.checkIndex = 3, i.page = 1, i.status = 3, i.orderlist = [], i.getlist();
                        });
                    },
                    querenshouhuo: function(t) {
                        this.order_id = t, this.Shouhuoshow = !0;
                    },
                    getlist: function(i) {
                        var e = this, s = {
                            page: this.page,
                            status: this.status
                        };
                        t.showLoading({
                            title: "加载中..."
                        }), this.$Request.get(this.$api.order.sendList, s).then(function(s) {
                            t.hideLoading(), e.orderlist = 2 == i ? e.orderlist.concat(s.data.list) : s.data.list;
                        });
                    },
                    checkStatus: function(t) {
                        var i = this.orderlist;
                        0 == i[t].status ? i[t].status = !0 : i[t].status = !1;
                        for (var e = 0; e < i.length; e++) {
                            var s = 0;
                            0 == i[e].status ? i[e].status = !1 : (s += 1) > 0 && i[e].length == s && (i[e].status = !0);
                        }
                        this.setAllSel();
                    },
                    setAllSel: function() {
                        for (var t = 0, i = 0; i < this.orderlist.length; i++) 1 == this.orderlist[i].status && (t += 1);
                        t == this.orderlist.length && t > 0 ? this.allStatus = !0 : this.allStatus = !1;
                    },
                    getCarIds: function() {
                        for (var i = this.orderlist, e = [], s = 0; s < i.length; s++) 1 == i[s].status && e.push(i[s].order_id);
                        return 0 == e.length ? (t.showToast({
                            title: "请选择商品",
                            icon: "none"
                        }), !1) : e;
                    },
                    goFahuo: function() {
                        var i = this.getCarIds();
                        0 != i ? t.navigateTo({
                            url: "/luckdraw/pages/shopfahuo?order_id=" + this.order_id + "&order_idarr=" + JSON.stringify(i)
                        }) : t.showToast({
                            title: "请选择商品",
                            icon: "none"
                        });
                    },
                    jimaiquxiao: function() {
                        this.$refs.jimaipop.close();
                    },
                    openjimai: function() {
                        this.$refs.jimaipop.open(), this.$refs.jimaiStatus.close();
                    },
                    classification: function(t) {
                        this.page = 1, this.checkIndex = t, this.status = t, this.orderlist = [], this.allStatus = !1, 
                        this.getlist();
                    },
                    jimaiAndfahuo: function(t) {
                        var i = this;
                        this.order_id = t;
                        var e = {
                            order_id: this.order_id
                        };
                        this.$Request.get(this.$api.order.exchangePreview, e).then(function(t) {
                            i.jimaiyulan = t.data, i.$refs.jimaiStatus.open();
                        });
                    },
                    guanbipop: function() {
                        this.$refs.jimaiStatus.close();
                    }
                }
            };
            i.default = e;
        }).call(this, e("543d").default);
    },
    "5e09": function(t, i, e) {
        e.r(i);
        var s = e("737a"), n = e("8995");
        for (var o in n) [ "default" ].indexOf(o) < 0 && function(t) {
            e.d(i, t, function() {
                return n[t];
            });
        }(o);
        e("fd09");
        var a = e("f0c5"), r = Object(a.a)(n.default, s.b, s.c, !1, null, "2e350277", null, !1, s.a, void 0);
        i.default = r.exports;
    },
    "737a": function(t, i, e) {
        e.d(i, "b", function() {
            return n;
        }), e.d(i, "c", function() {
            return o;
        }), e.d(i, "a", function() {
            return s;
        });
        var s = {
            uniPopup: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/uni-popup/uni-popup") ]).then(e.bind(null, "ce14"));
            },
            uModal: function() {
                return e.e("uview-ui/components/u-modal/u-modal").then(e.bind(null, "d387"));
            }
        }, n = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
    },
    8995: function(t, i, e) {
        e.r(i);
        var s = e("37c7"), n = e.n(s);
        for (var o in s) [ "default" ].indexOf(o) < 0 && function(t) {
            e.d(i, t, function() {
                return s[t];
            });
        }(o);
        i.default = n.a;
    },
    c2e9: function(t, i, e) {
        (function(t, i) {
            var s = e("4ea4");
            e("8740"), s(e("66fd"));
            var n = s(e("5e09"));
            t.__webpack_require_UNI_MP_PLUGIN__ = e, i(n.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    fd09: function(t, i, e) {
        var s = e("1da0");
        e.n(s).a;
    }
}, [ [ "c2e9", "common/runtime", "common/vendor" ] ] ]);