require("../@babel/runtime/helpers/Arrayincludes");

var e = require("../@babel/runtime/helpers/typeof");

(global.webpackJsonp = global.webpackJsonp || []).push([ [ "common/vendor" ], {
    "026d": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var u = {
            isInteger: function(e) {
                return Math.floor(e) === e;
            },
            toInteger: function(e) {
                var l = {
                    times: 1,
                    num: 0
                };
                if (this.isInteger(e)) return l.num = e, l;
                var a = e + "", u = a.indexOf("."), t = a.substr(u + 1).length, n = Math.pow(10, t), r = parseInt(e * n + .5, 10);
                return l.times = n, l.num = r, l;
            },
            operation: function(e, l, a) {
                var u = this.toInteger(e), t = this.toInteger(l), n = u.num, r = t.num, v = u.times, o = t.times, i = v > o ? v : o;
                switch (a) {
                  case "add":
                    return (v === o ? n + r : v > o ? n + r * (v / o) : n * (o / v) + r) / i;

                  case "subtract":
                    return (v === o ? n - r : v > o ? n - r * (v / o) : n * (o / v) - r) / i;

                  case "multiply":
                    return n * r / (v * o);

                  case "divide":
                    return n / r * (o / v);
                }
            },
            add: function(e, l) {
                return this.operation(e, l, "add");
            },
            subtract: function(e, l) {
                return this.operation(e, l, "subtract");
            },
            multiply: function(e, l) {
                return this.operation(e, l, "multiply");
            },
            divide: function(e, l) {
                return this.operation(e, l, "divide");
            }
        };
        l.default = u;
    },
    "02bc": function(e, l, a) {
        (function(e) {
            var u = a("4ea4");
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.default = void 0;
            var t = u(a("7037")), n = u(a("242c"));
            l.default = function() {
                var l = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], u = {
                    type: "navigateTo",
                    url: "",
                    delta: 1,
                    params: {},
                    animationType: "pop-in",
                    animationDuration: 300
                };
                if ("/" != (u = Object.assign(u, l)).url[0] && (u.url = "/" + u.url), Object.keys(u.params).length && "switchTab" != u.type) {
                    var r = "";
                    /.*\/.*\?.*=.*/.test(u.url) ? (r = (0, n.default)(u.params, !1), u.url += "&" + r) : (r = (0, 
                    n.default)(u.params), u.url += r);
                }
                if ("string" == typeof l && "object" == (0, t.default)(a)) {
                    /.*\/.*\?.*=.*/.test(l) ? l += "&" + (0, n.default)(a, !1) : l += (0, n.default)(a);
                }
                return "string" == typeof l ? ("/" != l[0] && (l = "/" + l), e.navigateTo({
                    url: l
                })) : "navigateTo" == u.type || "to" == u.type ? e.navigateTo({
                    url: u.url,
                    animationType: u.animationType,
                    animationDuration: u.animationDuration
                }) : "redirectTo" == u.type || "redirect" == u.type ? e.redirectTo({
                    url: u.url
                }) : "switchTab" == u.type || "tab" == u.type ? e.switchTab({
                    url: u.url
                }) : "reLaunch" == u.type ? e.reLaunch({
                    url: u.url
                }) : "navigateBack" == u.type || "back" == u.type ? e.navigateBack({
                    delta: parseInt(u.delta ? u.delta : this.delta)
                }) : void 0;
            };
        }).call(this, a("543d").default);
    },
    "0676": function(e, l) {
        e.exports = function() {
            throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "0fb5": function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("bc16")), n = {
            top: "top",
            bottom: "bottom",
            center: "center",
            message: "top",
            dialog: "center",
            share: "bottom"
        }, r = {
            data: function() {
                return {
                    config: n
                };
            },
            mixins: [ t.default ]
        };
        l.default = r;
    },
    "11b0": function(e, l) {
        e.exports = function(e) {
            if ("undefined" != typeof Symbol && null != e[Symbol.iterator] || null != e["@@iterator"]) return Array.from(e);
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    1938: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "auto", l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "rpx";
            return e = String(e), t.default.number(e) ? "".concat(e).concat(l) : e;
        };
        var t = u(a("b5d5"));
    },
    2236: function(e, l, a) {
        var u = a("5a43");
        e.exports = function(e) {
            if (Array.isArray(e)) return u(e);
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "242c": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, l = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "brackets", u = l ? "?" : "", t = [];
            -1 == [ "indices", "brackets", "repeat", "comma" ].indexOf(a) && (a = "brackets");
            var n = function(l) {
                var u = e[l];
                if ([ "", void 0, null ].indexOf(u) >= 0) return "continue";
                if (u.constructor === Array) switch (a) {
                  case "indices":
                    for (var n = 0; n < u.length; n++) t.push(l + "[" + n + "]=" + u[n]);
                    break;

                  case "brackets":
                    u.forEach(function(e) {
                        t.push(l + "[]=" + e);
                    });
                    break;

                  case "repeat":
                    u.forEach(function(e) {
                        t.push(l + "=" + e);
                    });
                    break;

                  case "comma":
                    var r = "";
                    u.forEach(function(e) {
                        r += (r ? "," : "") + e;
                    }), t.push(l + "=" + r);
                    break;

                  default:
                    u.forEach(function(e) {
                        t.push(l + "[]=" + e);
                    });
                } else t.push(l + "=" + u);
            };
            for (var r in e) n(r);
            return t.length ? u + t.join("&") : "";
        };
    },
    2678: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
            return e.sort(function() {
                return Math.random() - .5;
            });
        };
    },
    "278c": function(e, l, a) {
        var u = a("c135"), t = a("9b42"), n = a("6613"), r = a("c240");
        e.exports = function(e, l) {
            return u(e) || t(e, l) || n(e, l) || r();
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "29a2": function(e, l, a) {
        (function(l) {
            var a = "https://niushang.nndmysf.com/", u = l.getStorageSync("USERINFO");
            e.exports = {
                APIHOST: a + "api/v1/",
                DOMAIN: a,
                Stoken: u,
                urls: "https://niushang.nndmysf.com/"
            };
        }).call(this, a("543d").default);
    },
    "2bc2": function(e, l, a) {
        function u(e) {
            var l = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], a = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
            if ((e = e.toLowerCase()) && a.test(e)) {
                if (4 === e.length) {
                    for (var u = "#", t = 1; t < 4; t += 1) u += e.slice(t, t + 1).concat(e.slice(t, t + 1));
                    e = u;
                }
                for (var n = [], r = 1; r < 7; r += 2) n.push(parseInt("0x" + e.slice(r, r + 2)));
                return l ? "rgb(".concat(n[0], ",").concat(n[1], ",").concat(n[2], ")") : n;
            }
            if (/^(rgb|RGB)/.test(e)) {
                var v = e.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
                return v.map(function(e) {
                    return Number(e);
                });
            }
            return e;
        }
        function t(e) {
            var l = e;
            if (/^(rgb|RGB)/.test(l)) {
                for (var a = l.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(","), u = "#", t = 0; t < a.length; t++) {
                    var n = Number(a[t]).toString(16);
                    "0" === (n = 1 == String(n).length ? "0" + n : n) && (n += n), u += n;
                }
                return 7 !== u.length && (u = l), u;
            }
            if (!/^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/.test(l)) return l;
            var r = l.replace(/#/, "").split("");
            if (6 === r.length) return l;
            if (3 === r.length) {
                for (var v = "#", o = 0; o < r.length; o += 1) v += r[o] + r[o];
                return v;
            }
        }
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var n = {
            colorGradient: function() {
                for (var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "rgb(0, 0, 0)", l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "rgb(255, 255, 255)", a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 10, n = u(e, !1), r = n[0], v = n[1], o = n[2], i = u(l, !1), b = i[0], c = i[1], s = i[2], f = (b - r) / a, d = (c - v) / a, h = (s - o) / a, p = [], g = 0; g < a; g++) {
                    var y = t("rgb(" + Math.round(f * g + r) + "," + Math.round(d * g + v) + "," + Math.round(h * g + o) + ")");
                    p.push(y);
                }
                return p;
            },
            hexToRgb: u,
            rgbToHex: t
        };
        l.default = n;
    },
    "2e4e": function(e, l, a) {
        function u(e) {
            return "[object Object]" === Object.prototype.toString.call(e);
        }
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.getColumns = function(e) {
            var l = e.value, a = e.list, t = e.mode, n = e.props, r = e.level, v = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 2, o = [], i = [], b = [], c = [], s = null;
            switch (t) {
              case "selector":
                var f = a.findIndex(function(e) {
                    return u(e) ? e[n.value] === l : e === l;
                });
                -1 === f && 1 === v ? s = null : (c = a[f = f > -1 ? f : 0], b = u(c) ? c[n.value] : c, 
                s = {
                    index: o = [ f ],
                    value: b,
                    item: c,
                    columns: i = a
                });
                break;

              case "multiSelector":
                var d = function e() {
                    var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], u = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 0;
                    if (a.length) {
                        var t = l || [];
                        if (u < r) {
                            var s = t[u] || "", f = a.findIndex(function(e) {
                                return e[n.value] === s;
                            });
                            if (-1 === f && 1 === v) return;
                            f = f > -1 ? f : 0, o[u] = f, i[u] = a, a[f] && (b[u] = a[f][n.value], c[u] = a[f], 
                            e(a[f][n.children] || [], u + 1));
                        }
                    }
                };
                d(a), s = b.length || 1 !== v ? {
                    index: o,
                    value: b,
                    item: c,
                    columns: i
                } : null;
                break;

              case "unlinkedSelector":
                a.forEach(function(e, t) {
                    var r = e.findIndex(function(e) {
                        return u(e) ? e[n.value] === l[t] : e === l[t];
                    });
                    if (-1 !== r || 1 !== v) {
                        r = r > -1 ? r : 0;
                        var i = a[t][r], s = u(i) ? i[n.value] : i;
                        o[t] = r, b[t] = s, c[t] = i;
                    }
                }), i = a, s = b.length || 1 !== v ? {
                    index: o,
                    value: b,
                    item: c,
                    columns: i
                } : null;
            }
            return s;
        }, l.isObject = u;
    },
    "37dc": function(e, l, a) {
        (function(e, u) {
            var t = a("4ea4");
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.LOCALE_ZH_HANT = l.LOCALE_ZH_HANS = l.LOCALE_FR = l.LOCALE_ES = l.LOCALE_EN = l.I18n = l.Formatter = void 0, 
            l.compileI18nJsonStr = function(e, l) {
                var a = l.locale, u = l.locales, t = l.delimiters;
                if (!S(e, t)) return e;
                w || (w = new s());
                var n = [];
                Object.keys(u).forEach(function(e) {
                    e !== a && n.push({
                        locale: e,
                        values: u[e]
                    });
                }), n.unshift({
                    locale: a,
                    values: u[a]
                });
                try {
                    return JSON.stringify(function e(l, a, u) {
                        return j(l, function(l, t) {
                            !function(l, a, u, t) {
                                var n = l[a];
                                if (A(n)) {
                                    if (S(n, t) && (l[a] = k(n, u[0].values, t), u.length > 1)) {
                                        var r = l[a + "Locales"] = {};
                                        u.forEach(function(e) {
                                            r[e.locale] = k(n, e.values, t);
                                        });
                                    }
                                } else e(n, u, t);
                            }(l, t, a, u);
                        }), l;
                    }(JSON.parse(e), n, t), null, 2);
                } catch (e) {}
                return e;
            }, l.hasI18nJson = function e(l, a) {
                return w || (w = new s()), j(l, function(l, u) {
                    var t = l[u];
                    return A(t) ? !!S(t, a) || void 0 : e(t, a);
                });
            }, l.initVueI18n = function(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, a = arguments.length > 2 ? arguments[2] : void 0, u = arguments.length > 3 ? arguments[3] : void 0;
                if ("string" != typeof e) {
                    var t = [ l, e ];
                    e = t[0], l = t[1];
                }
                "string" != typeof e && (e = $()), "string" != typeof a && (a = "undefined" != typeof __uniConfig && __uniConfig.fallbackLocale || "en");
                var n = new O({
                    locale: e,
                    fallbackLocale: a,
                    messages: l,
                    watcher: u
                }), r = function(e, l) {
                    if ("function" != typeof getApp) r = function(e, l) {
                        return n.t(e, l);
                    }; else {
                        var a = !1;
                        r = function(e, l) {
                            var u = getApp().$vm;
                            return u && (u.$locale, a || (a = !0, x(u, n))), n.t(e, l);
                        };
                    }
                    return r(e, l);
                };
                return {
                    i18n: n,
                    f: function(e, l, a) {
                        return n.f(e, l, a);
                    },
                    t: function(e, l) {
                        return r(e, l);
                    },
                    add: function(e, l) {
                        var a = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2];
                        return n.add(e, l, a);
                    },
                    watch: function(e) {
                        return n.watchLocale(e);
                    },
                    getLocale: function() {
                        return n.getLocale();
                    },
                    setLocale: function(e) {
                        return n.setLocale(e);
                    }
                };
            }, l.isI18nStr = S, l.isString = void 0, l.normalizeLocale = m, l.parseI18nJson = function e(l, a, u) {
                return w || (w = new s()), j(l, function(l, t) {
                    var n = l[t];
                    A(n) ? S(n, u) && (l[t] = k(n, a, u)) : e(n, a, u);
                }), l;
            }, l.resolveLocale = function(e) {
                return function(l) {
                    return l ? function(e) {
                        for (var l = [], a = e.split("-"); a.length; ) l.push(a.join("-")), a.pop();
                        return l;
                    }(l = m(l) || l).find(function(l) {
                        return e.indexOf(l) > -1;
                    }) : l;
                };
            };
            var n = t(a("278c")), r = t(a("970b")), v = t(a("5bc3")), o = t(a("7037")), i = Array.isArray, b = function(e) {
                return null !== e && "object" === (0, o.default)(e);
            }, c = [ "{", "}" ], s = function() {
                function e() {
                    (0, r.default)(this, e), this._caches = Object.create(null);
                }
                return (0, v.default)(e, [ {
                    key: "interpolate",
                    value: function(e, l) {
                        var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : c;
                        if (!l) return [ e ];
                        var u = this._caches[e];
                        return u || (u = h(e, a), this._caches[e] = u), p(u, l);
                    }
                } ]), e;
            }();
            l.Formatter = s;
            var f = /^(?:\d)+/, d = /^(?:\w)+/;
            function h(e, l) {
                for (var a = (0, n.default)(l, 2), u = a[0], t = a[1], r = [], v = 0, o = ""; v < e.length; ) {
                    var i = e[v++];
                    if (i === u) {
                        o && r.push({
                            type: "text",
                            value: o
                        }), o = "";
                        var b = "";
                        for (i = e[v++]; void 0 !== i && i !== t; ) b += i, i = e[v++];
                        var c = i === t, s = f.test(b) ? "list" : c && d.test(b) ? "named" : "unknown";
                        r.push({
                            value: b,
                            type: s
                        });
                    } else o += i;
                }
                return o && r.push({
                    type: "text",
                    value: o
                }), r;
            }
            function p(e, l) {
                var a = [], u = 0, t = i(l) ? "list" : b(l) ? "named" : "unknown";
                if ("unknown" === t) return a;
                for (;u < e.length; ) {
                    var n = e[u];
                    switch (n.type) {
                      case "text":
                        a.push(n.value);
                        break;

                      case "list":
                        a.push(l[parseInt(n.value, 10)]);
                        break;

                      case "named":
                        "named" === t && a.push(l[n.value]);
                    }
                    u++;
                }
                return a;
            }
            l.LOCALE_ZH_HANS = "zh-Hans", l.LOCALE_ZH_HANT = "zh-Hant", l.LOCALE_EN = "en", 
            l.LOCALE_FR = "fr", l.LOCALE_ES = "es";
            var g = Object.prototype.hasOwnProperty, y = function(e, l) {
                return g.call(e, l);
            }, _ = new s();
            function m(e, l) {
                if (e) return e = e.trim().replace(/_/g, "-"), l && l[e] ? e : 0 === (e = e.toLowerCase()).indexOf("zh") ? e.indexOf("-hans") > -1 ? "zh-Hans" : e.indexOf("-hant") > -1 || function(e, l) {
                    return !![ "-tw", "-hk", "-mo", "-cht" ].find(function(l) {
                        return -1 !== e.indexOf(l);
                    });
                }(e) ? "zh-Hant" : "zh-Hans" : function(e, l) {
                    return [ "en", "fr", "es" ].find(function(l) {
                        return 0 === e.indexOf(l);
                    });
                }(e) || void 0;
            }
            var O = function() {
                function e(l) {
                    var a = l.locale, u = l.fallbackLocale, t = l.messages, n = l.watcher, v = l.formater;
                    (0, r.default)(this, e), this.locale = "en", this.fallbackLocale = "en", this.message = {}, 
                    this.messages = {}, this.watchers = [], u && (this.fallbackLocale = u), this.formater = v || _, 
                    this.messages = t || {}, this.setLocale(a || "en"), n && this.watchLocale(n);
                }
                return (0, v.default)(e, [ {
                    key: "setLocale",
                    value: function(e) {
                        var l = this, a = this.locale;
                        this.locale = m(e, this.messages) || this.fallbackLocale, this.messages[this.locale] || (this.messages[this.locale] = {}), 
                        this.message = this.messages[this.locale], a !== this.locale && this.watchers.forEach(function(e) {
                            e(l.locale, a);
                        });
                    }
                }, {
                    key: "getLocale",
                    value: function() {
                        return this.locale;
                    }
                }, {
                    key: "watchLocale",
                    value: function(e) {
                        var l = this, a = this.watchers.push(e) - 1;
                        return function() {
                            l.watchers.splice(a, 1);
                        };
                    }
                }, {
                    key: "add",
                    value: function(e, l) {
                        var a = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2], u = this.messages[e];
                        u ? a ? Object.assign(u, l) : Object.keys(l).forEach(function(e) {
                            y(u, e) || (u[e] = l[e]);
                        }) : this.messages[e] = l;
                    }
                }, {
                    key: "f",
                    value: function(e, l, a) {
                        return this.formater.interpolate(e, l, a).join("");
                    }
                }, {
                    key: "t",
                    value: function(e, l, a) {
                        var u = this.message;
                        return "string" == typeof l ? (l = m(l, this.messages)) && (u = this.messages[l]) : a = l, 
                        y(u, e) ? this.formater.interpolate(u[e], a).join("") : (console.warn("Cannot translate the value of keypath ".concat(e, ". Use the value of keypath as default.")), 
                        e);
                    }
                } ]), e;
            }();
            function x(e, l) {
                e.$watchLocale ? e.$watchLocale(function(e) {
                    l.setLocale(e);
                }) : e.$watch(function() {
                    return e.$locale;
                }, function(e) {
                    l.setLocale(e);
                });
            }
            function $() {
                return void 0 !== e && e.getLocale ? e.getLocale() : void 0 !== u && u.getLocale ? u.getLocale() : "en";
            }
            l.I18n = O;
            var w, A = function(e) {
                return "string" == typeof e;
            };
            function S(e, l) {
                return e.indexOf(l[0]) > -1;
            }
            function k(e, l, a) {
                return w.interpolate(e, l, a).join("");
            }
            function j(e, l) {
                if (i(e)) {
                    for (var a = 0; a < e.length; a++) if (l(e, a)) return !0;
                } else if (b(e)) for (var u in e) if (l(e, u)) return !0;
                return !1;
            }
            l.isString = A;
        }).call(this, a("543d").default, a("c8ba"));
    },
    "37fd": function(e, l, a) {
        (function(e) {
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.default = void 0;
            l.default = function(l) {
                var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1500;
                e.showToast({
                    title: l,
                    icon: "none",
                    duration: a
                });
            };
        }).call(this, a("543d").default);
    },
    "446a": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.commonMixin = void 0;
        var u = a("2e4e"), t = {
            data: function() {
                return {
                    indicatorStyle: "height: 34px"
                };
            },
            created: function() {
                this.init("init");
            },
            methods: {
                init: function(e) {
                    if (this.list && this.list.length) {
                        var l = (0, u.getColumns)({
                            value: this.value,
                            list: this.list,
                            mode: this.mode,
                            props: this.props,
                            level: this.level
                        }), a = l.columns, t = l.value, n = l.item, r = l.index;
                        this.selectValue = t, this.selectItem = n, this.pickerColumns = a, this.pickerValue = r, 
                        this.$emit("change", {
                            value: this.selectValue,
                            item: this.selectItem,
                            index: this.pickerValue,
                            change: e
                        });
                    }
                }
            },
            watch: {
                value: function() {
                    this.isConfirmChange || this.init("value");
                },
                list: function() {
                    this.init("list");
                }
            }
        };
        l.commonMixin = t;
    },
    "448a": function(e, l, a) {
        var u = a("2236"), t = a("11b0"), n = a("6613"), r = a("0676");
        e.exports = function(e) {
            return u(e) || t(e) || n(e) || r();
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "48aa": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 32, l = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1], a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null, u = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split(""), t = [];
            if (a = a || u.length, e) for (var n = 0; n < e; n++) t[n] = u[0 | Math.random() * a]; else {
                var r;
                t[8] = t[13] = t[18] = t[23] = "-", t[14] = "4";
                for (var v = 0; v < 36; v++) t[v] || (r = 0 | 16 * Math.random(), t[v] = u[19 == v ? 3 & r | 8 : r]);
            }
            return l ? (t.shift(), "u" + t.join("")) : t.join("");
        };
    },
    "4a4b": function(e, l) {
        function a(l, u) {
            return e.exports = a = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function(e, l) {
                return e.__proto__ = l, e;
            }, e.exports.__esModule = !0, e.exports.default = e.exports, a(l, u);
        }
        e.exports = a, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "4ea4": function(e, l) {
        e.exports = function(e) {
            return e && e.__esModule ? e : {
                default: e
            };
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "543d": function(e, l, a) {
        (function(e, u) {
            var t = a("4ea4");
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.createApp = wl, l.createComponent = Il, l.createPage = Ll, l.createPlugin = Tl, 
            l.createSubpackageApp = Ml, l.default = void 0;
            var n, r = t(a("278c")), v = t(a("9523")), o = t(a("b17c")), i = t(a("448a")), b = t(a("7037")), c = a("37dc"), s = t(a("66fd"));
            function f(e, l) {
                var a = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var u = Object.getOwnPropertySymbols(e);
                    l && (u = u.filter(function(l) {
                        return Object.getOwnPropertyDescriptor(e, l).enumerable;
                    })), a.push.apply(a, u);
                }
                return a;
            }
            function d(e) {
                for (var l = 1; l < arguments.length; l++) {
                    var a = null != arguments[l] ? arguments[l] : {};
                    l % 2 ? f(Object(a), !0).forEach(function(l) {
                        (0, v.default)(e, l, a[l]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(a)) : f(Object(a)).forEach(function(l) {
                        Object.defineProperty(e, l, Object.getOwnPropertyDescriptor(a, l));
                    });
                }
                return e;
            }
            var h = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", p = /^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;
            function g() {
                var l, a = e.getStorageSync("uni_id_token") || "", u = a.split(".");
                if (!a || 3 !== u.length) return {
                    uid: null,
                    role: [],
                    permission: [],
                    tokenExpired: 0
                };
                try {
                    l = JSON.parse(function(e) {
                        return decodeURIComponent(n(e).split("").map(function(e) {
                            return "%" + ("00" + e.charCodeAt(0).toString(16)).slice(-2);
                        }).join(""));
                    }(u[1]));
                } catch (e) {
                    throw new Error("获取当前用户信息出错，详细错误信息为：" + e.message);
                }
                return l.tokenExpired = 1e3 * l.exp, delete l.exp, delete l.iat, l;
            }
            n = "function" != typeof atob ? function(e) {
                if (e = String(e).replace(/[\t\n\f\r ]+/g, ""), !p.test(e)) throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");
                var l;
                e += "==".slice(2 - (3 & e.length));
                for (var a, u, t = "", n = 0; n < e.length; ) l = h.indexOf(e.charAt(n++)) << 18 | h.indexOf(e.charAt(n++)) << 12 | (a = h.indexOf(e.charAt(n++))) << 6 | (u = h.indexOf(e.charAt(n++))), 
                t += 64 === a ? String.fromCharCode(l >> 16 & 255) : 64 === u ? String.fromCharCode(l >> 16 & 255, l >> 8 & 255) : String.fromCharCode(l >> 16 & 255, l >> 8 & 255, 255 & l);
                return t;
            } : atob;
            var y = Object.prototype.toString, _ = Object.prototype.hasOwnProperty;
            function m(e) {
                return "function" == typeof e;
            }
            function O(e) {
                return "string" == typeof e;
            }
            function x(e) {
                return "[object Object]" === y.call(e);
            }
            function $(e, l) {
                return _.call(e, l);
            }
            function w() {}
            function A(e) {
                var l = Object.create(null);
                return function(a) {
                    return l[a] || (l[a] = e(a));
                };
            }
            var S = /-(\w)/g, k = A(function(e) {
                return e.replace(S, function(e, l) {
                    return l ? l.toUpperCase() : "";
                });
            });
            function j(e) {
                var l = {};
                return x(e) && Object.keys(e).sort().forEach(function(a) {
                    l[a] = e[a];
                }), Object.keys(l) ? l : e;
            }
            var P = [ "invoke", "success", "fail", "complete", "returnValue" ], E = {}, C = {};
            function L(e, l) {
                Object.keys(l).forEach(function(a) {
                    -1 !== P.indexOf(a) && m(l[a]) && (e[a] = function(e, l) {
                        var a = l ? e ? e.concat(l) : Array.isArray(l) ? l : [ l ] : e;
                        return a ? function(e) {
                            for (var l = [], a = 0; a < e.length; a++) -1 === l.indexOf(e[a]) && l.push(e[a]);
                            return l;
                        }(a) : a;
                    }(e[a], l[a]));
                });
            }
            function I(e, l) {
                e && l && Object.keys(l).forEach(function(a) {
                    -1 !== P.indexOf(a) && m(l[a]) && function(e, l) {
                        var a = e.indexOf(l);
                        -1 !== a && e.splice(a, 1);
                    }(e[a], l[a]);
                });
            }
            function M(e) {
                return function(l) {
                    return e(l) || l;
                };
            }
            function T(e) {
                return !!e && ("object" === (0, b.default)(e) || "function" == typeof e) && "function" == typeof e.then;
            }
            function D(e, l) {
                for (var a = !1, u = 0; u < e.length; u++) {
                    var t = e[u];
                    if (a) a = Promise.resolve(M(t)); else {
                        var n = t(l);
                        if (T(n) && (a = Promise.resolve(n)), !1 === n) return {
                            then: function() {}
                        };
                    }
                }
                return a || {
                    then: function(e) {
                        return e(l);
                    }
                };
            }
            function R(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                return [ "success", "fail", "complete" ].forEach(function(a) {
                    if (Array.isArray(e[a])) {
                        var u = l[a];
                        l[a] = function(l) {
                            D(e[a], l).then(function(e) {
                                return m(u) && u(e) || e;
                            });
                        };
                    }
                }), l;
            }
            function N(e, l) {
                var a = [];
                Array.isArray(E.returnValue) && a.push.apply(a, (0, i.default)(E.returnValue));
                var u = C[e];
                return u && Array.isArray(u.returnValue) && a.push.apply(a, (0, i.default)(u.returnValue)), 
                a.forEach(function(e) {
                    l = e(l) || l;
                }), l;
            }
            function V(e) {
                var l = Object.create(null);
                Object.keys(E).forEach(function(e) {
                    "returnValue" !== e && (l[e] = E[e].slice());
                });
                var a = C[e];
                return a && Object.keys(a).forEach(function(e) {
                    "returnValue" !== e && (l[e] = (l[e] || []).concat(a[e]));
                }), l;
            }
            function U(e, l, a) {
                for (var u = arguments.length, t = new Array(u > 3 ? u - 3 : 0), n = 3; n < u; n++) t[n - 3] = arguments[n];
                var r = V(e);
                if (r && Object.keys(r).length) {
                    if (Array.isArray(r.invoke)) {
                        var v = D(r.invoke, a);
                        return v.then(function(e) {
                            return l.apply(void 0, [ R(r, e) ].concat(t));
                        });
                    }
                    return l.apply(void 0, [ R(r, a) ].concat(t));
                }
                return l.apply(void 0, [ a ].concat(t));
            }
            var F = {
                returnValue: function(e) {
                    return T(e) ? new Promise(function(l, a) {
                        e.then(function(e) {
                            e[0] ? a(e[0]) : l(e[1]);
                        });
                    }) : e;
                }
            }, H = /^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo|getSystemSetting|getAppAuthorizeSetting|initUTS|requireUTS|registerUTS/, B = /^create|Manager$/, z = [ "createBLEConnection" ], Z = [ "createBLEConnection", "createPushMessage" ], q = /^on|^off/;
            function G(e) {
                return B.test(e) && -1 === z.indexOf(e);
            }
            function J(e) {
                return H.test(e) && -1 === Z.indexOf(e);
            }
            function W(e) {
                return e.then(function(e) {
                    return [ null, e ];
                }).catch(function(e) {
                    return [ e ];
                });
            }
            function K(e, l) {
                return function(e) {
                    return !(G(e) || J(e) || function(e) {
                        return q.test(e) && "onPush" !== e;
                    }(e));
                }(e) && m(l) ? function() {
                    for (var a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, u = arguments.length, t = new Array(u > 1 ? u - 1 : 0), n = 1; n < u; n++) t[n - 1] = arguments[n];
                    return m(a.success) || m(a.fail) || m(a.complete) ? N(e, U.apply(void 0, [ e, l, a ].concat(t))) : N(e, W(new Promise(function(u, n) {
                        U.apply(void 0, [ e, l, Object.assign({}, a, {
                            success: u,
                            fail: n
                        }) ].concat(t));
                    })));
                } : l;
            }
            Promise.prototype.finally || (Promise.prototype.finally = function(e) {
                var l = this.constructor;
                return this.then(function(a) {
                    return l.resolve(e()).then(function() {
                        return a;
                    });
                }, function(a) {
                    return l.resolve(e()).then(function() {
                        throw a;
                    });
                });
            });
            var Q, X = !1, Y = 0, ee = 0, le = {};
            Q = te(e.getSystemInfoSync().language) || "en", function() {
                if ("undefined" != typeof __uniConfig && __uniConfig.locales && Object.keys(__uniConfig.locales).length) {
                    var e = Object.keys(__uniConfig.locales);
                    e.length && e.forEach(function(e) {
                        var l = le[e], a = __uniConfig.locales[e];
                        l ? Object.assign(l, a) : le[e] = a;
                    });
                }
            }();
            var ae = (0, c.initVueI18n)(Q, {}), ue = ae.t;
            function te(e, l) {
                if (e) return e = e.trim().replace(/_/g, "-"), l && l[e] ? e : "chinese" === (e = e.toLowerCase()) ? "zh-Hans" : 0 === e.indexOf("zh") ? e.indexOf("-hans") > -1 ? "zh-Hans" : e.indexOf("-hant") > -1 || function(e, l) {
                    return !![ "-tw", "-hk", "-mo", "-cht" ].find(function(l) {
                        return -1 !== e.indexOf(l);
                    });
                }(e) ? "zh-Hant" : "zh-Hans" : function(e, l) {
                    return [ "en", "fr", "es" ].find(function(l) {
                        return 0 === e.indexOf(l);
                    });
                }(e) || void 0;
            }
            function ne() {
                if (m(getApp)) {
                    var l = getApp({
                        allowDefault: !0
                    });
                    if (l && l.$vm) return l.$vm.$locale;
                }
                return te(e.getSystemInfoSync().language) || "en";
            }
            ae.mixin = {
                beforeCreate: function() {
                    var e = this, l = ae.i18n.watchLocale(function() {
                        e.$forceUpdate();
                    });
                    this.$once("hook:beforeDestroy", function() {
                        l();
                    });
                },
                methods: {
                    $$t: function(e, l) {
                        return ue(e, l);
                    }
                }
            }, ae.setLocale, ae.getLocale;
            var re = [];
            void 0 !== u && (u.getLocale = ne);
            var ve, oe = {
                promiseInterceptor: F
            }, ie = Object.freeze({
                __proto__: null,
                upx2px: function(l, a) {
                    if (0 === Y && function() {
                        var l = e.getSystemInfoSync(), a = l.platform, u = l.pixelRatio, t = l.windowWidth;
                        Y = t, ee = u, X = "ios" === a;
                    }(), 0 === (l = Number(l))) return 0;
                    var u = l / 750 * (a || Y);
                    return u < 0 && (u = -u), 0 === (u = Math.floor(u + 1e-4)) && (u = 1 !== ee && X ? .5 : 1), 
                    l < 0 ? -u : u;
                },
                getLocale: ne,
                setLocale: function(e) {
                    var l = !!m(getApp) && getApp();
                    return !!l && (l.$vm.$locale !== e && (l.$vm.$locale = e, re.forEach(function(l) {
                        return l({
                            locale: e
                        });
                    }), !0));
                },
                onLocaleChange: function(e) {
                    -1 === re.indexOf(e) && re.push(e);
                },
                addInterceptor: function(e, l) {
                    "string" == typeof e && x(l) ? L(C[e] || (C[e] = {}), l) : x(e) && L(E, e);
                },
                removeInterceptor: function(e, l) {
                    "string" == typeof e ? x(l) ? I(C[e], l) : delete C[e] : x(e) && I(E, e);
                },
                interceptors: oe
            });
            function be(l) {
                (ve = ve || e.getStorageSync("__DC_STAT_UUID")) || (ve = Date.now() + "" + Math.floor(1e7 * Math.random()), 
                e.setStorage({
                    key: "__DC_STAT_UUID",
                    data: ve
                })), l.deviceId = ve;
            }
            function ce(e) {
                if (e.safeArea) {
                    var l = e.safeArea;
                    e.safeAreaInsets = {
                        top: l.top,
                        left: l.left,
                        right: e.windowWidth - l.right,
                        bottom: e.screenHeight - l.bottom
                    };
                }
            }
            function se(e, l) {
                for (var a = e.deviceType || "phone", u = {
                    ipad: "pad",
                    windows: "pc",
                    mac: "pc"
                }, t = Object.keys(u), n = l.toLocaleLowerCase(), r = 0; r < t.length; r++) {
                    var v = t[r];
                    if (-1 !== n.indexOf(v)) {
                        a = u[v];
                        break;
                    }
                }
                return a;
            }
            function fe(e) {
                var l = e;
                return l && (l = e.toLocaleLowerCase()), l;
            }
            function de(e) {
                return ne ? ne() : e;
            }
            function he(e) {
                var l = e.hostName || "WeChat";
                return e.environment ? l = e.environment : e.host && e.host.env && (l = e.host.env), 
                l;
            }
            var pe = {
                returnValue: function(e) {
                    be(e), ce(e), function(e) {
                        var l, a = e.brand, u = void 0 === a ? "" : a, t = e.model, n = void 0 === t ? "" : t, r = e.system, v = void 0 === r ? "" : r, o = e.language, i = void 0 === o ? "" : o, b = e.theme, c = e.version, s = (e.platform, 
                        e.fontSizeSetting), f = e.SDKVersion, d = e.pixelRatio, h = e.deviceOrientation, p = "";
                        p = v.split(" ")[0] || "", l = v.split(" ")[1] || "";
                        var g = c, y = se(e, n), _ = fe(u), m = he(e), O = h, x = d, $ = f, w = i.replace(/_/g, "-"), A = {
                            appId: "__UNI__2120096",
                            appName: "牛牛一番赏",
                            appVersion: "1.0.3",
                            appVersionCode: "103",
                            appLanguage: de(w),
                            uniCompileVersion: "3.7.9",
                            uniRuntimeVersion: "3.7.9",
                            uniPlatform: "mp-weixin",
                            deviceBrand: _,
                            deviceModel: n,
                            deviceType: y,
                            devicePixelRatio: x,
                            deviceOrientation: O,
                            osName: p.toLocaleLowerCase(),
                            osVersion: l,
                            hostTheme: b,
                            hostVersion: g,
                            hostLanguage: w,
                            hostName: m,
                            hostSDKVersion: $,
                            hostFontSizeSetting: s,
                            windowTop: 0,
                            windowBottom: 0,
                            osLanguage: void 0,
                            osTheme: void 0,
                            ua: void 0,
                            hostPackageName: void 0,
                            browserName: void 0,
                            browserVersion: void 0
                        };
                        Object.assign(e, A, {});
                    }(e);
                }
            }, ge = {
                redirectTo: {
                    name: function(e) {
                        return "back" === e.exists && e.delta ? "navigateBack" : "redirectTo";
                    },
                    args: function(e) {
                        if ("back" === e.exists && e.url) {
                            var l = function(e) {
                                for (var l = getCurrentPages(), a = l.length; a--; ) {
                                    var u = l[a];
                                    if (u.$page && u.$page.fullPath === e) return a;
                                }
                                return -1;
                            }(e.url);
                            if (-1 !== l) {
                                var a = getCurrentPages().length - 1 - l;
                                a > 0 && (e.delta = a);
                            }
                        }
                    }
                },
                previewImage: {
                    args: function(e) {
                        var l = parseInt(e.current);
                        if (!isNaN(l)) {
                            var a = e.urls;
                            if (Array.isArray(a)) {
                                var u = a.length;
                                if (u) return l < 0 ? l = 0 : l >= u && (l = u - 1), l > 0 ? (e.current = a[l], 
                                e.urls = a.filter(function(e, u) {
                                    return !(u < l) || e !== a[l];
                                })) : e.current = a[0], {
                                    indicator: !1,
                                    loop: !1
                                };
                            }
                        }
                    }
                },
                getSystemInfo: pe,
                getSystemInfoSync: pe,
                showActionSheet: {
                    args: function(e) {
                        "object" === (0, b.default)(e) && (e.alertText = e.title);
                    }
                },
                getAppBaseInfo: {
                    returnValue: function(e) {
                        var l = e, a = l.version, u = l.language, t = l.SDKVersion, n = l.theme, r = he(e), v = u.replace("_", "-");
                        e = j(Object.assign(e, {
                            appId: "__UNI__2120096",
                            appName: "牛牛一番赏",
                            appVersion: "1.0.3",
                            appVersionCode: "103",
                            appLanguage: de(v),
                            hostVersion: a,
                            hostLanguage: v,
                            hostName: r,
                            hostSDKVersion: t,
                            hostTheme: n
                        }));
                    }
                },
                getDeviceInfo: {
                    returnValue: function(e) {
                        var l = e, a = l.brand, u = l.model, t = se(e, u), n = fe(a);
                        be(e), e = j(Object.assign(e, {
                            deviceType: t,
                            deviceBrand: n,
                            deviceModel: u
                        }));
                    }
                },
                getWindowInfo: {
                    returnValue: function(e) {
                        ce(e), e = j(Object.assign(e, {
                            windowTop: 0,
                            windowBottom: 0
                        }));
                    }
                },
                getAppAuthorizeSetting: {
                    returnValue: function(e) {
                        var l = e.locationReducedAccuracy;
                        e.locationAccuracy = "unsupported", !0 === l ? e.locationAccuracy = "reduced" : !1 === l && (e.locationAccuracy = "full");
                    }
                },
                compressImage: {
                    args: function(e) {
                        e.compressedHeight && !e.compressHeight && (e.compressHeight = e.compressedHeight), 
                        e.compressedWidth && !e.compressWidth && (e.compressWidth = e.compressedWidth);
                    }
                }
            }, ye = [ "success", "fail", "cancel", "complete" ];
            function _e(e, l, a) {
                return function(u) {
                    return l(Oe(e, u, a));
                };
            }
            function me(e, l) {
                var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {}, u = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : {}, t = arguments.length > 4 && void 0 !== arguments[4] && arguments[4];
                if (x(l)) {
                    var n = !0 === t ? l : {};
                    for (var r in m(a) && (a = a(l, n) || {}), l) if ($(a, r)) {
                        var v = a[r];
                        m(v) && (v = v(l[r], l, n)), v ? O(v) ? n[v] = l[r] : x(v) && (n[v.name ? v.name : r] = v.value) : console.warn("The '".concat(e, "' method of platform '微信小程序' does not support option '").concat(r, "'"));
                    } else -1 !== ye.indexOf(r) ? m(l[r]) && (n[r] = _e(e, l[r], u)) : t || (n[r] = l[r]);
                    return n;
                }
                return m(l) && (l = _e(e, l, u)), l;
            }
            function Oe(e, l, a) {
                var u = arguments.length > 3 && void 0 !== arguments[3] && arguments[3];
                return m(ge.returnValue) && (l = ge.returnValue(e, l)), me(e, l, a, {}, u);
            }
            function xe(l, a) {
                if ($(ge, l)) {
                    var u = ge[l];
                    return u ? function(a, t) {
                        var n = u;
                        m(u) && (n = u(a));
                        var r = [ a = me(l, a, n.args, n.returnValue) ];
                        void 0 !== t && r.push(t), m(n.name) ? l = n.name(a) : O(n.name) && (l = n.name);
                        var v = e[l].apply(e, r);
                        return J(l) ? Oe(l, v, n.returnValue, G(l)) : v;
                    } : function() {
                        console.error("Platform '微信小程序' does not support '".concat(l, "'."));
                    };
                }
                return a;
            }
            var $e = Object.create(null);
            [ "onTabBarMidButtonTap", "subscribePush", "unsubscribePush", "onPush", "offPush", "share" ].forEach(function(e) {
                $e[e] = function(e) {
                    return function(l) {
                        var a = l.fail, u = l.complete, t = {
                            errMsg: "".concat(e, ":fail method '").concat(e, "' not supported")
                        };
                        m(a) && a(t), m(u) && u(t);
                    };
                }(e);
            });
            var we = {
                oauth: [ "weixin" ],
                share: [ "weixin" ],
                payment: [ "wxpay" ],
                push: [ "weixin" ]
            }, Ae = Object.freeze({
                __proto__: null,
                getProvider: function(e) {
                    var l = e.service, a = e.success, u = e.fail, t = e.complete, n = !1;
                    we[l] ? (n = {
                        errMsg: "getProvider:ok",
                        service: l,
                        provider: we[l]
                    }, m(a) && a(n)) : (n = {
                        errMsg: "getProvider:fail service not found"
                    }, m(u) && u(n)), m(t) && t(n);
                }
            }), Se = function() {
                var e;
                return function() {
                    return e || (e = new s.default()), e;
                };
            }();
            function ke(e, l, a) {
                return e[l].apply(e, a);
            }
            var je, Pe, Ee, Ce = Object.freeze({
                __proto__: null,
                $on: function() {
                    return ke(Se(), "$on", Array.prototype.slice.call(arguments));
                },
                $off: function() {
                    return ke(Se(), "$off", Array.prototype.slice.call(arguments));
                },
                $once: function() {
                    return ke(Se(), "$once", Array.prototype.slice.call(arguments));
                },
                $emit: function() {
                    return ke(Se(), "$emit", Array.prototype.slice.call(arguments));
                }
            });
            function Le(e) {
                return function() {
                    try {
                        return e.apply(e, arguments);
                    } catch (e) {
                        console.error(e);
                    }
                };
            }
            function Ie(e) {
                try {
                    return JSON.parse(e);
                } catch (e) {}
                return e;
            }
            var Me = [];
            function Te(e, l) {
                Me.forEach(function(a) {
                    a(e, l);
                }), Me.length = 0;
            }
            var De = [], Re = e.getAppBaseInfo && e.getAppBaseInfo();
            Re || (Re = e.getSystemInfoSync());
            var Ne = Re ? Re.host : null, Ve = Ne && "SAAASDK" === Ne.env ? e.miniapp.shareVideoMessage : e.shareVideoMessage, Ue = Object.freeze({
                __proto__: null,
                shareVideoMessage: Ve,
                getPushClientId: function(e) {
                    x(e) || (e = {});
                    var l = function(e) {
                        var l = {};
                        for (var a in e) {
                            var u = e[a];
                            m(u) && (l[a] = Le(u), delete e[a]);
                        }
                        return l;
                    }(e), a = l.success, u = l.fail, t = l.complete, n = m(a), r = m(u), v = m(t);
                    Promise.resolve().then(function() {
                        void 0 === Ee && (Ee = !1, je = "", Pe = "uniPush is not enabled"), Me.push(function(e, l) {
                            var o;
                            e ? (o = {
                                errMsg: "getPushClientId:ok",
                                cid: e
                            }, n && a(o)) : (o = {
                                errMsg: "getPushClientId:fail" + (l ? " " + l : "")
                            }, r && u(o)), v && t(o);
                        }), void 0 !== je && Te(je, Pe);
                    });
                },
                onPushMessage: function(e) {
                    -1 === De.indexOf(e) && De.push(e);
                },
                offPushMessage: function(e) {
                    if (e) {
                        var l = De.indexOf(e);
                        l > -1 && De.splice(l, 1);
                    } else De.length = 0;
                },
                invokePushCallback: function(e) {
                    if ("enabled" === e.type) Ee = !0; else if ("clientId" === e.type) je = e.cid, Pe = e.errMsg, 
                    Te(je, e.errMsg); else if ("pushMsg" === e.type) for (var l = {
                        type: "receive",
                        data: Ie(e.message)
                    }, a = 0; a < De.length; a++) {
                        if ((0, De[a])(l), l.stopped) break;
                    } else "click" === e.type && De.forEach(function(l) {
                        l({
                            type: "click",
                            data: Ie(e.message)
                        });
                    });
                }
            }), Fe = [ "__route__", "__wxExparserNodeId__", "__wxWebviewId__" ];
            function He(e) {
                return Behavior(e);
            }
            function Be() {
                return !!this.route;
            }
            function ze(e) {
                this.triggerEvent("__l", e);
            }
            function Ze(e) {
                var l = e.$scope, a = {};
                Object.defineProperty(e, "$refs", {
                    get: function() {
                        var e = {};
                        return function e(l, a, u) {
                            (l.selectAllComponents(a) || []).forEach(function(l) {
                                var t = l.dataset.ref;
                                u[t] = l.$vm || Je(l), "scoped" === l.dataset.vueGeneric && l.selectAllComponents(".scoped-ref").forEach(function(l) {
                                    e(l, a, u);
                                });
                            });
                        }(l, ".vue-ref", e), (l.selectAllComponents(".vue-ref-in-for") || []).forEach(function(l) {
                            var a = l.dataset.ref;
                            e[a] || (e[a] = []), e[a].push(l.$vm || Je(l));
                        }), function(e, l) {
                            var a = (0, o.default)(Set, (0, i.default)(Object.keys(e)));
                            return Object.keys(l).forEach(function(u) {
                                var t = e[u], n = l[u];
                                Array.isArray(t) && Array.isArray(n) && t.length === n.length && n.every(function(e) {
                                    return t.includes(e);
                                }) || (e[u] = n, a.delete(u));
                            }), a.forEach(function(l) {
                                delete e[l];
                            }), e;
                        }(a, e);
                    }
                });
            }
            function qe(e) {
                var l, a = e.detail || e.value, u = a.vuePid, t = a.vueOptions;
                u && (l = function e(l, a) {
                    for (var u, t = l.$children, n = t.length - 1; n >= 0; n--) {
                        var r = t[n];
                        if (r.$scope._$vueId === a) return r;
                    }
                    for (var v = t.length - 1; v >= 0; v--) if (u = e(t[v], a)) return u;
                }(this.$vm, u)), l || (l = this.$vm), t.parent = l;
            }
            function Ge(e) {
                return Object.defineProperty(e, "__v_isMPComponent", {
                    configurable: !0,
                    enumerable: !1,
                    value: !0
                }), e;
            }
            function Je(e) {
                return function(e) {
                    return null !== e && "object" === (0, b.default)(e);
                }(e) && Object.isExtensible(e) && Object.defineProperty(e, "__ob__", {
                    configurable: !0,
                    enumerable: !1,
                    value: (0, v.default)({}, "__v_skip", !0)
                }), e;
            }
            var We = /_(.*)_worklet_factory_/, Ke = Page, Qe = Component, Xe = /:/g, Ye = A(function(e) {
                return k(e.replace(Xe, "-"));
            });
            function el(e) {
                var l = e.triggerEvent, a = function(e) {
                    for (var a = arguments.length, u = new Array(a > 1 ? a - 1 : 0), t = 1; t < a; t++) u[t - 1] = arguments[t];
                    if (this.$vm || this.dataset && this.dataset.comType) e = Ye(e); else {
                        var n = Ye(e);
                        n !== e && l.apply(this, [ n ].concat(u));
                    }
                    return l.apply(this, [ e ].concat(u));
                };
                try {
                    e.triggerEvent = a;
                } catch (l) {
                    e._triggerEvent = a;
                }
            }
            function ll(e, l, a) {
                var u = l[e];
                l[e] = function() {
                    if (Ge(this), el(this), u) {
                        for (var e = arguments.length, l = new Array(e), a = 0; a < e; a++) l[a] = arguments[a];
                        return u.apply(this, l);
                    }
                };
            }
            function al(e, l, a) {
                l.forEach(function(l) {
                    (function e(l, a) {
                        if (!a) return !0;
                        if (s.default.options && Array.isArray(s.default.options[l])) return !0;
                        if (m(a = a.default || a)) return !!m(a.extendOptions[l]) || !!(a.super && a.super.options && Array.isArray(a.super.options[l]));
                        if (m(a[l]) || Array.isArray(a[l])) return !0;
                        var u = a.mixins;
                        return Array.isArray(u) ? !!u.find(function(a) {
                            return e(l, a);
                        }) : void 0;
                    })(l, a) && (e[l] = function(e) {
                        return this.$vm && this.$vm.__call_hook(l, e);
                    });
                });
            }
            function ul(e, l) {
                var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : [];
                tl(l).forEach(function(l) {
                    return nl(e, l, a);
                });
            }
            function tl(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : [];
                return e && Object.keys(e).forEach(function(a) {
                    0 === a.indexOf("on") && m(e[a]) && l.push(a);
                }), l;
            }
            function nl(e, l, a) {
                -1 !== a.indexOf(l) || $(e, l) || (e[l] = function(e) {
                    return this.$vm && this.$vm.__call_hook(l, e);
                });
            }
            function rl(e, l) {
                var a;
                return [ a = m(l = l.default || l) ? l : e.extend(l), l = a.options ];
            }
            function vl(e, l) {
                if (Array.isArray(l) && l.length) {
                    var a = Object.create(null);
                    l.forEach(function(e) {
                        a[e] = !0;
                    }), e.$scopedSlots = e.$slots = a;
                }
            }
            function ol(e, l) {
                var a = (e = (e || "").split(",")).length;
                1 === a ? l._$vueId = e[0] : 2 === a && (l._$vueId = e[0], l._$vuePid = e[1]);
            }
            function il(e, l) {
                var a = e.data || {}, u = e.methods || {};
                if ("function" == typeof a) try {
                    a = a.call(l);
                } catch (e) {
                    Object({
                        VUE_APP_DARK_MODE: "false",
                        VUE_APP_NAME: "牛牛一番赏",
                        VUE_APP_PLATFORM: "mp-weixin",
                        NODE_ENV: "production",
                        BASE_URL: "/"
                    }).VUE_APP_DEBUG && console.warn("根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。", a);
                } else try {
                    a = JSON.parse(JSON.stringify(a));
                } catch (e) {}
                return x(a) || (a = {}), Object.keys(u).forEach(function(e) {
                    -1 !== l.__lifecycle_hooks__.indexOf(e) || $(a, e) || (a[e] = u[e]);
                }), a;
            }
            Ke.__$wrappered || (Ke.__$wrappered = !0, Page = function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return ll("onLoad", e), Ke(e);
            }, Page.after = Ke.after, Component = function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return ll("created", e), Qe(e);
            });
            var bl = [ String, Number, Boolean, Object, Array, null ];
            function cl(e) {
                return function(l, a) {
                    this.$vm && (this.$vm[e] = l);
                };
            }
            function sl(e, l) {
                var a = e.behaviors, u = e.extends, t = e.mixins, n = e.props;
                n || (e.props = n = []);
                var r = [];
                return Array.isArray(a) && a.forEach(function(e) {
                    r.push(e.replace("uni://", "wx".concat("://"))), "uni://form-field" === e && (Array.isArray(n) ? (n.push("name"), 
                    n.push("value")) : (n.name = {
                        type: String,
                        default: ""
                    }, n.value = {
                        type: [ String, Number, Boolean, Array, Object, Date ],
                        default: ""
                    }));
                }), x(u) && u.props && r.push(l({
                    properties: dl(u.props, !0)
                })), Array.isArray(t) && t.forEach(function(e) {
                    x(e) && e.props && r.push(l({
                        properties: dl(e.props, !0)
                    }));
                }), r;
            }
            function fl(e, l, a, u) {
                return Array.isArray(l) && 1 === l.length ? l[0] : l;
            }
            function dl(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], a = arguments.length > 3 ? arguments[3] : void 0, u = {};
                return l || (u.vueId = {
                    type: String,
                    value: ""
                }, a.virtualHost && (u.virtualHostStyle = {
                    type: null,
                    value: ""
                }, u.virtualHostClass = {
                    type: null,
                    value: ""
                }), u.scopedSlotsCompiler = {
                    type: String,
                    value: ""
                }, u.vueSlots = {
                    type: null,
                    value: [],
                    observer: function(e, l) {
                        var a = Object.create(null);
                        e.forEach(function(e) {
                            a[e] = !0;
                        }), this.setData({
                            $slots: a
                        });
                    }
                }), Array.isArray(e) ? e.forEach(function(e) {
                    u[e] = {
                        type: null,
                        observer: cl(e)
                    };
                }) : x(e) && Object.keys(e).forEach(function(l) {
                    var a = e[l];
                    if (x(a)) {
                        var t = a.default;
                        m(t) && (t = t()), a.type = fl(0, a.type), u[l] = {
                            type: -1 !== bl.indexOf(a.type) ? a.type : null,
                            value: t,
                            observer: cl(l)
                        };
                    } else {
                        var n = fl(0, a);
                        u[l] = {
                            type: -1 !== bl.indexOf(n) ? n : null,
                            observer: cl(l)
                        };
                    }
                }), u;
            }
            function hl(e, l, a, u) {
                var t = {};
                return Array.isArray(l) && l.length && l.forEach(function(l, n) {
                    "string" == typeof l ? l ? "$event" === l ? t["$" + n] = a : "arguments" === l ? t["$" + n] = a.detail && a.detail.__args__ || u : 0 === l.indexOf("$event.") ? t["$" + n] = e.__get_value(l.replace("$event.", ""), a) : t["$" + n] = e.__get_value(l) : t["$" + n] = e : t["$" + n] = function(e, l) {
                        var a = e;
                        return l.forEach(function(l) {
                            var u = l[0], t = l[2];
                            if (u || void 0 !== t) {
                                var n, r = l[1], v = l[3];
                                Number.isInteger(u) ? n = u : u ? "string" == typeof u && u && (n = 0 === u.indexOf("#s#") ? u.substr(3) : e.__get_value(u, a)) : n = a, 
                                Number.isInteger(n) ? a = t : r ? Array.isArray(n) ? a = n.find(function(l) {
                                    return e.__get_value(r, l) === t;
                                }) : x(n) ? a = Object.keys(n).find(function(l) {
                                    return e.__get_value(r, n[l]) === t;
                                }) : console.error("v-for 暂不支持循环数据：", n) : a = n[t], v && (a = e.__get_value(v, a));
                            }
                        }), a;
                    }(e, l);
                }), t;
            }
            function pl(e) {
                for (var l = {}, a = 1; a < e.length; a++) {
                    var u = e[a];
                    l[u[0]] = u[1];
                }
                return l;
            }
            function gl(e, l) {
                var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : [], u = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : [], t = arguments.length > 4 ? arguments[4] : void 0, n = arguments.length > 5 ? arguments[5] : void 0, r = !1, v = x(l.detail) && l.detail.__args__ || [ l.detail ];
                if (t && (r = l.currentTarget && l.currentTarget.dataset && "wx" === l.currentTarget.dataset.comType, 
                !a.length)) return r ? [ l ] : v;
                var o = hl(e, u, l, v), i = [];
                return a.forEach(function(e) {
                    "$event" === e ? "__set_model" !== n || t ? t && !r ? i.push(v[0]) : i.push(l) : i.push(l.target.value) : Array.isArray(e) && "o" === e[0] ? i.push(pl(e)) : "string" == typeof e && $(o, e) ? i.push(o[e]) : i.push(e);
                }), i;
            }
            function yl(e) {
                var l = this, a = ((e = function(e) {
                    try {
                        e.mp = JSON.parse(JSON.stringify(e));
                    } catch (e) {}
                    return e.stopPropagation = w, e.preventDefault = w, e.target = e.target || {}, $(e, "detail") || (e.detail = {}), 
                    $(e, "markerId") && (e.detail = "object" === (0, b.default)(e.detail) ? e.detail : {}, 
                    e.detail.markerId = e.markerId), x(e.detail) && (e.target = Object.assign({}, e.target, e.detail)), 
                    e;
                }(e)).currentTarget || e.target).dataset;
                if (!a) return console.warn("事件信息不存在");
                var u = a.eventOpts || a["event-opts"];
                if (!u) return console.warn("事件信息不存在");
                var t = e.type, n = [];
                return u.forEach(function(a) {
                    var u = a[0], r = a[1], v = "^" === u.charAt(0), o = "~" === (u = v ? u.slice(1) : u).charAt(0);
                    u = o ? u.slice(1) : u, r && function(e, l) {
                        return e === l || "regionchange" === l && ("begin" === e || "end" === e);
                    }(t, u) && r.forEach(function(a) {
                        var u = a[0];
                        if (u) {
                            var t = l.$vm;
                            if (t.$options.generic && (t = function(e) {
                                for (var l = e.$parent; l && l.$parent && (l.$options.generic || l.$parent.$options.generic || l.$scope._$vuePid); ) l = l.$parent;
                                return l && l.$parent;
                            }(t) || t), "$emit" === u) return void t.$emit.apply(t, gl(l.$vm, e, a[1], a[2], v, u));
                            var r = t[u];
                            if (!m(r)) {
                                var i = "page" === l.$vm.mpType ? "Page" : "Component", b = l.route || l.is;
                                throw new Error("".concat(i, ' "').concat(b, '" does not have a method "').concat(u, '"'));
                            }
                            if (o) {
                                if (r.once) return;
                                r.once = !0;
                            }
                            var c = gl(l.$vm, e, a[1], a[2], v, u);
                            c = Array.isArray(c) ? c : [], /=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(r.toString()) && (c = c.concat([ , , , , , , , , , , e ])), 
                            n.push(r.apply(t, c));
                        }
                    });
                }), "input" === t && 1 === n.length && void 0 !== n[0] ? n[0] : void 0;
            }
            var _l = {}, ml = [], Ol = [ "onShow", "onHide", "onError", "onPageNotFound", "onThemeChange", "onUnhandledRejection" ];
            function xl(l, a) {
                var u = a.mocks, t = a.initRefs;
                (function() {
                    s.default.prototype.getOpenerEventChannel = function() {
                        return this.$scope.getOpenerEventChannel();
                    };
                    var e = s.default.prototype.__call_hook;
                    s.default.prototype.__call_hook = function(l, a) {
                        return "onLoad" === l && a && a.__id__ && (this.__eventChannel__ = function(e) {
                            if (e) {
                                var l = _l[e];
                                return delete _l[e], l;
                            }
                            return ml.shift();
                        }(a.__id__), delete a.__id__), e.call(this, l, a);
                    };
                })(), function() {
                    var e = {}, l = {};
                    s.default.prototype.$hasScopedSlotsParams = function(a) {
                        var u = e[a];
                        return u || (l[a] = this, this.$on("hook:destroyed", function() {
                            delete l[a];
                        })), u;
                    }, s.default.prototype.$getScopedSlotsParams = function(a, u, t) {
                        var n = e[a];
                        if (n) {
                            var r = n[u] || {};
                            return t ? r[t] : r;
                        }
                        l[a] = this, this.$on("hook:destroyed", function() {
                            delete l[a];
                        });
                    }, s.default.prototype.$setScopedSlotsParams = function(a, u) {
                        var t = this.$options.propsData.vueId;
                        if (t) {
                            var n = t.split(",")[0];
                            (e[n] = e[n] || {})[a] = u, l[n] && l[n].$forceUpdate();
                        }
                    }, s.default.mixin({
                        destroyed: function() {
                            var a = this.$options.propsData, u = a && a.vueId;
                            u && (delete e[u], delete l[u]);
                        }
                    });
                }(), l.$options.store && (s.default.prototype.$store = l.$options.store), function(e) {
                    e.prototype.uniIDHasRole = function(e) {
                        return g().role.indexOf(e) > -1;
                    }, e.prototype.uniIDHasPermission = function(e) {
                        var l = g().permission;
                        return this.uniIDHasRole("admin") || l.indexOf(e) > -1;
                    }, e.prototype.uniIDTokenValid = function() {
                        return g().tokenExpired > Date.now();
                    };
                }(s.default), s.default.prototype.mpHost = "mp-weixin", s.default.mixin({
                    beforeCreate: function() {
                        if (this.$options.mpType) {
                            if (this.mpType = this.$options.mpType, this.$mp = (0, v.default)({
                                data: {}
                            }, this.mpType, this.$options.mpInstance), this.$scope = this.$options.mpInstance, 
                            delete this.$options.mpType, delete this.$options.mpInstance, "page" === this.mpType && "function" == typeof getApp) {
                                var e = getApp();
                                e.$vm && e.$vm.$i18n && (this._i18n = e.$vm.$i18n);
                            }
                            "app" !== this.mpType && (t(this), function(e, l) {
                                var a = e.$mp[e.mpType];
                                l.forEach(function(l) {
                                    $(a, l) && (e[l] = a[l]);
                                });
                            }(this, u));
                        }
                    }
                });
                var n = {
                    onLaunch: function(a) {
                        this.$vm || (e.canIUse && !e.canIUse("nextTick") && console.error("当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上"), 
                        this.$vm = l, this.$vm.$mp = {
                            app: this
                        }, this.$vm.$scope = this, this.$vm.globalData = this.globalData, this.$vm._isMounted = !0, 
                        this.$vm.__call_hook("mounted", a), this.$vm.__call_hook("onLaunch", a));
                    }
                };
                n.globalData = l.$options.globalData || {};
                var r = l.$options.methods;
                return r && Object.keys(r).forEach(function(e) {
                    n[e] = r[e];
                }), function(e, l, a) {
                    var u = e.observable({
                        locale: a || ae.getLocale()
                    }), t = [];
                    l.$watchLocale = function(e) {
                        t.push(e);
                    }, Object.defineProperty(l, "$locale", {
                        get: function() {
                            return u.locale;
                        },
                        set: function(e) {
                            u.locale = e, t.forEach(function(l) {
                                return l(e);
                            });
                        }
                    });
                }(s.default, l, te(e.getSystemInfoSync().language) || "en"), al(n, Ol), ul(n, l.$options), 
                n;
            }
            function $l(e) {
                return xl(e, {
                    mocks: Fe,
                    initRefs: Ze
                });
            }
            function wl(e) {
                return App($l(e)), e;
            }
            var Al = /[!'()*]/g, Sl = function(e) {
                return "%" + e.charCodeAt(0).toString(16);
            }, kl = /%2C/g, jl = function(e) {
                return encodeURIComponent(e).replace(Al, Sl).replace(kl, ",");
            };
            function Pl(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : jl, a = e ? Object.keys(e).map(function(a) {
                    var u = e[a];
                    if (void 0 === u) return "";
                    if (null === u) return l(a);
                    if (Array.isArray(u)) {
                        var t = [];
                        return u.forEach(function(e) {
                            void 0 !== e && (null === e ? t.push(l(a)) : t.push(l(a) + "=" + l(e)));
                        }), t.join("&");
                    }
                    return l(a) + "=" + l(u);
                }).filter(function(e) {
                    return e.length > 0;
                }).join("&") : null;
                return a ? "?".concat(a) : "";
            }
            function El(e, l) {
                return function(e) {
                    var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, a = l.isPage, u = l.initRelation, t = arguments.length > 2 ? arguments[2] : void 0, n = rl(s.default, e), v = (0, 
                    r.default)(n, 2), o = v[0], i = v[1], b = d({
                        multipleSlots: !0,
                        addGlobalClass: !0
                    }, i.options || {});
                    i["mp-weixin"] && i["mp-weixin"].options && Object.assign(b, i["mp-weixin"].options);
                    var c = {
                        options: b,
                        data: il(i, s.default.prototype),
                        behaviors: sl(i, He),
                        properties: dl(i.props, !1, i.__file, b),
                        lifetimes: {
                            attached: function() {
                                var e = this.properties, l = {
                                    mpType: a.call(this) ? "page" : "component",
                                    mpInstance: this,
                                    propsData: e
                                };
                                ol(e.vueId, this), u.call(this, {
                                    vuePid: this._$vuePid,
                                    vueOptions: l
                                }), this.$vm = new o(l), vl(this.$vm, e.vueSlots), this.$vm.$mount();
                            },
                            ready: function() {
                                this.$vm && (this.$vm._isMounted = !0, this.$vm.__call_hook("mounted"), this.$vm.__call_hook("onReady"));
                            },
                            detached: function() {
                                this.$vm && this.$vm.$destroy();
                            }
                        },
                        pageLifetimes: {
                            show: function(e) {
                                this.$vm && this.$vm.__call_hook("onPageShow", e);
                            },
                            hide: function() {
                                this.$vm && this.$vm.__call_hook("onPageHide");
                            },
                            resize: function(e) {
                                this.$vm && this.$vm.__call_hook("onPageResize", e);
                            }
                        },
                        methods: {
                            __l: qe,
                            __e: yl
                        }
                    };
                    return i.externalClasses && (c.externalClasses = i.externalClasses), Array.isArray(i.wxsCallMethods) && i.wxsCallMethods.forEach(function(e) {
                        c.methods[e] = function(l) {
                            return this.$vm[e](l);
                        };
                    }), t ? [ c, i, o ] : a ? c : [ c, o ];
                }(e, {
                    isPage: Be,
                    initRelation: ze
                }, l);
            }
            var Cl = [ "onShow", "onHide", "onUnload" ];
            function Ll(e) {
                return Component(function(e) {
                    return function(e) {
                        var l = El(e, !0), a = (0, r.default)(l, 2), u = a[0], t = a[1];
                        return al(u.methods, Cl, t), u.methods.onLoad = function(e) {
                            this.options = e;
                            var l = Object.assign({}, e);
                            delete l.__id__, this.$page = {
                                fullPath: "/" + (this.route || this.is) + Pl(l)
                            }, this.$vm.$mp.query = e, this.$vm.__call_hook("onLoad", e);
                        }, ul(u.methods, e, [ "onReady" ]), function(e, l) {
                            l && Object.keys(l).forEach(function(a) {
                                var u = a.match(We);
                                if (u) {
                                    var t = u[1];
                                    e[a] = l[a], e[t] = l[t];
                                }
                            });
                        }(u.methods, t.methods), u;
                    }(e);
                }(e));
            }
            function Il(e) {
                return Component(El(e));
            }
            function Ml(l) {
                var a = $l(l), u = getApp({
                    allowDefault: !0
                });
                l.$scope = u;
                var t = u.globalData;
                if (t && Object.keys(a.globalData).forEach(function(e) {
                    $(t, e) || (t[e] = a.globalData[e]);
                }), Object.keys(a).forEach(function(e) {
                    $(u, e) || (u[e] = a[e]);
                }), m(a.onShow) && e.onAppShow && e.onAppShow(function() {
                    for (var e = arguments.length, a = new Array(e), u = 0; u < e; u++) a[u] = arguments[u];
                    l.__call_hook("onShow", a);
                }), m(a.onHide) && e.onAppHide && e.onAppHide(function() {
                    for (var e = arguments.length, a = new Array(e), u = 0; u < e; u++) a[u] = arguments[u];
                    l.__call_hook("onHide", a);
                }), m(a.onLaunch)) {
                    var n = e.getLaunchOptionsSync && e.getLaunchOptionsSync();
                    l.__call_hook("onLaunch", n);
                }
                return l;
            }
            function Tl(l) {
                var a = $l(l);
                if (m(a.onShow) && e.onAppShow && e.onAppShow(function() {
                    for (var e = arguments.length, a = new Array(e), u = 0; u < e; u++) a[u] = arguments[u];
                    l.__call_hook("onShow", a);
                }), m(a.onHide) && e.onAppHide && e.onAppHide(function() {
                    for (var e = arguments.length, a = new Array(e), u = 0; u < e; u++) a[u] = arguments[u];
                    l.__call_hook("onHide", a);
                }), m(a.onLaunch)) {
                    var u = e.getLaunchOptionsSync && e.getLaunchOptionsSync();
                    l.__call_hook("onLaunch", u);
                }
                return l;
            }
            Cl.push.apply(Cl, [ "onPullDownRefresh", "onReachBottom", "onAddToFavorites", "onShareTimeline", "onShareAppMessage", "onPageScroll", "onResize", "onTabItemTap" ]), 
            [ "vibrate", "preloadPage", "unPreloadPage", "loadSubPackage" ].forEach(function(e) {
                ge[e] = !1;
            }), [].forEach(function(l) {
                var a = ge[l] && ge[l].name ? ge[l].name : l;
                e.canIUse(a) || (ge[l] = !1);
            });
            var Dl = {};
            "undefined" != typeof Proxy ? Dl = new Proxy({}, {
                get: function(l, a) {
                    return $(l, a) ? l[a] : ie[a] ? ie[a] : Ue[a] ? K(a, Ue[a]) : Ae[a] ? K(a, Ae[a]) : $e[a] ? K(a, $e[a]) : Ce[a] ? Ce[a] : K(a, xe(a, e[a]));
                },
                set: function(e, l, a) {
                    return e[l] = a, !0;
                }
            }) : (Object.keys(ie).forEach(function(e) {
                Dl[e] = ie[e];
            }), Object.keys($e).forEach(function(e) {
                Dl[e] = K(e, $e[e]);
            }), Object.keys(Ae).forEach(function(e) {
                Dl[e] = K(e, $e[e]);
            }), Object.keys(Ce).forEach(function(e) {
                Dl[e] = Ce[e];
            }), Object.keys(Ue).forEach(function(e) {
                Dl[e] = K(e, Ue[e]);
            }), Object.keys(e).forEach(function(l) {
                ($(e, l) || $(ge, l)) && (Dl[l] = K(l, xe(l, e[l])));
            })), e.createApp = wl, e.createPage = Ll, e.createComponent = Il, e.createSubpackageApp = Ml, 
            e.createPlugin = Tl;
            var Rl = Dl;
            l.default = Rl;
        }).call(this, a("bc2e").default, a("c8ba"));
    },
    "54cf": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var u = null;
        l.default = function(e) {
            var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500, a = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
            if (null !== u && clearTimeout(u), a) {
                var t = !u;
                u = setTimeout(function() {
                    u = null;
                }, l), t && "function" == typeof e && e();
            } else u = setTimeout(function() {
                "function" == typeof e && e();
            }, l);
        };
    },
    "5a43": function(e, l) {
        e.exports = function(e, l) {
            (null == l || l > e.length) && (l = e.length);
            for (var a = 0, u = new Array(l); a < l; a++) u[a] = e[a];
            return u;
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "5bc3": function(e, l, a) {
        var u = a("a395");
        function t(e, l) {
            for (var a = 0; a < l.length; a++) {
                var t = l[a];
                t.enumerable = t.enumerable || !1, t.configurable = !0, "value" in t && (t.writable = !0), 
                Object.defineProperty(e, u(t.key), t);
            }
        }
        e.exports = function(e, l, a) {
            return l && t(e.prototype, l), a && t(e, a), Object.defineProperty(e, "prototype", {
                writable: !1
            }), e;
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "635e": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "success", l = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
            -1 == [ "primary", "info", "error", "warning", "success" ].indexOf(e) && (e = "success");
            var a = "";
            switch (e) {
              case "primary":
              case "info":
                a = "info-circle";
                break;

              case "error":
                a = "close-circle";
                break;

              case "warning":
                a = "error-circle";
                break;

              case "success":
                a = "checkmark-circle";
                break;

              default:
                a = "checkmark-circle";
            }
            return l && (a += "-fill"), a;
        };
    },
    6613: function(e, l, a) {
        var u = a("5a43");
        e.exports = function(e, l) {
            if (e) {
                if ("string" == typeof e) return u(e, l);
                var a = Object.prototype.toString.call(e).slice(8, -1);
                return "Object" === a && e.constructor && (a = e.constructor.name), "Map" === a || "Set" === a ? Array.from(e) : "Arguments" === a || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a) ? u(e, l) : void 0;
            }
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "66fd": function(l, a, u) {
        u.r(a), function(l) {
            var u = Object.freeze({});
            function t(e) {
                return null == e;
            }
            function n(e) {
                return null != e;
            }
            function r(e) {
                return !0 === e;
            }
            function v(l) {
                return "string" == typeof l || "number" == typeof l || "symbol" === e(l) || "boolean" == typeof l;
            }
            function o(l) {
                return null !== l && "object" === e(l);
            }
            var i = Object.prototype.toString;
            function b(e) {
                return "[object Object]" === i.call(e);
            }
            function c(e) {
                var l = parseFloat(String(e));
                return l >= 0 && Math.floor(l) === l && isFinite(e);
            }
            function s(e) {
                return n(e) && "function" == typeof e.then && "function" == typeof e.catch;
            }
            function f(e) {
                return null == e ? "" : Array.isArray(e) || b(e) && e.toString === i ? JSON.stringify(e, null, 2) : String(e);
            }
            function d(e) {
                var l = parseFloat(e);
                return isNaN(l) ? e : l;
            }
            function h(e, l) {
                for (var a = Object.create(null), u = e.split(","), t = 0; t < u.length; t++) a[u[t]] = !0;
                return l ? function(e) {
                    return a[e.toLowerCase()];
                } : function(e) {
                    return a[e];
                };
            }
            h("slot,component", !0);
            var p = h("key,ref,slot,slot-scope,is");
            function g(e, l) {
                if (e.length) {
                    var a = e.indexOf(l);
                    if (a > -1) return e.splice(a, 1);
                }
            }
            var y = Object.prototype.hasOwnProperty;
            function _(e, l) {
                return y.call(e, l);
            }
            function m(e) {
                var l = Object.create(null);
                return function(a) {
                    return l[a] || (l[a] = e(a));
                };
            }
            var O = /-(\w)/g, x = m(function(e) {
                return e.replace(O, function(e, l) {
                    return l ? l.toUpperCase() : "";
                });
            }), $ = m(function(e) {
                return e.charAt(0).toUpperCase() + e.slice(1);
            }), w = /\B([A-Z])/g, A = m(function(e) {
                return e.replace(w, "-$1").toLowerCase();
            }), S = Function.prototype.bind ? function(e, l) {
                return e.bind(l);
            } : function(e, l) {
                function a(a) {
                    var u = arguments.length;
                    return u ? u > 1 ? e.apply(l, arguments) : e.call(l, a) : e.call(l);
                }
                return a._length = e.length, a;
            };
            function k(e, l) {
                l = l || 0;
                for (var a = e.length - l, u = new Array(a); a--; ) u[a] = e[a + l];
                return u;
            }
            function j(e, l) {
                for (var a in l) e[a] = l[a];
                return e;
            }
            function P(e) {
                for (var l = {}, a = 0; a < e.length; a++) e[a] && j(l, e[a]);
                return l;
            }
            function E(e, l, a) {}
            var C = function(e, l, a) {
                return !1;
            }, L = function(e) {
                return e;
            };
            function I(e, l) {
                if (e === l) return !0;
                var a = o(e), u = o(l);
                if (!a || !u) return !a && !u && String(e) === String(l);
                try {
                    var t = Array.isArray(e), n = Array.isArray(l);
                    if (t && n) return e.length === l.length && e.every(function(e, a) {
                        return I(e, l[a]);
                    });
                    if (e instanceof Date && l instanceof Date) return e.getTime() === l.getTime();
                    if (t || n) return !1;
                    var r = Object.keys(e), v = Object.keys(l);
                    return r.length === v.length && r.every(function(a) {
                        return I(e[a], l[a]);
                    });
                } catch (e) {
                    return !1;
                }
            }
            function M(e, l) {
                for (var a = 0; a < e.length; a++) if (I(e[a], l)) return a;
                return -1;
            }
            function T(e) {
                var l = !1;
                return function() {
                    l || (l = !0, e.apply(this, arguments));
                };
            }
            var D = [ "component", "directive", "filter" ], R = [ "beforeCreate", "created", "beforeMount", "mounted", "beforeUpdate", "updated", "beforeDestroy", "destroyed", "activated", "deactivated", "errorCaptured", "serverPrefetch" ], N = {
                optionMergeStrategies: Object.create(null),
                silent: !1,
                productionTip: !1,
                devtools: !1,
                performance: !1,
                errorHandler: null,
                warnHandler: null,
                ignoredElements: [],
                keyCodes: Object.create(null),
                isReservedTag: C,
                isReservedAttr: C,
                isUnknownElement: C,
                getTagNamespace: E,
                parsePlatformTagName: L,
                mustUseProp: C,
                async: !0,
                _lifecycleHooks: R
            };
            function V(e) {
                var l = (e + "").charCodeAt(0);
                return 36 === l || 95 === l;
            }
            function U(e, l, a, u) {
                Object.defineProperty(e, l, {
                    value: a,
                    enumerable: !!u,
                    writable: !0,
                    configurable: !0
                });
            }
            var F, H = new RegExp("[^" + /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/.source + ".$_\\d]"), B = "__proto__" in {}, z = "undefined" != typeof window, Z = "undefined" != typeof WXEnvironment && !!WXEnvironment.platform, q = Z && WXEnvironment.platform.toLowerCase(), G = z && window.navigator.userAgent.toLowerCase(), J = G && /msie|trident/.test(G), W = (G && G.indexOf("msie 9.0"), 
            G && G.indexOf("edge/"), G && G.indexOf("android"), G && /iphone|ipad|ipod|ios/.test(G) || "ios" === q), K = (G && /chrome\/\d+/.test(G), 
            G && /phantomjs/.test(G), G && G.match(/firefox\/(\d+)/), {}.watch);
            if (z) try {
                var Q = {};
                Object.defineProperty(Q, "passive", {
                    get: function() {}
                }), window.addEventListener("test-passive", null, Q);
            } catch (e) {}
            var X = function() {
                return void 0 === F && (F = !z && !Z && void 0 !== l && l.process && "server" === l.process.env.VUE_ENV), 
                F;
            }, Y = z && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;
            function ee(e) {
                return "function" == typeof e && /native code/.test(e.toString());
            }
            var le, ae = "undefined" != typeof Symbol && ee(Symbol) && "undefined" != typeof Reflect && ee(Reflect.ownKeys);
            le = "undefined" != typeof Set && ee(Set) ? Set : function() {
                function e() {
                    this.set = Object.create(null);
                }
                return e.prototype.has = function(e) {
                    return !0 === this.set[e];
                }, e.prototype.add = function(e) {
                    this.set[e] = !0;
                }, e.prototype.clear = function() {
                    this.set = Object.create(null);
                }, e;
            }();
            var ue = E, te = 0, ne = function() {
                this.id = te++, this.subs = [];
            };
            function re(e) {
                ne.SharedObject.targetStack.push(e), ne.SharedObject.target = e, ne.target = e;
            }
            function ve() {
                ne.SharedObject.targetStack.pop(), ne.SharedObject.target = ne.SharedObject.targetStack[ne.SharedObject.targetStack.length - 1], 
                ne.target = ne.SharedObject.target;
            }
            ne.prototype.addSub = function(e) {
                this.subs.push(e);
            }, ne.prototype.removeSub = function(e) {
                g(this.subs, e);
            }, ne.prototype.depend = function() {
                ne.SharedObject.target && ne.SharedObject.target.addDep(this);
            }, ne.prototype.notify = function() {
                for (var e = this.subs.slice(), l = 0, a = e.length; l < a; l++) e[l].update();
            }, (ne.SharedObject = {}).target = null, ne.SharedObject.targetStack = [];
            var oe = function(e, l, a, u, t, n, r, v) {
                this.tag = e, this.data = l, this.children = a, this.text = u, this.elm = t, this.ns = void 0, 
                this.context = n, this.fnContext = void 0, this.fnOptions = void 0, this.fnScopeId = void 0, 
                this.key = l && l.key, this.componentOptions = r, this.componentInstance = void 0, 
                this.parent = void 0, this.raw = !1, this.isStatic = !1, this.isRootInsert = !0, 
                this.isComment = !1, this.isCloned = !1, this.isOnce = !1, this.asyncFactory = v, 
                this.asyncMeta = void 0, this.isAsyncPlaceholder = !1;
            }, ie = {
                child: {
                    configurable: !0
                }
            };
            ie.child.get = function() {
                return this.componentInstance;
            }, Object.defineProperties(oe.prototype, ie);
            var be = function(e) {
                void 0 === e && (e = "");
                var l = new oe();
                return l.text = e, l.isComment = !0, l;
            };
            function ce(e) {
                return new oe(void 0, void 0, void 0, String(e));
            }
            var se = Array.prototype, fe = Object.create(se);
            [ "push", "pop", "shift", "unshift", "splice", "sort", "reverse" ].forEach(function(e) {
                var l = se[e];
                U(fe, e, function() {
                    for (var a = [], u = arguments.length; u--; ) a[u] = arguments[u];
                    var t, n = l.apply(this, a), r = this.__ob__;
                    switch (e) {
                      case "push":
                      case "unshift":
                        t = a;
                        break;

                      case "splice":
                        t = a.slice(2);
                    }
                    return t && r.observeArray(t), r.dep.notify(), n;
                });
            });
            var de = Object.getOwnPropertyNames(fe), he = !0;
            function pe(e) {
                he = e;
            }
            var ge = function(e) {
                this.value = e, this.dep = new ne(), this.vmCount = 0, U(e, "__ob__", this), Array.isArray(e) ? (B ? e.push !== e.__proto__.push ? ye(e, fe, de) : function(e, l) {
                    e.__proto__ = l;
                }(e, fe) : ye(e, fe, de), this.observeArray(e)) : this.walk(e);
            };
            function ye(e, l, a) {
                for (var u = 0, t = a.length; u < t; u++) {
                    var n = a[u];
                    U(e, n, l[n]);
                }
            }
            function _e(e, l) {
                var a;
                if (o(e) && !(e instanceof oe)) return _(e, "__ob__") && e.__ob__ instanceof ge ? a = e.__ob__ : !he || X() || !Array.isArray(e) && !b(e) || !Object.isExtensible(e) || e._isVue || e.__v_isMPComponent || (a = new ge(e)), 
                l && a && a.vmCount++, a;
            }
            function me(e, l, a, u, t) {
                var n = new ne(), r = Object.getOwnPropertyDescriptor(e, l);
                if (!r || !1 !== r.configurable) {
                    var v = r && r.get, o = r && r.set;
                    v && !o || 2 !== arguments.length || (a = e[l]);
                    var i = !t && _e(a);
                    Object.defineProperty(e, l, {
                        enumerable: !0,
                        configurable: !0,
                        get: function() {
                            var l = v ? v.call(e) : a;
                            return ne.SharedObject.target && (n.depend(), i && (i.dep.depend(), Array.isArray(l) && $e(l))), 
                            l;
                        },
                        set: function(l) {
                            var u = v ? v.call(e) : a;
                            l === u || l != l && u != u || v && !o || (o ? o.call(e, l) : a = l, i = !t && _e(l), 
                            n.notify());
                        }
                    });
                }
            }
            function Oe(e, l, a) {
                if (Array.isArray(e) && c(l)) return e.length = Math.max(e.length, l), e.splice(l, 1, a), 
                a;
                if (l in e && !(l in Object.prototype)) return e[l] = a, a;
                var u = e.__ob__;
                return e._isVue || u && u.vmCount ? a : u ? (me(u.value, l, a), u.dep.notify(), 
                a) : (e[l] = a, a);
            }
            function xe(e, l) {
                if (Array.isArray(e) && c(l)) e.splice(l, 1); else {
                    var a = e.__ob__;
                    e._isVue || a && a.vmCount || _(e, l) && (delete e[l], a && a.dep.notify());
                }
            }
            function $e(e) {
                for (var l = void 0, a = 0, u = e.length; a < u; a++) (l = e[a]) && l.__ob__ && l.__ob__.dep.depend(), 
                Array.isArray(l) && $e(l);
            }
            ge.prototype.walk = function(e) {
                for (var l = Object.keys(e), a = 0; a < l.length; a++) me(e, l[a]);
            }, ge.prototype.observeArray = function(e) {
                for (var l = 0, a = e.length; l < a; l++) _e(e[l]);
            };
            var we = N.optionMergeStrategies;
            function Ae(e, l) {
                if (!l) return e;
                for (var a, u, t, n = ae ? Reflect.ownKeys(l) : Object.keys(l), r = 0; r < n.length; r++) "__ob__" !== (a = n[r]) && (u = e[a], 
                t = l[a], _(e, a) ? u !== t && b(u) && b(t) && Ae(u, t) : Oe(e, a, t));
                return e;
            }
            function Se(e, l, a) {
                return a ? function() {
                    var u = "function" == typeof l ? l.call(a, a) : l, t = "function" == typeof e ? e.call(a, a) : e;
                    return u ? Ae(u, t) : t;
                } : l ? e ? function() {
                    return Ae("function" == typeof l ? l.call(this, this) : l, "function" == typeof e ? e.call(this, this) : e);
                } : l : e;
            }
            function ke(e, l) {
                var a = l ? e ? e.concat(l) : Array.isArray(l) ? l : [ l ] : e;
                return a ? function(e) {
                    for (var l = [], a = 0; a < e.length; a++) -1 === l.indexOf(e[a]) && l.push(e[a]);
                    return l;
                }(a) : a;
            }
            function je(e, l, a, u) {
                var t = Object.create(e || null);
                return l ? j(t, l) : t;
            }
            we.data = function(e, l, a) {
                return a ? Se(e, l, a) : l && "function" != typeof l ? e : Se(e, l);
            }, R.forEach(function(e) {
                we[e] = ke;
            }), D.forEach(function(e) {
                we[e + "s"] = je;
            }), we.watch = function(e, l, a, u) {
                if (e === K && (e = void 0), l === K && (l = void 0), !l) return Object.create(e || null);
                if (!e) return l;
                var t = {};
                for (var n in j(t, e), l) {
                    var r = t[n], v = l[n];
                    r && !Array.isArray(r) && (r = [ r ]), t[n] = r ? r.concat(v) : Array.isArray(v) ? v : [ v ];
                }
                return t;
            }, we.props = we.methods = we.inject = we.computed = function(e, l, a, u) {
                if (!e) return l;
                var t = Object.create(null);
                return j(t, e), l && j(t, l), t;
            }, we.provide = Se;
            var Pe = function(e, l) {
                return void 0 === l ? e : l;
            };
            function Ee(e, l, a) {
                if ("function" == typeof l && (l = l.options), function(e, l) {
                    var a = e.props;
                    if (a) {
                        var u, t, n = {};
                        if (Array.isArray(a)) for (u = a.length; u--; ) "string" == typeof (t = a[u]) && (n[x(t)] = {
                            type: null
                        }); else if (b(a)) for (var r in a) t = a[r], n[x(r)] = b(t) ? t : {
                            type: t
                        };
                        e.props = n;
                    }
                }(l), function(e, l) {
                    var a = e.inject;
                    if (a) {
                        var u = e.inject = {};
                        if (Array.isArray(a)) for (var t = 0; t < a.length; t++) u[a[t]] = {
                            from: a[t]
                        }; else if (b(a)) for (var n in a) {
                            var r = a[n];
                            u[n] = b(r) ? j({
                                from: n
                            }, r) : {
                                from: r
                            };
                        }
                    }
                }(l), function(e) {
                    var l = e.directives;
                    if (l) for (var a in l) {
                        var u = l[a];
                        "function" == typeof u && (l[a] = {
                            bind: u,
                            update: u
                        });
                    }
                }(l), !l._base && (l.extends && (e = Ee(e, l.extends, a)), l.mixins)) for (var u = 0, t = l.mixins.length; u < t; u++) e = Ee(e, l.mixins[u], a);
                var n, r = {};
                for (n in e) v(n);
                for (n in l) _(e, n) || v(n);
                function v(u) {
                    var t = we[u] || Pe;
                    r[u] = t(e[u], l[u], a, u);
                }
                return r;
            }
            function Ce(e, l, a, u) {
                if ("string" == typeof a) {
                    var t = e[l];
                    if (_(t, a)) return t[a];
                    var n = x(a);
                    if (_(t, n)) return t[n];
                    var r = $(n);
                    return _(t, r) ? t[r] : t[a] || t[n] || t[r];
                }
            }
            function Le(e, l, a, u) {
                var t = l[e], n = !_(a, e), r = a[e], v = Te(Boolean, t.type);
                if (v > -1) if (n && !_(t, "default")) r = !1; else if ("" === r || r === A(e)) {
                    var o = Te(String, t.type);
                    (o < 0 || v < o) && (r = !0);
                }
                if (void 0 === r) {
                    r = function(e, l, a) {
                        if (_(l, "default")) {
                            var u = l.default;
                            return e && e.$options.propsData && void 0 === e.$options.propsData[a] && void 0 !== e._props[a] ? e._props[a] : "function" == typeof u && "Function" !== Ie(l.type) ? u.call(e) : u;
                        }
                    }(u, t, e);
                    var i = he;
                    pe(!0), _e(r), pe(i);
                }
                return r;
            }
            function Ie(e) {
                var l = e && e.toString().match(/^\s*function (\w+)/);
                return l ? l[1] : "";
            }
            function Me(e, l) {
                return Ie(e) === Ie(l);
            }
            function Te(e, l) {
                if (!Array.isArray(l)) return Me(l, e) ? 0 : -1;
                for (var a = 0, u = l.length; a < u; a++) if (Me(l[a], e)) return a;
                return -1;
            }
            function De(e, l, a) {
                re();
                try {
                    if (l) for (var u = l; u = u.$parent; ) {
                        var t = u.$options.errorCaptured;
                        if (t) for (var n = 0; n < t.length; n++) try {
                            if (!1 === t[n].call(u, e, l, a)) return;
                        } catch (e) {
                            Ne(e, u, "errorCaptured hook");
                        }
                    }
                    Ne(e, l, a);
                } finally {
                    ve();
                }
            }
            function Re(e, l, a, u, t) {
                var n;
                try {
                    (n = a ? e.apply(l, a) : e.call(l)) && !n._isVue && s(n) && !n._handled && (n.catch(function(e) {
                        return De(e, u, t + " (Promise/async)");
                    }), n._handled = !0);
                } catch (e) {
                    De(e, u, t);
                }
                return n;
            }
            function Ne(e, l, a) {
                if (N.errorHandler) try {
                    return N.errorHandler.call(null, e, l, a);
                } catch (l) {
                    l !== e && Ve(l, null, "config.errorHandler");
                }
                Ve(e, l, a);
            }
            function Ve(e, l, a) {
                if (!z && !Z || "undefined" == typeof console) throw e;
                console.error(e);
            }
            var Ue, Fe = [], He = !1;
            function Be() {
                He = !1;
                var e = Fe.slice(0);
                Fe.length = 0;
                for (var l = 0; l < e.length; l++) e[l]();
            }
            if ("undefined" != typeof Promise && ee(Promise)) {
                var ze = Promise.resolve();
                Ue = function() {
                    ze.then(Be), W && setTimeout(E);
                };
            } else if (J || "undefined" == typeof MutationObserver || !ee(MutationObserver) && "[object MutationObserverConstructor]" !== MutationObserver.toString()) Ue = "undefined" != typeof setImmediate && ee(setImmediate) ? function() {
                setImmediate(Be);
            } : function() {
                setTimeout(Be, 0);
            }; else {
                var Ze = 1, qe = new MutationObserver(Be), Ge = document.createTextNode(String(Ze));
                qe.observe(Ge, {
                    characterData: !0
                }), Ue = function() {
                    Ze = (Ze + 1) % 2, Ge.data = String(Ze);
                };
            }
            function Je(e, l) {
                var a;
                if (Fe.push(function() {
                    if (e) try {
                        e.call(l);
                    } catch (e) {
                        De(e, l, "nextTick");
                    } else a && a(l);
                }), He || (He = !0, Ue()), !e && "undefined" != typeof Promise) return new Promise(function(e) {
                    a = e;
                });
            }
            var We = new le();
            function Ke(e) {
                (function e(l, a) {
                    var u, t, n = Array.isArray(l);
                    if (!(!n && !o(l) || Object.isFrozen(l) || l instanceof oe)) {
                        if (l.__ob__) {
                            var r = l.__ob__.dep.id;
                            if (a.has(r)) return;
                            a.add(r);
                        }
                        if (n) for (u = l.length; u--; ) e(l[u], a); else for (u = (t = Object.keys(l)).length; u--; ) e(l[t[u]], a);
                    }
                })(e, We), We.clear();
            }
            var Qe = m(function(e) {
                var l = "&" === e.charAt(0), a = "~" === (e = l ? e.slice(1) : e).charAt(0), u = "!" === (e = a ? e.slice(1) : e).charAt(0);
                return {
                    name: e = u ? e.slice(1) : e,
                    once: a,
                    capture: u,
                    passive: l
                };
            });
            function Xe(e, l) {
                function a() {
                    var e = arguments, u = a.fns;
                    if (!Array.isArray(u)) return Re(u, null, arguments, l, "v-on handler");
                    for (var t = u.slice(), n = 0; n < t.length; n++) Re(t[n], null, e, l, "v-on handler");
                }
                return a.fns = e, a;
            }
            function Ye(e, l, a, u) {
                var r = l.options.mpOptions && l.options.mpOptions.properties;
                if (t(r)) return a;
                var v = l.options.mpOptions.externalClasses || [], o = e.attrs, i = e.props;
                if (n(o) || n(i)) for (var b in r) {
                    var c = A(b);
                    (el(a, i, b, c, !0) || el(a, o, b, c, !1)) && a[b] && -1 !== v.indexOf(c) && u[x(a[b])] && (a[b] = u[x(a[b])]);
                }
                return a;
            }
            function el(e, l, a, u, t) {
                if (n(l)) {
                    if (_(l, a)) return e[a] = l[a], t || delete l[a], !0;
                    if (_(l, u)) return e[a] = l[u], t || delete l[u], !0;
                }
                return !1;
            }
            function ll(e) {
                return v(e) ? [ ce(e) ] : Array.isArray(e) ? function e(l, a) {
                    var u, o, i, b, c = [];
                    for (u = 0; u < l.length; u++) t(o = l[u]) || "boolean" == typeof o || (b = c[i = c.length - 1], 
                    Array.isArray(o) ? o.length > 0 && (al((o = e(o, (a || "") + "_" + u))[0]) && al(b) && (c[i] = ce(b.text + o[0].text), 
                    o.shift()), c.push.apply(c, o)) : v(o) ? al(b) ? c[i] = ce(b.text + o) : "" !== o && c.push(ce(o)) : al(o) && al(b) ? c[i] = ce(b.text + o.text) : (r(l._isVList) && n(o.tag) && t(o.key) && n(a) && (o.key = "__vlist" + a + "_" + u + "__"), 
                    c.push(o)));
                    return c;
                }(e) : void 0;
            }
            function al(e) {
                return n(e) && n(e.text) && function(e) {
                    return !1 === e;
                }(e.isComment);
            }
            function ul(e) {
                var l = e.$options.provide;
                l && (e._provided = "function" == typeof l ? l.call(e) : l);
            }
            function tl(e) {
                var l = nl(e.$options.inject, e);
                l && (pe(!1), Object.keys(l).forEach(function(a) {
                    me(e, a, l[a]);
                }), pe(!0));
            }
            function nl(e, l) {
                if (e) {
                    for (var a = Object.create(null), u = ae ? Reflect.ownKeys(e) : Object.keys(e), t = 0; t < u.length; t++) {
                        var n = u[t];
                        if ("__ob__" !== n) {
                            for (var r = e[n].from, v = l; v; ) {
                                if (v._provided && _(v._provided, r)) {
                                    a[n] = v._provided[r];
                                    break;
                                }
                                v = v.$parent;
                            }
                            if (!v && "default" in e[n]) {
                                var o = e[n].default;
                                a[n] = "function" == typeof o ? o.call(l) : o;
                            }
                        }
                    }
                    return a;
                }
            }
            function rl(e, l) {
                if (!e || !e.length) return {};
                for (var a = {}, u = 0, t = e.length; u < t; u++) {
                    var n = e[u], r = n.data;
                    if (r && r.attrs && r.attrs.slot && delete r.attrs.slot, n.context !== l && n.fnContext !== l || !r || null == r.slot) n.asyncMeta && n.asyncMeta.data && "page" === n.asyncMeta.data.slot ? (a.page || (a.page = [])).push(n) : (a.default || (a.default = [])).push(n); else {
                        var v = r.slot, o = a[v] || (a[v] = []);
                        "template" === n.tag ? o.push.apply(o, n.children || []) : o.push(n);
                    }
                }
                for (var i in a) a[i].every(vl) && delete a[i];
                return a;
            }
            function vl(e) {
                return e.isComment && !e.asyncFactory || " " === e.text;
            }
            function ol(e, l, a) {
                var t, n = Object.keys(l).length > 0, r = e ? !!e.$stable : !n, v = e && e.$key;
                if (e) {
                    if (e._normalized) return e._normalized;
                    if (r && a && a !== u && v === a.$key && !n && !a.$hasNormal) return a;
                    for (var o in t = {}, e) e[o] && "$" !== o[0] && (t[o] = il(l, o, e[o]));
                } else t = {};
                for (var i in l) i in t || (t[i] = bl(l, i));
                return e && Object.isExtensible(e) && (e._normalized = t), U(t, "$stable", r), U(t, "$key", v), 
                U(t, "$hasNormal", n), t;
            }
            function il(l, a, u) {
                var t = function() {
                    var l = arguments.length ? u.apply(null, arguments) : u({});
                    return (l = l && "object" === e(l) && !Array.isArray(l) ? [ l ] : ll(l)) && (0 === l.length || 1 === l.length && l[0].isComment) ? void 0 : l;
                };
                return u.proxy && Object.defineProperty(l, a, {
                    get: t,
                    enumerable: !0,
                    configurable: !0
                }), t;
            }
            function bl(e, l) {
                return function() {
                    return e[l];
                };
            }
            function cl(e, l) {
                var a, u, t, r, v;
                if (Array.isArray(e) || "string" == typeof e) for (a = new Array(e.length), u = 0, 
                t = e.length; u < t; u++) a[u] = l(e[u], u, u, u); else if ("number" == typeof e) for (a = new Array(e), 
                u = 0; u < e; u++) a[u] = l(u + 1, u, u, u); else if (o(e)) if (ae && e[Symbol.iterator]) {
                    a = [];
                    for (var i = e[Symbol.iterator](), b = i.next(); !b.done; ) a.push(l(b.value, a.length, u, u++)), 
                    b = i.next();
                } else for (r = Object.keys(e), a = new Array(r.length), u = 0, t = r.length; u < t; u++) v = r[u], 
                a[u] = l(e[v], v, u, u);
                return n(a) || (a = []), a._isVList = !0, a;
            }
            function sl(e, l, a, u) {
                var t, n = this.$scopedSlots[e];
                n ? (a = a || {}, u && (a = j(j({}, u), a)), t = n(a, this, a._i) || l) : t = this.$slots[e] || l;
                var r = a && a.slot;
                return r ? this.$createElement("template", {
                    slot: r
                }, t) : t;
            }
            function fl(e) {
                return Ce(this.$options, "filters", e) || L;
            }
            function dl(e, l) {
                return Array.isArray(e) ? -1 === e.indexOf(l) : e !== l;
            }
            function hl(e, l, a, u, t) {
                var n = N.keyCodes[l] || a;
                return t && u && !N.keyCodes[l] ? dl(t, u) : n ? dl(n, e) : u ? A(u) !== l : void 0;
            }
            function pl(e, l, a, u, t) {
                if (a && o(a)) {
                    var n;
                    Array.isArray(a) && (a = P(a));
                    var r = function(r) {
                        if ("class" === r || "style" === r || p(r)) n = e; else {
                            var v = e.attrs && e.attrs.type;
                            n = u || N.mustUseProp(l, v, r) ? e.domProps || (e.domProps = {}) : e.attrs || (e.attrs = {});
                        }
                        var o = x(r), i = A(r);
                        o in n || i in n || (n[r] = a[r], !t) || ((e.on || (e.on = {}))["update:" + r] = function(e) {
                            a[r] = e;
                        });
                    };
                    for (var v in a) r(v);
                }
                return e;
            }
            function gl(e, l) {
                var a = this._staticTrees || (this._staticTrees = []), u = a[e];
                return u && !l || _l(u = a[e] = this.$options.staticRenderFns[e].call(this._renderProxy, null, this), "__static__" + e, !1), 
                u;
            }
            function yl(e, l, a) {
                return _l(e, "__once__" + l + (a ? "_" + a : ""), !0), e;
            }
            function _l(e, l, a) {
                if (Array.isArray(e)) for (var u = 0; u < e.length; u++) e[u] && "string" != typeof e[u] && ml(e[u], l + "_" + u, a); else ml(e, l, a);
            }
            function ml(e, l, a) {
                e.isStatic = !0, e.key = l, e.isOnce = a;
            }
            function Ol(e, l) {
                if (l && b(l)) {
                    var a = e.on = e.on ? j({}, e.on) : {};
                    for (var u in l) {
                        var t = a[u], n = l[u];
                        a[u] = t ? [].concat(t, n) : n;
                    }
                }
                return e;
            }
            function xl(e, l, a, u) {
                l = l || {
                    $stable: !a
                };
                for (var t = 0; t < e.length; t++) {
                    var n = e[t];
                    Array.isArray(n) ? xl(n, l, a) : n && (n.proxy && (n.fn.proxy = !0), l[n.key] = n.fn);
                }
                return u && (l.$key = u), l;
            }
            function $l(e, l) {
                for (var a = 0; a < l.length; a += 2) {
                    var u = l[a];
                    "string" == typeof u && u && (e[l[a]] = l[a + 1]);
                }
                return e;
            }
            function wl(e, l) {
                return "string" == typeof e ? l + e : e;
            }
            function Al(e) {
                e._o = yl, e._n = d, e._s = f, e._l = cl, e._t = sl, e._q = I, e._i = M, e._m = gl, 
                e._f = fl, e._k = hl, e._b = pl, e._v = ce, e._e = be, e._u = xl, e._g = Ol, e._d = $l, 
                e._p = wl;
            }
            function Sl(e, l, a, t, n) {
                var v, o = this, i = n.options;
                _(t, "_uid") ? (v = Object.create(t))._original = t : (v = t, t = t._original);
                var b = r(i._compiled), c = !b;
                this.data = e, this.props = l, this.children = a, this.parent = t, this.listeners = e.on || u, 
                this.injections = nl(i.inject, t), this.slots = function() {
                    return o.$slots || ol(e.scopedSlots, o.$slots = rl(a, t)), o.$slots;
                }, Object.defineProperty(this, "scopedSlots", {
                    enumerable: !0,
                    get: function() {
                        return ol(e.scopedSlots, this.slots());
                    }
                }), b && (this.$options = i, this.$slots = this.slots(), this.$scopedSlots = ol(e.scopedSlots, this.$slots)), 
                i._scopeId ? this._c = function(e, l, a, u) {
                    var n = Il(v, e, l, a, u, c);
                    return n && !Array.isArray(n) && (n.fnScopeId = i._scopeId, n.fnContext = t), n;
                } : this._c = function(e, l, a, u) {
                    return Il(v, e, l, a, u, c);
                };
            }
            function kl(e, l, a, u, t) {
                var n = function(e) {
                    var l = new oe(e.tag, e.data, e.children && e.children.slice(), e.text, e.elm, e.context, e.componentOptions, e.asyncFactory);
                    return l.ns = e.ns, l.isStatic = e.isStatic, l.key = e.key, l.isComment = e.isComment, 
                    l.fnContext = e.fnContext, l.fnOptions = e.fnOptions, l.fnScopeId = e.fnScopeId, 
                    l.asyncMeta = e.asyncMeta, l.isCloned = !0, l;
                }(e);
                return n.fnContext = a, n.fnOptions = u, l.slot && ((n.data || (n.data = {})).slot = l.slot), 
                n;
            }
            function jl(e, l) {
                for (var a in l) e[x(a)] = l[a];
            }
            Al(Sl.prototype);
            var Pl = {
                init: function(e, l) {
                    if (e.componentInstance && !e.componentInstance._isDestroyed && e.data.keepAlive) {
                        var a = e;
                        Pl.prepatch(a, a);
                    } else {
                        (e.componentInstance = function(e, l) {
                            var a = {
                                _isComponent: !0,
                                _parentVnode: e,
                                parent: l
                            }, u = e.data.inlineTemplate;
                            return n(u) && (a.render = u.render, a.staticRenderFns = u.staticRenderFns), new e.componentOptions.Ctor(a);
                        }(e, Hl)).$mount(l ? e.elm : void 0, l);
                    }
                },
                prepatch: function(e, l) {
                    var a = l.componentOptions;
                    !function(e, l, a, t, n) {
                        var r = t.data.scopedSlots, v = e.$scopedSlots, o = !!(r && !r.$stable || v !== u && !v.$stable || r && e.$scopedSlots.$key !== r.$key), i = !!(n || e.$options._renderChildren || o);
                        if (e.$options._parentVnode = t, e.$vnode = t, e._vnode && (e._vnode.parent = t), 
                        e.$options._renderChildren = n, e.$attrs = t.data.attrs || u, e.$listeners = a || u, 
                        l && e.$options.props) {
                            pe(!1);
                            for (var b = e._props, c = e.$options._propKeys || [], s = 0; s < c.length; s++) {
                                var f = c[s], d = e.$options.props;
                                b[f] = Le(f, d, l, e);
                            }
                            pe(!0), e.$options.propsData = l;
                        }
                        e._$updateProperties && e._$updateProperties(e), a = a || u;
                        var h = e.$options._parentListeners;
                        e.$options._parentListeners = a, Fl(e, a, h), i && (e.$slots = rl(n, t.context), 
                        e.$forceUpdate());
                    }(l.componentInstance = e.componentInstance, a.propsData, a.listeners, l, a.children);
                },
                insert: function(e) {
                    var l = e.context, a = e.componentInstance;
                    a._isMounted || (Zl(a, "onServiceCreated"), Zl(a, "onServiceAttached"), a._isMounted = !0, 
                    Zl(a, "mounted")), e.data.keepAlive && (l._isMounted ? function(e) {
                        e._inactive = !1, Gl.push(e);
                    }(a) : zl(a, !0));
                },
                destroy: function(e) {
                    var l = e.componentInstance;
                    l._isDestroyed || (e.data.keepAlive ? function e(l, a) {
                        if (!(a && (l._directInactive = !0, Bl(l)) || l._inactive)) {
                            l._inactive = !0;
                            for (var u = 0; u < l.$children.length; u++) e(l.$children[u]);
                            Zl(l, "deactivated");
                        }
                    }(l, !0) : l.$destroy());
                }
            }, El = Object.keys(Pl);
            function Cl(e, l, a, v, i) {
                if (!t(e)) {
                    var b = a.$options._base;
                    if (o(e) && (e = b.extend(e)), "function" == typeof e) {
                        var c;
                        if (t(e.cid) && void 0 === (e = function(e, l) {
                            if (r(e.error) && n(e.errorComp)) return e.errorComp;
                            if (n(e.resolved)) return e.resolved;
                            var a = Tl;
                            if (a && n(e.owners) && -1 === e.owners.indexOf(a) && e.owners.push(a), r(e.loading) && n(e.loadingComp)) return e.loadingComp;
                            if (a && !n(e.owners)) {
                                var u = e.owners = [ a ], v = !0, i = null, b = null;
                                a.$on("hook:destroyed", function() {
                                    return g(u, a);
                                });
                                var c = function(e) {
                                    for (var l = 0, a = u.length; l < a; l++) u[l].$forceUpdate();
                                    e && (u.length = 0, null !== i && (clearTimeout(i), i = null), null !== b && (clearTimeout(b), 
                                    b = null));
                                }, f = T(function(a) {
                                    e.resolved = Dl(a, l), v ? u.length = 0 : c(!0);
                                }), d = T(function(l) {
                                    n(e.errorComp) && (e.error = !0, c(!0));
                                }), h = e(f, d);
                                return o(h) && (s(h) ? t(e.resolved) && h.then(f, d) : s(h.component) && (h.component.then(f, d), 
                                n(h.error) && (e.errorComp = Dl(h.error, l)), n(h.loading) && (e.loadingComp = Dl(h.loading, l), 
                                0 === h.delay ? e.loading = !0 : i = setTimeout(function() {
                                    i = null, t(e.resolved) && t(e.error) && (e.loading = !0, c(!1));
                                }, h.delay || 200)), n(h.timeout) && (b = setTimeout(function() {
                                    b = null, t(e.resolved) && d(null);
                                }, h.timeout)))), v = !1, e.loading ? e.loadingComp : e.resolved;
                            }
                        }(c = e, b))) return function(e, l, a, u, t) {
                            var n = be();
                            return n.asyncFactory = e, n.asyncMeta = {
                                data: l,
                                context: a,
                                children: u,
                                tag: t
                            }, n;
                        }(c, l, a, v, i);
                        l = l || {}, ca(e), n(l.model) && function(e, l) {
                            var a = e.model && e.model.prop || "value", u = e.model && e.model.event || "input";
                            (l.attrs || (l.attrs = {}))[a] = l.model.value;
                            var t = l.on || (l.on = {}), r = t[u], v = l.model.callback;
                            n(r) ? (Array.isArray(r) ? -1 === r.indexOf(v) : r !== v) && (t[u] = [ v ].concat(r)) : t[u] = v;
                        }(e.options, l);
                        var f = function(e, l, a, u) {
                            var r = l.options.props;
                            if (t(r)) return Ye(e, l, {}, u);
                            var v = {}, o = e.attrs, i = e.props;
                            if (n(o) || n(i)) for (var b in r) {
                                var c = A(b);
                                el(v, i, b, c, !0) || el(v, o, b, c, !1);
                            }
                            return Ye(e, l, v, u);
                        }(l, e, 0, a);
                        if (r(e.options.functional)) return function(e, l, a, t, r) {
                            var v = e.options, o = {}, i = v.props;
                            if (n(i)) for (var b in i) o[b] = Le(b, i, l || u); else n(a.attrs) && jl(o, a.attrs), 
                            n(a.props) && jl(o, a.props);
                            var c = new Sl(a, o, r, t, e), s = v.render.call(null, c._c, c);
                            if (s instanceof oe) return kl(s, a, c.parent, v);
                            if (Array.isArray(s)) {
                                for (var f = ll(s) || [], d = new Array(f.length), h = 0; h < f.length; h++) d[h] = kl(f[h], a, c.parent, v);
                                return d;
                            }
                        }(e, f, l, a, v);
                        var d = l.on;
                        if (l.on = l.nativeOn, r(e.options.abstract)) {
                            var h = l.slot;
                            l = {}, h && (l.slot = h);
                        }
                        !function(e) {
                            for (var l = e.hook || (e.hook = {}), a = 0; a < El.length; a++) {
                                var u = El[a], t = l[u], n = Pl[u];
                                t === n || t && t._merged || (l[u] = t ? Ll(n, t) : n);
                            }
                        }(l);
                        var p = e.options.name || i;
                        return new oe("vue-component-" + e.cid + (p ? "-" + p : ""), l, void 0, void 0, void 0, a, {
                            Ctor: e,
                            propsData: f,
                            listeners: d,
                            tag: i,
                            children: v
                        }, c);
                    }
                }
            }
            function Ll(e, l) {
                var a = function(a, u) {
                    e(a, u), l(a, u);
                };
                return a._merged = !0, a;
            }
            function Il(e, l, a, u, i, b) {
                return (Array.isArray(a) || v(a)) && (i = u, u = a, a = void 0), r(b) && (i = 2), 
                function(e, l, a, u, v) {
                    if (n(a) && n(a.__ob__)) return be();
                    if (n(a) && n(a.is) && (l = a.is), !l) return be();
                    var i, b, c;
                    (Array.isArray(u) && "function" == typeof u[0] && ((a = a || {}).scopedSlots = {
                        default: u[0]
                    }, u.length = 0), 2 === v ? u = ll(u) : 1 === v && (u = function(e) {
                        for (var l = 0; l < e.length; l++) if (Array.isArray(e[l])) return Array.prototype.concat.apply([], e);
                        return e;
                    }(u)), "string" == typeof l) ? (b = e.$vnode && e.$vnode.ns || N.getTagNamespace(l), 
                    i = N.isReservedTag(l) ? new oe(N.parsePlatformTagName(l), a, u, void 0, void 0, e) : a && a.pre || !n(c = Ce(e.$options, "components", l)) ? new oe(l, a, u, void 0, void 0, e) : Cl(c, a, e, u, l)) : i = Cl(l, a, e, u);
                    return Array.isArray(i) ? i : n(i) ? (n(b) && function e(l, a, u) {
                        if (l.ns = a, "foreignObject" === l.tag && (a = void 0, u = !0), n(l.children)) for (var v = 0, o = l.children.length; v < o; v++) {
                            var i = l.children[v];
                            n(i.tag) && (t(i.ns) || r(u) && "svg" !== i.tag) && e(i, a, u);
                        }
                    }(i, b), n(a) && function(e) {
                        o(e.style) && Ke(e.style), o(e.class) && Ke(e.class);
                    }(a), i) : be();
                }(e, l, a, u, i);
            }
            var Ml, Tl = null;
            function Dl(e, l) {
                return (e.__esModule || ae && "Module" === e[Symbol.toStringTag]) && (e = e.default), 
                o(e) ? l.extend(e) : e;
            }
            function Rl(e) {
                return e.isComment && e.asyncFactory;
            }
            function Nl(e, l) {
                Ml.$on(e, l);
            }
            function Vl(e, l) {
                Ml.$off(e, l);
            }
            function Ul(e, l) {
                var a = Ml;
                return function u() {
                    var t = l.apply(null, arguments);
                    null !== t && a.$off(e, u);
                };
            }
            function Fl(e, l, a) {
                Ml = e, function(e, l, a, u, n, v) {
                    var o, i, b, c;
                    for (o in e) i = e[o], b = l[o], c = Qe(o), t(i) || (t(b) ? (t(i.fns) && (i = e[o] = Xe(i, v)), 
                    r(c.once) && (i = e[o] = n(c.name, i, c.capture)), a(c.name, i, c.capture, c.passive, c.params)) : i !== b && (b.fns = i, 
                    e[o] = b));
                    for (o in l) t(e[o]) && u((c = Qe(o)).name, l[o], c.capture);
                }(l, a || {}, Nl, Vl, Ul, e), Ml = void 0;
            }
            var Hl = null;
            function Bl(e) {
                for (;e && (e = e.$parent); ) if (e._inactive) return !0;
                return !1;
            }
            function zl(e, l) {
                if (l) {
                    if (e._directInactive = !1, Bl(e)) return;
                } else if (e._directInactive) return;
                if (e._inactive || null === e._inactive) {
                    e._inactive = !1;
                    for (var a = 0; a < e.$children.length; a++) zl(e.$children[a]);
                    Zl(e, "activated");
                }
            }
            function Zl(e, l) {
                re();
                var a = e.$options[l], u = l + " hook";
                if (a) for (var t = 0, n = a.length; t < n; t++) Re(a[t], e, null, e, u);
                e._hasHookEvent && e.$emit("hook:" + l), ve();
            }
            var ql = [], Gl = [], Jl = {}, Wl = !1, Kl = !1, Ql = 0, Xl = Date.now;
            if (z && !J) {
                var Yl = window.performance;
                Yl && "function" == typeof Yl.now && Xl() > document.createEvent("Event").timeStamp && (Xl = function() {
                    return Yl.now();
                });
            }
            function ea() {
                var e, l;
                for (Xl(), Kl = !0, ql.sort(function(e, l) {
                    return e.id - l.id;
                }), Ql = 0; Ql < ql.length; Ql++) (e = ql[Ql]).before && e.before(), l = e.id, Jl[l] = null, 
                e.run();
                var a = Gl.slice(), u = ql.slice();
                Ql = ql.length = Gl.length = 0, Jl = {}, Wl = Kl = !1, function(e) {
                    for (var l = 0; l < e.length; l++) e[l]._inactive = !0, zl(e[l], !0);
                }(a), function(e) {
                    for (var l = e.length; l--; ) {
                        var a = e[l], u = a.vm;
                        u._watcher === a && u._isMounted && !u._isDestroyed && Zl(u, "updated");
                    }
                }(u), Y && N.devtools && Y.emit("flush");
            }
            var la = 0, aa = function(e, l, a, u, t) {
                this.vm = e, t && (e._watcher = this), e._watchers.push(this), u ? (this.deep = !!u.deep, 
                this.user = !!u.user, this.lazy = !!u.lazy, this.sync = !!u.sync, this.before = u.before) : this.deep = this.user = this.lazy = this.sync = !1, 
                this.cb = a, this.id = ++la, this.active = !0, this.dirty = this.lazy, this.deps = [], 
                this.newDeps = [], this.depIds = new le(), this.newDepIds = new le(), this.expression = "", 
                "function" == typeof l ? this.getter = l : (this.getter = function(e) {
                    if (!H.test(e)) {
                        var l = e.split(".");
                        return function(e) {
                            for (var a = 0; a < l.length; a++) {
                                if (!e) return;
                                e = e[l[a]];
                            }
                            return e;
                        };
                    }
                }(l), this.getter || (this.getter = E)), this.value = this.lazy ? void 0 : this.get();
            };
            aa.prototype.get = function() {
                var e;
                re(this);
                var l = this.vm;
                try {
                    e = this.getter.call(l, l);
                } catch (e) {
                    if (!this.user) throw e;
                    De(e, l, 'getter for watcher "' + this.expression + '"');
                } finally {
                    this.deep && Ke(e), ve(), this.cleanupDeps();
                }
                return e;
            }, aa.prototype.addDep = function(e) {
                var l = e.id;
                this.newDepIds.has(l) || (this.newDepIds.add(l), this.newDeps.push(e), this.depIds.has(l) || e.addSub(this));
            }, aa.prototype.cleanupDeps = function() {
                for (var e = this.deps.length; e--; ) {
                    var l = this.deps[e];
                    this.newDepIds.has(l.id) || l.removeSub(this);
                }
                var a = this.depIds;
                this.depIds = this.newDepIds, this.newDepIds = a, this.newDepIds.clear(), a = this.deps, 
                this.deps = this.newDeps, this.newDeps = a, this.newDeps.length = 0;
            }, aa.prototype.update = function() {
                this.lazy ? this.dirty = !0 : this.sync ? this.run() : function(e) {
                    var l = e.id;
                    if (null == Jl[l]) {
                        if (Jl[l] = !0, Kl) {
                            for (var a = ql.length - 1; a > Ql && ql[a].id > e.id; ) a--;
                            ql.splice(a + 1, 0, e);
                        } else ql.push(e);
                        Wl || (Wl = !0, Je(ea));
                    }
                }(this);
            }, aa.prototype.run = function() {
                if (this.active) {
                    var e = this.get();
                    if (e !== this.value || o(e) || this.deep) {
                        var l = this.value;
                        if (this.value = e, this.user) try {
                            this.cb.call(this.vm, e, l);
                        } catch (e) {
                            De(e, this.vm, 'callback for watcher "' + this.expression + '"');
                        } else this.cb.call(this.vm, e, l);
                    }
                }
            }, aa.prototype.evaluate = function() {
                this.value = this.get(), this.dirty = !1;
            }, aa.prototype.depend = function() {
                for (var e = this.deps.length; e--; ) this.deps[e].depend();
            }, aa.prototype.teardown = function() {
                if (this.active) {
                    this.vm._isBeingDestroyed || g(this.vm._watchers, this);
                    for (var e = this.deps.length; e--; ) this.deps[e].removeSub(this);
                    this.active = !1;
                }
            };
            var ua = {
                enumerable: !0,
                configurable: !0,
                get: E,
                set: E
            };
            function ta(e, l, a) {
                ua.get = function() {
                    return this[l][a];
                }, ua.set = function(e) {
                    this[l][a] = e;
                }, Object.defineProperty(e, a, ua);
            }
            var na = {
                lazy: !0
            };
            function ra(e, l, a) {
                var u = !X();
                "function" == typeof a ? (ua.get = u ? va(l) : oa(a), ua.set = E) : (ua.get = a.get ? u && !1 !== a.cache ? va(l) : oa(a.get) : E, 
                ua.set = a.set || E), Object.defineProperty(e, l, ua);
            }
            function va(e) {
                return function() {
                    var l = this._computedWatchers && this._computedWatchers[e];
                    if (l) return l.dirty && l.evaluate(), ne.SharedObject.target && l.depend(), l.value;
                };
            }
            function oa(e) {
                return function() {
                    return e.call(this, this);
                };
            }
            function ia(e, l, a, u) {
                return b(a) && (u = a, a = a.handler), "string" == typeof a && (a = e[a]), e.$watch(l, a, u);
            }
            var ba = 0;
            function ca(e) {
                var l = e.options;
                if (e.super) {
                    var a = ca(e.super);
                    if (a !== e.superOptions) {
                        e.superOptions = a;
                        var u = function(e) {
                            var l, a = e.options, u = e.sealedOptions;
                            for (var t in a) a[t] !== u[t] && (l || (l = {}), l[t] = a[t]);
                            return l;
                        }(e);
                        u && j(e.extendOptions, u), (l = e.options = Ee(a, e.extendOptions)).name && (l.components[l.name] = e);
                    }
                }
                return l;
            }
            function sa(e) {
                this._init(e);
            }
            function fa(e) {
                return e && (e.Ctor.options.name || e.tag);
            }
            function da(e, l) {
                return Array.isArray(e) ? e.indexOf(l) > -1 : "string" == typeof e ? e.split(",").indexOf(l) > -1 : !!function(e) {
                    return "[object RegExp]" === i.call(e);
                }(e) && e.test(l);
            }
            function ha(e, l) {
                var a = e.cache, u = e.keys, t = e._vnode;
                for (var n in a) {
                    var r = a[n];
                    if (r) {
                        var v = fa(r.componentOptions);
                        v && !l(v) && pa(a, n, u, t);
                    }
                }
            }
            function pa(e, l, a, u) {
                var t = e[l];
                !t || u && t.tag === u.tag || t.componentInstance.$destroy(), e[l] = null, g(a, l);
            }
            (function(e) {
                e.prototype._init = function(e) {
                    var l = this;
                    l._uid = ba++, l._isVue = !0, e && e._isComponent ? function(e, l) {
                        var a = e.$options = Object.create(e.constructor.options), u = l._parentVnode;
                        a.parent = l.parent, a._parentVnode = u;
                        var t = u.componentOptions;
                        a.propsData = t.propsData, a._parentListeners = t.listeners, a._renderChildren = t.children, 
                        a._componentTag = t.tag, l.render && (a.render = l.render, a.staticRenderFns = l.staticRenderFns);
                    }(l, e) : l.$options = Ee(ca(l.constructor), e || {}, l), l._renderProxy = l, l._self = l, 
                    function(e) {
                        var l = e.$options, a = l.parent;
                        if (a && !l.abstract) {
                            for (;a.$options.abstract && a.$parent; ) a = a.$parent;
                            a.$children.push(e);
                        }
                        e.$parent = a, e.$root = a ? a.$root : e, e.$children = [], e.$refs = {}, e._watcher = null, 
                        e._inactive = null, e._directInactive = !1, e._isMounted = !1, e._isDestroyed = !1, 
                        e._isBeingDestroyed = !1;
                    }(l), function(e) {
                        e._events = Object.create(null), e._hasHookEvent = !1;
                        var l = e.$options._parentListeners;
                        l && Fl(e, l);
                    }(l), function(e) {
                        e._vnode = null, e._staticTrees = null;
                        var l = e.$options, a = e.$vnode = l._parentVnode, t = a && a.context;
                        e.$slots = rl(l._renderChildren, t), e.$scopedSlots = u, e._c = function(l, a, u, t) {
                            return Il(e, l, a, u, t, !1);
                        }, e.$createElement = function(l, a, u, t) {
                            return Il(e, l, a, u, t, !0);
                        };
                        var n = a && a.data;
                        me(e, "$attrs", n && n.attrs || u, null, !0), me(e, "$listeners", l._parentListeners || u, null, !0);
                    }(l), Zl(l, "beforeCreate"), !l._$fallback && tl(l), function(e) {
                        e._watchers = [];
                        var l = e.$options;
                        l.props && function(e, l) {
                            var a = e.$options.propsData || {}, u = e._props = {}, t = e.$options._propKeys = [];
                            !e.$parent || pe(!1);
                            var n = function(n) {
                                t.push(n);
                                var r = Le(n, l, a, e);
                                me(u, n, r), n in e || ta(e, "_props", n);
                            };
                            for (var r in l) n(r);
                            pe(!0);
                        }(e, l.props), l.methods && function(e, l) {
                            for (var a in e.$options.props, l) e[a] = "function" != typeof l[a] ? E : S(l[a], e);
                        }(e, l.methods), l.data ? function(e) {
                            var l = e.$options.data;
                            b(l = e._data = "function" == typeof l ? function(e, l) {
                                re();
                                try {
                                    return e.call(l, l);
                                } catch (e) {
                                    return De(e, l, "data()"), {};
                                } finally {
                                    ve();
                                }
                            }(l, e) : l || {}) || (l = {});
                            for (var a = Object.keys(l), u = e.$options.props, t = (e.$options.methods, a.length); t--; ) {
                                var n = a[t];
                                u && _(u, n) || V(n) || ta(e, "_data", n);
                            }
                            _e(l, !0);
                        }(e) : _e(e._data = {}, !0), l.computed && function(e, l) {
                            var a = e._computedWatchers = Object.create(null), u = X();
                            for (var t in l) {
                                var n = l[t], r = "function" == typeof n ? n : n.get;
                                u || (a[t] = new aa(e, r || E, E, na)), t in e || ra(e, t, n);
                            }
                        }(e, l.computed), l.watch && l.watch !== K && function(e, l) {
                            for (var a in l) {
                                var u = l[a];
                                if (Array.isArray(u)) for (var t = 0; t < u.length; t++) ia(e, a, u[t]); else ia(e, a, u);
                            }
                        }(e, l.watch);
                    }(l), !l._$fallback && ul(l), !l._$fallback && Zl(l, "created"), l.$options.el && l.$mount(l.$options.el);
                };
            })(sa), function(e) {
                Object.defineProperty(e.prototype, "$data", {
                    get: function() {
                        return this._data;
                    }
                }), Object.defineProperty(e.prototype, "$props", {
                    get: function() {
                        return this._props;
                    }
                }), e.prototype.$set = Oe, e.prototype.$delete = xe, e.prototype.$watch = function(e, l, a) {
                    if (b(l)) return ia(this, e, l, a);
                    (a = a || {}).user = !0;
                    var u = new aa(this, e, l, a);
                    if (a.immediate) try {
                        l.call(this, u.value);
                    } catch (e) {
                        De(e, this, 'callback for immediate watcher "' + u.expression + '"');
                    }
                    return function() {
                        u.teardown();
                    };
                };
            }(sa), function(e) {
                var l = /^hook:/;
                e.prototype.$on = function(e, a) {
                    var u = this;
                    if (Array.isArray(e)) for (var t = 0, n = e.length; t < n; t++) u.$on(e[t], a); else (u._events[e] || (u._events[e] = [])).push(a), 
                    l.test(e) && (u._hasHookEvent = !0);
                    return u;
                }, e.prototype.$once = function(e, l) {
                    var a = this;
                    function u() {
                        a.$off(e, u), l.apply(a, arguments);
                    }
                    return u.fn = l, a.$on(e, u), a;
                }, e.prototype.$off = function(e, l) {
                    var a = this;
                    if (!arguments.length) return a._events = Object.create(null), a;
                    if (Array.isArray(e)) {
                        for (var u = 0, t = e.length; u < t; u++) a.$off(e[u], l);
                        return a;
                    }
                    var n, r = a._events[e];
                    if (!r) return a;
                    if (!l) return a._events[e] = null, a;
                    for (var v = r.length; v--; ) if ((n = r[v]) === l || n.fn === l) {
                        r.splice(v, 1);
                        break;
                    }
                    return a;
                }, e.prototype.$emit = function(e) {
                    var l = this, a = l._events[e];
                    if (a) {
                        a = a.length > 1 ? k(a) : a;
                        for (var u = k(arguments, 1), t = 'event handler for "' + e + '"', n = 0, r = a.length; n < r; n++) Re(a[n], l, u, l, t);
                    }
                    return l;
                };
            }(sa), function(e) {
                e.prototype._update = function(e, l) {
                    var a = this, u = a.$el, t = a._vnode, n = function(e) {
                        var l = Hl;
                        return Hl = e, function() {
                            Hl = l;
                        };
                    }(a);
                    a._vnode = e, a.$el = t ? a.__patch__(t, e) : a.__patch__(a.$el, e, l, !1), n(), 
                    u && (u.__vue__ = null), a.$el && (a.$el.__vue__ = a), a.$vnode && a.$parent && a.$vnode === a.$parent._vnode && (a.$parent.$el = a.$el);
                }, e.prototype.$forceUpdate = function() {
                    this._watcher && this._watcher.update();
                }, e.prototype.$destroy = function() {
                    var e = this;
                    if (!e._isBeingDestroyed) {
                        Zl(e, "beforeDestroy"), e._isBeingDestroyed = !0;
                        var l = e.$parent;
                        !l || l._isBeingDestroyed || e.$options.abstract || g(l.$children, e), e._watcher && e._watcher.teardown();
                        for (var a = e._watchers.length; a--; ) e._watchers[a].teardown();
                        e._data.__ob__ && e._data.__ob__.vmCount--, e._isDestroyed = !0, e.__patch__(e._vnode, null), 
                        Zl(e, "destroyed"), e.$off(), e.$el && (e.$el.__vue__ = null), e.$vnode && (e.$vnode.parent = null);
                    }
                };
            }(sa), function(e) {
                Al(e.prototype), e.prototype.$nextTick = function(e) {
                    return Je(e, this);
                }, e.prototype._render = function() {
                    var e, l = this, a = l.$options, u = a.render, t = a._parentVnode;
                    t && (l.$scopedSlots = ol(t.data.scopedSlots, l.$slots, l.$scopedSlots)), l.$vnode = t;
                    try {
                        Tl = l, e = u.call(l._renderProxy, l.$createElement);
                    } catch (a) {
                        De(a, l, "render"), e = l._vnode;
                    } finally {
                        Tl = null;
                    }
                    return Array.isArray(e) && 1 === e.length && (e = e[0]), e instanceof oe || (e = be()), 
                    e.parent = t, e;
                };
            }(sa);
            var ga = [ String, RegExp, Array ], ya = {
                KeepAlive: {
                    name: "keep-alive",
                    abstract: !0,
                    props: {
                        include: ga,
                        exclude: ga,
                        max: [ String, Number ]
                    },
                    created: function() {
                        this.cache = Object.create(null), this.keys = [];
                    },
                    destroyed: function() {
                        for (var e in this.cache) pa(this.cache, e, this.keys);
                    },
                    mounted: function() {
                        var e = this;
                        this.$watch("include", function(l) {
                            ha(e, function(e) {
                                return da(l, e);
                            });
                        }), this.$watch("exclude", function(l) {
                            ha(e, function(e) {
                                return !da(l, e);
                            });
                        });
                    },
                    render: function() {
                        var e = this.$slots.default, l = function(e) {
                            if (Array.isArray(e)) for (var l = 0; l < e.length; l++) {
                                var a = e[l];
                                if (n(a) && (n(a.componentOptions) || Rl(a))) return a;
                            }
                        }(e), a = l && l.componentOptions;
                        if (a) {
                            var u = fa(a), t = this.include, r = this.exclude;
                            if (t && (!u || !da(t, u)) || r && u && da(r, u)) return l;
                            var v = this.cache, o = this.keys, i = null == l.key ? a.Ctor.cid + (a.tag ? "::" + a.tag : "") : l.key;
                            v[i] ? (l.componentInstance = v[i].componentInstance, g(o, i), o.push(i)) : (v[i] = l, 
                            o.push(i), this.max && o.length > parseInt(this.max) && pa(v, o[0], o, this._vnode)), 
                            l.data.keepAlive = !0;
                        }
                        return l || e && e[0];
                    }
                }
            };
            (function(e) {
                var l = {
                    get: function() {
                        return N;
                    }
                };
                Object.defineProperty(e, "config", l), e.util = {
                    warn: ue,
                    extend: j,
                    mergeOptions: Ee,
                    defineReactive: me
                }, e.set = Oe, e.delete = xe, e.nextTick = Je, e.observable = function(e) {
                    return _e(e), e;
                }, e.options = Object.create(null), D.forEach(function(l) {
                    e.options[l + "s"] = Object.create(null);
                }), e.options._base = e, j(e.options.components, ya), function(e) {
                    e.use = function(e) {
                        var l = this._installedPlugins || (this._installedPlugins = []);
                        if (l.indexOf(e) > -1) return this;
                        var a = k(arguments, 1);
                        return a.unshift(this), "function" == typeof e.install ? e.install.apply(e, a) : "function" == typeof e && e.apply(null, a), 
                        l.push(e), this;
                    };
                }(e), function(e) {
                    e.mixin = function(e) {
                        return this.options = Ee(this.options, e), this;
                    };
                }(e), function(e) {
                    e.cid = 0;
                    var l = 1;
                    e.extend = function(e) {
                        e = e || {};
                        var a = this, u = a.cid, t = e._Ctor || (e._Ctor = {});
                        if (t[u]) return t[u];
                        var n = e.name || a.options.name, r = function(e) {
                            this._init(e);
                        };
                        return (r.prototype = Object.create(a.prototype)).constructor = r, r.cid = l++, 
                        r.options = Ee(a.options, e), r.super = a, r.options.props && function(e) {
                            var l = e.options.props;
                            for (var a in l) ta(e.prototype, "_props", a);
                        }(r), r.options.computed && function(e) {
                            var l = e.options.computed;
                            for (var a in l) ra(e.prototype, a, l[a]);
                        }(r), r.extend = a.extend, r.mixin = a.mixin, r.use = a.use, D.forEach(function(e) {
                            r[e] = a[e];
                        }), n && (r.options.components[n] = r), r.superOptions = a.options, r.extendOptions = e, 
                        r.sealedOptions = j({}, r.options), t[u] = r, r;
                    };
                }(e), function(e) {
                    D.forEach(function(l) {
                        e[l] = function(e, a) {
                            return a ? ("component" === l && b(a) && (a.name = a.name || e, a = this.options._base.extend(a)), 
                            "directive" === l && "function" == typeof a && (a = {
                                bind: a,
                                update: a
                            }), this.options[l + "s"][e] = a, a) : this.options[l + "s"][e];
                        };
                    });
                }(e);
            })(sa), Object.defineProperty(sa.prototype, "$isServer", {
                get: X
            }), Object.defineProperty(sa.prototype, "$ssrContext", {
                get: function() {
                    return this.$vnode && this.$vnode.ssrContext;
                }
            }), Object.defineProperty(sa, "FunctionalRenderContext", {
                value: Sl
            }), sa.version = "2.6.11";
            var _a = "[object Array]", ma = "[object Object]";
            function Oa(e, l, a) {
                e[l] = a;
            }
            function xa(e) {
                return Object.prototype.toString.call(e);
            }
            function $a(e) {
                if (e.__next_tick_callbacks && e.__next_tick_callbacks.length) {
                    if (Object({
                        VUE_APP_DARK_MODE: "false",
                        VUE_APP_NAME: "牛牛一番赏",
                        VUE_APP_PLATFORM: "mp-weixin",
                        NODE_ENV: "production",
                        BASE_URL: "/"
                    }).VUE_APP_DEBUG) {
                        var l = e.$scope;
                        console.log("[" + +new Date() + "][" + (l.is || l.route) + "][" + e._uid + "]:flushCallbacks[" + e.__next_tick_callbacks.length + "]");
                    }
                    var a = e.__next_tick_callbacks.slice(0);
                    e.__next_tick_callbacks.length = 0;
                    for (var u = 0; u < a.length; u++) a[u]();
                }
            }
            function wa(e, l) {
                return l && (l._isVue || l.__v_isMPComponent) ? {} : l;
            }
            function Aa() {}
            var Sa = m(function(e) {
                var l = {}, a = /:(.+)/;
                return e.split(/;(?![^(]*\))/g).forEach(function(e) {
                    if (e) {
                        var u = e.split(a);
                        u.length > 1 && (l[u[0].trim()] = u[1].trim());
                    }
                }), l;
            }), ka = [ "createSelectorQuery", "createIntersectionObserver", "selectAllComponents", "selectComponent" ], ja = [ "onLaunch", "onShow", "onHide", "onUniNViewMessage", "onPageNotFound", "onThemeChange", "onError", "onUnhandledRejection", "onInit", "onLoad", "onReady", "onUnload", "onPullDownRefresh", "onReachBottom", "onTabItemTap", "onAddToFavorites", "onShareTimeline", "onShareAppMessage", "onResize", "onPageScroll", "onNavigationBarButtonTap", "onBackPress", "onNavigationBarSearchInputChanged", "onNavigationBarSearchInputConfirmed", "onNavigationBarSearchInputClicked", "onPageShow", "onPageHide", "onPageResize", "onUploadDouyinVideo" ];
            sa.prototype.__patch__ = function(e, l) {
                var a = this;
                if (null !== l && ("page" === this.mpType || "component" === this.mpType)) {
                    var u = this.$scope, t = Object.create(null);
                    try {
                        t = function(e) {
                            var l = Object.create(null);
                            [].concat(Object.keys(e._data || {}), Object.keys(e._computedWatchers || {})).reduce(function(l, a) {
                                return l[a] = e[a], l;
                            }, l);
                            var a = e.__composition_api_state__ || e.__secret_vfa_state__, u = a && a.rawBindings;
                            return u && Object.keys(u).forEach(function(a) {
                                l[a] = e[a];
                            }), Object.assign(l, e.$mp.data || {}), Array.isArray(e.$options.behaviors) && -1 !== e.$options.behaviors.indexOf("uni://form-field") && (l.name = e.name, 
                            l.value = e.value), JSON.parse(JSON.stringify(l, wa));
                        }(this);
                    } catch (e) {
                        console.error(e);
                    }
                    t.__webviewId__ = u.data.__webviewId__;
                    var n = Object.create(null);
                    Object.keys(t).forEach(function(e) {
                        n[e] = u.data[e];
                    });
                    var r = !1 === this.$shouldDiffData ? t : function(e, l) {
                        var a = {};
                        return function e(l, a) {
                            if (l !== a) {
                                var u = xa(l), t = xa(a);
                                if (u == ma && t == ma) {
                                    if (Object.keys(l).length >= Object.keys(a).length) for (var n in a) {
                                        var r = l[n];
                                        void 0 === r ? l[n] = null : e(r, a[n]);
                                    }
                                } else u == _a && t == _a && l.length >= a.length && a.forEach(function(a, u) {
                                    e(l[u], a);
                                });
                            }
                        }(e, l), function e(l, a, u, t) {
                            if (l !== a) {
                                var n = xa(l), r = xa(a);
                                if (n == ma) if (r != ma || Object.keys(l).length < Object.keys(a).length) Oa(t, u, l); else {
                                    var v = function(n) {
                                        var r = l[n], v = a[n], o = xa(r), i = xa(v);
                                        if (o != _a && o != ma) r !== a[n] && function(e, l) {
                                            return "[object Null]" !== e && "[object Undefined]" !== e || "[object Null]" !== l && "[object Undefined]" !== l;
                                        }(o, i) && Oa(t, ("" == u ? "" : u + ".") + n, r); else if (o == _a) i != _a || r.length < v.length ? Oa(t, ("" == u ? "" : u + ".") + n, r) : r.forEach(function(l, a) {
                                            e(l, v[a], ("" == u ? "" : u + ".") + n + "[" + a + "]", t);
                                        }); else if (o == ma) if (i != ma || Object.keys(r).length < Object.keys(v).length) Oa(t, ("" == u ? "" : u + ".") + n, r); else for (var b in r) e(r[b], v[b], ("" == u ? "" : u + ".") + n + "." + b, t);
                                    };
                                    for (var o in l) v(o);
                                } else n == _a ? r != _a || l.length < a.length ? Oa(t, u, l) : l.forEach(function(l, n) {
                                    e(l, a[n], u + "[" + n + "]", t);
                                }) : Oa(t, u, l);
                            }
                        }(e, l, "", a), a;
                    }(t, n);
                    Object.keys(r).length ? (Object({
                        VUE_APP_DARK_MODE: "false",
                        VUE_APP_NAME: "牛牛一番赏",
                        VUE_APP_PLATFORM: "mp-weixin",
                        NODE_ENV: "production",
                        BASE_URL: "/"
                    }).VUE_APP_DEBUG && console.log("[" + +new Date() + "][" + (u.is || u.route) + "][" + this._uid + "]差量更新", JSON.stringify(r)), 
                    this.__next_tick_pending = !0, u.setData(r, function() {
                        a.__next_tick_pending = !1, $a(a);
                    })) : $a(this);
                }
            }, sa.prototype.$mount = function(e, l) {
                return function(e, l, a) {
                    return e.mpType ? ("app" === e.mpType && (e.$options.render = Aa), e.$options.render || (e.$options.render = Aa), 
                    !e._$fallback && Zl(e, "beforeMount"), new aa(e, function() {
                        e._update(e._render(), a);
                    }, E, {
                        before: function() {
                            e._isMounted && !e._isDestroyed && Zl(e, "beforeUpdate");
                        }
                    }, !0), a = !1, e) : e;
                }(this, 0, l);
            }, function(e) {
                var l = e.extend;
                e.extend = function(e) {
                    var a = (e = e || {}).methods;
                    return a && Object.keys(a).forEach(function(l) {
                        -1 !== ja.indexOf(l) && (e[l] = a[l], delete a[l]);
                    }), l.call(this, e);
                };
                var a = e.config.optionMergeStrategies, u = a.created;
                ja.forEach(function(e) {
                    a[e] = u;
                }), e.prototype.__lifecycle_hooks__ = ja;
            }(sa), function(e) {
                e.config.errorHandler = function(l, a, u) {
                    e.util.warn("Error in " + u + ': "' + l.toString() + '"', a), console.error(l);
                    var t = "function" == typeof getApp && getApp();
                    t && t.onError && t.onError(l);
                };
                var l = e.prototype.$emit;
                e.prototype.$emit = function(e) {
                    if (this.$scope && e) {
                        var a = this.$scope._triggerEvent || this.$scope.triggerEvent;
                        if (a) try {
                            a.call(this.$scope, e, {
                                __args__: k(arguments, 1)
                            });
                        } catch (e) {}
                    }
                    return l.apply(this, arguments);
                }, e.prototype.$nextTick = function(e) {
                    return function(e, l) {
                        if (!e.__next_tick_pending && !function(e) {
                            return ql.find(function(l) {
                                return e._watcher === l;
                            });
                        }(e)) {
                            if (Object({
                                VUE_APP_DARK_MODE: "false",
                                VUE_APP_NAME: "牛牛一番赏",
                                VUE_APP_PLATFORM: "mp-weixin",
                                NODE_ENV: "production",
                                BASE_URL: "/"
                            }).VUE_APP_DEBUG) {
                                var a = e.$scope;
                                console.log("[" + +new Date() + "][" + (a.is || a.route) + "][" + e._uid + "]:nextVueTick");
                            }
                            return Je(l, e);
                        }
                        if (Object({
                            VUE_APP_DARK_MODE: "false",
                            VUE_APP_NAME: "牛牛一番赏",
                            VUE_APP_PLATFORM: "mp-weixin",
                            NODE_ENV: "production",
                            BASE_URL: "/"
                        }).VUE_APP_DEBUG) {
                            var u = e.$scope;
                            console.log("[" + +new Date() + "][" + (u.is || u.route) + "][" + e._uid + "]:nextMPTick");
                        }
                        var t;
                        if (e.__next_tick_callbacks || (e.__next_tick_callbacks = []), e.__next_tick_callbacks.push(function() {
                            if (l) try {
                                l.call(e);
                            } catch (l) {
                                De(l, e, "nextTick");
                            } else t && t(e);
                        }), !l && "undefined" != typeof Promise) return new Promise(function(e) {
                            t = e;
                        });
                    }(this, e);
                }, ka.forEach(function(l) {
                    e.prototype[l] = function(e) {
                        return this.$scope && this.$scope[l] ? this.$scope[l](e) : "undefined" != typeof my ? "createSelectorQuery" === l ? my.createSelectorQuery(e) : "createIntersectionObserver" === l ? my.createIntersectionObserver(e) : void 0 : void 0;
                    };
                }), e.prototype.__init_provide = ul, e.prototype.__init_injections = tl, e.prototype.__call_hook = function(e, l) {
                    var a = this;
                    re();
                    var u, t = a.$options[e], n = e + " hook";
                    if (t) for (var r = 0, v = t.length; r < v; r++) u = Re(t[r], a, l ? [ l ] : null, a, n);
                    return a._hasHookEvent && a.$emit("hook:" + e, l), ve(), u;
                }, e.prototype.__set_model = function(l, a, u, t) {
                    Array.isArray(t) && (-1 !== t.indexOf("trim") && (u = u.trim()), -1 !== t.indexOf("number") && (u = this._n(u))), 
                    l || (l = this), e.set(l, a, u);
                }, e.prototype.__set_sync = function(l, a, u) {
                    l || (l = this), e.set(l, a, u);
                }, e.prototype.__get_orig = function(e) {
                    return b(e) && e.$orig || e;
                }, e.prototype.__get_value = function(e, l) {
                    return function e(l, a) {
                        var u = a.split("."), t = u[0];
                        return 0 === t.indexOf("__$n") && (t = parseInt(t.replace("__$n", ""))), 1 === u.length ? l[t] : e(l[t], u.slice(1).join("."));
                    }(l || this, e);
                }, e.prototype.__get_class = function(e, l) {
                    return function(e, l) {
                        return n(e) || n(l) ? function(e, l) {
                            return e ? l ? e + " " + l : e : l || "";
                        }(e, function e(l) {
                            return Array.isArray(l) ? function(l) {
                                for (var a, u = "", t = 0, r = l.length; t < r; t++) n(a = e(l[t])) && "" !== a && (u && (u += " "), 
                                u += a);
                                return u;
                            }(l) : o(l) ? function(e) {
                                var l = "";
                                for (var a in e) e[a] && (l && (l += " "), l += a);
                                return l;
                            }(l) : "string" == typeof l ? l : "";
                        }(l)) : "";
                    }(l, e);
                }, e.prototype.__get_style = function(e, l) {
                    if (!e && !l) return "";
                    var a = function(e) {
                        return Array.isArray(e) ? P(e) : "string" == typeof e ? Sa(e) : e;
                    }(e), u = l ? j(l, a) : a;
                    return Object.keys(u).map(function(e) {
                        return A(e) + ":" + u[e];
                    }).join(";");
                }, e.prototype.__map = function(e, l) {
                    var a, u, t, n, r;
                    if (Array.isArray(e)) {
                        for (a = new Array(e.length), u = 0, t = e.length; u < t; u++) a[u] = l(e[u], u);
                        return a;
                    }
                    if (o(e)) {
                        for (n = Object.keys(e), a = Object.create(null), u = 0, t = n.length; u < t; u++) a[r = n[u]] = l(e[r], r, u);
                        return a;
                    }
                    if ("number" == typeof e) {
                        for (a = new Array(e), u = 0, t = e; u < t; u++) a[u] = l(u, u);
                        return a;
                    }
                    return [];
                };
            }(sa), a.default = sa;
        }.call(this, u("c8ba"));
    },
    "6c70": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = function() {
            for (var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : void 0, l = this.$parent; l; ) {
                if (!l.$options || l.$options.name === e) return l;
                l = l.$parent;
            }
            return !1;
        };
    },
    "6f8f": function(e, l) {
        e.exports = function() {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function() {})), 
                !0;
            } catch (e) {
                return !1;
            }
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    7037: function(l, a) {
        function u(a) {
            return l.exports = u = "function" == typeof Symbol && "symbol" == e(Symbol.iterator) ? function(l) {
                return e(l);
            } : function(l) {
                return l && "function" == typeof Symbol && l.constructor === Symbol && l !== Symbol.prototype ? "symbol" : e(l);
            }, l.exports.__esModule = !0, l.exports.default = l.exports, u(a);
        }
        l.exports = u, l.exports.__esModule = !0, l.exports.default = l.exports;
    },
    7067: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0, l.default = {
            toast: 10090,
            noNetwork: 10080,
            popup: 10075,
            mask: 10070,
            navbar: 980,
            topTips: 975,
            sticky: 970,
            indexListSticky: 965
        };
    },
    7929: function(e, l, a) {
        (function(l) {
            var u = a("4ea4"), t = u(a("29a2"));
            u(a("b1e2")), e.exports = {
                post: function(e, a) {
                    return e = t.default.APIHOST + e, new Promise(function(u, t) {
                        l.request({
                            url: e,
                            data: a,
                            method: "POST",
                            header: {
                                Authorization: "Bearer " + l.getStorageSync("USERINFO")
                            },
                            success: function(e) {
                                0 == e.data.code || 3 == e.data.code || 406 == e.data.code ? u(e.data) : (l.showToast({
                                    title: e.data.msg,
                                    icon: "none"
                                }), t());
                            },
                            fail: function(e) {
                                t(e);
                            }
                        });
                    });
                },
                get: function(e, a) {
                    return e = t.default.APIHOST + e, new Promise(function(u, t) {
                        l.request({
                            url: e,
                            data: a,
                            method: "GET",
                            header: {
                                Authorization: "Bearer " + l.getStorageSync("USERINFO")
                            },
                            success: function(e) {
                                0 == e.data.code || 3 == e.data.code || 406 == e.data.code ? u(e.data) : (l.showToast({
                                    title: e.data.msg,
                                    icon: "none"
                                }), t());
                            }
                        });
                    });
                },
                caleJiaoBiao: function() {
                    l.getStorageSync("USERINFO");
                },
                uploadImg: function(e) {
                    var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1, u = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {}, n = arguments.length > 3 ? arguments[3] : void 0, r = t.default.APIHOST;
                    l.chooseImage({
                        count: a,
                        sizeType: [ "original", "compressed" ],
                        sourceType: [ "album", "camera" ],
                        success: function(a) {
                            var t = a.tempFilePaths, v = {};
                            for (var o in t) v["uploadTask" + o] = l.uploadFile({
                                url: r + "v1/5d5fa8984f0c2",
                                filePath: t[o],
                                name: "file[]",
                                formData: u,
                                success: function(a) {
                                    if (200 == a.statusCode) {
                                        var u = JSON.parse(a.data);
                                        "function" == typeof e && e(u);
                                    } else l.showToast({
                                        title: a.errMsg,
                                        icon: "none"
                                    });
                                },
                                fail: function(e) {
                                    l.showToast({
                                        title: "图片上传失败，请稍后再试." + e.errMsg,
                                        icon: "none"
                                    });
                                }
                            }), v["uploadTask" + o].onProgressUpdate(function(e) {
                                "function" == typeof n && n(e), e.progress < 99.99 ? l.showLoading({
                                    title: e.progress + "%"
                                }) : l.hideLoading();
                            });
                        }
                    });
                }
            };
        }).call(this, a("543d").default);
    },
    "7f6b": function(e, l, a) {
        (function(e) {
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.os = function() {
                return e.getSystemInfoSync().platform;
            }, l.sys = function() {
                return e.getSystemInfoSync();
            };
        }).call(this, a("543d").default);
    },
    "81d3": function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("8625"));
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null, l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "yyyy-mm-dd";
            null == e && (e = Number(new Date())), 10 == (e = parseInt(e)).toString().length && (e *= 1e3);
            var a = new Date().getTime() - e, u = "";
            switch (!0) {
              case (a = parseInt(a / 1e3)) < 300:
                u = "刚刚";
                break;

              case a >= 300 && a < 3600:
                u = parseInt(a / 60) + "分钟前";
                break;

              case a >= 3600 && a < 86400:
                u = parseInt(a / 3600) + "小时前";
                break;

              case a >= 86400 && a < 2592e3:
                u = parseInt(a / 86400) + "天前";
                break;

              default:
                u = !1 === l ? a >= 2592e3 && a < 31536e3 ? parseInt(a / 2592e3) + "个月前" : parseInt(a / 31536e3) + "年前" : (0, 
                t.default)(e, l);
            }
            return u;
        };
    },
    8625: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0, String.prototype.padStart || (String.prototype.padStart = function(e) {
            var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : " ";
            if ("[object String]" !== Object.prototype.toString.call(l)) throw new TypeError("fillString must be String");
            var a = this;
            if (a.length >= e) return String(a);
            for (var u = e - a.length, t = Math.ceil(u / l.length); t >>= 1; ) l += l, 1 === t && (l += l);
            return l.slice(0, u) + a;
        });
        l.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null, l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "yyyy-mm-dd";
            (e = parseInt(e)) || (e = Number(new Date())), 10 == e.toString().length && (e *= 1e3);
            var a, u = new Date(e), t = {
                "y+": u.getFullYear().toString(),
                "m+": (u.getMonth() + 1).toString(),
                "d+": u.getDate().toString(),
                "h+": u.getHours().toString(),
                "M+": u.getMinutes().toString(),
                "s+": u.getSeconds().toString()
            };
            for (var n in t) (a = new RegExp("(" + n + ")").exec(l)) && (l = l.replace(a[1], 1 == a[1].length ? t[n] : t[n].padStart(a[1].length, "0")));
            return l;
        };
    },
    8740: function(e, l) {},
    "8bbd": function(e, l, a) {
        var u;
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function(e) {
            var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 500, a = !(arguments.length > 2 && void 0 !== arguments[2]) || arguments[2];
            a ? u || (u = !0, "function" == typeof e && e(), setTimeout(function() {
                u = !1;
            }, l)) : u || (u = !0, setTimeout(function() {
                u = !1, "function" == typeof e && e();
            }, l));
        };
    },
    9298: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("bb09")), n = u(a("ee21")), r = u(a("242c")), v = u(a("02bc")), o = u(a("8625")), i = u(a("81d3")), b = u(a("2bc2")), c = u(a("48aa")), s = u(a("99e3")), f = u(a("635e")), d = u(a("2678")), h = u(a("c4a7")), p = u(a("a383")), g = u(a("1938")), y = u(a("b5d5")), _ = u(a("b144")), m = u(a("e258")), O = u(a("37fd")), x = u(a("f5a1")), $ = u(a("6c70")), w = a("7f6b"), A = u(a("54cf")), S = u(a("8bbd")), k = u(a("93ae")), j = u(a("7067")), P = {
            queryParams: r.default,
            route: v.default,
            timeFormat: o.default,
            date: o.default,
            timeFrom: i.default,
            colorGradient: b.default.colorGradient,
            guid: c.default,
            color: s.default,
            sys: w.sys,
            os: w.os,
            type2icon: f.default,
            randomArray: d.default,
            wranning: function(e) {},
            get: n.default.get,
            post: n.default.post,
            put: n.default.put,
            delete: n.default.delete,
            hexToRgb: b.default.hexToRgb,
            rgbToHex: b.default.rgbToHex,
            test: y.default,
            random: _.default,
            deepClone: h.default,
            deepMerge: p.default,
            getParent: x.default,
            $parent: $.default,
            addUnit: g.default,
            trim: m.default,
            type: [ "primary", "success", "error", "warning", "info" ],
            http: n.default,
            toast: O.default,
            config: k.default,
            zIndex: j.default,
            debounce: A.default,
            throttle: S.default
        }, E = {
            install: function(e) {
                e.mixin(t.default), e.prototype.openShare && e.mixin(mpShare), e.filter("timeFormat", function(e, l) {
                    return (0, o.default)(e, l);
                }), e.filter("date", function(e, l) {
                    return (0, o.default)(e, l);
                }), e.filter("timeFrom", function(e, l) {
                    return (0, i.default)(e, l);
                }), e.prototype.$u = P;
            }
        };
        l.default = E;
    },
    "93ae": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = {
            v: "1.7.1",
            version: "1.7.1",
            type: [ "primary", "success", "info", "error", "warning" ]
        };
    },
    9523: function(e, l, a) {
        var u = a("a395");
        e.exports = function(e, l, a) {
            return (l = u(l)) in e ? Object.defineProperty(e, l, {
                value: a,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[l] = a, e;
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "970b": function(e, l) {
        e.exports = function(e, l) {
            if (!(e instanceof l)) throw new TypeError("Cannot call a class as a function");
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    "99e3": function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = {
            primary: "#2979ff",
            primaryDark: "#2b85e4",
            primaryDisabled: "#a0cfff",
            primaryLight: "#ecf5ff",
            bgColor: "#f3f4f6",
            info: "#909399",
            infoDark: "#82848a",
            infoDisabled: "#c8c9cc",
            infoLight: "#f4f4f5",
            warning: "#ff9900",
            warningDark: "#f29100",
            warningDisabled: "#fcbd71",
            warningLight: "#fdf6ec",
            error: "#fa3534",
            errorDark: "#dd6161",
            errorDisabled: "#fab6b6",
            errorLight: "#fef0f0",
            success: "#19be6b",
            successDark: "#18b566",
            successDisabled: "#71d5a1",
            successLight: "#dbf1e1",
            mainColor: "#303133",
            contentColor: "#606266",
            tipsColor: "#909399",
            lightColor: "#c0c4cc",
            borderColor: "#e4e7ed"
        };
    },
    "9b42": function(e, l) {
        e.exports = function(e, l) {
            var a = null == e ? null : "undefined" != typeof Symbol && e[Symbol.iterator] || e["@@iterator"];
            if (null != a) {
                var u, t, n, r, v = [], o = !0, i = !1;
                try {
                    if (n = (a = a.call(e)).next, 0 === l) {
                        if (Object(a) !== a) return;
                        o = !1;
                    } else for (;!(o = (u = n.call(a)).done) && (v.push(u.value), v.length !== l); o = !0) ;
                } catch (e) {
                    i = !0, t = e;
                } finally {
                    try {
                        if (!o && null != a.return && (r = a.return(), Object(r) !== r)) return;
                    } finally {
                        if (i) throw t;
                    }
                }
                return v;
            }
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    a383: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("7037")), n = u(a("c4a7"));
        l.default = function e() {
            var l = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
            if (l = (0, n.default)(l), "object" !== (0, t.default)(l) || "object" !== (0, t.default)(a)) return !1;
            for (var u in a) a.hasOwnProperty(u) && (u in l ? "object" !== (0, t.default)(l[u]) || "object" !== (0, 
            t.default)(a[u]) ? l[u] = a[u] : l[u].concat && a[u].concat ? l[u] = l[u].concat(a[u]) : l[u] = e(l[u], a[u]) : l[u] = a[u]);
            return l;
        };
    },
    a395: function(e, l, a) {
        var u = a("7037").default, t = a("e50d");
        e.exports = function(e) {
            var l = t(e, "string");
            return "symbol" === u(l) ? l : String(l);
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    a990: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0, l.default = [ {
            value: "110000",
            label: "北京",
            children: [ {
                value: "110100",
                label: "北京",
                children: [ {
                    value: "110101",
                    label: "东城区"
                }, {
                    value: "110102",
                    label: "西城区"
                }, {
                    value: "110105",
                    label: "朝阳区"
                }, {
                    value: "110106",
                    label: "丰台区"
                }, {
                    value: "110107",
                    label: "石景山区"
                }, {
                    value: "110108",
                    label: "海淀区"
                }, {
                    value: "110109",
                    label: "门头沟区"
                }, {
                    value: "110111",
                    label: "房山区"
                }, {
                    value: "110112",
                    label: "通州区"
                }, {
                    value: "110113",
                    label: "顺义区"
                }, {
                    value: "110114",
                    label: "昌平区"
                }, {
                    value: "110115",
                    label: "大兴区"
                }, {
                    value: "110116",
                    label: "怀柔区"
                }, {
                    value: "110117",
                    label: "平谷区"
                }, {
                    value: "110118",
                    label: "密云区"
                }, {
                    value: "110119",
                    label: "延庆区"
                } ]
            } ]
        }, {
            value: "120000",
            label: "天津",
            children: [ {
                value: "120100",
                label: "天津",
                children: [ {
                    value: "120101",
                    label: "和平区"
                }, {
                    value: "120102",
                    label: "河东区"
                }, {
                    value: "120103",
                    label: "河西区"
                }, {
                    value: "120104",
                    label: "南开区"
                }, {
                    value: "120105",
                    label: "河北区"
                }, {
                    value: "120106",
                    label: "红桥区"
                }, {
                    value: "120110",
                    label: "东丽区"
                }, {
                    value: "120111",
                    label: "西青区"
                }, {
                    value: "120112",
                    label: "津南区"
                }, {
                    value: "120113",
                    label: "北辰区"
                }, {
                    value: "120114",
                    label: "武清区"
                }, {
                    value: "120115",
                    label: "宝坻区"
                }, {
                    value: "120116",
                    label: "滨海新区"
                }, {
                    value: "120117",
                    label: "宁河区"
                }, {
                    value: "120118",
                    label: "静海区"
                }, {
                    value: "120119",
                    label: "蓟州区"
                } ]
            } ]
        }, {
            value: "130000",
            label: "河北",
            children: [ {
                value: "130100",
                label: "石家庄",
                children: [ {
                    value: "130102",
                    label: "长安区"
                }, {
                    value: "130104",
                    label: "桥西区"
                }, {
                    value: "130105",
                    label: "新华区"
                }, {
                    value: "130107",
                    label: "井陉矿区"
                }, {
                    value: "130108",
                    label: "裕华区"
                }, {
                    value: "130109",
                    label: "藁城区"
                }, {
                    value: "130110",
                    label: "鹿泉区"
                }, {
                    value: "130111",
                    label: "栾城区"
                }, {
                    value: "130121",
                    label: "井陉县"
                }, {
                    value: "130123",
                    label: "正定县"
                }, {
                    value: "130125",
                    label: "行唐县"
                }, {
                    value: "130126",
                    label: "灵寿县"
                }, {
                    value: "130127",
                    label: "高邑县"
                }, {
                    value: "130128",
                    label: "深泽县"
                }, {
                    value: "130129",
                    label: "赞皇县"
                }, {
                    value: "130130",
                    label: "无极县"
                }, {
                    value: "130131",
                    label: "平山县"
                }, {
                    value: "130132",
                    label: "元氏县"
                }, {
                    value: "130133",
                    label: "赵县"
                }, {
                    value: "130181",
                    label: "辛集市"
                }, {
                    value: "130183",
                    label: "晋州市"
                }, {
                    value: "130184",
                    label: "新乐市"
                } ]
            }, {
                value: "130200",
                label: "唐山",
                children: [ {
                    value: "130202",
                    label: "路南区"
                }, {
                    value: "130203",
                    label: "路北区"
                }, {
                    value: "130204",
                    label: "古冶区"
                }, {
                    value: "130205",
                    label: "开平区"
                }, {
                    value: "130207",
                    label: "丰南区"
                }, {
                    value: "130208",
                    label: "丰润区"
                }, {
                    value: "130209",
                    label: "曹妃甸区"
                }, {
                    value: "130223",
                    label: "滦县"
                }, {
                    value: "130224",
                    label: "滦南县"
                }, {
                    value: "130225",
                    label: "乐亭县"
                }, {
                    value: "130227",
                    label: "迁西县"
                }, {
                    value: "130229",
                    label: "玉田县"
                }, {
                    value: "130281",
                    label: "遵化市"
                }, {
                    value: "130283",
                    label: "迁安市"
                } ]
            }, {
                value: "130300",
                label: "秦皇岛",
                children: [ {
                    value: "130302",
                    label: "海港区"
                }, {
                    value: "130303",
                    label: "山海关区"
                }, {
                    value: "130304",
                    label: "北戴河区"
                }, {
                    value: "130306",
                    label: "抚宁区"
                }, {
                    value: "130321",
                    label: "青龙满族自治县"
                }, {
                    value: "130322",
                    label: "昌黎县"
                }, {
                    value: "130324",
                    label: "卢龙县"
                } ]
            }, {
                value: "130400",
                label: "邯郸",
                children: [ {
                    value: "130402",
                    label: "邯山区"
                }, {
                    value: "130403",
                    label: "丛台区"
                }, {
                    value: "130404",
                    label: "复兴区"
                }, {
                    value: "130406",
                    label: "峰峰矿区"
                }, {
                    value: "130407",
                    label: "肥乡区"
                }, {
                    value: "130408",
                    label: "永年区"
                }, {
                    value: "130423",
                    label: "临漳县"
                }, {
                    value: "130424",
                    label: "成安县"
                }, {
                    value: "130425",
                    label: "大名县"
                }, {
                    value: "130426",
                    label: "涉县"
                }, {
                    value: "130427",
                    label: "磁县"
                }, {
                    value: "130430",
                    label: "邱县"
                }, {
                    value: "130431",
                    label: "鸡泽县"
                }, {
                    value: "130432",
                    label: "广平县"
                }, {
                    value: "130433",
                    label: "馆陶县"
                }, {
                    value: "130434",
                    label: "魏县"
                }, {
                    value: "130435",
                    label: "曲周县"
                }, {
                    value: "130481",
                    label: "武安市"
                } ]
            }, {
                value: "130500",
                label: "邢台",
                children: [ {
                    value: "130502",
                    label: "桥东区"
                }, {
                    value: "130503",
                    label: "桥西区"
                }, {
                    value: "130521",
                    label: "邢台县"
                }, {
                    value: "130522",
                    label: "临城县"
                }, {
                    value: "130523",
                    label: "内丘县"
                }, {
                    value: "130524",
                    label: "柏乡县"
                }, {
                    value: "130525",
                    label: "隆尧县"
                }, {
                    value: "130526",
                    label: "任县"
                }, {
                    value: "130527",
                    label: "南和县"
                }, {
                    value: "130528",
                    label: "宁晋县"
                }, {
                    value: "130529",
                    label: "巨鹿县"
                }, {
                    value: "130530",
                    label: "新河县"
                }, {
                    value: "130531",
                    label: "广宗县"
                }, {
                    value: "130532",
                    label: "平乡县"
                }, {
                    value: "130533",
                    label: "威县"
                }, {
                    value: "130534",
                    label: "清河县"
                }, {
                    value: "130535",
                    label: "临西县"
                }, {
                    value: "130581",
                    label: "南宫市"
                }, {
                    value: "130582",
                    label: "沙河市"
                } ]
            }, {
                value: "130600",
                label: "保定",
                children: [ {
                    value: "130602",
                    label: "竞秀区"
                }, {
                    value: "130606",
                    label: "莲池区"
                }, {
                    value: "130607",
                    label: "满城区"
                }, {
                    value: "130608",
                    label: "清苑区"
                }, {
                    value: "130609",
                    label: "徐水区"
                }, {
                    value: "130623",
                    label: "涞水县"
                }, {
                    value: "130624",
                    label: "阜平县"
                }, {
                    value: "130626",
                    label: "定兴县"
                }, {
                    value: "130627",
                    label: "唐县"
                }, {
                    value: "130628",
                    label: "高阳县"
                }, {
                    value: "130629",
                    label: "容城县"
                }, {
                    value: "130630",
                    label: "涞源县"
                }, {
                    value: "130631",
                    label: "望都县"
                }, {
                    value: "130632",
                    label: "安新县"
                }, {
                    value: "130633",
                    label: "易县"
                }, {
                    value: "130634",
                    label: "曲阳县"
                }, {
                    value: "130635",
                    label: "蠡县"
                }, {
                    value: "130636",
                    label: "顺平县"
                }, {
                    value: "130637",
                    label: "博野县"
                }, {
                    value: "130638",
                    label: "雄县"
                }, {
                    value: "130681",
                    label: "涿州市"
                }, {
                    value: "130682",
                    label: "定州市"
                }, {
                    value: "130683",
                    label: "安国市"
                }, {
                    value: "130684",
                    label: "高碑店市"
                } ]
            }, {
                value: "130700",
                label: "张家口",
                children: [ {
                    value: "130702",
                    label: "桥东区"
                }, {
                    value: "130703",
                    label: "桥西区"
                }, {
                    value: "130705",
                    label: "宣化区"
                }, {
                    value: "130706",
                    label: "下花园区"
                }, {
                    value: "130708",
                    label: "万全区"
                }, {
                    value: "130709",
                    label: "崇礼区"
                }, {
                    value: "130722",
                    label: "张北县"
                }, {
                    value: "130723",
                    label: "康保县"
                }, {
                    value: "130724",
                    label: "沽源县"
                }, {
                    value: "130725",
                    label: "尚义县"
                }, {
                    value: "130726",
                    label: "蔚县"
                }, {
                    value: "130727",
                    label: "阳原县"
                }, {
                    value: "130728",
                    label: "怀安县"
                }, {
                    value: "130730",
                    label: "怀来县"
                }, {
                    value: "130731",
                    label: "涿鹿县"
                }, {
                    value: "130732",
                    label: "赤城县"
                } ]
            }, {
                value: "130800",
                label: "承德",
                children: [ {
                    value: "130802",
                    label: "双桥区"
                }, {
                    value: "130803",
                    label: "双滦区"
                }, {
                    value: "130804",
                    label: "鹰手营子矿区"
                }, {
                    value: "130821",
                    label: "承德县"
                }, {
                    value: "130822",
                    label: "兴隆县"
                }, {
                    value: "130824",
                    label: "滦平县"
                }, {
                    value: "130825",
                    label: "隆化县"
                }, {
                    value: "130826",
                    label: "丰宁满族自治县"
                }, {
                    value: "130827",
                    label: "宽城满族自治县"
                }, {
                    value: "130828",
                    label: "围场满族蒙古族自治县"
                }, {
                    value: "130881",
                    label: "平泉市"
                } ]
            }, {
                value: "130900",
                label: "沧州",
                children: [ {
                    value: "130902",
                    label: "新华区"
                }, {
                    value: "130903",
                    label: "运河区"
                }, {
                    value: "130921",
                    label: "沧县"
                }, {
                    value: "130922",
                    label: "青县"
                }, {
                    value: "130923",
                    label: "东光县"
                }, {
                    value: "130924",
                    label: "海兴县"
                }, {
                    value: "130925",
                    label: "盐山县"
                }, {
                    value: "130926",
                    label: "肃宁县"
                }, {
                    value: "130927",
                    label: "南皮县"
                }, {
                    value: "130928",
                    label: "吴桥县"
                }, {
                    value: "130929",
                    label: "献县"
                }, {
                    value: "130930",
                    label: "孟村回族自治县"
                }, {
                    value: "130981",
                    label: "泊头市"
                }, {
                    value: "130982",
                    label: "任丘市"
                }, {
                    value: "130983",
                    label: "黄骅市"
                }, {
                    value: "130984",
                    label: "河间市"
                } ]
            }, {
                value: "131000",
                label: "廊坊",
                children: [ {
                    value: "131002",
                    label: "安次区"
                }, {
                    value: "131003",
                    label: "广阳区"
                }, {
                    value: "131022",
                    label: "固安县"
                }, {
                    value: "131023",
                    label: "永清县"
                }, {
                    value: "131024",
                    label: "香河县"
                }, {
                    value: "131025",
                    label: "大城县"
                }, {
                    value: "131026",
                    label: "文安县"
                }, {
                    value: "131028",
                    label: "大厂回族自治县"
                }, {
                    value: "131081",
                    label: "霸州市"
                }, {
                    value: "131082",
                    label: "三河市"
                } ]
            }, {
                value: "131100",
                label: "衡水",
                children: [ {
                    value: "131102",
                    label: "桃城区"
                }, {
                    value: "131103",
                    label: "冀州区"
                }, {
                    value: "131121",
                    label: "枣强县"
                }, {
                    value: "131122",
                    label: "武邑县"
                }, {
                    value: "131123",
                    label: "武强县"
                }, {
                    value: "131124",
                    label: "饶阳县"
                }, {
                    value: "131125",
                    label: "安平县"
                }, {
                    value: "131126",
                    label: "故城县"
                }, {
                    value: "131127",
                    label: "景县"
                }, {
                    value: "131128",
                    label: "阜城县"
                }, {
                    value: "131182",
                    label: "深州市"
                } ]
            } ]
        }, {
            value: "140000",
            label: "山西",
            children: [ {
                value: "140100",
                label: "太原",
                children: [ {
                    value: "140105",
                    label: "小店区"
                }, {
                    value: "140106",
                    label: "迎泽区"
                }, {
                    value: "140107",
                    label: "杏花岭区"
                }, {
                    value: "140108",
                    label: "尖草坪区"
                }, {
                    value: "140109",
                    label: "万柏林区"
                }, {
                    value: "140110",
                    label: "晋源区"
                }, {
                    value: "140121",
                    label: "清徐县"
                }, {
                    value: "140122",
                    label: "阳曲县"
                }, {
                    value: "140123",
                    label: "娄烦县"
                }, {
                    value: "140181",
                    label: "古交市"
                } ]
            }, {
                value: "140200",
                label: "大同",
                children: [ {
                    value: "140202",
                    label: "城区"
                }, {
                    value: "140203",
                    label: "矿区"
                }, {
                    value: "140211",
                    label: "南郊区"
                }, {
                    value: "140212",
                    label: "新荣区"
                }, {
                    value: "140221",
                    label: "阳高县"
                }, {
                    value: "140222",
                    label: "天镇县"
                }, {
                    value: "140223",
                    label: "广灵县"
                }, {
                    value: "140224",
                    label: "灵丘县"
                }, {
                    value: "140225",
                    label: "浑源县"
                }, {
                    value: "140226",
                    label: "左云县"
                }, {
                    value: "140227",
                    label: "大同县"
                } ]
            }, {
                value: "140300",
                label: "阳泉",
                children: [ {
                    value: "140302",
                    label: "城区"
                }, {
                    value: "140303",
                    label: "矿区"
                }, {
                    value: "140311",
                    label: "郊区"
                }, {
                    value: "140321",
                    label: "平定县"
                }, {
                    value: "140322",
                    label: "盂县"
                } ]
            }, {
                value: "140400",
                label: "长治",
                children: [ {
                    value: "140402",
                    label: "城区"
                }, {
                    value: "140411",
                    label: "郊区"
                }, {
                    value: "140421",
                    label: "长治县"
                }, {
                    value: "140423",
                    label: "襄垣县"
                }, {
                    value: "140424",
                    label: "屯留县"
                }, {
                    value: "140425",
                    label: "平顺县"
                }, {
                    value: "140426",
                    label: "黎城县"
                }, {
                    value: "140427",
                    label: "壶关县"
                }, {
                    value: "140428",
                    label: "长子县"
                }, {
                    value: "140429",
                    label: "武乡县"
                }, {
                    value: "140430",
                    label: "沁县"
                }, {
                    value: "140431",
                    label: "沁源县"
                }, {
                    value: "140481",
                    label: "潞城市"
                } ]
            }, {
                value: "140500",
                label: "晋城",
                children: [ {
                    value: "140502",
                    label: "城区"
                }, {
                    value: "140521",
                    label: "沁水县"
                }, {
                    value: "140522",
                    label: "阳城县"
                }, {
                    value: "140524",
                    label: "陵川县"
                }, {
                    value: "140525",
                    label: "泽州县"
                }, {
                    value: "140581",
                    label: "高平市"
                } ]
            }, {
                value: "140600",
                label: "朔州",
                children: [ {
                    value: "140602",
                    label: "朔城区"
                }, {
                    value: "140603",
                    label: "平鲁区"
                }, {
                    value: "140621",
                    label: "山阴县"
                }, {
                    value: "140622",
                    label: "应县"
                }, {
                    value: "140623",
                    label: "右玉县"
                }, {
                    value: "140624",
                    label: "怀仁县"
                } ]
            }, {
                value: "140700",
                label: "晋中",
                children: [ {
                    value: "140702",
                    label: "榆次区"
                }, {
                    value: "140721",
                    label: "榆社县"
                }, {
                    value: "140722",
                    label: "左权县"
                }, {
                    value: "140723",
                    label: "和顺县"
                }, {
                    value: "140724",
                    label: "昔阳县"
                }, {
                    value: "140725",
                    label: "寿阳县"
                }, {
                    value: "140726",
                    label: "太谷县"
                }, {
                    value: "140727",
                    label: "祁县"
                }, {
                    value: "140728",
                    label: "平遥县"
                }, {
                    value: "140729",
                    label: "灵石县"
                }, {
                    value: "140781",
                    label: "介休市"
                } ]
            }, {
                value: "140800",
                label: "运城",
                children: [ {
                    value: "140802",
                    label: "盐湖区"
                }, {
                    value: "140821",
                    label: "临猗县"
                }, {
                    value: "140822",
                    label: "万荣县"
                }, {
                    value: "140823",
                    label: "闻喜县"
                }, {
                    value: "140824",
                    label: "稷山县"
                }, {
                    value: "140825",
                    label: "新绛县"
                }, {
                    value: "140826",
                    label: "绛县"
                }, {
                    value: "140827",
                    label: "垣曲县"
                }, {
                    value: "140828",
                    label: "夏县"
                }, {
                    value: "140829",
                    label: "平陆县"
                }, {
                    value: "140830",
                    label: "芮城县"
                }, {
                    value: "140881",
                    label: "永济市"
                }, {
                    value: "140882",
                    label: "河津市"
                } ]
            }, {
                value: "140900",
                label: "忻州",
                children: [ {
                    value: "140902",
                    label: "忻府区"
                }, {
                    value: "140921",
                    label: "定襄县"
                }, {
                    value: "140922",
                    label: "五台县"
                }, {
                    value: "140923",
                    label: "代县"
                }, {
                    value: "140924",
                    label: "繁峙县"
                }, {
                    value: "140925",
                    label: "宁武县"
                }, {
                    value: "140926",
                    label: "静乐县"
                }, {
                    value: "140927",
                    label: "神池县"
                }, {
                    value: "140928",
                    label: "五寨县"
                }, {
                    value: "140929",
                    label: "岢岚县"
                }, {
                    value: "140930",
                    label: "河曲县"
                }, {
                    value: "140931",
                    label: "保德县"
                }, {
                    value: "140932",
                    label: "偏关县"
                }, {
                    value: "140981",
                    label: "原平市"
                } ]
            }, {
                value: "141000",
                label: "临汾",
                children: [ {
                    value: "141002",
                    label: "尧都区"
                }, {
                    value: "141021",
                    label: "曲沃县"
                }, {
                    value: "141022",
                    label: "翼城县"
                }, {
                    value: "141023",
                    label: "襄汾县"
                }, {
                    value: "141024",
                    label: "洪洞县"
                }, {
                    value: "141025",
                    label: "古县"
                }, {
                    value: "141026",
                    label: "安泽县"
                }, {
                    value: "141027",
                    label: "浮山县"
                }, {
                    value: "141028",
                    label: "吉县"
                }, {
                    value: "141029",
                    label: "乡宁县"
                }, {
                    value: "141030",
                    label: "大宁县"
                }, {
                    value: "141031",
                    label: "隰县"
                }, {
                    value: "141032",
                    label: "永和县"
                }, {
                    value: "141033",
                    label: "蒲县"
                }, {
                    value: "141034",
                    label: "汾西县"
                }, {
                    value: "141081",
                    label: "侯马市"
                }, {
                    value: "141082",
                    label: "霍州市"
                } ]
            }, {
                value: "141100",
                label: "吕梁",
                children: [ {
                    value: "141102",
                    label: "离石区"
                }, {
                    value: "141121",
                    label: "文水县"
                }, {
                    value: "141122",
                    label: "交城县"
                }, {
                    value: "141123",
                    label: "兴县"
                }, {
                    value: "141124",
                    label: "临县"
                }, {
                    value: "141125",
                    label: "柳林县"
                }, {
                    value: "141126",
                    label: "石楼县"
                }, {
                    value: "141127",
                    label: "岚县"
                }, {
                    value: "141128",
                    label: "方山县"
                }, {
                    value: "141129",
                    label: "中阳县"
                }, {
                    value: "141130",
                    label: "交口县"
                }, {
                    value: "141181",
                    label: "孝义市"
                }, {
                    value: "141182",
                    label: "汾阳市"
                } ]
            } ]
        }, {
            value: "150000",
            label: "内蒙古",
            children: [ {
                value: "150100",
                label: "呼和浩特",
                children: [ {
                    value: "150102",
                    label: "新城区"
                }, {
                    value: "150103",
                    label: "回民区"
                }, {
                    value: "150104",
                    label: "玉泉区"
                }, {
                    value: "150105",
                    label: "赛罕区"
                }, {
                    value: "150121",
                    label: "土默特左旗"
                }, {
                    value: "150122",
                    label: "托克托县"
                }, {
                    value: "150123",
                    label: "和林格尔县"
                }, {
                    value: "150124",
                    label: "清水河县"
                }, {
                    value: "150125",
                    label: "武川县"
                } ]
            }, {
                value: "150200",
                label: "包头",
                children: [ {
                    value: "150202",
                    label: "东河区"
                }, {
                    value: "150203",
                    label: "昆都仑区"
                }, {
                    value: "150204",
                    label: "青山区"
                }, {
                    value: "150205",
                    label: "石拐区"
                }, {
                    value: "150206",
                    label: "白云鄂博矿区"
                }, {
                    value: "150207",
                    label: "九原区"
                }, {
                    value: "150221",
                    label: "土默特右旗"
                }, {
                    value: "150222",
                    label: "固阳县"
                }, {
                    value: "150223",
                    label: "达尔罕茂明安联合旗"
                } ]
            }, {
                value: "150300",
                label: "乌海",
                children: [ {
                    value: "150302",
                    label: "海勃湾区"
                }, {
                    value: "150303",
                    label: "海南区"
                }, {
                    value: "150304",
                    label: "乌达区"
                } ]
            }, {
                value: "150400",
                label: "赤峰",
                children: [ {
                    value: "150402",
                    label: "红山区"
                }, {
                    value: "150403",
                    label: "元宝山区"
                }, {
                    value: "150404",
                    label: "松山区"
                }, {
                    value: "150421",
                    label: "阿鲁科尔沁旗"
                }, {
                    value: "150422",
                    label: "巴林左旗"
                }, {
                    value: "150423",
                    label: "巴林右旗"
                }, {
                    value: "150424",
                    label: "林西县"
                }, {
                    value: "150425",
                    label: "克什克腾旗"
                }, {
                    value: "150426",
                    label: "翁牛特旗"
                }, {
                    value: "150428",
                    label: "喀喇沁旗"
                }, {
                    value: "150429",
                    label: "宁城县"
                }, {
                    value: "150430",
                    label: "敖汉旗"
                } ]
            }, {
                value: "150500",
                label: "通辽",
                children: [ {
                    value: "150502",
                    label: "科尔沁区"
                }, {
                    value: "150521",
                    label: "科尔沁左翼中旗"
                }, {
                    value: "150522",
                    label: "科尔沁左翼后旗"
                }, {
                    value: "150523",
                    label: "开鲁县"
                }, {
                    value: "150524",
                    label: "库伦旗"
                }, {
                    value: "150525",
                    label: "奈曼旗"
                }, {
                    value: "150526",
                    label: "扎鲁特旗"
                }, {
                    value: "150581",
                    label: "霍林郭勒市"
                } ]
            }, {
                value: "150600",
                label: "鄂尔多斯",
                children: [ {
                    value: "150602",
                    label: "东胜区"
                }, {
                    value: "150603",
                    label: "康巴什区"
                }, {
                    value: "150621",
                    label: "达拉特旗"
                }, {
                    value: "150622",
                    label: "准格尔旗"
                }, {
                    value: "150623",
                    label: "鄂托克前旗"
                }, {
                    value: "150624",
                    label: "鄂托克旗"
                }, {
                    value: "150625",
                    label: "杭锦旗"
                }, {
                    value: "150626",
                    label: "乌审旗"
                }, {
                    value: "150627",
                    label: "伊金霍洛旗"
                } ]
            }, {
                value: "150700",
                label: "呼伦贝尔",
                children: [ {
                    value: "150702",
                    label: "海拉尔区"
                }, {
                    value: "150703",
                    label: "扎赉诺尔区"
                }, {
                    value: "150721",
                    label: "阿荣旗"
                }, {
                    value: "150722",
                    label: "莫力达瓦达斡尔族自治旗"
                }, {
                    value: "150723",
                    label: "鄂伦春自治旗"
                }, {
                    value: "150724",
                    label: "鄂温克族自治旗"
                }, {
                    value: "150725",
                    label: "陈巴尔虎旗"
                }, {
                    value: "150726",
                    label: "新巴尔虎左旗"
                }, {
                    value: "150727",
                    label: "新巴尔虎右旗"
                }, {
                    value: "150781",
                    label: "满洲里市"
                }, {
                    value: "150782",
                    label: "牙克石市"
                }, {
                    value: "150783",
                    label: "扎兰屯市"
                }, {
                    value: "150784",
                    label: "额尔古纳市"
                }, {
                    value: "150785",
                    label: "根河市"
                } ]
            }, {
                value: "150800",
                label: "巴彦淖尔",
                children: [ {
                    value: "150802",
                    label: "临河区"
                }, {
                    value: "150821",
                    label: "五原县"
                }, {
                    value: "150822",
                    label: "磴口县"
                }, {
                    value: "150823",
                    label: "乌拉特前旗"
                }, {
                    value: "150824",
                    label: "乌拉特中旗"
                }, {
                    value: "150825",
                    label: "乌拉特后旗"
                }, {
                    value: "150826",
                    label: "杭锦后旗"
                } ]
            }, {
                value: "150900",
                label: "乌兰察布",
                children: [ {
                    value: "150902",
                    label: "集宁区"
                }, {
                    value: "150921",
                    label: "卓资县"
                }, {
                    value: "150922",
                    label: "化德县"
                }, {
                    value: "150923",
                    label: "商都县"
                }, {
                    value: "150924",
                    label: "兴和县"
                }, {
                    value: "150925",
                    label: "凉城县"
                }, {
                    value: "150926",
                    label: "察哈尔右翼前旗"
                }, {
                    value: "150927",
                    label: "察哈尔右翼中旗"
                }, {
                    value: "150928",
                    label: "察哈尔右翼后旗"
                }, {
                    value: "150929",
                    label: "四子王旗"
                }, {
                    value: "150981",
                    label: "丰镇市"
                } ]
            }, {
                value: "152200",
                label: "兴安盟",
                children: [ {
                    value: "152201",
                    label: "乌兰浩特"
                }, {
                    value: "152202",
                    label: "阿尔山市"
                }, {
                    value: "152221",
                    label: "科尔沁右翼前旗"
                }, {
                    value: "152222",
                    label: "科尔沁右翼中旗"
                }, {
                    value: "152223",
                    label: "扎赉特旗"
                }, {
                    value: "152224",
                    label: "突泉县"
                } ]
            }, {
                value: "152500",
                label: "锡林郭勒",
                children: [ {
                    value: "152501",
                    label: "二连浩特市"
                }, {
                    value: "152502",
                    label: "锡林浩特市"
                }, {
                    value: "152522",
                    label: "阿巴嘎旗"
                }, {
                    value: "152523",
                    label: "苏尼特左旗"
                }, {
                    value: "152524",
                    label: "苏尼特右旗"
                }, {
                    value: "152525",
                    label: "东乌珠穆沁旗"
                }, {
                    value: "152526",
                    label: "西乌珠穆沁旗"
                }, {
                    value: "152527",
                    label: "太仆寺旗"
                }, {
                    value: "152528",
                    label: "镶黄旗"
                }, {
                    value: "152529",
                    label: "正镶白旗"
                }, {
                    value: "152530",
                    label: "正蓝旗"
                }, {
                    value: "152531",
                    label: "多伦县"
                } ]
            }, {
                value: "152900",
                label: "阿拉善",
                children: [ {
                    value: "152921",
                    label: "阿拉善左旗"
                }, {
                    value: "152922",
                    label: "阿拉善右旗"
                }, {
                    value: "152923",
                    label: "额济纳旗"
                } ]
            } ]
        }, {
            value: "210000",
            label: "辽宁",
            children: [ {
                value: "210100",
                label: "沈阳",
                children: [ {
                    value: "210102",
                    label: "和平区"
                }, {
                    value: "210103",
                    label: "沈河区"
                }, {
                    value: "210104",
                    label: "大东区"
                }, {
                    value: "210105",
                    label: "皇姑区"
                }, {
                    value: "210106",
                    label: "铁西区"
                }, {
                    value: "210111",
                    label: "苏家屯区"
                }, {
                    value: "210112",
                    label: "浑南区"
                }, {
                    value: "210113",
                    label: "沈北新区"
                }, {
                    value: "210114",
                    label: "于洪区"
                }, {
                    value: "210115",
                    label: "辽中区"
                }, {
                    value: "210123",
                    label: "康平县"
                }, {
                    value: "210124",
                    label: "法库县"
                }, {
                    value: "210181",
                    label: "新民市"
                } ]
            }, {
                value: "210200",
                label: "大连",
                children: [ {
                    value: "210202",
                    label: "中山区"
                }, {
                    value: "210203",
                    label: "西岗区"
                }, {
                    value: "210204",
                    label: "沙河口区"
                }, {
                    value: "210211",
                    label: "甘井子区"
                }, {
                    value: "210212",
                    label: "旅顺口区"
                }, {
                    value: "210213",
                    label: "金州区"
                }, {
                    value: "210214",
                    label: "普兰店区"
                }, {
                    value: "210224",
                    label: "长海县"
                }, {
                    value: "210281",
                    label: "瓦房店市"
                }, {
                    value: "210283",
                    label: "庄河市"
                } ]
            }, {
                value: "210300",
                label: "鞍山",
                children: [ {
                    value: "210302",
                    label: "铁东区"
                }, {
                    value: "210303",
                    label: "铁西区"
                }, {
                    value: "210304",
                    label: "立山区"
                }, {
                    value: "210311",
                    label: "千山区"
                }, {
                    value: "210321",
                    label: "台安县"
                }, {
                    value: "210323",
                    label: "岫岩满族自治县"
                }, {
                    value: "210381",
                    label: "海城市"
                } ]
            }, {
                value: "210400",
                label: "抚顺",
                children: [ {
                    value: "210402",
                    label: "新抚区"
                }, {
                    value: "210403",
                    label: "东洲区"
                }, {
                    value: "210404",
                    label: "望花区"
                }, {
                    value: "210411",
                    label: "顺城区"
                }, {
                    value: "210421",
                    label: "抚顺县"
                }, {
                    value: "210422",
                    label: "新宾满族自治县"
                }, {
                    value: "210423",
                    label: "清原满族自治县"
                } ]
            }, {
                value: "210500",
                label: "本溪",
                children: [ {
                    value: "210502",
                    label: "平山区"
                }, {
                    value: "210503",
                    label: "溪湖区"
                }, {
                    value: "210504",
                    label: "明山区"
                }, {
                    value: "210505",
                    label: "南芬区"
                }, {
                    value: "210521",
                    label: "本溪满族自治县"
                }, {
                    value: "210522",
                    label: "桓仁满族自治县"
                } ]
            }, {
                value: "210600",
                label: "丹东",
                children: [ {
                    value: "210602",
                    label: "元宝区"
                }, {
                    value: "210603",
                    label: "振兴区"
                }, {
                    value: "210604",
                    label: "振安区"
                }, {
                    value: "210624",
                    label: "宽甸满族自治县"
                }, {
                    value: "210681",
                    label: "东港市"
                }, {
                    value: "210682",
                    label: "凤城市"
                } ]
            }, {
                value: "210700",
                label: "锦州",
                children: [ {
                    value: "210702",
                    label: "古塔区"
                }, {
                    value: "210703",
                    label: "凌河区"
                }, {
                    value: "210711",
                    label: "太和区"
                }, {
                    value: "210726",
                    label: "黑山县"
                }, {
                    value: "210727",
                    label: "义县"
                }, {
                    value: "210781",
                    label: "凌海市"
                }, {
                    value: "210782",
                    label: "北镇市"
                } ]
            }, {
                value: "210800",
                label: "营口",
                children: [ {
                    value: "210802",
                    label: "站前区"
                }, {
                    value: "210803",
                    label: "西市区"
                }, {
                    value: "210804",
                    label: "鲅鱼圈区"
                }, {
                    value: "210811",
                    label: "老边区"
                }, {
                    value: "210881",
                    label: "盖州市"
                }, {
                    value: "210882",
                    label: "大石桥市"
                } ]
            }, {
                value: "210900",
                label: "阜新",
                children: [ {
                    value: "210902",
                    label: "海州区"
                }, {
                    value: "210903",
                    label: "新邱区"
                }, {
                    value: "210904",
                    label: "太平区"
                }, {
                    value: "210905",
                    label: "清河门区"
                }, {
                    value: "210911",
                    label: "细河区"
                }, {
                    value: "210921",
                    label: "阜新蒙古族自治县"
                }, {
                    value: "210922",
                    label: "彰武县"
                } ]
            }, {
                value: "211000",
                label: "辽阳",
                children: [ {
                    value: "211002",
                    label: "白塔区"
                }, {
                    value: "211003",
                    label: "文圣区"
                }, {
                    value: "211004",
                    label: "宏伟区"
                }, {
                    value: "211005",
                    label: "弓长岭区"
                }, {
                    value: "211011",
                    label: "太子河区"
                }, {
                    value: "211021",
                    label: "辽阳县"
                }, {
                    value: "211081",
                    label: "灯塔市"
                } ]
            }, {
                value: "211100",
                label: "盘锦",
                children: [ {
                    value: "211102",
                    label: "双台子区"
                }, {
                    value: "211103",
                    label: "兴隆台区"
                }, {
                    value: "211104",
                    label: "大洼区"
                }, {
                    value: "211122",
                    label: "盘山县"
                } ]
            }, {
                value: "211200",
                label: "铁岭",
                children: [ {
                    value: "211202",
                    label: "银州区"
                }, {
                    value: "211204",
                    label: "清河区"
                }, {
                    value: "211221",
                    label: "铁岭县"
                }, {
                    value: "211223",
                    label: "西丰县"
                }, {
                    value: "211224",
                    label: "昌图县"
                }, {
                    value: "211281",
                    label: "调兵山市"
                }, {
                    value: "211282",
                    label: "开原市"
                } ]
            }, {
                value: "211300",
                label: "朝阳",
                children: [ {
                    value: "211302",
                    label: "双塔区"
                }, {
                    value: "211303",
                    label: "龙城区"
                }, {
                    value: "211321",
                    label: "朝阳县"
                }, {
                    value: "211322",
                    label: "建平县"
                }, {
                    value: "211324",
                    label: "喀喇沁左翼蒙古族自治县"
                }, {
                    value: "211381",
                    label: "北票市"
                }, {
                    value: "211382",
                    label: "凌源市"
                } ]
            }, {
                value: "211400",
                label: "葫芦岛",
                children: [ {
                    value: "211402",
                    label: "连山区"
                }, {
                    value: "211403",
                    label: "龙港区"
                }, {
                    value: "211404",
                    label: "南票区"
                }, {
                    value: "211421",
                    label: "绥中县"
                }, {
                    value: "211422",
                    label: "建昌县"
                }, {
                    value: "211481",
                    label: "兴城市"
                } ]
            } ]
        }, {
            value: "220000",
            label: "吉林",
            children: [ {
                value: "220100",
                label: "长春",
                children: [ {
                    value: "220102",
                    label: "南关区"
                }, {
                    value: "220103",
                    label: "宽城区"
                }, {
                    value: "220104",
                    label: "朝阳区"
                }, {
                    value: "220105",
                    label: "二道区"
                }, {
                    value: "220106",
                    label: "绿园区"
                }, {
                    value: "220112",
                    label: "双阳区"
                }, {
                    value: "220113",
                    label: "九台区"
                }, {
                    value: "220122",
                    label: "农安县"
                }, {
                    value: "220182",
                    label: "榆树市"
                }, {
                    value: "220183",
                    label: "德惠市"
                } ]
            }, {
                value: "220200",
                label: "吉林",
                children: [ {
                    value: "220202",
                    label: "昌邑区"
                }, {
                    value: "220203",
                    label: "龙潭区"
                }, {
                    value: "220204",
                    label: "船营区"
                }, {
                    value: "220211",
                    label: "丰满区"
                }, {
                    value: "220221",
                    label: "永吉县"
                }, {
                    value: "220281",
                    label: "蛟河市"
                }, {
                    value: "220282",
                    label: "桦甸市"
                }, {
                    value: "220283",
                    label: "舒兰市"
                }, {
                    value: "220284",
                    label: "磐石市"
                } ]
            }, {
                value: "220300",
                label: "四平",
                children: [ {
                    value: "220302",
                    label: "铁西区"
                }, {
                    value: "220303",
                    label: "铁东区"
                }, {
                    value: "220322",
                    label: "梨树县"
                }, {
                    value: "220323",
                    label: "伊通满族自治县"
                }, {
                    value: "220381",
                    label: "公主岭市"
                }, {
                    value: "220382",
                    label: "双辽市"
                } ]
            }, {
                value: "220400",
                label: "辽源",
                children: [ {
                    value: "220402",
                    label: "龙山区"
                }, {
                    value: "220403",
                    label: "西安区"
                }, {
                    value: "220421",
                    label: "东丰县"
                }, {
                    value: "220422",
                    label: "东辽县"
                } ]
            }, {
                value: "220500",
                label: "通化",
                children: [ {
                    value: "220502",
                    label: "东昌区"
                }, {
                    value: "220503",
                    label: "二道江区"
                }, {
                    value: "220521",
                    label: "通化县"
                }, {
                    value: "220523",
                    label: "辉南县"
                }, {
                    value: "220524",
                    label: "柳河县"
                }, {
                    value: "220581",
                    label: "梅河口市"
                }, {
                    value: "220582",
                    label: "集安市"
                } ]
            }, {
                value: "220600",
                label: "白山",
                children: [ {
                    value: "220602",
                    label: "浑江区"
                }, {
                    value: "220605",
                    label: "江源区"
                }, {
                    value: "220621",
                    label: "抚松县"
                }, {
                    value: "220622",
                    label: "靖宇县"
                }, {
                    value: "220623",
                    label: "长白朝鲜族自治县"
                }, {
                    value: "220681",
                    label: "临江市"
                } ]
            }, {
                value: "220700",
                label: "松原",
                children: [ {
                    value: "220702",
                    label: "宁江区"
                }, {
                    value: "220721",
                    label: "前郭尔罗斯蒙古族自治县"
                }, {
                    value: "220722",
                    label: "长岭县"
                }, {
                    value: "220723",
                    label: "乾安县"
                }, {
                    value: "220781",
                    label: "扶余市"
                } ]
            }, {
                value: "220800",
                label: "白城",
                children: [ {
                    value: "220802",
                    label: "洮北区"
                }, {
                    value: "220821",
                    label: "镇赉县"
                }, {
                    value: "220822",
                    label: "通榆县"
                }, {
                    value: "220881",
                    label: "洮南市"
                }, {
                    value: "220882",
                    label: "大安市"
                } ]
            }, {
                value: "222400",
                label: "延边朝鲜族",
                children: [ {
                    value: "222401",
                    label: "延吉市"
                }, {
                    value: "222402",
                    label: "图们市"
                }, {
                    value: "222403",
                    label: "敦化市"
                }, {
                    value: "222404",
                    label: "珲春市"
                }, {
                    value: "222405",
                    label: "龙井市"
                }, {
                    value: "222406",
                    label: "和龙市"
                }, {
                    value: "222424",
                    label: "汪清县"
                }, {
                    value: "222426",
                    label: "安图县"
                } ]
            } ]
        }, {
            value: "230000",
            label: "黑龙江",
            children: [ {
                value: "230100",
                label: "哈尔滨",
                children: [ {
                    value: "230102",
                    label: "道里区"
                }, {
                    value: "230103",
                    label: "南岗区"
                }, {
                    value: "230104",
                    label: "道外区"
                }, {
                    value: "230108",
                    label: "平房区"
                }, {
                    value: "230109",
                    label: "松北区"
                }, {
                    value: "230110",
                    label: "香坊区"
                }, {
                    value: "230111",
                    label: "呼兰区"
                }, {
                    value: "230112",
                    label: "阿城区"
                }, {
                    value: "230113",
                    label: "双城区"
                }, {
                    value: "230123",
                    label: "依兰县"
                }, {
                    value: "230124",
                    label: "方正县"
                }, {
                    value: "230125",
                    label: "宾县"
                }, {
                    value: "230126",
                    label: "巴彦县"
                }, {
                    value: "230127",
                    label: "木兰县"
                }, {
                    value: "230128",
                    label: "通河县"
                }, {
                    value: "230129",
                    label: "延寿县"
                }, {
                    value: "230183",
                    label: "尚志市"
                }, {
                    value: "230184",
                    label: "五常市"
                } ]
            }, {
                value: "230200",
                label: "齐齐哈尔",
                children: [ {
                    value: "230202",
                    label: "龙沙区"
                }, {
                    value: "230203",
                    label: "建华区"
                }, {
                    value: "230204",
                    label: "铁锋区"
                }, {
                    value: "230205",
                    label: "昂昂溪区"
                }, {
                    value: "230206",
                    label: "富拉尔基区"
                }, {
                    value: "230207",
                    label: "碾子山区"
                }, {
                    value: "230208",
                    label: "梅里斯达斡尔族区"
                }, {
                    value: "230221",
                    label: "龙江县"
                }, {
                    value: "230223",
                    label: "依安县"
                }, {
                    value: "230224",
                    label: "泰来县"
                }, {
                    value: "230225",
                    label: "甘南县"
                }, {
                    value: "230227",
                    label: "富裕县"
                }, {
                    value: "230229",
                    label: "克山县"
                }, {
                    value: "230230",
                    label: "克东县"
                }, {
                    value: "230231",
                    label: "拜泉县"
                }, {
                    value: "230281",
                    label: "讷河市"
                } ]
            }, {
                value: "230300",
                label: "鸡西",
                children: [ {
                    value: "230302",
                    label: "鸡冠区"
                }, {
                    value: "230303",
                    label: "恒山区"
                }, {
                    value: "230304",
                    label: "滴道区"
                }, {
                    value: "230305",
                    label: "梨树区"
                }, {
                    value: "230306",
                    label: "城子河区"
                }, {
                    value: "230307",
                    label: "麻山区"
                }, {
                    value: "230321",
                    label: "鸡东县"
                }, {
                    value: "230381",
                    label: "虎林市"
                }, {
                    value: "230382",
                    label: "密山市"
                } ]
            }, {
                value: "230400",
                label: "鹤岗",
                children: [ {
                    value: "230402",
                    label: "向阳区"
                }, {
                    value: "230403",
                    label: "工农区"
                }, {
                    value: "230404",
                    label: "南山区"
                }, {
                    value: "230405",
                    label: "兴安区"
                }, {
                    value: "230406",
                    label: "东山区"
                }, {
                    value: "230407",
                    label: "兴山区"
                }, {
                    value: "230421",
                    label: "萝北县"
                }, {
                    value: "230422",
                    label: "绥滨县"
                } ]
            }, {
                value: "230500",
                label: "双鸭山",
                children: [ {
                    value: "230502",
                    label: "尖山区"
                }, {
                    value: "230503",
                    label: "岭东区"
                }, {
                    value: "230505",
                    label: "四方台区"
                }, {
                    value: "230506",
                    label: "宝山区"
                }, {
                    value: "230521",
                    label: "集贤县"
                }, {
                    value: "230522",
                    label: "友谊县"
                }, {
                    value: "230523",
                    label: "宝清县"
                }, {
                    value: "230524",
                    label: "饶河县"
                } ]
            }, {
                value: "230600",
                label: "大庆",
                children: [ {
                    value: "230602",
                    label: "萨尔图区"
                }, {
                    value: "230603",
                    label: "龙凤区"
                }, {
                    value: "230604",
                    label: "让胡路区"
                }, {
                    value: "230605",
                    label: "红岗区"
                }, {
                    value: "230606",
                    label: "大同区"
                }, {
                    value: "230621",
                    label: "肇州县"
                }, {
                    value: "230622",
                    label: "肇源县"
                }, {
                    value: "230623",
                    label: "林甸县"
                }, {
                    value: "230624",
                    label: "杜尔伯特蒙古族自治县"
                } ]
            }, {
                value: "230700",
                label: "伊春",
                children: [ {
                    value: "230702",
                    label: "伊春区"
                }, {
                    value: "230703",
                    label: "南岔区"
                }, {
                    value: "230704",
                    label: "友好区"
                }, {
                    value: "230705",
                    label: "西林区"
                }, {
                    value: "230706",
                    label: "翠峦区"
                }, {
                    value: "230707",
                    label: "新青区"
                }, {
                    value: "230708",
                    label: "美溪区"
                }, {
                    value: "230709",
                    label: "金山屯区"
                }, {
                    value: "230710",
                    label: "五营区"
                }, {
                    value: "230711",
                    label: "乌马河区"
                }, {
                    value: "230712",
                    label: "汤旺河区"
                }, {
                    value: "230713",
                    label: "带岭区"
                }, {
                    value: "230714",
                    label: "乌伊岭区"
                }, {
                    value: "230715",
                    label: "红星区"
                }, {
                    value: "230716",
                    label: "上甘岭区"
                }, {
                    value: "230722",
                    label: "嘉荫县"
                }, {
                    value: "230781",
                    label: "铁力市"
                } ]
            }, {
                value: "230800",
                label: "佳木斯",
                children: [ {
                    value: "230803",
                    label: "向阳区"
                }, {
                    value: "230804",
                    label: "前进区"
                }, {
                    value: "230805",
                    label: "东风区"
                }, {
                    value: "230811",
                    label: "郊区"
                }, {
                    value: "230822",
                    label: "桦南县"
                }, {
                    value: "230826",
                    label: "桦川县"
                }, {
                    value: "230828",
                    label: "汤原县"
                }, {
                    value: "230881",
                    label: "同江市"
                }, {
                    value: "230882",
                    label: "富锦市"
                }, {
                    value: "230883",
                    label: "抚远市"
                } ]
            }, {
                value: "230900",
                label: "七台河",
                children: [ {
                    value: "230902",
                    label: "新兴区"
                }, {
                    value: "230903",
                    label: "桃山区"
                }, {
                    value: "230904",
                    label: "茄子河区"
                }, {
                    value: "230921",
                    label: "勃利县"
                } ]
            }, {
                value: "231000",
                label: "牡丹江",
                children: [ {
                    value: "231002",
                    label: "东安区"
                }, {
                    value: "231003",
                    label: "阳明区"
                }, {
                    value: "231004",
                    label: "爱民区"
                }, {
                    value: "231005",
                    label: "西安区"
                }, {
                    value: "231025",
                    label: "林口县"
                }, {
                    value: "231081",
                    label: "绥芬河市"
                }, {
                    value: "231083",
                    label: "海林市"
                }, {
                    value: "231084",
                    label: "宁安市"
                }, {
                    value: "231085",
                    label: "穆棱市"
                }, {
                    value: "231086",
                    label: "东宁市"
                } ]
            }, {
                value: "231100",
                label: "黑河",
                children: [ {
                    value: "231102",
                    label: "爱辉区"
                }, {
                    value: "231121",
                    label: "嫩江县"
                }, {
                    value: "231123",
                    label: "逊克县"
                }, {
                    value: "231124",
                    label: "孙吴县"
                }, {
                    value: "231181",
                    label: "北安市"
                }, {
                    value: "231182",
                    label: "五大连池市"
                } ]
            }, {
                value: "231200",
                label: "绥化",
                children: [ {
                    value: "231202",
                    label: "北林区"
                }, {
                    value: "231221",
                    label: "望奎县"
                }, {
                    value: "231222",
                    label: "兰西县"
                }, {
                    value: "231223",
                    label: "青冈县"
                }, {
                    value: "231224",
                    label: "庆安县"
                }, {
                    value: "231225",
                    label: "明水县"
                }, {
                    value: "231226",
                    label: "绥棱县"
                }, {
                    value: "231281",
                    label: "安达市"
                }, {
                    value: "231282",
                    label: "肇东市"
                }, {
                    value: "231283",
                    label: "海伦市"
                } ]
            }, {
                value: "232700",
                label: "大兴安岭",
                children: [ {
                    value: "232721",
                    label: "呼玛县"
                }, {
                    value: "232722",
                    label: "塔河县"
                }, {
                    value: "232723",
                    label: "漠河县"
                } ]
            } ]
        }, {
            value: "310000",
            label: "上海",
            children: [ {
                value: "310100",
                label: "上海",
                children: [ {
                    value: "310101",
                    label: "黄浦区"
                }, {
                    value: "310104",
                    label: "徐汇区"
                }, {
                    value: "310105",
                    label: "长宁区"
                }, {
                    value: "310106",
                    label: "静安区"
                }, {
                    value: "310107",
                    label: "普陀区"
                }, {
                    value: "310109",
                    label: "虹口区"
                }, {
                    value: "310110",
                    label: "杨浦区"
                }, {
                    value: "310112",
                    label: "闵行区"
                }, {
                    value: "310113",
                    label: "宝山区"
                }, {
                    value: "310114",
                    label: "嘉定区"
                }, {
                    value: "310115",
                    label: "浦东新区"
                }, {
                    value: "310116",
                    label: "金山区"
                }, {
                    value: "310117",
                    label: "松江区"
                }, {
                    value: "310118",
                    label: "青浦区"
                }, {
                    value: "310120",
                    label: "奉贤区"
                }, {
                    value: "310151",
                    label: "崇明区"
                } ]
            } ]
        }, {
            value: "320000",
            label: "江苏",
            children: [ {
                value: "320100",
                label: "南京",
                children: [ {
                    value: "320102",
                    label: "玄武区"
                }, {
                    value: "320104",
                    label: "秦淮区"
                }, {
                    value: "320105",
                    label: "建邺区"
                }, {
                    value: "320106",
                    label: "鼓楼区"
                }, {
                    value: "320111",
                    label: "浦口区"
                }, {
                    value: "320113",
                    label: "栖霞区"
                }, {
                    value: "320114",
                    label: "雨花台区"
                }, {
                    value: "320115",
                    label: "江宁区"
                }, {
                    value: "320116",
                    label: "六合区"
                }, {
                    value: "320117",
                    label: "溧水区"
                }, {
                    value: "320118",
                    label: "高淳区"
                } ]
            }, {
                value: "320200",
                label: "无锡",
                children: [ {
                    value: "320205",
                    label: "锡山区"
                }, {
                    value: "320206",
                    label: "惠山区"
                }, {
                    value: "320211",
                    label: "滨湖区"
                }, {
                    value: "320213",
                    label: "梁溪区"
                }, {
                    value: "320214",
                    label: "新吴区"
                }, {
                    value: "320281",
                    label: "江阴市"
                }, {
                    value: "320282",
                    label: "宜兴市"
                } ]
            }, {
                value: "320300",
                label: "徐州",
                children: [ {
                    value: "320302",
                    label: "鼓楼区"
                }, {
                    value: "320303",
                    label: "云龙区"
                }, {
                    value: "320305",
                    label: "贾汪区"
                }, {
                    value: "320311",
                    label: "泉山区"
                }, {
                    value: "320312",
                    label: "铜山区"
                }, {
                    value: "320321",
                    label: "丰县"
                }, {
                    value: "320322",
                    label: "沛县"
                }, {
                    value: "320324",
                    label: "睢宁县"
                }, {
                    value: "320381",
                    label: "新沂市"
                }, {
                    value: "320382",
                    label: "邳州市"
                } ]
            }, {
                value: "320400",
                label: "常州",
                children: [ {
                    value: "320402",
                    label: "天宁区"
                }, {
                    value: "320404",
                    label: "钟楼区"
                }, {
                    value: "320411",
                    label: "新北区"
                }, {
                    value: "320412",
                    label: "武进区"
                }, {
                    value: "320413",
                    label: "金坛区"
                }, {
                    value: "320481",
                    label: "溧阳市"
                } ]
            }, {
                value: "320500",
                label: "苏州",
                children: [ {
                    value: "320505",
                    label: "虎丘区"
                }, {
                    value: "320506",
                    label: "吴中区"
                }, {
                    value: "320507",
                    label: "相城区"
                }, {
                    value: "320508",
                    label: "姑苏区"
                }, {
                    value: "320509",
                    label: "吴江区"
                }, {
                    value: "320581",
                    label: "常熟市"
                }, {
                    value: "320582",
                    label: "张家港市"
                }, {
                    value: "320583",
                    label: "昆山市"
                }, {
                    value: "320585",
                    label: "太仓市"
                } ]
            }, {
                value: "320600",
                label: "南通",
                children: [ {
                    value: "320602",
                    label: "崇川区"
                }, {
                    value: "320611",
                    label: "港闸区"
                }, {
                    value: "320612",
                    label: "通州区"
                }, {
                    value: "320621",
                    label: "海安县"
                }, {
                    value: "320623",
                    label: "如东县"
                }, {
                    value: "320681",
                    label: "启东市"
                }, {
                    value: "320682",
                    label: "如皋市"
                }, {
                    value: "320684",
                    label: "海门市"
                } ]
            }, {
                value: "320700",
                label: "连云港",
                children: [ {
                    value: "320703",
                    label: "连云区"
                }, {
                    value: "320706",
                    label: "海州区"
                }, {
                    value: "320707",
                    label: "赣榆区"
                }, {
                    value: "320722",
                    label: "东海县"
                }, {
                    value: "320723",
                    label: "灌云县"
                }, {
                    value: "320724",
                    label: "灌南县"
                } ]
            }, {
                value: "320800",
                label: "淮安",
                children: [ {
                    value: "320803",
                    label: "淮安区"
                }, {
                    value: "320804",
                    label: "淮阴区"
                }, {
                    value: "320812",
                    label: "清江浦区"
                }, {
                    value: "320813",
                    label: "洪泽区"
                }, {
                    value: "320826",
                    label: "涟水县"
                }, {
                    value: "320830",
                    label: "盱眙县"
                }, {
                    value: "320831",
                    label: "金湖县"
                } ]
            }, {
                value: "320900",
                label: "盐城",
                children: [ {
                    value: "320902",
                    label: "亭湖区"
                }, {
                    value: "320903",
                    label: "盐都区"
                }, {
                    value: "320904",
                    label: "大丰区"
                }, {
                    value: "320921",
                    label: "响水县"
                }, {
                    value: "320922",
                    label: "滨海县"
                }, {
                    value: "320923",
                    label: "阜宁县"
                }, {
                    value: "320924",
                    label: "射阳县"
                }, {
                    value: "320925",
                    label: "建湖县"
                }, {
                    value: "320981",
                    label: "东台市"
                } ]
            }, {
                value: "321000",
                label: "扬州",
                children: [ {
                    value: "321002",
                    label: "广陵区"
                }, {
                    value: "321003",
                    label: "邗江区"
                }, {
                    value: "321012",
                    label: "江都区"
                }, {
                    value: "321023",
                    label: "宝应县"
                }, {
                    value: "321081",
                    label: "仪征市"
                }, {
                    value: "321084",
                    label: "高邮市"
                } ]
            }, {
                value: "321100",
                label: "镇江",
                children: [ {
                    value: "321102",
                    label: "京口区"
                }, {
                    value: "321111",
                    label: "润州区"
                }, {
                    value: "321112",
                    label: "丹徒区"
                }, {
                    value: "321181",
                    label: "丹阳市"
                }, {
                    value: "321182",
                    label: "扬中市"
                }, {
                    value: "321183",
                    label: "句容市"
                } ]
            }, {
                value: "321200",
                label: "泰州",
                children: [ {
                    value: "321202",
                    label: "海陵区"
                }, {
                    value: "321203",
                    label: "高港区"
                }, {
                    value: "321204",
                    label: "姜堰区"
                }, {
                    value: "321281",
                    label: "兴化市"
                }, {
                    value: "321282",
                    label: "靖江市"
                }, {
                    value: "321283",
                    label: "泰兴市"
                } ]
            }, {
                value: "321300",
                label: "宿迁",
                children: [ {
                    value: "321302",
                    label: "宿城区"
                }, {
                    value: "321311",
                    label: "宿豫区"
                }, {
                    value: "321322",
                    label: "沭阳县"
                }, {
                    value: "321323",
                    label: "泗阳县"
                }, {
                    value: "321324",
                    label: "泗洪县"
                } ]
            } ]
        }, {
            value: "330000",
            label: "浙江",
            children: [ {
                value: "330100",
                label: "杭州",
                children: [ {
                    value: "330102",
                    label: "上城区"
                }, {
                    value: "330103",
                    label: "下城区"
                }, {
                    value: "330104",
                    label: "江干区"
                }, {
                    value: "330105",
                    label: "拱墅区"
                }, {
                    value: "330106",
                    label: "西湖区"
                }, {
                    value: "330108",
                    label: "滨江区"
                }, {
                    value: "330109",
                    label: "萧山区"
                }, {
                    value: "330110",
                    label: "余杭区"
                }, {
                    value: "330111",
                    label: "富阳区"
                }, {
                    value: "330112",
                    label: "临安区"
                }, {
                    value: "330122",
                    label: "桐庐县"
                }, {
                    value: "330127",
                    label: "淳安县"
                }, {
                    value: "330182",
                    label: "建德市"
                } ]
            }, {
                value: "330200",
                label: "宁波",
                children: [ {
                    value: "330203",
                    label: "海曙区"
                }, {
                    value: "330205",
                    label: "江北区"
                }, {
                    value: "330206",
                    label: "北仑区"
                }, {
                    value: "330211",
                    label: "镇海区"
                }, {
                    value: "330212",
                    label: "鄞州区"
                }, {
                    value: "330213",
                    label: "奉化区"
                }, {
                    value: "330225",
                    label: "象山县"
                }, {
                    value: "330226",
                    label: "宁海县"
                }, {
                    value: "330281",
                    label: "余姚市"
                }, {
                    value: "330282",
                    label: "慈溪市"
                } ]
            }, {
                value: "330300",
                label: "温州",
                children: [ {
                    value: "330302",
                    label: "鹿城区"
                }, {
                    value: "330303",
                    label: "龙湾区"
                }, {
                    value: "330304",
                    label: "瓯海区"
                }, {
                    value: "330305",
                    label: "洞头区"
                }, {
                    value: "330324",
                    label: "永嘉县"
                }, {
                    value: "330326",
                    label: "平阳县"
                }, {
                    value: "330327",
                    label: "苍南县"
                }, {
                    value: "330328",
                    label: "文成县"
                }, {
                    value: "330329",
                    label: "泰顺县"
                }, {
                    value: "330381",
                    label: "瑞安市"
                }, {
                    value: "330382",
                    label: "乐清市"
                } ]
            }, {
                value: "330400",
                label: "嘉兴",
                children: [ {
                    value: "330402",
                    label: "南湖区"
                }, {
                    value: "330411",
                    label: "秀洲区"
                }, {
                    value: "330421",
                    label: "嘉善县"
                }, {
                    value: "330424",
                    label: "海盐县"
                }, {
                    value: "330481",
                    label: "海宁市"
                }, {
                    value: "330482",
                    label: "平湖市"
                }, {
                    value: "330483",
                    label: "桐乡市"
                } ]
            }, {
                value: "330500",
                label: "湖州",
                children: [ {
                    value: "330502",
                    label: "吴兴区"
                }, {
                    value: "330503",
                    label: "南浔区"
                }, {
                    value: "330521",
                    label: "德清县"
                }, {
                    value: "330522",
                    label: "长兴县"
                }, {
                    value: "330523",
                    label: "安吉县"
                } ]
            }, {
                value: "330600",
                label: "绍兴",
                children: [ {
                    value: "330602",
                    label: "越城区"
                }, {
                    value: "330603",
                    label: "柯桥区"
                }, {
                    value: "330604",
                    label: "上虞区"
                }, {
                    value: "330624",
                    label: "新昌县"
                }, {
                    value: "330681",
                    label: "诸暨市"
                }, {
                    value: "330683",
                    label: "嵊州市"
                } ]
            }, {
                value: "330700",
                label: "金华",
                children: [ {
                    value: "330702",
                    label: "婺城区"
                }, {
                    value: "330703",
                    label: "金东区"
                }, {
                    value: "330723",
                    label: "武义县"
                }, {
                    value: "330726",
                    label: "浦江县"
                }, {
                    value: "330727",
                    label: "磐安县"
                }, {
                    value: "330781",
                    label: "兰溪市"
                }, {
                    value: "330782",
                    label: "义乌市"
                }, {
                    value: "330783",
                    label: "东阳市"
                }, {
                    value: "330784",
                    label: "永康市"
                } ]
            }, {
                value: "330800",
                label: "衢州",
                children: [ {
                    value: "330802",
                    label: "柯城区"
                }, {
                    value: "330803",
                    label: "衢江区"
                }, {
                    value: "330822",
                    label: "常山县"
                }, {
                    value: "330824",
                    label: "开化县"
                }, {
                    value: "330825",
                    label: "龙游县"
                }, {
                    value: "330881",
                    label: "江山市"
                } ]
            }, {
                value: "330900",
                label: "舟山",
                children: [ {
                    value: "330902",
                    label: "定海区"
                }, {
                    value: "330903",
                    label: "普陀区"
                }, {
                    value: "330921",
                    label: "岱山县"
                }, {
                    value: "330922",
                    label: "嵊泗县"
                } ]
            }, {
                value: "331000",
                label: "台州",
                children: [ {
                    value: "331002",
                    label: "椒江区"
                }, {
                    value: "331003",
                    label: "黄岩区"
                }, {
                    value: "331004",
                    label: "路桥区"
                }, {
                    value: "331022",
                    label: "三门县"
                }, {
                    value: "331023",
                    label: "天台县"
                }, {
                    value: "331024",
                    label: "仙居县"
                }, {
                    value: "331081",
                    label: "温岭市"
                }, {
                    value: "331082",
                    label: "临海市"
                }, {
                    value: "331083",
                    label: "玉环市"
                } ]
            }, {
                value: "331100",
                label: "丽水",
                children: [ {
                    value: "331102",
                    label: "莲都区"
                }, {
                    value: "331121",
                    label: "青田县"
                }, {
                    value: "331122",
                    label: "缙云县"
                }, {
                    value: "331123",
                    label: "遂昌县"
                }, {
                    value: "331124",
                    label: "松阳县"
                }, {
                    value: "331125",
                    label: "云和县"
                }, {
                    value: "331126",
                    label: "庆元县"
                }, {
                    value: "331127",
                    label: "景宁畲族自治县"
                }, {
                    value: "331181",
                    label: "龙泉市"
                } ]
            } ]
        }, {
            value: "340000",
            label: "安徽",
            children: [ {
                value: "340100",
                label: "合肥",
                children: [ {
                    value: "340102",
                    label: "瑶海区"
                }, {
                    value: "340103",
                    label: "庐阳区"
                }, {
                    value: "340104",
                    label: "蜀山区"
                }, {
                    value: "340111",
                    label: "包河区"
                }, {
                    value: "340121",
                    label: "长丰县"
                }, {
                    value: "340122",
                    label: "肥东县"
                }, {
                    value: "340123",
                    label: "肥西县"
                }, {
                    value: "340124",
                    label: "庐江县"
                }, {
                    value: "340181",
                    label: "巢湖市"
                } ]
            }, {
                value: "340200",
                label: "芜湖",
                children: [ {
                    value: "340202",
                    label: "镜湖区"
                }, {
                    value: "340203",
                    label: "弋江区"
                }, {
                    value: "340207",
                    label: "鸠江区"
                }, {
                    value: "340208",
                    label: "三山区"
                }, {
                    value: "340221",
                    label: "芜湖县"
                }, {
                    value: "340222",
                    label: "繁昌县"
                }, {
                    value: "340223",
                    label: "南陵县"
                }, {
                    value: "340225",
                    label: "无为县"
                } ]
            }, {
                value: "340300",
                label: "蚌埠",
                children: [ {
                    value: "340302",
                    label: "龙子湖区"
                }, {
                    value: "340303",
                    label: "蚌山区"
                }, {
                    value: "340304",
                    label: "禹会区"
                }, {
                    value: "340311",
                    label: "淮上区"
                }, {
                    value: "340321",
                    label: "怀远县"
                }, {
                    value: "340322",
                    label: "五河县"
                }, {
                    value: "340323",
                    label: "固镇县"
                } ]
            }, {
                value: "340400",
                label: "淮南",
                children: [ {
                    value: "340402",
                    label: "大通区"
                }, {
                    value: "340403",
                    label: "田家庵区"
                }, {
                    value: "340404",
                    label: "谢家集区"
                }, {
                    value: "340405",
                    label: "八公山区"
                }, {
                    value: "340406",
                    label: "潘集区"
                }, {
                    value: "340421",
                    label: "凤台县"
                }, {
                    value: "340422",
                    label: "寿县"
                } ]
            }, {
                value: "340500",
                label: "马鞍山",
                children: [ {
                    value: "340503",
                    label: "花山区"
                }, {
                    value: "340504",
                    label: "雨山区"
                }, {
                    value: "340506",
                    label: "博望区"
                }, {
                    value: "340521",
                    label: "当涂县"
                }, {
                    value: "340522",
                    label: "含山县"
                }, {
                    value: "340523",
                    label: "和县"
                } ]
            }, {
                value: "340600",
                label: "淮北",
                children: [ {
                    value: "340602",
                    label: "杜集区"
                }, {
                    value: "340603",
                    label: "相山区"
                }, {
                    value: "340604",
                    label: "烈山区"
                }, {
                    value: "340621",
                    label: "濉溪县"
                } ]
            }, {
                value: "340700",
                label: "铜陵",
                children: [ {
                    value: "340705",
                    label: "铜官区"
                }, {
                    value: "340706",
                    label: "义安区"
                }, {
                    value: "340711",
                    label: "郊区"
                }, {
                    value: "340722",
                    label: "枞阳县"
                } ]
            }, {
                value: "340800",
                label: "安庆",
                children: [ {
                    value: "340802",
                    label: "迎江区"
                }, {
                    value: "340803",
                    label: "大观区"
                }, {
                    value: "340811",
                    label: "宜秀区"
                }, {
                    value: "340822",
                    label: "怀宁县"
                }, {
                    value: "340824",
                    label: "潜山县"
                }, {
                    value: "340825",
                    label: "太湖县"
                }, {
                    value: "340826",
                    label: "宿松县"
                }, {
                    value: "340827",
                    label: "望江县"
                }, {
                    value: "340828",
                    label: "岳西县"
                }, {
                    value: "340881",
                    label: "桐城市"
                } ]
            }, {
                value: "341000",
                label: "黄山",
                children: [ {
                    value: "341002",
                    label: "屯溪区"
                }, {
                    value: "341003",
                    label: "黄山区"
                }, {
                    value: "341004",
                    label: "徽州区"
                }, {
                    value: "341021",
                    label: "歙县"
                }, {
                    value: "341022",
                    label: "休宁县"
                }, {
                    value: "341023",
                    label: "黟县"
                }, {
                    value: "341024",
                    label: "祁门县"
                } ]
            }, {
                value: "341100",
                label: "滁州",
                children: [ {
                    value: "341102",
                    label: "琅琊区"
                }, {
                    value: "341103",
                    label: "南谯区"
                }, {
                    value: "341122",
                    label: "来安县"
                }, {
                    value: "341124",
                    label: "全椒县"
                }, {
                    value: "341125",
                    label: "定远县"
                }, {
                    value: "341126",
                    label: "凤阳县"
                }, {
                    value: "341181",
                    label: "天长市"
                }, {
                    value: "341182",
                    label: "明光市"
                } ]
            }, {
                value: "341200",
                label: "阜阳",
                children: [ {
                    value: "341202",
                    label: "颍州区"
                }, {
                    value: "341203",
                    label: "颍东区"
                }, {
                    value: "341204",
                    label: "颍泉区"
                }, {
                    value: "341221",
                    label: "临泉县"
                }, {
                    value: "341222",
                    label: "太和县"
                }, {
                    value: "341225",
                    label: "阜南县"
                }, {
                    value: "341226",
                    label: "颍上县"
                }, {
                    value: "341282",
                    label: "界首市"
                } ]
            }, {
                value: "341300",
                label: "宿州",
                children: [ {
                    value: "341302",
                    label: "埇桥区"
                }, {
                    value: "341321",
                    label: "砀山县"
                }, {
                    value: "341322",
                    label: "萧县"
                }, {
                    value: "341323",
                    label: "灵璧县"
                }, {
                    value: "341324",
                    label: "泗县"
                } ]
            }, {
                value: "341500",
                label: "六安",
                children: [ {
                    value: "341502",
                    label: "金安区"
                }, {
                    value: "341503",
                    label: "裕安区"
                }, {
                    value: "341504",
                    label: "叶集区"
                }, {
                    value: "341522",
                    label: "霍邱县"
                }, {
                    value: "341523",
                    label: "舒城县"
                }, {
                    value: "341524",
                    label: "金寨县"
                }, {
                    value: "341525",
                    label: "霍山县"
                } ]
            }, {
                value: "341600",
                label: "亳州",
                children: [ {
                    value: "341602",
                    label: "谯城区"
                }, {
                    value: "341621",
                    label: "涡阳县"
                }, {
                    value: "341622",
                    label: "蒙城县"
                }, {
                    value: "341623",
                    label: "利辛县"
                } ]
            }, {
                value: "341700",
                label: "池州",
                children: [ {
                    value: "341702",
                    label: "贵池区"
                }, {
                    value: "341721",
                    label: "东至县"
                }, {
                    value: "341722",
                    label: "石台县"
                }, {
                    value: "341723",
                    label: "青阳县"
                } ]
            }, {
                value: "341800",
                label: "宣城",
                children: [ {
                    value: "341802",
                    label: "宣州区"
                }, {
                    value: "341821",
                    label: "郎溪县"
                }, {
                    value: "341822",
                    label: "广德县"
                }, {
                    value: "341823",
                    label: "泾县"
                }, {
                    value: "341824",
                    label: "绩溪县"
                }, {
                    value: "341825",
                    label: "旌德县"
                }, {
                    value: "341881",
                    label: "宁国市"
                } ]
            } ]
        }, {
            value: "350000",
            label: "福建",
            children: [ {
                value: "350100",
                label: "福州",
                children: [ {
                    value: "350102",
                    label: "鼓楼区"
                }, {
                    value: "350103",
                    label: "台江区"
                }, {
                    value: "350104",
                    label: "仓山区"
                }, {
                    value: "350105",
                    label: "马尾区"
                }, {
                    value: "350111",
                    label: "晋安区"
                }, {
                    value: "350112",
                    label: "长乐区"
                }, {
                    value: "350121",
                    label: "闽侯县"
                }, {
                    value: "350122",
                    label: "连江县"
                }, {
                    value: "350123",
                    label: "罗源县"
                }, {
                    value: "350124",
                    label: "闽清县"
                }, {
                    value: "350125",
                    label: "永泰县"
                }, {
                    value: "350128",
                    label: "平潭县"
                }, {
                    value: "350181",
                    label: "福清市"
                } ]
            }, {
                value: "350200",
                label: "厦门",
                children: [ {
                    value: "350203",
                    label: "思明区"
                }, {
                    value: "350205",
                    label: "海沧区"
                }, {
                    value: "350206",
                    label: "湖里区"
                }, {
                    value: "350211",
                    label: "集美区"
                }, {
                    value: "350212",
                    label: "同安区"
                }, {
                    value: "350213",
                    label: "翔安区"
                } ]
            }, {
                value: "350300",
                label: "莆田",
                children: [ {
                    value: "350302",
                    label: "城厢区"
                }, {
                    value: "350303",
                    label: "涵江区"
                }, {
                    value: "350304",
                    label: "荔城区"
                }, {
                    value: "350305",
                    label: "秀屿区"
                }, {
                    value: "350322",
                    label: "仙游县"
                } ]
            }, {
                value: "350400",
                label: "三明",
                children: [ {
                    value: "350402",
                    label: "梅列区"
                }, {
                    value: "350403",
                    label: "三元区"
                }, {
                    value: "350421",
                    label: "明溪县"
                }, {
                    value: "350423",
                    label: "清流县"
                }, {
                    value: "350424",
                    label: "宁化县"
                }, {
                    value: "350425",
                    label: "大田县"
                }, {
                    value: "350426",
                    label: "尤溪县"
                }, {
                    value: "350427",
                    label: "沙县"
                }, {
                    value: "350428",
                    label: "将乐县"
                }, {
                    value: "350429",
                    label: "泰宁县"
                }, {
                    value: "350430",
                    label: "建宁县"
                }, {
                    value: "350481",
                    label: "永安市"
                } ]
            }, {
                value: "350500",
                label: "泉州",
                children: [ {
                    value: "350502",
                    label: "鲤城区"
                }, {
                    value: "350503",
                    label: "丰泽区"
                }, {
                    value: "350504",
                    label: "洛江区"
                }, {
                    value: "350505",
                    label: "泉港区"
                }, {
                    value: "350521",
                    label: "惠安县"
                }, {
                    value: "350524",
                    label: "安溪县"
                }, {
                    value: "350525",
                    label: "永春县"
                }, {
                    value: "350526",
                    label: "德化县"
                }, {
                    value: "350527",
                    label: "金门县"
                }, {
                    value: "350581",
                    label: "石狮市"
                }, {
                    value: "350582",
                    label: "晋江市"
                }, {
                    value: "350583",
                    label: "南安市"
                } ]
            }, {
                value: "350600",
                label: "漳州",
                children: [ {
                    value: "350602",
                    label: "芗城区"
                }, {
                    value: "350603",
                    label: "龙文区"
                }, {
                    value: "350622",
                    label: "云霄县"
                }, {
                    value: "350623",
                    label: "漳浦县"
                }, {
                    value: "350624",
                    label: "诏安县"
                }, {
                    value: "350625",
                    label: "长泰县"
                }, {
                    value: "350626",
                    label: "东山县"
                }, {
                    value: "350627",
                    label: "南靖县"
                }, {
                    value: "350628",
                    label: "平和县"
                }, {
                    value: "350629",
                    label: "华安县"
                }, {
                    value: "350681",
                    label: "龙海市"
                } ]
            }, {
                value: "350700",
                label: "南平",
                children: [ {
                    value: "350702",
                    label: "延平区"
                }, {
                    value: "350703",
                    label: "建阳区"
                }, {
                    value: "350721",
                    label: "顺昌县"
                }, {
                    value: "350722",
                    label: "浦城县"
                }, {
                    value: "350723",
                    label: "光泽县"
                }, {
                    value: "350724",
                    label: "松溪县"
                }, {
                    value: "350725",
                    label: "政和县"
                }, {
                    value: "350781",
                    label: "邵武市"
                }, {
                    value: "350782",
                    label: "武夷山市"
                }, {
                    value: "350783",
                    label: "建瓯市"
                } ]
            }, {
                value: "350800",
                label: "龙岩",
                children: [ {
                    value: "350802",
                    label: "新罗区"
                }, {
                    value: "350803",
                    label: "永定区"
                }, {
                    value: "350821",
                    label: "长汀县"
                }, {
                    value: "350823",
                    label: "上杭县"
                }, {
                    value: "350824",
                    label: "武平县"
                }, {
                    value: "350825",
                    label: "连城县"
                }, {
                    value: "350881",
                    label: "漳平市"
                } ]
            }, {
                value: "350900",
                label: "宁德",
                children: [ {
                    value: "350902",
                    label: "蕉城区"
                }, {
                    value: "350921",
                    label: "霞浦县"
                }, {
                    value: "350922",
                    label: "古田县"
                }, {
                    value: "350923",
                    label: "屏南县"
                }, {
                    value: "350924",
                    label: "寿宁县"
                }, {
                    value: "350925",
                    label: "周宁县"
                }, {
                    value: "350926",
                    label: "柘荣县"
                }, {
                    value: "350981",
                    label: "福安市"
                }, {
                    value: "350982",
                    label: "福鼎市"
                } ]
            } ]
        }, {
            value: "360000",
            label: "江西",
            children: [ {
                value: "360100",
                label: "南昌",
                children: [ {
                    value: "360102",
                    label: "东湖区"
                }, {
                    value: "360103",
                    label: "西湖区"
                }, {
                    value: "360104",
                    label: "青云谱区"
                }, {
                    value: "360105",
                    label: "湾里区"
                }, {
                    value: "360111",
                    label: "青山湖区"
                }, {
                    value: "360112",
                    label: "新建区"
                }, {
                    value: "360121",
                    label: "南昌县"
                }, {
                    value: "360123",
                    label: "安义县"
                }, {
                    value: "360124",
                    label: "进贤县"
                } ]
            }, {
                value: "360200",
                label: "景德镇",
                children: [ {
                    value: "360202",
                    label: "昌江区"
                }, {
                    value: "360203",
                    label: "珠山区"
                }, {
                    value: "360222",
                    label: "浮梁县"
                }, {
                    value: "360281",
                    label: "乐平市"
                } ]
            }, {
                value: "360300",
                label: "萍乡",
                children: [ {
                    value: "360302",
                    label: "安源区"
                }, {
                    value: "360313",
                    label: "湘东区"
                }, {
                    value: "360321",
                    label: "莲花县"
                }, {
                    value: "360322",
                    label: "上栗县"
                }, {
                    value: "360323",
                    label: "芦溪县"
                } ]
            }, {
                value: "360400",
                label: "九江",
                children: [ {
                    value: "360402",
                    label: "濂溪区"
                }, {
                    value: "360403",
                    label: "浔阳区"
                }, {
                    value: "360404",
                    label: "柴桑区"
                }, {
                    value: "360423",
                    label: "武宁县"
                }, {
                    value: "360424",
                    label: "修水县"
                }, {
                    value: "360425",
                    label: "永修县"
                }, {
                    value: "360426",
                    label: "德安县"
                }, {
                    value: "360428",
                    label: "都昌县"
                }, {
                    value: "360429",
                    label: "湖口县"
                }, {
                    value: "360430",
                    label: "彭泽县"
                }, {
                    value: "360481",
                    label: "瑞昌市"
                }, {
                    value: "360482",
                    label: "共青城市"
                }, {
                    value: "360483",
                    label: "庐山市"
                } ]
            }, {
                value: "360500",
                label: "新余",
                children: [ {
                    value: "360502",
                    label: "渝水区"
                }, {
                    value: "360521",
                    label: "分宜县"
                } ]
            }, {
                value: "360600",
                label: "鹰潭",
                children: [ {
                    value: "360602",
                    label: "月湖区"
                }, {
                    value: "360622",
                    label: "余江县"
                }, {
                    value: "360681",
                    label: "贵溪市"
                } ]
            }, {
                value: "360700",
                label: "赣州",
                children: [ {
                    value: "360702",
                    label: "章贡区"
                }, {
                    value: "360703",
                    label: "南康区"
                }, {
                    value: "360704",
                    label: "赣县区"
                }, {
                    value: "360722",
                    label: "信丰县"
                }, {
                    value: "360723",
                    label: "大余县"
                }, {
                    value: "360724",
                    label: "上犹县"
                }, {
                    value: "360725",
                    label: "崇义县"
                }, {
                    value: "360726",
                    label: "安远县"
                }, {
                    value: "360727",
                    label: "龙南县"
                }, {
                    value: "360728",
                    label: "定南县"
                }, {
                    value: "360729",
                    label: "全南县"
                }, {
                    value: "360730",
                    label: "宁都县"
                }, {
                    value: "360731",
                    label: "于都县"
                }, {
                    value: "360732",
                    label: "兴国县"
                }, {
                    value: "360733",
                    label: "会昌县"
                }, {
                    value: "360734",
                    label: "寻乌县"
                }, {
                    value: "360735",
                    label: "石城县"
                }, {
                    value: "360781",
                    label: "瑞金市"
                } ]
            }, {
                value: "360800",
                label: "吉安",
                children: [ {
                    value: "360802",
                    label: "吉州区"
                }, {
                    value: "360803",
                    label: "青原区"
                }, {
                    value: "360821",
                    label: "吉安县"
                }, {
                    value: "360822",
                    label: "吉水县"
                }, {
                    value: "360823",
                    label: "峡江县"
                }, {
                    value: "360824",
                    label: "新干县"
                }, {
                    value: "360825",
                    label: "永丰县"
                }, {
                    value: "360826",
                    label: "泰和县"
                }, {
                    value: "360827",
                    label: "遂川县"
                }, {
                    value: "360828",
                    label: "万安县"
                }, {
                    value: "360829",
                    label: "安福县"
                }, {
                    value: "360830",
                    label: "永新县"
                }, {
                    value: "360881",
                    label: "井冈山市"
                } ]
            }, {
                value: "360900",
                label: "宜春",
                children: [ {
                    value: "360902",
                    label: "袁州区"
                }, {
                    value: "360921",
                    label: "奉新县"
                }, {
                    value: "360922",
                    label: "万载县"
                }, {
                    value: "360923",
                    label: "上高县"
                }, {
                    value: "360924",
                    label: "宜丰县"
                }, {
                    value: "360925",
                    label: "靖安县"
                }, {
                    value: "360926",
                    label: "铜鼓县"
                }, {
                    value: "360981",
                    label: "丰城市"
                }, {
                    value: "360982",
                    label: "樟树市"
                }, {
                    value: "360983",
                    label: "高安市"
                } ]
            }, {
                value: "361000",
                label: "抚州",
                children: [ {
                    value: "361002",
                    label: "临川区"
                }, {
                    value: "361003",
                    label: "东乡区"
                }, {
                    value: "361021",
                    label: "南城县"
                }, {
                    value: "361022",
                    label: "黎川县"
                }, {
                    value: "361023",
                    label: "南丰县"
                }, {
                    value: "361024",
                    label: "崇仁县"
                }, {
                    value: "361025",
                    label: "乐安县"
                }, {
                    value: "361026",
                    label: "宜黄县"
                }, {
                    value: "361027",
                    label: "金溪县"
                }, {
                    value: "361028",
                    label: "资溪县"
                }, {
                    value: "361030",
                    label: "广昌县"
                } ]
            }, {
                value: "361100",
                label: "上饶",
                children: [ {
                    value: "361102",
                    label: "信州区"
                }, {
                    value: "361103",
                    label: "广丰区"
                }, {
                    value: "361121",
                    label: "上饶县"
                }, {
                    value: "361123",
                    label: "玉山县"
                }, {
                    value: "361124",
                    label: "铅山县"
                }, {
                    value: "361125",
                    label: "横峰县"
                }, {
                    value: "361126",
                    label: "弋阳县"
                }, {
                    value: "361127",
                    label: "余干县"
                }, {
                    value: "361128",
                    label: "鄱阳县"
                }, {
                    value: "361129",
                    label: "万年县"
                }, {
                    value: "361130",
                    label: "婺源县"
                }, {
                    value: "361181",
                    label: "德兴市"
                } ]
            } ]
        }, {
            value: "370000",
            label: "山东",
            children: [ {
                value: "370100",
                label: "济南",
                children: [ {
                    value: "370102",
                    label: "历下区"
                }, {
                    value: "370103",
                    label: "市中区"
                }, {
                    value: "370104",
                    label: "槐荫区"
                }, {
                    value: "370105",
                    label: "天桥区"
                }, {
                    value: "370112",
                    label: "历城区"
                }, {
                    value: "370113",
                    label: "长清区"
                }, {
                    value: "370114",
                    label: "章丘区"
                }, {
                    value: "370124",
                    label: "平阴县"
                }, {
                    value: "370125",
                    label: "济阳县"
                }, {
                    value: "370126",
                    label: "商河县"
                } ]
            }, {
                value: "370200",
                label: "青岛",
                children: [ {
                    value: "370202",
                    label: "市南区"
                }, {
                    value: "370203",
                    label: "市北区"
                }, {
                    value: "370211",
                    label: "黄岛区"
                }, {
                    value: "370212",
                    label: "崂山区"
                }, {
                    value: "370213",
                    label: "李沧区"
                }, {
                    value: "370214",
                    label: "城阳区"
                }, {
                    value: "370281",
                    label: "胶州市"
                }, {
                    value: "370282",
                    label: "即墨区"
                }, {
                    value: "370283",
                    label: "平度市"
                }, {
                    value: "370285",
                    label: "莱西市"
                } ]
            }, {
                value: "370300",
                label: "淄博",
                children: [ {
                    value: "370302",
                    label: "淄川区"
                }, {
                    value: "370303",
                    label: "张店区"
                }, {
                    value: "370304",
                    label: "博山区"
                }, {
                    value: "370305",
                    label: "临淄区"
                }, {
                    value: "370306",
                    label: "周村区"
                }, {
                    value: "370321",
                    label: "桓台县"
                }, {
                    value: "370322",
                    label: "高青县"
                }, {
                    value: "370323",
                    label: "沂源县"
                } ]
            }, {
                value: "370400",
                label: "枣庄",
                children: [ {
                    value: "370402",
                    label: "市中区"
                }, {
                    value: "370403",
                    label: "薛城区"
                }, {
                    value: "370404",
                    label: "峄城区"
                }, {
                    value: "370405",
                    label: "台儿庄区"
                }, {
                    value: "370406",
                    label: "山亭区"
                }, {
                    value: "370481",
                    label: "滕州市"
                } ]
            }, {
                value: "370500",
                label: "东营",
                children: [ {
                    value: "370502",
                    label: "东营区"
                }, {
                    value: "370503",
                    label: "河口区"
                }, {
                    value: "370505",
                    label: "垦利区"
                }, {
                    value: "370522",
                    label: "利津县"
                }, {
                    value: "370523",
                    label: "广饶县"
                } ]
            }, {
                value: "370600",
                label: "烟台",
                children: [ {
                    value: "370602",
                    label: "芝罘区"
                }, {
                    value: "370611",
                    label: "福山区"
                }, {
                    value: "370612",
                    label: "牟平区"
                }, {
                    value: "370613",
                    label: "莱山区"
                }, {
                    value: "370634",
                    label: "长岛县"
                }, {
                    value: "370681",
                    label: "龙口市"
                }, {
                    value: "370682",
                    label: "莱阳市"
                }, {
                    value: "370683",
                    label: "莱州市"
                }, {
                    value: "370684",
                    label: "蓬莱市"
                }, {
                    value: "370685",
                    label: "招远市"
                }, {
                    value: "370686",
                    label: "栖霞市"
                }, {
                    value: "370687",
                    label: "海阳市"
                } ]
            }, {
                value: "370700",
                label: "潍坊",
                children: [ {
                    value: "370702",
                    label: "潍城区"
                }, {
                    value: "370703",
                    label: "寒亭区"
                }, {
                    value: "370704",
                    label: "坊子区"
                }, {
                    value: "370705",
                    label: "奎文区"
                }, {
                    value: "370724",
                    label: "临朐县"
                }, {
                    value: "370725",
                    label: "昌乐县"
                }, {
                    value: "370781",
                    label: "青州市"
                }, {
                    value: "370782",
                    label: "诸城市"
                }, {
                    value: "370783",
                    label: "寿光市"
                }, {
                    value: "370784",
                    label: "安丘市"
                }, {
                    value: "370785",
                    label: "高密市"
                }, {
                    value: "370786",
                    label: "昌邑市"
                } ]
            }, {
                value: "370800",
                label: "济宁",
                children: [ {
                    value: "370811",
                    label: "任城区"
                }, {
                    value: "370812",
                    label: "兖州区"
                }, {
                    value: "370826",
                    label: "微山县"
                }, {
                    value: "370827",
                    label: "鱼台县"
                }, {
                    value: "370828",
                    label: "金乡县"
                }, {
                    value: "370829",
                    label: "嘉祥县"
                }, {
                    value: "370830",
                    label: "汶上县"
                }, {
                    value: "370831",
                    label: "泗水县"
                }, {
                    value: "370832",
                    label: "梁山县"
                }, {
                    value: "370881",
                    label: "曲阜市"
                }, {
                    value: "370883",
                    label: "邹城市"
                } ]
            }, {
                value: "370900",
                label: "泰安",
                children: [ {
                    value: "370902",
                    label: "泰山区"
                }, {
                    value: "370911",
                    label: "岱岳区"
                }, {
                    value: "370921",
                    label: "宁阳县"
                }, {
                    value: "370923",
                    label: "东平县"
                }, {
                    value: "370982",
                    label: "新泰市"
                }, {
                    value: "370983",
                    label: "肥城市"
                } ]
            }, {
                value: "371000",
                label: "威海",
                children: [ {
                    value: "371002",
                    label: "环翠区"
                }, {
                    value: "371003",
                    label: "文登区"
                }, {
                    value: "371082",
                    label: "荣成市"
                }, {
                    value: "371083",
                    label: "乳山市"
                } ]
            }, {
                value: "371100",
                label: "日照",
                children: [ {
                    value: "371102",
                    label: "东港区"
                }, {
                    value: "371103",
                    label: "岚山区"
                }, {
                    value: "371121",
                    label: "五莲县"
                }, {
                    value: "371122",
                    label: "莒县"
                } ]
            }, {
                value: "371200",
                label: "莱芜",
                children: [ {
                    value: "371202",
                    label: "莱城区"
                }, {
                    value: "371203",
                    label: "钢城区"
                } ]
            }, {
                value: "371300",
                label: "临沂",
                children: [ {
                    value: "371302",
                    label: "兰山区"
                }, {
                    value: "371311",
                    label: "罗庄区"
                }, {
                    value: "371312",
                    label: "河东区"
                }, {
                    value: "371321",
                    label: "沂南县"
                }, {
                    value: "371322",
                    label: "郯城县"
                }, {
                    value: "371323",
                    label: "沂水县"
                }, {
                    value: "371324",
                    label: "兰陵县"
                }, {
                    value: "371325",
                    label: "费县"
                }, {
                    value: "371326",
                    label: "平邑县"
                }, {
                    value: "371327",
                    label: "莒南县"
                }, {
                    value: "371328",
                    label: "蒙阴县"
                }, {
                    value: "371329",
                    label: "临沭县"
                } ]
            }, {
                value: "371400",
                label: "德州",
                children: [ {
                    value: "371402",
                    label: "德城区"
                }, {
                    value: "371403",
                    label: "陵城区"
                }, {
                    value: "371422",
                    label: "宁津县"
                }, {
                    value: "371423",
                    label: "庆云县"
                }, {
                    value: "371424",
                    label: "临邑县"
                }, {
                    value: "371425",
                    label: "齐河县"
                }, {
                    value: "371426",
                    label: "平原县"
                }, {
                    value: "371427",
                    label: "夏津县"
                }, {
                    value: "371428",
                    label: "武城县"
                }, {
                    value: "371481",
                    label: "乐陵市"
                }, {
                    value: "371482",
                    label: "禹城市"
                } ]
            }, {
                value: "371500",
                label: "聊城",
                children: [ {
                    value: "371502",
                    label: "东昌府区"
                }, {
                    value: "371521",
                    label: "阳谷县"
                }, {
                    value: "371522",
                    label: "莘县"
                }, {
                    value: "371523",
                    label: "茌平县"
                }, {
                    value: "371524",
                    label: "东阿县"
                }, {
                    value: "371525",
                    label: "冠县"
                }, {
                    value: "371526",
                    label: "高唐县"
                }, {
                    value: "371581",
                    label: "临清市"
                } ]
            }, {
                value: "371600",
                label: "滨州",
                children: [ {
                    value: "371602",
                    label: "滨城区"
                }, {
                    value: "371603",
                    label: "沾化区"
                }, {
                    value: "371621",
                    label: "惠民县"
                }, {
                    value: "371622",
                    label: "阳信县"
                }, {
                    value: "371623",
                    label: "无棣县"
                }, {
                    value: "371625",
                    label: "博兴县"
                }, {
                    value: "371626",
                    label: "邹平县"
                } ]
            }, {
                value: "371700",
                label: "菏泽",
                children: [ {
                    value: "371702",
                    label: "牡丹区"
                }, {
                    value: "371703",
                    label: "定陶区"
                }, {
                    value: "371721",
                    label: "曹县"
                }, {
                    value: "371722",
                    label: "单县"
                }, {
                    value: "371723",
                    label: "成武县"
                }, {
                    value: "371724",
                    label: "巨野县"
                }, {
                    value: "371725",
                    label: "郓城县"
                }, {
                    value: "371726",
                    label: "鄄城县"
                }, {
                    value: "371728",
                    label: "东明县"
                } ]
            } ]
        }, {
            value: "410000",
            label: "河南",
            children: [ {
                value: "410100",
                label: "郑州",
                children: [ {
                    value: "410102",
                    label: "中原区"
                }, {
                    value: "410103",
                    label: "二七区"
                }, {
                    value: "410104",
                    label: "管城回族区"
                }, {
                    value: "410105",
                    label: "金水区"
                }, {
                    value: "410106",
                    label: "上街区"
                }, {
                    value: "410108",
                    label: "惠济区"
                }, {
                    value: "410122",
                    label: "中牟县"
                }, {
                    value: "410181",
                    label: "巩义市"
                }, {
                    value: "410182",
                    label: "荥阳市"
                }, {
                    value: "410183",
                    label: "新密市"
                }, {
                    value: "410184",
                    label: "新郑市"
                }, {
                    value: "410185",
                    label: "登封市"
                } ]
            }, {
                value: "410200",
                label: "开封",
                children: [ {
                    value: "410202",
                    label: "龙亭区"
                }, {
                    value: "410203",
                    label: "顺河回族区"
                }, {
                    value: "410204",
                    label: "鼓楼区"
                }, {
                    value: "410205",
                    label: "禹王台区"
                }, {
                    value: "410212",
                    label: "祥符区"
                }, {
                    value: "410221",
                    label: "杞县"
                }, {
                    value: "410222",
                    label: "通许县"
                }, {
                    value: "410223",
                    label: "尉氏县"
                }, {
                    value: "410225",
                    label: "兰考县"
                } ]
            }, {
                value: "410300",
                label: "洛阳",
                children: [ {
                    value: "410302",
                    label: "老城区"
                }, {
                    value: "410303",
                    label: "西工区"
                }, {
                    value: "410304",
                    label: "瀍河回族区"
                }, {
                    value: "410305",
                    label: "涧西区"
                }, {
                    value: "410306",
                    label: "吉利区"
                }, {
                    value: "410311",
                    label: "洛龙区"
                }, {
                    value: "410322",
                    label: "孟津县"
                }, {
                    value: "410323",
                    label: "新安县"
                }, {
                    value: "410324",
                    label: "栾川县"
                }, {
                    value: "410325",
                    label: "嵩县"
                }, {
                    value: "410326",
                    label: "汝阳县"
                }, {
                    value: "410327",
                    label: "宜阳县"
                }, {
                    value: "410328",
                    label: "洛宁县"
                }, {
                    value: "410329",
                    label: "伊川县"
                }, {
                    value: "410381",
                    label: "偃师市"
                } ]
            }, {
                value: "410400",
                label: "平顶山",
                children: [ {
                    value: "410402",
                    label: "新华区"
                }, {
                    value: "410403",
                    label: "卫东区"
                }, {
                    value: "410404",
                    label: "石龙区"
                }, {
                    value: "410411",
                    label: "湛河区"
                }, {
                    value: "410421",
                    label: "宝丰县"
                }, {
                    value: "410422",
                    label: "叶县"
                }, {
                    value: "410423",
                    label: "鲁山县"
                }, {
                    value: "410425",
                    label: "郏县"
                }, {
                    value: "410481",
                    label: "舞钢市"
                }, {
                    value: "410482",
                    label: "汝州市"
                } ]
            }, {
                value: "410500",
                label: "安阳",
                children: [ {
                    value: "410502",
                    label: "文峰区"
                }, {
                    value: "410503",
                    label: "北关区"
                }, {
                    value: "410505",
                    label: "殷都区"
                }, {
                    value: "410506",
                    label: "龙安区"
                }, {
                    value: "410522",
                    label: "安阳县"
                }, {
                    value: "410523",
                    label: "汤阴县"
                }, {
                    value: "410526",
                    label: "滑县"
                }, {
                    value: "410527",
                    label: "内黄县"
                }, {
                    value: "410581",
                    label: "林州市"
                } ]
            }, {
                value: "410600",
                label: "鹤壁",
                children: [ {
                    value: "410602",
                    label: "鹤山区"
                }, {
                    value: "410603",
                    label: "山城区"
                }, {
                    value: "410611",
                    label: "淇滨区"
                }, {
                    value: "410621",
                    label: "浚县"
                }, {
                    value: "410622",
                    label: "淇县"
                } ]
            }, {
                value: "410700",
                label: "新乡",
                children: [ {
                    value: "410702",
                    label: "红旗区"
                }, {
                    value: "410703",
                    label: "卫滨区"
                }, {
                    value: "410704",
                    label: "凤泉区"
                }, {
                    value: "410711",
                    label: "牧野区"
                }, {
                    value: "410721",
                    label: "新乡县"
                }, {
                    value: "410724",
                    label: "获嘉县"
                }, {
                    value: "410725",
                    label: "原阳县"
                }, {
                    value: "410726",
                    label: "延津县"
                }, {
                    value: "410727",
                    label: "封丘县"
                }, {
                    value: "410728",
                    label: "长垣县"
                }, {
                    value: "410781",
                    label: "卫辉市"
                }, {
                    value: "410782",
                    label: "辉县市"
                } ]
            }, {
                value: "410800",
                label: "焦作",
                children: [ {
                    value: "410802",
                    label: "解放区"
                }, {
                    value: "410803",
                    label: "中站区"
                }, {
                    value: "410804",
                    label: "马村区"
                }, {
                    value: "410811",
                    label: "山阳区"
                }, {
                    value: "410821",
                    label: "修武县"
                }, {
                    value: "410822",
                    label: "博爱县"
                }, {
                    value: "410823",
                    label: "武陟县"
                }, {
                    value: "410825",
                    label: "温县"
                }, {
                    value: "410882",
                    label: "沁阳市"
                }, {
                    value: "410883",
                    label: "孟州市"
                } ]
            }, {
                value: "410900",
                label: "濮阳",
                children: [ {
                    value: "410902",
                    label: "华龙区"
                }, {
                    value: "410922",
                    label: "清丰县"
                }, {
                    value: "410923",
                    label: "南乐县"
                }, {
                    value: "410926",
                    label: "范县"
                }, {
                    value: "410927",
                    label: "台前县"
                }, {
                    value: "410928",
                    label: "濮阳县"
                } ]
            }, {
                value: "411000",
                label: "许昌",
                children: [ {
                    value: "411002",
                    label: "魏都区"
                }, {
                    value: "411003",
                    label: "建安区"
                }, {
                    value: "411024",
                    label: "鄢陵县"
                }, {
                    value: "411025",
                    label: "襄城县"
                }, {
                    value: "411081",
                    label: "禹州市"
                }, {
                    value: "411082",
                    label: "长葛市"
                } ]
            }, {
                value: "411100",
                label: "漯河",
                children: [ {
                    value: "411102",
                    label: "源汇区"
                }, {
                    value: "411103",
                    label: "郾城区"
                }, {
                    value: "411104",
                    label: "召陵区"
                }, {
                    value: "411121",
                    label: "舞阳县"
                }, {
                    value: "411122",
                    label: "临颍县"
                } ]
            }, {
                value: "411200",
                label: "三门峡",
                children: [ {
                    value: "411202",
                    label: "湖滨区"
                }, {
                    value: "411203",
                    label: "陕州区"
                }, {
                    value: "411221",
                    label: "渑池县"
                }, {
                    value: "411224",
                    label: "卢氏县"
                }, {
                    value: "411281",
                    label: "义马市"
                }, {
                    value: "411282",
                    label: "灵宝市"
                } ]
            }, {
                value: "411300",
                label: "南阳",
                children: [ {
                    value: "411302",
                    label: "宛城区"
                }, {
                    value: "411303",
                    label: "卧龙区"
                }, {
                    value: "411321",
                    label: "南召县"
                }, {
                    value: "411322",
                    label: "方城县"
                }, {
                    value: "411323",
                    label: "西峡县"
                }, {
                    value: "411324",
                    label: "镇平县"
                }, {
                    value: "411325",
                    label: "内乡县"
                }, {
                    value: "411326",
                    label: "淅川县"
                }, {
                    value: "411327",
                    label: "社旗县"
                }, {
                    value: "411328",
                    label: "唐河县"
                }, {
                    value: "411329",
                    label: "新野县"
                }, {
                    value: "411330",
                    label: "桐柏县"
                }, {
                    value: "411381",
                    label: "邓州市"
                } ]
            }, {
                value: "411400",
                label: "商丘",
                children: [ {
                    value: "411402",
                    label: "梁园区"
                }, {
                    value: "411403",
                    label: "睢阳区"
                }, {
                    value: "411421",
                    label: "民权县"
                }, {
                    value: "411422",
                    label: "睢县"
                }, {
                    value: "411423",
                    label: "宁陵县"
                }, {
                    value: "411424",
                    label: "柘城县"
                }, {
                    value: "411425",
                    label: "虞城县"
                }, {
                    value: "411426",
                    label: "夏邑县"
                }, {
                    value: "411481",
                    label: "永城市"
                } ]
            }, {
                value: "411500",
                label: "信阳",
                children: [ {
                    value: "411502",
                    label: "浉河区"
                }, {
                    value: "411503",
                    label: "平桥区"
                }, {
                    value: "411521",
                    label: "罗山县"
                }, {
                    value: "411522",
                    label: "光山县"
                }, {
                    value: "411523",
                    label: "新县"
                }, {
                    value: "411524",
                    label: "商城县"
                }, {
                    value: "411525",
                    label: "固始县"
                }, {
                    value: "411526",
                    label: "潢川县"
                }, {
                    value: "411527",
                    label: "淮滨县"
                }, {
                    value: "411528",
                    label: "息县"
                } ]
            }, {
                value: "411600",
                label: "周口",
                children: [ {
                    value: "411602",
                    label: "川汇区"
                }, {
                    value: "411621",
                    label: "扶沟县"
                }, {
                    value: "411622",
                    label: "西华县"
                }, {
                    value: "411623",
                    label: "商水县"
                }, {
                    value: "411624",
                    label: "沈丘县"
                }, {
                    value: "411625",
                    label: "郸城县"
                }, {
                    value: "411626",
                    label: "淮阳县"
                }, {
                    value: "411627",
                    label: "太康县"
                }, {
                    value: "411628",
                    label: "鹿邑县"
                }, {
                    value: "411681",
                    label: "项城市"
                } ]
            }, {
                value: "411700",
                label: "驻马店",
                children: [ {
                    value: "411702",
                    label: "驿城区"
                }, {
                    value: "411721",
                    label: "西平县"
                }, {
                    value: "411722",
                    label: "上蔡县"
                }, {
                    value: "411723",
                    label: "平舆县"
                }, {
                    value: "411724",
                    label: "正阳县"
                }, {
                    value: "411725",
                    label: "确山县"
                }, {
                    value: "411726",
                    label: "泌阳县"
                }, {
                    value: "411727",
                    label: "汝南县"
                }, {
                    value: "411728",
                    label: "遂平县"
                }, {
                    value: "411729",
                    label: "新蔡县"
                } ]
            }, {
                value: "419001",
                label: "济源",
                children: [ {
                    value: "419001",
                    label: "济源市"
                } ]
            } ]
        }, {
            value: "420000",
            label: "湖北",
            children: [ {
                value: "420100",
                label: "武汉",
                children: [ {
                    value: "420102",
                    label: "江岸区"
                }, {
                    value: "420103",
                    label: "江汉区"
                }, {
                    value: "420104",
                    label: "硚口区"
                }, {
                    value: "420105",
                    label: "汉阳区"
                }, {
                    value: "420106",
                    label: "武昌区"
                }, {
                    value: "420107",
                    label: "青山区"
                }, {
                    value: "420111",
                    label: "洪山区"
                }, {
                    value: "420112",
                    label: "东西湖区"
                }, {
                    value: "420113",
                    label: "汉南区"
                }, {
                    value: "420114",
                    label: "蔡甸区"
                }, {
                    value: "420115",
                    label: "江夏区"
                }, {
                    value: "420116",
                    label: "黄陂区"
                }, {
                    value: "420117",
                    label: "新洲区"
                } ]
            }, {
                value: "420200",
                label: "黄石",
                children: [ {
                    value: "420202",
                    label: "黄石港区"
                }, {
                    value: "420203",
                    label: "西塞山区"
                }, {
                    value: "420204",
                    label: "下陆区"
                }, {
                    value: "420205",
                    label: "铁山区"
                }, {
                    value: "420222",
                    label: "阳新县"
                }, {
                    value: "420281",
                    label: "大冶市"
                } ]
            }, {
                value: "420300",
                label: "十堰",
                children: [ {
                    value: "420302",
                    label: "茅箭区"
                }, {
                    value: "420303",
                    label: "张湾区"
                }, {
                    value: "420304",
                    label: "郧阳区"
                }, {
                    value: "420322",
                    label: "郧西县"
                }, {
                    value: "420323",
                    label: "竹山县"
                }, {
                    value: "420324",
                    label: "竹溪县"
                }, {
                    value: "420325",
                    label: "房县"
                }, {
                    value: "420381",
                    label: "丹江口市"
                } ]
            }, {
                value: "420500",
                label: "宜昌",
                children: [ {
                    value: "420502",
                    label: "西陵区"
                }, {
                    value: "420503",
                    label: "伍家岗区"
                }, {
                    value: "420504",
                    label: "点军区"
                }, {
                    value: "420505",
                    label: "猇亭区"
                }, {
                    value: "420506",
                    label: "夷陵区"
                }, {
                    value: "420525",
                    label: "远安县"
                }, {
                    value: "420526",
                    label: "兴山县"
                }, {
                    value: "420527",
                    label: "秭归县"
                }, {
                    value: "420528",
                    label: "长阳土家族自治县"
                }, {
                    value: "420529",
                    label: "五峰土家族自治县"
                }, {
                    value: "420581",
                    label: "宜都市"
                }, {
                    value: "420582",
                    label: "当阳市"
                }, {
                    value: "420583",
                    label: "枝江市"
                } ]
            }, {
                value: "420600",
                label: "襄阳",
                children: [ {
                    value: "420602",
                    label: "襄城区"
                }, {
                    value: "420606",
                    label: "樊城区"
                }, {
                    value: "420607",
                    label: "襄州区"
                }, {
                    value: "420624",
                    label: "南漳县"
                }, {
                    value: "420625",
                    label: "谷城县"
                }, {
                    value: "420626",
                    label: "保康县"
                }, {
                    value: "420682",
                    label: "老河口市"
                }, {
                    value: "420683",
                    label: "枣阳市"
                }, {
                    value: "420684",
                    label: "宜城市"
                } ]
            }, {
                value: "420700",
                label: "鄂州",
                children: [ {
                    value: "420702",
                    label: "梁子湖区"
                }, {
                    value: "420703",
                    label: "华容区"
                }, {
                    value: "420704",
                    label: "鄂城区"
                } ]
            }, {
                value: "420800",
                label: "荆门",
                children: [ {
                    value: "420802",
                    label: "东宝区"
                }, {
                    value: "420804",
                    label: "掇刀区"
                }, {
                    value: "420821",
                    label: "京山县"
                }, {
                    value: "420822",
                    label: "沙洋县"
                }, {
                    value: "420881",
                    label: "钟祥市"
                } ]
            }, {
                value: "420900",
                label: "孝感",
                children: [ {
                    value: "420902",
                    label: "孝南区"
                }, {
                    value: "420921",
                    label: "孝昌县"
                }, {
                    value: "420922",
                    label: "大悟县"
                }, {
                    value: "420923",
                    label: "云梦县"
                }, {
                    value: "420981",
                    label: "应城市"
                }, {
                    value: "420982",
                    label: "安陆市"
                }, {
                    value: "420984",
                    label: "汉川市"
                } ]
            }, {
                value: "421000",
                label: "荆州",
                children: [ {
                    value: "421002",
                    label: "沙市区"
                }, {
                    value: "421003",
                    label: "荆州区"
                }, {
                    value: "421022",
                    label: "公安县"
                }, {
                    value: "421023",
                    label: "监利县"
                }, {
                    value: "421024",
                    label: "江陵县"
                }, {
                    value: "421081",
                    label: "石首市"
                }, {
                    value: "421083",
                    label: "洪湖市"
                }, {
                    value: "421087",
                    label: "松滋市"
                } ]
            }, {
                value: "421100",
                label: "黄冈",
                children: [ {
                    value: "421102",
                    label: "黄州区"
                }, {
                    value: "421121",
                    label: "团风县"
                }, {
                    value: "421122",
                    label: "红安县"
                }, {
                    value: "421123",
                    label: "罗田县"
                }, {
                    value: "421124",
                    label: "英山县"
                }, {
                    value: "421125",
                    label: "浠水县"
                }, {
                    value: "421126",
                    label: "蕲春县"
                }, {
                    value: "421127",
                    label: "黄梅县"
                }, {
                    value: "421181",
                    label: "麻城市"
                }, {
                    value: "421182",
                    label: "武穴市"
                } ]
            }, {
                value: "421200",
                label: "咸宁",
                children: [ {
                    value: "421202",
                    label: "咸安区"
                }, {
                    value: "421221",
                    label: "嘉鱼县"
                }, {
                    value: "421222",
                    label: "通城县"
                }, {
                    value: "421223",
                    label: "崇阳县"
                }, {
                    value: "421224",
                    label: "通山县"
                }, {
                    value: "421281",
                    label: "赤壁市"
                } ]
            }, {
                value: "421300",
                label: "随州",
                children: [ {
                    value: "421303",
                    label: "曾都区"
                }, {
                    value: "421321",
                    label: "随县"
                }, {
                    value: "421381",
                    label: "广水市"
                } ]
            }, {
                value: "422800",
                label: "恩施",
                children: [ {
                    value: "422801",
                    label: "恩施市"
                }, {
                    value: "422802",
                    label: "利川市"
                }, {
                    value: "422822",
                    label: "建始县"
                }, {
                    value: "422823",
                    label: "巴东县"
                }, {
                    value: "422825",
                    label: "宣恩县"
                }, {
                    value: "422826",
                    label: "咸丰县"
                }, {
                    value: "422827",
                    label: "来凤县"
                }, {
                    value: "422828",
                    label: "鹤峰县"
                } ]
            }, {
                value: "429004",
                label: "仙桃",
                children: [ {
                    value: "429004",
                    label: "仙桃市"
                } ]
            }, {
                value: "429005",
                label: "潜江",
                children: [ {
                    value: "429005",
                    label: "潜江市"
                } ]
            }, {
                value: "429006",
                label: "天门",
                children: [ {
                    value: "429006",
                    label: "天门市"
                } ]
            }, {
                value: "429021",
                label: "神农架",
                children: [ {
                    value: "429021",
                    label: "神农架林区"
                } ]
            } ]
        }, {
            value: "430000",
            label: "湖南",
            children: [ {
                value: "430100",
                label: "长沙",
                children: [ {
                    value: "430102",
                    label: "芙蓉区"
                }, {
                    value: "430103",
                    label: "天心区"
                }, {
                    value: "430104",
                    label: "岳麓区"
                }, {
                    value: "430105",
                    label: "开福区"
                }, {
                    value: "430111",
                    label: "雨花区"
                }, {
                    value: "430112",
                    label: "望城区"
                }, {
                    value: "430121",
                    label: "长沙县"
                }, {
                    value: "430124",
                    label: "宁乡市"
                }, {
                    value: "430181",
                    label: "浏阳市"
                } ]
            }, {
                value: "430200",
                label: "株洲",
                children: [ {
                    value: "430202",
                    label: "荷塘区"
                }, {
                    value: "430203",
                    label: "芦淞区"
                }, {
                    value: "430204",
                    label: "石峰区"
                }, {
                    value: "430211",
                    label: "天元区"
                }, {
                    value: "430221",
                    label: "株洲县"
                }, {
                    value: "430223",
                    label: "攸县"
                }, {
                    value: "430224",
                    label: "茶陵县"
                }, {
                    value: "430225",
                    label: "炎陵县"
                }, {
                    value: "430281",
                    label: "醴陵市"
                } ]
            }, {
                value: "430300",
                label: "湘潭",
                children: [ {
                    value: "430302",
                    label: "雨湖区"
                }, {
                    value: "430304",
                    label: "岳塘区"
                }, {
                    value: "430321",
                    label: "湘潭县"
                }, {
                    value: "430381",
                    label: "湘乡市"
                }, {
                    value: "430382",
                    label: "韶山市"
                } ]
            }, {
                value: "430400",
                label: "衡阳",
                children: [ {
                    value: "430405",
                    label: "珠晖区"
                }, {
                    value: "430406",
                    label: "雁峰区"
                }, {
                    value: "430407",
                    label: "石鼓区"
                }, {
                    value: "430408",
                    label: "蒸湘区"
                }, {
                    value: "430412",
                    label: "南岳区"
                }, {
                    value: "430421",
                    label: "衡阳县"
                }, {
                    value: "430422",
                    label: "衡南县"
                }, {
                    value: "430423",
                    label: "衡山县"
                }, {
                    value: "430424",
                    label: "衡东县"
                }, {
                    value: "430426",
                    label: "祁东县"
                }, {
                    value: "430481",
                    label: "耒阳市"
                }, {
                    value: "430482",
                    label: "常宁市"
                } ]
            }, {
                value: "430500",
                label: "邵阳",
                children: [ {
                    value: "430502",
                    label: "双清区"
                }, {
                    value: "430503",
                    label: "大祥区"
                }, {
                    value: "430511",
                    label: "北塔区"
                }, {
                    value: "430521",
                    label: "邵东县"
                }, {
                    value: "430522",
                    label: "新邵县"
                }, {
                    value: "430523",
                    label: "邵阳县"
                }, {
                    value: "430524",
                    label: "隆回县"
                }, {
                    value: "430525",
                    label: "洞口县"
                }, {
                    value: "430527",
                    label: "绥宁县"
                }, {
                    value: "430528",
                    label: "新宁县"
                }, {
                    value: "430529",
                    label: "城步苗族自治县"
                }, {
                    value: "430581",
                    label: "武冈市"
                } ]
            }, {
                value: "430600",
                label: "岳阳",
                children: [ {
                    value: "430602",
                    label: "岳阳楼区"
                }, {
                    value: "430603",
                    label: "云溪区"
                }, {
                    value: "430611",
                    label: "君山区"
                }, {
                    value: "430621",
                    label: "岳阳县"
                }, {
                    value: "430623",
                    label: "华容县"
                }, {
                    value: "430624",
                    label: "湘阴县"
                }, {
                    value: "430626",
                    label: "平江县"
                }, {
                    value: "430681",
                    label: "汨罗市"
                }, {
                    value: "430682",
                    label: "临湘市"
                } ]
            }, {
                value: "430700",
                label: "常德",
                children: [ {
                    value: "430702",
                    label: "武陵区"
                }, {
                    value: "430703",
                    label: "鼎城区"
                }, {
                    value: "430721",
                    label: "安乡县"
                }, {
                    value: "430722",
                    label: "汉寿县"
                }, {
                    value: "430723",
                    label: "澧县"
                }, {
                    value: "430724",
                    label: "临澧县"
                }, {
                    value: "430725",
                    label: "桃源县"
                }, {
                    value: "430726",
                    label: "石门县"
                }, {
                    value: "430781",
                    label: "津市市"
                } ]
            }, {
                value: "430800",
                label: "张家界",
                children: [ {
                    value: "430802",
                    label: "永定区"
                }, {
                    value: "430811",
                    label: "武陵源区"
                }, {
                    value: "430821",
                    label: "慈利县"
                }, {
                    value: "430822",
                    label: "桑植县"
                } ]
            }, {
                value: "430900",
                label: "益阳",
                children: [ {
                    value: "430902",
                    label: "资阳区"
                }, {
                    value: "430903",
                    label: "赫山区"
                }, {
                    value: "430921",
                    label: "南县"
                }, {
                    value: "430922",
                    label: "桃江县"
                }, {
                    value: "430923",
                    label: "安化县"
                }, {
                    value: "430981",
                    label: "沅江市"
                } ]
            }, {
                value: "431000",
                label: "郴州",
                children: [ {
                    value: "431002",
                    label: "北湖区"
                }, {
                    value: "431003",
                    label: "苏仙区"
                }, {
                    value: "431021",
                    label: "桂阳县"
                }, {
                    value: "431022",
                    label: "宜章县"
                }, {
                    value: "431023",
                    label: "永兴县"
                }, {
                    value: "431024",
                    label: "嘉禾县"
                }, {
                    value: "431025",
                    label: "临武县"
                }, {
                    value: "431026",
                    label: "汝城县"
                }, {
                    value: "431027",
                    label: "桂东县"
                }, {
                    value: "431028",
                    label: "安仁县"
                }, {
                    value: "431081",
                    label: "资兴市"
                } ]
            }, {
                value: "431100",
                label: "永州",
                children: [ {
                    value: "431102",
                    label: "零陵区"
                }, {
                    value: "431103",
                    label: "冷水滩区"
                }, {
                    value: "431121",
                    label: "祁阳县"
                }, {
                    value: "431122",
                    label: "东安县"
                }, {
                    value: "431123",
                    label: "双牌县"
                }, {
                    value: "431124",
                    label: "道县"
                }, {
                    value: "431125",
                    label: "江永县"
                }, {
                    value: "431126",
                    label: "宁远县"
                }, {
                    value: "431127",
                    label: "蓝山县"
                }, {
                    value: "431128",
                    label: "新田县"
                }, {
                    value: "431129",
                    label: "江华瑶族自治县"
                } ]
            }, {
                value: "431200",
                label: "怀化",
                children: [ {
                    value: "431202",
                    label: "鹤城区"
                }, {
                    value: "431221",
                    label: "中方县"
                }, {
                    value: "431222",
                    label: "沅陵县"
                }, {
                    value: "431223",
                    label: "辰溪县"
                }, {
                    value: "431224",
                    label: "溆浦县"
                }, {
                    value: "431225",
                    label: "会同县"
                }, {
                    value: "431226",
                    label: "麻阳苗族自治县"
                }, {
                    value: "431227",
                    label: "新晃侗族自治县"
                }, {
                    value: "431228",
                    label: "芷江侗族自治县"
                }, {
                    value: "431229",
                    label: "靖州苗族侗族自治县"
                }, {
                    value: "431230",
                    label: "通道侗族自治县"
                }, {
                    value: "431281",
                    label: "洪江市"
                } ]
            }, {
                value: "431300",
                label: "娄底",
                children: [ {
                    value: "431302",
                    label: "娄星区"
                }, {
                    value: "431321",
                    label: "双峰县"
                }, {
                    value: "431322",
                    label: "新化县"
                }, {
                    value: "431381",
                    label: "冷水江市"
                }, {
                    value: "431382",
                    label: "涟源市"
                } ]
            }, {
                value: "433100",
                label: "湘西",
                children: [ {
                    value: "433101",
                    label: "吉首市"
                }, {
                    value: "433122",
                    label: "泸溪县"
                }, {
                    value: "433123",
                    label: "凤凰县"
                }, {
                    value: "433124",
                    label: "花垣县"
                }, {
                    value: "433125",
                    label: "保靖县"
                }, {
                    value: "433126",
                    label: "古丈县"
                }, {
                    value: "433127",
                    label: "永顺县"
                }, {
                    value: "433130",
                    label: "龙山县"
                } ]
            } ]
        }, {
            value: "440000",
            label: "广东",
            children: [ {
                value: "440100",
                label: "广州",
                children: [ {
                    value: "440103",
                    label: "荔湾区"
                }, {
                    value: "440104",
                    label: "越秀区"
                }, {
                    value: "440105",
                    label: "海珠区"
                }, {
                    value: "440106",
                    label: "天河区"
                }, {
                    value: "440111",
                    label: "白云区"
                }, {
                    value: "440112",
                    label: "黄埔区"
                }, {
                    value: "440113",
                    label: "番禺区"
                }, {
                    value: "440114",
                    label: "花都区"
                }, {
                    value: "440115",
                    label: "南沙区"
                }, {
                    value: "440117",
                    label: "从化区"
                }, {
                    value: "440118",
                    label: "增城区"
                } ]
            }, {
                value: "440200",
                label: "韶关",
                children: [ {
                    value: "440203",
                    label: "武江区"
                }, {
                    value: "440204",
                    label: "浈江区"
                }, {
                    value: "440205",
                    label: "曲江区"
                }, {
                    value: "440222",
                    label: "始兴县"
                }, {
                    value: "440224",
                    label: "仁化县"
                }, {
                    value: "440229",
                    label: "翁源县"
                }, {
                    value: "440232",
                    label: "乳源瑶族自治县"
                }, {
                    value: "440233",
                    label: "新丰县"
                }, {
                    value: "440281",
                    label: "乐昌市"
                }, {
                    value: "440282",
                    label: "南雄市"
                } ]
            }, {
                value: "440300",
                label: "深圳",
                children: [ {
                    value: "440303",
                    label: "罗湖区"
                }, {
                    value: "440304",
                    label: "福田区"
                }, {
                    value: "440305",
                    label: "南山区"
                }, {
                    value: "440306",
                    label: "宝安区"
                }, {
                    value: "440307",
                    label: "龙岗区"
                }, {
                    value: "440308",
                    label: "盐田区"
                }, {
                    value: "440309",
                    label: "龙华区"
                }, {
                    value: "440310",
                    label: "坪山区"
                } ]
            }, {
                value: "440400",
                label: "珠海",
                children: [ {
                    value: "440402",
                    label: "香洲区"
                }, {
                    value: "440403",
                    label: "斗门区"
                }, {
                    value: "440404",
                    label: "金湾区"
                } ]
            }, {
                value: "440500",
                label: "汕头",
                children: [ {
                    value: "440507",
                    label: "龙湖区"
                }, {
                    value: "440511",
                    label: "金平区"
                }, {
                    value: "440512",
                    label: "濠江区"
                }, {
                    value: "440513",
                    label: "潮阳区"
                }, {
                    value: "440514",
                    label: "潮南区"
                }, {
                    value: "440515",
                    label: "澄海区"
                }, {
                    value: "440523",
                    label: "南澳县"
                } ]
            }, {
                value: "440600",
                label: "佛山",
                children: [ {
                    value: "440604",
                    label: "禅城区"
                }, {
                    value: "440605",
                    label: "南海区"
                }, {
                    value: "440606",
                    label: "顺德区"
                }, {
                    value: "440607",
                    label: "三水区"
                }, {
                    value: "440608",
                    label: "高明区"
                } ]
            }, {
                value: "440700",
                label: "江门",
                children: [ {
                    value: "440703",
                    label: "蓬江区"
                }, {
                    value: "440704",
                    label: "江海区"
                }, {
                    value: "440705",
                    label: "新会区"
                }, {
                    value: "440781",
                    label: "台山市"
                }, {
                    value: "440783",
                    label: "开平市"
                }, {
                    value: "440784",
                    label: "鹤山市"
                }, {
                    value: "440785",
                    label: "恩平市"
                } ]
            }, {
                value: "440800",
                label: "湛江",
                children: [ {
                    value: "440802",
                    label: "赤坎区"
                }, {
                    value: "440803",
                    label: "霞山区"
                }, {
                    value: "440804",
                    label: "坡头区"
                }, {
                    value: "440811",
                    label: "麻章区"
                }, {
                    value: "440823",
                    label: "遂溪县"
                }, {
                    value: "440825",
                    label: "徐闻县"
                }, {
                    value: "440881",
                    label: "廉江市"
                }, {
                    value: "440882",
                    label: "雷州市"
                }, {
                    value: "440883",
                    label: "吴川市"
                } ]
            }, {
                value: "440900",
                label: "茂名",
                children: [ {
                    value: "440902",
                    label: "茂南区"
                }, {
                    value: "440904",
                    label: "电白区"
                }, {
                    value: "440981",
                    label: "高州市"
                }, {
                    value: "440982",
                    label: "化州市"
                }, {
                    value: "440983",
                    label: "信宜市"
                } ]
            }, {
                value: "441200",
                label: "肇庆",
                children: [ {
                    value: "441202",
                    label: "端州区"
                }, {
                    value: "441203",
                    label: "鼎湖区"
                }, {
                    value: "441204",
                    label: "高要区"
                }, {
                    value: "441223",
                    label: "广宁县"
                }, {
                    value: "441224",
                    label: "怀集县"
                }, {
                    value: "441225",
                    label: "封开县"
                }, {
                    value: "441226",
                    label: "德庆县"
                }, {
                    value: "441284",
                    label: "四会市"
                } ]
            }, {
                value: "441300",
                label: "惠州",
                children: [ {
                    value: "441302",
                    label: "惠城区"
                }, {
                    value: "441303",
                    label: "惠阳区"
                }, {
                    value: "441322",
                    label: "博罗县"
                }, {
                    value: "441323",
                    label: "惠东县"
                }, {
                    value: "441324",
                    label: "龙门县"
                } ]
            }, {
                value: "441400",
                label: "梅州",
                children: [ {
                    value: "441402",
                    label: "梅江区"
                }, {
                    value: "441403",
                    label: "梅县区"
                }, {
                    value: "441422",
                    label: "大埔县"
                }, {
                    value: "441423",
                    label: "丰顺县"
                }, {
                    value: "441424",
                    label: "五华县"
                }, {
                    value: "441426",
                    label: "平远县"
                }, {
                    value: "441427",
                    label: "蕉岭县"
                }, {
                    value: "441481",
                    label: "兴宁市"
                } ]
            }, {
                value: "441500",
                label: "汕尾",
                children: [ {
                    value: "441502",
                    label: "城区"
                }, {
                    value: "441521",
                    label: "海丰县"
                }, {
                    value: "441523",
                    label: "陆河县"
                }, {
                    value: "441581",
                    label: "陆丰市"
                } ]
            }, {
                value: "441600",
                label: "河源",
                children: [ {
                    value: "441602",
                    label: "源城区"
                }, {
                    value: "441621",
                    label: "紫金县"
                }, {
                    value: "441622",
                    label: "龙川县"
                }, {
                    value: "441623",
                    label: "连平县"
                }, {
                    value: "441624",
                    label: "和平县"
                }, {
                    value: "441625",
                    label: "东源县"
                } ]
            }, {
                value: "441700",
                label: "阳江",
                children: [ {
                    value: "441702",
                    label: "江城区"
                }, {
                    value: "441704",
                    label: "阳东区"
                }, {
                    value: "441721",
                    label: "阳西县"
                }, {
                    value: "441781",
                    label: "阳春市"
                } ]
            }, {
                value: "441800",
                label: "清远",
                children: [ {
                    value: "441802",
                    label: "清城区"
                }, {
                    value: "441803",
                    label: "清新区"
                }, {
                    value: "441821",
                    label: "佛冈县"
                }, {
                    value: "441823",
                    label: "阳山县"
                }, {
                    value: "441825",
                    label: "连山壮族瑶族自治县"
                }, {
                    value: "441826",
                    label: "连南瑶族自治县"
                }, {
                    value: "441881",
                    label: "英德市"
                }, {
                    value: "441882",
                    label: "连州市"
                } ]
            }, {
                value: "441900",
                label: "东莞",
                children: [ {
                    value: "441900",
                    label: "东莞市"
                } ]
            }, {
                value: "442000",
                label: "中山",
                children: [ {
                    value: "442000",
                    label: "中山市"
                } ]
            }, {
                value: "445100",
                label: "潮州",
                children: [ {
                    value: "445102",
                    label: "湘桥区"
                }, {
                    value: "445103",
                    label: "潮安区"
                }, {
                    value: "445122",
                    label: "饶平县"
                } ]
            }, {
                value: "445200",
                label: "揭阳",
                children: [ {
                    value: "445202",
                    label: "榕城区"
                }, {
                    value: "445203",
                    label: "揭东区"
                }, {
                    value: "445222",
                    label: "揭西县"
                }, {
                    value: "445224",
                    label: "惠来县"
                }, {
                    value: "445281",
                    label: "普宁市"
                } ]
            }, {
                value: "445300",
                label: "云浮",
                children: [ {
                    value: "445302",
                    label: "云城区"
                }, {
                    value: "445303",
                    label: "云安区"
                }, {
                    value: "445321",
                    label: "新兴县"
                }, {
                    value: "445322",
                    label: "郁南县"
                }, {
                    value: "445381",
                    label: "罗定市"
                } ]
            } ]
        }, {
            value: "450000",
            label: "广西",
            children: [ {
                value: "450100",
                label: "南宁",
                children: [ {
                    value: "450102",
                    label: "兴宁区"
                }, {
                    value: "450103",
                    label: "青秀区"
                }, {
                    value: "450105",
                    label: "江南区"
                }, {
                    value: "450107",
                    label: "西乡塘区"
                }, {
                    value: "450108",
                    label: "良庆区"
                }, {
                    value: "450109",
                    label: "邕宁区"
                }, {
                    value: "450110",
                    label: "武鸣区"
                }, {
                    value: "450123",
                    label: "隆安县"
                }, {
                    value: "450124",
                    label: "马山县"
                }, {
                    value: "450125",
                    label: "上林县"
                }, {
                    value: "450126",
                    label: "宾阳县"
                }, {
                    value: "450127",
                    label: "横县"
                } ]
            }, {
                value: "450200",
                label: "柳州",
                children: [ {
                    value: "450202",
                    label: "城中区"
                }, {
                    value: "450203",
                    label: "鱼峰区"
                }, {
                    value: "450204",
                    label: "柳南区"
                }, {
                    value: "450205",
                    label: "柳北区"
                }, {
                    value: "450206",
                    label: "柳江区"
                }, {
                    value: "450222",
                    label: "柳城县"
                }, {
                    value: "450223",
                    label: "鹿寨县"
                }, {
                    value: "450224",
                    label: "融安县"
                }, {
                    value: "450225",
                    label: "融水苗族自治县"
                }, {
                    value: "450226",
                    label: "三江侗族自治县"
                } ]
            }, {
                value: "450300",
                label: "桂林",
                children: [ {
                    value: "450302",
                    label: "秀峰区"
                }, {
                    value: "450303",
                    label: "叠彩区"
                }, {
                    value: "450304",
                    label: "象山区"
                }, {
                    value: "450305",
                    label: "七星区"
                }, {
                    value: "450311",
                    label: "雁山区"
                }, {
                    value: "450312",
                    label: "临桂区"
                }, {
                    value: "450321",
                    label: "阳朔县"
                }, {
                    value: "450323",
                    label: "灵川县"
                }, {
                    value: "450324",
                    label: "全州县"
                }, {
                    value: "450325",
                    label: "兴安县"
                }, {
                    value: "450326",
                    label: "永福县"
                }, {
                    value: "450327",
                    label: "灌阳县"
                }, {
                    value: "450328",
                    label: "龙胜各族自治县"
                }, {
                    value: "450329",
                    label: "资源县"
                }, {
                    value: "450330",
                    label: "平乐县"
                }, {
                    value: "450331",
                    label: "荔浦县"
                }, {
                    value: "450332",
                    label: "恭城瑶族自治县"
                } ]
            }, {
                value: "450400",
                label: "梧州",
                children: [ {
                    value: "450403",
                    label: "万秀区"
                }, {
                    value: "450405",
                    label: "长洲区"
                }, {
                    value: "450406",
                    label: "龙圩区"
                }, {
                    value: "450421",
                    label: "苍梧县"
                }, {
                    value: "450422",
                    label: "藤县"
                }, {
                    value: "450423",
                    label: "蒙山县"
                }, {
                    value: "450481",
                    label: "岑溪市"
                } ]
            }, {
                value: "450500",
                label: "北海",
                children: [ {
                    value: "450502",
                    label: "海城区"
                }, {
                    value: "450503",
                    label: "银海区"
                }, {
                    value: "450512",
                    label: "铁山港区"
                }, {
                    value: "450521",
                    label: "合浦县"
                } ]
            }, {
                value: "450600",
                label: "防城港",
                children: [ {
                    value: "450602",
                    label: "港口区"
                }, {
                    value: "450603",
                    label: "防城区"
                }, {
                    value: "450621",
                    label: "上思县"
                }, {
                    value: "450681",
                    label: "东兴市"
                } ]
            }, {
                value: "450700",
                label: "钦州",
                children: [ {
                    value: "450702",
                    label: "钦南区"
                }, {
                    value: "450703",
                    label: "钦北区"
                }, {
                    value: "450721",
                    label: "灵山县"
                }, {
                    value: "450722",
                    label: "浦北县"
                } ]
            }, {
                value: "450800",
                label: "贵港",
                children: [ {
                    value: "450802",
                    label: "港北区"
                }, {
                    value: "450803",
                    label: "港南区"
                }, {
                    value: "450804",
                    label: "覃塘区"
                }, {
                    value: "450821",
                    label: "平南县"
                }, {
                    value: "450881",
                    label: "桂平市"
                } ]
            }, {
                value: "450900",
                label: "玉林",
                children: [ {
                    value: "450902",
                    label: "玉州区"
                }, {
                    value: "450903",
                    label: "福绵区"
                }, {
                    value: "450921",
                    label: "容县"
                }, {
                    value: "450922",
                    label: "陆川县"
                }, {
                    value: "450923",
                    label: "博白县"
                }, {
                    value: "450924",
                    label: "兴业县"
                }, {
                    value: "450981",
                    label: "北流市"
                } ]
            }, {
                value: "451000",
                label: "百色",
                children: [ {
                    value: "451002",
                    label: "右江区"
                }, {
                    value: "451021",
                    label: "田阳县"
                }, {
                    value: "451022",
                    label: "田东县"
                }, {
                    value: "451023",
                    label: "平果县"
                }, {
                    value: "451024",
                    label: "德保县"
                }, {
                    value: "451026",
                    label: "那坡县"
                }, {
                    value: "451027",
                    label: "凌云县"
                }, {
                    value: "451028",
                    label: "乐业县"
                }, {
                    value: "451029",
                    label: "田林县"
                }, {
                    value: "451030",
                    label: "西林县"
                }, {
                    value: "451031",
                    label: "隆林各族自治县"
                }, {
                    value: "451081",
                    label: "靖西市"
                } ]
            }, {
                value: "451100",
                label: "贺州",
                children: [ {
                    value: "451102",
                    label: "八步区"
                }, {
                    value: "451103",
                    label: "平桂区"
                }, {
                    value: "451121",
                    label: "昭平县"
                }, {
                    value: "451122",
                    label: "钟山县"
                }, {
                    value: "451123",
                    label: "富川瑶族自治县"
                } ]
            }, {
                value: "451200",
                label: "河池",
                children: [ {
                    value: "451202",
                    label: "金城江区"
                }, {
                    value: "451203",
                    label: "宜州区"
                }, {
                    value: "451221",
                    label: "南丹县"
                }, {
                    value: "451222",
                    label: "天峨县"
                }, {
                    value: "451223",
                    label: "凤山县"
                }, {
                    value: "451224",
                    label: "东兰县"
                }, {
                    value: "451225",
                    label: "罗城仫佬族自治县"
                }, {
                    value: "451226",
                    label: "环江毛南族自治县"
                }, {
                    value: "451227",
                    label: "巴马瑶族自治县"
                }, {
                    value: "451228",
                    label: "都安瑶族自治县"
                }, {
                    value: "451229",
                    label: "大化瑶族自治县"
                } ]
            }, {
                value: "451300",
                label: "来宾",
                children: [ {
                    value: "451302",
                    label: "兴宾区"
                }, {
                    value: "451321",
                    label: "忻城县"
                }, {
                    value: "451322",
                    label: "象州县"
                }, {
                    value: "451323",
                    label: "武宣县"
                }, {
                    value: "451324",
                    label: "金秀瑶族自治县"
                }, {
                    value: "451381",
                    label: "合山市"
                } ]
            }, {
                value: "451400",
                label: "崇左",
                children: [ {
                    value: "451402",
                    label: "江州区"
                }, {
                    value: "451421",
                    label: "扶绥县"
                }, {
                    value: "451422",
                    label: "宁明县"
                }, {
                    value: "451423",
                    label: "龙州县"
                }, {
                    value: "451424",
                    label: "大新县"
                }, {
                    value: "451425",
                    label: "天等县"
                }, {
                    value: "451481",
                    label: "凭祥市"
                } ]
            } ]
        }, {
            value: "460000",
            label: "海南",
            children: [ {
                value: "460100",
                label: "海口",
                children: [ {
                    value: "460105",
                    label: "秀英区"
                }, {
                    value: "460106",
                    label: "龙华区"
                }, {
                    value: "460107",
                    label: "琼山区"
                }, {
                    value: "460108",
                    label: "美兰区"
                } ]
            }, {
                value: "460200",
                label: "三亚",
                children: [ {
                    value: "460202",
                    label: "海棠区"
                }, {
                    value: "460203",
                    label: "吉阳区"
                }, {
                    value: "460204",
                    label: "天涯区"
                }, {
                    value: "460205",
                    label: "崖州区"
                } ]
            }, {
                value: "460300",
                label: "三沙",
                children: [ {
                    value: "460321",
                    label: "西沙群岛"
                }, {
                    value: "460322",
                    label: "南沙群岛"
                }, {
                    value: "460323",
                    label: "中沙群岛"
                }, {
                    value: "460324",
                    label: "永乐群岛"
                } ]
            }, {
                value: "460400",
                label: "儋州",
                children: [ {
                    value: "460400",
                    label: "儋州市"
                } ]
            }, {
                value: "469001",
                label: "五指山",
                children: [ {
                    value: "469001",
                    label: "五指山市"
                } ]
            }, {
                value: "469002",
                label: "琼海",
                children: [ {
                    value: "469002",
                    label: "琼海市"
                } ]
            }, {
                value: "469005",
                label: "文昌",
                children: [ {
                    value: "469005",
                    label: "文昌市"
                } ]
            }, {
                value: "469006",
                label: "万宁",
                children: [ {
                    value: "469006",
                    label: "万宁市"
                } ]
            }, {
                value: "469007",
                label: "东方",
                children: [ {
                    value: "469007",
                    label: "东方市"
                } ]
            }, {
                value: "469021",
                label: "定安",
                children: [ {
                    value: "469021",
                    label: "定安县"
                } ]
            }, {
                value: "469022",
                label: "屯昌",
                children: [ {
                    value: "469022",
                    label: "屯昌县"
                } ]
            }, {
                value: "469023",
                label: "澄迈",
                children: [ {
                    value: "469023",
                    label: "澄迈县"
                } ]
            }, {
                value: "469024",
                label: "临高",
                children: [ {
                    value: "469024",
                    label: "临高县"
                } ]
            }, {
                value: "469025",
                label: "白沙黎族",
                children: [ {
                    value: "469025",
                    label: "白沙黎族自治县"
                } ]
            }, {
                value: "469026",
                label: "昌江黎族",
                children: [ {
                    value: "469026",
                    label: "昌江黎族自治县"
                } ]
            }, {
                value: "469027",
                label: "乐东黎族",
                children: [ {
                    value: "469027",
                    label: "乐东黎族自治县"
                } ]
            }, {
                value: "469028",
                label: "陵水黎族",
                children: [ {
                    value: "469028",
                    label: "陵水黎族自治县"
                } ]
            }, {
                value: "469029",
                label: "保亭黎族",
                children: [ {
                    value: "469029",
                    label: "保亭黎族苗族自治县"
                } ]
            }, {
                value: "469030",
                label: "琼中",
                children: [ {
                    value: "469030",
                    label: "琼中黎族苗族自治县"
                } ]
            } ]
        }, {
            value: "500000",
            label: "重庆",
            children: [ {
                value: "500100",
                label: "重庆城区",
                children: [ {
                    value: "500101",
                    label: "万州区"
                }, {
                    value: "500102",
                    label: "涪陵区"
                }, {
                    value: "500103",
                    label: "渝中区"
                }, {
                    value: "500104",
                    label: "大渡口区"
                }, {
                    value: "500105",
                    label: "江北区"
                }, {
                    value: "500106",
                    label: "沙坪坝区"
                }, {
                    value: "500107",
                    label: "九龙坡区"
                }, {
                    value: "500108",
                    label: "南岸区"
                }, {
                    value: "500109",
                    label: "北碚区"
                }, {
                    value: "500110",
                    label: "綦江区"
                }, {
                    value: "500111",
                    label: "大足区"
                }, {
                    value: "500112",
                    label: "渝北区"
                }, {
                    value: "500113",
                    label: "巴南区"
                }, {
                    value: "500114",
                    label: "黔江区"
                }, {
                    value: "500115",
                    label: "长寿区"
                }, {
                    value: "500116",
                    label: "江津区"
                }, {
                    value: "500117",
                    label: "合川区"
                }, {
                    value: "500118",
                    label: "永川区"
                }, {
                    value: "500119",
                    label: "南川区"
                }, {
                    value: "500120",
                    label: "璧山区"
                }, {
                    value: "500151",
                    label: "铜梁区"
                }, {
                    value: "500152",
                    label: "潼南区"
                }, {
                    value: "500153",
                    label: "荣昌区"
                }, {
                    value: "500154",
                    label: "开州区"
                }, {
                    value: "500155",
                    label: "梁平区"
                }, {
                    value: "500156",
                    label: "武隆区"
                } ]
            }, {
                value: "500200",
                label: "重庆郊县",
                children: [ {
                    value: "500229",
                    label: "城口县"
                }, {
                    value: "500230",
                    label: "丰都县"
                }, {
                    value: "500231",
                    label: "垫江县"
                }, {
                    value: "500233",
                    label: "忠县"
                }, {
                    value: "500235",
                    label: "云阳县"
                }, {
                    value: "500236",
                    label: "奉节县"
                }, {
                    value: "500237",
                    label: "巫山县"
                }, {
                    value: "500238",
                    label: "巫溪县"
                }, {
                    value: "500240",
                    label: "石柱土家族自治县"
                }, {
                    value: "500241",
                    label: "秀山土家族苗族自治县"
                }, {
                    value: "500242",
                    label: "酉阳土家族苗族自治县"
                }, {
                    value: "500243",
                    label: "彭水苗族土家族自治县"
                } ]
            } ]
        }, {
            value: "510000",
            label: "四川",
            children: [ {
                value: "510100",
                label: "成都",
                children: [ {
                    value: "510104",
                    label: "锦江区"
                }, {
                    value: "510105",
                    label: "青羊区"
                }, {
                    value: "510106",
                    label: "金牛区"
                }, {
                    value: "510107",
                    label: "武侯区"
                }, {
                    value: "510108",
                    label: "成华区"
                }, {
                    value: "510112",
                    label: "龙泉驿区"
                }, {
                    value: "510113",
                    label: "青白江区"
                }, {
                    value: "510114",
                    label: "新都区"
                }, {
                    value: "510115",
                    label: "温江区"
                }, {
                    value: "510116",
                    label: "双流区"
                }, {
                    value: "510117",
                    label: "郫都区"
                }, {
                    value: "510121",
                    label: "金堂县"
                }, {
                    value: "510129",
                    label: "大邑县"
                }, {
                    value: "510131",
                    label: "蒲江县"
                }, {
                    value: "510132",
                    label: "新津县"
                }, {
                    value: "510181",
                    label: "都江堰市"
                }, {
                    value: "510182",
                    label: "彭州市"
                }, {
                    value: "510183",
                    label: "邛崃市"
                }, {
                    value: "510184",
                    label: "崇州市"
                }, {
                    value: "510185",
                    label: "简阳市"
                } ]
            }, {
                value: "510300",
                label: "自贡",
                children: [ {
                    value: "510302",
                    label: "自流井区"
                }, {
                    value: "510303",
                    label: "贡井区"
                }, {
                    value: "510304",
                    label: "大安区"
                }, {
                    value: "510311",
                    label: "沿滩区"
                }, {
                    value: "510321",
                    label: "荣县"
                }, {
                    value: "510322",
                    label: "富顺县"
                } ]
            }, {
                value: "510400",
                label: "攀枝花",
                children: [ {
                    value: "510402",
                    label: "东区"
                }, {
                    value: "510403",
                    label: "西区"
                }, {
                    value: "510411",
                    label: "仁和区"
                }, {
                    value: "510421",
                    label: "米易县"
                }, {
                    value: "510422",
                    label: "盐边县"
                } ]
            }, {
                value: "510500",
                label: "泸州",
                children: [ {
                    value: "510502",
                    label: "江阳区"
                }, {
                    value: "510503",
                    label: "纳溪区"
                }, {
                    value: "510504",
                    label: "龙马潭区"
                }, {
                    value: "510521",
                    label: "泸县"
                }, {
                    value: "510522",
                    label: "合江县"
                }, {
                    value: "510524",
                    label: "叙永县"
                }, {
                    value: "510525",
                    label: "古蔺县"
                } ]
            }, {
                value: "510600",
                label: "德阳",
                children: [ {
                    value: "510603",
                    label: "旌阳区"
                }, {
                    value: "510623",
                    label: "中江县"
                }, {
                    value: "510626",
                    label: "罗江县"
                }, {
                    value: "510681",
                    label: "广汉市"
                }, {
                    value: "510682",
                    label: "什邡市"
                }, {
                    value: "510683",
                    label: "绵竹市"
                } ]
            }, {
                value: "510700",
                label: "绵阳",
                children: [ {
                    value: "510703",
                    label: "涪城区"
                }, {
                    value: "510704",
                    label: "游仙区"
                }, {
                    value: "510705",
                    label: "安州区"
                }, {
                    value: "510722",
                    label: "三台县"
                }, {
                    value: "510723",
                    label: "盐亭县"
                }, {
                    value: "510725",
                    label: "梓潼县"
                }, {
                    value: "510726",
                    label: "北川羌族自治县"
                }, {
                    value: "510727",
                    label: "平武县"
                }, {
                    value: "510781",
                    label: "江油市"
                } ]
            }, {
                value: "510800",
                label: "广元",
                children: [ {
                    value: "510802",
                    label: "利州区"
                }, {
                    value: "510811",
                    label: "昭化区"
                }, {
                    value: "510812",
                    label: "朝天区"
                }, {
                    value: "510821",
                    label: "旺苍县"
                }, {
                    value: "510822",
                    label: "青川县"
                }, {
                    value: "510823",
                    label: "剑阁县"
                }, {
                    value: "510824",
                    label: "苍溪县"
                } ]
            }, {
                value: "510900",
                label: "遂宁",
                children: [ {
                    value: "510903",
                    label: "船山区"
                }, {
                    value: "510904",
                    label: "安居区"
                }, {
                    value: "510921",
                    label: "蓬溪县"
                }, {
                    value: "510922",
                    label: "射洪县"
                }, {
                    value: "510923",
                    label: "大英县"
                } ]
            }, {
                value: "511000",
                label: "内江",
                children: [ {
                    value: "511002",
                    label: "市中区"
                }, {
                    value: "511011",
                    label: "东兴区"
                }, {
                    value: "511024",
                    label: "威远县"
                }, {
                    value: "511025",
                    label: "资中县"
                }, {
                    value: "511083",
                    label: "隆昌市"
                } ]
            }, {
                value: "511100",
                label: "乐山",
                children: [ {
                    value: "511102",
                    label: "市中区"
                }, {
                    value: "511111",
                    label: "沙湾区"
                }, {
                    value: "511112",
                    label: "五通桥区"
                }, {
                    value: "511113",
                    label: "金口河区"
                }, {
                    value: "511123",
                    label: "犍为县"
                }, {
                    value: "511124",
                    label: "井研县"
                }, {
                    value: "511126",
                    label: "夹江县"
                }, {
                    value: "511129",
                    label: "沐川县"
                }, {
                    value: "511132",
                    label: "峨边彝族自治县"
                }, {
                    value: "511133",
                    label: "马边彝族自治县"
                }, {
                    value: "511181",
                    label: "峨眉山市"
                } ]
            }, {
                value: "511300",
                label: "南充",
                children: [ {
                    value: "511302",
                    label: "顺庆区"
                }, {
                    value: "511303",
                    label: "高坪区"
                }, {
                    value: "511304",
                    label: "嘉陵区"
                }, {
                    value: "511321",
                    label: "南部县"
                }, {
                    value: "511322",
                    label: "营山县"
                }, {
                    value: "511323",
                    label: "蓬安县"
                }, {
                    value: "511324",
                    label: "仪陇县"
                }, {
                    value: "511325",
                    label: "西充县"
                }, {
                    value: "511381",
                    label: "阆中市"
                } ]
            }, {
                value: "511400",
                label: "眉山",
                children: [ {
                    value: "511402",
                    label: "东坡区"
                }, {
                    value: "511403",
                    label: "彭山区"
                }, {
                    value: "511421",
                    label: "仁寿县"
                }, {
                    value: "511423",
                    label: "洪雅县"
                }, {
                    value: "511424",
                    label: "丹棱县"
                }, {
                    value: "511425",
                    label: "青神县"
                } ]
            }, {
                value: "511500",
                label: "宜宾",
                children: [ {
                    value: "511502",
                    label: "翠屏区"
                }, {
                    value: "511503",
                    label: "南溪区"
                }, {
                    value: "511521",
                    label: "宜宾县"
                }, {
                    value: "511523",
                    label: "江安县"
                }, {
                    value: "511524",
                    label: "长宁县"
                }, {
                    value: "511525",
                    label: "高县"
                }, {
                    value: "511526",
                    label: "珙县"
                }, {
                    value: "511527",
                    label: "筠连县"
                }, {
                    value: "511528",
                    label: "兴文县"
                }, {
                    value: "511529",
                    label: "屏山县"
                } ]
            }, {
                value: "511600",
                label: "广安",
                children: [ {
                    value: "511602",
                    label: "广安区"
                }, {
                    value: "511603",
                    label: "前锋区"
                }, {
                    value: "511621",
                    label: "岳池县"
                }, {
                    value: "511622",
                    label: "武胜县"
                }, {
                    value: "511623",
                    label: "邻水县"
                }, {
                    value: "511681",
                    label: "华蓥市"
                } ]
            }, {
                value: "511700",
                label: "达州",
                children: [ {
                    value: "511702",
                    label: "通川区"
                }, {
                    value: "511703",
                    label: "达川区"
                }, {
                    value: "511722",
                    label: "宣汉县"
                }, {
                    value: "511723",
                    label: "开江县"
                }, {
                    value: "511724",
                    label: "大竹县"
                }, {
                    value: "511725",
                    label: "渠县"
                }, {
                    value: "511781",
                    label: "万源市"
                } ]
            }, {
                value: "511800",
                label: "雅安",
                children: [ {
                    value: "511802",
                    label: "雨城区"
                }, {
                    value: "511803",
                    label: "名山区"
                }, {
                    value: "511822",
                    label: "荥经县"
                }, {
                    value: "511823",
                    label: "汉源县"
                }, {
                    value: "511824",
                    label: "石棉县"
                }, {
                    value: "511825",
                    label: "天全县"
                }, {
                    value: "511826",
                    label: "芦山县"
                }, {
                    value: "511827",
                    label: "宝兴县"
                } ]
            }, {
                value: "511900",
                label: "巴中",
                children: [ {
                    value: "511902",
                    label: "巴州区"
                }, {
                    value: "511903",
                    label: "恩阳区"
                }, {
                    value: "511921",
                    label: "通江县"
                }, {
                    value: "511922",
                    label: "南江县"
                }, {
                    value: "511923",
                    label: "平昌县"
                } ]
            }, {
                value: "512000",
                label: "资阳",
                children: [ {
                    value: "512002",
                    label: "雁江区"
                }, {
                    value: "512021",
                    label: "安岳县"
                }, {
                    value: "512022",
                    label: "乐至县"
                } ]
            }, {
                value: "513200",
                label: "阿坝",
                children: [ {
                    value: "513201",
                    label: "马尔康市"
                }, {
                    value: "513221",
                    label: "汶川县"
                }, {
                    value: "513222",
                    label: "理县"
                }, {
                    value: "513223",
                    label: "茂县"
                }, {
                    value: "513224",
                    label: "松潘县"
                }, {
                    value: "513225",
                    label: "九寨沟县"
                }, {
                    value: "513226",
                    label: "金川县"
                }, {
                    value: "513227",
                    label: "小金县"
                }, {
                    value: "513228",
                    label: "黑水县"
                }, {
                    value: "513230",
                    label: "壤塘县"
                }, {
                    value: "513231",
                    label: "阿坝县"
                }, {
                    value: "513232",
                    label: "若尔盖县"
                }, {
                    value: "513233",
                    label: "红原县"
                } ]
            }, {
                value: "513300",
                label: "甘孜藏族",
                children: [ {
                    value: "513301",
                    label: "康定市"
                }, {
                    value: "513322",
                    label: "泸定县"
                }, {
                    value: "513323",
                    label: "丹巴县"
                }, {
                    value: "513324",
                    label: "九龙县"
                }, {
                    value: "513325",
                    label: "雅江县"
                }, {
                    value: "513326",
                    label: "道孚县"
                }, {
                    value: "513327",
                    label: "炉霍县"
                }, {
                    value: "513328",
                    label: "甘孜县"
                }, {
                    value: "513329",
                    label: "新龙县"
                }, {
                    value: "513330",
                    label: "德格县"
                }, {
                    value: "513331",
                    label: "白玉县"
                }, {
                    value: "513332",
                    label: "石渠县"
                }, {
                    value: "513333",
                    label: "色达县"
                }, {
                    value: "513334",
                    label: "理塘县"
                }, {
                    value: "513335",
                    label: "巴塘县"
                }, {
                    value: "513336",
                    label: "乡城县"
                }, {
                    value: "513337",
                    label: "稻城县"
                }, {
                    value: "513338",
                    label: "得荣县"
                } ]
            }, {
                value: "513400",
                label: "凉山彝族",
                children: [ {
                    value: "513401",
                    label: "西昌市"
                }, {
                    value: "513422",
                    label: "木里藏族自治县"
                }, {
                    value: "513423",
                    label: "盐源县"
                }, {
                    value: "513424",
                    label: "德昌县"
                }, {
                    value: "513425",
                    label: "会理县"
                }, {
                    value: "513426",
                    label: "会东县"
                }, {
                    value: "513427",
                    label: "宁南县"
                }, {
                    value: "513428",
                    label: "普格县"
                }, {
                    value: "513429",
                    label: "布拖县"
                }, {
                    value: "513430",
                    label: "金阳县"
                }, {
                    value: "513431",
                    label: "昭觉县"
                }, {
                    value: "513432",
                    label: "喜德县"
                }, {
                    value: "513433",
                    label: "冕宁县"
                }, {
                    value: "513434",
                    label: "越西县"
                }, {
                    value: "513435",
                    label: "甘洛县"
                }, {
                    value: "513436",
                    label: "美姑县"
                }, {
                    value: "513437",
                    label: "雷波县"
                } ]
            } ]
        }, {
            value: "520000",
            label: "贵州",
            children: [ {
                value: "520100",
                label: "贵阳",
                children: [ {
                    value: "520102",
                    label: "南明区"
                }, {
                    value: "520103",
                    label: "云岩区"
                }, {
                    value: "520111",
                    label: "花溪区"
                }, {
                    value: "520112",
                    label: "乌当区"
                }, {
                    value: "520113",
                    label: "白云区"
                }, {
                    value: "520115",
                    label: "观山湖区"
                }, {
                    value: "520121",
                    label: "开阳县"
                }, {
                    value: "520122",
                    label: "息烽县"
                }, {
                    value: "520123",
                    label: "修文县"
                }, {
                    value: "520181",
                    label: "清镇市"
                } ]
            }, {
                value: "520200",
                label: "六盘水",
                children: [ {
                    value: "520201",
                    label: "钟山区"
                }, {
                    value: "520203",
                    label: "六枝特区"
                }, {
                    value: "520221",
                    label: "水城县"
                }, {
                    value: "520281",
                    label: "盘州市"
                } ]
            }, {
                value: "520300",
                label: "遵义",
                children: [ {
                    value: "520302",
                    label: "红花岗区"
                }, {
                    value: "520303",
                    label: "汇川区"
                }, {
                    value: "520304",
                    label: "播州区"
                }, {
                    value: "520322",
                    label: "桐梓县"
                }, {
                    value: "520323",
                    label: "绥阳县"
                }, {
                    value: "520324",
                    label: "正安县"
                }, {
                    value: "520325",
                    label: "道真仡佬族苗族自治县"
                }, {
                    value: "520326",
                    label: "务川仡佬族苗族自治县"
                }, {
                    value: "520327",
                    label: "凤冈县"
                }, {
                    value: "520328",
                    label: "湄潭县"
                }, {
                    value: "520329",
                    label: "余庆县"
                }, {
                    value: "520330",
                    label: "习水县"
                }, {
                    value: "520381",
                    label: "赤水市"
                }, {
                    value: "520382",
                    label: "仁怀市"
                } ]
            }, {
                value: "520400",
                label: "安顺",
                children: [ {
                    value: "520402",
                    label: "西秀区"
                }, {
                    value: "520403",
                    label: "平坝区"
                }, {
                    value: "520422",
                    label: "普定县"
                }, {
                    value: "520423",
                    label: "镇宁布依族苗族自治县"
                }, {
                    value: "520424",
                    label: "关岭布依族苗族自治县"
                }, {
                    value: "520425",
                    label: "紫云苗族布依族自治县"
                } ]
            }, {
                value: "520500",
                label: "毕节",
                children: [ {
                    value: "520502",
                    label: "七星关区"
                }, {
                    value: "520521",
                    label: "大方县"
                }, {
                    value: "520522",
                    label: "黔西县"
                }, {
                    value: "520523",
                    label: "金沙县"
                }, {
                    value: "520524",
                    label: "织金县"
                }, {
                    value: "520525",
                    label: "纳雍县"
                }, {
                    value: "520526",
                    label: "威宁彝族回族苗族自治县"
                }, {
                    value: "520527",
                    label: "赫章县"
                } ]
            }, {
                value: "520600",
                label: "铜仁",
                children: [ {
                    value: "520602",
                    label: "碧江区"
                }, {
                    value: "520603",
                    label: "万山区"
                }, {
                    value: "520621",
                    label: "江口县"
                }, {
                    value: "520622",
                    label: "玉屏侗族自治县"
                }, {
                    value: "520623",
                    label: "石阡县"
                }, {
                    value: "520624",
                    label: "思南县"
                }, {
                    value: "520625",
                    label: "印江土家族苗族自治县"
                }, {
                    value: "520626",
                    label: "德江县"
                }, {
                    value: "520627",
                    label: "沿河土家族自治县"
                }, {
                    value: "520628",
                    label: "松桃苗族自治县"
                } ]
            }, {
                value: "522300",
                label: "黔西南",
                children: [ {
                    value: "522301",
                    label: "兴义市"
                }, {
                    value: "522322",
                    label: "兴仁县"
                }, {
                    value: "522323",
                    label: "普安县"
                }, {
                    value: "522324",
                    label: "晴隆县"
                }, {
                    value: "522325",
                    label: "贞丰县"
                }, {
                    value: "522326",
                    label: "望谟县"
                }, {
                    value: "522327",
                    label: "册亨县"
                }, {
                    value: "522328",
                    label: "安龙县"
                } ]
            }, {
                value: "522600",
                label: "黔东南",
                children: [ {
                    value: "522601",
                    label: "凯里市"
                }, {
                    value: "522622",
                    label: "黄平县"
                }, {
                    value: "522623",
                    label: "施秉县"
                }, {
                    value: "522624",
                    label: "三穗县"
                }, {
                    value: "522625",
                    label: "镇远县"
                }, {
                    value: "522626",
                    label: "岑巩县"
                }, {
                    value: "522627",
                    label: "天柱县"
                }, {
                    value: "522628",
                    label: "锦屏县"
                }, {
                    value: "522629",
                    label: "剑河县"
                }, {
                    value: "522630",
                    label: "台江县"
                }, {
                    value: "522631",
                    label: "黎平县"
                }, {
                    value: "522632",
                    label: "榕江县"
                }, {
                    value: "522633",
                    label: "从江县"
                }, {
                    value: "522634",
                    label: "雷山县"
                }, {
                    value: "522635",
                    label: "麻江县"
                }, {
                    value: "522636",
                    label: "丹寨县"
                } ]
            }, {
                value: "522700",
                label: "黔南",
                children: [ {
                    value: "522701",
                    label: "都匀市"
                }, {
                    value: "522702",
                    label: "福泉市"
                }, {
                    value: "522722",
                    label: "荔波县"
                }, {
                    value: "522723",
                    label: "贵定县"
                }, {
                    value: "522725",
                    label: "瓮安县"
                }, {
                    value: "522726",
                    label: "独山县"
                }, {
                    value: "522727",
                    label: "平塘县"
                }, {
                    value: "522728",
                    label: "罗甸县"
                }, {
                    value: "522729",
                    label: "长顺县"
                }, {
                    value: "522730",
                    label: "龙里县"
                }, {
                    value: "522731",
                    label: "惠水县"
                }, {
                    value: "522732",
                    label: "三都水族自治县"
                } ]
            } ]
        }, {
            value: "530000",
            label: "云南",
            children: [ {
                value: "530100",
                label: "昆明",
                children: [ {
                    value: "530102",
                    label: "五华区"
                }, {
                    value: "530103",
                    label: "盘龙区"
                }, {
                    value: "530111",
                    label: "官渡区"
                }, {
                    value: "530112",
                    label: "西山区"
                }, {
                    value: "530113",
                    label: "东川区"
                }, {
                    value: "530114",
                    label: "呈贡区"
                }, {
                    value: "530115",
                    label: "晋宁区"
                }, {
                    value: "530124",
                    label: "富民县"
                }, {
                    value: "530125",
                    label: "宜良县"
                }, {
                    value: "530126",
                    label: "石林彝族自治县"
                }, {
                    value: "530127",
                    label: "嵩明县"
                }, {
                    value: "530128",
                    label: "禄劝彝族苗族自治县"
                }, {
                    value: "530129",
                    label: "寻甸回族彝族自治县"
                }, {
                    value: "530181",
                    label: "安宁市"
                } ]
            }, {
                value: "530300",
                label: "曲靖",
                children: [ {
                    value: "530302",
                    label: "麒麟区"
                }, {
                    value: "530303",
                    label: "沾益区"
                }, {
                    value: "530321",
                    label: "马龙县"
                }, {
                    value: "530322",
                    label: "陆良县"
                }, {
                    value: "530323",
                    label: "师宗县"
                }, {
                    value: "530324",
                    label: "罗平县"
                }, {
                    value: "530325",
                    label: "富源县"
                }, {
                    value: "530326",
                    label: "会泽县"
                }, {
                    value: "530381",
                    label: "宣威市"
                } ]
            }, {
                value: "530400",
                label: "玉溪",
                children: [ {
                    value: "530402",
                    label: "红塔区"
                }, {
                    value: "530403",
                    label: "江川区"
                }, {
                    value: "530422",
                    label: "澄江县"
                }, {
                    value: "530423",
                    label: "通海县"
                }, {
                    value: "530424",
                    label: "华宁县"
                }, {
                    value: "530425",
                    label: "易门县"
                }, {
                    value: "530426",
                    label: "峨山彝族自治县"
                }, {
                    value: "530427",
                    label: "新平彝族傣族自治县"
                }, {
                    value: "530428",
                    label: "元江哈尼族彝族傣族自治县"
                } ]
            }, {
                value: "530500",
                label: "保山",
                children: [ {
                    value: "530502",
                    label: "隆阳区"
                }, {
                    value: "530521",
                    label: "施甸县"
                }, {
                    value: "530523",
                    label: "龙陵县"
                }, {
                    value: "530524",
                    label: "昌宁县"
                }, {
                    value: "530581",
                    label: "腾冲市"
                } ]
            }, {
                value: "530600",
                label: "昭通",
                children: [ {
                    value: "530602",
                    label: "昭阳区"
                }, {
                    value: "530621",
                    label: "鲁甸县"
                }, {
                    value: "530622",
                    label: "巧家县"
                }, {
                    value: "530623",
                    label: "盐津县"
                }, {
                    value: "530624",
                    label: "大关县"
                }, {
                    value: "530625",
                    label: "永善县"
                }, {
                    value: "530626",
                    label: "绥江县"
                }, {
                    value: "530627",
                    label: "镇雄县"
                }, {
                    value: "530628",
                    label: "彝良县"
                }, {
                    value: "530629",
                    label: "威信县"
                }, {
                    value: "530630",
                    label: "水富县"
                } ]
            }, {
                value: "530700",
                label: "丽江",
                children: [ {
                    value: "530702",
                    label: "古城区"
                }, {
                    value: "530721",
                    label: "玉龙纳西族自治县"
                }, {
                    value: "530722",
                    label: "永胜县"
                }, {
                    value: "530723",
                    label: "华坪县"
                }, {
                    value: "530724",
                    label: "宁蒗彝族自治县"
                } ]
            }, {
                value: "530800",
                label: "普洱",
                children: [ {
                    value: "530802",
                    label: "思茅区"
                }, {
                    value: "530821",
                    label: "宁洱哈尼族彝族自治县"
                }, {
                    value: "530822",
                    label: "墨江哈尼族自治县"
                }, {
                    value: "530823",
                    label: "景东彝族自治县"
                }, {
                    value: "530824",
                    label: "景谷傣族彝族自治县"
                }, {
                    value: "530825",
                    label: "镇沅彝族哈尼族拉祜族自治县"
                }, {
                    value: "530826",
                    label: "江城哈尼族彝族自治县"
                }, {
                    value: "530827",
                    label: "孟连傣族拉祜族佤族自治县"
                }, {
                    value: "530828",
                    label: "澜沧拉祜族自治县"
                }, {
                    value: "530829",
                    label: "西盟佤族自治县"
                } ]
            }, {
                value: "530900",
                label: "临沧",
                children: [ {
                    value: "530902",
                    label: "临翔区"
                }, {
                    value: "530921",
                    label: "凤庆县"
                }, {
                    value: "530922",
                    label: "云县"
                }, {
                    value: "530923",
                    label: "永德县"
                }, {
                    value: "530924",
                    label: "镇康县"
                }, {
                    value: "530925",
                    label: "双江拉祜族佤族布朗族傣族自治县"
                }, {
                    value: "530926",
                    label: "耿马傣族佤族自治县"
                }, {
                    value: "530927",
                    label: "沧源佤族自治县"
                } ]
            }, {
                value: "532300",
                label: "楚雄",
                children: [ {
                    value: "532301",
                    label: "楚雄市"
                }, {
                    value: "532322",
                    label: "双柏县"
                }, {
                    value: "532323",
                    label: "牟定县"
                }, {
                    value: "532324",
                    label: "南华县"
                }, {
                    value: "532325",
                    label: "姚安县"
                }, {
                    value: "532326",
                    label: "大姚县"
                }, {
                    value: "532327",
                    label: "永仁县"
                }, {
                    value: "532328",
                    label: "元谋县"
                }, {
                    value: "532329",
                    label: "武定县"
                }, {
                    value: "532331",
                    label: "禄丰县"
                } ]
            }, {
                value: "532500",
                label: "红河",
                children: [ {
                    value: "532501",
                    label: "个旧市"
                }, {
                    value: "532502",
                    label: "开远市"
                }, {
                    value: "532503",
                    label: "蒙自市"
                }, {
                    value: "532504",
                    label: "弥勒市"
                }, {
                    value: "532523",
                    label: "屏边苗族自治县"
                }, {
                    value: "532524",
                    label: "建水县"
                }, {
                    value: "532525",
                    label: "石屏县"
                }, {
                    value: "532527",
                    label: "泸西县"
                }, {
                    value: "532528",
                    label: "元阳县"
                }, {
                    value: "532529",
                    label: "红河县"
                }, {
                    value: "532530",
                    label: "金平苗族瑶族傣族自治县"
                }, {
                    value: "532531",
                    label: "绿春县"
                }, {
                    value: "532532",
                    label: "河口瑶族自治县"
                } ]
            }, {
                value: "532600",
                label: "文山",
                children: [ {
                    value: "532601",
                    label: "文山市"
                }, {
                    value: "532622",
                    label: "砚山县"
                }, {
                    value: "532623",
                    label: "西畴县"
                }, {
                    value: "532624",
                    label: "麻栗坡县"
                }, {
                    value: "532625",
                    label: "马关县"
                }, {
                    value: "532626",
                    label: "丘北县"
                }, {
                    value: "532627",
                    label: "广南县"
                }, {
                    value: "532628",
                    label: "富宁县"
                } ]
            }, {
                value: "532800",
                label: "西双版纳",
                children: [ {
                    value: "532801",
                    label: "景洪市"
                }, {
                    value: "532822",
                    label: "勐海县"
                }, {
                    value: "532823",
                    label: "勐腊县"
                } ]
            }, {
                value: "532900",
                label: "大理",
                children: [ {
                    value: "532901",
                    label: "大理市"
                }, {
                    value: "532922",
                    label: "漾濞彝族自治县"
                }, {
                    value: "532923",
                    label: "祥云县"
                }, {
                    value: "532924",
                    label: "宾川县"
                }, {
                    value: "532925",
                    label: "弥渡县"
                }, {
                    value: "532926",
                    label: "南涧彝族自治县"
                }, {
                    value: "532927",
                    label: "巍山彝族回族自治县"
                }, {
                    value: "532928",
                    label: "永平县"
                }, {
                    value: "532929",
                    label: "云龙县"
                }, {
                    value: "532930",
                    label: "洱源县"
                }, {
                    value: "532931",
                    label: "剑川县"
                }, {
                    value: "532932",
                    label: "鹤庆县"
                } ]
            }, {
                value: "533100",
                label: "德宏",
                children: [ {
                    value: "533102",
                    label: "瑞丽市"
                }, {
                    value: "533103",
                    label: "芒市"
                }, {
                    value: "533122",
                    label: "梁河县"
                }, {
                    value: "533123",
                    label: "盈江县"
                }, {
                    value: "533124",
                    label: "陇川县"
                } ]
            }, {
                value: "533300",
                label: "怒江",
                children: [ {
                    value: "533301",
                    label: "泸水市"
                }, {
                    value: "533323",
                    label: "福贡县"
                }, {
                    value: "533324",
                    label: "贡山独龙族怒族自治县"
                }, {
                    value: "533325",
                    label: "兰坪白族普米族自治县"
                } ]
            }, {
                value: "533400",
                label: "迪庆",
                children: [ {
                    value: "533401",
                    label: "香格里拉市"
                }, {
                    value: "533422",
                    label: "德钦县"
                }, {
                    value: "533423",
                    label: "维西傈僳族自治县"
                } ]
            } ]
        }, {
            value: "540000",
            label: "西藏",
            children: [ {
                value: "540100",
                label: "拉萨",
                children: [ {
                    value: "540102",
                    label: "城关区"
                }, {
                    value: "540103",
                    label: "堆龙德庆区"
                }, {
                    value: "540104",
                    label: "达孜区"
                }, {
                    value: "540121",
                    label: "林周县"
                }, {
                    value: "540122",
                    label: "当雄县"
                }, {
                    value: "540123",
                    label: "尼木县"
                }, {
                    value: "540124",
                    label: "曲水县"
                }, {
                    value: "540127",
                    label: "墨竹工卡县"
                } ]
            }, {
                value: "540200",
                label: "日喀则",
                children: [ {
                    value: "540202",
                    label: "桑珠孜区"
                }, {
                    value: "540221",
                    label: "南木林县"
                }, {
                    value: "540222",
                    label: "江孜县"
                }, {
                    value: "540223",
                    label: "定日县"
                }, {
                    value: "540224",
                    label: "萨迦县"
                }, {
                    value: "540226",
                    label: "昂仁县"
                }, {
                    value: "540227",
                    label: "谢通门县"
                }, {
                    value: "540228",
                    label: "白朗县"
                }, {
                    value: "540229",
                    label: "仁布县"
                }, {
                    value: "540230",
                    label: "康马县"
                }, {
                    value: "540231",
                    label: "定结县"
                }, {
                    value: "540232",
                    label: "仲巴县"
                }, {
                    value: "540233",
                    label: "亚东县"
                }, {
                    value: "540234",
                    label: "吉隆县"
                }, {
                    value: "540235",
                    label: "聂拉木县"
                }, {
                    value: "540236",
                    label: "萨嘎县"
                }, {
                    value: "540237",
                    label: "岗巴县"
                } ]
            }, {
                value: "540300",
                label: "昌都",
                children: [ {
                    value: "540302",
                    label: "卡若区"
                }, {
                    value: "540321",
                    label: "江达县"
                }, {
                    value: "540322",
                    label: "贡觉县"
                }, {
                    value: "540323",
                    label: "类乌齐县"
                }, {
                    value: "540324",
                    label: "丁青县"
                }, {
                    value: "540325",
                    label: "察雅县"
                }, {
                    value: "540326",
                    label: "八宿县"
                }, {
                    value: "540327",
                    label: "左贡县"
                }, {
                    value: "540328",
                    label: "芒康县"
                }, {
                    value: "540329",
                    label: "洛隆县"
                }, {
                    value: "540330",
                    label: "边坝县"
                } ]
            }, {
                value: "540400",
                label: "林芝",
                children: [ {
                    value: "540402",
                    label: "巴宜区"
                }, {
                    value: "540421",
                    label: "工布江达县"
                }, {
                    value: "540422",
                    label: "米林县"
                }, {
                    value: "540423",
                    label: "墨脱县"
                }, {
                    value: "540424",
                    label: "波密县"
                }, {
                    value: "540425",
                    label: "察隅县"
                }, {
                    value: "540426",
                    label: "朗县"
                } ]
            }, {
                value: "540500",
                label: "山南",
                children: [ {
                    value: "540502",
                    label: "乃东区"
                }, {
                    value: "540521",
                    label: "扎囊县"
                }, {
                    value: "540522",
                    label: "贡嘎县"
                }, {
                    value: "540523",
                    label: "桑日县"
                }, {
                    value: "540524",
                    label: "琼结县"
                }, {
                    value: "540525",
                    label: "曲松县"
                }, {
                    value: "540526",
                    label: "措美县"
                }, {
                    value: "540527",
                    label: "洛扎县"
                }, {
                    value: "540528",
                    label: "加查县"
                }, {
                    value: "540529",
                    label: "隆子县"
                }, {
                    value: "540530",
                    label: "错那县"
                }, {
                    value: "540531",
                    label: "浪卡子县"
                } ]
            }, {
                value: "540600",
                label: "那曲",
                children: [ {
                    value: "540602",
                    label: "色尼区"
                }, {
                    value: "540621",
                    label: "嘉黎县"
                }, {
                    value: "540622",
                    label: "比如县"
                }, {
                    value: "540623",
                    label: "聂荣县"
                }, {
                    value: "540624",
                    label: "安多县"
                }, {
                    value: "540625",
                    label: "申扎县"
                }, {
                    value: "540626",
                    label: "索县"
                }, {
                    value: "540627",
                    label: "班戈县"
                }, {
                    value: "540628",
                    label: "巴青县"
                }, {
                    value: "540629",
                    label: "尼玛县"
                }, {
                    value: "540630",
                    label: "双湖县"
                } ]
            }, {
                value: "542500",
                label: "阿里",
                children: [ {
                    value: "542521",
                    label: "普兰县"
                }, {
                    value: "542522",
                    label: "札达县"
                }, {
                    value: "542523",
                    label: "噶尔县"
                }, {
                    value: "542524",
                    label: "日土县"
                }, {
                    value: "542525",
                    label: "革吉县"
                }, {
                    value: "542526",
                    label: "改则县"
                }, {
                    value: "542527",
                    label: "措勤县"
                } ]
            } ]
        }, {
            value: "610000",
            label: "陕西",
            children: [ {
                value: "610100",
                label: "西安",
                children: [ {
                    value: "610102",
                    label: "新城区"
                }, {
                    value: "610103",
                    label: "碑林区"
                }, {
                    value: "610104",
                    label: "莲湖区"
                }, {
                    value: "610111",
                    label: "灞桥区"
                }, {
                    value: "610112",
                    label: "未央区"
                }, {
                    value: "610113",
                    label: "雁塔区"
                }, {
                    value: "610114",
                    label: "阎良区"
                }, {
                    value: "610115",
                    label: "临潼区"
                }, {
                    value: "610116",
                    label: "长安区"
                }, {
                    value: "610117",
                    label: "高陵区"
                }, {
                    value: "610118",
                    label: "鄠邑区"
                }, {
                    value: "610122",
                    label: "蓝田县"
                }, {
                    value: "610124",
                    label: "周至县"
                } ]
            }, {
                value: "610200",
                label: "铜川",
                children: [ {
                    value: "610202",
                    label: "王益区"
                }, {
                    value: "610203",
                    label: "印台区"
                }, {
                    value: "610204",
                    label: "耀州区"
                }, {
                    value: "610222",
                    label: "宜君县"
                } ]
            }, {
                value: "610300",
                label: "宝鸡",
                children: [ {
                    value: "610302",
                    label: "渭滨区"
                }, {
                    value: "610303",
                    label: "金台区"
                }, {
                    value: "610304",
                    label: "陈仓区"
                }, {
                    value: "610322",
                    label: "凤翔县"
                }, {
                    value: "610323",
                    label: "岐山县"
                }, {
                    value: "610324",
                    label: "扶风县"
                }, {
                    value: "610326",
                    label: "眉县"
                }, {
                    value: "610327",
                    label: "陇县"
                }, {
                    value: "610328",
                    label: "千阳县"
                }, {
                    value: "610329",
                    label: "麟游县"
                }, {
                    value: "610330",
                    label: "凤县"
                }, {
                    value: "610331",
                    label: "太白县"
                } ]
            }, {
                value: "610400",
                label: "咸阳",
                children: [ {
                    value: "610402",
                    label: "秦都区"
                }, {
                    value: "610403",
                    label: "杨陵区"
                }, {
                    value: "610404",
                    label: "渭城区"
                }, {
                    value: "610422",
                    label: "三原县"
                }, {
                    value: "610423",
                    label: "泾阳县"
                }, {
                    value: "610424",
                    label: "乾县"
                }, {
                    value: "610425",
                    label: "礼泉县"
                }, {
                    value: "610426",
                    label: "永寿县"
                }, {
                    value: "610427",
                    label: "彬县"
                }, {
                    value: "610428",
                    label: "长武县"
                }, {
                    value: "610429",
                    label: "旬邑县"
                }, {
                    value: "610430",
                    label: "淳化县"
                }, {
                    value: "610431",
                    label: "武功县"
                }, {
                    value: "610481",
                    label: "兴平市"
                } ]
            }, {
                value: "610500",
                label: "渭南",
                children: [ {
                    value: "610502",
                    label: "临渭区"
                }, {
                    value: "610503",
                    label: "华州区"
                }, {
                    value: "610522",
                    label: "潼关县"
                }, {
                    value: "610523",
                    label: "大荔县"
                }, {
                    value: "610524",
                    label: "合阳县"
                }, {
                    value: "610525",
                    label: "澄城县"
                }, {
                    value: "610526",
                    label: "蒲城县"
                }, {
                    value: "610527",
                    label: "白水县"
                }, {
                    value: "610528",
                    label: "富平县"
                }, {
                    value: "610581",
                    label: "韩城市"
                }, {
                    value: "610582",
                    label: "华阴市"
                } ]
            }, {
                value: "610600",
                label: "延安",
                children: [ {
                    value: "610602",
                    label: "宝塔区"
                }, {
                    value: "610603",
                    label: "安塞区"
                }, {
                    value: "610621",
                    label: "延长县"
                }, {
                    value: "610622",
                    label: "延川县"
                }, {
                    value: "610623",
                    label: "子长县"
                }, {
                    value: "610625",
                    label: "志丹县"
                }, {
                    value: "610626",
                    label: "吴起县"
                }, {
                    value: "610627",
                    label: "甘泉县"
                }, {
                    value: "610628",
                    label: "富县"
                }, {
                    value: "610629",
                    label: "洛川县"
                }, {
                    value: "610630",
                    label: "宜川县"
                }, {
                    value: "610631",
                    label: "黄龙县"
                }, {
                    value: "610632",
                    label: "黄陵县"
                } ]
            }, {
                value: "610700",
                label: "汉中",
                children: [ {
                    value: "610702",
                    label: "汉台区"
                }, {
                    value: "610703",
                    label: "南郑区"
                }, {
                    value: "610722",
                    label: "城固县"
                }, {
                    value: "610723",
                    label: "洋县"
                }, {
                    value: "610724",
                    label: "西乡县"
                }, {
                    value: "610725",
                    label: "勉县"
                }, {
                    value: "610726",
                    label: "宁强县"
                }, {
                    value: "610727",
                    label: "略阳县"
                }, {
                    value: "610728",
                    label: "镇巴县"
                }, {
                    value: "610729",
                    label: "留坝县"
                }, {
                    value: "610730",
                    label: "佛坪县"
                } ]
            }, {
                value: "610800",
                label: "榆林",
                children: [ {
                    value: "610802",
                    label: "榆阳区"
                }, {
                    value: "610803",
                    label: "横山区"
                }, {
                    value: "610822",
                    label: "府谷县"
                }, {
                    value: "610824",
                    label: "靖边县"
                }, {
                    value: "610825",
                    label: "定边县"
                }, {
                    value: "610826",
                    label: "绥德县"
                }, {
                    value: "610827",
                    label: "米脂县"
                }, {
                    value: "610828",
                    label: "佳县"
                }, {
                    value: "610829",
                    label: "吴堡县"
                }, {
                    value: "610830",
                    label: "清涧县"
                }, {
                    value: "610831",
                    label: "子洲县"
                }, {
                    value: "610881",
                    label: "神木市"
                } ]
            }, {
                value: "610900",
                label: "安康",
                children: [ {
                    value: "610902",
                    label: "汉滨区"
                }, {
                    value: "610921",
                    label: "汉阴县"
                }, {
                    value: "610922",
                    label: "石泉县"
                }, {
                    value: "610923",
                    label: "宁陕县"
                }, {
                    value: "610924",
                    label: "紫阳县"
                }, {
                    value: "610925",
                    label: "岚皋县"
                }, {
                    value: "610926",
                    label: "平利县"
                }, {
                    value: "610927",
                    label: "镇坪县"
                }, {
                    value: "610928",
                    label: "旬阳县"
                }, {
                    value: "610929",
                    label: "白河县"
                } ]
            }, {
                value: "611000",
                label: "商洛",
                children: [ {
                    value: "611002",
                    label: "商州区"
                }, {
                    value: "611021",
                    label: "洛南县"
                }, {
                    value: "611022",
                    label: "丹凤县"
                }, {
                    value: "611023",
                    label: "商南县"
                }, {
                    value: "611024",
                    label: "山阳县"
                }, {
                    value: "611025",
                    label: "镇安县"
                }, {
                    value: "611026",
                    label: "柞水县"
                } ]
            } ]
        }, {
            value: "620000",
            label: "甘肃",
            children: [ {
                value: "620100",
                label: "兰州",
                children: [ {
                    value: "620102",
                    label: "城关区"
                }, {
                    value: "620103",
                    label: "七里河区"
                }, {
                    value: "620104",
                    label: "西固区"
                }, {
                    value: "620105",
                    label: "安宁区"
                }, {
                    value: "620111",
                    label: "红古区"
                }, {
                    value: "620121",
                    label: "永登县"
                }, {
                    value: "620122",
                    label: "皋兰县"
                }, {
                    value: "620123",
                    label: "榆中县"
                } ]
            }, {
                value: "620200",
                label: "嘉峪关",
                children: [ {
                    value: "620200",
                    label: "嘉峪关市"
                } ]
            }, {
                value: "620300",
                label: "金昌",
                children: [ {
                    value: "620302",
                    label: "金川区"
                }, {
                    value: "620321",
                    label: "永昌县"
                } ]
            }, {
                value: "620400",
                label: "白银",
                children: [ {
                    value: "620402",
                    label: "白银区"
                }, {
                    value: "620403",
                    label: "平川区"
                }, {
                    value: "620421",
                    label: "靖远县"
                }, {
                    value: "620422",
                    label: "会宁县"
                }, {
                    value: "620423",
                    label: "景泰县"
                } ]
            }, {
                value: "620500",
                label: "天水",
                children: [ {
                    value: "620502",
                    label: "秦州区"
                }, {
                    value: "620503",
                    label: "麦积区"
                }, {
                    value: "620521",
                    label: "清水县"
                }, {
                    value: "620522",
                    label: "秦安县"
                }, {
                    value: "620523",
                    label: "甘谷县"
                }, {
                    value: "620524",
                    label: "武山县"
                }, {
                    value: "620525",
                    label: "张家川回族自治县"
                } ]
            }, {
                value: "620600",
                label: "武威",
                children: [ {
                    value: "620602",
                    label: "凉州区"
                }, {
                    value: "620621",
                    label: "民勤县"
                }, {
                    value: "620622",
                    label: "古浪县"
                }, {
                    value: "620623",
                    label: "天祝藏族自治县"
                } ]
            }, {
                value: "620700",
                label: "张掖",
                children: [ {
                    value: "620702",
                    label: "甘州区"
                }, {
                    value: "620721",
                    label: "肃南裕固族自治县"
                }, {
                    value: "620722",
                    label: "民乐县"
                }, {
                    value: "620723",
                    label: "临泽县"
                }, {
                    value: "620724",
                    label: "高台县"
                }, {
                    value: "620725",
                    label: "山丹县"
                } ]
            }, {
                value: "620800",
                label: "平凉",
                children: [ {
                    value: "620802",
                    label: "崆峒区"
                }, {
                    value: "620821",
                    label: "泾川县"
                }, {
                    value: "620822",
                    label: "灵台县"
                }, {
                    value: "620823",
                    label: "崇信县"
                }, {
                    value: "620824",
                    label: "华亭县"
                }, {
                    value: "620825",
                    label: "庄浪县"
                }, {
                    value: "620826",
                    label: "静宁县"
                } ]
            }, {
                value: "620900",
                label: "酒泉",
                children: [ {
                    value: "620902",
                    label: "肃州区"
                }, {
                    value: "620921",
                    label: "金塔县"
                }, {
                    value: "620922",
                    label: "瓜州县"
                }, {
                    value: "620923",
                    label: "肃北蒙古族自治县"
                }, {
                    value: "620924",
                    label: "阿克塞哈萨克族自治县"
                }, {
                    value: "620981",
                    label: "玉门市"
                }, {
                    value: "620982",
                    label: "敦煌市"
                } ]
            }, {
                value: "621000",
                label: "庆阳",
                children: [ {
                    value: "621002",
                    label: "西峰区"
                }, {
                    value: "621021",
                    label: "庆城县"
                }, {
                    value: "621022",
                    label: "环县"
                }, {
                    value: "621023",
                    label: "华池县"
                }, {
                    value: "621024",
                    label: "合水县"
                }, {
                    value: "621025",
                    label: "正宁县"
                }, {
                    value: "621026",
                    label: "宁县"
                }, {
                    value: "621027",
                    label: "镇原县"
                } ]
            }, {
                value: "621100",
                label: "定西",
                children: [ {
                    value: "621102",
                    label: "安定区"
                }, {
                    value: "621121",
                    label: "通渭县"
                }, {
                    value: "621122",
                    label: "陇西县"
                }, {
                    value: "621123",
                    label: "渭源县"
                }, {
                    value: "621124",
                    label: "临洮县"
                }, {
                    value: "621125",
                    label: "漳县"
                }, {
                    value: "621126",
                    label: "岷县"
                } ]
            }, {
                value: "621200",
                label: "陇南",
                children: [ {
                    value: "621202",
                    label: "武都区"
                }, {
                    value: "621221",
                    label: "成县"
                }, {
                    value: "621222",
                    label: "文县"
                }, {
                    value: "621223",
                    label: "宕昌县"
                }, {
                    value: "621224",
                    label: "康县"
                }, {
                    value: "621225",
                    label: "西和县"
                }, {
                    value: "621226",
                    label: "礼县"
                }, {
                    value: "621227",
                    label: "徽县"
                }, {
                    value: "621228",
                    label: "两当县"
                } ]
            }, {
                value: "622900",
                label: "临夏",
                children: [ {
                    value: "622901",
                    label: "临夏市"
                }, {
                    value: "622921",
                    label: "临夏县"
                }, {
                    value: "622922",
                    label: "康乐县"
                }, {
                    value: "622923",
                    label: "永靖县"
                }, {
                    value: "622924",
                    label: "广河县"
                }, {
                    value: "622925",
                    label: "和政县"
                }, {
                    value: "622926",
                    label: "东乡族自治县"
                }, {
                    value: "622927",
                    label: "积石山保安族东乡族撒拉族自治县"
                } ]
            }, {
                value: "623000",
                label: "甘南",
                children: [ {
                    value: "623001",
                    label: "合作市"
                }, {
                    value: "623021",
                    label: "临潭县"
                }, {
                    value: "623022",
                    label: "卓尼县"
                }, {
                    value: "623023",
                    label: "舟曲县"
                }, {
                    value: "623024",
                    label: "迭部县"
                }, {
                    value: "623025",
                    label: "玛曲县"
                }, {
                    value: "623026",
                    label: "碌曲县"
                }, {
                    value: "623027",
                    label: "夏河县"
                } ]
            } ]
        }, {
            value: "630000",
            label: "青海",
            children: [ {
                value: "630100",
                label: "西宁",
                children: [ {
                    value: "630102",
                    label: "城东区"
                }, {
                    value: "630103",
                    label: "城中区"
                }, {
                    value: "630104",
                    label: "城西区"
                }, {
                    value: "630105",
                    label: "城北区"
                }, {
                    value: "630121",
                    label: "大通回族土族自治县"
                }, {
                    value: "630122",
                    label: "湟中县"
                }, {
                    value: "630123",
                    label: "湟源县"
                } ]
            }, {
                value: "630200",
                label: "海东",
                children: [ {
                    value: "630202",
                    label: "乐都区"
                }, {
                    value: "630203",
                    label: "平安区"
                }, {
                    value: "630222",
                    label: "民和回族土族自治县"
                }, {
                    value: "630223",
                    label: "互助土族自治县"
                }, {
                    value: "630224",
                    label: "化隆回族自治县"
                }, {
                    value: "630225",
                    label: "循化撒拉族自治县"
                } ]
            }, {
                value: "632200",
                label: "海北",
                children: [ {
                    value: "632221",
                    label: "门源回族自治县"
                }, {
                    value: "632222",
                    label: "祁连县"
                }, {
                    value: "632223",
                    label: "海晏县"
                }, {
                    value: "632224",
                    label: "刚察县"
                } ]
            }, {
                value: "632300",
                label: "黄南",
                children: [ {
                    value: "632321",
                    label: "同仁县"
                }, {
                    value: "632322",
                    label: "尖扎县"
                }, {
                    value: "632323",
                    label: "泽库县"
                }, {
                    value: "632324",
                    label: "河南蒙古族自治县"
                } ]
            }, {
                value: "632500",
                label: "海南",
                children: [ {
                    value: "632521",
                    label: "共和县"
                }, {
                    value: "632522",
                    label: "同德县"
                }, {
                    value: "632523",
                    label: "贵德县"
                }, {
                    value: "632524",
                    label: "兴海县"
                }, {
                    value: "632525",
                    label: "贵南县"
                } ]
            }, {
                value: "632600",
                label: "果洛",
                children: [ {
                    value: "632621",
                    label: "玛沁县"
                }, {
                    value: "632622",
                    label: "班玛县"
                }, {
                    value: "632623",
                    label: "甘德县"
                }, {
                    value: "632624",
                    label: "达日县"
                }, {
                    value: "632625",
                    label: "久治县"
                }, {
                    value: "632626",
                    label: "玛多县"
                } ]
            }, {
                value: "632700",
                label: "玉树",
                children: [ {
                    value: "632701",
                    label: "玉树市"
                }, {
                    value: "632722",
                    label: "杂多县"
                }, {
                    value: "632723",
                    label: "称多县"
                }, {
                    value: "632724",
                    label: "治多县"
                }, {
                    value: "632725",
                    label: "囊谦县"
                }, {
                    value: "632726",
                    label: "曲麻莱县"
                } ]
            }, {
                value: "632800",
                label: "海西",
                children: [ {
                    value: "632801",
                    label: "格尔木市"
                }, {
                    value: "632802",
                    label: "德令哈市"
                }, {
                    value: "632821",
                    label: "乌兰县"
                }, {
                    value: "632822",
                    label: "都兰县"
                }, {
                    value: "632823",
                    label: "天峻县"
                } ]
            } ]
        }, {
            value: "640000",
            label: "宁夏",
            children: [ {
                value: "640100",
                label: "银川市",
                children: [ {
                    value: "640104",
                    label: "兴庆区"
                }, {
                    value: "640105",
                    label: "西夏区"
                }, {
                    value: "640106",
                    label: "金凤区"
                }, {
                    value: "640121",
                    label: "永宁县"
                }, {
                    value: "640122",
                    label: "贺兰县"
                }, {
                    value: "640181",
                    label: "灵武市"
                } ]
            }, {
                value: "640200",
                label: "石嘴山市",
                children: [ {
                    value: "640202",
                    label: "大武口区"
                }, {
                    value: "640205",
                    label: "惠农区"
                }, {
                    value: "640221",
                    label: "平罗县"
                } ]
            }, {
                value: "640300",
                label: "吴忠市",
                children: [ {
                    value: "640302",
                    label: "利通区"
                }, {
                    value: "640303",
                    label: "红寺堡区"
                }, {
                    value: "640323",
                    label: "盐池县"
                }, {
                    value: "640324",
                    label: "同心县"
                }, {
                    value: "640381",
                    label: "青铜峡市"
                } ]
            }, {
                value: "640400",
                label: "固原市",
                children: [ {
                    value: "640402",
                    label: "原州区"
                }, {
                    value: "640422",
                    label: "西吉县"
                }, {
                    value: "640423",
                    label: "隆德县"
                }, {
                    value: "640424",
                    label: "泾源县"
                }, {
                    value: "640425",
                    label: "彭阳县"
                } ]
            }, {
                value: "640500",
                label: "中卫市",
                children: [ {
                    value: "640502",
                    label: "沙坡头区"
                }, {
                    value: "640521",
                    label: "中宁县"
                }, {
                    value: "640522",
                    label: "海原县"
                } ]
            } ]
        }, {
            value: "650000",
            label: "新疆",
            children: [ {
                value: "650100",
                label: "乌鲁木齐",
                children: [ {
                    value: "650102",
                    label: "天山区"
                }, {
                    value: "650103",
                    label: "沙依巴克区"
                }, {
                    value: "650104",
                    label: "新市区"
                }, {
                    value: "650105",
                    label: "水磨沟区"
                }, {
                    value: "650106",
                    label: "头屯河区"
                }, {
                    value: "650107",
                    label: "达坂城区"
                }, {
                    value: "650109",
                    label: "米东区"
                }, {
                    value: "650121",
                    label: "乌鲁木齐县"
                } ]
            }, {
                value: "650200",
                label: "克拉玛依",
                children: [ {
                    value: "650202",
                    label: "独山子区"
                }, {
                    value: "650203",
                    label: "克拉玛依区"
                }, {
                    value: "650204",
                    label: "白碱滩区"
                }, {
                    value: "650205",
                    label: "乌尔禾区"
                } ]
            }, {
                value: "650400",
                label: "吐鲁番",
                children: [ {
                    value: "650402",
                    label: "高昌区"
                }, {
                    value: "650421",
                    label: "鄯善县"
                }, {
                    value: "650422",
                    label: "托克逊县"
                } ]
            }, {
                value: "650500",
                label: "哈密",
                children: [ {
                    value: "650502",
                    label: "伊州区"
                }, {
                    value: "650521",
                    label: "巴里坤哈萨克自治县"
                }, {
                    value: "650522",
                    label: "伊吾县"
                } ]
            }, {
                value: "652300",
                label: "昌吉",
                children: [ {
                    value: "652301",
                    label: "昌吉市"
                }, {
                    value: "652302",
                    label: "阜康市"
                }, {
                    value: "652323",
                    label: "呼图壁县"
                }, {
                    value: "652324",
                    label: "玛纳斯县"
                }, {
                    value: "652325",
                    label: "奇台县"
                }, {
                    value: "652327",
                    label: "吉木萨尔县"
                }, {
                    value: "652328",
                    label: "木垒哈萨克自治县"
                } ]
            }, {
                value: "652700",
                label: "博尔塔拉蒙",
                children: [ {
                    value: "652701",
                    label: "博乐市"
                }, {
                    value: "652702",
                    label: "阿拉山口市"
                }, {
                    value: "652722",
                    label: "精河县"
                }, {
                    value: "652723",
                    label: "温泉县"
                } ]
            }, {
                value: "652800",
                label: "巴音",
                children: [ {
                    value: "652801",
                    label: "库尔勒市"
                }, {
                    value: "652822",
                    label: "轮台县"
                }, {
                    value: "652823",
                    label: "尉犁县"
                }, {
                    value: "652824",
                    label: "若羌县"
                }, {
                    value: "652825",
                    label: "且末县"
                }, {
                    value: "652826",
                    label: "焉耆回族自治县"
                }, {
                    value: "652827",
                    label: "和静县"
                }, {
                    value: "652828",
                    label: "和硕县"
                }, {
                    value: "652829",
                    label: "博湖县"
                } ]
            }, {
                value: "652900",
                label: "阿克苏",
                children: [ {
                    value: "652901",
                    label: "阿克苏市"
                }, {
                    value: "652922",
                    label: "温宿县"
                }, {
                    value: "652923",
                    label: "库车县"
                }, {
                    value: "652924",
                    label: "沙雅县"
                }, {
                    value: "652925",
                    label: "新和县"
                }, {
                    value: "652926",
                    label: "拜城县"
                }, {
                    value: "652927",
                    label: "乌什县"
                }, {
                    value: "652928",
                    label: "阿瓦提县"
                }, {
                    value: "652929",
                    label: "柯坪县"
                } ]
            }, {
                value: "653000",
                label: "克孜勒",
                children: [ {
                    value: "653001",
                    label: "阿图什市"
                }, {
                    value: "653022",
                    label: "阿克陶县"
                }, {
                    value: "653023",
                    label: "阿合奇县"
                }, {
                    value: "653024",
                    label: "乌恰县"
                } ]
            }, {
                value: "653100",
                label: "喀什",
                children: [ {
                    value: "653101",
                    label: "喀什市"
                }, {
                    value: "653121",
                    label: "疏附县"
                }, {
                    value: "653122",
                    label: "疏勒县"
                }, {
                    value: "653123",
                    label: "英吉沙县"
                }, {
                    value: "653124",
                    label: "泽普县"
                }, {
                    value: "653125",
                    label: "莎车县"
                }, {
                    value: "653126",
                    label: "叶城县"
                }, {
                    value: "653127",
                    label: "麦盖提县"
                }, {
                    value: "653128",
                    label: "岳普湖县"
                }, {
                    value: "653129",
                    label: "伽师县"
                }, {
                    value: "653130",
                    label: "巴楚县"
                }, {
                    value: "653131",
                    label: "塔什库尔干塔吉克自治县"
                } ]
            }, {
                value: "653200",
                label: "和田",
                children: [ {
                    value: "653201",
                    label: "和田市"
                }, {
                    value: "653221",
                    label: "和田县"
                }, {
                    value: "653222",
                    label: "墨玉县"
                }, {
                    value: "653223",
                    label: "皮山县"
                }, {
                    value: "653224",
                    label: "洛浦县"
                }, {
                    value: "653225",
                    label: "策勒县"
                }, {
                    value: "653226",
                    label: "于田县"
                }, {
                    value: "653227",
                    label: "民丰县"
                } ]
            }, {
                value: "654000",
                label: "伊犁",
                children: [ {
                    value: "654002",
                    label: "伊宁市"
                }, {
                    value: "654003",
                    label: "奎屯市"
                }, {
                    value: "654004",
                    label: "霍尔果斯市"
                }, {
                    value: "654021",
                    label: "伊宁县"
                }, {
                    value: "654022",
                    label: "察布查尔锡伯自治县"
                }, {
                    value: "654023",
                    label: "霍城县"
                }, {
                    value: "654024",
                    label: "巩留县"
                }, {
                    value: "654025",
                    label: "新源县"
                }, {
                    value: "654026",
                    label: "昭苏县"
                }, {
                    value: "654027",
                    label: "特克斯县"
                }, {
                    value: "654028",
                    label: "尼勒克县"
                } ]
            }, {
                value: "654200",
                label: "塔城",
                children: [ {
                    value: "654201",
                    label: "塔城市"
                }, {
                    value: "654202",
                    label: "乌苏市"
                }, {
                    value: "654221",
                    label: "额敏县"
                }, {
                    value: "654223",
                    label: "沙湾县"
                }, {
                    value: "654224",
                    label: "托里县"
                }, {
                    value: "654225",
                    label: "裕民县"
                }, {
                    value: "654226",
                    label: "和布克赛尔蒙古自治县"
                } ]
            }, {
                value: "654300",
                label: "阿勒泰",
                children: [ {
                    value: "654301",
                    label: "阿勒泰市"
                }, {
                    value: "654321",
                    label: "布尔津县"
                }, {
                    value: "654322",
                    label: "富蕴县"
                }, {
                    value: "654323",
                    label: "福海县"
                }, {
                    value: "654324",
                    label: "哈巴河县"
                }, {
                    value: "654325",
                    label: "青河县"
                }, {
                    value: "654326",
                    label: "吉木乃县"
                } ]
            }, {
                value: "659001",
                label: "石河子",
                children: [ {
                    value: "659001",
                    label: "石河子市"
                } ]
            }, {
                value: "659002",
                label: "阿拉尔",
                children: [ {
                    value: "659002",
                    label: "阿拉尔市"
                } ]
            }, {
                value: "659003",
                label: "图木舒克",
                children: [ {
                    value: "659003",
                    label: "图木舒克市"
                } ]
            }, {
                value: "659004",
                label: "五家渠",
                children: [ {
                    value: "659004",
                    label: "五家渠市"
                } ]
            }, {
                value: "659005",
                label: "北屯",
                children: [ {
                    value: "659005",
                    label: "北屯市"
                } ]
            }, {
                value: "659006",
                label: "铁门关",
                children: [ {
                    value: "659006",
                    label: "铁门关市"
                } ]
            }, {
                value: "659007",
                label: "双河",
                children: [ {
                    value: "659007",
                    label: "双河市"
                } ]
            }, {
                value: "659008",
                label: "可克达拉",
                children: [ {
                    value: "659008",
                    label: "可克达拉市"
                } ]
            }, {
                value: "659009",
                label: "昆玉",
                children: [ {
                    value: "659009",
                    label: "昆玉市"
                } ]
            } ]
        }, {
            value: "710000",
            label: "台湾",
            children: [ {
                value: "710100",
                label: "台北",
                children: [ {
                    value: "710101",
                    label: "中正区"
                }, {
                    value: "710102",
                    label: "大同区"
                }, {
                    value: "710103",
                    label: "中山区"
                }, {
                    value: "710104",
                    label: "万华区"
                }, {
                    value: "710105",
                    label: "信义区"
                }, {
                    value: "710106",
                    label: "松山区"
                }, {
                    value: "710107",
                    label: "大安区"
                }, {
                    value: "710108",
                    label: "南港区"
                }, {
                    value: "710109",
                    label: "北投区"
                }, {
                    value: "710110",
                    label: "内湖区"
                }, {
                    value: "710111",
                    label: "士林区"
                }, {
                    value: "710112",
                    label: "文山区"
                } ]
            }, {
                value: "710200",
                label: "新北",
                children: [ {
                    value: "710201",
                    label: "板桥区"
                }, {
                    value: "710202",
                    label: "土城区"
                }, {
                    value: "710203",
                    label: "新庄区"
                }, {
                    value: "710204",
                    label: "新店区"
                }, {
                    value: "710205",
                    label: "深坑区"
                }, {
                    value: "710206",
                    label: "石碇区"
                }, {
                    value: "710207",
                    label: "坪林区"
                }, {
                    value: "710208",
                    label: "乌来区"
                }, {
                    value: "710209",
                    label: "五股区"
                }, {
                    value: "710210",
                    label: "八里区"
                }, {
                    value: "710211",
                    label: "林口区"
                }, {
                    value: "710212",
                    label: "淡水区"
                }, {
                    value: "710213",
                    label: "中和区"
                }, {
                    value: "710214",
                    label: "永和区"
                }, {
                    value: "710215",
                    label: "三重区"
                }, {
                    value: "710216",
                    label: "芦洲区"
                }, {
                    value: "710217",
                    label: "泰山区"
                }, {
                    value: "710218",
                    label: "树林区"
                }, {
                    value: "710219",
                    label: "莺歌区"
                }, {
                    value: "710220",
                    label: "三峡区"
                }, {
                    value: "710221",
                    label: "汐止区"
                }, {
                    value: "710222",
                    label: "金山区"
                }, {
                    value: "710223",
                    label: "万里区"
                }, {
                    value: "710224",
                    label: "三芝区"
                }, {
                    value: "710225",
                    label: "石门区"
                }, {
                    value: "710226",
                    label: "瑞芳区"
                }, {
                    value: "710227",
                    label: "贡寮区"
                }, {
                    value: "710228",
                    label: "双溪区"
                }, {
                    value: "710229",
                    label: "平溪区"
                } ]
            }, {
                value: "710300",
                label: "桃园",
                children: [ {
                    value: "710301",
                    label: "桃园区"
                }, {
                    value: "710302",
                    label: "中坜区"
                }, {
                    value: "710303",
                    label: "平镇区"
                }, {
                    value: "710304",
                    label: "八德区"
                }, {
                    value: "710305",
                    label: "杨梅区"
                }, {
                    value: "710306",
                    label: "芦竹区"
                }, {
                    value: "710307",
                    label: "大溪区"
                }, {
                    value: "710308",
                    label: "龙潭区"
                }, {
                    value: "710309",
                    label: "龟山区"
                }, {
                    value: "710310",
                    label: "大园区"
                }, {
                    value: "710311",
                    label: "观音区"
                }, {
                    value: "710312",
                    label: "新屋区"
                }, {
                    value: "710313",
                    label: "复兴区"
                } ]
            }, {
                value: "710400",
                label: "台中",
                children: [ {
                    value: "710401",
                    label: "中区"
                }, {
                    value: "710402",
                    label: "东区"
                }, {
                    value: "710403",
                    label: "西区"
                }, {
                    value: "710404",
                    label: "南区"
                }, {
                    value: "710405",
                    label: "北区"
                }, {
                    value: "710406",
                    label: "西屯区"
                }, {
                    value: "710407",
                    label: "南屯区"
                }, {
                    value: "710408",
                    label: "北屯区"
                }, {
                    value: "710409",
                    label: "丰原区"
                }, {
                    value: "710410",
                    label: "大里区"
                }, {
                    value: "710411",
                    label: "太平区"
                }, {
                    value: "710412",
                    label: "东势区"
                }, {
                    value: "710413",
                    label: "大甲区"
                }, {
                    value: "710414",
                    label: "清水区"
                }, {
                    value: "710415",
                    label: "沙鹿区"
                }, {
                    value: "710416",
                    label: "梧栖区"
                }, {
                    value: "710417",
                    label: "后里区"
                }, {
                    value: "710418",
                    label: "神冈区"
                }, {
                    value: "710419",
                    label: "潭子区"
                }, {
                    value: "710420",
                    label: "大雅区"
                }, {
                    value: "710421",
                    label: "新小区"
                }, {
                    value: "710422",
                    label: "石冈区"
                }, {
                    value: "710423",
                    label: "外埔区"
                }, {
                    value: "710424",
                    label: "大安区"
                }, {
                    value: "710425",
                    label: "乌日区"
                }, {
                    value: "710426",
                    label: "大肚区"
                }, {
                    value: "710427",
                    label: "龙井区"
                }, {
                    value: "710428",
                    label: "雾峰区"
                }, {
                    value: "710429",
                    label: "和平区"
                } ]
            }, {
                value: "710500",
                label: "台南",
                children: [ {
                    value: "710501",
                    label: "中西区"
                }, {
                    value: "710502",
                    label: "东区"
                }, {
                    value: "710503",
                    label: "南区"
                }, {
                    value: "710504",
                    label: "北区"
                }, {
                    value: "710505",
                    label: "安平区"
                }, {
                    value: "710506",
                    label: "安南区"
                }, {
                    value: "710507",
                    label: "永康区"
                }, {
                    value: "710508",
                    label: "归仁区"
                }, {
                    value: "710509",
                    label: "新化区"
                }, {
                    value: "710510",
                    label: "左镇区"
                }, {
                    value: "710511",
                    label: "玉井区"
                }, {
                    value: "710512",
                    label: "楠西区"
                }, {
                    value: "710513",
                    label: "南化区"
                }, {
                    value: "710514",
                    label: "仁德区"
                }, {
                    value: "710515",
                    label: "关庙区"
                }, {
                    value: "710516",
                    label: "龙崎区"
                }, {
                    value: "710517",
                    label: "官田区"
                }, {
                    value: "710518",
                    label: "麻豆区"
                }, {
                    value: "710519",
                    label: "佳里区"
                }, {
                    value: "710520",
                    label: "西港区"
                }, {
                    value: "710521",
                    label: "七股区"
                }, {
                    value: "710522",
                    label: "将军区"
                }, {
                    value: "710523",
                    label: "学甲区"
                }, {
                    value: "710524",
                    label: "北门区"
                }, {
                    value: "710525",
                    label: "新营区"
                }, {
                    value: "710526",
                    label: "后壁区"
                }, {
                    value: "710527",
                    label: "白河区"
                }, {
                    value: "710528",
                    label: "东山区"
                }, {
                    value: "710529",
                    label: "六甲区"
                }, {
                    value: "710530",
                    label: "下营区"
                }, {
                    value: "710531",
                    label: "柳营区"
                }, {
                    value: "710532",
                    label: "盐水区"
                }, {
                    value: "710533",
                    label: "善化区"
                }, {
                    value: "710534",
                    label: "大内区"
                }, {
                    value: "710535",
                    label: "山上区"
                }, {
                    value: "710536",
                    label: "新市区"
                }, {
                    value: "710537",
                    label: "安定区"
                } ]
            }, {
                value: "710600",
                label: "高雄",
                children: [ {
                    value: "710601",
                    label: "楠梓区"
                }, {
                    value: "710602",
                    label: "左营区"
                }, {
                    value: "710603",
                    label: "鼓山区"
                }, {
                    value: "710604",
                    label: "三民区"
                }, {
                    value: "710605",
                    label: "盐埕区"
                }, {
                    value: "710606",
                    label: "前金区"
                }, {
                    value: "710607",
                    label: "新兴区"
                }, {
                    value: "710608",
                    label: "苓雅区"
                }, {
                    value: "710609",
                    label: "前镇区"
                }, {
                    value: "710610",
                    label: "旗津区"
                }, {
                    value: "710611",
                    label: "小港区"
                }, {
                    value: "710612",
                    label: "凤山区"
                }, {
                    value: "710613",
                    label: "大寮区"
                }, {
                    value: "710614",
                    label: "鸟松区"
                }, {
                    value: "710615",
                    label: "林园区"
                }, {
                    value: "710616",
                    label: "仁武区"
                }, {
                    value: "710617",
                    label: "大树区"
                }, {
                    value: "710618",
                    label: "大社区"
                }, {
                    value: "710619",
                    label: "冈山区"
                }, {
                    value: "710620",
                    label: "路竹区"
                }, {
                    value: "710621",
                    label: "桥头区"
                }, {
                    value: "710622",
                    label: "梓官区"
                }, {
                    value: "710623",
                    label: "弥陀区"
                }, {
                    value: "710624",
                    label: "永安区"
                }, {
                    value: "710625",
                    label: "燕巢区"
                }, {
                    value: "710626",
                    label: "阿莲区"
                }, {
                    value: "710627",
                    label: "茄萣区"
                }, {
                    value: "710628",
                    label: "湖内区"
                }, {
                    value: "710629",
                    label: "旗山区"
                }, {
                    value: "710630",
                    label: "美浓区"
                }, {
                    value: "710631",
                    label: "内门区"
                }, {
                    value: "710632",
                    label: "杉林区"
                }, {
                    value: "710633",
                    label: "甲仙区"
                }, {
                    value: "710634",
                    label: "六龟区"
                }, {
                    value: "710635",
                    label: "茂林区"
                }, {
                    value: "710636",
                    label: "桃源区"
                }, {
                    value: "710637",
                    label: "那玛夏区"
                } ]
            }, {
                value: "710700",
                label: "基隆",
                children: [ {
                    value: "710701",
                    label: "中正区"
                }, {
                    value: "710702",
                    label: "七堵区"
                }, {
                    value: "710703",
                    label: "暖暖区"
                }, {
                    value: "710704",
                    label: "仁爱区"
                }, {
                    value: "710705",
                    label: "中山区"
                }, {
                    value: "710706",
                    label: "安乐区"
                }, {
                    value: "710707",
                    label: "信义区"
                } ]
            }, {
                value: "710800",
                label: "新竹",
                children: [ {
                    value: "710801",
                    label: "东区"
                }, {
                    value: "710802",
                    label: "北区"
                }, {
                    value: "710803",
                    label: "香山区"
                } ]
            }, {
                value: "710900",
                label: "嘉义",
                children: [ {
                    value: "710901",
                    label: "东区"
                }, {
                    value: "710902",
                    label: "西区"
                } ]
            }, {
                value: "719001",
                label: "宜兰",
                children: [ {
                    value: "719001",
                    label: "宜兰县"
                } ]
            }, {
                value: "719002",
                label: "新竹",
                children: [ {
                    value: "719002",
                    label: "新竹县"
                } ]
            }, {
                value: "719003",
                label: "苗栗",
                children: [ {
                    value: "719003",
                    label: "苗栗县"
                } ]
            }, {
                value: "719004",
                label: "彰化",
                children: [ {
                    value: "719004",
                    label: "彰化县"
                } ]
            }, {
                value: "719005",
                label: "南投",
                children: [ {
                    value: "719005",
                    label: "南投县"
                } ]
            }, {
                value: "719006",
                label: "嘉义",
                children: [ {
                    value: "719006",
                    label: "嘉义县"
                } ]
            }, {
                value: "719007",
                label: "云林",
                children: [ {
                    value: "719007",
                    label: "云林县"
                } ]
            }, {
                value: "719008",
                label: "屏东",
                children: [ {
                    value: "719008",
                    label: "屏东县"
                } ]
            }, {
                value: "719009",
                label: "台东",
                children: [ {
                    value: "719009",
                    label: "台东县"
                } ]
            }, {
                value: "719010",
                label: "花莲",
                children: [ {
                    value: "719010",
                    label: "花莲县"
                } ]
            }, {
                value: "719011",
                label: "澎湖",
                children: [ {
                    value: "719011",
                    label: "澎湖县"
                } ]
            }, {
                value: "719012",
                label: "金门",
                children: [ {
                    value: "719012",
                    label: "金门县"
                } ]
            }, {
                value: "719013",
                label: "连江",
                children: [ {
                    value: "719013",
                    label: "连江县"
                } ]
            } ]
        }, {
            value: "810000",
            label: "香港",
            children: [ {
                value: "810101",
                label: "中西区",
                children: [ {
                    value: "810101",
                    label: "中西区"
                } ]
            }, {
                value: "810102",
                label: "湾仔",
                children: [ {
                    value: "810102",
                    label: "湾仔区"
                } ]
            }, {
                value: "810103",
                label: "东区",
                children: [ {
                    value: "810103",
                    label: "东区"
                } ]
            }, {
                value: "810104",
                label: "南区",
                children: [ {
                    value: "810104",
                    label: "南区"
                } ]
            }, {
                value: "810105",
                label: "油尖旺区",
                children: [ {
                    value: "810105",
                    label: "油尖旺区"
                } ]
            }, {
                value: "810106",
                label: "深水埗区",
                children: [ {
                    value: "810106",
                    label: "深水埗区"
                } ]
            }, {
                value: "810107",
                label: "九龙城区",
                children: [ {
                    value: "810107",
                    label: "九龙城区"
                } ]
            }, {
                value: "810108",
                label: "黄大仙区",
                children: [ {
                    value: "810108",
                    label: "黄大仙区"
                } ]
            }, {
                value: "810109",
                label: "观塘区",
                children: [ {
                    value: "810109",
                    label: "观塘区"
                } ]
            }, {
                value: "810110",
                label: "北区",
                children: [ {
                    value: "810110",
                    label: "北区"
                } ]
            }, {
                value: "810111",
                label: "大埔区",
                children: [ {
                    value: "810111",
                    label: "大埔区"
                } ]
            }, {
                value: "810112",
                label: "沙田区",
                children: [ {
                    value: "810112",
                    label: "沙田区"
                } ]
            }, {
                value: "810113",
                label: "西贡区",
                children: [ {
                    value: "810113",
                    label: "西贡区"
                } ]
            }, {
                value: "810114",
                label: "荃湾区",
                children: [ {
                    value: "810114",
                    label: "荃湾区"
                } ]
            }, {
                value: "810115",
                label: "屯门区",
                children: [ {
                    value: "810115",
                    label: "屯门区"
                } ]
            }, {
                value: "810116",
                label: "元朗区",
                children: [ {
                    value: "810116",
                    label: "元朗区"
                } ]
            }, {
                value: "810117",
                label: "葵青区",
                children: [ {
                    value: "810117",
                    label: "葵青区"
                } ]
            }, {
                value: "810118",
                label: "离岛区",
                children: [ {
                    value: "810118",
                    label: "离岛区"
                } ]
            } ]
        }, {
            value: "820000",
            label: "澳门",
            children: [ {
                value: "820101",
                label: "花地玛堂区",
                children: [ {
                    value: "820101",
                    label: "花地玛堂区"
                } ]
            }, {
                value: "820102",
                label: "圣安多尼堂区",
                children: [ {
                    value: "820102",
                    label: "圣安多尼堂区"
                } ]
            }, {
                value: "820103",
                label: "大堂区",
                children: [ {
                    value: "820103",
                    label: "大堂区"
                } ]
            }, {
                value: "820104",
                label: "望德堂区",
                children: [ {
                    value: "820104",
                    label: "望德堂区"
                } ]
            }, {
                value: "820105",
                label: "风顺堂区",
                children: [ {
                    value: "820105",
                    label: "风顺堂区"
                } ]
            }, {
                value: "820106",
                label: "嘉模堂区",
                children: [ {
                    value: "820106",
                    label: "嘉模堂区"
                } ]
            }, {
                value: "820107",
                label: "圣方济各堂区",
                children: [ {
                    value: "820107",
                    label: "圣方济各堂区"
                } ]
            }, {
                value: "820108",
                label: "路氹城",
                children: [ {
                    value: "820108",
                    label: "路氹城"
                } ]
            }, {
                value: "820109",
                label: "澳门新城",
                children: [ {
                    value: "820109",
                    label: "澳门新城"
                } ]
            } ]
        } ];
    },
    b144: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function(e, l) {
            if (e >= 0 && l > 0 && l >= e) {
                var a = l - e + 1;
                return Math.floor(Math.random() * a + e);
            }
            return 0;
        };
    },
    b17c: function(e, l, a) {
        var u = a("4a4b"), t = a("6f8f");
        function n(l, a, r) {
            return t() ? (e.exports = n = Reflect.construct.bind(), e.exports.__esModule = !0, 
            e.exports.default = e.exports) : (e.exports = n = function(e, l, a) {
                var t = [ null ];
                t.push.apply(t, l);
                var n = Function.bind.apply(e, t), r = new n();
                return a && u(r, a.prototype), r;
            }, e.exports.__esModule = !0, e.exports.default = e.exports), n.apply(null, arguments);
        }
        e.exports = n, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    b1e2: function(e, l) {
        e.exports = {
            index: {
                defaultindex: "default/index",
                boxList: "default/boxList",
                wenzhang: "article/detail",
                addQueue: "box/addQueue",
                cancelQueue: "box/cancelQueue",
                queueCount: "box/queueCount"
            },
            wuxian: {
                newSubmit: "wuxian/newSubmit",
                boxDetail: "wuxian/boxDetail",
                submitPreview: "wuxian/submitPreview",
                boxList: "wuxian/boxList",
                winningRecord: "wuxian/winningRecord",
                prizeProbability: "wuxian/prizeProbability",
                winningRecordDetail: "wuxian/winningRecordDetail",
                onlineAdd: "wuxian/onlineAdd",
                onlineCount: "wuxian/onlineCount",
                wuxianling: "wuxian/wuxianling",
                lingReceive: "wuxian/lingReceive"
            },
            user: {
                getYzm: "sms/mobileSend",
                registerLogin: "passport/registerLogin",
                kefu: "user/kefu",
                wechatLogin: "passport/doLogin",
                index: "user/index",
                wallet: "user/wallet",
                moneyrecord: "user/money-record",
                recordList: "integral/recordList",
                submitOrder: "recharge/submitOrder",
                getList: "collection/getList",
                del: "collection/del",
                addressList: "address/index",
                addAddress: "address/edit",
                delAddress: "address/del",
                cityList: "default/districtList",
                payRecordList: "user/payRecordList",
                qunImg: "user/qunImg",
                minMobile: "passport/minMobile",
                editInfo: "user/editInfo",
                uploadfile: "upload/file",
                vipIndex: "user/vipIndex",
                vipReceive: "user/vipReceive",
                vipPaySubmit: "user/vipPaySubmit"
            },
            blindBox: {
                index: "box/indexV2",
                suitList: "box/suitList",
                suitPage: "box/suitPage",
                playExplain: "box/playExplain",
                winningRecord: "box/winningRecord",
                orderPreview: "box/orderPreviewV2",
                orderSubmit: "box/orderSubmitV2",
                payOrderSu: "box/payOrderSu",
                add: "collection/add",
                onlineCount: "box/onlineCount",
                onlineAdd: "box/onlineAdd"
            },
            order: {
                orderList: "box-order/orderList",
                sendOrder: "box-order/sendPreview",
                sendOrderSubmit: "box-order/sendSubmit",
                sendOrderPayData: "box-order/sendOrderPayData",
                exchangeRecord: "box-order/exchangeRecord",
                sendList: "box-order/sendList",
                sendYun: "box-order/sendYun",
                recoveryPreview: "box-order/recoveryPreview",
                exchange: "order-box/exchange",
                exchangePreview: "order-box/exchangePreview",
                integralOrderList: "order-box/integralOrderList",
                sellPreview: "order-box/sellPreview",
                sellSubmit: "order-box/sellSubmit",
                orderExchangeList: "box-order/exchangeRecord",
                lockList: "box-order/lockList",
                lockAdd: "box-order/lockAdd",
                lockRemove: "box-order/lockRemove"
            },
            market: {
                goodsList: "market/goodsList",
                myGoodsList: "market/myGoodsList",
                cancel: "market/cancel",
                orderPreview: "market/orderPreview",
                orderSubmit: "market/orderSubmit",
                buyOrderList: "market/buyOrderList",
                selloutList: "market/selloutList"
            },
            coupon: {
                cardList: "card/cardList",
                openCard: "card/openCard",
                detail: "card/detail",
                sharecard: "card/sharecard",
                synthesis: "card/synthesis"
            }
        };
    },
    b5d5: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("7037"));
        function n(e) {
            switch ((0, t.default)(e)) {
              case "undefined":
                return !0;

              case "string":
                if (0 == e.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, "").length) return !0;
                break;

              case "boolean":
                if (!e) return !0;
                break;

              case "number":
                if (0 === e || isNaN(e)) return !0;
                break;

              case "object":
                if (null === e || 0 === e.length) return !0;
                for (var l in e) return !1;
                return !0;
            }
            return !1;
        }
        var r = {
            email: function(e) {
                return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(e);
            },
            mobile: function(e) {
                return /^1[23456789]\d{9}$/.test(e);
            },
            url: function(e) {
                return /^((https|http|ftp|rtsp|mms):\/\/)(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z].[a-zA-Z]{2,6})(:[0-9]{1,4})?((\/?)|(\/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+\/?)$/.test(e);
            },
            date: function(e) {
                return !/Invalid|NaN/.test(new Date(e).toString());
            },
            dateISO: function(e) {
                return /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(e);
            },
            number: function(e) {
                return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(e);
            },
            digits: function(e) {
                return /^\d+$/.test(e);
            },
            idCard: function(e) {
                return /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(e);
            },
            carNo: function(e) {
                return 7 === e.length ? /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}$/.test(e) : 8 === e.length && /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF]$)|([DF][A-HJ-NP-Z0-9][0-9]{4}$))/.test(e);
            },
            amount: function(e) {
                return /^[1-9]\d*(,\d{3})*(\.\d{1,2})?$|^0\.\d{1,2}$/.test(e);
            },
            chinese: function(e) {
                return /^[\u4e00-\u9fa5]+$/gi.test(e);
            },
            letter: function(e) {
                return /^[a-zA-Z]*$/.test(e);
            },
            enOrNum: function(e) {
                return /^[0-9a-zA-Z]*$/g.test(e);
            },
            contains: function(e, l) {
                return e.indexOf(l) >= 0;
            },
            range: function(e, l) {
                return e >= l[0] && e <= l[1];
            },
            rangeLength: function(e, l) {
                return e.length >= l[0] && e.length <= l[1];
            },
            empty: n,
            isEmpty: n,
            jsonString: function(e) {
                if ("string" == typeof e) try {
                    var l = JSON.parse(e);
                    return !("object" != (0, t.default)(l) || !l);
                } catch (e) {
                    return !1;
                }
                return !1;
            },
            landline: function(e) {
                return /^\d{3,4}-\d{7,8}(-\d{3,4})?$/.test(e);
            },
            object: function(e) {
                return "[object Object]" === Object.prototype.toString.call(e);
            },
            array: function(e) {
                return "function" == typeof Array.isArray ? Array.isArray(e) : "[object Array]" === Object.prototype.toString.call(e);
            },
            code: function(e) {
                var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 6;
                return new RegExp("^\\d{".concat(l, "}$")).test(e);
            }
        };
        l.default = r;
    },
    bb09: function(e, l, a) {
        (function(l) {
            e.exports = {
                data: function() {
                    return {};
                },
                onLoad: function() {
                    this.$u.getRect = this.$uGetRect;
                },
                methods: {
                    $uGetRect: function(e, a) {
                        var u = this;
                        return new Promise(function(t) {
                            l.createSelectorQuery().in(u)[a ? "selectAll" : "select"](e).boundingClientRect(function(e) {
                                a && Array.isArray(e) && e.length && t(e), !a && e && t(e);
                            }).exec();
                        });
                    },
                    getParentData: function() {
                        var e = this, l = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
                        this.parent || (this.parent = !1), this.parent = this.$u.$parent.call(this, l), 
                        this.parent && Object.keys(this.parentData).map(function(l) {
                            e.parentData[l] = e.parent[l];
                        });
                    }
                },
                onReachBottom: function() {
                    l.$emit("uOnReachBottom");
                }
            };
        }).call(this, a("543d").default);
    },
    bc16: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0, l.default = {
            created: function() {
                "message" === this.type && (this.maskShow = !1, this.childrenMsg = null);
            },
            methods: {
                customOpen: function() {
                    this.childrenMsg && this.childrenMsg.open();
                },
                customClose: function() {
                    this.childrenMsg && this.childrenMsg.close();
                }
            }
        };
    },
    bc2e: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var u = [ "qy", "env", "error", "version", "lanDebug", "cloud", "serviceMarket", "router", "worklet" ], t = [ "lanDebug", "router", "worklet" ], n = "undefined" != typeof globalThis ? globalThis : function() {
            return this;
        }(), r = [ "w", "x" ].join(""), v = n[r], o = v.getLaunchOptionsSync ? v.getLaunchOptionsSync() : null;
        function i(e) {
            return (!o || 1154 !== o.scene || !t.includes(e)) && (u.indexOf(e) > -1 || "function" == typeof v[e]);
        }
        n[r] = function() {
            var e = {};
            for (var l in v) i(l) && (e[l] = v[l]);
            return e;
        }();
        var b = n[r];
        l.default = b;
    },
    c135: function(e, l) {
        e.exports = function(e) {
            if (Array.isArray(e)) return e;
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    c240: function(e, l) {
        e.exports = function() {
            throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    c4a7: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        var t = u(a("7037"));
        l.default = function e(l) {
            if ([ null, void 0, NaN, !1 ].includes(l)) return l;
            if ("object" !== (0, t.default)(l) && "function" != typeof l) return l;
            var a = function(e) {
                return "[object Array]" === Object.prototype.toString.call(e);
            }(l) ? [] : {};
            for (var u in l) l.hasOwnProperty(u) && (a[u] = "object" === (0, t.default)(l[u]) ? e(l[u]) : l[u]);
            return a;
        };
    },
    c8ba: function(l, a) {
        var u;
        u = function() {
            return this;
        }();
        try {
            u = u || new Function("return this")();
        } catch (l) {
            "object" === ("undefined" == typeof window ? "undefined" : e(window)) && (u = window);
        }
        l.exports = u;
    },
    e258: function(e, l, a) {
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = void 0;
        l.default = function(e) {
            var l = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "both";
            return "both" == l ? e.replace(/^\s+|\s+$/g, "") : "left" == l ? e.replace(/^\s*/, "") : "right" == l ? e.replace(/(\s*$)/g, "") : "all" == l ? e.replace(/\s+/g, "") : e;
        };
    },
    e50d: function(e, l, a) {
        var u = a("7037").default;
        e.exports = function(e, l) {
            if ("object" !== u(e) || null === e) return e;
            var a = e[Symbol.toPrimitive];
            if (void 0 !== a) {
                var t = a.call(e, l || "default");
                if ("object" !== u(t)) return t;
                throw new TypeError("@@toPrimitive must return a primitive value.");
            }
            return ("string" === l ? String : Number)(e);
        }, e.exports.__esModule = !0, e.exports.default = e.exports;
    },
    ee21: function(e, l, a) {
        (function(e) {
            var u = a("4ea4");
            Object.defineProperty(l, "__esModule", {
                value: !0
            }), l.default = void 0;
            var t = u(a("970b")), n = u(a("5bc3")), r = u(a("a383")), v = u(a("b5d5")), o = new (function() {
                function l() {
                    var e = this;
                    (0, t.default)(this, l), this.config = {
                        baseUrl: "",
                        header: {},
                        method: "POST",
                        dataType: "json",
                        responseType: "text",
                        showLoading: !0,
                        loadingText: "请求中...",
                        loadingTime: 800,
                        timer: null,
                        originalData: !1,
                        loadingMask: !0
                    }, this.interceptor = {
                        request: null,
                        response: null
                    }, this.get = function(l) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, u = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                        return e.request({
                            method: "GET",
                            url: l,
                            header: u,
                            data: a
                        });
                    }, this.post = function(l) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, u = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                        return e.request({
                            url: l,
                            method: "POST",
                            header: u,
                            data: a
                        });
                    }, this.put = function(l) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, u = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                        return e.request({
                            url: l,
                            method: "PUT",
                            header: u,
                            data: a
                        });
                    }, this.delete = function(l) {
                        var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {}, u = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                        return e.request({
                            url: l,
                            method: "DELETE",
                            header: u,
                            data: a
                        });
                    };
                }
                return (0, n.default)(l, [ {
                    key: "setConfig",
                    value: function(e) {
                        this.config = (0, r.default)(this.config, e);
                    }
                }, {
                    key: "request",
                    value: function() {
                        var l = this, a = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                        if (this.interceptor.request && "function" == typeof this.interceptor.request) {
                            var u = this.interceptor.request(a);
                            if (!1 === u) return new Promise(function() {});
                            this.options = u;
                        }
                        return a.dataType = a.dataType || this.config.dataType, a.responseType = a.responseType || this.config.responseType, 
                        a.url = a.url || "", a.params = a.params || {}, a.header = Object.assign(this.config.header, a.header), 
                        a.method = a.method || this.config.method, new Promise(function(u, t) {
                            a.complete = function(a) {
                                if (e.hideLoading(), clearTimeout(l.config.timer), l.config.timer = null, l.config.originalData) if (l.interceptor.response && "function" == typeof l.interceptor.response) {
                                    var n = l.interceptor.response(a);
                                    !1 !== n ? u(n) : t(a);
                                } else u(a); else if (200 == a.statusCode) if (l.interceptor.response && "function" == typeof l.interceptor.response) {
                                    var r = l.interceptor.response(a.data);
                                    !1 !== r ? u(r) : t(a.data);
                                } else u(a.data); else t(a);
                            }, a.url = v.default.url(a.url) ? a.url : l.config.baseUrl + (0 == a.url.indexOf("/") ? a.url : "/" + a.url), 
                            l.config.showLoading && !l.config.timer && (l.config.timer = setTimeout(function() {
                                e.showLoading({
                                    title: l.config.loadingText,
                                    mask: l.config.loadingMask
                                }), l.config.timer = null;
                            }, l.config.loadingTime)), e.request(a);
                        });
                    }
                } ]), l;
            }())();
            l.default = o;
        }).call(this, a("543d").default);
    },
    f0c5: function(e, l, a) {
        function u(e, l, a, u, t, n, r, v, o, i) {
            var b, c = "function" == typeof e ? e.options : e;
            if (o) {
                c.components || (c.components = {});
                var s = Object.prototype.hasOwnProperty;
                for (var f in o) s.call(o, f) && !s.call(c.components, f) && (c.components[f] = o[f]);
            }
            if (i && ("function" == typeof i.beforeCreate && (i.beforeCreate = [ i.beforeCreate ]), 
            (i.beforeCreate || (i.beforeCreate = [])).unshift(function() {
                this[i.__module] = this;
            }), (c.mixins || (c.mixins = [])).push(i)), l && (c.render = l, c.staticRenderFns = a, 
            c._compiled = !0), u && (c.functional = !0), n && (c._scopeId = "data-v-" + n), 
            r ? (b = function(e) {
                (e = e || this.$vnode && this.$vnode.ssrContext || this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) || "undefined" == typeof __VUE_SSR_CONTEXT__ || (e = __VUE_SSR_CONTEXT__), 
                t && t.call(this, e), e && e._registeredComponents && e._registeredComponents.add(r);
            }, c._ssrRegister = b) : t && (b = v ? function() {
                t.call(this, this.$root.$options.shadowRoot);
            } : t), b) if (c.functional) {
                c._injectStyles = b;
                var d = c.render;
                c.render = function(e, l) {
                    return b.call(l), d(e, l);
                };
            } else {
                var h = c.beforeCreate;
                c.beforeCreate = h ? [].concat(h, b) : [ b ];
            }
            return {
                exports: e,
                options: c
            };
        }
        a.d(l, "a", function() {
            return u;
        });
    },
    f5a1: function(e, l, a) {
        var u = a("4ea4");
        Object.defineProperty(l, "__esModule", {
            value: !0
        }), l.default = function(e, l) {
            for (var a = this.$parent; a; ) if (a.$options.name !== e) a = a.$parent; else {
                var u = function() {
                    var e = {};
                    if (Array.isArray(l)) l.map(function(l) {
                        e[l] = a[l] ? a[l] : "";
                    }); else for (var u in l) Array.isArray(l[u]) ? l[u].length ? e[u] = l[u] : e[u] = a[u] : l[u].constructor === Object ? Object.keys(l[u]).length ? e[u] = l[u] : e[u] = a[u] : e[u] = l[u] || !1 === l[u] ? l[u] : a[u];
                    return {
                        v: e
                    };
                }();
                if ("object" === (0, t.default)(u)) return u.v;
            }
            return {};
        };
        var t = u(a("7037"));
    }
} ]);