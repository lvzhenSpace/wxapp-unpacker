(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],[
/* 0 */,
/* 1 */
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.default = void 0;var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(n);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.then(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return res.then(function (res) {
      return res[1];
    }).catch(function (res) {
      return res[0];
    });
  } };


var SYNC_API_RE =
/^\$|sendNativeEvent|restoreGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.windowHeight - safeArea.bottom };

  }
}
var protocols = {
  previewImage: previewImage,
  getSystemInfo: {
    returnValue: addSafeAreaInsets },

  getSystemInfoSync: {
    returnValue: addSafeAreaInsets } };


var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F ".concat(methodName, "\u6682\u4E0D\u652F\u6301").concat(key));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F \u6682\u4E0D\u652F\u6301".concat(methodName));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      var returnValue = wx[options.name || methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail:\u6682\u4E0D\u652F\u6301 ").concat(name, " \u65B9\u6CD5") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail:服务[' + service + ']不存在' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  if (typeof getUniEmitter === 'function') {
    /* eslint-disable no-undef */
    return getUniEmitter;
  }
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


var api = /*#__PURE__*/Object.freeze({
  __proto__: null });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  {
    if (!wx.canIUse('nextTick')) {
      return;
    }
  }
  var oldTriggerEvent = mpInstance.triggerEvent;
  mpInstance.triggerEvent = function (event) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
    return oldTriggerEvent.apply(mpInstance, [customize(event)].concat(args));
  };
}

function initHook(name, options) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {args[_key4] = arguments[_key4];}
      return oldHook.apply(this, args);
    };
  }
}

Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  initHook('onLoad', options);
  return MPPage(options);
};

Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  initHook('created', options);
  return MPComponent(options);
};

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"NODE_ENV":"development","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor = dataPath ? vm.__get_value(dataPath, context) : context;

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象
  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return event.detail.__args__ || event.detail;
    }
  }

  var extraObj = processEventExtra(vm, extra, event);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(event.detail.__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function handleEvent(event) {var _this = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this.$vm;
          if (
          handlerCtx.$options.generic &&
          handlerCtx.$parent &&
          handlerCtx.$parent.$parent)
          {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = handlerCtx.$parent.$parent;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            throw new Error(" _vm.".concat(methodName, " is not a function"));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          ret.push(handler.apply(handlerCtx, processEventArgs(
          _this.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName)));

        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound'];


function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;

      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (!wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initHooks(appOptions, hooks);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      var components = mpInstance.selectAllComponents('.vue-ref');
      components.forEach(function (component) {
        var ref = component.dataset.ref;
        $refs[ref] = component.$vm || component;
      });
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  App(parseApp(vm));
  return vm;
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (args) {
    this.$vm.$mp.query = args; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', args);
  };

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (target[name]) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;

/***/ }),
/* 2 */
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2020 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  // fixed by xxxxxx (nvue vuex)
  /* eslint-disable no-undef */
  if(typeof SharedObject !== 'undefined'){
    this.id = SharedObject.uid++;
  } else {
    this.id = uid++;
  }
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = typeof SharedObject !== 'undefined' ? SharedObject : {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i++, i)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu'){//百度 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    if (currentValue != pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"NODE_ENV":"development","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"NODE_ENV":"development","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"NODE_ENV":"development","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);
  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"NODE_ENV":"development","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue ) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      this.$scope['triggerEvent'](event, {
        __args__: toArray(arguments, 1)
      });
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string,number
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onError',
    //Page
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 3 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 4 */
/*!************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/pages.json ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */
/*!*******************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/common/runtime.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {!function () {
  try {
    var e = Function('return this')();
    if (e && !e.Math) {
      Object.assign(e, {
        isFinite: isFinite,
        Array: Array,
        Date: Date,
        Error: Error,
        Function: Function,
        Math: Math,
        Object: Object,
        RegExp: RegExp,
        String: String,
        TypeError: TypeError,
        setTimeout: setTimeout,
        clearTimeout: clearTimeout,
        setInterval: setInterval,
        clearInterval: clearInterval });

      if ('undefined' != typeof Reflect) {
        e.Reflect = Reflect;
      }
    }
  } catch (e) {
    console.log('CatchClause', e);
    console.log('CatchClause', e);
  }
}();
(function (e) {
  function o(o) {
    for (m = o[0], c = o[1], r = o[2], i = 0, g = [], void 0; i < m.length; i++) {
      var t;
      var p;
      var m;
      var c;
      var r;
      var i;
      var g;
      p = m[i];
      if (Object.prototype.hasOwnProperty.call(s, p) && s[p]) {
        g.push(s[p][0]);
      }
      s[p] = 0;
    }
    for (t in c) {
      if (Object.prototype.hasOwnProperty.call(c, t)) {
        e[t] = c[t];
      }
    }
    for (u && u(o); g.length;) {
      g.shift()();
    }
    a.push.apply(a, r || []);
    return n();
  }
  function n() {
    for (o = 0, void 0; o < a.length; o++) {
      var e;
      var o;
      for (n = a[o], t = true, p = 1, void 0; p < n.length; p++) {
        var n;
        var t;
        var p;
        var c = n[p];
        if (0 !== s[c]) {
          t = false;
        }
      }
      if (t) {
        a.splice(o--, 1);
        e = m(m.s = n[0]);
      }
    }
    return e;
  }
  var t = {};
  var p = {
    'common/runtime': 0 };

  var s = {
    'common/runtime': 0 };

  var a = [];
  function m(o) {
    if (t[o]) {
      return t[o].exports;
    }
    var n = t[o] = {
      i: o,
      l: false,
      exports: {} };

    e[o].call(n.exports, n, n.exports, m);
    n.l = true;
    return n.exports;
  }
  m.e = function (e) {
    var o = [];
    if (p[e]) {
      o.push(p[e]);
    } else {
      if (
      0 !== p[e] &&
      {
        'components/TabBar/tabBar': 1,
        'components/PageRender/PageRender': 1,
        'components/CouponPopup/CouponPopup': 1,
        'components/Danmus/Danmus': 1,
        'components/HomeNavbar/HomeNavbar': 1,
        'components/Popup/Popup': 1,
        'pages/fudai/components/PayCard': 1,
        'components/BoxSkuPopup/BoxSkuPopup': 1,
        'components/FreeTicketFloatBtn/FreeTicketFloatBtn': 1,
        'components/OpenBoxPopupTheme2/OpenBoxPopupTheme2': 1,
        'components/PriceDisplay/PriceDisplay': 1,
        'pages/fudai/components/RecordList': 1,
        'pages/fudai/components/imagepop': 1,
        'uni_modules/uni-popup/components/uni-popup/uni-popup': 1,
        'pages/renyimen/components/PayCard': 1,
        'pages/renyimen/components/bangDan': 1,
        'components/OpenBoxPopup/OpenBoxPopup': 1,
        'pages/renyimen/components/OpenBoxPopup': 1,
        'pages/renyimen/components/RecordList': 1,
        'pages/renyimen/components/imagepop': 1,
        'pages/renyimen/components/jiangli': 1,
        'pages/renyimen/components/wanfa': 1,
        'pages/wuxianshang/components/PayCard': 1,
        'pages/wuxianshang/components/bangDan': 1,
        'pages/wuxianshang/components/RecordList': 1,
        'pages/wuxianshang/components/imagepop': 1,
        'pages/wuxianshang/components/jiangli': 1,
        'pages/wuxianshang/components/wanfa': 1,
        'components/NoData/NoData': 1,
        'components/SkuItem/SkuItem': 1,
        'components/Button/index': 1,
        'components/SelectAddress/SelectAddress': 1,
        'components/SplitLine/index': 1,
        'components/UsableCouponPopup/UsableCouponPopup': 1,
        'components/ActionSheet/index': 1,
        'components/ReturnSalePopup/ReturnSalePopup': 1,
        'components/UserGroupCheck/UserGroupCheck': 1,
        'pages/couponDetail/components/CouponItem': 1,
        'pages/myBox/components/PackageSku': 1,
        'pages/myBox/components/PayCard': 1,
        'components/ResalePopup/ResalePopup': 1,
        'pages/orderList/components/OrderItem': 1,
        'components/ReturnSalePopupOld/ReturnSalePopupOld': 1,
        'pages/orderList/components/PayCard': 1,
        'pages/myActivity/components/ActivityItem': 1,
        'uni_modules/uni-swipe-action/components/uni-swipe-action-item/uni-swipe-action-item': 1,
        'components/InputNumber/index': 1,
        'components/Banner/Banner': 1,
        'pages/lotteryTicket/components/InviteeItem': 1,
        'pages/lotteryTicket/components/TicketItem': 1,
        'components/FloatBtn/FloatBtn': 1,
        'pages/buyVip/components/PayCard': 1,
        'components/TextNavBar/TextNavBar': 1,
        'components/SharePopup/SharePopup': 1,
        'components/CountDown/CountDown': 1,
        'components/HiddenSkuRank/HiddenSkuRank': 1,
        'pages/boxDetail/boxTheme/Box3D': 1,
        'pages/boxDetail/boxTheme/Default': 1,
        'components/NoMore/NoMore': 1,
        'pages/resale/components/ResaleItem': 1,
        'components/ProductList/ProductList': 1,
        'pages/search/components/list': 1,
        'pages/search/components/sortBar': 1,
        'pages/openBox/components/PayCard': 1,
        'pages/zhuli/components/MyRecordList': 1,
        'components/AreaSelect/AreaSelect': 1,
        'pages/myCoupons/components/CouponItem': 1,
        'pages/myInvitees/components/AgentRecordItem': 1,
        'pages/myInvitees/components/UserItem': 1,
        'pages/activityTicket/components/Rule': 1,
        'pages/activityTicket/components/TicketItem': 1,
        'pages/activityTicket/components/UserItem': 1,
        'components/GetPhonePopup/GetPhonePopup': 1,
        'components/SharePopup/components/posterTheme1': 1,
        'components/GetPhonePopup/GetPhonePopupForWechat': 1,
        'components/UserStatementFalse/UserStatementFalse': 1,
        'member/pages/memberGrade/components/memberTop': 1,
        'member/pages/memberGrade/components/memberBottom': 1,
        'pages/rotateLottery/components/Lottery': 1,
        'components/BuyActivityTicket/BuyActivityTicket': 1,
        'components/LotteryResultPopup/LotteryResultPopup': 1,
        'pages/rotateLottery/components/InviteRecordPopup': 1,
        'pages/lottery/components/allUserList': 1,
        'pages/lottery/components/luckyUserList': 1,
        'pages/lottery/components/PayCard': 1,
        'pages/lottery/components/InvitePopup': 1,
        'pages/lottery/components/MyTicket': 1,
        'pages/yifanshang/components/PayCard': 1,
        'pages/yifanshang/components/OpenBoxPopup': 1,
        'pages/yifanshang/components/RecordList': 1,
        'pages/yifanshang/components/RoomPopup': 1,
        'pages/yifanshang/components/imagepop': 1,
        'pages/malishang/components/PayCard': 1,
        'pages/malishang/components/OpenBoxPopup': 1,
        'pages/malishang/components/RecordList': 1,
        'pages/malishang/components/RoomPopup': 1,
        'pages/malishang/components/imagepop': 1,
        'components/GroupPriceCheck/GroupPriceCheck': 1,
        'components/OrderReward/OrderReward': 1,
        'components/SeckillPriceCheck/SeckillPriceCheck': 1,
        'pages/productDetail/components/ProductDetail': 1,
        'pages/productDetail/components/SwiperImages': 1,
        'components/PageRender/themes/HomepageTheme': 1,
        'components/PageRender/themes/DefaultTheme': 1,
        'components/CouponPopup/CouponItem': 1,
        'components/UserStatement/UserStatement': 1,
        'components/UsableCouponPopup/components/CouponItem': 1,
        'pages/myBox/components/SkuInfo': 1,
        'components/CountDown/theme/ZhuliTheme': 1,
        'components/ProductItem/ProductItem': 1,
        'components/IPicker/IPicker': 1,
        'components/AreaSelect/simple-address': 1,
        'components/slider-verify/slider-verify': 1,
        'components/ai-progress/ai-progress': 1,
        'components/HTML/HTML': 1,
        'components/ActivityList/ActivityList': 1,
        'components/BoxList/BoxList': 1,
        'components/CategoryList/CategoryList': 1,
        'components/CouponList/CouponList': 1,
        'components/IPList/IPList': 1,
        'components/PageRender/modules/ImageList': 1,
        'components/PageRender/modules/PureImageList': 1,
        'components/PageRender/modules/Video': 1,
        'components/SigninCard/SigninCard': 1,
        'components/cardTitle/cardTitle': 1,
        'components/PageRender/modules/SearchBar': 1,
        'components/jyf-Parser/index': 1,
        'components/ActivityItem/Grid1': 1,
        'components/ActivityItem/Grid2': 1,
        'components/ActivityItem/Grid3': 1,
        'components/ActivityItem/Row1': 1,
        'components/ActivityItem/Row1Seckill': 1,
        'components/BoxItem/Grid1': 1,
        'components/BoxItem/Grid2': 1,
        'components/BoxItem/Grid3': 1,
        'components/BoxItem/Row1': 1,
        'components/CouponList/components/CouponItem': 1,
        'components/jyf-Parser/trees': 1 }[
      e])
      {
        o.push(
        p[e] = new Promise(function (o, n) {
          for (
          t =
          ({
            'components/TabBar/tabBar': 'components/TabBar/tabBar',
            'components/PageRender/PageRender': 'components/PageRender/PageRender',
            'components/CouponPopup/CouponPopup': 'components/CouponPopup/CouponPopup',
            'components/Danmus/Danmus': 'components/Danmus/Danmus',
            'components/HomeNavbar/HomeNavbar': 'components/HomeNavbar/HomeNavbar',
            'components/Popup/Popup': 'components/Popup/Popup',
            'pages/fudai/components/PayCard': 'pages/fudai/components/PayCard',
            'components/BoxSkuPopup/BoxSkuPopup': 'components/BoxSkuPopup/BoxSkuPopup',
            'components/FreeTicketFloatBtn/FreeTicketFloatBtn': 'components/FreeTicketFloatBtn/FreeTicketFloatBtn',
            'components/OpenBoxPopupTheme2/OpenBoxPopupTheme2': 'components/OpenBoxPopupTheme2/OpenBoxPopupTheme2',
            'components/PriceDisplay/PriceDisplay': 'components/PriceDisplay/PriceDisplay',
            'pages/fudai/components/RecordList': 'pages/fudai/components/RecordList',
            'pages/fudai/components/imagepop': 'pages/fudai/components/imagepop',
            'uni_modules/uni-popup/components/uni-popup/uni-popup': 'uni_modules/uni-popup/components/uni-popup/uni-popup',
            'pages/renyimen/components/PayCard': 'pages/renyimen/components/PayCard',
            'pages/renyimen/components/bangDan': 'pages/renyimen/components/bangDan',
            'components/OpenBoxPopup/OpenBoxPopup': 'components/OpenBoxPopup/OpenBoxPopup',
            'pages/renyimen/components/OpenBoxPopup': 'pages/renyimen/components/OpenBoxPopup',
            'pages/renyimen/components/RecordList': 'pages/renyimen/components/RecordList',
            'pages/renyimen/components/imagepop': 'pages/renyimen/components/imagepop',
            'pages/renyimen/components/jiangli': 'pages/renyimen/components/jiangli',
            'pages/renyimen/components/wanfa': 'pages/renyimen/components/wanfa',
            'pages/wuxianshang/components/PayCard': 'pages/wuxianshang/components/PayCard',
            'pages/wuxianshang/components/bangDan': 'pages/wuxianshang/components/bangDan',
            'pages/wuxianshang/components/RecordList': 'pages/wuxianshang/components/RecordList',
            'pages/wuxianshang/components/imagepop': 'pages/wuxianshang/components/imagepop',
            'pages/wuxianshang/components/jiangli': 'pages/wuxianshang/components/jiangli',
            'pages/wuxianshang/components/wanfa': 'pages/wuxianshang/components/wanfa',
            'components/NoData/NoData': 'components/NoData/NoData',
            'components/SkuItem/SkuItem': 'components/SkuItem/SkuItem',
            'components/Button/index': 'components/Button/index',
            'components/SelectAddress/SelectAddress': 'components/SelectAddress/SelectAddress',
            'components/SplitLine/index': 'components/SplitLine/index',
            'components/UsableCouponPopup/UsableCouponPopup': 'components/UsableCouponPopup/UsableCouponPopup',
            'components/ActionSheet/index': 'components/ActionSheet/index',
            'components/ReturnSalePopup/ReturnSalePopup': 'components/ReturnSalePopup/ReturnSalePopup',
            'components/UserGroupCheck/UserGroupCheck': 'components/UserGroupCheck/UserGroupCheck',
            'pages/couponDetail/components/CouponItem': 'pages/couponDetail/components/CouponItem',
            'pages/myBox/components/PackageSku': 'pages/myBox/components/PackageSku',
            'pages/myBox/components/PayCard': 'pages/myBox/components/PayCard',
            'components/ResalePopup/ResalePopup': 'components/ResalePopup/ResalePopup',
            'pages/orderList/components/OrderItem': 'pages/orderList/components/OrderItem',
            'components/ReturnSalePopupOld/ReturnSalePopupOld': 'components/ReturnSalePopupOld/ReturnSalePopupOld',
            'pages/orderList/components/PayCard': 'pages/orderList/components/PayCard',
            'pages/myActivity/components/ActivityItem': 'pages/myActivity/components/ActivityItem',
            'uni_modules/uni-swipe-action/components/uni-swipe-action-item/uni-swipe-action-item':
            'uni_modules/uni-swipe-action/components/uni-swipe-action-item/uni-swipe-action-item',
            'components/InputNumber/index': 'components/InputNumber/index',
            'components/Banner/Banner': 'components/Banner/Banner',
            'pages/lotteryTicket/components/InviteeItem': 'pages/lotteryTicket/components/InviteeItem',
            'pages/lotteryTicket/components/TicketItem': 'pages/lotteryTicket/components/TicketItem',
            'components/FloatBtn/FloatBtn': 'components/FloatBtn/FloatBtn',
            'pages/buyVip/components/PayCard': 'pages/buyVip/components/PayCard',
            'components/TextNavBar/TextNavBar': 'components/TextNavBar/TextNavBar',
            'components/SharePopup/SharePopup': 'components/SharePopup/SharePopup',
            'components/CountDown/CountDown': 'components/CountDown/CountDown',
            'components/HiddenSkuRank/HiddenSkuRank': 'components/HiddenSkuRank/HiddenSkuRank',
            'pages/boxDetail/boxTheme/Box3D': 'pages/boxDetail/boxTheme/Box3D',
            'pages/boxDetail/boxTheme/Default': 'pages/boxDetail/boxTheme/Default',
            'components/NoMore/NoMore': 'components/NoMore/NoMore',
            'pages/resale/components/ResaleItem': 'pages/resale/components/ResaleItem',
            'components/ProductList/ProductList': 'components/ProductList/ProductList',
            'pages/search/components/list': 'pages/search/components/list',
            'pages/search/components/sortBar': 'pages/search/components/sortBar',
            'pages/openBox/components/PayCard': 'pages/openBox/components/PayCard',
            'pages/zhuli/components/MyRecordList': 'pages/zhuli/components/MyRecordList',
            'components/AreaSelect/AreaSelect': 'components/AreaSelect/AreaSelect',
            'pages/myCoupons/components/CouponItem': 'pages/myCoupons/components/CouponItem',
            'pages/myInvitees/components/AgentRecordItem': 'pages/myInvitees/components/AgentRecordItem',
            'pages/myInvitees/components/UserItem': 'pages/myInvitees/components/UserItem',
            'pages/activityTicket/components/Rule': 'pages/activityTicket/components/Rule',
            'pages/activityTicket/components/TicketItem': 'pages/activityTicket/components/TicketItem',
            'pages/activityTicket/components/UserItem': 'pages/activityTicket/components/UserItem',
            'components/GetPhonePopup/GetPhonePopup': 'components/GetPhonePopup/GetPhonePopup',
            'components/SharePopup/components/posterTheme1': 'components/SharePopup/components/posterTheme1',
            'components/GetPhonePopup/GetPhonePopupForWechat': 'components/GetPhonePopup/GetPhonePopupForWechat',
            'components/UserStatementFalse/UserStatementFalse': 'components/UserStatementFalse/UserStatementFalse',
            'member/pages/memberGrade/components/memberTop': 'member/pages/memberGrade/components/memberTop',
            'member/pages/memberGrade/components/memberBottom': 'member/pages/memberGrade/components/memberBottom',
            'pages/rotateLottery/common/vendor': 'pages/rotateLottery/common/vendor',
            'pages/rotateLottery/components/Lottery': 'pages/rotateLottery/components/Lottery',
            'components/BuyActivityTicket/BuyActivityTicket': 'components/BuyActivityTicket/BuyActivityTicket',
            'components/LotteryResultPopup/LotteryResultPopup': 'components/LotteryResultPopup/LotteryResultPopup',
            'pages/rotateLottery/components/InviteRecordPopup': 'pages/rotateLottery/components/InviteRecordPopup',
            'pages/lottery/components/allUserList': 'pages/lottery/components/allUserList',
            'pages/lottery/components/luckyUserList': 'pages/lottery/components/luckyUserList',
            'pages/lottery/components/PayCard': 'pages/lottery/components/PayCard',
            'pages/lottery/components/InvitePopup': 'pages/lottery/components/InvitePopup',
            'pages/lottery/components/MyTicket': 'pages/lottery/components/MyTicket',
            'pages/yifanshang/components/PayCard': 'pages/yifanshang/components/PayCard',
            'pages/yifanshang/components/OpenBoxPopup': 'pages/yifanshang/components/OpenBoxPopup',
            'pages/yifanshang/components/RecordList': 'pages/yifanshang/components/RecordList',
            'pages/yifanshang/components/RoomPopup': 'pages/yifanshang/components/RoomPopup',
            'pages/yifanshang/components/imagepop': 'pages/yifanshang/components/imagepop',
            'pages/malishang/components/PayCard': 'pages/malishang/components/PayCard',
            'pages/malishang/components/OpenBoxPopup': 'pages/malishang/components/OpenBoxPopup',
            'pages/malishang/components/RecordList': 'pages/malishang/components/RecordList',
            'pages/malishang/components/RoomPopup': 'pages/malishang/components/RoomPopup',
            'pages/malishang/components/imagepop': 'pages/malishang/components/imagepop',
            'components/GroupPriceCheck/GroupPriceCheck': 'components/GroupPriceCheck/GroupPriceCheck',
            'components/OrderReward/OrderReward': 'components/OrderReward/OrderReward',
            'components/SeckillPriceCheck/SeckillPriceCheck': 'components/SeckillPriceCheck/SeckillPriceCheck',
            'pages/productDetail/components/ProductDetail': 'pages/productDetail/components/ProductDetail',
            'pages/productDetail/components/SwiperImages': 'pages/productDetail/components/SwiperImages',
            'components/PageRender/themes/HomepageTheme': 'components/PageRender/themes/HomepageTheme',
            'components/PageRender/themes/DefaultTheme': 'components/PageRender/themes/DefaultTheme',
            'components/CouponPopup/CouponItem': 'components/CouponPopup/CouponItem',
            'uni_modules/uni-transition/components/uni-transition/uni-transition': 'uni_modules/uni-transition/components/uni-transition/uni-transition',
            'components/UserStatement/UserStatement': 'components/UserStatement/UserStatement',
            'components/UsableCouponPopup/components/CouponItem': 'components/UsableCouponPopup/components/CouponItem',
            'pages/myBox/components/SkuInfo': 'pages/myBox/components/SkuInfo',
            'components/CountDown/theme/Default': 'components/CountDown/theme/Default',
            'components/CountDown/theme/ProductDetailTheme1': 'components/CountDown/theme/ProductDetailTheme1',
            'components/CountDown/theme/ZhuliTheme': 'components/CountDown/theme/ZhuliTheme',
            'components/ProductItem/ProductItem': 'components/ProductItem/ProductItem',
            'components/IPicker/IPicker': 'components/IPicker/IPicker',
            'components/AreaSelect/simple-address': 'components/AreaSelect/simple-address',
            'components/SingleRewardDisplay/SingleRewardDisplay': 'components/SingleRewardDisplay/SingleRewardDisplay',
            'components/slider-verify/slider-verify': 'components/slider-verify/slider-verify',
            'components/ai-progress/ai-progress': 'components/ai-progress/ai-progress',
            'components/HTML/HTML': 'components/HTML/HTML',
            'components/ActivityList/ActivityList': 'components/ActivityList/ActivityList',
            'components/BoxList/BoxList': 'components/BoxList/BoxList',
            'components/CategoryList/CategoryList': 'components/CategoryList/CategoryList',
            'components/CouponList/CouponList': 'components/CouponList/CouponList',
            'components/IPList/IPList': 'components/IPList/IPList',
            'components/PageRender/modules/ImageList': 'components/PageRender/modules/ImageList',
            'components/PageRender/modules/PureImageList': 'components/PageRender/modules/PureImageList',
            'components/PageRender/modules/Video': 'components/PageRender/modules/Video',
            'components/SigninCard/SigninCard': 'components/SigninCard/SigninCard',
            'components/cardTitle/cardTitle': 'components/cardTitle/cardTitle',
            'components/PageRender/modules/SearchBar': 'components/PageRender/modules/SearchBar',
            'components/jyf-Parser/index': 'components/jyf-Parser/index',
            'components/ActivityItem/Grid1': 'components/ActivityItem/Grid1',
            'components/ActivityItem/Grid2': 'components/ActivityItem/Grid2',
            'components/ActivityItem/Grid3': 'components/ActivityItem/Grid3',
            'components/ActivityItem/Row1': 'components/ActivityItem/Row1',
            'components/ActivityItem/Row1Seckill': 'components/ActivityItem/Row1Seckill',
            'components/BoxItem/Grid1': 'components/BoxItem/Grid1',
            'components/BoxItem/Grid2': 'components/BoxItem/Grid2',
            'components/BoxItem/Grid3': 'components/BoxItem/Grid3',
            'components/BoxItem/Row1': 'components/BoxItem/Row1',
            'components/CouponList/components/CouponItem': 'components/CouponList/components/CouponItem',
            'components/jyf-Parser/trees': 'components/jyf-Parser/trees' }[
          e] || e) + '.wxss',
          s = m.p + t,
          a = document.getElementsByTagName('link'),
          c = 0,
          void 0;
          c < a.length;
          c++)
          {
            var t;
            var s;
            var a;
            var c;
            var r = a[c];
            var i = r.getAttribute('data-href') || r.getAttribute('href');
            if ('stylesheet' === r.rel && (i === t || i === s)) {
              return o();
            }
          }
          var u = document.getElementsByTagName('style');
          for (c = 0; c < u.length; c++) {
            if ((i = (r = u[c]).getAttribute('data-href')) === t || i === s) {
              return o();
            }
          }
          var g = document.createElement('link');
          g.rel = 'stylesheet';
          g.type = 'text/css';
          g.onload = o;
          g.onerror = function (o) {
            var t = o && o.target && o.target.src || s;
            var a = new Error('Loading CSS chunk ' + e + ' failed.\n(' + t + ')');
            a.code = 'CSS_CHUNK_LOAD_FAILED';
            a.request = t;
            delete p[e];
            g.parentNode.removeChild(g);
            n(a);
          };
          g.href = s;
          document.getElementsByTagName('head')[0].appendChild(g);
        }).then(function () {
          p[e] = 0;
        }));

      }
    }
    var n = s[e];
    if (0 !== n) {
      if (n) {
        o.push(n[2]);
      } else {
        var t = new Promise(function (o, t) {
          n = s[e] = [o, t];
        });
        o.push(n[2] = t);
        var a;
        var c = document.createElement('script');
        c.charset = 'utf-8';
        c.timeout = 120;
        if (m.nc) {
          c.setAttribute('nonce', m.nc);
        }
        c.src = function (e) {
          return m.p + '' + e + '.js';
        }(e);
        var r = new Error();
        a = function a(o) {
          c.onerror = c.onload = null;
          clearTimeout(i);
          var n = s[e];
          if (0 !== n) {
            if (n) {
              var t = o && ('load' === o.type ? 'missing' : o.type);
              var p = o && o.target && o.target.src;
              r.message = 'Loading chunk ' + e + ' failed.\n(' + t + ': ' + p + ')';
              r.name = 'ChunkLoadError';
              r.type = t;
              r.request = p;
              n[1](r);
            }
            s[e] = void 0;
          }
        };
        var i = setTimeout(function () {
          a({
            type: 'timeout',
            target: c });

        }, 120000);
        c.onerror = c.onload = a;
        document.head.appendChild(c);
      }
    }
    return Promise.all(o);
  };
  m.m = e;
  m.c = t;
  m.d = function (e, o, n) {
    m.o(e, o) ||
    Object.defineProperty(e, o, {
      enumerable: true,
      get: n });

  };
  m.r = function (e) {
    if ('undefined' != typeof Symbol && Symbol.toStringTag) {
      Object.defineProperty(e, Symbol.toStringTag, {
        value: 'Module' });

    }
    Object.defineProperty(e, '__esModule', {
      value: true });

  };
  m.t = function (e, o) {
    if (1 & o) {
      e = m(e);
    }
    if (8 & o) {
      return e;
    }
    if (4 & o && 'object' == typeof e && e && e.__esModule) {
      return e;
    }
    var n = Object.create(null);
    m.r(n);
    Object.defineProperty(n, 'default', {
      enumerable: true,
      value: e });

    if (2 & o && 'string' != typeof e) {
      for (var t in e) {
        m.d(
        n,
        t,
        function (o) {
          return e[o];
        }.bind(null, t));

      }
    }
    return n;
  };
  m.n = function (e) {
    if (e && e.__esModule) {
      var o = function o() {
        return e.default;
      };
    } else {
      var o = function o() {
        return e;
      };
    }
    m.d(o, 'a', o);
    return o;
  };
  m.o = function (e, o) {
    return Object.prototype.hasOwnProperty.call(e, o);
  };
  m.p = '/';
  m.oe = function (e) {
    throw console.error(e), e;
  };
  var c = global.webpackJsonp = global.webpackJsonp || [];
  var r = c.push.bind(c);
  c.push = o;
  c = c.slice();
  for (var i = 0; i < c.length; i++) {
    o(c[i]);
  }
  var u = r;
  n();
})([]);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! (webpack)/buildin/global.js */ 3)))

/***/ }),
/* 11 */
/*!******************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/common/vendor.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(global.webpackJsonp=global.webpackJsonp||[]).push([["common/vendor"],{
"04fe":function fe(e,l){
e.exports={
list:["GET","/track-records"],
store:["POST","/track-records"]};

},
"0676":function _(e,l){
e.exports=function(){
throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"0863":function _(e,l,a){
"use strict";
(function(l){
var t=a("4ea4"),n=t(a("9523")),r=t(a("7037")),u=a("cbb4"),i=(t(a("66fd")),
a("2684"));
function o(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function s(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?o(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):o(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var c="wechat-"+(l.getSystemInfoSync().appName||"")+"-miniapp-"+(l.getSystemInfoSync().platform||""),v=null,b=Date.now(),f=null,h=0;
e.exports=function(e,a,t){
var n=arguments.length>3&&void 0!==arguments[3]?arguments[3]:{};
if("object"==(0,r.default)(e)&&(a=e.method,t=e.data,n=e.header,e=e.url),
"POST"===a&&v===e&&f===a&&Date.now()-b<100)return l.hideLoading(),
console.log("已拒绝重复发送POST请求"),!1;
v=e,b=Date.now(),f=a,a=(a||"GET").toUpperCase(),n=s(s({},n),{},{
Authorization:(0,u.$getStorage)("token")||"",
"Client-Type":c,
"Client-Name":"default"}),
e=i.BASE_URL+e;
var o=function o(e,a,t){
switch(e.statusCode){
case 200:
a(e.data);
break;

case 400:
a(e.data),l.showModal({
title:"提示",
content:e.data.message,
showCancel:!1}),
l.hideLoading();
break;

case 401:
if(t(e),l.hideLoading(),(0,u.$setStorage)("token",""),h&&new Date().getTime()-h<1e3)break;
h=new Date().getTime(),l.navigateTo({
url:"/pages/login/index"});

break;

case 409:
t(e),l.hideLoading(),40012==e.data.code?l.showModal({
title:"此操作需要先绑定手机号",
confirmText:"去绑定",
success:function success(e){
e.confirm&&l.navigateTo({
url:"/pages/myProfile/index"});

}}):
40021==e.data.code?l.showModal({
title:"积分不足",
content:"是否前往充值页充值？",
confirmText:"去充值",
success:function success(e){
e.confirm&&l.navigateTo({
url:"/pages/myScore/buy"});

}}):
l.showModal({
title:e.data.message||"系统繁忙",
showCancel:!1});

break;

case 500:
t(e),l.showModal({
title:"提示",
content:"服务器开小差了哦~",
showCancel:!1}),
l.hideLoading();
break;

default:
l.hideLoading(),t(e);}

};
return new Promise(function(r,u){
l.request({
url:e,
method:a||"GET",
data:t,
header:n,
success:function success(e){},
error:function error(e){
l.hideLoading(),u(e),l.showModal({
title:"提示",
content:"网络错误！",
showCancel:!1});

},
complete:function complete(e){
o(e,r,u);
}});

});
};
}).call(this,a("543d").default);
},
"0cca":function cca(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAACfFBMVEUi+f8i9/8i9f0g8Pjp//8awcgg6fEe2+Mc0Ncf5OwYtLog7PQYqrAi9//n/v8XFSUh9v4h9Pwh8fn+//sAAAAg6/Mh8/sg7fUHnP8e2+Mg6fEg7/cf5OwaGRgg7vYf4ekg6vIe3eUf5+8f5u4d2N8f4+sd1t0d1Nsd2eEc0Nce4Ogcy9IcztUYsbcbxs0e3+cawcgav8YbyM8Xqa8Yr7Ud09oZucAYs7kZtrwWp60bytEsxf0bxcwZub8Xq7EYFiAZFxwczdQbxMsavsUavcQZuL4aw8oZu8IYsLYc0tkYtbsXrbMaGhzr7OgaGhpZWlgYFybg4d5KS0krLCv7/PgWpaszNDPR0s8NaGwMDAyPkI12d3UyMjEZHB4BDQ4DBAODhIIjIyESExISiI0RgocZHyECFhcGBgYHl/YMe8a0tbSvr7Gur6ycnJmQkY4dfYB0dXMFLzEZLC329/PW1tTGx8OSk5CHh40MXWEcUlQePEYiITAFJykaJCYJj+mhop+TlJaGh4QSSnAKTE8HNjcVFRTb29cnn8rMzcm/wLynp6kXmp8PXpR+fn91dXwhXXRjY2shVGYUQV8fTl5UU10UO1VHRlIfRFEIQkVGRkQdOEIyMT4HOTs8PDodHCoBDxDx8u4Jh9oftrwNbq0ej5OLjIkdg4YSUn0dcXQObXEbZmgMYmVPTlkWMUMYJjAZICbv8O0qtegqtOfm5+MKhNQfr7QfqrAgqa8mh6olhqgPZaAQWIgQfYIiZX5mZW5fX2ggUGJhYl9TU1EKTE4bS0w9PEgdOkQbOzsXLTsXKTUQEA8JCQi5uryio59sbGoURmccQUIGMzQKKywFJyhmiqV8AAAADXRSTlN/f39/f39/f39/f39/idrGnwAAG91JREFUeNrsmfdzFVUUx+1lnGfuvdnC9t1X8tITmBAxL44QURwMCQaJFCMEiYAF7KMoFmxYsICOojL23nufUcext/Evcs/37HtZQkScLDNPh/NDTva+tzOfe/e733PufUec/N+NYw7DH1wchj8MXw9xGP4g4zD8/wg+V7dRsCUlw9UpqSZcyq4Il1Ho5I6oX3jlugayrShJH0lIM/5Dc8rljqpf+JwidsJFImJDCkoyVPj8xHqFlwm4gexHGNMtJeJBvdMWNHhCncLrTQK4gRKUrLyiqRSgdwiJ4qQ6hXd1B5imjzkYTCvjJAQU5Mhc/b6wBicYDOvd5wFbFzSr/AJZn/BKcJJIJixS6M0u1D9QwIRarHxdwucjxWucF5C9C3inFXo3TEUPQkg7lzuy/uCFyiteciQ4Juk8TpId0zckJbserVKI1P/Mr5JnUcZVYQ4JS7rH1hu8yczKB7YDx5ROWZckmT4XjtlsQf7quDqD98ugF0EoKQdcS/WCzU6ZOKYeT0TUn+aV4wNeOjXHhDPGxJL1rmPcbg1F/bmNmEyGAD4phhQeGJB9Z4k+tk9z68zn/cRedCTTwqUTRQIdcUmCOnL5O2Z9ycaxTIFyVFAwdkrU1uuJ43CtjZPOj8Y8uo7gpaM4cxKCjR0CQjL5hQhb2zAb6xBaZdh/6vpXV6x4df2pneHBiV0gIRIBmTqsMuwUUP9CHWt/huvQaPvxhwRedq+/40ItFRfesb5bHnjPV9W7RJprwnOaShgu55FKbsBPxscc5aGQzcCKp7Vp4ukVAwewSMvFupu2wUvuMGZYexACqdos6G7uELTEYvmVzLpydHj12s2b164eHl1Z5b9yufi726r1B+zgk0lSXGsLBltQRFlGva3Zw/fcRJBrRlad35iK81eNrGH8m3r+uadJGpnQZT21gF5vsyiZCwIF12/OZw1fvJ2WfHzzksb9Ysl1yxj/9uJ+t9kWkhGw3osmt8IWSUeELp8auHn2UzgmHMnOFL6DXtIL9nh772mcLi67gF/ejqmvd2QrUIc+UlKrDL92eGAkk5RIoQ7nL7VnaJWFi2KydZc3Nu74xvt6ybT4l6/bsPLONdpFU+Qi1dQzDimZ30QywxIkEyyipNwemoUIT8nKKttu2OZ5634eYejLll2wYxr2Jbdd4Hk/xeq/Xu13xpGMmMhSL0hcdaOYGt0BzKePi60VuFzTMrLKFwY9xLK15wLz/ZHx/aRz9c7x8dXXXgHpPFWq3lnKA9MohEjsmNLlnaAfJo5Zqlql4JQjKxXZaL5/yHutxWnbclXMP3zJeUQ6PH7Nvuw7RsdffCfOt2wAPcFhjQMDOVRpxzScWr1VnHRTslUaUH9nUWXlNt96ryCrj26g5b/t3Jh+z97z0uz3jI/EoxTX3rnm55/WXfWhYno5deunpEidTSq3mR9Jp0PDpYVFSm528GrMi6r/N1WX/5qhy1Psd40PX83//PrbMg9x1UCu+qLKpKvkZ9GEPszvZ8ecW9QpFVj2osvmE7VYQUYm8GVvInVVXf7hPSn4b/Y+03jejlu/BvjQH6+88NKP3sRzZQXcruRMz1a8SQKfH/OxgVatSCjBr4nkaZQy2cO2e97G9HWy/N69NeGc77344h681BPfvvQc4Dbu8r7X/WqjlXZMNal3ySacrHaAbUm+ZTEN+8VzMoG/3fM2zZqipI8T9ZNDXrJzFELZdsML3VWTnDVr49vec/iXUdPGLl3We8EycDVPx6ROiRTl7gjfKgZZwPdo67zts+Lgyynqv2IvK/y1+xYAsAY/a7v3EhoZrGr1TC8AdV8JQipaJn+oc+kt8CSdnJRxVhloXtykbfAGnyX6aZaf4vtXlsMyUugUL3tb6ExPYl/NQlZdwJSp3SyNQ+8AL7i4dttNkYnbLI/7mWHvh5sBNJW/vGXLh1yQ0ugcj3qvTbbCEmzcyPAOlR3TDh12orkoT3Nm65TLZzZnAx/375e9P+r9sAlEU/mnBUfcPOH9yTKQSSfM5cpux5XdbQi66tUlXfbjNEqWLHbMiJ7JzOEHNG1ZY+O5cdeyC/gcBwBHQPJ3amdhVeGYQrUAU84pKOBFAfSumzUFcccmfbZKy5/5D2orNO06spQrhjzvrc8hfY6/B0fc73kbtBVEkShDhXlKsuqYfqL32i9rQQRFtZZRZLtmL54xvLxYW7MEfdey+V4cb94P8SOm50Zs2uV58e7wYgmDT9RTk77DhSm0uMG3ykRtNPXk8VNgBzvmImvGXWW3po2gDt3b0NBwOvEPPfr8xlkHig8++/2t+GujWhzdks/0QmCKUsDeM6cVm6jZuFTtRXxqB4lj2twK+TPW/HpNWwX4NxoQ4J/Yvml67o2bPn/0bXpA46vvWkXwX7Sw3i0uRxZohR9xx276XGjNqoJkaIh4RHZZMpfBcd/jmoa99jtLG6oxf8iL5f/yB1PAb37+5TeH4PuDt91zddwyEPzjrg/MUOcKK4EpjNqZnq4k6x0HOYU57WgNFp0RigzgL9ZWYuEvZfDU8qfl/+xn23/0EPNPP72h4RfcslJj0QvGNJJa6yCHrX3QSDRPoSPusEG9sMmh0dZmPzdznw81bRQkO9Pwaflv3HT/rreZe37MjdiJWyD6MHZ0B/h2v0Jx7a2e6RGfaLFc6L1AKw8FCUPQiy3ycqYr369pwyB5JA0+NHQ6Esl/DOAYqcUjuGWY4PtjFykaMPa8w41MK58+mWxBDjWX0HuioOYyZlXunjtT+A5NWw2SJ1Nsg19tHUymkShlcOtXPMLxJG5ZTfAdOeHbtcMDojNqZ3pmgA/cPlvQk+lrUai1PQVyzKAngs/PrLFZC5IbGYuJ7/7E21dE3id3e6mlvxG3rCX45ax3Q6LW5kNooxlmIqNFER5JLy79vqLDP9NWHXOmmr9P0zaDZGkKfqLyLlBT03m3MpEaWYpbNhM8n+EUFqJrdNoshTPiVkrCbrahoDw7Jh0OJwoq+eSYJ80cnm0+hTp/a6WybX5DemRbpbI1PYJbVgFeR3Eq+Vyr8kmPVqu3eViRbDUxnSL/WGgtbiNhHX0IZDP4cKXyxL7wT1QqDw9OK5v7chRAFUi2YhsLQ+i9fQBX7fNgkS3n4DTKPg01F/AZv7BjD1Yqj+0L/1il8uDY37ywBr+vvhuiYw9mK0iFz/T8eRFRyzbLht5dvVpzlZrxoVPnNFY59mWlsntf+N2Vypdj01plZ04/C1pR3UVQF/lMz3Xd6uZcIuekb7DeTQO7kzY7syL1wL9Z+QdSRUo43BELk870cIYgEsc0Tei91CQh9NklSR3xKX14qXs73Czbg4PX/KWT7QEflhF+nAoKmGaEZ6EPNAFzQW8XrX3LPEvySR++nY+w8pk0Zs8sPXi3WfoM3cGNmc1nHAuQosUK5agXNVdfUGT5N/MhrGvimSgnp9iOTsq4JYarj1Xem+rz71XGJkfemHTK19HIyLkWLNEqcf3J27wnUXwCFQs9kII01eTiWVioufL4zDYjt6Yr7KdTK+yn6Qp7K24YIfhFvAkxaG0hdGGwY5Z0ieoalKD37rPIioJFi+GYnT0uzfnEzLaBd03qZnD3dymBY+S73ZMjS3FcvGQNSZ4aGT7eywfcETcHklL7aUrQGp9NjintDuwIZb+VZyuyqYIdmc0GnOKhVFfJpGn6VFf5EL5+XcxOG3Cj1AwndxcWcGY2r2ygQ+AzvcAtoPehVlgIUpHuQ++666qMjj4o/urlSlZlKYIoKoo4YGZgVpJVRc1jV/VQXT1eXje0176vWzcKCi5cuRFcuXACF7rQjQtx5Ur8AX9BdCF+gPhDZkS087io9vIg6ekRWXnyZMSJU/XWU//xj7sO1By0x6cclYrbx4plm/RnxpSXRCahd2Q2obWYbmJ8O+8neiDRicL59r/F/i23raipzOkhkTpeVy3IKzxi4TJvCUH5bolLE2zWVJVPmDHDuxsYSO5jHfuF/xL7C9xefpXyYS05Y3d91vTIIKFvFlrgkhypKvfnzJi+iWzs5EiXktoLDw0itD7NTZB3/0vw73JTk9rhoO6TaKBOykHOucuQ9kVhFGf3iojdglxoYkyde2hKVBVVsmoII/TCtl+JcF76/N9j//wloprXMPjFk3E+IooJIrYZKJZbLd49JnbDfr5lGVN18twMKdIfj1NcoYcHaS5YrqeYXnz/32J//0WaJXH8d7/V9AQEhPeQhctwTshOu2Mg0HjwfMJRk2DvzGqF3x3EFJrZtv2bnOH8W/Cc1bxJbfyaAK8YI1C0FFe5CQUGvykod6y5vRD7Pk7SgchK3xJzzMQd6raLsY3lAxb9XvjHzfoJfemDp2m33kXk5L8ZkaZXnjX3ZTUzpmtBLijH1EzsrhGId9PhWujieeMMZJywtoPX3+Zr/8I/xM7X/e3Xqf7zDJ78HJvgg/aSyKRSELGzoFrcNxLJZtEGWObuxwVOrtvVcijXx3tPP/0xR//Gl38X+5dvXJrIGPt7nCFefBwABA5D2zdt7uHbye1C4eG6PBFjxveIMWHU0GELFm1D2bTgC3vtGTlvff43PMMnK7fvP/JIB4sNlX75muPqU2RMr6sUtY8Nko/QCRO7VV1Bk1jmGo2MGQ92z0j+hQ3pzZeISz788i8u+4f8GRsnPnreUDLZ5cyEU58qVFcxY2o0ZxFjuoBh6qnRtKnnmcBNvbjDirB4bDC/Dbxng7pYVl787IU/oP2zF9m0YjkSMaNd9ulJzfeA/KrpuYkkfm804X29RIp0x0eNu/R2T3lNvK41yvdD+iovZiEO/93vfw39+3cxdDILPc1CE3n3BftXWetIJsTk6plGUcb+HDFmuaaaSrfMpyaLyOFnZZHQccSDw9u0MEuj+N9499Nvvvn03Tde/INNq084kelY09slGGY8o1NJ15SFyWhqESSIMaWUwgFrznJxadK8QCoyzU7J4Q1yNkN+558Mct8t53Tky3KkWdNLWdPzuHkGlusl+SRiLekQW3JZ8hyyDcR96+Kv+kUxtDUxfPnHf7UmQuBfDBKMdyQSTmSmmoFeEweZHRkV3PUiwl16s6cTubrX4LuBaTw5sIs7HsE/m0IvFar2mN+5XPKbgq5xSV0Qr+h9h2oqArpsjYvErmJNB5kbPOn6AsFkhr69Dv7Zjgu5K4nRuY+WrCthh2Bl6GU7TWkuCeKdxDIPmRKwhy/tO0lWRPj5va7EexjmTwwYvLwM4i+M0D+8snaIqUvCe3jxpKCmx2EKyamw1sSYI+PYD7x45iLeq33r4K/6FW7udNcbiXT/2HDBu61kNxgIYpFsZi3oX3313tfbe5EVr4Hl1F+68OJikPCBEplw5OEiTJ8NETL+4h5+EY7PhTip+alB9nfbVtmZQVUorBi94e7WEa6raVTqt74lSR0aDpxHnxMZzX7yaP4sLUl9JMZcPltRHn9TMGPGNFlpB5ASLSKxP8JVyrKJEANuWCH/6FsCj4eIRnAnhPdkTZqevh9Sk7IhRUb4xqUwU2x/EGOCctA6F01DStPKBn/mzhfYT9PL7V04GFXCZeAJKMVm+CWL1yuXgFRzX3ukIgYQRsvitRSUCvtUoYoyT0jTu5cJhN+4U1idLMYh7tK7HhlTLp9tYKgbXsIQ2BvGFn4zkrwfXRycnw1wGC3XfJdOt8sax6imWcVdjd2RfLYOyKrV+0CM1GoUDQ6ta7lApgakBSb4gTADeYklBy/gdwY9bk5yqeEIHNwps0xGs/PahkQ885yhTcOMKefMmLk19TlCOAGqZFT6VbGWQghzc4PbJdsMxTZ/vhfRkbwWBgcnZ2L3O1oa3ZyBJO34wpisiVF7m4hdeQnOO8mXgCxT3M8J7/3cQeLcrnNcqPEQPA88SB6DWhLeGyIXp6Y1kXnh0oq4mjcF+zgcB51vzCcJa9hnjesUPofTkul2lmPCvNgQ/LsFMqaYzg/KtiOiYgDYwJwhE2vJ52PA1zplpoTLtvwZQUKxxWZK3xN5FbNHeAVkjFinAol9RYw5nczxykOyjDAfcIyWLjhCgaiNeXKI3AZA0Sg1DX/wLSFILrPyE1qZuCO8Vwt8W6abkHh+UmnaO5TIWLRpmm/kPKkSQMY05xJ/ZiYr/JnZ9UuNVDkk3jlacBnvIZsgoy4hIK2pOSnvVTQJ4ytm1YjmixpHQIypXYWMGWSZj3M932LWBu3+PkqV1X6BCkhyaxlTDMTz4DGUmXSgqi7EzjbVMOTvRIRuR3HD2KH76Tg1APJx3E4RYNP1wbHjz5re3aICpPtT69k1CSYHy0FS5gYgHyY9EAHbq+DCmO4FSPDL3dsBjvZ7ICmfqblxNuHGWTchVfhmTYwZ7pgxnzlHuJJh7D6ppAANcpSAdEA0dSSFcG6eHaiG/fO9WcDbUgHQEJfEPdnEoyJqNWULf0kVqjHehTEJ7549g5CHUNML8T/MyjnYD6bzdYV4X/VoK9L1dsCs8oL34NLgmDHed+xbmjBjZsuI4B9eGPOSyNhIIyAxbxSzhn0vwpIlex4lDzDbZ3ESUT/GSwCr5w/SAm85GU49gBvOEFoWpbM04F0wZUZCwDKQQND1zcGh7xmaTkGqsPDX9z2koudOhoh91RLF3NGV16M6EkoCGPvPA+k/NFzwnmacp4QfBDohhRBE/M42rFER4vtpe4vBR4cFLg3EY1oab8al37RWmK459sJ7ChlThnEgHCnaZxpH2IL2iEQ7ZErMyOHry/ciMt4z9i2FOx8/y085AaljH0fBTs9IUbTWHWFTA4knRDJyEUHhpJniXJ8ZHxBIz/Y7F3msP7ViyODxxFHsSy08QvZNJekEngJ9yBmxF3GF6ihu96GmRxWH9iugpO1oBO7tBeJQZPsF/sxd7UvshxeLFoQUtozNJQwqOkmf2xoR545Okv5yCAhmJAVccbi0FuaeJr1pXOLbanyn8GvH05k+XLWAMit2QcB+qXJFCMJTAOcQHE+rdjZcGXgJkwH/i4U/kfI3Ru20ORM3zu80LcmR8ppqRl0QN2Pfs1fYkleQydX1QEhHVKMCOw3zex0IIdrVAs/ceL0d/B5wuLhOVIhDMHqW8U6GVKk6NsDVJW1LNeXkUnlMQ2B3p8bNnWQZadjzFf7Mb/raUlLUbFchmj/69dzOTW+eHzZ40mAAB+dZvsm89hVbgckJBJ7Hd1b8ounJKmAfx9JFoFXPFoCQ2T9Lmt76eYQ/7NaNRDqdNY6UIjEzXyeO1vXgT9OKtOYFCHFg3xKjSF80jphYpmpY01vdB/xVd8JTCco9aWdB1yos/UythY1XVVr4ud1LEspSCc8J6mc7W7UMHDxG94sXW1/KcOMK0rApLljufHLyn0ig0StmzDJ0aYv4GitGvJE3TiXWrKbMEej1bhbb/7Ra9zdgZ7HZbsLrPMcMcp/JpKVyI+0yfBk9U7FYxh4szZqeYy3DGgsWTGRIPktM6VHesyZN79B3ASZt/d7H4nzz/ASwTDs9C1JeI3inKrn0G3GCHLJqEHiEIIkaB0tJJqayJL6vcLnCRatR6u5nyLAwfp4Yc7k5gF0B/7b2pOVM04YwAql9CJbLazz3QMCvmh4wY+aaGwkjxY2EFt/3150mWWdDk2juNzikWRsg3nWp8L4cJ1XCRJ6FPsyrxrISNKsjCCHb/f5aD2HzWcSO/EzgULNvqRgnOCmfNY6gyRR5cKdskLALBR45y/zcRWtQ1UyQMevVGFs+WbdtLNCju+2xsMRV9eMrBe+4z5Fu6q1DQOK8mbIHeEqwhgBTYWJMjxlTmRRIob9JybOyLoEymI2PWdppX1mi8jbrW3z33O3AcUSY3T1yrSvPmp7W/h98S04aSNI4aiId02UEpNMuwF8d1yHSVNsXEuG/O+Cu9rIlOJ4j3YPrhkbI3NHLe5EIfufuG5gxBTis6XGJ6B9GiKDRqkUE6W5Dwd+MR6TpbVq6UeGcuXgk+L4nFQjwPHE2jt2vML+JwQ7L9dFgTbPd2mJKXvFRYJAZB4f5nEyQ1X2qyqezSxc+TjBalbhYjTv26HXZfVDFMWZHpr7BVlS5ex5FrKTbPhuh33jbV9JqCGPLmFJcCzbk5F9SmFMD9DKMOJFhC7+UNvAc33GnNZ0DxXMufrzczzVObrsLEXf7/ZLaacdbLaSsbmfgJLZMbP2wVA9eL3jq0Dh03grFPo6ffUvniDxkmwYhkz5/P7cDjDdUpE+OxJjTsxVhQaio9MCXEuJUVb6WgQPNfG6n7t+eNld8LJJAn14ekYad1WRIfeboCTxrTwkFvyLVILrXeEjsbqko55+C40aY+osi84UUzrxboThcrtFXIYrntxO7JuFqu8ENe70/MCdFtMGMCc/wvSrZyMepeQE3J8FqeqmDQIoPoYeTnJBvOz/2NX78zHaToU9isW+EEOl43EkQ0N6urvpAKiFCzohdlbJvySbuF99S4ko7aDMZ0XPjdiVmCG3/nMbjYLEwElO5vkR1Kd7dpnZXq2IGoAHyg6qyswAfrnvlBWbs2mNNryDGzFaGDKnjBlBBG48N3bWwLoBqllZjI/OmdJ/U9ks5QKilp5S7xNJVes3RAFawPa4FXPkZrTY+d15Sxv7sMy7SxnxHeXzRVUA2/1rhplaxjVbio21krJSQqOkVuOnnd3euQOtK30rUL7coHqjVdm8Z07ly8CRIUvCqqjxWNS+GVE0VI6DGESOQqnriIMvM97mDx8H2GYWamcU7Vux9P5Ool2w6cEAub+90MBWPXz14qX/V9HxkDXu5ifl1we3tbNEozP57JHapFidkJnm7OUg7ZvWzkQPCT++BN5IQxK5pYiU1hN39e9d//J0gTS8hDXvS4Ct/t9ECF58rVHOscU2SWYOaHoS1QjFZTh3AGzJTUEU9EiCgOR2lpahJvw8tNR2228n/8qRQdSALP5gxMWYwiylrO5cRpgZBpOn5fCDlyFBTtr01AdYhx0WMEtZ4ixm+Hm+fw13aPL9vsDwcj2fXvvKMnJjvBUktY2IdIQNbp3vkWxqRpWw07wx+PlnM0fh8ux1rXKjnx7hLw1l/Twqhl5vODaRMznc6SRSMWqTKa/+x6ipTKgZVfsNSx7pwcOjnOIlkuzAkDi+WDtUsLdifRPNJjqVfmAHYSUZukE3uQDoy7vZnm1j/P89oJRfTuiZNb92lyJirNSUMbddguy8yraYnk6GmR10QOCSucDxon1liMdOMZ2gQmu97bGaaBTKmyLm5cP0/Me0KQGQUhi3DhSajgq+tdclBvJuixlO4me1QEmnv9yFg9r+d2V0b3WzXGXrq+v2dI6S6v5kBePDo/xU8HrQOa3pBCsiYLhuHwnvHVGLpt5255Dh4boqS8n5MxH5/c1/asW5utVIyDid6WkCgTWDuFcn/9vx5IsU2pN7OaiKR2BfkWwrXfWUHrzwdEP7T2wPYcHV8lyodSChBV8aRPlSTiREg/G6Pt7HpZ7Z9+L8FTyIeya3QLgoywHVnhdVJW0ZYnbipFmaEhQrUJVggBfWmCGyqfO7RuwtZv0ULdNpvnyuQkfan+QM/AQKMPHUZ2EAWAAAAAElFTkSuQmCC";
},
"11b0":function b0(e,l){
e.exports=function(e){
if("undefined"!=typeof Symbol&&null!=e[Symbol.iterator]||null!=e["@@iterator"])return Array.from(e);
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"18ba":function ba(e,l){},
"1c67":function c67(e,l,a){
"use strict";
(function(l){
a("4ea4")(a("66fd")),e.exports={
getStorage:function getStorage(e){
try{
return l.getStorageSync(e);
}catch(e){
return null;
}
},
setStorage:function setStorage(e,a){
try{
l.setStorageSync(e,a);
}catch(e){
throw new Error("setStorage Error");
}
},
removeStorage:function removeStorage(e){
l.removeStorage({
key:e});

},
getUrlParam:function getUrlParam(e){
var l=new RegExp("(^|&)"+e+"=([^&]*)(&|$)","i"),a=window.location.search.substr(1).match(l);
return null!=a?decodeURIComponent(a[2]):null;
},
addUrlParam:function addUrlParam(e,l,a){
a=a||window.location.href;
var t=new RegExp("([?&])"+e+"=.*?(&|$)","i"),n=-1!==a.indexOf("?")?"&":"?";
return a.match(t)?a.replace(t,"$1"+e+"="+l+"$2"):a+n+e+"="+l;
},
isIOS:function isIOS(){
return"ios"===l.getSystemInfoSync().platform;
},
isAndroid:function isAndroid(){
return"android"===l.getSystemInfoSync().platform;
},
isIOSWeb:function isIOSWeb(){
return!0===window.__wxjs_is_wkwebview;
},
isPhoneNumber:function isPhoneNumber(e){
return /^[1]\d{10}$/.test(e);
},
isEmail:function isEmail(e){
return /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/.test(e);
},
isTaxNumber:function isTaxNumber(e){
return /^[0-9a-zA-Z]{15,20}$/.test(e);
},
checkNickName:function checkNickName(e){
return /^[A-Za-z0-9\u4e00-\u9fa5\-\_]{1,15}$/.test(e);
},
maskPhone:function maskPhone(e){
return null!=e&&null!=e?e.replace(/(\d{3})\d*(\d{4})/,"$1****$2"):"";
},
now:function now(e){
return this.formatDate(null,e);
},
formatPrice:function formatPrice(e){
return(e/100).toFixed(2);
},
formatPrices:function formatPrices(e){
return e/100;
},
formatDate:function formatDate(e,l){
if("string"==typeof e){
10===e.length&&(e+=" 00:00:00");
var a=e.split(/[- :]/),t=new Date(a[0],a[1]-1,a[2],a[3],a[4],a[5]),n=new Date(t);
}else n=new Date();
var r={
"M+":n.getMonth()+1,
"d+":n.getDate(),
"h+":n.getHours(),
"m+":n.getMinutes(),
"s+":n.getSeconds(),
"q+":Math.floor((n.getMonth()+3)/3),
S:n.getMilliseconds()};

for(var u in /(y+)/.test(l)&&(l=l.replace(RegExp.$1,(n.getFullYear()+"").substr(4-RegExp.$1.length))),
r){new RegExp("("+u+")").test(l)&&(l=l.replace(RegExp.$1,1==RegExp.$1.length?r[u]:("00"+r[u]).substr((""+r[u]).length)));}
return l;
},
showShortTime:function showShortTime(e){
var l=e.split(/[- :]/),a=new Date(l[0],l[1]-1,l[2],l[3],l[4],l[5]),t=new Date().getTime()-a.getTime(),n=Math.floor(t/864e5),r=t%864e5,u=Math.floor(r/36e5),i=r%36e5,o=Math.floor(i/6e4);
return n>4?this.formatDate(e,"yyyy/MM/dd hh:mm"):n>0?"".concat(n,"天").concat(u,"小时前"):u>0?"".concat(u,"小时前"):o>4?"".concat(o,"分钟前"):"刚刚";
},
scrollToTop:function scrollToTop(){
window.scrollTo(0,0);
},
previewImage:function previewImage(e,a){
a=a||0,"string"==typeof e&&(e=[e]),l.previewImage({
urls:e,
current:a});

},
copyText:function copyText(e,a){
a=a||"复制成功",l.setClipboardData({
data:e,
success:function success(){
l.showToast({
title:a,
icon:"none"});

}});

},
devTips:function devTips(){
l.showToast({
title:"此页面开发中",
icon:"none"});

},
toPage:function toPage(e){
l.navigateTo({
url:e});

}};

}).call(this,a("543d").default);
},
"1e0a":function e0a(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.getCouponList=function(e){
return(0,u.default)({
url:"/coupons",
method:"get",
data:e});

},l.getUsableCoupon=function(e){
return i.apply(this,arguments);
},l.pickCoupon=function(e){
return o.apply(this,arguments);
};
var n=t(a("2eee")),r=t(a("c973")),u=t(a("0863"));
function i(){
return(i=(0,r.default)(n.default.mark(function e(l){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/usable-coupons",
method:"get",
data:l});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function o(){
return(o=(0,r.default)(n.default.mark(function e(l){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/pick-coupon",
method:"post",
data:{
code:l}});



case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
},
2000:function _(e,l,a){
"use strict";
(function(l){
var t=a("4ea4"),n=t(a("9523")),r=(a("cbb4"),a("2684")),u=t(a("0863")),i=t(a("a8b1")),o=t(a("475c"));
function s(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function c(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?s(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):s(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var v={
app:"",
device:{
width:r.DEVICE_INFO.width,
height:r.DEVICE_INFO.height,
is_wifi:0},

client:{
type:"miniapp",
version:"2.0"}};


e.exports={
record:function record(e,a){
if(!o.default.state.setting.is_log_visitor)return!1;
"string"==typeof e&&(e={
type:e}),
a=a||{};
var t=c(c(c({},v),a),{},{
page:e,
scene_id:o.default.state.scene_id,
wechat:{
gender:i.default.state.wechatInfo.gender?"男":"女",
nickname:i.default.state.wechatInfo.nickName,
headimgurl:i.default.state.wechatInfo.avatarUrl}});


l.getNetworkType({
success:function success(e){
v.device.is_wifi="wifi"===e.networkType?1:0,setTimeout(function(){
(0,u.default)("/visitors","POST",t);
},600);
}});

}};

}).call(this,a("543d").default);
},
2236:function _(e,l,a){
var t=a("5a43");
e.exports=function(e){
if(Array.isArray(e))return t(e);
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
2684:function _(e,l,a){
(function(l){
var a,t=l.getSystemInfoSync(),n=t.statusBarHeight;
a="android"==t.platform?t.statusBarHeight+48:t.statusBarHeight+44;
var r={
height:t.screenHeight,
width:t.screenWidth,
system:t.system,
model:t.model,
platform:t.platform,
statusBar:n,
customBar:a};

e.exports={
VERSIONS:"1.0.0",
BASE_URL:"https://api.we213.fuzhouweiyi.top",
DEVICE_INFO:r};

}).call(this,a("543d").default);
},
"26cb":function cb(e,l,a){
"use strict";
(function(l){
var a=("undefined"!=typeof window?window:void 0!==l?l:{}).__VUE_DEVTOOLS_GLOBAL_HOOK__;
function t(e,l){
if(void 0===l&&(l=[]),null===e||"object"!=typeof e)return e;
var a=function(e,l){
return e.filter(l)[0];
}(l,function(l){
return l.original===e;
});
if(a)return a.copy;
var n=Array.isArray(e)?[]:{};
return l.push({
original:e,
copy:n}),
Object.keys(e).forEach(function(a){
n[a]=t(e[a],l);
}),n;
}
function n(e,l){
Object.keys(e).forEach(function(a){
return l(e[a],a);
});
}
function r(e){
return null!==e&&"object"==typeof e;
}
var u=function u(e,l){
this.runtime=l,this._children=Object.create(null),this._rawModule=e;
var a=e.state;
this.state=("function"==typeof a?a():a)||{};
},i={
namespaced:{
configurable:!0}};


i.namespaced.get=function(){
return!!this._rawModule.namespaced;
},u.prototype.addChild=function(e,l){
this._children[e]=l;
},u.prototype.removeChild=function(e){
delete this._children[e];
},u.prototype.getChild=function(e){
return this._children[e];
},u.prototype.hasChild=function(e){
return e in this._children;
},u.prototype.update=function(e){
this._rawModule.namespaced=e.namespaced,e.actions&&(this._rawModule.actions=e.actions),
e.mutations&&(this._rawModule.mutations=e.mutations),e.getters&&(this._rawModule.getters=e.getters);
},u.prototype.forEachChild=function(e){
n(this._children,e);
},u.prototype.forEachGetter=function(e){
this._rawModule.getters&&n(this._rawModule.getters,e);
},u.prototype.forEachAction=function(e){
this._rawModule.actions&&n(this._rawModule.actions,e);
},u.prototype.forEachMutation=function(e){
this._rawModule.mutations&&n(this._rawModule.mutations,e);
},Object.defineProperties(u.prototype,i);
var o,s=function s(e){
this.register([],e,!1);
};
s.prototype.get=function(e){
return e.reduce(function(e,l){
return e.getChild(l);
},this.root);
},s.prototype.getNamespace=function(e){
var l=this.root;
return e.reduce(function(e,a){
return e+((l=l.getChild(a)).namespaced?a+"/":"");
},"");
},s.prototype.update=function(e){
!function e(l,a,t){
if(a.update(t),t.modules)for(var n in t.modules){
if(!a.getChild(n))return;
e(l.concat(n),a.getChild(n),t.modules[n]);
}
}([],this.root,e);
},s.prototype.register=function(e,l,a){
var t=this;
void 0===a&&(a=!0);
var r=new u(l,a);
0===e.length?this.root=r:this.get(e.slice(0,-1)).addChild(e[e.length-1],r),
l.modules&&n(l.modules,function(l,n){
t.register(e.concat(n),l,a);
});
},s.prototype.unregister=function(e){
var l=this.get(e.slice(0,-1)),a=e[e.length-1],t=l.getChild(a);
t&&t.runtime&&l.removeChild(a);
},s.prototype.isRegistered=function(e){
var l=this.get(e.slice(0,-1)),a=e[e.length-1];
return!!l&&l.hasChild(a);
};
var c=function c(e){
var l=this;
void 0===e&&(e={}),!o&&"undefined"!=typeof window&&window.Vue&&m(window.Vue);
var t=e.plugins;
void 0===t&&(t=[]);
var n=e.strict;
void 0===n&&(n=!1),this._committing=!1,this._actions=Object.create(null),
this._actionSubscribers=[],this._mutations=Object.create(null),this._wrappedGetters=Object.create(null),
this._modules=new s(e),this._modulesNamespaceMap=Object.create(null),this._subscribers=[],
this._watcherVM=new o(),this._makeLocalGettersCache=Object.create(null);
var r=this,u=this.dispatch,i=this.commit;
this.dispatch=function(e,l){
return u.call(r,e,l);
},this.commit=function(e,l,a){
return i.call(r,e,l,a);
},this.strict=n;
var c=this._modules.root.state;
d(this,c,[],this._modules.root),h(this,c),t.forEach(function(e){
return e(l);
}),(void 0!==e.devtools?e.devtools:o.config.devtools)&&function(e){
a&&(e._devtoolHook=a,a.emit("vuex:init",e),a.on("vuex:travel-to-state",function(l){
e.replaceState(l);
}),e.subscribe(function(e,l){
a.emit("vuex:mutation",e,l);
},{
prepend:!0}),
e.subscribeAction(function(e,l){
a.emit("vuex:action",e,l);
},{
prepend:!0}));

}(this);
},v={
state:{
configurable:!0}};


function b(e,l,a){
return l.indexOf(e)<0&&(a&&a.prepend?l.unshift(e):l.push(e)),function(){
var a=l.indexOf(e);
a>-1&&l.splice(a,1);
};
}
function f(e,l){
e._actions=Object.create(null),e._mutations=Object.create(null),e._wrappedGetters=Object.create(null),
e._modulesNamespaceMap=Object.create(null);
var a=e.state;
d(e,a,[],e._modules.root,!0),h(e,a,l);
}
function h(e,l,a){
var t=e._vm;
e.getters={},e._makeLocalGettersCache=Object.create(null);
var r=e._wrappedGetters,u={};
n(r,function(l,a){
u[a]=function(e,l){
return function(){
return e(l);
};
}(l,e),Object.defineProperty(e.getters,a,{
get:function get(){
return e._vm[a];
},
enumerable:!0});

});
var i=o.config.silent;
o.config.silent=!0,e._vm=new o({
data:{
$$state:l},

computed:u}),
o.config.silent=i,e.strict&&function(e){
e._vm.$watch(function(){
return this._data.$$state;
},function(){},{
deep:!0,
sync:!0});

}(e),t&&(a&&e._withCommit(function(){
t._data.$$state=null;
}),o.nextTick(function(){
return t.$destroy();
}));
}
function d(e,l,a,t,n){
var r=!a.length,u=e._modules.getNamespace(a);
if(t.namespaced&&(e._modulesNamespaceMap[u],e._modulesNamespaceMap[u]=t),
!r&&!n){
var i=p(l,a.slice(0,-1)),s=a[a.length-1];
e._withCommit(function(){
o.set(i,s,t.state);
});
}
var c=t.context=function(e,l,a){
var t=""===l,n={
dispatch:t?e.dispatch:function(a,t,n){
var r=g(a,t,n),u=r.payload,i=r.options,o=r.type;
return i&&i.root||(o=l+o),e.dispatch(o,u);
},
commit:t?e.commit:function(a,t,n){
var r=g(a,t,n),u=r.payload,i=r.options,o=r.type;
i&&i.root||(o=l+o),e.commit(o,u,i);
}};

return Object.defineProperties(n,{
getters:{
get:t?function(){
return e.getters;
}:function(){
return function(e,l){
if(!e._makeLocalGettersCache[l]){
var a={},t=l.length;
Object.keys(e.getters).forEach(function(n){
if(n.slice(0,t)===l){
var r=n.slice(t);
Object.defineProperty(a,r,{
get:function get(){
return e.getters[n];
},
enumerable:!0});

}
}),e._makeLocalGettersCache[l]=a;
}
return e._makeLocalGettersCache[l];
}(e,l);
}},

state:{
get:function get(){
return p(e.state,a);
}}}),

n;
}(e,u,a);
t.forEachMutation(function(l,a){
!function(e,l,a,t){
(e._mutations[l]||(e._mutations[l]=[])).push(function(l){
a.call(e,t.state,l);
});
}(e,u+a,l,c);
}),t.forEachAction(function(l,a){
var t=l.root?a:u+a,n=l.handler||l;
!function(e,l,a,t){
(e._actions[l]||(e._actions[l]=[])).push(function(l){
var n=a.call(e,{
dispatch:t.dispatch,
commit:t.commit,
getters:t.getters,
state:t.state,
rootGetters:e.getters,
rootState:e.state},
l);
return function(e){
return e&&"function"==typeof e.then;
}(n)||(n=Promise.resolve(n)),e._devtoolHook?n.catch(function(l){
throw e._devtoolHook.emit("vuex:error",l),l;
}):n;
});
}(e,t,n,c);
}),t.forEachGetter(function(l,a){
!function(e,l,a,t){
e._wrappedGetters[l]||(e._wrappedGetters[l]=function(e){
return a(t.state,t.getters,e.state,e.getters);
});
}(e,u+a,l,c);
}),t.forEachChild(function(t,r){
d(e,l,a.concat(r),t,n);
});
}
function p(e,l){
return l.reduce(function(e,l){
return e[l];
},e);
}
function g(e,l,a){
return r(e)&&e.type&&(a=l,l=e,e=e.type),{
type:e,
payload:l,
options:a};

}
function m(e){
o&&e===o||function(e){
if(Number(e.version.split(".")[0])>=2)e.mixin({
beforeCreate:a});else
{
var l=e.prototype._init;
e.prototype._init=function(e){
void 0===e&&(e={}),e.init=e.init?[a].concat(e.init):a,l.call(this,e);
};
}
function a(){
var e=this.$options;
e.store?this.$store="function"==typeof e.store?e.store():e.store:e.parent&&e.parent.$store&&(this.$store=e.parent.$store);
}
}(o=e);
}
v.state.get=function(){
return this._vm._data.$$state;
},v.state.set=function(e){},c.prototype.commit=function(e,l,a){
var t=this,n=g(e,l,a),r=n.type,u=n.payload,i=(n.options,{
type:r,
payload:u}),
o=this._mutations[r];
o&&(this._withCommit(function(){
o.forEach(function(e){
e(u);
});
}),this._subscribers.slice().forEach(function(e){
return e(i,t.state);
}));
},c.prototype.dispatch=function(e,l){
var a=this,t=g(e,l),n=t.type,r=t.payload,u={
type:n,
payload:r},
i=this._actions[n];
if(i){
try{
this._actionSubscribers.slice().filter(function(e){
return e.before;
}).forEach(function(e){
return e.before(u,a.state);
});
}catch(e){}
var o=i.length>1?Promise.all(i.map(function(e){
return e(r);
})):i[0](r);
return new Promise(function(e,l){
o.then(function(l){
try{
a._actionSubscribers.filter(function(e){
return e.after;
}).forEach(function(e){
return e.after(u,a.state);
});
}catch(e){}
e(l);
},function(e){
try{
a._actionSubscribers.filter(function(e){
return e.error;
}).forEach(function(l){
return l.error(u,a.state,e);
});
}catch(e){}
l(e);
});
});
}
},c.prototype.subscribe=function(e,l){
return b(e,this._subscribers,l);
},c.prototype.subscribeAction=function(e,l){
return b("function"==typeof e?{
before:e}:
e,this._actionSubscribers,l);
},c.prototype.watch=function(e,l,a){
var t=this;
return this._watcherVM.$watch(function(){
return e(t.state,t.getters);
},l,a);
},c.prototype.replaceState=function(e){
var l=this;
this._withCommit(function(){
l._vm._data.$$state=e;
});
},c.prototype.registerModule=function(e,l,a){
void 0===a&&(a={}),"string"==typeof e&&(e=[e]),this._modules.register(e,l),
d(this,this.state,e,this._modules.get(e),a.preserveState),h(this,this.state);
},c.prototype.unregisterModule=function(e){
var l=this;
"string"==typeof e&&(e=[e]),this._modules.unregister(e),this._withCommit(function(){
var a=p(l.state,e.slice(0,-1));
o.delete(a,e[e.length-1]);
}),f(this);
},c.prototype.hasModule=function(e){
return"string"==typeof e&&(e=[e]),this._modules.isRegistered(e);
},c.prototype[[104,111,116,85,112,100,97,116,101].map(function(e){
return String.fromCharCode(e);
}).join("")]=function(e){
this._modules.update(e),f(this,!0);
},c.prototype._withCommit=function(e){
var l=this._committing;
this._committing=!0,e(),this._committing=l;
},Object.defineProperties(c.prototype,v);
var y=x(function(e,l){
var a={};
return O(l).forEach(function(l){
var t=l.key,n=l.val;
a[t]=function(){
var l=this.$store.state,a=this.$store.getters;
if(e){
var t=k(this.$store,0,e);
if(!t)return;
l=t.context.state,a=t.context.getters;
}
return"function"==typeof n?n.call(this,l,a):l[n];
},a[t].vuex=!0;
}),a;
}),w=x(function(e,l){
var a={};
return O(l).forEach(function(l){
var t=l.key,n=l.val;
a[t]=function(){
for(var l=[],a=arguments.length;a--;){l[a]=arguments[a];}
var t=this.$store.commit;
if(e){
var r=k(this.$store,0,e);
if(!r)return;
t=r.context.commit;
}
return"function"==typeof n?n.apply(this,[t].concat(l)):t.apply(this.$store,[n].concat(l));
};
}),a;
}),_=x(function(e,l){
var a={};
return O(l).forEach(function(l){
var t=l.key,n=l.val;
n=e+n,a[t]=function(){
if(!e||k(this.$store,0,e))return this.$store.getters[n];
},a[t].vuex=!0;
}),a;
}),S=x(function(e,l){
var a={};
return O(l).forEach(function(l){
var t=l.key,n=l.val;
a[t]=function(){
for(var l=[],a=arguments.length;a--;){l[a]=arguments[a];}
var t=this.$store.dispatch;
if(e){
var r=k(this.$store,0,e);
if(!r)return;
t=r.context.dispatch;
}
return"function"==typeof n?n.apply(this,[t].concat(l)):t.apply(this.$store,[n].concat(l));
};
}),a;
});
function O(e){
return function(e){
return Array.isArray(e)||r(e);
}(e)?Array.isArray(e)?e.map(function(e){
return{
key:e,
val:e};

}):Object.keys(e).map(function(l){
return{
key:l,
val:e[l]};

}):[];
}
function x(e){
return function(l,a){
return"string"!=typeof l?(a=l,l=""):"/"!==l.charAt(l.length-1)&&(l+="/"),
e(l,a);
};
}
function k(e,l,a){
return e._modulesNamespaceMap[a];
}
function A(e,l,a){
var t=a?e.groupCollapsed:e.group;
try{
t.call(e,l);
}catch(a){
e.log(l);
}
}
function P(e){
try{
e.groupEnd();
}catch(l){
e.log("—— log end ——");
}
}
function T(){
var e=new Date();
return" @ "+M(e.getHours(),2)+":"+M(e.getMinutes(),2)+":"+M(e.getSeconds(),2)+"."+M(e.getMilliseconds(),3);
}
function M(e,l){
return function(e,l){
return new Array(l+1).join("0");
}(0,l-e.toString().length)+e;
}
var E={
Store:c,
install:m,
version:"3.6.2",
mapState:y,
mapMutations:w,
mapGetters:_,
mapActions:S,
createNamespacedHelpers:function createNamespacedHelpers(e){
return{
mapState:y.bind(null,e),
mapGetters:_.bind(null,e),
mapMutations:w.bind(null,e),
mapActions:S.bind(null,e)};

},
createLogger:function createLogger(e){
void 0===e&&(e={});
var l=e.collapsed;
void 0===l&&(l=!0);
var a=e.filter;
void 0===a&&(a=function a(e,l,_a2){
return!0;
});
var n=e.transformer;
void 0===n&&(n=function n(e){
return e;
});
var r=e.mutationTransformer;
void 0===r&&(r=function r(e){
return e;
});
var u=e.actionFilter;
void 0===u&&(u=function u(e,l){
return!0;
});
var i=e.actionTransformer;
void 0===i&&(i=function i(e){
return e;
});
var o=e.logMutations;
void 0===o&&(o=!0);
var s=e.logActions;
void 0===s&&(s=!0);
var c=e.logger;
return void 0===c&&(c=console),function(e){
var v=t(e.state);
void 0!==c&&(o&&e.subscribe(function(e,u){
var i=t(u);
if(a(e,v,i)){
var o=T(),s=r(e),b="mutation "+e.type+o;
A(c,b,l),c.log("%c prev state","color: #9E9E9E; font-weight: bold",n(v)),c.log("%c mutation","color: #03A9F4; font-weight: bold",s),
c.log("%c next state","color: #4CAF50; font-weight: bold",n(i)),P(c);
}
v=i;
}),s&&e.subscribeAction(function(e,a){
if(u(e,a)){
var t=T(),n=i(e),r="action "+e.type+t;
A(c,r,l),c.log("%c action","color: #03A9F4; font-weight: bold",n),P(c);
}
}));
};
}};

e.exports=E;
}).call(this,a("c8ba"));
},
"278c":function c(e,l,a){
var t=a("c135"),n=a("9b42"),r=a("6613"),u=a("c240");
e.exports=function(e,l){
return t(e)||n(e,l)||r(e,l)||u();
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"29fe":function fe(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,l.default=[[[{
label:"东城区",
value:"110101"},
{
label:"西城区",
value:"110102"},
{
label:"朝阳区",
value:"110105"},
{
label:"丰台区",
value:"110106"},
{
label:"石景山区",
value:"110107"},
{
label:"海淀区",
value:"110108"},
{
label:"门头沟区",
value:"110109"},
{
label:"房山区",
value:"110111"},
{
label:"通州区",
value:"110112"},
{
label:"顺义区",
value:"110113"},
{
label:"昌平区",
value:"110114"},
{
label:"大兴区",
value:"110115"},
{
label:"怀柔区",
value:"110116"},
{
label:"平谷区",
value:"110117"},
{
label:"密云区",
value:"110118"},
{
label:"延庆区",
value:"110119"}]],
[[{
label:"和平区",
value:"120101"},
{
label:"河东区",
value:"120102"},
{
label:"河西区",
value:"120103"},
{
label:"南开区",
value:"120104"},
{
label:"河北区",
value:"120105"},
{
label:"红桥区",
value:"120106"},
{
label:"东丽区",
value:"120110"},
{
label:"西青区",
value:"120111"},
{
label:"津南区",
value:"120112"},
{
label:"北辰区",
value:"120113"},
{
label:"武清区",
value:"120114"},
{
label:"宝坻区",
value:"120115"},
{
label:"滨海新区",
value:"120116"},
{
label:"宁河区",
value:"120117"},
{
label:"静海区",
value:"120118"},
{
label:"蓟州区",
value:"120119"}]],
[[{
label:"长安区",
value:"130102"},
{
label:"桥西区",
value:"130104"},
{
label:"新华区",
value:"130105"},
{
label:"井陉矿区",
value:"130107"},
{
label:"裕华区",
value:"130108"},
{
label:"藁城区",
value:"130109"},
{
label:"鹿泉区",
value:"130110"},
{
label:"栾城区",
value:"130111"},
{
label:"井陉县",
value:"130121"},
{
label:"正定县",
value:"130123"},
{
label:"行唐县",
value:"130125"},
{
label:"灵寿县",
value:"130126"},
{
label:"高邑县",
value:"130127"},
{
label:"深泽县",
value:"130128"},
{
label:"赞皇县",
value:"130129"},
{
label:"无极县",
value:"130130"},
{
label:"平山县",
value:"130131"},
{
label:"元氏县",
value:"130132"},
{
label:"赵县",
value:"130133"},
{
label:"石家庄高新技术产业开发区",
value:"130171"},
{
label:"石家庄循环化工园区",
value:"130172"},
{
label:"辛集市",
value:"130181"},
{
label:"晋州市",
value:"130183"},
{
label:"新乐市",
value:"130184"}],
[{
label:"路南区",
value:"130202"},
{
label:"路北区",
value:"130203"},
{
label:"古冶区",
value:"130204"},
{
label:"开平区",
value:"130205"},
{
label:"丰南区",
value:"130207"},
{
label:"丰润区",
value:"130208"},
{
label:"曹妃甸区",
value:"130209"},
{
label:"滦县",
value:"130223"},
{
label:"滦南县",
value:"130224"},
{
label:"乐亭县",
value:"130225"},
{
label:"迁西县",
value:"130227"},
{
label:"玉田县",
value:"130229"},
{
label:"唐山市芦台经济技术开发区",
value:"130271"},
{
label:"唐山市汉沽管理区",
value:"130272"},
{
label:"唐山高新技术产业开发区",
value:"130273"},
{
label:"河北唐山海港经济开发区",
value:"130274"},
{
label:"遵化市",
value:"130281"},
{
label:"迁安市",
value:"130283"}],
[{
label:"海港区",
value:"130302"},
{
label:"山海关区",
value:"130303"},
{
label:"北戴河区",
value:"130304"},
{
label:"抚宁区",
value:"130306"},
{
label:"青龙满族自治县",
value:"130321"},
{
label:"昌黎县",
value:"130322"},
{
label:"卢龙县",
value:"130324"},
{
label:"秦皇岛市经济技术开发区",
value:"130371"},
{
label:"北戴河新区",
value:"130372"}],
[{
label:"邯山区",
value:"130402"},
{
label:"丛台区",
value:"130403"},
{
label:"复兴区",
value:"130404"},
{
label:"峰峰矿区",
value:"130406"},
{
label:"肥乡区",
value:"130407"},
{
label:"永年区",
value:"130408"},
{
label:"临漳县",
value:"130423"},
{
label:"成安县",
value:"130424"},
{
label:"大名县",
value:"130425"},
{
label:"涉县",
value:"130426"},
{
label:"磁县",
value:"130427"},
{
label:"邱县",
value:"130430"},
{
label:"鸡泽县",
value:"130431"},
{
label:"广平县",
value:"130432"},
{
label:"馆陶县",
value:"130433"},
{
label:"魏县",
value:"130434"},
{
label:"曲周县",
value:"130435"},
{
label:"邯郸经济技术开发区",
value:"130471"},
{
label:"邯郸冀南新区",
value:"130473"},
{
label:"武安市",
value:"130481"}],
[{
label:"桥东区",
value:"130502"},
{
label:"桥西区",
value:"130503"},
{
label:"邢台县",
value:"130521"},
{
label:"临城县",
value:"130522"},
{
label:"内丘县",
value:"130523"},
{
label:"柏乡县",
value:"130524"},
{
label:"隆尧县",
value:"130525"},
{
label:"任县",
value:"130526"},
{
label:"南和县",
value:"130527"},
{
label:"宁晋县",
value:"130528"},
{
label:"巨鹿县",
value:"130529"},
{
label:"新河县",
value:"130530"},
{
label:"广宗县",
value:"130531"},
{
label:"平乡县",
value:"130532"},
{
label:"威县",
value:"130533"},
{
label:"清河县",
value:"130534"},
{
label:"临西县",
value:"130535"},
{
label:"河北邢台经济开发区",
value:"130571"},
{
label:"南宫市",
value:"130581"},
{
label:"沙河市",
value:"130582"}],
[{
label:"竞秀区",
value:"130602"},
{
label:"莲池区",
value:"130606"},
{
label:"满城区",
value:"130607"},
{
label:"清苑区",
value:"130608"},
{
label:"徐水区",
value:"130609"},
{
label:"涞水县",
value:"130623"},
{
label:"阜平县",
value:"130624"},
{
label:"定兴县",
value:"130626"},
{
label:"唐县",
value:"130627"},
{
label:"高阳县",
value:"130628"},
{
label:"容城县",
value:"130629"},
{
label:"涞源县",
value:"130630"},
{
label:"望都县",
value:"130631"},
{
label:"安新县",
value:"130632"},
{
label:"易县",
value:"130633"},
{
label:"曲阳县",
value:"130634"},
{
label:"蠡县",
value:"130635"},
{
label:"顺平县",
value:"130636"},
{
label:"博野县",
value:"130637"},
{
label:"雄县",
value:"130638"},
{
label:"保定高新技术产业开发区",
value:"130671"},
{
label:"保定白沟新城",
value:"130672"},
{
label:"涿州市",
value:"130681"},
{
label:"定州市",
value:"130682"},
{
label:"安国市",
value:"130683"},
{
label:"高碑店市",
value:"130684"}],
[{
label:"桥东区",
value:"130702"},
{
label:"桥西区",
value:"130703"},
{
label:"宣化区",
value:"130705"},
{
label:"下花园区",
value:"130706"},
{
label:"万全区",
value:"130708"},
{
label:"崇礼区",
value:"130709"},
{
label:"张北县",
value:"130722"},
{
label:"康保县",
value:"130723"},
{
label:"沽源县",
value:"130724"},
{
label:"尚义县",
value:"130725"},
{
label:"蔚县",
value:"130726"},
{
label:"阳原县",
value:"130727"},
{
label:"怀安县",
value:"130728"},
{
label:"怀来县",
value:"130730"},
{
label:"涿鹿县",
value:"130731"},
{
label:"赤城县",
value:"130732"},
{
label:"张家口市高新技术产业开发区",
value:"130771"},
{
label:"张家口市察北管理区",
value:"130772"},
{
label:"张家口市塞北管理区",
value:"130773"}],
[{
label:"双桥区",
value:"130802"},
{
label:"双滦区",
value:"130803"},
{
label:"鹰手营子矿区",
value:"130804"},
{
label:"承德县",
value:"130821"},
{
label:"兴隆县",
value:"130822"},
{
label:"滦平县",
value:"130824"},
{
label:"隆化县",
value:"130825"},
{
label:"丰宁满族自治县",
value:"130826"},
{
label:"宽城满族自治县",
value:"130827"},
{
label:"围场满族蒙古族自治县",
value:"130828"},
{
label:"承德高新技术产业开发区",
value:"130871"},
{
label:"平泉市",
value:"130881"}],
[{
label:"新华区",
value:"130902"},
{
label:"运河区",
value:"130903"},
{
label:"沧县",
value:"130921"},
{
label:"青县",
value:"130922"},
{
label:"东光县",
value:"130923"},
{
label:"海兴县",
value:"130924"},
{
label:"盐山县",
value:"130925"},
{
label:"肃宁县",
value:"130926"},
{
label:"南皮县",
value:"130927"},
{
label:"吴桥县",
value:"130928"},
{
label:"献县",
value:"130929"},
{
label:"孟村回族自治县",
value:"130930"},
{
label:"河北沧州经济开发区",
value:"130971"},
{
label:"沧州高新技术产业开发区",
value:"130972"},
{
label:"沧州渤海新区",
value:"130973"},
{
label:"泊头市",
value:"130981"},
{
label:"任丘市",
value:"130982"},
{
label:"黄骅市",
value:"130983"},
{
label:"河间市",
value:"130984"}],
[{
label:"安次区",
value:"131002"},
{
label:"广阳区",
value:"131003"},
{
label:"固安县",
value:"131022"},
{
label:"永清县",
value:"131023"},
{
label:"香河县",
value:"131024"},
{
label:"大城县",
value:"131025"},
{
label:"文安县",
value:"131026"},
{
label:"大厂回族自治县",
value:"131028"},
{
label:"廊坊经济技术开发区",
value:"131071"},
{
label:"霸州市",
value:"131081"},
{
label:"三河市",
value:"131082"}],
[{
label:"桃城区",
value:"131102"},
{
label:"冀州区",
value:"131103"},
{
label:"枣强县",
value:"131121"},
{
label:"武邑县",
value:"131122"},
{
label:"武强县",
value:"131123"},
{
label:"饶阳县",
value:"131124"},
{
label:"安平县",
value:"131125"},
{
label:"故城县",
value:"131126"},
{
label:"景县",
value:"131127"},
{
label:"阜城县",
value:"131128"},
{
label:"河北衡水经济开发区",
value:"131171"},
{
label:"衡水滨湖新区",
value:"131172"},
{
label:"深州市",
value:"131182"}]],
[[{
label:"小店区",
value:"140105"},
{
label:"迎泽区",
value:"140106"},
{
label:"杏花岭区",
value:"140107"},
{
label:"尖草坪区",
value:"140108"},
{
label:"万柏林区",
value:"140109"},
{
label:"晋源区",
value:"140110"},
{
label:"清徐县",
value:"140121"},
{
label:"阳曲县",
value:"140122"},
{
label:"娄烦县",
value:"140123"},
{
label:"山西转型综合改革示范区",
value:"140171"},
{
label:"古交市",
value:"140181"}],
[{
label:"城区",
value:"140202"},
{
label:"矿区",
value:"140203"},
{
label:"南郊区",
value:"140211"},
{
label:"新荣区",
value:"140212"},
{
label:"阳高县",
value:"140221"},
{
label:"天镇县",
value:"140222"},
{
label:"广灵县",
value:"140223"},
{
label:"灵丘县",
value:"140224"},
{
label:"浑源县",
value:"140225"},
{
label:"左云县",
value:"140226"},
{
label:"大同县",
value:"140227"},
{
label:"山西大同经济开发区",
value:"140271"}],
[{
label:"城区",
value:"140302"},
{
label:"矿区",
value:"140303"},
{
label:"郊区",
value:"140311"},
{
label:"平定县",
value:"140321"},
{
label:"盂县",
value:"140322"},
{
label:"山西阳泉经济开发区",
value:"140371"}],
[{
label:"城区",
value:"140402"},
{
label:"郊区",
value:"140411"},
{
label:"长治县",
value:"140421"},
{
label:"襄垣县",
value:"140423"},
{
label:"屯留县",
value:"140424"},
{
label:"平顺县",
value:"140425"},
{
label:"黎城县",
value:"140426"},
{
label:"壶关县",
value:"140427"},
{
label:"长子县",
value:"140428"},
{
label:"武乡县",
value:"140429"},
{
label:"沁县",
value:"140430"},
{
label:"沁源县",
value:"140431"},
{
label:"山西长治高新技术产业园区",
value:"140471"},
{
label:"潞城市",
value:"140481"}],
[{
label:"城区",
value:"140502"},
{
label:"沁水县",
value:"140521"},
{
label:"阳城县",
value:"140522"},
{
label:"陵川县",
value:"140524"},
{
label:"泽州县",
value:"140525"},
{
label:"高平市",
value:"140581"}],
[{
label:"朔城区",
value:"140602"},
{
label:"平鲁区",
value:"140603"},
{
label:"山阴县",
value:"140621"},
{
label:"应县",
value:"140622"},
{
label:"右玉县",
value:"140623"},
{
label:"怀仁县",
value:"140624"},
{
label:"山西朔州经济开发区",
value:"140671"}],
[{
label:"榆次区",
value:"140702"},
{
label:"榆社县",
value:"140721"},
{
label:"左权县",
value:"140722"},
{
label:"和顺县",
value:"140723"},
{
label:"昔阳县",
value:"140724"},
{
label:"寿阳县",
value:"140725"},
{
label:"太谷县",
value:"140726"},
{
label:"祁县",
value:"140727"},
{
label:"平遥县",
value:"140728"},
{
label:"灵石县",
value:"140729"},
{
label:"介休市",
value:"140781"}],
[{
label:"盐湖区",
value:"140802"},
{
label:"临猗县",
value:"140821"},
{
label:"万荣县",
value:"140822"},
{
label:"闻喜县",
value:"140823"},
{
label:"稷山县",
value:"140824"},
{
label:"新绛县",
value:"140825"},
{
label:"绛县",
value:"140826"},
{
label:"垣曲县",
value:"140827"},
{
label:"夏县",
value:"140828"},
{
label:"平陆县",
value:"140829"},
{
label:"芮城县",
value:"140830"},
{
label:"永济市",
value:"140881"},
{
label:"河津市",
value:"140882"}],
[{
label:"忻府区",
value:"140902"},
{
label:"定襄县",
value:"140921"},
{
label:"五台县",
value:"140922"},
{
label:"代县",
value:"140923"},
{
label:"繁峙县",
value:"140924"},
{
label:"宁武县",
value:"140925"},
{
label:"静乐县",
value:"140926"},
{
label:"神池县",
value:"140927"},
{
label:"五寨县",
value:"140928"},
{
label:"岢岚县",
value:"140929"},
{
label:"河曲县",
value:"140930"},
{
label:"保德县",
value:"140931"},
{
label:"偏关县",
value:"140932"},
{
label:"五台山风景名胜区",
value:"140971"},
{
label:"原平市",
value:"140981"}],
[{
label:"尧都区",
value:"141002"},
{
label:"曲沃县",
value:"141021"},
{
label:"翼城县",
value:"141022"},
{
label:"襄汾县",
value:"141023"},
{
label:"洪洞县",
value:"141024"},
{
label:"古县",
value:"141025"},
{
label:"安泽县",
value:"141026"},
{
label:"浮山县",
value:"141027"},
{
label:"吉县",
value:"141028"},
{
label:"乡宁县",
value:"141029"},
{
label:"大宁县",
value:"141030"},
{
label:"隰县",
value:"141031"},
{
label:"永和县",
value:"141032"},
{
label:"蒲县",
value:"141033"},
{
label:"汾西县",
value:"141034"},
{
label:"侯马市",
value:"141081"},
{
label:"霍州市",
value:"141082"}],
[{
label:"离石区",
value:"141102"},
{
label:"文水县",
value:"141121"},
{
label:"交城县",
value:"141122"},
{
label:"兴县",
value:"141123"},
{
label:"临县",
value:"141124"},
{
label:"柳林县",
value:"141125"},
{
label:"石楼县",
value:"141126"},
{
label:"岚县",
value:"141127"},
{
label:"方山县",
value:"141128"},
{
label:"中阳县",
value:"141129"},
{
label:"交口县",
value:"141130"},
{
label:"孝义市",
value:"141181"},
{
label:"汾阳市",
value:"141182"}]],
[[{
label:"新城区",
value:"150102"},
{
label:"回民区",
value:"150103"},
{
label:"玉泉区",
value:"150104"},
{
label:"赛罕区",
value:"150105"},
{
label:"土默特左旗",
value:"150121"},
{
label:"托克托县",
value:"150122"},
{
label:"和林格尔县",
value:"150123"},
{
label:"清水河县",
value:"150124"},
{
label:"武川县",
value:"150125"},
{
label:"呼和浩特金海工业园区",
value:"150171"},
{
label:"呼和浩特经济技术开发区",
value:"150172"}],
[{
label:"东河区",
value:"150202"},
{
label:"昆都仑区",
value:"150203"},
{
label:"青山区",
value:"150204"},
{
label:"石拐区",
value:"150205"},
{
label:"白云鄂博矿区",
value:"150206"},
{
label:"九原区",
value:"150207"},
{
label:"土默特右旗",
value:"150221"},
{
label:"固阳县",
value:"150222"},
{
label:"达尔罕茂明安联合旗",
value:"150223"},
{
label:"包头稀土高新技术产业开发区",
value:"150271"}],
[{
label:"海勃湾区",
value:"150302"},
{
label:"海南区",
value:"150303"},
{
label:"乌达区",
value:"150304"}],
[{
label:"红山区",
value:"150402"},
{
label:"元宝山区",
value:"150403"},
{
label:"松山区",
value:"150404"},
{
label:"阿鲁科尔沁旗",
value:"150421"},
{
label:"巴林左旗",
value:"150422"},
{
label:"巴林右旗",
value:"150423"},
{
label:"林西县",
value:"150424"},
{
label:"克什克腾旗",
value:"150425"},
{
label:"翁牛特旗",
value:"150426"},
{
label:"喀喇沁旗",
value:"150428"},
{
label:"宁城县",
value:"150429"},
{
label:"敖汉旗",
value:"150430"}],
[{
label:"科尔沁区",
value:"150502"},
{
label:"科尔沁左翼中旗",
value:"150521"},
{
label:"科尔沁左翼后旗",
value:"150522"},
{
label:"开鲁县",
value:"150523"},
{
label:"库伦旗",
value:"150524"},
{
label:"奈曼旗",
value:"150525"},
{
label:"扎鲁特旗",
value:"150526"},
{
label:"通辽经济技术开发区",
value:"150571"},
{
label:"霍林郭勒市",
value:"150581"}],
[{
label:"东胜区",
value:"150602"},
{
label:"康巴什区",
value:"150603"},
{
label:"达拉特旗",
value:"150621"},
{
label:"准格尔旗",
value:"150622"},
{
label:"鄂托克前旗",
value:"150623"},
{
label:"鄂托克旗",
value:"150624"},
{
label:"杭锦旗",
value:"150625"},
{
label:"乌审旗",
value:"150626"},
{
label:"伊金霍洛旗",
value:"150627"}],
[{
label:"海拉尔区",
value:"150702"},
{
label:"扎赉诺尔区",
value:"150703"},
{
label:"阿荣旗",
value:"150721"},
{
label:"莫力达瓦达斡尔族自治旗",
value:"150722"},
{
label:"鄂伦春自治旗",
value:"150723"},
{
label:"鄂温克族自治旗",
value:"150724"},
{
label:"陈巴尔虎旗",
value:"150725"},
{
label:"新巴尔虎左旗",
value:"150726"},
{
label:"新巴尔虎右旗",
value:"150727"},
{
label:"满洲里市",
value:"150781"},
{
label:"牙克石市",
value:"150782"},
{
label:"扎兰屯市",
value:"150783"},
{
label:"额尔古纳市",
value:"150784"},
{
label:"根河市",
value:"150785"}],
[{
label:"临河区",
value:"150802"},
{
label:"五原县",
value:"150821"},
{
label:"磴口县",
value:"150822"},
{
label:"乌拉特前旗",
value:"150823"},
{
label:"乌拉特中旗",
value:"150824"},
{
label:"乌拉特后旗",
value:"150825"},
{
label:"杭锦后旗",
value:"150826"}],
[{
label:"集宁区",
value:"150902"},
{
label:"卓资县",
value:"150921"},
{
label:"化德县",
value:"150922"},
{
label:"商都县",
value:"150923"},
{
label:"兴和县",
value:"150924"},
{
label:"凉城县",
value:"150925"},
{
label:"察哈尔右翼前旗",
value:"150926"},
{
label:"察哈尔右翼中旗",
value:"150927"},
{
label:"察哈尔右翼后旗",
value:"150928"},
{
label:"四子王旗",
value:"150929"},
{
label:"丰镇市",
value:"150981"}],
[{
label:"乌兰浩特市",
value:"152201"},
{
label:"阿尔山市",
value:"152202"},
{
label:"科尔沁右翼前旗",
value:"152221"},
{
label:"科尔沁右翼中旗",
value:"152222"},
{
label:"扎赉特旗",
value:"152223"},
{
label:"突泉县",
value:"152224"}],
[{
label:"二连浩特市",
value:"152501"},
{
label:"锡林浩特市",
value:"152502"},
{
label:"阿巴嘎旗",
value:"152522"},
{
label:"苏尼特左旗",
value:"152523"},
{
label:"苏尼特右旗",
value:"152524"},
{
label:"东乌珠穆沁旗",
value:"152525"},
{
label:"西乌珠穆沁旗",
value:"152526"},
{
label:"太仆寺旗",
value:"152527"},
{
label:"镶黄旗",
value:"152528"},
{
label:"正镶白旗",
value:"152529"},
{
label:"正蓝旗",
value:"152530"},
{
label:"多伦县",
value:"152531"},
{
label:"乌拉盖管委会",
value:"152571"}],
[{
label:"阿拉善左旗",
value:"152921"},
{
label:"阿拉善右旗",
value:"152922"},
{
label:"额济纳旗",
value:"152923"},
{
label:"内蒙古阿拉善经济开发区",
value:"152971"}]],
[[{
label:"和平区",
value:"210102"},
{
label:"沈河区",
value:"210103"},
{
label:"大东区",
value:"210104"},
{
label:"皇姑区",
value:"210105"},
{
label:"铁西区",
value:"210106"},
{
label:"苏家屯区",
value:"210111"},
{
label:"浑南区",
value:"210112"},
{
label:"沈北新区",
value:"210113"},
{
label:"于洪区",
value:"210114"},
{
label:"辽中区",
value:"210115"},
{
label:"康平县",
value:"210123"},
{
label:"法库县",
value:"210124"},
{
label:"新民市",
value:"210181"}],
[{
label:"中山区",
value:"210202"},
{
label:"西岗区",
value:"210203"},
{
label:"沙河口区",
value:"210204"},
{
label:"甘井子区",
value:"210211"},
{
label:"旅顺口区",
value:"210212"},
{
label:"金州区",
value:"210213"},
{
label:"普兰店区",
value:"210214"},
{
label:"长海县",
value:"210224"},
{
label:"瓦房店市",
value:"210281"},
{
label:"庄河市",
value:"210283"}],
[{
label:"铁东区",
value:"210302"},
{
label:"铁西区",
value:"210303"},
{
label:"立山区",
value:"210304"},
{
label:"千山区",
value:"210311"},
{
label:"台安县",
value:"210321"},
{
label:"岫岩满族自治县",
value:"210323"},
{
label:"海城市",
value:"210381"}],
[{
label:"新抚区",
value:"210402"},
{
label:"东洲区",
value:"210403"},
{
label:"望花区",
value:"210404"},
{
label:"顺城区",
value:"210411"},
{
label:"抚顺县",
value:"210421"},
{
label:"新宾满族自治县",
value:"210422"},
{
label:"清原满族自治县",
value:"210423"}],
[{
label:"平山区",
value:"210502"},
{
label:"溪湖区",
value:"210503"},
{
label:"明山区",
value:"210504"},
{
label:"南芬区",
value:"210505"},
{
label:"本溪满族自治县",
value:"210521"},
{
label:"桓仁满族自治县",
value:"210522"}],
[{
label:"元宝区",
value:"210602"},
{
label:"振兴区",
value:"210603"},
{
label:"振安区",
value:"210604"},
{
label:"宽甸满族自治县",
value:"210624"},
{
label:"东港市",
value:"210681"},
{
label:"凤城市",
value:"210682"}],
[{
label:"古塔区",
value:"210702"},
{
label:"凌河区",
value:"210703"},
{
label:"太和区",
value:"210711"},
{
label:"黑山县",
value:"210726"},
{
label:"义县",
value:"210727"},
{
label:"凌海市",
value:"210781"},
{
label:"北镇市",
value:"210782"}],
[{
label:"站前区",
value:"210802"},
{
label:"西市区",
value:"210803"},
{
label:"鲅鱼圈区",
value:"210804"},
{
label:"老边区",
value:"210811"},
{
label:"盖州市",
value:"210881"},
{
label:"大石桥市",
value:"210882"}],
[{
label:"海州区",
value:"210902"},
{
label:"新邱区",
value:"210903"},
{
label:"太平区",
value:"210904"},
{
label:"清河门区",
value:"210905"},
{
label:"细河区",
value:"210911"},
{
label:"阜新蒙古族自治县",
value:"210921"},
{
label:"彰武县",
value:"210922"}],
[{
label:"白塔区",
value:"211002"},
{
label:"文圣区",
value:"211003"},
{
label:"宏伟区",
value:"211004"},
{
label:"弓长岭区",
value:"211005"},
{
label:"太子河区",
value:"211011"},
{
label:"辽阳县",
value:"211021"},
{
label:"灯塔市",
value:"211081"}],
[{
label:"双台子区",
value:"211102"},
{
label:"兴隆台区",
value:"211103"},
{
label:"大洼区",
value:"211104"},
{
label:"盘山县",
value:"211122"}],
[{
label:"银州区",
value:"211202"},
{
label:"清河区",
value:"211204"},
{
label:"铁岭县",
value:"211221"},
{
label:"西丰县",
value:"211223"},
{
label:"昌图县",
value:"211224"},
{
label:"调兵山市",
value:"211281"},
{
label:"开原市",
value:"211282"}],
[{
label:"双塔区",
value:"211302"},
{
label:"龙城区",
value:"211303"},
{
label:"朝阳县",
value:"211321"},
{
label:"建平县",
value:"211322"},
{
label:"喀喇沁左翼蒙古族自治县",
value:"211324"},
{
label:"北票市",
value:"211381"},
{
label:"凌源市",
value:"211382"}],
[{
label:"连山区",
value:"211402"},
{
label:"龙港区",
value:"211403"},
{
label:"南票区",
value:"211404"},
{
label:"绥中县",
value:"211421"},
{
label:"建昌县",
value:"211422"},
{
label:"兴城市",
value:"211481"}]],
[[{
label:"南关区",
value:"220102"},
{
label:"宽城区",
value:"220103"},
{
label:"朝阳区",
value:"220104"},
{
label:"二道区",
value:"220105"},
{
label:"绿园区",
value:"220106"},
{
label:"双阳区",
value:"220112"},
{
label:"九台区",
value:"220113"},
{
label:"农安县",
value:"220122"},
{
label:"长春经济技术开发区",
value:"220171"},
{
label:"长春净月高新技术产业开发区",
value:"220172"},
{
label:"长春高新技术产业开发区",
value:"220173"},
{
label:"长春汽车经济技术开发区",
value:"220174"},
{
label:"榆树市",
value:"220182"},
{
label:"德惠市",
value:"220183"}],
[{
label:"昌邑区",
value:"220202"},
{
label:"龙潭区",
value:"220203"},
{
label:"船营区",
value:"220204"},
{
label:"丰满区",
value:"220211"},
{
label:"永吉县",
value:"220221"},
{
label:"吉林经济开发区",
value:"220271"},
{
label:"吉林高新技术产业开发区",
value:"220272"},
{
label:"吉林中国新加坡食品区",
value:"220273"},
{
label:"蛟河市",
value:"220281"},
{
label:"桦甸市",
value:"220282"},
{
label:"舒兰市",
value:"220283"},
{
label:"磐石市",
value:"220284"}],
[{
label:"铁西区",
value:"220302"},
{
label:"铁东区",
value:"220303"},
{
label:"梨树县",
value:"220322"},
{
label:"伊通满族自治县",
value:"220323"},
{
label:"公主岭市",
value:"220381"},
{
label:"双辽市",
value:"220382"}],
[{
label:"龙山区",
value:"220402"},
{
label:"西安区",
value:"220403"},
{
label:"东丰县",
value:"220421"},
{
label:"东辽县",
value:"220422"}],
[{
label:"东昌区",
value:"220502"},
{
label:"二道江区",
value:"220503"},
{
label:"通化县",
value:"220521"},
{
label:"辉南县",
value:"220523"},
{
label:"柳河县",
value:"220524"},
{
label:"梅河口市",
value:"220581"},
{
label:"集安市",
value:"220582"}],
[{
label:"浑江区",
value:"220602"},
{
label:"江源区",
value:"220605"},
{
label:"抚松县",
value:"220621"},
{
label:"靖宇县",
value:"220622"},
{
label:"长白朝鲜族自治县",
value:"220623"},
{
label:"临江市",
value:"220681"}],
[{
label:"宁江区",
value:"220702"},
{
label:"前郭尔罗斯蒙古族自治县",
value:"220721"},
{
label:"长岭县",
value:"220722"},
{
label:"乾安县",
value:"220723"},
{
label:"吉林松原经济开发区",
value:"220771"},
{
label:"扶余市",
value:"220781"}],
[{
label:"洮北区",
value:"220802"},
{
label:"镇赉县",
value:"220821"},
{
label:"通榆县",
value:"220822"},
{
label:"吉林白城经济开发区",
value:"220871"},
{
label:"洮南市",
value:"220881"},
{
label:"大安市",
value:"220882"}],
[{
label:"延吉市",
value:"222401"},
{
label:"图们市",
value:"222402"},
{
label:"敦化市",
value:"222403"},
{
label:"珲春市",
value:"222404"},
{
label:"龙井市",
value:"222405"},
{
label:"和龙市",
value:"222406"},
{
label:"汪清县",
value:"222424"},
{
label:"安图县",
value:"222426"}]],
[[{
label:"道里区",
value:"230102"},
{
label:"南岗区",
value:"230103"},
{
label:"道外区",
value:"230104"},
{
label:"平房区",
value:"230108"},
{
label:"松北区",
value:"230109"},
{
label:"香坊区",
value:"230110"},
{
label:"呼兰区",
value:"230111"},
{
label:"阿城区",
value:"230112"},
{
label:"双城区",
value:"230113"},
{
label:"依兰县",
value:"230123"},
{
label:"方正县",
value:"230124"},
{
label:"宾县",
value:"230125"},
{
label:"巴彦县",
value:"230126"},
{
label:"木兰县",
value:"230127"},
{
label:"通河县",
value:"230128"},
{
label:"延寿县",
value:"230129"},
{
label:"尚志市",
value:"230183"},
{
label:"五常市",
value:"230184"}],
[{
label:"龙沙区",
value:"230202"},
{
label:"建华区",
value:"230203"},
{
label:"铁锋区",
value:"230204"},
{
label:"昂昂溪区",
value:"230205"},
{
label:"富拉尔基区",
value:"230206"},
{
label:"碾子山区",
value:"230207"},
{
label:"梅里斯达斡尔族区",
value:"230208"},
{
label:"龙江县",
value:"230221"},
{
label:"依安县",
value:"230223"},
{
label:"泰来县",
value:"230224"},
{
label:"甘南县",
value:"230225"},
{
label:"富裕县",
value:"230227"},
{
label:"克山县",
value:"230229"},
{
label:"克东县",
value:"230230"},
{
label:"拜泉县",
value:"230231"},
{
label:"讷河市",
value:"230281"}],
[{
label:"鸡冠区",
value:"230302"},
{
label:"恒山区",
value:"230303"},
{
label:"滴道区",
value:"230304"},
{
label:"梨树区",
value:"230305"},
{
label:"城子河区",
value:"230306"},
{
label:"麻山区",
value:"230307"},
{
label:"鸡东县",
value:"230321"},
{
label:"虎林市",
value:"230381"},
{
label:"密山市",
value:"230382"}],
[{
label:"向阳区",
value:"230402"},
{
label:"工农区",
value:"230403"},
{
label:"南山区",
value:"230404"},
{
label:"兴安区",
value:"230405"},
{
label:"东山区",
value:"230406"},
{
label:"兴山区",
value:"230407"},
{
label:"萝北县",
value:"230421"},
{
label:"绥滨县",
value:"230422"}],
[{
label:"尖山区",
value:"230502"},
{
label:"岭东区",
value:"230503"},
{
label:"四方台区",
value:"230505"},
{
label:"宝山区",
value:"230506"},
{
label:"集贤县",
value:"230521"},
{
label:"友谊县",
value:"230522"},
{
label:"宝清县",
value:"230523"},
{
label:"饶河县",
value:"230524"}],
[{
label:"萨尔图区",
value:"230602"},
{
label:"龙凤区",
value:"230603"},
{
label:"让胡路区",
value:"230604"},
{
label:"红岗区",
value:"230605"},
{
label:"大同区",
value:"230606"},
{
label:"肇州县",
value:"230621"},
{
label:"肇源县",
value:"230622"},
{
label:"林甸县",
value:"230623"},
{
label:"杜尔伯特蒙古族自治县",
value:"230624"},
{
label:"大庆高新技术产业开发区",
value:"230671"}],
[{
label:"伊春区",
value:"230702"},
{
label:"南岔区",
value:"230703"},
{
label:"友好区",
value:"230704"},
{
label:"西林区",
value:"230705"},
{
label:"翠峦区",
value:"230706"},
{
label:"新青区",
value:"230707"},
{
label:"美溪区",
value:"230708"},
{
label:"金山屯区",
value:"230709"},
{
label:"五营区",
value:"230710"},
{
label:"乌马河区",
value:"230711"},
{
label:"汤旺河区",
value:"230712"},
{
label:"带岭区",
value:"230713"},
{
label:"乌伊岭区",
value:"230714"},
{
label:"红星区",
value:"230715"},
{
label:"上甘岭区",
value:"230716"},
{
label:"嘉荫县",
value:"230722"},
{
label:"铁力市",
value:"230781"}],
[{
label:"向阳区",
value:"230803"},
{
label:"前进区",
value:"230804"},
{
label:"东风区",
value:"230805"},
{
label:"郊区",
value:"230811"},
{
label:"桦南县",
value:"230822"},
{
label:"桦川县",
value:"230826"},
{
label:"汤原县",
value:"230828"},
{
label:"同江市",
value:"230881"},
{
label:"富锦市",
value:"230882"},
{
label:"抚远市",
value:"230883"}],
[{
label:"新兴区",
value:"230902"},
{
label:"桃山区",
value:"230903"},
{
label:"茄子河区",
value:"230904"},
{
label:"勃利县",
value:"230921"}],
[{
label:"东安区",
value:"231002"},
{
label:"阳明区",
value:"231003"},
{
label:"爱民区",
value:"231004"},
{
label:"西安区",
value:"231005"},
{
label:"林口县",
value:"231025"},
{
label:"牡丹江经济技术开发区",
value:"231071"},
{
label:"绥芬河市",
value:"231081"},
{
label:"海林市",
value:"231083"},
{
label:"宁安市",
value:"231084"},
{
label:"穆棱市",
value:"231085"},
{
label:"东宁市",
value:"231086"}],
[{
label:"爱辉区",
value:"231102"},
{
label:"嫩江县",
value:"231121"},
{
label:"逊克县",
value:"231123"},
{
label:"孙吴县",
value:"231124"},
{
label:"北安市",
value:"231181"},
{
label:"五大连池市",
value:"231182"}],
[{
label:"北林区",
value:"231202"},
{
label:"望奎县",
value:"231221"},
{
label:"兰西县",
value:"231222"},
{
label:"青冈县",
value:"231223"},
{
label:"庆安县",
value:"231224"},
{
label:"明水县",
value:"231225"},
{
label:"绥棱县",
value:"231226"},
{
label:"安达市",
value:"231281"},
{
label:"肇东市",
value:"231282"},
{
label:"海伦市",
value:"231283"}],
[{
label:"加格达奇区",
value:"232701"},
{
label:"松岭区",
value:"232702"},
{
label:"新林区",
value:"232703"},
{
label:"呼中区",
value:"232704"},
{
label:"呼玛县",
value:"232721"},
{
label:"塔河县",
value:"232722"},
{
label:"漠河县",
value:"232723"}]],
[[{
label:"黄浦区",
value:"310101"},
{
label:"徐汇区",
value:"310104"},
{
label:"长宁区",
value:"310105"},
{
label:"静安区",
value:"310106"},
{
label:"普陀区",
value:"310107"},
{
label:"虹口区",
value:"310109"},
{
label:"杨浦区",
value:"310110"},
{
label:"闵行区",
value:"310112"},
{
label:"宝山区",
value:"310113"},
{
label:"嘉定区",
value:"310114"},
{
label:"浦东新区",
value:"310115"},
{
label:"金山区",
value:"310116"},
{
label:"松江区",
value:"310117"},
{
label:"青浦区",
value:"310118"},
{
label:"奉贤区",
value:"310120"},
{
label:"崇明区",
value:"310151"}]],
[[{
label:"玄武区",
value:"320102"},
{
label:"秦淮区",
value:"320104"},
{
label:"建邺区",
value:"320105"},
{
label:"鼓楼区",
value:"320106"},
{
label:"浦口区",
value:"320111"},
{
label:"栖霞区",
value:"320113"},
{
label:"雨花台区",
value:"320114"},
{
label:"江宁区",
value:"320115"},
{
label:"六合区",
value:"320116"},
{
label:"溧水区",
value:"320117"},
{
label:"高淳区",
value:"320118"}],
[{
label:"锡山区",
value:"320205"},
{
label:"惠山区",
value:"320206"},
{
label:"滨湖区",
value:"320211"},
{
label:"梁溪区",
value:"320213"},
{
label:"新吴区",
value:"320214"},
{
label:"江阴市",
value:"320281"},
{
label:"宜兴市",
value:"320282"}],
[{
label:"鼓楼区",
value:"320302"},
{
label:"云龙区",
value:"320303"},
{
label:"贾汪区",
value:"320305"},
{
label:"泉山区",
value:"320311"},
{
label:"铜山区",
value:"320312"},
{
label:"丰县",
value:"320321"},
{
label:"沛县",
value:"320322"},
{
label:"睢宁县",
value:"320324"},
{
label:"徐州经济技术开发区",
value:"320371"},
{
label:"新沂市",
value:"320381"},
{
label:"邳州市",
value:"320382"}],
[{
label:"天宁区",
value:"320402"},
{
label:"钟楼区",
value:"320404"},
{
label:"新北区",
value:"320411"},
{
label:"武进区",
value:"320412"},
{
label:"金坛区",
value:"320413"},
{
label:"溧阳市",
value:"320481"}],
[{
label:"虎丘区",
value:"320505"},
{
label:"吴中区",
value:"320506"},
{
label:"相城区",
value:"320507"},
{
label:"姑苏区",
value:"320508"},
{
label:"吴江区",
value:"320509"},
{
label:"苏州工业园区",
value:"320571"},
{
label:"常熟市",
value:"320581"},
{
label:"张家港市",
value:"320582"},
{
label:"昆山市",
value:"320583"},
{
label:"太仓市",
value:"320585"}],
[{
label:"崇川区",
value:"320602"},
{
label:"港闸区",
value:"320611"},
{
label:"通州区",
value:"320612"},
{
label:"海安县",
value:"320621"},
{
label:"如东县",
value:"320623"},
{
label:"南通经济技术开发区",
value:"320671"},
{
label:"启东市",
value:"320681"},
{
label:"如皋市",
value:"320682"},
{
label:"海门市",
value:"320684"}],
[{
label:"连云区",
value:"320703"},
{
label:"海州区",
value:"320706"},
{
label:"赣榆区",
value:"320707"},
{
label:"东海县",
value:"320722"},
{
label:"灌云县",
value:"320723"},
{
label:"灌南县",
value:"320724"},
{
label:"连云港经济技术开发区",
value:"320771"},
{
label:"连云港高新技术产业开发区",
value:"320772"}],
[{
label:"淮安区",
value:"320803"},
{
label:"淮阴区",
value:"320804"},
{
label:"清江浦区",
value:"320812"},
{
label:"洪泽区",
value:"320813"},
{
label:"涟水县",
value:"320826"},
{
label:"盱眙县",
value:"320830"},
{
label:"金湖县",
value:"320831"},
{
label:"淮安经济技术开发区",
value:"320871"}],
[{
label:"亭湖区",
value:"320902"},
{
label:"盐都区",
value:"320903"},
{
label:"大丰区",
value:"320904"},
{
label:"响水县",
value:"320921"},
{
label:"滨海县",
value:"320922"},
{
label:"阜宁县",
value:"320923"},
{
label:"射阳县",
value:"320924"},
{
label:"建湖县",
value:"320925"},
{
label:"盐城经济技术开发区",
value:"320971"},
{
label:"东台市",
value:"320981"}],
[{
label:"广陵区",
value:"321002"},
{
label:"邗江区",
value:"321003"},
{
label:"江都区",
value:"321012"},
{
label:"宝应县",
value:"321023"},
{
label:"扬州经济技术开发区",
value:"321071"},
{
label:"仪征市",
value:"321081"},
{
label:"高邮市",
value:"321084"}],
[{
label:"京口区",
value:"321102"},
{
label:"润州区",
value:"321111"},
{
label:"丹徒区",
value:"321112"},
{
label:"镇江新区",
value:"321171"},
{
label:"丹阳市",
value:"321181"},
{
label:"扬中市",
value:"321182"},
{
label:"句容市",
value:"321183"}],
[{
label:"海陵区",
value:"321202"},
{
label:"高港区",
value:"321203"},
{
label:"姜堰区",
value:"321204"},
{
label:"泰州医药高新技术产业开发区",
value:"321271"},
{
label:"兴化市",
value:"321281"},
{
label:"靖江市",
value:"321282"},
{
label:"泰兴市",
value:"321283"}],
[{
label:"宿城区",
value:"321302"},
{
label:"宿豫区",
value:"321311"},
{
label:"沭阳县",
value:"321322"},
{
label:"泗阳县",
value:"321323"},
{
label:"泗洪县",
value:"321324"},
{
label:"宿迁经济技术开发区",
value:"321371"}]],
[[{
label:"上城区",
value:"330102"},
{
label:"下城区",
value:"330103"},
{
label:"江干区",
value:"330104"},
{
label:"拱墅区",
value:"330105"},
{
label:"西湖区",
value:"330106"},
{
label:"滨江区",
value:"330108"},
{
label:"萧山区",
value:"330109"},
{
label:"余杭区",
value:"330110"},
{
label:"富阳区",
value:"330111"},
{
label:"临安区",
value:"330112"},
{
label:"桐庐县",
value:"330122"},
{
label:"淳安县",
value:"330127"},
{
label:"建德市",
value:"330182"}],
[{
label:"海曙区",
value:"330203"},
{
label:"江北区",
value:"330205"},
{
label:"北仑区",
value:"330206"},
{
label:"镇海区",
value:"330211"},
{
label:"鄞州区",
value:"330212"},
{
label:"奉化区",
value:"330213"},
{
label:"象山县",
value:"330225"},
{
label:"宁海县",
value:"330226"},
{
label:"余姚市",
value:"330281"},
{
label:"慈溪市",
value:"330282"}],
[{
label:"鹿城区",
value:"330302"},
{
label:"龙湾区",
value:"330303"},
{
label:"瓯海区",
value:"330304"},
{
label:"洞头区",
value:"330305"},
{
label:"永嘉县",
value:"330324"},
{
label:"平阳县",
value:"330326"},
{
label:"苍南县",
value:"330327"},
{
label:"文成县",
value:"330328"},
{
label:"泰顺县",
value:"330329"},
{
label:"温州经济技术开发区",
value:"330371"},
{
label:"瑞安市",
value:"330381"},
{
label:"乐清市",
value:"330382"}],
[{
label:"南湖区",
value:"330402"},
{
label:"秀洲区",
value:"330411"},
{
label:"嘉善县",
value:"330421"},
{
label:"海盐县",
value:"330424"},
{
label:"海宁市",
value:"330481"},
{
label:"平湖市",
value:"330482"},
{
label:"桐乡市",
value:"330483"}],
[{
label:"吴兴区",
value:"330502"},
{
label:"南浔区",
value:"330503"},
{
label:"德清县",
value:"330521"},
{
label:"长兴县",
value:"330522"},
{
label:"安吉县",
value:"330523"}],
[{
label:"越城区",
value:"330602"},
{
label:"柯桥区",
value:"330603"},
{
label:"上虞区",
value:"330604"},
{
label:"新昌县",
value:"330624"},
{
label:"诸暨市",
value:"330681"},
{
label:"嵊州市",
value:"330683"}],
[{
label:"婺城区",
value:"330702"},
{
label:"金东区",
value:"330703"},
{
label:"武义县",
value:"330723"},
{
label:"浦江县",
value:"330726"},
{
label:"磐安县",
value:"330727"},
{
label:"兰溪市",
value:"330781"},
{
label:"义乌市",
value:"330782"},
{
label:"东阳市",
value:"330783"},
{
label:"永康市",
value:"330784"}],
[{
label:"柯城区",
value:"330802"},
{
label:"衢江区",
value:"330803"},
{
label:"常山县",
value:"330822"},
{
label:"开化县",
value:"330824"},
{
label:"龙游县",
value:"330825"},
{
label:"江山市",
value:"330881"}],
[{
label:"定海区",
value:"330902"},
{
label:"普陀区",
value:"330903"},
{
label:"岱山县",
value:"330921"},
{
label:"嵊泗县",
value:"330922"}],
[{
label:"椒江区",
value:"331002"},
{
label:"黄岩区",
value:"331003"},
{
label:"路桥区",
value:"331004"},
{
label:"三门县",
value:"331022"},
{
label:"天台县",
value:"331023"},
{
label:"仙居县",
value:"331024"},
{
label:"温岭市",
value:"331081"},
{
label:"临海市",
value:"331082"},
{
label:"玉环市",
value:"331083"}],
[{
label:"莲都区",
value:"331102"},
{
label:"青田县",
value:"331121"},
{
label:"缙云县",
value:"331122"},
{
label:"遂昌县",
value:"331123"},
{
label:"松阳县",
value:"331124"},
{
label:"云和县",
value:"331125"},
{
label:"庆元县",
value:"331126"},
{
label:"景宁畲族自治县",
value:"331127"},
{
label:"龙泉市",
value:"331181"}]],
[[{
label:"瑶海区",
value:"340102"},
{
label:"庐阳区",
value:"340103"},
{
label:"蜀山区",
value:"340104"},
{
label:"包河区",
value:"340111"},
{
label:"长丰县",
value:"340121"},
{
label:"肥东县",
value:"340122"},
{
label:"肥西县",
value:"340123"},
{
label:"庐江县",
value:"340124"},
{
label:"合肥高新技术产业开发区",
value:"340171"},
{
label:"合肥经济技术开发区",
value:"340172"},
{
label:"合肥新站高新技术产业开发区",
value:"340173"},
{
label:"巢湖市",
value:"340181"}],
[{
label:"镜湖区",
value:"340202"},
{
label:"弋江区",
value:"340203"},
{
label:"鸠江区",
value:"340207"},
{
label:"三山区",
value:"340208"},
{
label:"芜湖县",
value:"340221"},
{
label:"繁昌县",
value:"340222"},
{
label:"南陵县",
value:"340223"},
{
label:"无为县",
value:"340225"},
{
label:"芜湖经济技术开发区",
value:"340271"},
{
label:"安徽芜湖长江大桥经济开发区",
value:"340272"}],
[{
label:"龙子湖区",
value:"340302"},
{
label:"蚌山区",
value:"340303"},
{
label:"禹会区",
value:"340304"},
{
label:"淮上区",
value:"340311"},
{
label:"怀远县",
value:"340321"},
{
label:"五河县",
value:"340322"},
{
label:"固镇县",
value:"340323"},
{
label:"蚌埠市高新技术开发区",
value:"340371"},
{
label:"蚌埠市经济开发区",
value:"340372"}],
[{
label:"大通区",
value:"340402"},
{
label:"田家庵区",
value:"340403"},
{
label:"谢家集区",
value:"340404"},
{
label:"八公山区",
value:"340405"},
{
label:"潘集区",
value:"340406"},
{
label:"凤台县",
value:"340421"},
{
label:"寿县",
value:"340422"}],
[{
label:"花山区",
value:"340503"},
{
label:"雨山区",
value:"340504"},
{
label:"博望区",
value:"340506"},
{
label:"当涂县",
value:"340521"},
{
label:"含山县",
value:"340522"},
{
label:"和县",
value:"340523"}],
[{
label:"杜集区",
value:"340602"},
{
label:"相山区",
value:"340603"},
{
label:"烈山区",
value:"340604"},
{
label:"濉溪县",
value:"340621"}],
[{
label:"铜官区",
value:"340705"},
{
label:"义安区",
value:"340706"},
{
label:"郊区",
value:"340711"},
{
label:"枞阳县",
value:"340722"}],
[{
label:"迎江区",
value:"340802"},
{
label:"大观区",
value:"340803"},
{
label:"宜秀区",
value:"340811"},
{
label:"怀宁县",
value:"340822"},
{
label:"潜山县",
value:"340824"},
{
label:"太湖县",
value:"340825"},
{
label:"宿松县",
value:"340826"},
{
label:"望江县",
value:"340827"},
{
label:"岳西县",
value:"340828"},
{
label:"安徽安庆经济开发区",
value:"340871"},
{
label:"桐城市",
value:"340881"}],
[{
label:"屯溪区",
value:"341002"},
{
label:"黄山区",
value:"341003"},
{
label:"徽州区",
value:"341004"},
{
label:"歙县",
value:"341021"},
{
label:"休宁县",
value:"341022"},
{
label:"黟县",
value:"341023"},
{
label:"祁门县",
value:"341024"}],
[{
label:"琅琊区",
value:"341102"},
{
label:"南谯区",
value:"341103"},
{
label:"来安县",
value:"341122"},
{
label:"全椒县",
value:"341124"},
{
label:"定远县",
value:"341125"},
{
label:"凤阳县",
value:"341126"},
{
label:"苏滁现代产业园",
value:"341171"},
{
label:"滁州经济技术开发区",
value:"341172"},
{
label:"天长市",
value:"341181"},
{
label:"明光市",
value:"341182"}],
[{
label:"颍州区",
value:"341202"},
{
label:"颍东区",
value:"341203"},
{
label:"颍泉区",
value:"341204"},
{
label:"临泉县",
value:"341221"},
{
label:"太和县",
value:"341222"},
{
label:"阜南县",
value:"341225"},
{
label:"颍上县",
value:"341226"},
{
label:"阜阳合肥现代产业园区",
value:"341271"},
{
label:"阜阳经济技术开发区",
value:"341272"},
{
label:"界首市",
value:"341282"}],
[{
label:"埇桥区",
value:"341302"},
{
label:"砀山县",
value:"341321"},
{
label:"萧县",
value:"341322"},
{
label:"灵璧县",
value:"341323"},
{
label:"泗县",
value:"341324"},
{
label:"宿州马鞍山现代产业园区",
value:"341371"},
{
label:"宿州经济技术开发区",
value:"341372"}],
[{
label:"金安区",
value:"341502"},
{
label:"裕安区",
value:"341503"},
{
label:"叶集区",
value:"341504"},
{
label:"霍邱县",
value:"341522"},
{
label:"舒城县",
value:"341523"},
{
label:"金寨县",
value:"341524"},
{
label:"霍山县",
value:"341525"}],
[{
label:"谯城区",
value:"341602"},
{
label:"涡阳县",
value:"341621"},
{
label:"蒙城县",
value:"341622"},
{
label:"利辛县",
value:"341623"}],
[{
label:"贵池区",
value:"341702"},
{
label:"东至县",
value:"341721"},
{
label:"石台县",
value:"341722"},
{
label:"青阳县",
value:"341723"}],
[{
label:"宣州区",
value:"341802"},
{
label:"郎溪县",
value:"341821"},
{
label:"广德县",
value:"341822"},
{
label:"泾县",
value:"341823"},
{
label:"绩溪县",
value:"341824"},
{
label:"旌德县",
value:"341825"},
{
label:"宣城市经济开发区",
value:"341871"},
{
label:"宁国市",
value:"341881"}]],
[[{
label:"鼓楼区",
value:"350102"},
{
label:"台江区",
value:"350103"},
{
label:"仓山区",
value:"350104"},
{
label:"马尾区",
value:"350105"},
{
label:"晋安区",
value:"350111"},
{
label:"闽侯县",
value:"350121"},
{
label:"连江县",
value:"350122"},
{
label:"罗源县",
value:"350123"},
{
label:"闽清县",
value:"350124"},
{
label:"永泰县",
value:"350125"},
{
label:"平潭县",
value:"350128"},
{
label:"福清市",
value:"350181"},
{
label:"长乐市",
value:"350182"}],
[{
label:"思明区",
value:"350203"},
{
label:"海沧区",
value:"350205"},
{
label:"湖里区",
value:"350206"},
{
label:"集美区",
value:"350211"},
{
label:"同安区",
value:"350212"},
{
label:"翔安区",
value:"350213"}],
[{
label:"城厢区",
value:"350302"},
{
label:"涵江区",
value:"350303"},
{
label:"荔城区",
value:"350304"},
{
label:"秀屿区",
value:"350305"},
{
label:"仙游县",
value:"350322"}],
[{
label:"梅列区",
value:"350402"},
{
label:"三元区",
value:"350403"},
{
label:"明溪县",
value:"350421"},
{
label:"清流县",
value:"350423"},
{
label:"宁化县",
value:"350424"},
{
label:"大田县",
value:"350425"},
{
label:"尤溪县",
value:"350426"},
{
label:"沙县",
value:"350427"},
{
label:"将乐县",
value:"350428"},
{
label:"泰宁县",
value:"350429"},
{
label:"建宁县",
value:"350430"},
{
label:"永安市",
value:"350481"}],
[{
label:"鲤城区",
value:"350502"},
{
label:"丰泽区",
value:"350503"},
{
label:"洛江区",
value:"350504"},
{
label:"泉港区",
value:"350505"},
{
label:"惠安县",
value:"350521"},
{
label:"安溪县",
value:"350524"},
{
label:"永春县",
value:"350525"},
{
label:"德化县",
value:"350526"},
{
label:"金门县",
value:"350527"},
{
label:"石狮市",
value:"350581"},
{
label:"晋江市",
value:"350582"},
{
label:"南安市",
value:"350583"}],
[{
label:"芗城区",
value:"350602"},
{
label:"龙文区",
value:"350603"},
{
label:"云霄县",
value:"350622"},
{
label:"漳浦县",
value:"350623"},
{
label:"诏安县",
value:"350624"},
{
label:"长泰县",
value:"350625"},
{
label:"东山县",
value:"350626"},
{
label:"南靖县",
value:"350627"},
{
label:"平和县",
value:"350628"},
{
label:"华安县",
value:"350629"},
{
label:"龙海市",
value:"350681"}],
[{
label:"延平区",
value:"350702"},
{
label:"建阳区",
value:"350703"},
{
label:"顺昌县",
value:"350721"},
{
label:"浦城县",
value:"350722"},
{
label:"光泽县",
value:"350723"},
{
label:"松溪县",
value:"350724"},
{
label:"政和县",
value:"350725"},
{
label:"邵武市",
value:"350781"},
{
label:"武夷山市",
value:"350782"},
{
label:"建瓯市",
value:"350783"}],
[{
label:"新罗区",
value:"350802"},
{
label:"永定区",
value:"350803"},
{
label:"长汀县",
value:"350821"},
{
label:"上杭县",
value:"350823"},
{
label:"武平县",
value:"350824"},
{
label:"连城县",
value:"350825"},
{
label:"漳平市",
value:"350881"}],
[{
label:"蕉城区",
value:"350902"},
{
label:"霞浦县",
value:"350921"},
{
label:"古田县",
value:"350922"},
{
label:"屏南县",
value:"350923"},
{
label:"寿宁县",
value:"350924"},
{
label:"周宁县",
value:"350925"},
{
label:"柘荣县",
value:"350926"},
{
label:"福安市",
value:"350981"},
{
label:"福鼎市",
value:"350982"}]],
[[{
label:"东湖区",
value:"360102"},
{
label:"西湖区",
value:"360103"},
{
label:"青云谱区",
value:"360104"},
{
label:"湾里区",
value:"360105"},
{
label:"青山湖区",
value:"360111"},
{
label:"新建区",
value:"360112"},
{
label:"南昌县",
value:"360121"},
{
label:"安义县",
value:"360123"},
{
label:"进贤县",
value:"360124"}],
[{
label:"昌江区",
value:"360202"},
{
label:"珠山区",
value:"360203"},
{
label:"浮梁县",
value:"360222"},
{
label:"乐平市",
value:"360281"}],
[{
label:"安源区",
value:"360302"},
{
label:"湘东区",
value:"360313"},
{
label:"莲花县",
value:"360321"},
{
label:"上栗县",
value:"360322"},
{
label:"芦溪县",
value:"360323"}],
[{
label:"濂溪区",
value:"360402"},
{
label:"浔阳区",
value:"360403"},
{
label:"柴桑区",
value:"360404"},
{
label:"武宁县",
value:"360423"},
{
label:"修水县",
value:"360424"},
{
label:"永修县",
value:"360425"},
{
label:"德安县",
value:"360426"},
{
label:"都昌县",
value:"360428"},
{
label:"湖口县",
value:"360429"},
{
label:"彭泽县",
value:"360430"},
{
label:"瑞昌市",
value:"360481"},
{
label:"共青城市",
value:"360482"},
{
label:"庐山市",
value:"360483"}],
[{
label:"渝水区",
value:"360502"},
{
label:"分宜县",
value:"360521"}],
[{
label:"月湖区",
value:"360602"},
{
label:"余江县",
value:"360622"},
{
label:"贵溪市",
value:"360681"}],
[{
label:"章贡区",
value:"360702"},
{
label:"南康区",
value:"360703"},
{
label:"赣县区",
value:"360704"},
{
label:"信丰县",
value:"360722"},
{
label:"大余县",
value:"360723"},
{
label:"上犹县",
value:"360724"},
{
label:"崇义县",
value:"360725"},
{
label:"安远县",
value:"360726"},
{
label:"龙南县",
value:"360727"},
{
label:"定南县",
value:"360728"},
{
label:"全南县",
value:"360729"},
{
label:"宁都县",
value:"360730"},
{
label:"于都县",
value:"360731"},
{
label:"兴国县",
value:"360732"},
{
label:"会昌县",
value:"360733"},
{
label:"寻乌县",
value:"360734"},
{
label:"石城县",
value:"360735"},
{
label:"瑞金市",
value:"360781"}],
[{
label:"吉州区",
value:"360802"},
{
label:"青原区",
value:"360803"},
{
label:"吉安县",
value:"360821"},
{
label:"吉水县",
value:"360822"},
{
label:"峡江县",
value:"360823"},
{
label:"新干县",
value:"360824"},
{
label:"永丰县",
value:"360825"},
{
label:"泰和县",
value:"360826"},
{
label:"遂川县",
value:"360827"},
{
label:"万安县",
value:"360828"},
{
label:"安福县",
value:"360829"},
{
label:"永新县",
value:"360830"},
{
label:"井冈山市",
value:"360881"}],
[{
label:"袁州区",
value:"360902"},
{
label:"奉新县",
value:"360921"},
{
label:"万载县",
value:"360922"},
{
label:"上高县",
value:"360923"},
{
label:"宜丰县",
value:"360924"},
{
label:"靖安县",
value:"360925"},
{
label:"铜鼓县",
value:"360926"},
{
label:"丰城市",
value:"360981"},
{
label:"樟树市",
value:"360982"},
{
label:"高安市",
value:"360983"}],
[{
label:"临川区",
value:"361002"},
{
label:"东乡区",
value:"361003"},
{
label:"南城县",
value:"361021"},
{
label:"黎川县",
value:"361022"},
{
label:"南丰县",
value:"361023"},
{
label:"崇仁县",
value:"361024"},
{
label:"乐安县",
value:"361025"},
{
label:"宜黄县",
value:"361026"},
{
label:"金溪县",
value:"361027"},
{
label:"资溪县",
value:"361028"},
{
label:"广昌县",
value:"361030"}],
[{
label:"信州区",
value:"361102"},
{
label:"广丰区",
value:"361103"},
{
label:"上饶县",
value:"361121"},
{
label:"玉山县",
value:"361123"},
{
label:"铅山县",
value:"361124"},
{
label:"横峰县",
value:"361125"},
{
label:"弋阳县",
value:"361126"},
{
label:"余干县",
value:"361127"},
{
label:"鄱阳县",
value:"361128"},
{
label:"万年县",
value:"361129"},
{
label:"婺源县",
value:"361130"},
{
label:"德兴市",
value:"361181"}]],
[[{
label:"历下区",
value:"370102"},
{
label:"市中区",
value:"370103"},
{
label:"槐荫区",
value:"370104"},
{
label:"天桥区",
value:"370105"},
{
label:"历城区",
value:"370112"},
{
label:"长清区",
value:"370113"},
{
label:"章丘区",
value:"370114"},
{
label:"平阴县",
value:"370124"},
{
label:"济阳县",
value:"370125"},
{
label:"商河县",
value:"370126"},
{
label:"济南高新技术产业开发区",
value:"370171"}],
[{
label:"市南区",
value:"370202"},
{
label:"市北区",
value:"370203"},
{
label:"黄岛区",
value:"370211"},
{
label:"崂山区",
value:"370212"},
{
label:"李沧区",
value:"370213"},
{
label:"城阳区",
value:"370214"},
{
label:"即墨区",
value:"370215"},
{
label:"青岛高新技术产业开发区",
value:"370271"},
{
label:"胶州市",
value:"370281"},
{
label:"平度市",
value:"370283"},
{
label:"莱西市",
value:"370285"}],
[{
label:"淄川区",
value:"370302"},
{
label:"张店区",
value:"370303"},
{
label:"博山区",
value:"370304"},
{
label:"临淄区",
value:"370305"},
{
label:"周村区",
value:"370306"},
{
label:"桓台县",
value:"370321"},
{
label:"高青县",
value:"370322"},
{
label:"沂源县",
value:"370323"}],
[{
label:"市中区",
value:"370402"},
{
label:"薛城区",
value:"370403"},
{
label:"峄城区",
value:"370404"},
{
label:"台儿庄区",
value:"370405"},
{
label:"山亭区",
value:"370406"},
{
label:"滕州市",
value:"370481"}],
[{
label:"东营区",
value:"370502"},
{
label:"河口区",
value:"370503"},
{
label:"垦利区",
value:"370505"},
{
label:"利津县",
value:"370522"},
{
label:"广饶县",
value:"370523"},
{
label:"东营经济技术开发区",
value:"370571"},
{
label:"东营港经济开发区",
value:"370572"}],
[{
label:"芝罘区",
value:"370602"},
{
label:"福山区",
value:"370611"},
{
label:"牟平区",
value:"370612"},
{
label:"莱山区",
value:"370613"},
{
label:"长岛县",
value:"370634"},
{
label:"烟台高新技术产业开发区",
value:"370671"},
{
label:"烟台经济技术开发区",
value:"370672"},
{
label:"龙口市",
value:"370681"},
{
label:"莱阳市",
value:"370682"},
{
label:"莱州市",
value:"370683"},
{
label:"蓬莱市",
value:"370684"},
{
label:"招远市",
value:"370685"},
{
label:"栖霞市",
value:"370686"},
{
label:"海阳市",
value:"370687"}],
[{
label:"潍城区",
value:"370702"},
{
label:"寒亭区",
value:"370703"},
{
label:"坊子区",
value:"370704"},
{
label:"奎文区",
value:"370705"},
{
label:"临朐县",
value:"370724"},
{
label:"昌乐县",
value:"370725"},
{
label:"潍坊滨海经济技术开发区",
value:"370772"},
{
label:"青州市",
value:"370781"},
{
label:"诸城市",
value:"370782"},
{
label:"寿光市",
value:"370783"},
{
label:"安丘市",
value:"370784"},
{
label:"高密市",
value:"370785"},
{
label:"昌邑市",
value:"370786"}],
[{
label:"任城区",
value:"370811"},
{
label:"兖州区",
value:"370812"},
{
label:"微山县",
value:"370826"},
{
label:"鱼台县",
value:"370827"},
{
label:"金乡县",
value:"370828"},
{
label:"嘉祥县",
value:"370829"},
{
label:"汶上县",
value:"370830"},
{
label:"泗水县",
value:"370831"},
{
label:"梁山县",
value:"370832"},
{
label:"济宁高新技术产业开发区",
value:"370871"},
{
label:"曲阜市",
value:"370881"},
{
label:"邹城市",
value:"370883"}],
[{
label:"泰山区",
value:"370902"},
{
label:"岱岳区",
value:"370911"},
{
label:"宁阳县",
value:"370921"},
{
label:"东平县",
value:"370923"},
{
label:"新泰市",
value:"370982"},
{
label:"肥城市",
value:"370983"}],
[{
label:"环翠区",
value:"371002"},
{
label:"文登区",
value:"371003"},
{
label:"威海火炬高技术产业开发区",
value:"371071"},
{
label:"威海经济技术开发区",
value:"371072"},
{
label:"威海临港经济技术开发区",
value:"371073"},
{
label:"荣成市",
value:"371082"},
{
label:"乳山市",
value:"371083"}],
[{
label:"东港区",
value:"371102"},
{
label:"岚山区",
value:"371103"},
{
label:"五莲县",
value:"371121"},
{
label:"莒县",
value:"371122"},
{
label:"日照经济技术开发区",
value:"371171"},
{
label:"日照国际海洋城",
value:"371172"}],
[{
label:"莱城区",
value:"371202"},
{
label:"钢城区",
value:"371203"}],
[{
label:"兰山区",
value:"371302"},
{
label:"罗庄区",
value:"371311"},
{
label:"河东区",
value:"371312"},
{
label:"沂南县",
value:"371321"},
{
label:"郯城县",
value:"371322"},
{
label:"沂水县",
value:"371323"},
{
label:"兰陵县",
value:"371324"},
{
label:"费县",
value:"371325"},
{
label:"平邑县",
value:"371326"},
{
label:"莒南县",
value:"371327"},
{
label:"蒙阴县",
value:"371328"},
{
label:"临沭县",
value:"371329"},
{
label:"临沂高新技术产业开发区",
value:"371371"},
{
label:"临沂经济技术开发区",
value:"371372"},
{
label:"临沂临港经济开发区",
value:"371373"}],
[{
label:"德城区",
value:"371402"},
{
label:"陵城区",
value:"371403"},
{
label:"宁津县",
value:"371422"},
{
label:"庆云县",
value:"371423"},
{
label:"临邑县",
value:"371424"},
{
label:"齐河县",
value:"371425"},
{
label:"平原县",
value:"371426"},
{
label:"夏津县",
value:"371427"},
{
label:"武城县",
value:"371428"},
{
label:"德州经济技术开发区",
value:"371471"},
{
label:"德州运河经济开发区",
value:"371472"},
{
label:"乐陵市",
value:"371481"},
{
label:"禹城市",
value:"371482"}],
[{
label:"东昌府区",
value:"371502"},
{
label:"阳谷县",
value:"371521"},
{
label:"莘县",
value:"371522"},
{
label:"茌平县",
value:"371523"},
{
label:"东阿县",
value:"371524"},
{
label:"冠县",
value:"371525"},
{
label:"高唐县",
value:"371526"},
{
label:"临清市",
value:"371581"}],
[{
label:"滨城区",
value:"371602"},
{
label:"沾化区",
value:"371603"},
{
label:"惠民县",
value:"371621"},
{
label:"阳信县",
value:"371622"},
{
label:"无棣县",
value:"371623"},
{
label:"博兴县",
value:"371625"},
{
label:"邹平县",
value:"371626"}],
[{
label:"牡丹区",
value:"371702"},
{
label:"定陶区",
value:"371703"},
{
label:"曹县",
value:"371721"},
{
label:"单县",
value:"371722"},
{
label:"成武县",
value:"371723"},
{
label:"巨野县",
value:"371724"},
{
label:"郓城县",
value:"371725"},
{
label:"鄄城县",
value:"371726"},
{
label:"东明县",
value:"371728"},
{
label:"菏泽经济技术开发区",
value:"371771"},
{
label:"菏泽高新技术开发区",
value:"371772"}]],
[[{
label:"中原区",
value:"410102"},
{
label:"二七区",
value:"410103"},
{
label:"管城回族区",
value:"410104"},
{
label:"金水区",
value:"410105"},
{
label:"上街区",
value:"410106"},
{
label:"惠济区",
value:"410108"},
{
label:"中牟县",
value:"410122"},
{
label:"郑州经济技术开发区",
value:"410171"},
{
label:"郑州高新技术产业开发区",
value:"410172"},
{
label:"郑州航空港经济综合实验区",
value:"410173"},
{
label:"巩义市",
value:"410181"},
{
label:"荥阳市",
value:"410182"},
{
label:"新密市",
value:"410183"},
{
label:"新郑市",
value:"410184"},
{
label:"登封市",
value:"410185"}],
[{
label:"龙亭区",
value:"410202"},
{
label:"顺河回族区",
value:"410203"},
{
label:"鼓楼区",
value:"410204"},
{
label:"禹王台区",
value:"410205"},
{
label:"祥符区",
value:"410212"},
{
label:"杞县",
value:"410221"},
{
label:"通许县",
value:"410222"},
{
label:"尉氏县",
value:"410223"},
{
label:"兰考县",
value:"410225"}],
[{
label:"老城区",
value:"410302"},
{
label:"西工区",
value:"410303"},
{
label:"瀍河回族区",
value:"410304"},
{
label:"涧西区",
value:"410305"},
{
label:"吉利区",
value:"410306"},
{
label:"洛龙区",
value:"410311"},
{
label:"孟津县",
value:"410322"},
{
label:"新安县",
value:"410323"},
{
label:"栾川县",
value:"410324"},
{
label:"嵩县",
value:"410325"},
{
label:"汝阳县",
value:"410326"},
{
label:"宜阳县",
value:"410327"},
{
label:"洛宁县",
value:"410328"},
{
label:"伊川县",
value:"410329"},
{
label:"洛阳高新技术产业开发区",
value:"410371"},
{
label:"偃师市",
value:"410381"}],
[{
label:"新华区",
value:"410402"},
{
label:"卫东区",
value:"410403"},
{
label:"石龙区",
value:"410404"},
{
label:"湛河区",
value:"410411"},
{
label:"宝丰县",
value:"410421"},
{
label:"叶县",
value:"410422"},
{
label:"鲁山县",
value:"410423"},
{
label:"郏县",
value:"410425"},
{
label:"平顶山高新技术产业开发区",
value:"410471"},
{
label:"平顶山市新城区",
value:"410472"},
{
label:"舞钢市",
value:"410481"},
{
label:"汝州市",
value:"410482"}],
[{
label:"文峰区",
value:"410502"},
{
label:"北关区",
value:"410503"},
{
label:"殷都区",
value:"410505"},
{
label:"龙安区",
value:"410506"},
{
label:"安阳县",
value:"410522"},
{
label:"汤阴县",
value:"410523"},
{
label:"滑县",
value:"410526"},
{
label:"内黄县",
value:"410527"},
{
label:"安阳高新技术产业开发区",
value:"410571"},
{
label:"林州市",
value:"410581"}],
[{
label:"鹤山区",
value:"410602"},
{
label:"山城区",
value:"410603"},
{
label:"淇滨区",
value:"410611"},
{
label:"浚县",
value:"410621"},
{
label:"淇县",
value:"410622"},
{
label:"鹤壁经济技术开发区",
value:"410671"}],
[{
label:"红旗区",
value:"410702"},
{
label:"卫滨区",
value:"410703"},
{
label:"凤泉区",
value:"410704"},
{
label:"牧野区",
value:"410711"},
{
label:"新乡县",
value:"410721"},
{
label:"获嘉县",
value:"410724"},
{
label:"原阳县",
value:"410725"},
{
label:"延津县",
value:"410726"},
{
label:"封丘县",
value:"410727"},
{
label:"长垣县",
value:"410728"},
{
label:"新乡高新技术产业开发区",
value:"410771"},
{
label:"新乡经济技术开发区",
value:"410772"},
{
label:"新乡市平原城乡一体化示范区",
value:"410773"},
{
label:"卫辉市",
value:"410781"},
{
label:"辉县市",
value:"410782"}],
[{
label:"解放区",
value:"410802"},
{
label:"中站区",
value:"410803"},
{
label:"马村区",
value:"410804"},
{
label:"山阳区",
value:"410811"},
{
label:"修武县",
value:"410821"},
{
label:"博爱县",
value:"410822"},
{
label:"武陟县",
value:"410823"},
{
label:"温县",
value:"410825"},
{
label:"焦作城乡一体化示范区",
value:"410871"},
{
label:"沁阳市",
value:"410882"},
{
label:"孟州市",
value:"410883"}],
[{
label:"华龙区",
value:"410902"},
{
label:"清丰县",
value:"410922"},
{
label:"南乐县",
value:"410923"},
{
label:"范县",
value:"410926"},
{
label:"台前县",
value:"410927"},
{
label:"濮阳县",
value:"410928"},
{
label:"河南濮阳工业园区",
value:"410971"},
{
label:"濮阳经济技术开发区",
value:"410972"}],
[{
label:"魏都区",
value:"411002"},
{
label:"建安区",
value:"411003"},
{
label:"鄢陵县",
value:"411024"},
{
label:"襄城县",
value:"411025"},
{
label:"许昌经济技术开发区",
value:"411071"},
{
label:"禹州市",
value:"411081"},
{
label:"长葛市",
value:"411082"}],
[{
label:"源汇区",
value:"411102"},
{
label:"郾城区",
value:"411103"},
{
label:"召陵区",
value:"411104"},
{
label:"舞阳县",
value:"411121"},
{
label:"临颍县",
value:"411122"},
{
label:"漯河经济技术开发区",
value:"411171"}],
[{
label:"湖滨区",
value:"411202"},
{
label:"陕州区",
value:"411203"},
{
label:"渑池县",
value:"411221"},
{
label:"卢氏县",
value:"411224"},
{
label:"河南三门峡经济开发区",
value:"411271"},
{
label:"义马市",
value:"411281"},
{
label:"灵宝市",
value:"411282"}],
[{
label:"宛城区",
value:"411302"},
{
label:"卧龙区",
value:"411303"},
{
label:"南召县",
value:"411321"},
{
label:"方城县",
value:"411322"},
{
label:"西峡县",
value:"411323"},
{
label:"镇平县",
value:"411324"},
{
label:"内乡县",
value:"411325"},
{
label:"淅川县",
value:"411326"},
{
label:"社旗县",
value:"411327"},
{
label:"唐河县",
value:"411328"},
{
label:"新野县",
value:"411329"},
{
label:"桐柏县",
value:"411330"},
{
label:"南阳高新技术产业开发区",
value:"411371"},
{
label:"南阳市城乡一体化示范区",
value:"411372"},
{
label:"邓州市",
value:"411381"}],
[{
label:"梁园区",
value:"411402"},
{
label:"睢阳区",
value:"411403"},
{
label:"民权县",
value:"411421"},
{
label:"睢县",
value:"411422"},
{
label:"宁陵县",
value:"411423"},
{
label:"柘城县",
value:"411424"},
{
label:"虞城县",
value:"411425"},
{
label:"夏邑县",
value:"411426"},
{
label:"豫东综合物流产业聚集区",
value:"411471"},
{
label:"河南商丘经济开发区",
value:"411472"},
{
label:"永城市",
value:"411481"}],
[{
label:"浉河区",
value:"411502"},
{
label:"平桥区",
value:"411503"},
{
label:"罗山县",
value:"411521"},
{
label:"光山县",
value:"411522"},
{
label:"新县",
value:"411523"},
{
label:"商城县",
value:"411524"},
{
label:"固始县",
value:"411525"},
{
label:"潢川县",
value:"411526"},
{
label:"淮滨县",
value:"411527"},
{
label:"息县",
value:"411528"},
{
label:"信阳高新技术产业开发区",
value:"411571"}],
[{
label:"川汇区",
value:"411602"},
{
label:"扶沟县",
value:"411621"},
{
label:"西华县",
value:"411622"},
{
label:"商水县",
value:"411623"},
{
label:"沈丘县",
value:"411624"},
{
label:"郸城县",
value:"411625"},
{
label:"淮阳县",
value:"411626"},
{
label:"太康县",
value:"411627"},
{
label:"鹿邑县",
value:"411628"},
{
label:"河南周口经济开发区",
value:"411671"},
{
label:"项城市",
value:"411681"}],
[{
label:"驿城区",
value:"411702"},
{
label:"西平县",
value:"411721"},
{
label:"上蔡县",
value:"411722"},
{
label:"平舆县",
value:"411723"},
{
label:"正阳县",
value:"411724"},
{
label:"确山县",
value:"411725"},
{
label:"泌阳县",
value:"411726"},
{
label:"汝南县",
value:"411727"},
{
label:"遂平县",
value:"411728"},
{
label:"新蔡县",
value:"411729"},
{
label:"河南驻马店经济开发区",
value:"411771"}],
[{
label:"济源市",
value:"419001"}]],
[[{
label:"江岸区",
value:"420102"},
{
label:"江汉区",
value:"420103"},
{
label:"硚口区",
value:"420104"},
{
label:"汉阳区",
value:"420105"},
{
label:"武昌区",
value:"420106"},
{
label:"青山区",
value:"420107"},
{
label:"洪山区",
value:"420111"},
{
label:"东西湖区",
value:"420112"},
{
label:"汉南区",
value:"420113"},
{
label:"蔡甸区",
value:"420114"},
{
label:"江夏区",
value:"420115"},
{
label:"黄陂区",
value:"420116"},
{
label:"新洲区",
value:"420117"}],
[{
label:"黄石港区",
value:"420202"},
{
label:"西塞山区",
value:"420203"},
{
label:"下陆区",
value:"420204"},
{
label:"铁山区",
value:"420205"},
{
label:"阳新县",
value:"420222"},
{
label:"大冶市",
value:"420281"}],
[{
label:"茅箭区",
value:"420302"},
{
label:"张湾区",
value:"420303"},
{
label:"郧阳区",
value:"420304"},
{
label:"郧西县",
value:"420322"},
{
label:"竹山县",
value:"420323"},
{
label:"竹溪县",
value:"420324"},
{
label:"房县",
value:"420325"},
{
label:"丹江口市",
value:"420381"}],
[{
label:"西陵区",
value:"420502"},
{
label:"伍家岗区",
value:"420503"},
{
label:"点军区",
value:"420504"},
{
label:"猇亭区",
value:"420505"},
{
label:"夷陵区",
value:"420506"},
{
label:"远安县",
value:"420525"},
{
label:"兴山县",
value:"420526"},
{
label:"秭归县",
value:"420527"},
{
label:"长阳土家族自治县",
value:"420528"},
{
label:"五峰土家族自治县",
value:"420529"},
{
label:"宜都市",
value:"420581"},
{
label:"当阳市",
value:"420582"},
{
label:"枝江市",
value:"420583"}],
[{
label:"襄城区",
value:"420602"},
{
label:"樊城区",
value:"420606"},
{
label:"襄州区",
value:"420607"},
{
label:"南漳县",
value:"420624"},
{
label:"谷城县",
value:"420625"},
{
label:"保康县",
value:"420626"},
{
label:"老河口市",
value:"420682"},
{
label:"枣阳市",
value:"420683"},
{
label:"宜城市",
value:"420684"}],
[{
label:"梁子湖区",
value:"420702"},
{
label:"华容区",
value:"420703"},
{
label:"鄂城区",
value:"420704"}],
[{
label:"东宝区",
value:"420802"},
{
label:"掇刀区",
value:"420804"},
{
label:"京山县",
value:"420821"},
{
label:"沙洋县",
value:"420822"},
{
label:"钟祥市",
value:"420881"}],
[{
label:"孝南区",
value:"420902"},
{
label:"孝昌县",
value:"420921"},
{
label:"大悟县",
value:"420922"},
{
label:"云梦县",
value:"420923"},
{
label:"应城市",
value:"420981"},
{
label:"安陆市",
value:"420982"},
{
label:"汉川市",
value:"420984"}],
[{
label:"沙市区",
value:"421002"},
{
label:"荆州区",
value:"421003"},
{
label:"公安县",
value:"421022"},
{
label:"监利县",
value:"421023"},
{
label:"江陵县",
value:"421024"},
{
label:"荆州经济技术开发区",
value:"421071"},
{
label:"石首市",
value:"421081"},
{
label:"洪湖市",
value:"421083"},
{
label:"松滋市",
value:"421087"}],
[{
label:"黄州区",
value:"421102"},
{
label:"团风县",
value:"421121"},
{
label:"红安县",
value:"421122"},
{
label:"罗田县",
value:"421123"},
{
label:"英山县",
value:"421124"},
{
label:"浠水县",
value:"421125"},
{
label:"蕲春县",
value:"421126"},
{
label:"黄梅县",
value:"421127"},
{
label:"龙感湖管理区",
value:"421171"},
{
label:"麻城市",
value:"421181"},
{
label:"武穴市",
value:"421182"}],
[{
label:"咸安区",
value:"421202"},
{
label:"嘉鱼县",
value:"421221"},
{
label:"通城县",
value:"421222"},
{
label:"崇阳县",
value:"421223"},
{
label:"通山县",
value:"421224"},
{
label:"赤壁市",
value:"421281"}],
[{
label:"曾都区",
value:"421303"},
{
label:"随县",
value:"421321"},
{
label:"广水市",
value:"421381"}],
[{
label:"恩施市",
value:"422801"},
{
label:"利川市",
value:"422802"},
{
label:"建始县",
value:"422822"},
{
label:"巴东县",
value:"422823"},
{
label:"宣恩县",
value:"422825"},
{
label:"咸丰县",
value:"422826"},
{
label:"来凤县",
value:"422827"},
{
label:"鹤峰县",
value:"422828"}],
[{
label:"仙桃市",
value:"429004"},
{
label:"潜江市",
value:"429005"},
{
label:"天门市",
value:"429006"},
{
label:"神农架林区",
value:"429021"}]],
[[{
label:"芙蓉区",
value:"430102"},
{
label:"天心区",
value:"430103"},
{
label:"岳麓区",
value:"430104"},
{
label:"开福区",
value:"430105"},
{
label:"雨花区",
value:"430111"},
{
label:"望城区",
value:"430112"},
{
label:"长沙县",
value:"430121"},
{
label:"浏阳市",
value:"430181"},
{
label:"宁乡市",
value:"430182"}],
[{
label:"荷塘区",
value:"430202"},
{
label:"芦淞区",
value:"430203"},
{
label:"石峰区",
value:"430204"},
{
label:"天元区",
value:"430211"},
{
label:"株洲县",
value:"430221"},
{
label:"攸县",
value:"430223"},
{
label:"茶陵县",
value:"430224"},
{
label:"炎陵县",
value:"430225"},
{
label:"云龙示范区",
value:"430271"},
{
label:"醴陵市",
value:"430281"}],
[{
label:"雨湖区",
value:"430302"},
{
label:"岳塘区",
value:"430304"},
{
label:"湘潭县",
value:"430321"},
{
label:"湖南湘潭高新技术产业园区",
value:"430371"},
{
label:"湘潭昭山示范区",
value:"430372"},
{
label:"湘潭九华示范区",
value:"430373"},
{
label:"湘乡市",
value:"430381"},
{
label:"韶山市",
value:"430382"}],
[{
label:"珠晖区",
value:"430405"},
{
label:"雁峰区",
value:"430406"},
{
label:"石鼓区",
value:"430407"},
{
label:"蒸湘区",
value:"430408"},
{
label:"南岳区",
value:"430412"},
{
label:"衡阳县",
value:"430421"},
{
label:"衡南县",
value:"430422"},
{
label:"衡山县",
value:"430423"},
{
label:"衡东县",
value:"430424"},
{
label:"祁东县",
value:"430426"},
{
label:"衡阳综合保税区",
value:"430471"},
{
label:"湖南衡阳高新技术产业园区",
value:"430472"},
{
label:"湖南衡阳松木经济开发区",
value:"430473"},
{
label:"耒阳市",
value:"430481"},
{
label:"常宁市",
value:"430482"}],
[{
label:"双清区",
value:"430502"},
{
label:"大祥区",
value:"430503"},
{
label:"北塔区",
value:"430511"},
{
label:"邵东县",
value:"430521"},
{
label:"新邵县",
value:"430522"},
{
label:"邵阳县",
value:"430523"},
{
label:"隆回县",
value:"430524"},
{
label:"洞口县",
value:"430525"},
{
label:"绥宁县",
value:"430527"},
{
label:"新宁县",
value:"430528"},
{
label:"城步苗族自治县",
value:"430529"},
{
label:"武冈市",
value:"430581"}],
[{
label:"岳阳楼区",
value:"430602"},
{
label:"云溪区",
value:"430603"},
{
label:"君山区",
value:"430611"},
{
label:"岳阳县",
value:"430621"},
{
label:"华容县",
value:"430623"},
{
label:"湘阴县",
value:"430624"},
{
label:"平江县",
value:"430626"},
{
label:"岳阳市屈原管理区",
value:"430671"},
{
label:"汨罗市",
value:"430681"},
{
label:"临湘市",
value:"430682"}],
[{
label:"武陵区",
value:"430702"},
{
label:"鼎城区",
value:"430703"},
{
label:"安乡县",
value:"430721"},
{
label:"汉寿县",
value:"430722"},
{
label:"澧县",
value:"430723"},
{
label:"临澧县",
value:"430724"},
{
label:"桃源县",
value:"430725"},
{
label:"石门县",
value:"430726"},
{
label:"常德市西洞庭管理区",
value:"430771"},
{
label:"津市市",
value:"430781"}],
[{
label:"永定区",
value:"430802"},
{
label:"武陵源区",
value:"430811"},
{
label:"慈利县",
value:"430821"},
{
label:"桑植县",
value:"430822"}],
[{
label:"资阳区",
value:"430902"},
{
label:"赫山区",
value:"430903"},
{
label:"南县",
value:"430921"},
{
label:"桃江县",
value:"430922"},
{
label:"安化县",
value:"430923"},
{
label:"益阳市大通湖管理区",
value:"430971"},
{
label:"湖南益阳高新技术产业园区",
value:"430972"},
{
label:"沅江市",
value:"430981"}],
[{
label:"北湖区",
value:"431002"},
{
label:"苏仙区",
value:"431003"},
{
label:"桂阳县",
value:"431021"},
{
label:"宜章县",
value:"431022"},
{
label:"永兴县",
value:"431023"},
{
label:"嘉禾县",
value:"431024"},
{
label:"临武县",
value:"431025"},
{
label:"汝城县",
value:"431026"},
{
label:"桂东县",
value:"431027"},
{
label:"安仁县",
value:"431028"},
{
label:"资兴市",
value:"431081"}],
[{
label:"零陵区",
value:"431102"},
{
label:"冷水滩区",
value:"431103"},
{
label:"祁阳县",
value:"431121"},
{
label:"东安县",
value:"431122"},
{
label:"双牌县",
value:"431123"},
{
label:"道县",
value:"431124"},
{
label:"江永县",
value:"431125"},
{
label:"宁远县",
value:"431126"},
{
label:"蓝山县",
value:"431127"},
{
label:"新田县",
value:"431128"},
{
label:"江华瑶族自治县",
value:"431129"},
{
label:"永州经济技术开发区",
value:"431171"},
{
label:"永州市金洞管理区",
value:"431172"},
{
label:"永州市回龙圩管理区",
value:"431173"}],
[{
label:"鹤城区",
value:"431202"},
{
label:"中方县",
value:"431221"},
{
label:"沅陵县",
value:"431222"},
{
label:"辰溪县",
value:"431223"},
{
label:"溆浦县",
value:"431224"},
{
label:"会同县",
value:"431225"},
{
label:"麻阳苗族自治县",
value:"431226"},
{
label:"新晃侗族自治县",
value:"431227"},
{
label:"芷江侗族自治县",
value:"431228"},
{
label:"靖州苗族侗族自治县",
value:"431229"},
{
label:"通道侗族自治县",
value:"431230"},
{
label:"怀化市洪江管理区",
value:"431271"},
{
label:"洪江市",
value:"431281"}],
[{
label:"娄星区",
value:"431302"},
{
label:"双峰县",
value:"431321"},
{
label:"新化县",
value:"431322"},
{
label:"冷水江市",
value:"431381"},
{
label:"涟源市",
value:"431382"}],
[{
label:"吉首市",
value:"433101"},
{
label:"泸溪县",
value:"433122"},
{
label:"凤凰县",
value:"433123"},
{
label:"花垣县",
value:"433124"},
{
label:"保靖县",
value:"433125"},
{
label:"古丈县",
value:"433126"},
{
label:"永顺县",
value:"433127"},
{
label:"龙山县",
value:"433130"},
{
label:"湖南吉首经济开发区",
value:"433172"},
{
label:"湖南永顺经济开发区",
value:"433173"}]],
[[{
label:"荔湾区",
value:"440103"},
{
label:"越秀区",
value:"440104"},
{
label:"海珠区",
value:"440105"},
{
label:"天河区",
value:"440106"},
{
label:"白云区",
value:"440111"},
{
label:"黄埔区",
value:"440112"},
{
label:"番禺区",
value:"440113"},
{
label:"花都区",
value:"440114"},
{
label:"南沙区",
value:"440115"},
{
label:"从化区",
value:"440117"},
{
label:"增城区",
value:"440118"}],
[{
label:"武江区",
value:"440203"},
{
label:"浈江区",
value:"440204"},
{
label:"曲江区",
value:"440205"},
{
label:"始兴县",
value:"440222"},
{
label:"仁化县",
value:"440224"},
{
label:"翁源县",
value:"440229"},
{
label:"乳源瑶族自治县",
value:"440232"},
{
label:"新丰县",
value:"440233"},
{
label:"乐昌市",
value:"440281"},
{
label:"南雄市",
value:"440282"}],
[{
label:"罗湖区",
value:"440303"},
{
label:"福田区",
value:"440304"},
{
label:"南山区",
value:"440305"},
{
label:"宝安区",
value:"440306"},
{
label:"龙岗区",
value:"440307"},
{
label:"盐田区",
value:"440308"},
{
label:"龙华区",
value:"440309"},
{
label:"坪山区",
value:"440310"}],
[{
label:"香洲区",
value:"440402"},
{
label:"斗门区",
value:"440403"},
{
label:"金湾区",
value:"440404"}],
[{
label:"龙湖区",
value:"440507"},
{
label:"金平区",
value:"440511"},
{
label:"濠江区",
value:"440512"},
{
label:"潮阳区",
value:"440513"},
{
label:"潮南区",
value:"440514"},
{
label:"澄海区",
value:"440515"},
{
label:"南澳县",
value:"440523"}],
[{
label:"禅城区",
value:"440604"},
{
label:"南海区",
value:"440605"},
{
label:"顺德区",
value:"440606"},
{
label:"三水区",
value:"440607"},
{
label:"高明区",
value:"440608"}],
[{
label:"蓬江区",
value:"440703"},
{
label:"江海区",
value:"440704"},
{
label:"新会区",
value:"440705"},
{
label:"台山市",
value:"440781"},
{
label:"开平市",
value:"440783"},
{
label:"鹤山市",
value:"440784"},
{
label:"恩平市",
value:"440785"}],
[{
label:"赤坎区",
value:"440802"},
{
label:"霞山区",
value:"440803"},
{
label:"坡头区",
value:"440804"},
{
label:"麻章区",
value:"440811"},
{
label:"遂溪县",
value:"440823"},
{
label:"徐闻县",
value:"440825"},
{
label:"廉江市",
value:"440881"},
{
label:"雷州市",
value:"440882"},
{
label:"吴川市",
value:"440883"}],
[{
label:"茂南区",
value:"440902"},
{
label:"电白区",
value:"440904"},
{
label:"高州市",
value:"440981"},
{
label:"化州市",
value:"440982"},
{
label:"信宜市",
value:"440983"}],
[{
label:"端州区",
value:"441202"},
{
label:"鼎湖区",
value:"441203"},
{
label:"高要区",
value:"441204"},
{
label:"广宁县",
value:"441223"},
{
label:"怀集县",
value:"441224"},
{
label:"封开县",
value:"441225"},
{
label:"德庆县",
value:"441226"},
{
label:"四会市",
value:"441284"}],
[{
label:"惠城区",
value:"441302"},
{
label:"惠阳区",
value:"441303"},
{
label:"博罗县",
value:"441322"},
{
label:"惠东县",
value:"441323"},
{
label:"龙门县",
value:"441324"}],
[{
label:"梅江区",
value:"441402"},
{
label:"梅县区",
value:"441403"},
{
label:"大埔县",
value:"441422"},
{
label:"丰顺县",
value:"441423"},
{
label:"五华县",
value:"441424"},
{
label:"平远县",
value:"441426"},
{
label:"蕉岭县",
value:"441427"},
{
label:"兴宁市",
value:"441481"}],
[{
label:"城区",
value:"441502"},
{
label:"海丰县",
value:"441521"},
{
label:"陆河县",
value:"441523"},
{
label:"陆丰市",
value:"441581"}],
[{
label:"源城区",
value:"441602"},
{
label:"紫金县",
value:"441621"},
{
label:"龙川县",
value:"441622"},
{
label:"连平县",
value:"441623"},
{
label:"和平县",
value:"441624"},
{
label:"东源县",
value:"441625"}],
[{
label:"江城区",
value:"441702"},
{
label:"阳东区",
value:"441704"},
{
label:"阳西县",
value:"441721"},
{
label:"阳春市",
value:"441781"}],
[{
label:"清城区",
value:"441802"},
{
label:"清新区",
value:"441803"},
{
label:"佛冈县",
value:"441821"},
{
label:"阳山县",
value:"441823"},
{
label:"连山壮族瑶族自治县",
value:"441825"},
{
label:"连南瑶族自治县",
value:"441826"},
{
label:"英德市",
value:"441881"},
{
label:"连州市",
value:"441882"}],
[{
label:"东莞市",
value:"441900"}],
[{
label:"中山市",
value:"442000"}],
[{
label:"湘桥区",
value:"445102"},
{
label:"潮安区",
value:"445103"},
{
label:"饶平县",
value:"445122"}],
[{
label:"榕城区",
value:"445202"},
{
label:"揭东区",
value:"445203"},
{
label:"揭西县",
value:"445222"},
{
label:"惠来县",
value:"445224"},
{
label:"普宁市",
value:"445281"}],
[{
label:"云城区",
value:"445302"},
{
label:"云安区",
value:"445303"},
{
label:"新兴县",
value:"445321"},
{
label:"郁南县",
value:"445322"},
{
label:"罗定市",
value:"445381"}]],
[[{
label:"兴宁区",
value:"450102"},
{
label:"青秀区",
value:"450103"},
{
label:"江南区",
value:"450105"},
{
label:"西乡塘区",
value:"450107"},
{
label:"良庆区",
value:"450108"},
{
label:"邕宁区",
value:"450109"},
{
label:"武鸣区",
value:"450110"},
{
label:"隆安县",
value:"450123"},
{
label:"马山县",
value:"450124"},
{
label:"上林县",
value:"450125"},
{
label:"宾阳县",
value:"450126"},
{
label:"横县",
value:"450127"}],
[{
label:"城中区",
value:"450202"},
{
label:"鱼峰区",
value:"450203"},
{
label:"柳南区",
value:"450204"},
{
label:"柳北区",
value:"450205"},
{
label:"柳江区",
value:"450206"},
{
label:"柳城县",
value:"450222"},
{
label:"鹿寨县",
value:"450223"},
{
label:"融安县",
value:"450224"},
{
label:"融水苗族自治县",
value:"450225"},
{
label:"三江侗族自治县",
value:"450226"}],
[{
label:"秀峰区",
value:"450302"},
{
label:"叠彩区",
value:"450303"},
{
label:"象山区",
value:"450304"},
{
label:"七星区",
value:"450305"},
{
label:"雁山区",
value:"450311"},
{
label:"临桂区",
value:"450312"},
{
label:"阳朔县",
value:"450321"},
{
label:"灵川县",
value:"450323"},
{
label:"全州县",
value:"450324"},
{
label:"兴安县",
value:"450325"},
{
label:"永福县",
value:"450326"},
{
label:"灌阳县",
value:"450327"},
{
label:"龙胜各族自治县",
value:"450328"},
{
label:"资源县",
value:"450329"},
{
label:"平乐县",
value:"450330"},
{
label:"荔浦县",
value:"450331"},
{
label:"恭城瑶族自治县",
value:"450332"}],
[{
label:"万秀区",
value:"450403"},
{
label:"长洲区",
value:"450405"},
{
label:"龙圩区",
value:"450406"},
{
label:"苍梧县",
value:"450421"},
{
label:"藤县",
value:"450422"},
{
label:"蒙山县",
value:"450423"},
{
label:"岑溪市",
value:"450481"}],
[{
label:"海城区",
value:"450502"},
{
label:"银海区",
value:"450503"},
{
label:"铁山港区",
value:"450512"},
{
label:"合浦县",
value:"450521"}],
[{
label:"港口区",
value:"450602"},
{
label:"防城区",
value:"450603"},
{
label:"上思县",
value:"450621"},
{
label:"东兴市",
value:"450681"}],
[{
label:"钦南区",
value:"450702"},
{
label:"钦北区",
value:"450703"},
{
label:"灵山县",
value:"450721"},
{
label:"浦北县",
value:"450722"}],
[{
label:"港北区",
value:"450802"},
{
label:"港南区",
value:"450803"},
{
label:"覃塘区",
value:"450804"},
{
label:"平南县",
value:"450821"},
{
label:"桂平市",
value:"450881"}],
[{
label:"玉州区",
value:"450902"},
{
label:"福绵区",
value:"450903"},
{
label:"容县",
value:"450921"},
{
label:"陆川县",
value:"450922"},
{
label:"博白县",
value:"450923"},
{
label:"兴业县",
value:"450924"},
{
label:"北流市",
value:"450981"}],
[{
label:"右江区",
value:"451002"},
{
label:"田阳县",
value:"451021"},
{
label:"田东县",
value:"451022"},
{
label:"平果县",
value:"451023"},
{
label:"德保县",
value:"451024"},
{
label:"那坡县",
value:"451026"},
{
label:"凌云县",
value:"451027"},
{
label:"乐业县",
value:"451028"},
{
label:"田林县",
value:"451029"},
{
label:"西林县",
value:"451030"},
{
label:"隆林各族自治县",
value:"451031"},
{
label:"靖西市",
value:"451081"}],
[{
label:"八步区",
value:"451102"},
{
label:"平桂区",
value:"451103"},
{
label:"昭平县",
value:"451121"},
{
label:"钟山县",
value:"451122"},
{
label:"富川瑶族自治县",
value:"451123"}],
[{
label:"金城江区",
value:"451202"},
{
label:"宜州区",
value:"451203"},
{
label:"南丹县",
value:"451221"},
{
label:"天峨县",
value:"451222"},
{
label:"凤山县",
value:"451223"},
{
label:"东兰县",
value:"451224"},
{
label:"罗城仫佬族自治县",
value:"451225"},
{
label:"环江毛南族自治县",
value:"451226"},
{
label:"巴马瑶族自治县",
value:"451227"},
{
label:"都安瑶族自治县",
value:"451228"},
{
label:"大化瑶族自治县",
value:"451229"}],
[{
label:"兴宾区",
value:"451302"},
{
label:"忻城县",
value:"451321"},
{
label:"象州县",
value:"451322"},
{
label:"武宣县",
value:"451323"},
{
label:"金秀瑶族自治县",
value:"451324"},
{
label:"合山市",
value:"451381"}],
[{
label:"江州区",
value:"451402"},
{
label:"扶绥县",
value:"451421"},
{
label:"宁明县",
value:"451422"},
{
label:"龙州县",
value:"451423"},
{
label:"大新县",
value:"451424"},
{
label:"天等县",
value:"451425"},
{
label:"凭祥市",
value:"451481"}]],
[[{
label:"秀英区",
value:"460105"},
{
label:"龙华区",
value:"460106"},
{
label:"琼山区",
value:"460107"},
{
label:"美兰区",
value:"460108"}],
[{
label:"海棠区",
value:"460202"},
{
label:"吉阳区",
value:"460203"},
{
label:"天涯区",
value:"460204"},
{
label:"崖州区",
value:"460205"}],
[{
label:"西沙群岛",
value:"460321"},
{
label:"南沙群岛",
value:"460322"},
{
label:"中沙群岛的岛礁及其海域",
value:"460323"}],
[{
label:"儋州市",
value:"460400"}],
[{
label:"五指山市",
value:"469001"},
{
label:"琼海市",
value:"469002"},
{
label:"文昌市",
value:"469005"},
{
label:"万宁市",
value:"469006"},
{
label:"东方市",
value:"469007"},
{
label:"定安县",
value:"469021"},
{
label:"屯昌县",
value:"469022"},
{
label:"澄迈县",
value:"469023"},
{
label:"临高县",
value:"469024"},
{
label:"白沙黎族自治县",
value:"469025"},
{
label:"昌江黎族自治县",
value:"469026"},
{
label:"乐东黎族自治县",
value:"469027"},
{
label:"陵水黎族自治县",
value:"469028"},
{
label:"保亭黎族苗族自治县",
value:"469029"},
{
label:"琼中黎族苗族自治县",
value:"469030"}]],
[[{
label:"万州区",
value:"500101"},
{
label:"涪陵区",
value:"500102"},
{
label:"渝中区",
value:"500103"},
{
label:"大渡口区",
value:"500104"},
{
label:"江北区",
value:"500105"},
{
label:"沙坪坝区",
value:"500106"},
{
label:"九龙坡区",
value:"500107"},
{
label:"南岸区",
value:"500108"},
{
label:"北碚区",
value:"500109"},
{
label:"綦江区",
value:"500110"},
{
label:"大足区",
value:"500111"},
{
label:"渝北区",
value:"500112"},
{
label:"巴南区",
value:"500113"},
{
label:"黔江区",
value:"500114"},
{
label:"长寿区",
value:"500115"},
{
label:"江津区",
value:"500116"},
{
label:"合川区",
value:"500117"},
{
label:"永川区",
value:"500118"},
{
label:"南川区",
value:"500119"},
{
label:"璧山区",
value:"500120"},
{
label:"铜梁区",
value:"500151"},
{
label:"潼南区",
value:"500152"},
{
label:"荣昌区",
value:"500153"},
{
label:"开州区",
value:"500154"},
{
label:"梁平区",
value:"500155"},
{
label:"武隆区",
value:"500156"}],
[{
label:"城口县",
value:"500229"},
{
label:"丰都县",
value:"500230"},
{
label:"垫江县",
value:"500231"},
{
label:"忠县",
value:"500233"},
{
label:"云阳县",
value:"500235"},
{
label:"奉节县",
value:"500236"},
{
label:"巫山县",
value:"500237"},
{
label:"巫溪县",
value:"500238"},
{
label:"石柱土家族自治县",
value:"500240"},
{
label:"秀山土家族苗族自治县",
value:"500241"},
{
label:"酉阳土家族苗族自治县",
value:"500242"},
{
label:"彭水苗族土家族自治县",
value:"500243"}]],
[[{
label:"锦江区",
value:"510104"},
{
label:"青羊区",
value:"510105"},
{
label:"金牛区",
value:"510106"},
{
label:"武侯区",
value:"510107"},
{
label:"成华区",
value:"510108"},
{
label:"龙泉驿区",
value:"510112"},
{
label:"青白江区",
value:"510113"},
{
label:"新都区",
value:"510114"},
{
label:"温江区",
value:"510115"},
{
label:"双流区",
value:"510116"},
{
label:"郫都区",
value:"510117"},
{
label:"金堂县",
value:"510121"},
{
label:"大邑县",
value:"510129"},
{
label:"蒲江县",
value:"510131"},
{
label:"新津县",
value:"510132"},
{
label:"都江堰市",
value:"510181"},
{
label:"彭州市",
value:"510182"},
{
label:"邛崃市",
value:"510183"},
{
label:"崇州市",
value:"510184"},
{
label:"简阳市",
value:"510185"}],
[{
label:"自流井区",
value:"510302"},
{
label:"贡井区",
value:"510303"},
{
label:"大安区",
value:"510304"},
{
label:"沿滩区",
value:"510311"},
{
label:"荣县",
value:"510321"},
{
label:"富顺县",
value:"510322"}],
[{
label:"东区",
value:"510402"},
{
label:"西区",
value:"510403"},
{
label:"仁和区",
value:"510411"},
{
label:"米易县",
value:"510421"},
{
label:"盐边县",
value:"510422"}],
[{
label:"江阳区",
value:"510502"},
{
label:"纳溪区",
value:"510503"},
{
label:"龙马潭区",
value:"510504"},
{
label:"泸县",
value:"510521"},
{
label:"合江县",
value:"510522"},
{
label:"叙永县",
value:"510524"},
{
label:"古蔺县",
value:"510525"}],
[{
label:"旌阳区",
value:"510603"},
{
label:"罗江区",
value:"510604"},
{
label:"中江县",
value:"510623"},
{
label:"广汉市",
value:"510681"},
{
label:"什邡市",
value:"510682"},
{
label:"绵竹市",
value:"510683"}],
[{
label:"涪城区",
value:"510703"},
{
label:"游仙区",
value:"510704"},
{
label:"安州区",
value:"510705"},
{
label:"三台县",
value:"510722"},
{
label:"盐亭县",
value:"510723"},
{
label:"梓潼县",
value:"510725"},
{
label:"北川羌族自治县",
value:"510726"},
{
label:"平武县",
value:"510727"},
{
label:"江油市",
value:"510781"}],
[{
label:"利州区",
value:"510802"},
{
label:"昭化区",
value:"510811"},
{
label:"朝天区",
value:"510812"},
{
label:"旺苍县",
value:"510821"},
{
label:"青川县",
value:"510822"},
{
label:"剑阁县",
value:"510823"},
{
label:"苍溪县",
value:"510824"}],
[{
label:"船山区",
value:"510903"},
{
label:"安居区",
value:"510904"},
{
label:"蓬溪县",
value:"510921"},
{
label:"射洪县",
value:"510922"},
{
label:"大英县",
value:"510923"}],
[{
label:"市中区",
value:"511002"},
{
label:"东兴区",
value:"511011"},
{
label:"威远县",
value:"511024"},
{
label:"资中县",
value:"511025"},
{
label:"内江经济开发区",
value:"511071"},
{
label:"隆昌市",
value:"511083"}],
[{
label:"市中区",
value:"511102"},
{
label:"沙湾区",
value:"511111"},
{
label:"五通桥区",
value:"511112"},
{
label:"金口河区",
value:"511113"},
{
label:"犍为县",
value:"511123"},
{
label:"井研县",
value:"511124"},
{
label:"夹江县",
value:"511126"},
{
label:"沐川县",
value:"511129"},
{
label:"峨边彝族自治县",
value:"511132"},
{
label:"马边彝族自治县",
value:"511133"},
{
label:"峨眉山市",
value:"511181"}],
[{
label:"顺庆区",
value:"511302"},
{
label:"高坪区",
value:"511303"},
{
label:"嘉陵区",
value:"511304"},
{
label:"南部县",
value:"511321"},
{
label:"营山县",
value:"511322"},
{
label:"蓬安县",
value:"511323"},
{
label:"仪陇县",
value:"511324"},
{
label:"西充县",
value:"511325"},
{
label:"阆中市",
value:"511381"}],
[{
label:"东坡区",
value:"511402"},
{
label:"彭山区",
value:"511403"},
{
label:"仁寿县",
value:"511421"},
{
label:"洪雅县",
value:"511423"},
{
label:"丹棱县",
value:"511424"},
{
label:"青神县",
value:"511425"}],
[{
label:"翠屏区",
value:"511502"},
{
label:"南溪区",
value:"511503"},
{
label:"宜宾县",
value:"511521"},
{
label:"江安县",
value:"511523"},
{
label:"长宁县",
value:"511524"},
{
label:"高县",
value:"511525"},
{
label:"珙县",
value:"511526"},
{
label:"筠连县",
value:"511527"},
{
label:"兴文县",
value:"511528"},
{
label:"屏山县",
value:"511529"}],
[{
label:"广安区",
value:"511602"},
{
label:"前锋区",
value:"511603"},
{
label:"岳池县",
value:"511621"},
{
label:"武胜县",
value:"511622"},
{
label:"邻水县",
value:"511623"},
{
label:"华蓥市",
value:"511681"}],
[{
label:"通川区",
value:"511702"},
{
label:"达川区",
value:"511703"},
{
label:"宣汉县",
value:"511722"},
{
label:"开江县",
value:"511723"},
{
label:"大竹县",
value:"511724"},
{
label:"渠县",
value:"511725"},
{
label:"达州经济开发区",
value:"511771"},
{
label:"万源市",
value:"511781"}],
[{
label:"雨城区",
value:"511802"},
{
label:"名山区",
value:"511803"},
{
label:"荥经县",
value:"511822"},
{
label:"汉源县",
value:"511823"},
{
label:"石棉县",
value:"511824"},
{
label:"天全县",
value:"511825"},
{
label:"芦山县",
value:"511826"},
{
label:"宝兴县",
value:"511827"}],
[{
label:"巴州区",
value:"511902"},
{
label:"恩阳区",
value:"511903"},
{
label:"通江县",
value:"511921"},
{
label:"南江县",
value:"511922"},
{
label:"平昌县",
value:"511923"},
{
label:"巴中经济开发区",
value:"511971"}],
[{
label:"雁江区",
value:"512002"},
{
label:"安岳县",
value:"512021"},
{
label:"乐至县",
value:"512022"}],
[{
label:"马尔康市",
value:"513201"},
{
label:"汶川县",
value:"513221"},
{
label:"理县",
value:"513222"},
{
label:"茂县",
value:"513223"},
{
label:"松潘县",
value:"513224"},
{
label:"九寨沟县",
value:"513225"},
{
label:"金川县",
value:"513226"},
{
label:"小金县",
value:"513227"},
{
label:"黑水县",
value:"513228"},
{
label:"壤塘县",
value:"513230"},
{
label:"阿坝县",
value:"513231"},
{
label:"若尔盖县",
value:"513232"},
{
label:"红原县",
value:"513233"}],
[{
label:"康定市",
value:"513301"},
{
label:"泸定县",
value:"513322"},
{
label:"丹巴县",
value:"513323"},
{
label:"九龙县",
value:"513324"},
{
label:"雅江县",
value:"513325"},
{
label:"道孚县",
value:"513326"},
{
label:"炉霍县",
value:"513327"},
{
label:"甘孜县",
value:"513328"},
{
label:"新龙县",
value:"513329"},
{
label:"德格县",
value:"513330"},
{
label:"白玉县",
value:"513331"},
{
label:"石渠县",
value:"513332"},
{
label:"色达县",
value:"513333"},
{
label:"理塘县",
value:"513334"},
{
label:"巴塘县",
value:"513335"},
{
label:"乡城县",
value:"513336"},
{
label:"稻城县",
value:"513337"},
{
label:"得荣县",
value:"513338"}],
[{
label:"西昌市",
value:"513401"},
{
label:"木里藏族自治县",
value:"513422"},
{
label:"盐源县",
value:"513423"},
{
label:"德昌县",
value:"513424"},
{
label:"会理县",
value:"513425"},
{
label:"会东县",
value:"513426"},
{
label:"宁南县",
value:"513427"},
{
label:"普格县",
value:"513428"},
{
label:"布拖县",
value:"513429"},
{
label:"金阳县",
value:"513430"},
{
label:"昭觉县",
value:"513431"},
{
label:"喜德县",
value:"513432"},
{
label:"冕宁县",
value:"513433"},
{
label:"越西县",
value:"513434"},
{
label:"甘洛县",
value:"513435"},
{
label:"美姑县",
value:"513436"},
{
label:"雷波县",
value:"513437"}]],
[[{
label:"南明区",
value:"520102"},
{
label:"云岩区",
value:"520103"},
{
label:"花溪区",
value:"520111"},
{
label:"乌当区",
value:"520112"},
{
label:"白云区",
value:"520113"},
{
label:"观山湖区",
value:"520115"},
{
label:"开阳县",
value:"520121"},
{
label:"息烽县",
value:"520122"},
{
label:"修文县",
value:"520123"},
{
label:"清镇市",
value:"520181"}],
[{
label:"钟山区",
value:"520201"},
{
label:"六枝特区",
value:"520203"},
{
label:"水城县",
value:"520221"},
{
label:"盘州市",
value:"520281"}],
[{
label:"红花岗区",
value:"520302"},
{
label:"汇川区",
value:"520303"},
{
label:"播州区",
value:"520304"},
{
label:"桐梓县",
value:"520322"},
{
label:"绥阳县",
value:"520323"},
{
label:"正安县",
value:"520324"},
{
label:"道真仡佬族苗族自治县",
value:"520325"},
{
label:"务川仡佬族苗族自治县",
value:"520326"},
{
label:"凤冈县",
value:"520327"},
{
label:"湄潭县",
value:"520328"},
{
label:"余庆县",
value:"520329"},
{
label:"习水县",
value:"520330"},
{
label:"赤水市",
value:"520381"},
{
label:"仁怀市",
value:"520382"}],
[{
label:"西秀区",
value:"520402"},
{
label:"平坝区",
value:"520403"},
{
label:"普定县",
value:"520422"},
{
label:"镇宁布依族苗族自治县",
value:"520423"},
{
label:"关岭布依族苗族自治县",
value:"520424"},
{
label:"紫云苗族布依族自治县",
value:"520425"}],
[{
label:"七星关区",
value:"520502"},
{
label:"大方县",
value:"520521"},
{
label:"黔西县",
value:"520522"},
{
label:"金沙县",
value:"520523"},
{
label:"织金县",
value:"520524"},
{
label:"纳雍县",
value:"520525"},
{
label:"威宁彝族回族苗族自治县",
value:"520526"},
{
label:"赫章县",
value:"520527"}],
[{
label:"碧江区",
value:"520602"},
{
label:"万山区",
value:"520603"},
{
label:"江口县",
value:"520621"},
{
label:"玉屏侗族自治县",
value:"520622"},
{
label:"石阡县",
value:"520623"},
{
label:"思南县",
value:"520624"},
{
label:"印江土家族苗族自治县",
value:"520625"},
{
label:"德江县",
value:"520626"},
{
label:"沿河土家族自治县",
value:"520627"},
{
label:"松桃苗族自治县",
value:"520628"}],
[{
label:"兴义市",
value:"522301"},
{
label:"兴仁县",
value:"522322"},
{
label:"普安县",
value:"522323"},
{
label:"晴隆县",
value:"522324"},
{
label:"贞丰县",
value:"522325"},
{
label:"望谟县",
value:"522326"},
{
label:"册亨县",
value:"522327"},
{
label:"安龙县",
value:"522328"}],
[{
label:"凯里市",
value:"522601"},
{
label:"黄平县",
value:"522622"},
{
label:"施秉县",
value:"522623"},
{
label:"三穗县",
value:"522624"},
{
label:"镇远县",
value:"522625"},
{
label:"岑巩县",
value:"522626"},
{
label:"天柱县",
value:"522627"},
{
label:"锦屏县",
value:"522628"},
{
label:"剑河县",
value:"522629"},
{
label:"台江县",
value:"522630"},
{
label:"黎平县",
value:"522631"},
{
label:"榕江县",
value:"522632"},
{
label:"从江县",
value:"522633"},
{
label:"雷山县",
value:"522634"},
{
label:"麻江县",
value:"522635"},
{
label:"丹寨县",
value:"522636"}],
[{
label:"都匀市",
value:"522701"},
{
label:"福泉市",
value:"522702"},
{
label:"荔波县",
value:"522722"},
{
label:"贵定县",
value:"522723"},
{
label:"瓮安县",
value:"522725"},
{
label:"独山县",
value:"522726"},
{
label:"平塘县",
value:"522727"},
{
label:"罗甸县",
value:"522728"},
{
label:"长顺县",
value:"522729"},
{
label:"龙里县",
value:"522730"},
{
label:"惠水县",
value:"522731"},
{
label:"三都水族自治县",
value:"522732"}]],
[[{
label:"五华区",
value:"530102"},
{
label:"盘龙区",
value:"530103"},
{
label:"官渡区",
value:"530111"},
{
label:"西山区",
value:"530112"},
{
label:"东川区",
value:"530113"},
{
label:"呈贡区",
value:"530114"},
{
label:"晋宁区",
value:"530115"},
{
label:"富民县",
value:"530124"},
{
label:"宜良县",
value:"530125"},
{
label:"石林彝族自治县",
value:"530126"},
{
label:"嵩明县",
value:"530127"},
{
label:"禄劝彝族苗族自治县",
value:"530128"},
{
label:"寻甸回族彝族自治县",
value:"530129"},
{
label:"安宁市",
value:"530181"}],
[{
label:"麒麟区",
value:"530302"},
{
label:"沾益区",
value:"530303"},
{
label:"马龙县",
value:"530321"},
{
label:"陆良县",
value:"530322"},
{
label:"师宗县",
value:"530323"},
{
label:"罗平县",
value:"530324"},
{
label:"富源县",
value:"530325"},
{
label:"会泽县",
value:"530326"},
{
label:"宣威市",
value:"530381"}],
[{
label:"红塔区",
value:"530402"},
{
label:"江川区",
value:"530403"},
{
label:"澄江县",
value:"530422"},
{
label:"通海县",
value:"530423"},
{
label:"华宁县",
value:"530424"},
{
label:"易门县",
value:"530425"},
{
label:"峨山彝族自治县",
value:"530426"},
{
label:"新平彝族傣族自治县",
value:"530427"},
{
label:"元江哈尼族彝族傣族自治县",
value:"530428"}],
[{
label:"隆阳区",
value:"530502"},
{
label:"施甸县",
value:"530521"},
{
label:"龙陵县",
value:"530523"},
{
label:"昌宁县",
value:"530524"},
{
label:"腾冲市",
value:"530581"}],
[{
label:"昭阳区",
value:"530602"},
{
label:"鲁甸县",
value:"530621"},
{
label:"巧家县",
value:"530622"},
{
label:"盐津县",
value:"530623"},
{
label:"大关县",
value:"530624"},
{
label:"永善县",
value:"530625"},
{
label:"绥江县",
value:"530626"},
{
label:"镇雄县",
value:"530627"},
{
label:"彝良县",
value:"530628"},
{
label:"威信县",
value:"530629"},
{
label:"水富县",
value:"530630"}],
[{
label:"古城区",
value:"530702"},
{
label:"玉龙纳西族自治县",
value:"530721"},
{
label:"永胜县",
value:"530722"},
{
label:"华坪县",
value:"530723"},
{
label:"宁蒗彝族自治县",
value:"530724"}],
[{
label:"思茅区",
value:"530802"},
{
label:"宁洱哈尼族彝族自治县",
value:"530821"},
{
label:"墨江哈尼族自治县",
value:"530822"},
{
label:"景东彝族自治县",
value:"530823"},
{
label:"景谷傣族彝族自治县",
value:"530824"},
{
label:"镇沅彝族哈尼族拉祜族自治县",
value:"530825"},
{
label:"江城哈尼族彝族自治县",
value:"530826"},
{
label:"孟连傣族拉祜族佤族自治县",
value:"530827"},
{
label:"澜沧拉祜族自治县",
value:"530828"},
{
label:"西盟佤族自治县",
value:"530829"}],
[{
label:"临翔区",
value:"530902"},
{
label:"凤庆县",
value:"530921"},
{
label:"云县",
value:"530922"},
{
label:"永德县",
value:"530923"},
{
label:"镇康县",
value:"530924"},
{
label:"双江拉祜族佤族布朗族傣族自治县",
value:"530925"},
{
label:"耿马傣族佤族自治县",
value:"530926"},
{
label:"沧源佤族自治县",
value:"530927"}],
[{
label:"楚雄市",
value:"532301"},
{
label:"双柏县",
value:"532322"},
{
label:"牟定县",
value:"532323"},
{
label:"南华县",
value:"532324"},
{
label:"姚安县",
value:"532325"},
{
label:"大姚县",
value:"532326"},
{
label:"永仁县",
value:"532327"},
{
label:"元谋县",
value:"532328"},
{
label:"武定县",
value:"532329"},
{
label:"禄丰县",
value:"532331"}],
[{
label:"个旧市",
value:"532501"},
{
label:"开远市",
value:"532502"},
{
label:"蒙自市",
value:"532503"},
{
label:"弥勒市",
value:"532504"},
{
label:"屏边苗族自治县",
value:"532523"},
{
label:"建水县",
value:"532524"},
{
label:"石屏县",
value:"532525"},
{
label:"泸西县",
value:"532527"},
{
label:"元阳县",
value:"532528"},
{
label:"红河县",
value:"532529"},
{
label:"金平苗族瑶族傣族自治县",
value:"532530"},
{
label:"绿春县",
value:"532531"},
{
label:"河口瑶族自治县",
value:"532532"}],
[{
label:"文山市",
value:"532601"},
{
label:"砚山县",
value:"532622"},
{
label:"西畴县",
value:"532623"},
{
label:"麻栗坡县",
value:"532624"},
{
label:"马关县",
value:"532625"},
{
label:"丘北县",
value:"532626"},
{
label:"广南县",
value:"532627"},
{
label:"富宁县",
value:"532628"}],
[{
label:"景洪市",
value:"532801"},
{
label:"勐海县",
value:"532822"},
{
label:"勐腊县",
value:"532823"}],
[{
label:"大理市",
value:"532901"},
{
label:"漾濞彝族自治县",
value:"532922"},
{
label:"祥云县",
value:"532923"},
{
label:"宾川县",
value:"532924"},
{
label:"弥渡县",
value:"532925"},
{
label:"南涧彝族自治县",
value:"532926"},
{
label:"巍山彝族回族自治县",
value:"532927"},
{
label:"永平县",
value:"532928"},
{
label:"云龙县",
value:"532929"},
{
label:"洱源县",
value:"532930"},
{
label:"剑川县",
value:"532931"},
{
label:"鹤庆县",
value:"532932"}],
[{
label:"瑞丽市",
value:"533102"},
{
label:"芒市",
value:"533103"},
{
label:"梁河县",
value:"533122"},
{
label:"盈江县",
value:"533123"},
{
label:"陇川县",
value:"533124"}],
[{
label:"泸水市",
value:"533301"},
{
label:"福贡县",
value:"533323"},
{
label:"贡山独龙族怒族自治县",
value:"533324"},
{
label:"兰坪白族普米族自治县",
value:"533325"}],
[{
label:"香格里拉市",
value:"533401"},
{
label:"德钦县",
value:"533422"},
{
label:"维西傈僳族自治县",
value:"533423"}]],
[[{
label:"城关区",
value:"540102"},
{
label:"堆龙德庆区",
value:"540103"},
{
label:"林周县",
value:"540121"},
{
label:"当雄县",
value:"540122"},
{
label:"尼木县",
value:"540123"},
{
label:"曲水县",
value:"540124"},
{
label:"达孜县",
value:"540126"},
{
label:"墨竹工卡县",
value:"540127"},
{
label:"格尔木藏青工业园区",
value:"540171"},
{
label:"拉萨经济技术开发区",
value:"540172"},
{
label:"西藏文化旅游创意园区",
value:"540173"},
{
label:"达孜工业园区",
value:"540174"}],
[{
label:"桑珠孜区",
value:"540202"},
{
label:"南木林县",
value:"540221"},
{
label:"江孜县",
value:"540222"},
{
label:"定日县",
value:"540223"},
{
label:"萨迦县",
value:"540224"},
{
label:"拉孜县",
value:"540225"},
{
label:"昂仁县",
value:"540226"},
{
label:"谢通门县",
value:"540227"},
{
label:"白朗县",
value:"540228"},
{
label:"仁布县",
value:"540229"},
{
label:"康马县",
value:"540230"},
{
label:"定结县",
value:"540231"},
{
label:"仲巴县",
value:"540232"},
{
label:"亚东县",
value:"540233"},
{
label:"吉隆县",
value:"540234"},
{
label:"聂拉木县",
value:"540235"},
{
label:"萨嘎县",
value:"540236"},
{
label:"岗巴县",
value:"540237"}],
[{
label:"卡若区",
value:"540302"},
{
label:"江达县",
value:"540321"},
{
label:"贡觉县",
value:"540322"},
{
label:"类乌齐县",
value:"540323"},
{
label:"丁青县",
value:"540324"},
{
label:"察雅县",
value:"540325"},
{
label:"八宿县",
value:"540326"},
{
label:"左贡县",
value:"540327"},
{
label:"芒康县",
value:"540328"},
{
label:"洛隆县",
value:"540329"},
{
label:"边坝县",
value:"540330"}],
[{
label:"巴宜区",
value:"540402"},
{
label:"工布江达县",
value:"540421"},
{
label:"米林县",
value:"540422"},
{
label:"墨脱县",
value:"540423"},
{
label:"波密县",
value:"540424"},
{
label:"察隅县",
value:"540425"},
{
label:"朗县",
value:"540426"}],
[{
label:"乃东区",
value:"540502"},
{
label:"扎囊县",
value:"540521"},
{
label:"贡嘎县",
value:"540522"},
{
label:"桑日县",
value:"540523"},
{
label:"琼结县",
value:"540524"},
{
label:"曲松县",
value:"540525"},
{
label:"措美县",
value:"540526"},
{
label:"洛扎县",
value:"540527"},
{
label:"加查县",
value:"540528"},
{
label:"隆子县",
value:"540529"},
{
label:"错那县",
value:"540530"},
{
label:"浪卡子县",
value:"540531"}],
[{
label:"那曲县",
value:"542421"},
{
label:"嘉黎县",
value:"542422"},
{
label:"比如县",
value:"542423"},
{
label:"聂荣县",
value:"542424"},
{
label:"安多县",
value:"542425"},
{
label:"申扎县",
value:"542426"},
{
label:"索县",
value:"542427"},
{
label:"班戈县",
value:"542428"},
{
label:"巴青县",
value:"542429"},
{
label:"尼玛县",
value:"542430"},
{
label:"双湖县",
value:"542431"}],
[{
label:"普兰县",
value:"542521"},
{
label:"札达县",
value:"542522"},
{
label:"噶尔县",
value:"542523"},
{
label:"日土县",
value:"542524"},
{
label:"革吉县",
value:"542525"},
{
label:"改则县",
value:"542526"},
{
label:"措勤县",
value:"542527"}]],
[[{
label:"新城区",
value:"610102"},
{
label:"碑林区",
value:"610103"},
{
label:"莲湖区",
value:"610104"},
{
label:"灞桥区",
value:"610111"},
{
label:"未央区",
value:"610112"},
{
label:"雁塔区",
value:"610113"},
{
label:"阎良区",
value:"610114"},
{
label:"临潼区",
value:"610115"},
{
label:"长安区",
value:"610116"},
{
label:"高陵区",
value:"610117"},
{
label:"鄠邑区",
value:"610118"},
{
label:"蓝田县",
value:"610122"},
{
label:"周至县",
value:"610124"}],
[{
label:"王益区",
value:"610202"},
{
label:"印台区",
value:"610203"},
{
label:"耀州区",
value:"610204"},
{
label:"宜君县",
value:"610222"}],
[{
label:"渭滨区",
value:"610302"},
{
label:"金台区",
value:"610303"},
{
label:"陈仓区",
value:"610304"},
{
label:"凤翔县",
value:"610322"},
{
label:"岐山县",
value:"610323"},
{
label:"扶风县",
value:"610324"},
{
label:"眉县",
value:"610326"},
{
label:"陇县",
value:"610327"},
{
label:"千阳县",
value:"610328"},
{
label:"麟游县",
value:"610329"},
{
label:"凤县",
value:"610330"},
{
label:"太白县",
value:"610331"}],
[{
label:"秦都区",
value:"610402"},
{
label:"杨陵区",
value:"610403"},
{
label:"渭城区",
value:"610404"},
{
label:"三原县",
value:"610422"},
{
label:"泾阳县",
value:"610423"},
{
label:"乾县",
value:"610424"},
{
label:"礼泉县",
value:"610425"},
{
label:"永寿县",
value:"610426"},
{
label:"彬县",
value:"610427"},
{
label:"长武县",
value:"610428"},
{
label:"旬邑县",
value:"610429"},
{
label:"淳化县",
value:"610430"},
{
label:"武功县",
value:"610431"},
{
label:"兴平市",
value:"610481"}],
[{
label:"临渭区",
value:"610502"},
{
label:"华州区",
value:"610503"},
{
label:"潼关县",
value:"610522"},
{
label:"大荔县",
value:"610523"},
{
label:"合阳县",
value:"610524"},
{
label:"澄城县",
value:"610525"},
{
label:"蒲城县",
value:"610526"},
{
label:"白水县",
value:"610527"},
{
label:"富平县",
value:"610528"},
{
label:"韩城市",
value:"610581"},
{
label:"华阴市",
value:"610582"}],
[{
label:"宝塔区",
value:"610602"},
{
label:"安塞区",
value:"610603"},
{
label:"延长县",
value:"610621"},
{
label:"延川县",
value:"610622"},
{
label:"子长县",
value:"610623"},
{
label:"志丹县",
value:"610625"},
{
label:"吴起县",
value:"610626"},
{
label:"甘泉县",
value:"610627"},
{
label:"富县",
value:"610628"},
{
label:"洛川县",
value:"610629"},
{
label:"宜川县",
value:"610630"},
{
label:"黄龙县",
value:"610631"},
{
label:"黄陵县",
value:"610632"}],
[{
label:"汉台区",
value:"610702"},
{
label:"南郑区",
value:"610703"},
{
label:"城固县",
value:"610722"},
{
label:"洋县",
value:"610723"},
{
label:"西乡县",
value:"610724"},
{
label:"勉县",
value:"610725"},
{
label:"宁强县",
value:"610726"},
{
label:"略阳县",
value:"610727"},
{
label:"镇巴县",
value:"610728"},
{
label:"留坝县",
value:"610729"},
{
label:"佛坪县",
value:"610730"}],
[{
label:"榆阳区",
value:"610802"},
{
label:"横山区",
value:"610803"},
{
label:"府谷县",
value:"610822"},
{
label:"靖边县",
value:"610824"},
{
label:"定边县",
value:"610825"},
{
label:"绥德县",
value:"610826"},
{
label:"米脂县",
value:"610827"},
{
label:"佳县",
value:"610828"},
{
label:"吴堡县",
value:"610829"},
{
label:"清涧县",
value:"610830"},
{
label:"子洲县",
value:"610831"},
{
label:"神木市",
value:"610881"}],
[{
label:"汉滨区",
value:"610902"},
{
label:"汉阴县",
value:"610921"},
{
label:"石泉县",
value:"610922"},
{
label:"宁陕县",
value:"610923"},
{
label:"紫阳县",
value:"610924"},
{
label:"岚皋县",
value:"610925"},
{
label:"平利县",
value:"610926"},
{
label:"镇坪县",
value:"610927"},
{
label:"旬阳县",
value:"610928"},
{
label:"白河县",
value:"610929"}],
[{
label:"商州区",
value:"611002"},
{
label:"洛南县",
value:"611021"},
{
label:"丹凤县",
value:"611022"},
{
label:"商南县",
value:"611023"},
{
label:"山阳县",
value:"611024"},
{
label:"镇安县",
value:"611025"},
{
label:"柞水县",
value:"611026"}]],
[[{
label:"城关区",
value:"620102"},
{
label:"七里河区",
value:"620103"},
{
label:"西固区",
value:"620104"},
{
label:"安宁区",
value:"620105"},
{
label:"红古区",
value:"620111"},
{
label:"永登县",
value:"620121"},
{
label:"皋兰县",
value:"620122"},
{
label:"榆中县",
value:"620123"},
{
label:"兰州新区",
value:"620171"}],
[{
label:"嘉峪关市",
value:"620201"}],
[{
label:"金川区",
value:"620302"},
{
label:"永昌县",
value:"620321"}],
[{
label:"白银区",
value:"620402"},
{
label:"平川区",
value:"620403"},
{
label:"靖远县",
value:"620421"},
{
label:"会宁县",
value:"620422"},
{
label:"景泰县",
value:"620423"}],
[{
label:"秦州区",
value:"620502"},
{
label:"麦积区",
value:"620503"},
{
label:"清水县",
value:"620521"},
{
label:"秦安县",
value:"620522"},
{
label:"甘谷县",
value:"620523"},
{
label:"武山县",
value:"620524"},
{
label:"张家川回族自治县",
value:"620525"}],
[{
label:"凉州区",
value:"620602"},
{
label:"民勤县",
value:"620621"},
{
label:"古浪县",
value:"620622"},
{
label:"天祝藏族自治县",
value:"620623"}],
[{
label:"甘州区",
value:"620702"},
{
label:"肃南裕固族自治县",
value:"620721"},
{
label:"民乐县",
value:"620722"},
{
label:"临泽县",
value:"620723"},
{
label:"高台县",
value:"620724"},
{
label:"山丹县",
value:"620725"}],
[{
label:"崆峒区",
value:"620802"},
{
label:"泾川县",
value:"620821"},
{
label:"灵台县",
value:"620822"},
{
label:"崇信县",
value:"620823"},
{
label:"华亭县",
value:"620824"},
{
label:"庄浪县",
value:"620825"},
{
label:"静宁县",
value:"620826"},
{
label:"平凉工业园区",
value:"620871"}],
[{
label:"肃州区",
value:"620902"},
{
label:"金塔县",
value:"620921"},
{
label:"瓜州县",
value:"620922"},
{
label:"肃北蒙古族自治县",
value:"620923"},
{
label:"阿克塞哈萨克族自治县",
value:"620924"},
{
label:"玉门市",
value:"620981"},
{
label:"敦煌市",
value:"620982"}],
[{
label:"西峰区",
value:"621002"},
{
label:"庆城县",
value:"621021"},
{
label:"环县",
value:"621022"},
{
label:"华池县",
value:"621023"},
{
label:"合水县",
value:"621024"},
{
label:"正宁县",
value:"621025"},
{
label:"宁县",
value:"621026"},
{
label:"镇原县",
value:"621027"}],
[{
label:"安定区",
value:"621102"},
{
label:"通渭县",
value:"621121"},
{
label:"陇西县",
value:"621122"},
{
label:"渭源县",
value:"621123"},
{
label:"临洮县",
value:"621124"},
{
label:"漳县",
value:"621125"},
{
label:"岷县",
value:"621126"}],
[{
label:"武都区",
value:"621202"},
{
label:"成县",
value:"621221"},
{
label:"文县",
value:"621222"},
{
label:"宕昌县",
value:"621223"},
{
label:"康县",
value:"621224"},
{
label:"西和县",
value:"621225"},
{
label:"礼县",
value:"621226"},
{
label:"徽县",
value:"621227"},
{
label:"两当县",
value:"621228"}],
[{
label:"临夏市",
value:"622901"},
{
label:"临夏县",
value:"622921"},
{
label:"康乐县",
value:"622922"},
{
label:"永靖县",
value:"622923"},
{
label:"广河县",
value:"622924"},
{
label:"和政县",
value:"622925"},
{
label:"东乡族自治县",
value:"622926"},
{
label:"积石山保安族东乡族撒拉族自治县",
value:"622927"}],
[{
label:"合作市",
value:"623001"},
{
label:"临潭县",
value:"623021"},
{
label:"卓尼县",
value:"623022"},
{
label:"舟曲县",
value:"623023"},
{
label:"迭部县",
value:"623024"},
{
label:"玛曲县",
value:"623025"},
{
label:"碌曲县",
value:"623026"},
{
label:"夏河县",
value:"623027"}]],
[[{
label:"城东区",
value:"630102"},
{
label:"城中区",
value:"630103"},
{
label:"城西区",
value:"630104"},
{
label:"城北区",
value:"630105"},
{
label:"大通回族土族自治县",
value:"630121"},
{
label:"湟中县",
value:"630122"},
{
label:"湟源县",
value:"630123"}],
[{
label:"乐都区",
value:"630202"},
{
label:"平安区",
value:"630203"},
{
label:"民和回族土族自治县",
value:"630222"},
{
label:"互助土族自治县",
value:"630223"},
{
label:"化隆回族自治县",
value:"630224"},
{
label:"循化撒拉族自治县",
value:"630225"}],
[{
label:"门源回族自治县",
value:"632221"},
{
label:"祁连县",
value:"632222"},
{
label:"海晏县",
value:"632223"},
{
label:"刚察县",
value:"632224"}],
[{
label:"同仁县",
value:"632321"},
{
label:"尖扎县",
value:"632322"},
{
label:"泽库县",
value:"632323"},
{
label:"河南蒙古族自治县",
value:"632324"}],
[{
label:"共和县",
value:"632521"},
{
label:"同德县",
value:"632522"},
{
label:"贵德县",
value:"632523"},
{
label:"兴海县",
value:"632524"},
{
label:"贵南县",
value:"632525"}],
[{
label:"玛沁县",
value:"632621"},
{
label:"班玛县",
value:"632622"},
{
label:"甘德县",
value:"632623"},
{
label:"达日县",
value:"632624"},
{
label:"久治县",
value:"632625"},
{
label:"玛多县",
value:"632626"}],
[{
label:"玉树市",
value:"632701"},
{
label:"杂多县",
value:"632722"},
{
label:"称多县",
value:"632723"},
{
label:"治多县",
value:"632724"},
{
label:"囊谦县",
value:"632725"},
{
label:"曲麻莱县",
value:"632726"}],
[{
label:"格尔木市",
value:"632801"},
{
label:"德令哈市",
value:"632802"},
{
label:"乌兰县",
value:"632821"},
{
label:"都兰县",
value:"632822"},
{
label:"天峻县",
value:"632823"},
{
label:"大柴旦行政委员会",
value:"632857"},
{
label:"冷湖行政委员会",
value:"632858"},
{
label:"茫崖行政委员会",
value:"632859"}]],
[[{
label:"兴庆区",
value:"640104"},
{
label:"西夏区",
value:"640105"},
{
label:"金凤区",
value:"640106"},
{
label:"永宁县",
value:"640121"},
{
label:"贺兰县",
value:"640122"},
{
label:"灵武市",
value:"640181"}],
[{
label:"大武口区",
value:"640202"},
{
label:"惠农区",
value:"640205"},
{
label:"平罗县",
value:"640221"}],
[{
label:"利通区",
value:"640302"},
{
label:"红寺堡区",
value:"640303"},
{
label:"盐池县",
value:"640323"},
{
label:"同心县",
value:"640324"},
{
label:"青铜峡市",
value:"640381"}],
[{
label:"原州区",
value:"640402"},
{
label:"西吉县",
value:"640422"},
{
label:"隆德县",
value:"640423"},
{
label:"泾源县",
value:"640424"},
{
label:"彭阳县",
value:"640425"}],
[{
label:"沙坡头区",
value:"640502"},
{
label:"中宁县",
value:"640521"},
{
label:"海原县",
value:"640522"}]],
[[{
label:"天山区",
value:"650102"},
{
label:"沙依巴克区",
value:"650103"},
{
label:"新市区",
value:"650104"},
{
label:"水磨沟区",
value:"650105"},
{
label:"头屯河区",
value:"650106"},
{
label:"达坂城区",
value:"650107"},
{
label:"米东区",
value:"650109"},
{
label:"乌鲁木齐县",
value:"650121"},
{
label:"乌鲁木齐经济技术开发区",
value:"650171"},
{
label:"乌鲁木齐高新技术产业开发区",
value:"650172"}],
[{
label:"独山子区",
value:"650202"},
{
label:"克拉玛依区",
value:"650203"},
{
label:"白碱滩区",
value:"650204"},
{
label:"乌尔禾区",
value:"650205"}],
[{
label:"高昌区",
value:"650402"},
{
label:"鄯善县",
value:"650421"},
{
label:"托克逊县",
value:"650422"}],
[{
label:"伊州区",
value:"650502"},
{
label:"巴里坤哈萨克自治县",
value:"650521"},
{
label:"伊吾县",
value:"650522"}],
[{
label:"昌吉市",
value:"652301"},
{
label:"阜康市",
value:"652302"},
{
label:"呼图壁县",
value:"652323"},
{
label:"玛纳斯县",
value:"652324"},
{
label:"奇台县",
value:"652325"},
{
label:"吉木萨尔县",
value:"652327"},
{
label:"木垒哈萨克自治县",
value:"652328"}],
[{
label:"博乐市",
value:"652701"},
{
label:"阿拉山口市",
value:"652702"},
{
label:"精河县",
value:"652722"},
{
label:"温泉县",
value:"652723"}],
[{
label:"库尔勒市",
value:"652801"},
{
label:"轮台县",
value:"652822"},
{
label:"尉犁县",
value:"652823"},
{
label:"若羌县",
value:"652824"},
{
label:"且末县",
value:"652825"},
{
label:"焉耆回族自治县",
value:"652826"},
{
label:"和静县",
value:"652827"},
{
label:"和硕县",
value:"652828"},
{
label:"博湖县",
value:"652829"},
{
label:"库尔勒经济技术开发区",
value:"652871"}],
[{
label:"阿克苏市",
value:"652901"},
{
label:"温宿县",
value:"652922"},
{
label:"库车县",
value:"652923"},
{
label:"沙雅县",
value:"652924"},
{
label:"新和县",
value:"652925"},
{
label:"拜城县",
value:"652926"},
{
label:"乌什县",
value:"652927"},
{
label:"阿瓦提县",
value:"652928"},
{
label:"柯坪县",
value:"652929"}],
[{
label:"阿图什市",
value:"653001"},
{
label:"阿克陶县",
value:"653022"},
{
label:"阿合奇县",
value:"653023"},
{
label:"乌恰县",
value:"653024"}],
[{
label:"喀什市",
value:"653101"},
{
label:"疏附县",
value:"653121"},
{
label:"疏勒县",
value:"653122"},
{
label:"英吉沙县",
value:"653123"},
{
label:"泽普县",
value:"653124"},
{
label:"莎车县",
value:"653125"},
{
label:"叶城县",
value:"653126"},
{
label:"麦盖提县",
value:"653127"},
{
label:"岳普湖县",
value:"653128"},
{
label:"伽师县",
value:"653129"},
{
label:"巴楚县",
value:"653130"},
{
label:"塔什库尔干塔吉克自治县",
value:"653131"}],
[{
label:"和田市",
value:"653201"},
{
label:"和田县",
value:"653221"},
{
label:"墨玉县",
value:"653222"},
{
label:"皮山县",
value:"653223"},
{
label:"洛浦县",
value:"653224"},
{
label:"策勒县",
value:"653225"},
{
label:"于田县",
value:"653226"},
{
label:"民丰县",
value:"653227"}],
[{
label:"伊宁市",
value:"654002"},
{
label:"奎屯市",
value:"654003"},
{
label:"霍尔果斯市",
value:"654004"},
{
label:"伊宁县",
value:"654021"},
{
label:"察布查尔锡伯自治县",
value:"654022"},
{
label:"霍城县",
value:"654023"},
{
label:"巩留县",
value:"654024"},
{
label:"新源县",
value:"654025"},
{
label:"昭苏县",
value:"654026"},
{
label:"特克斯县",
value:"654027"},
{
label:"尼勒克县",
value:"654028"}],
[{
label:"塔城市",
value:"654201"},
{
label:"乌苏市",
value:"654202"},
{
label:"额敏县",
value:"654221"},
{
label:"沙湾县",
value:"654223"},
{
label:"托里县",
value:"654224"},
{
label:"裕民县",
value:"654225"},
{
label:"和布克赛尔蒙古自治县",
value:"654226"}],
[{
label:"阿勒泰市",
value:"654301"},
{
label:"布尔津县",
value:"654321"},
{
label:"富蕴县",
value:"654322"},
{
label:"福海县",
value:"654323"},
{
label:"哈巴河县",
value:"654324"},
{
label:"青河县",
value:"654325"},
{
label:"吉木乃县",
value:"654326"}],
[{
label:"石河子市",
value:"659001"},
{
label:"阿拉尔市",
value:"659002"},
{
label:"图木舒克市",
value:"659003"},
{
label:"五家渠市",
value:"659004"},
{
label:"铁门关市",
value:"659006"}]],
[[{
label:"台北",
value:"660101"}],
[{
label:"高雄",
value:"660201"}],
[{
label:"基隆",
value:"660301"}],
[{
label:"台中",
value:"660401"}],
[{
label:"台南",
value:"660501"}],
[{
label:"新竹",
value:"660601"}],
[{
label:"嘉义",
value:"660701"}],
[{
label:"宜兰",
value:"660801"}],
[{
label:"桃园",
value:"660901"}],
[{
label:"苗栗",
value:"661001"}],
[{
label:"彰化",
value:"661101"}],
[{
label:"南投",
value:"661201"}],
[{
label:"云林",
value:"661301"}],
[{
label:"屏东",
value:"661401"}],
[{
label:"台东",
value:"661501"}],
[{
label:"花莲",
value:"661601"}],
[{
label:"澎湖",
value:"661701"}]],
[[{
label:"香港岛",
value:"670101"}],
[{
label:"九龙",
value:"670201"}],
[{
label:"新界",
value:"670301"}]],
[[{
label:"澳门半岛",
value:"680101"}],
[{
label:"氹仔岛",
value:"680201"}],
[{
label:"路环岛",
value:"680301"}],
[{
label:"路氹城",
value:"680401"}]],
[[{
label:"钓鱼岛全岛",
value:"690101"}]]];

},
"2eee":function eee(e,l,a){
var t=a("7ec2")();
e.exports=t;
},
3162:function _(e,l,a){
"use strict";
(function(l){
var t=a("4ea4"),n=t(a("9523"));
t(a("0863")),e.exports=(0,n.default)({
checkUpdate:function checkUpdate(){
this.checkUpdate();
}},
"checkUpdate",function(){
var e=l.getUpdateManager();
e.onCheckForUpdate(function(e){
console.log(e.hasUpdate);
}),e.onUpdateReady(function(a){
l.showModal({
title:"更新提示",
content:"新版本已经准备好，是否重启应用？",
success:function success(l){
l.confirm&&e.applyUpdate();
}});

}),e.onUpdateFailed(function(e){});
});
}).call(this,a("543d").default);
},
"35ce":function ce(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAAC91BMVEUi9//n/v8h9f0AAAAh8/v///8g6fEh8vog6/MdHR8g7fUi8Pkh8Pgf5Owg7/cg7vYe2+Mf4uoe2uIe3eWj9fcf5+8f5u6f9/kd2OAf4+uj9vgd1dwe4Ogcz9YbydAYr7Wi9/mn9Pai9vgczdQc0dgawMcXp60e3+cbxs0avsWl9feg9vim9PYZucAc09obxcyo8/Ud194ZtrwYs7kXrbMd1t0Xqa+q8vQby9Ifio6p8/UZub8avMMXq7EczNMTExQBAwQf5OsEBAUbyM8ZuL6g+PoYtbsZu8IYsbdMTEwYsrgODg8aw8qvr687OzsaGhsXFxis8fMfiY4fh4wICAkawsmm9fchIyUegoceHyEVFRcMDAwBCgqd+fsrMjT7+/ut8fOMl5hRbnA6Sk1ISEgRERLv7++0tLQehYpeXl4MW141QkQlJygCERLz8/NdhogbbnILVFdDQ0P9/f3f39/Hx8d7sbORkZETjJAcd3tAV1kJRUgxMTEFKSoFJCUDFxic8PLq6urX19eS0tTS0tKMzM5unJ5olJYden9+fn4Qd3t1dXVMamwbZGhEXV9XV1cIQkQIPkExPkAtNjgGMjQnLjAtLS0DHR6S4+Xk5OSi4uTb29x1sbMWpaoWoacYmqBgj5Eef4QdfYJWeXoPc3hubm4baW1OZ2kNZWlJZGYKTVD4+Pil8PKi7e+Z4eKU2tyI0dPDw8ODu756ubqBsLJ1qqtyo6VsoaOWlpYThIiCgoIccndTdHYObXFoaGhkZGQ8TlAHOjweODwlKiwdKCqr8/WN19nLy8uFwcN5vL+8vLwfn6WcnJxjiYsQfYF7e3sfU1ccRko3Nzeh8vSl5uic5ugi3OPNzc0hxs27u7sgtbu4uLiqqqogpKqmpqagoKCLi4uGhoZbfX8aXmIfWFtOTk4HNjgi6vEh4Och09siz9bU1NSDy86Mx8mIvb4hrLEhq7Ctra2ZmZkUkZYgkJUTiIwTh4tfgoRQUFAKSUwWMTMPHB4OFhfkODBHAAAdz0lEQVR42uxaZ2wcRRQ+5ia7O1tv73x3vrgksXGcEMcOJiYQDDaGOIABW2AwdnAQJKEZMNVA6IQklBBC6D0QWuhFCR0CoRfRhOiiiyIQRRSB+MG892Z9PpsS6RbpQHna7Lc7O5a+ffvNNzPvEtnovxv7biC/frGB/AbyhRAbyK9nbCD/PyJfuJHSOAB3DAC9QWMSWHUCbxOaG9kyUrChOw5H1GwA7iEwbsoT3Eciu0cKN3SOwBgCMOacIWgWNNjXRgo0mCLOEb0E3hlxiwFMQAV5x0cKM4wGZM99G5DFkzpAivRukZAie0UKMxzDRZqmR0K3Iog2qUgeLosU7oDlWSC23KMGzWCg92QlL0zyNkOwCEyNk94dVH9dCoVUFksWJPlkwqYcJxmA7yB5t9TAVhOkI4WkRSK7RQoumJW0KOUEOifl8EEReeSYWiFaJWPDHVO3LLzRSvAutR06pnN4pMDCJM66h7RddEzmlhiAvNzR0ThjKH+70KzSK0H2zKd1jU+ObqQ0eiXlmFL9nBWe5nVXGbs7aJUMmqGJ9G5gu1aqscJzG5YFjtfcQGCOz9GCJmRQ/Vs5Bebzno5gGQhmzAZwEwkGjQ0ZjqwTqHfdMAtLNm7cZKjolA7gpkjhKQPB1skxJRj0acwQlsSpEzaXOXFOGM8j6x0TD26V50Y4Z4O7trIaAqCqnhCarkd63wbAjt2YP/lto9FTzt40g+f1e9vjTopGp9I5R+wMAUOns2lgmzYBgBuNBoxae6Lj4grhzDDIQ5yP52Xj9b/ti+neG/vjOUs+pejS8LRqTfScTTPYXJK0ATKOD8B1j97zwDDIN919UGdUxSnXl/xT0pvnfj+XOmfJ6zEH+Zgap5S7tFLQ1GP6JLY8yDENGLZbhkJ+1Kj9j704GsSyq6w/7zn54JZotP3lo0eN2nEY+ez8w7N7PgU2zbUpThaUAOSJyaWhkYc4Jpv+Q6+vHdntp5Mh6bvsA32HkyfCgd51WlQ69LHKkL2xRQzArPR1dP14MlTyMv33Z9N//tU56d/qCEj6WUdDtxHktTgC9xlyLybH9GMugOZYNEklyU/tQcfUQiRP6X9s1mD6H9lY9fB/hqTvSEkfSZ4laAVjax6S95BtsHViqCAMjREYBkCm8qGwyB901ulB+k8ckn7Jqp6Sfhk93ueA894dTp6N1Dsj/qbaS2UYgD8TQHfGaBw+yWbXhUV+12j0xA+VKp4Zkv7HL4GkH7CPenJDTzS6Sw55HkxMxBORG2p4Vhn4UlU+mk85TbYx36E5LU+rNIaQh9j12NOQ5GE/ZNMvHlRJv+LBQ7AhS755aiST5NXAhBa9PE6O6dBO0NNcbLUzgVUyAAsmW9mSp+aPP/etLHnic+S7RPXNRyH9LZ8dcBTdv6d6ZMl39sybyozxy25Gb7HJ2JXe3cH5VicwTEZWyQG0CcV63m7zvhCvvpBLPnqQvKP0v3gkJp2iO+igyM/qEGLW1uMvjEavyd366XStapO2E8e72AQXmjONxQBOGOS/EjLaOw7Lkr8eyY+M09TzvX8F8nNntQsZLadA03ZqpLox2iY1cIY1DhOA1xaj7lMke1atkV6lgnie5PmTYsVyIbrPOz0gf/ZfkD8gMKBborscdo4kvnyFEPQ6drWq6WmW2iQBeJIfFYkDK2I6fZ4Mp9fI5LuHfUU813vvfMmlozMgLz46YwT1o489Mfgyt7TM7RZizpXp50QHtjzOrZE1PRK6YptS2fZxW5IsawXwiqfmS/46saK3t/eu/bokffTHS5rlqfuG57MT0lHHvJydeJuv+U0IMf85+UcrxDxsegSp5hg7c2wAK4XDkzv1qBx7s4QNWJXAXsV+vuS/FUvT6d5076X9F0nxtwHzQy8EJbe9GYgd80sCOeKBT7pF96qve+FvlgqaDa7GrAY1PR9HaXkG9V4cM+mhQVNvil7SjTAmUc93eVAsZqcpTr2vTwgxrwVJ3nxy9FFF/thgrfxI6zuvCNH18NNpitmiBR/U4gqGhGxVI03mWWp1HyiI2QwvU2oeqDTDqBI/KS6dkZ6RTsvTDBR/D4p/WXTerUQ+EPvV370qxLr+S1XfGZeKdpqDSS8janomOaamubS/rcXpqXyiAViyfTwM8p+KK2dASEYyAvFDvIfcT1da//0JIWYvPnXwRdNXih58cgLSBWIWItPKyGSqOIO7ychWr8NqFM/EyTET8E3yJr+76E8Pjaz45yL5c5B6W7v0xvtOpS5Evl+0kT1F3BIdJ9eNVU0vZSG9hI96N8zBeggjx/QY8o95eVcP3hHzA97DxN98GViN1EbLPIHemBvzBZnr1kwpw9aSOY7pDTqmRZ/GTwCy0hKcZKurWvMm74t1Y9Nj0+lJ6UmTxk4ai5Em8Z8lyb8dbWmS10ufVc/lofqtE81IvhqFjjnP/mZDxq7FPKpxlKBjNoxJopB2SuCnmRnLfwP+hHiaKA8SgwDxd8Eyv02I7iV3QXtun6fVeL2E0Z6P5qmMT2Wx7UoRqnzUe2Ux1Wt85ZgaLYW8/HdS54rVQUYVUlwpOp4ZdVhnu3j4zopJFfKg5uB6tRqvy0jvMZqOYkTeo5oeNz2SjRl4ENM4A6iOsUgY5b4fxcoKFWMrxspD4amHiINGfdApluc+C2JlMF49yrwx3DENtRTWGendxOVZeRmHnM+cqLEwyDeKtaNVVIyuqCiqKJIH4BLRceuRPWIFtkNIgGvCOWq8tg4OS67TCsa1aXlWDmAl6nX8JDtoqJTGTV1QTmncC6Vu87noen10UdHootHDYkB0fNgsuu7AZ8Pj9S41XjXp6C7V9Op0AG8y1vT0iY4HtxvTj4BaygQABTHO4AuxJIfM5z1iF0pyRfgChBRSN51tYlVO22CfhWq8ngwuUszR2JMuLWRKSUAmfRLZqDPUu2GhguIl+FYlVbUhkL9R3Fc0nBziEiHnpntHPoNYJJqQ/MGQazTGoY5p06Xp4wOnXEOLLC/TIfd1Y1IgMn9MIoQS9zXiYclmk6JNMORFEANCiAuK6Bm1Z6/3U+P1OKV3TjW9pAZgx8l7EjMT+Ekmx1Dv5cUu/UxrAFheGEWnt0SfJF2zSU1NUQ2RU3H7IUK8BO3y4fDoU+N1+xhtNxpRGu4WMUxubSkA0+JUUkgqx/QiTCko4zEJIVSJvxCia3bf2qVLVvYvXjNwz8I736iRsWfNnnsuEd13AsqbmjfuXHjPwJrF/SuXLF3bN7tLbQGb4zG0xExGOWYyqFYGjpkkfy818XWKS0yUfesWPKQSd4cYEhfst+aOgPCAWFoTvMgdq/ebLbKhpqiTuDmkSoaANRAJmgbgVNbhXVm9C3oqm5picLsVzrm7h1Exm3f0GcdIiYiLVi1aUDOuRh7jxgHefsgAYhALFs+XKRdnHXPGkUHFDEjTePUcdHLbn6ijVMbgKPXqE8CabRPDXknHCOZcS2ehFVpfE+KeccNiyse3TRk3Jafp9n7RkVvuixg7o1bsPYpJGVTTcxxaIYDQaVfOPU56NznWSbYIr0q8a49YpIhKWLgQrqfcNgXIyyOL0mZyyTOXVsTMzCA3plPFVf4zTWLbwLFEOTHDYEW8WTkO6sk7OLuFRn6WWCvJyViwaNVFomvROCQLMXBB3y/P3iavIS4QLUPJM1oGuyT7lE01vUQSwKjb1MJF5eRqnGvr45wqfdg7mQgx881CLJDEb4JhuXzOnDmLiez0216SN31i3U1r7pg+ZfoC0T60Sry5ScaeqgTQE60Wbf1wzjUqSUhJ5ZiOiS9qu1LvOnyavcIrcTeJPvST9naxavqw+FhAzHnpm8WiaSj58fUG5r42jpYYy6De7aRGexJLGjtt/XyGS+EGxwLJxGjODbE+3wnEmz5pibYIsXra9GlEW15Mm/5st1zm9AiMWUPJXxX8PwkNALLJlGNmDIazq59BY6/aGW79ma3omBPGONDr2vDIR5vaZCGm8eDjKl8QF12uiGPcvhwXA82dr8oSwgnRYZpXM1LSpxVx3Gcoma1sBjkmx9R2wh0hr4slyYpwMxXegFV0GqkectM0FU9Ne+qplaoqeZIe+fL9W3LID1YzmNOYAjDqccOq11JNz3dS9HumiVUyZkUMz4YvZDiOteW/Qv4P1q0l9IYojN+O05njzMydV3O7070zd3Fx73+BuF7lGULeiuSRZOEVQoRkY0USIkLYeMWKLLylrCRKUjZkwUbZ2Cgb8/u+uZ7bmeQzD/Wde37nd77v933n7bPBd+A5X18HszY2bSjqy/8532xoLh9rlm1q3ABn5Z4KuCljflLv4t7qjI7wOBnWNaU7P5OeAThLloxfMj7/8+ZkEUG+VNDY/4YNJdR94UAYWpaVJguXSZsQlEwcganxRvcoK++OppAzmJ+V/8tv3Wux9H1tSXE9LEBzSgpZmf150F8LVnLErlyuLlCDhMlahrajmUTs7oy6TUzpg4NyqzFQlBc2luX8F7QTcHI0pgAO+367D5q8z2TG/kGDVu65+Yfzs2kz1S1K/eT8OlhHjPI1R/eaNtsc5MIQY5qkiqnRIWWyel8pgRmJ8Ru29N1/mg2//mPwmnNr8+sXaFa/HLkjV77vb8O3/cDsUtIkivEUl7V1v7wtq0zsPhG7GNHgtGTWMrx2587B4jbbS2lZ2bKB6sN7uFrA1+LB1+D8OgYNX0uPUKWTgkrW+gjvrGF7hPeAhctgRggE1QZmeoDIhDGxhNdzSLCXy1KNb8toCm0dyFXVg1T9vjJ9+ZHnx+AWtqo7a9feAWhw93jDzulPzlK14fG0/vr4JkHsrIONSsmvxuhAwPnRFI1VUy4vRK7LjKkgfWNq4rKOXVh7tw6afmU9uU8NCK/h2rwcOOfWMGiOnce7v6dn2o4QbuqsSZpeY4ghng8M51QKxA7VVRibiF35hHd/AHNhRo0JZRnOAzrXpw2aevd3Her9CgLOunWDV0Ds5grb3wtjf+YnHApzAc3JjcOMWWNNrx5ZtHxn+xJk00o9BNCT54zC04GJKZwv55pwfNCg0+9+OXnz4yoAB6BZeOvX04O/KakF9ug3mysufEd+E6Y2ZSQgEw+0tEAhtkXEHo2sOySWTaHN1vZ1mQdexuV9KZ/O/64aP5mWK8TzTm/gW14RfbDfC7hmE4WwdtKzya9xNYkfdyDURDEwQphYk4ZpvIptBFJd5RtMTfSgUt5l31s9aPoRIBsXSGXxivfceQCw353aB/v1wJvkcztHwkzYcSlDVZpFS0MaB3IqBVsxHVq39qisjveN1nwJPB2tlHk1N+eb0Av4y9eC3xPx/Fct9kSG9RhwKAwIFJqeZA07Zn6fQst3bG8EXqs5Mw1W6cBkimui3hCDp2W3oHef5n03APnf1/nvfdfPDOuX+5Tg/lVF8UTcJSbXIydoIKg7ixiz0aOcyrR5z/XrCl0gllKVQEpR/iGvC2dyZtz1l+vbjvTBvvqeU/F0zMXJoZxuTIwFzFCCvxlCUZhUHUQwxJgSjOlUlFZgzFoyClPjT5ioRdmN0GgSf7l60NJX236X71/82mM3R6iCzOAtv9E0rOnVKrBxlRkzJ3YtiDEjQ2mJP8KmtGQW2MaKhrUV/te41tiyu7gDSt6iHPqLPhTQv/Wr5+nzbMM1JZfHaTHeAeaiWGMMEfsQElb9iUMBKNWjZEpmk4cAQeHIKWBMz59SlSV3cUdNCSNn5j1Ar3ej3ePYL7Bf/EPTM1Xmd06X3Cl0ukI3Rjt01GKYKymnIsaUbZ82W51/i85R5VWUKwAmv+zjdXYfPZfy7olNBw8v7YP9sqLXiRLE6KlDUOmFgoohvoXbdocQ5MbAO7UMV8GUDmr4SALj+iiF9yMHGoBnViZVisKwDR6tpryVorAdQ2a4gpi6YfHZJ6c4NUr8CE1PsKZnDGkITV/mL6rRMgzWCienmItg2PAY0tTEcT7RfYnOq1TASN9mFkkaOwadGvAm3cj/5gY4YPV3Fb5gTNcmvAfNKp53JgUU+7ZG4kNrJjPmjNYEjEW1KRS2wlEaGWO1vNM6QilDVmsyKO6l5CYGRY6zdR0bxnA/uZoxidSCdCYx5ohJIW5r2VhmzIgGK3PjSCEcpxK5TcxSvd4VosQFK2Rhf5/NqrLp9xZ0Ce9xjzQ9MzsgBE2gqqQoMlRRQ/mDGNPWEsdhVSegMK0xAVGcylqop5kRw+YHpVGlXRj2WrPcZUbE5PxwRbdDuK7d1IoBpBhBNqlkAql1lRDUSOIKgD6yTrvXnAENvLfmAkHe/HGppBmaYJd14CUIbNZCuRHJZ8a0XVWcNOCFTN6ypic5QuBRNVMaVTQ0BZMmQ3v43snGuRYxUmqAnHZKjFnzHZkD03Y94R8qx3kZWDAAJdnfNSUGERALozr0nVOn0VXbEzwa6ywfRg33bdiMGTOpUWiA5lDtSVBRGBnECn6WYbnUR18tC/D//tOWsMAyjExSi+6G0tSYCUNwa0eRwzPEmhg0PcmaXjXGDMXJCAssM3Z2Qngfl0ngaVgvwUTNPVoe3mVBhinjfYoHK1PMCaobCsZRhhcFsTlV4R3BfIJQGKGBAbEHsyJMXm3Y0AQBc2s0xfFDiTFFZ0Zb5+UINaoE2Dgz4D3iKMn7o8e/dY2MLBDk/NI4NO9iHfpOJGHEPcLDHWqM6NWI2IcTY3a6MxwQezwCCJLSN1LZUmhHpKFfKYMqLUeTFYbNb6AXe60xZF03hvGioYT3kDQ9WRtNTGh3Q8NnLRRFlK7hkxWyomML69sf0qC4rDvcCARt40YYUGWZeJcFscMiCBDcsBTDmB4eCzkypEH4rmZWVRgvQCc8YkyjNLZTr153MdYhA2MBpPbk2SCrcHILCkg8MC4VZcXzTpWMZsZ0wpDxHkmWYgL+RrF4TWk1MWbVpicBV+sbAx2KEHptLPa+pje/FVpI/VptNLJ63TYhKPFtOyknPBBeIGHsgjFVASTyjcVS2Pw7hxjTHULLVHW5cDa0SwjKej4hZ6LvwI4cojCTQaQqWgrb2LIZW1I6YkKqQKHZpJJyWCn/PeBnMeC1Y5GJGtwc3K3CNIZ3uIWfcGx8n+MHt0YbbVXnRCtZ0wswQ/VGZucvOhlF0P7wYWgrMumwknj+N95tryhwLBNkJmoKAbqBTc6PUAT/oGBMj2bHkn2NQzcjMuFIhZSlPiY0QOGwSQGlfnMbAuXjMW2JCmK3PPXAypgx225Rhfd4FXTopQXA8l5gc4aaWJK+81mSz372ci05CuNANCpZtuUkzk+goASyyAeyAMQvQqJhA+pGiNVILc0R5ihzjlnMJeZyM6/c8103k41Fp0M77fKrqlevnDJ5vfuwgKL3KsbHdsl6uPh59bGTy0aSFn4QB0FsfQpRXPiuPWswkBrz8A/dEv4w8N3JsMpDQtw112NesmZOz88evF1s61K/eYNkikRsPKuBmCLJIhJE9foCidnsVhnxPYHZf+39y9faMY86j4mB8xQSvFI1ETCk3tW1N4n+s+tMEKEtTVtemlEpgZjJ4jLHu64fNSi/t+4kgWNDNaXvnDw8jvZcLGAxmJljImfzr/NdXOFJugxVaE/AcpQDJ/JMmAb49Zzl5SKvZgHM6lzhMbk8F6gPbqqpIqJw3U+E+lbSSYSurOFiR65Bupzkb6WnchmHdJHz2nDE/sBjpB9PZoVvVcM3l9MANGvbaM/3KEslJYqsVn6TBMIaXX/++L3cgf1XLyLq2sKBjuQ742PD2Hh9GrzU9ZYR1+Y3bP650z3bA9wQi1yl9UkISssDws3ruveJqF5WqJpku+7bj8tQvsBgdIIhKt8E2/tDchWk5YhdNQVvSz03/IbaekJxHd+iEQ0cR44hmy0RUYTHbio9T166ZYLlHXZX1Gy3+2+ePDiYAIN4+2oydxFMkrESyLfWBTLqL04vjRSrzNkBiPRto2Ay5zfJfZd7mL867S5MabdHIQSN4s/QjIQxzbcTrdIYtwCOw0aG+mVFxnEccPewlaPj9JYfzOm1zOmp4swWFLW1RpAfNwZmo1ND4QQS7qAoNFkRNW9oyvjuydM/Ob2vNDyWxBz2QbBm8gT0EJsqEbi5dMq3IpG8RUKDHAyImY0F3jstJoCX5tRmCCp33QzwtO22yWvOMVOTkNgGphFDZJ/jo1ynjixzGizDnB5Lho1QSMdGkumzUVxYjnt2zOnVQxshaOvO+NLJdr/wkaZVb4rEKyYv0iJguC9dhppIR33bPwXYwgkMyXXS2+xDO3FwbYCv3ecYT933jJirbQ3EDPvGkvb8uE6CMiAT+tFq9ZP3/RcFf3N6yiGmkxNSVmrC0NYAnXDXGqZ1toyYl48jk2V5HcHeTaFFRHB8FEtLQqhrevRIqcvyFgAxz+dXHcIWOhJbhs5kmpMP/uPwGBHAbhlzwnrJ9T+qIF4ZcFJjlRdOJLZ5elkgQmiWd5R88r67JJ4nn93tQJ6fDvffvJdcQr4zb2p3SYAlmM0Zg+I5m3UQuVBYANgFJq5jjoN0MePsabYrcHvabfH78+qckkd2u+uRUzXtSQlBSf581bFIoZYu9+bZEjJU5XRL4+iL42DQiVvH6VWnCE/ddgkhzxiATDQ6ATHJ5qtAWCFkLWWSEk2EWa0lReIX7yUXebD3L04v+epFLDGWy6niquSWJz+7l8zpbZnT85qcU78wtOA3fGupSQW8xWyW+X8Mqx0065R33dqHuu9lV5CnAsP1KgkQ/ZG6xrLcx0/LbMSRwUi61M96MgiAmGkGQKd4ytrJ4rRvgP99B5/rf3YDLCh97BeK6FVmA3u/rHzuEk0D/viFmNpJ+AUYGY4Z5JzTJbl5l4g7V+eZQeGnOyWwu/N5xcn5rTckKO1bX4w8ndZhUujXHbZJZP7m9PSfuiXF2N1ITKjcXgI4sf0HXiK4bzlJX9yOvELNJfIUaVlYPyTys7FOQ0ORCI6zqydE2Fdb4PyLLka/iWQOO28woWR9swRfW4148svYsuu9WGwSWTiBxFwJKbm765CHyAWv/RLkcLEbEDFs9t0i+eO7lt32Z++Vl5861cloB/KAgrXrVcnLEK9mI4MKCHpz9JgRM6sTi5dcsG57chsa3F532xxBZXW+gKB/PHrhU1D3y5ee0UqUhF8Z6tjplqxHBliNjcpyjnRR8rlxp4I4NX03cAcVkwejxVCAXcpOPR7Xh0/fN4Ga1DrNG1Kh/9r/PCFCMJahM9k4qoP7hsTkflRg0O6PGCvU7jaKc5aaqyCzQnoGbTC+SgxZreWqViTIXm5xgAx2wFoELz6jldAeyomtfGMeSVxPjJiHljHIZI32gDEZlG+eigLKtCZSdJxu2DU/nxIubTeA9g/vTB7oZQefK148eSYkefI6Ta1jNeE0ubyglABZNiozvGXaLJiSn50n2LWf3VqDM/vD3hGdDcMnSifttg+EEqv+aaL5C3H+b8SMPAocpxcKAvOVpXAAZuPK23l11IijBwA76aoCMol+y1xxPn2TIqBwvPZtKVSUyfSSaWH8pP9Yv/74O4Khu8aEYnFkTu+0NYTF38eYfHybWmRa7TEg8oKk0SCTaS6COEQbiT40JSlSx+omyLOL4ZwIj+quWyC2efmla85Q/fjOiBm1Lo1tCuY4Imn4fL6ARBkrD5xeDxJErG4VDGpy76aRR+bRved/fMlxz4iZ3h/t/3HwIFHmekHGeuwpIGbkSYdB45LraOWsj3F/Uc2QcvXdw2Ch9nfs0qQd1oLIrLa9jEiMmqcZjbQq6xdC5X8Rc8yhjZ7MHNWx2wisxTCznPpVqSOHVyAAD/dpgML9bDER2lNJ7gfliKSM8sUTBaqsPTcRef/TGa2EYs0UJqN3LXezLndc8Zy2lwCxWTw12NQSnB6qIMqvR5KEDer1Coh5fLQh8P88HBFUVkBMmvzq/T8XzduNgmUcXNVPHwyLEUPjyUiABU8PDbzwsT0JkFUfQxIg+u8+Q5SPu12O5oDh/BRE+mPbKt+q37z/5yJhrCstKC8a+0BMuSkCF61hLcys+5RQHHTvc/DN5zuA3f/YfqAmPj32RmvKkoWZH1Rk4iheH0b/2/nzjDbThGs7S7gjiivWLSW7AT7XFlVtsUJ9rQR8wXOsTUSqUCaNhQj9dLFIyaewP78r8sy6G5L/bfJM4t0ldmnNLC/pvtF4p7qQpAj99RSXKOcHTeF7SkTNdgPuoxn2KXzZ0EECPR669w0Q6Vxdf/gdLMVRcBeMXL8AAAAASUVORK5CYII=";
},
"37dc":function dc(e,l,a){
"use strict";
(function(e,t){
var n=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.LOCALE_ZH_HANT=l.LOCALE_ZH_HANS=l.LOCALE_FR=l.LOCALE_ES=l.LOCALE_EN=l.I18n=l.Formatter=void 0,
l.compileI18nJsonStr=function(e,l){
var a=l.locale,t=l.locales,n=l.delimiters;
if(!A(e,n))return e;
x||(x=new b());
var r=[];
Object.keys(t).forEach(function(e){
e!==a&&r.push({
locale:e,
values:t[e]});

}),r.unshift({
locale:a,
values:t[a]});

try{
return JSON.stringify(function e(l,a,t){
return T(l,function(l,n){
!function(l,a,t,n){
var r=l[a];
if(k(r)){
if(A(r,n)&&(l[a]=P(r,t[0].values,n),t.length>1)){
var u=l[a+"Locales"]={};
t.forEach(function(e){
u[e.locale]=P(r,e.values,n);
});
}
}else e(r,t,n);
}(l,n,a,t);
}),l;
}(JSON.parse(e),r,n),null,2);
}catch(e){}
return e;
},l.hasI18nJson=function e(l,a){
return x||(x=new b()),T(l,function(l,t){
var n=l[t];
return k(n)?!!A(n,a)||void 0:e(n,a);
});
},l.initVueI18n=function(e){
var l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},a=arguments.length>2?arguments[2]:void 0,t=arguments.length>3?arguments[3]:void 0;
if("string"!=typeof e){
var n=[l,e];
e=n[0],l=n[1];
}
"string"!=typeof e&&(e=O()),"string"!=typeof a&&(a="undefined"!=typeof __uniConfig&&__uniConfig.fallbackLocale||"en");
var r=new _({
locale:e,
fallbackLocale:a,
messages:l,
watcher:t}),
_u=function u(e,l){
if("function"!=typeof getApp)_u=function u(e,l){
return r.t(e,l);
};else{
var a=!1;
_u=function u(e,l){
var t=getApp().$vm;
return t&&(t.$locale,a||(a=!0,S(t,r))),r.t(e,l);
};
}
return _u(e,l);
};
return{
i18n:r,
f:function f(e,l,a){
return r.f(e,l,a);
},
t:function t(e,l){
return _u(e,l);
},
add:function add(e,l){
var a=!(arguments.length>2&&void 0!==arguments[2])||arguments[2];
return r.add(e,l,a);
},
watch:function watch(e){
return r.watchLocale(e);
},
getLocale:function getLocale(){
return r.getLocale();
},
setLocale:function setLocale(e){
return r.setLocale(e);
}};

},l.isI18nStr=A,l.isString=void 0,l.normalizeLocale=w,l.parseI18nJson=function e(l,a,t){
return x||(x=new b()),T(l,function(l,n){
var r=l[n];
k(r)?A(r,t)&&(l[n]=P(r,a,t)):e(r,a,t);
}),l;
},l.resolveLocale=function(e){
return function(l){
return l?function(e){
for(var l=[],a=e.split("-");a.length;){l.push(a.join("-")),a.pop();}
return l;
}(l=w(l)||l).find(function(l){
return e.indexOf(l)>-1;
}):l;
};
};
var r=n(a("278c")),u=n(a("970b")),i=n(a("5bc3")),o=n(a("7037")),s=Array.isArray,c=function c(e){
return null!==e&&"object"===(0,o.default)(e);
},v=["{","}"],b=function(){
function e(){
(0,u.default)(this,e),this._caches=Object.create(null);
}
return(0,i.default)(e,[{
key:"interpolate",
value:function value(e,l){
var a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:v;
if(!l)return[e];
var t=this._caches[e];
return t||(t=d(e,a),this._caches[e]=t),p(t,l);
}}]),
e;
}();
l.Formatter=b;
var f=/^(?:\d)+/,h=/^(?:\w)+/;
function d(e,l){
for(var a=(0,r.default)(l,2),t=a[0],n=a[1],u=[],i=0,o="";i<e.length;){
var s=e[i++];
if(s===t){
o&&u.push({
type:"text",
value:o}),
o="";
var c="";
for(s=e[i++];void 0!==s&&s!==n;){c+=s,s=e[i++];}
var v=s===n,b=f.test(c)?"list":v&&h.test(c)?"named":"unknown";
u.push({
value:c,
type:b});

}else o+=s;
}
return o&&u.push({
type:"text",
value:o}),
u;
}
function p(e,l){
var a=[],t=0,n=s(l)?"list":c(l)?"named":"unknown";
if("unknown"===n)return a;
for(;t<e.length;){
var r=e[t];
switch(r.type){
case"text":
a.push(r.value);
break;

case"list":
a.push(l[parseInt(r.value,10)]);
break;

case"named":
"named"===n&&a.push(l[r.value]);}

t++;
}
return a;
}
l.LOCALE_ZH_HANS="zh-Hans",l.LOCALE_ZH_HANT="zh-Hant",l.LOCALE_EN="en",
l.LOCALE_FR="fr",l.LOCALE_ES="es";
var g=Object.prototype.hasOwnProperty,m=function m(e,l){
return g.call(e,l);
},y=new b();
function w(e,l){
if(e)return e=e.trim().replace(/_/g,"-"),l&&l[e]?e:0===(e=e.toLowerCase()).indexOf("zh")?e.indexOf("-hans")>-1?"zh-Hans":e.indexOf("-hant")>-1||function(e,l){
return!!["-tw","-hk","-mo","-cht"].find(function(l){
return-1!==e.indexOf(l);
});
}(e)?"zh-Hant":"zh-Hans":function(e,l){
return["en","fr","es"].find(function(l){
return 0===e.indexOf(l);
});
}(e)||void 0;
}
var _=function(){
function e(l){
var a=l.locale,t=l.fallbackLocale,n=l.messages,r=l.watcher,i=l.formater;
(0,u.default)(this,e),this.locale="en",this.fallbackLocale="en",this.message={},
this.messages={},this.watchers=[],t&&(this.fallbackLocale=t),this.formater=i||y,
this.messages=n||{},this.setLocale(a||"en"),r&&this.watchLocale(r);
}
return(0,i.default)(e,[{
key:"setLocale",
value:function value(e){
var l=this,a=this.locale;
this.locale=w(e,this.messages)||this.fallbackLocale,this.messages[this.locale]||(this.messages[this.locale]={}),
this.message=this.messages[this.locale],a!==this.locale&&this.watchers.forEach(function(e){
e(l.locale,a);
});
}},
{
key:"getLocale",
value:function value(){
return this.locale;
}},
{
key:"watchLocale",
value:function value(e){
var l=this,a=this.watchers.push(e)-1;
return function(){
l.watchers.splice(a,1);
};
}},
{
key:"add",
value:function value(e,l){
var a=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],t=this.messages[e];
t?a?Object.assign(t,l):Object.keys(l).forEach(function(e){
m(t,e)||(t[e]=l[e]);
}):this.messages[e]=l;
}},
{
key:"f",
value:function value(e,l,a){
return this.formater.interpolate(e,l,a).join("");
}},
{
key:"t",
value:function value(e,l,a){
var t=this.message;
return"string"==typeof l?(l=w(l,this.messages))&&(t=this.messages[l]):a=l,
m(t,e)?this.formater.interpolate(t[e],a).join(""):(console.warn("Cannot translate the value of keypath ".concat(e,". Use the value of keypath as default.")),
e);
}}]),
e;
}();
function S(e,l){
e.$watchLocale?e.$watchLocale(function(e){
l.setLocale(e);
}):e.$watch(function(){
return e.$locale;
},function(e){
l.setLocale(e);
});
}
function O(){
return void 0!==e&&e.getLocale?e.getLocale():void 0!==t&&t.getLocale?t.getLocale():"en";
}
l.I18n=_;
var x,k=function k(e){
return"string"==typeof e;
};
function A(e,l){
return e.indexOf(l[0])>-1;
}
function P(e,l,a){
return x.interpolate(e,l,a).join("");
}
function T(e,l){
if(s(e)){
for(var a=0;a<e.length;a++){if(l(e,a))return!0;}
}else if(c(e))for(var t in e){if(l(e,t))return!0;}
return!1;
}
l.isString=k;
}).call(this,a("543d").default,a("c8ba"));
},
"3ce8":function ce8(e,l,a){
"use strict";
function t(e){
return e<128?[e]:e<2048?[192+(e>>6),128+(63&e)]:[224+(e>>12),128+(e>>6&63),128+(63&e)];
}
function n(e,l){
this.typeNumber=-1,this.errorCorrectLevel=l,this.modules=null,this.moduleCount=0,
this.dataCache=null,this.rsBlocks=null,this.totalDataCount=-1,this.data=e,
this.utf8bytes=function(e){
for(var l=[],a=0;a<e.length;a++){for(var n=t(e.charCodeAt(a)),r=0;r<n.length;r++){l.push(n[r]);}}
return l;
}(e),this.make();
}
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=n,n.prototype={
constructor:n,
getModuleCount:function getModuleCount(){
return this.moduleCount;
},
make:function make(){
this.getRightType(),this.dataCache=this.createData(),this.createQrcode();
},
makeImpl:function makeImpl(e){
this.moduleCount=4*this.typeNumber+17,this.modules=new Array(this.moduleCount);
for(var l=0;l<this.moduleCount;l++){this.modules[l]=new Array(this.moduleCount);}
this.setupPositionProbePattern(0,0),this.setupPositionProbePattern(this.moduleCount-7,0),
this.setupPositionProbePattern(0,this.moduleCount-7),this.setupPositionAdjustPattern(),
this.setupTimingPattern(),this.setupTypeInfo(!0,e),this.typeNumber>=7&&this.setupTypeNumber(!0),
this.mapData(this.dataCache,e);
},
setupPositionProbePattern:function setupPositionProbePattern(e,l){
for(var a=-1;a<=7;a++){if(!(e+a<=-1||this.moduleCount<=e+a))for(var t=-1;t<=7;t++){l+t<=-1||this.moduleCount<=l+t||(this.modules[e+a][l+t]=0<=a&&a<=6&&(0==t||6==t)||0<=t&&t<=6&&(0==a||6==a)||2<=a&&a<=4&&2<=t&&t<=4);}}
},
createQrcode:function createQrcode(){
for(var e=0,l=0,a=null,t=0;t<8;t++){
this.makeImpl(t);
var n=i.getLostPoint(this);
(0==t||e>n)&&(e=n,l=t,a=this.modules);
}
this.modules=a,this.setupTypeInfo(!1,l),this.typeNumber>=7&&this.setupTypeNumber(!1);
},
setupTimingPattern:function setupTimingPattern(){
for(var e=8;e<this.moduleCount-8;e++){null==this.modules[e][6]&&(this.modules[e][6]=e%2==0,
null==this.modules[6][e]&&(this.modules[6][e]=e%2==0));}
},
setupPositionAdjustPattern:function setupPositionAdjustPattern(){
for(var e=i.getPatternPosition(this.typeNumber),l=0;l<e.length;l++){for(var a=0;a<e.length;a++){
var t=e[l],n=e[a];
if(null==this.modules[t][n])for(var r=-2;r<=2;r++){for(var u=-2;u<=2;u++){this.modules[t+r][n+u]=-2==r||2==r||-2==u||2==u||0==r&&0==u;}}
}}
},
setupTypeNumber:function setupTypeNumber(e){
for(var l=i.getBCHTypeNumber(this.typeNumber),a=0;a<18;a++){
var t=!e&&1==(l>>a&1);
this.modules[Math.floor(a/3)][a%3+this.moduleCount-8-3]=t,this.modules[a%3+this.moduleCount-8-3][Math.floor(a/3)]=t;
}
},
setupTypeInfo:function setupTypeInfo(e,l){
for(var a=r[this.errorCorrectLevel]<<3|l,t=i.getBCHTypeInfo(a),n=0;n<15;n++){
var u=!e&&1==(t>>n&1);
n<6?this.modules[n][8]=u:n<8?this.modules[n+1][8]=u:this.modules[this.moduleCount-15+n][8]=u,
u=!e&&1==(t>>n&1),n<8?this.modules[8][this.moduleCount-n-1]=u:n<9?this.modules[8][15-n-1+1]=u:this.modules[8][15-n-1]=u;
}
this.modules[this.moduleCount-8][8]=!e;
},
createData:function createData(){
var e=new b(),l=this.typeNumber>9?16:8;
e.put(4,4),e.put(this.utf8bytes.length,l);
for(var a=0,t=this.utf8bytes.length;a<t;a++){e.put(this.utf8bytes[a],8);}
for(e.length+4<=8*this.totalDataCount&&e.put(0,4);e.length%8!=0;){e.putBit(!1);}
for(;!(e.length>=8*this.totalDataCount||(e.put(n.PAD0,8),e.length>=8*this.totalDataCount));){e.put(n.PAD1,8);}
return this.createBytes(e);
},
createBytes:function createBytes(e){
for(var l=0,a=0,t=0,n=this.rsBlock.length/3,r=new Array(),u=0;u<n;u++){for(var o=this.rsBlock[3*u+0],s=this.rsBlock[3*u+1],v=this.rsBlock[3*u+2],b=0;b<o;b++){r.push([v,s]);}}
for(var f=new Array(r.length),h=new Array(r.length),d=0;d<r.length;d++){
var p=r[d][0],g=r[d][1]-p;
for(a=Math.max(a,p),t=Math.max(t,g),f[d]=new Array(p),u=0;u<f[d].length;u++){f[d][u]=255&e.buffer[u+l];}
l+=p;
var m=i.getErrorCorrectPolynomial(g),y=new c(f[d],m.getLength()-1).mod(m);
for(h[d]=new Array(m.getLength()-1),u=0;u<h[d].length;u++){
var w=u+y.getLength()-h[d].length;
h[d][u]=w>=0?y.get(w):0;
}
}
var _=new Array(this.totalDataCount),S=0;
for(u=0;u<a;u++){for(d=0;d<r.length;d++){u<f[d].length&&(_[S++]=f[d][u]);}}
for(u=0;u<t;u++){for(d=0;d<r.length;d++){u<h[d].length&&(_[S++]=h[d][u]);}}
return _;
},
mapData:function mapData(e,l){
for(var a=-1,t=this.moduleCount-1,n=7,r=0,u=this.moduleCount-1;u>0;u-=2){for(6==u&&u--;;){
for(var o=0;o<2;o++){if(null==this.modules[t][u-o]){
var s=!1;
r<e.length&&(s=1==(e[r]>>>n&1)),i.getMask(l,t,u-o)&&(s=!s),
this.modules[t][u-o]=s,-1==--n&&(r++,n=7);
}}
if((t+=a)<0||this.moduleCount<=t){
t-=a,a=-a;
break;
}
}}
}},
n.PAD0=236,n.PAD1=17;
for(var r=[1,0,3,2],u={
PATTERN000:0,
PATTERN001:1,
PATTERN010:2,
PATTERN011:3,
PATTERN100:4,
PATTERN101:5,
PATTERN110:6,
PATTERN111:7},
i={
PATTERN_POSITION_TABLE:[[],[6,18],[6,22],[6,26],[6,30],[6,34],[6,22,38],[6,24,42],[6,26,46],[6,28,50],[6,30,54],[6,32,58],[6,34,62],[6,26,46,66],[6,26,48,70],[6,26,50,74],[6,30,54,78],[6,30,56,82],[6,30,58,86],[6,34,62,90],[6,28,50,72,94],[6,26,50,74,98],[6,30,54,78,102],[6,28,54,80,106],[6,32,58,84,110],[6,30,58,86,114],[6,34,62,90,118],[6,26,50,74,98,122],[6,30,54,78,102,126],[6,26,52,78,104,130],[6,30,56,82,108,134],[6,34,60,86,112,138],[6,30,58,86,114,142],[6,34,62,90,118,146],[6,30,54,78,102,126,150],[6,24,50,76,102,128,154],[6,28,54,80,106,132,158],[6,32,58,84,110,136,162],[6,26,54,82,110,138,166],[6,30,58,86,114,142,170]],
G15:1335,
G18:7973,
G15_MASK:21522,
getBCHTypeInfo:function getBCHTypeInfo(e){
for(var l=e<<10;i.getBCHDigit(l)-i.getBCHDigit(i.G15)>=0;){l^=i.G15<<i.getBCHDigit(l)-i.getBCHDigit(i.G15);}
return(e<<10|l)^i.G15_MASK;
},
getBCHTypeNumber:function getBCHTypeNumber(e){
for(var l=e<<12;i.getBCHDigit(l)-i.getBCHDigit(i.G18)>=0;){l^=i.G18<<i.getBCHDigit(l)-i.getBCHDigit(i.G18);}
return e<<12|l;
},
getBCHDigit:function getBCHDigit(e){
for(var l=0;0!=e;){l++,e>>>=1;}
return l;
},
getPatternPosition:function getPatternPosition(e){
return i.PATTERN_POSITION_TABLE[e-1];
},
getMask:function getMask(e,l,a){
switch(e){
case u.PATTERN000:
return(l+a)%2==0;

case u.PATTERN001:
return l%2==0;

case u.PATTERN010:
return a%3==0;

case u.PATTERN011:
return(l+a)%3==0;

case u.PATTERN100:
return(Math.floor(l/2)+Math.floor(a/3))%2==0;

case u.PATTERN101:
return l*a%2+l*a%3==0;

case u.PATTERN110:
return(l*a%2+l*a%3)%2==0;

case u.PATTERN111:
return(l*a%3+(l+a)%2)%2==0;

default:
throw new Error("bad maskPattern:"+e);}

},
getErrorCorrectPolynomial:function getErrorCorrectPolynomial(e){
for(var l=new c([1],0),a=0;a<e;a++){l=l.multiply(new c([1,o.gexp(a)],0));}
return l;
},
getLostPoint:function getLostPoint(e){
for(var l=e.getModuleCount(),a=0,t=0,n=0;n<l;n++){for(var r=0,u=e.modules[n][0],i=0;i<l;i++){
var o=e.modules[n][i];
if(i<l-6&&o&&!e.modules[n][i+1]&&e.modules[n][i+2]&&e.modules[n][i+3]&&e.modules[n][i+4]&&!e.modules[n][i+5]&&e.modules[n][i+6]&&(i<l-10?e.modules[n][i+7]&&e.modules[n][i+8]&&e.modules[n][i+9]&&e.modules[n][i+10]&&(a+=40):i>3&&e.modules[n][i-1]&&e.modules[n][i-2]&&e.modules[n][i-3]&&e.modules[n][i-4]&&(a+=40)),
n<l-1&&i<l-1){
var s=0;
o&&s++,e.modules[n+1][i]&&s++,e.modules[n][i+1]&&s++,e.modules[n+1][i+1]&&s++,
0!=s&&4!=s||(a+=3);
}
u^o?r++:(u=o,r>=5&&(a+=3+r-5),r=1),o&&t++;
}}
for(i=0;i<l;i++){for(r=0,u=e.modules[0][i],n=0;n<l;n++){o=e.modules[n][i],
n<l-6&&o&&!e.modules[n+1][i]&&e.modules[n+2][i]&&e.modules[n+3][i]&&e.modules[n+4][i]&&!e.modules[n+5][i]&&e.modules[n+6][i]&&(n<l-10?e.modules[n+7][i]&&e.modules[n+8][i]&&e.modules[n+9][i]&&e.modules[n+10][i]&&(a+=40):n>3&&e.modules[n-1][i]&&e.modules[n-2][i]&&e.modules[n-3][i]&&e.modules[n-4][i]&&(a+=40)),
u^o?r++:(u=o,r>=5&&(a+=3+r-5),r=1);}}
return a+=Math.abs(100*t/l/l-50)/5*10;
}},
o={
glog:function glog(e){
if(e<1)throw new Error("glog("+e+")");
return o.LOG_TABLE[e];
},
gexp:function gexp(e){
for(;e<0;){e+=255;}
for(;e>=256;){e-=255;}
return o.EXP_TABLE[e];
},
EXP_TABLE:new Array(256),
LOG_TABLE:new Array(256)},
s=0;s<8;s++){o.EXP_TABLE[s]=1<<s;}
for(s=8;s<256;s++){o.EXP_TABLE[s]=o.EXP_TABLE[s-4]^o.EXP_TABLE[s-5]^o.EXP_TABLE[s-6]^o.EXP_TABLE[s-8];}
for(s=0;s<255;s++){o.LOG_TABLE[o.EXP_TABLE[s]]=s;}
function c(e,l){
if(null==e.length)throw new Error(e.length+"/"+l);
for(var a=0;a<e.length&&0==e[a];){a++;}
this.num=new Array(e.length-a+l);
for(var t=0;t<e.length-a;t++){this.num[t]=e[t+a];}
}
c.prototype={
get:function get(e){
return this.num[e];
},
getLength:function getLength(){
return this.num.length;
},
multiply:function multiply(e){
for(var l=new Array(this.getLength()+e.getLength()-1),a=0;a<this.getLength();a++){for(var t=0;t<e.getLength();t++){l[a+t]^=o.gexp(o.glog(this.get(a))+o.glog(e.get(t)));}}
return new c(l,0);
},
mod:function mod(e){
var l=this.getLength(),a=e.getLength();
if(l-a<0)return this;
for(var t=new Array(l),n=0;n<l;n++){t[n]=this.get(n);}
for(;t.length>=a;){
var r=o.glog(t[0])-o.glog(e.get(0));
for(n=0;n<e.getLength();n++){t[n]^=o.gexp(o.glog(e.get(n))+r);}
for(;0==t[0];){t.shift();}
}
return new c(t,0);
}};

var v=[[1,26,19],[1,26,16],[1,26,13],[1,26,9],[1,44,34],[1,44,28],[1,44,22],[1,44,16],[1,70,55],[1,70,44],[2,35,17],[2,35,13],[1,100,80],[2,50,32],[2,50,24],[4,25,9],[1,134,108],[2,67,43],[2,33,15,2,34,16],[2,33,11,2,34,12],[2,86,68],[4,43,27],[4,43,19],[4,43,15],[2,98,78],[4,49,31],[2,32,14,4,33,15],[4,39,13,1,40,14],[2,121,97],[2,60,38,2,61,39],[4,40,18,2,41,19],[4,40,14,2,41,15],[2,146,116],[3,58,36,2,59,37],[4,36,16,4,37,17],[4,36,12,4,37,13],[2,86,68,2,87,69],[4,69,43,1,70,44],[6,43,19,2,44,20],[6,43,15,2,44,16],[4,101,81],[1,80,50,4,81,51],[4,50,22,4,51,23],[3,36,12,8,37,13],[2,116,92,2,117,93],[6,58,36,2,59,37],[4,46,20,6,47,21],[7,42,14,4,43,15],[4,133,107],[8,59,37,1,60,38],[8,44,20,4,45,21],[12,33,11,4,34,12],[3,145,115,1,146,116],[4,64,40,5,65,41],[11,36,16,5,37,17],[11,36,12,5,37,13],[5,109,87,1,110,88],[5,65,41,5,66,42],[5,54,24,7,55,25],[11,36,12],[5,122,98,1,123,99],[7,73,45,3,74,46],[15,43,19,2,44,20],[3,45,15,13,46,16],[1,135,107,5,136,108],[10,74,46,1,75,47],[1,50,22,15,51,23],[2,42,14,17,43,15],[5,150,120,1,151,121],[9,69,43,4,70,44],[17,50,22,1,51,23],[2,42,14,19,43,15],[3,141,113,4,142,114],[3,70,44,11,71,45],[17,47,21,4,48,22],[9,39,13,16,40,14],[3,135,107,5,136,108],[3,67,41,13,68,42],[15,54,24,5,55,25],[15,43,15,10,44,16],[4,144,116,4,145,117],[17,68,42],[17,50,22,6,51,23],[19,46,16,6,47,17],[2,139,111,7,140,112],[17,74,46],[7,54,24,16,55,25],[34,37,13],[4,151,121,5,152,122],[4,75,47,14,76,48],[11,54,24,14,55,25],[16,45,15,14,46,16],[6,147,117,4,148,118],[6,73,45,14,74,46],[11,54,24,16,55,25],[30,46,16,2,47,17],[8,132,106,4,133,107],[8,75,47,13,76,48],[7,54,24,22,55,25],[22,45,15,13,46,16],[10,142,114,2,143,115],[19,74,46,4,75,47],[28,50,22,6,51,23],[33,46,16,4,47,17],[8,152,122,4,153,123],[22,73,45,3,74,46],[8,53,23,26,54,24],[12,45,15,28,46,16],[3,147,117,10,148,118],[3,73,45,23,74,46],[4,54,24,31,55,25],[11,45,15,31,46,16],[7,146,116,7,147,117],[21,73,45,7,74,46],[1,53,23,37,54,24],[19,45,15,26,46,16],[5,145,115,10,146,116],[19,75,47,10,76,48],[15,54,24,25,55,25],[23,45,15,25,46,16],[13,145,115,3,146,116],[2,74,46,29,75,47],[42,54,24,1,55,25],[23,45,15,28,46,16],[17,145,115],[10,74,46,23,75,47],[10,54,24,35,55,25],[19,45,15,35,46,16],[17,145,115,1,146,116],[14,74,46,21,75,47],[29,54,24,19,55,25],[11,45,15,46,46,16],[13,145,115,6,146,116],[14,74,46,23,75,47],[44,54,24,7,55,25],[59,46,16,1,47,17],[12,151,121,7,152,122],[12,75,47,26,76,48],[39,54,24,14,55,25],[22,45,15,41,46,16],[6,151,121,14,152,122],[6,75,47,34,76,48],[46,54,24,10,55,25],[2,45,15,64,46,16],[17,152,122,4,153,123],[29,74,46,14,75,47],[49,54,24,10,55,25],[24,45,15,46,46,16],[4,152,122,18,153,123],[13,74,46,32,75,47],[48,54,24,14,55,25],[42,45,15,32,46,16],[20,147,117,4,148,118],[40,75,47,7,76,48],[43,54,24,22,55,25],[10,45,15,67,46,16],[19,148,118,6,149,119],[18,75,47,31,76,48],[34,54,24,34,55,25],[20,45,15,61,46,16]];
function b(){
this.buffer=new Array(),this.length=0;
}
n.prototype.getRightType=function(){
for(var e=1;e<41;e++){
var l=v[4*(e-1)+this.errorCorrectLevel];
if(null==l)throw new Error("bad rs block @ typeNumber:"+e+"/errorCorrectLevel:"+this.errorCorrectLevel);
for(var a=l.length/3,t=0,n=0;n<a;n++){
var r=l[3*n+0];
t+=l[3*n+2]*r;
}
var u=e>9?2:1;
if(this.utf8bytes.length+u<t||40==e){
this.typeNumber=e,this.rsBlock=l,this.totalDataCount=t;
break;
}
}
},b.prototype={
get:function get(e){
var l=Math.floor(e/8);
return this.buffer[l]>>>7-e%8&1;
},
put:function put(e,l){
for(var a=0;a<l;a++){this.putBit(e>>>l-a-1&1);}
},
putBit:function putBit(e){
var l=Math.floor(this.length/8);
this.buffer.length<=l&&this.buffer.push(0),e&&(this.buffer[l]|=128>>>this.length%8),
this.length++;
}};

},
"3cf8":function cf8(e,l,a){
"use strict";
function t(e,l){
var a="undefined"!=typeof Symbol&&e[Symbol.iterator]||e["@@iterator"];
if(!a){
if(Array.isArray(e)||(a=function(e,l){
if(e){
if("string"==typeof e)return n(e,l);
var a=Object.prototype.toString.call(e).slice(8,-1);
return"Object"===a&&e.constructor&&(a=e.constructor.name),"Map"===a||"Set"===a?Array.from(e):"Arguments"===a||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a)?n(e,l):void 0;
}
}(e))||l&&e&&"number"==typeof e.length){
a&&(e=a);
var t=0,r=function r(){};
return{
s:r,
n:function n(){
return t>=e.length?{
done:!0}:
{
done:!1,
value:e[t++]};

},
e:function(_e2){function e(_x){return _e2.apply(this,arguments);}e.toString=function(){return _e2.toString();};return e;}(function(e){
throw e;
}),
f:r};

}
throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
var u,i=!0,o=!1;
return{
s:function s(){
a=a.call(e);
},
n:function n(){
var e=a.next();
return i=e.done,e;
},
e:function(_e3){function e(_x2){return _e3.apply(this,arguments);}e.toString=function(){return _e3.toString();};return e;}(function(e){
o=!0,u=e;
}),
f:function f(){
try{
i||null==a.return||a.return();
}finally{
if(o)throw u;
}
}};

}
function n(e,l){
(null==l||l>e.length)&&(l=e.length);
for(var a=0,t=new Array(l);a<l;a++){t[a]=e[a];}
return t;
}
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,l.default=function(e){
var l,a={},n=t(e.keys());
try{
for(n.s();!(l=n.n()).done;){
var r=l.value,u=r.split("/");
u.shift(),a[u.join(".").replace(/\.js$/g,"")]=e(r);
}
}catch(e){
n.e(e);
}finally{
n.f();
}
return a;
};
},
"3d2e":function d2e(e,l){
e.exports={
solution:{
info:["GET","/my-solution"]},

clockIn:{
get_record:["GET","/clock-in-record"]},

setting:{
shopBanner:{
index:["GET","/setting/shop-banner"]}},


previewOrder:{
check_address:["POST","/check-address"]}};


},
"3e28":function e28(e,l){
e.exports={
index:["GET","/addresses"],
update:["PUT","/addresses/{uuid}"],
store:["POST","/addresses"],
destory:["DELETE","/addresses/{uuid}"]};

},
"448a":function a(e,l,_a3){
var t=_a3("2236"),n=_a3("11b0"),r=_a3("6613"),u=_a3("0676");
e.exports=function(e){
return t(e)||n(e)||r(e)||u();
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"452d":function d(e,l,a){
"use strict";
(function(e,a){
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var t={
filters:{
priceToFixed:function priceToFixed(e){
return e?(e/100).toFixed(2):"0.00";
},
productAttrsToString:function productAttrsToString(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:[],l=[];
return e.forEach(function(e){
l.push(e.value||e.v);
}),l.join("/");
},
bigNumberDisplay:function bigNumberDisplay(e){
return e>999999?(e/1e7).toFixed(1)+"kw":e>9999?(e/1e4).toFixed(1)+"w":e>999?(e/1e3).toFixed(1)+"k":e;
}},

computed:{
isBgmPlay:function isBgmPlay(){
return this.$store.getters.isBgmPlay;
},
scoreAlias:function scoreAlias(){
return this.$store.getters.setting.score&&this.$store.getters.setting.score.alias||"积分";
},
miniappSubscribeIds:function miniappSubscribeIds(){
return this.$store.getters.setting.miniapp_subscribe_ids;
},
isMiniappAndUseMiniappKf:function isMiniappAndUseMiniappKf(){
return"wx_custom_service"!=(this.$store.getters.setting.custom_service||{}).miniapp_kf_type;
}},

data:function data(){
return{
noClick:!0};

},
methods:{
copyText:function copyText(l){
e.setClipboardData({
data:l,
success:function success(l){
e.showToast({
title:"复制成功",
icon:"none"});

}});

},
disableMultiClick:function disableMultiClick(e){
var l=this;
this.noClick?(this.noClick=!1,e(),setTimeout(function(){
l.noClick=!0;
},1e3)):console.log("屏蔽连击");
},
getStorage:function getStorage(l){
try{
return e.getStorageSync(l);
}catch(e){
return null;
}
},
setStorage:function setStorage(l,a){
try{
e.setStorageSync(l,a);
}catch(e){
throw new Error("setStorage Error");
}
},
openContact:function openContact(){
var e=this.$store.getters.setting.custom_service||{};
if("wx_custom_service"!==e.miniapp_kf_type)return!1;
var l=e.miniapp_wx_corpid,t=e.miniapp_wx_kf_link;
a.openCustomerServiceChat({
extInfo:{
url:t},

corpId:l,
success:function success(e){}});

},
toLoginPage:function toLoginPage(){
e.navigateTo({
url:"/pages/login/index"});

},
toLink:function toLink(l){
"box"===l.type?e.navigateTo({
url:"/pages/boxDetail/index?uuid="+l.box}):
"path"===l.type||"xpath"===l.type?(e.navigateTo({
url:l.path}),
e.switchTab({
url:l.path})):
"url"===l.type?e.navigateTo({
url:"/pages/webview/index?url="+l.url}):
"product_list"===l.type?e.navigateTo({
url:"/pages/search/index?category_id="+(l.category_id||"")}):
"category_list"===l.type?e.navigateTo({
url:"/pages/category/index?category_id="+l.category_id}):
"live"===l.type?e.navigateTo({
url:"plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=".concat(l.live_room_id)}):
"ipage"===l.type?e.navigateTo({
url:"/pages/page/index?uuid="+l.page_uuid}):
"coupon"===l.type?e.navigateTo({
url:"/pages/couponDetail/index?uuid="+l.coupon_uuid}):
"contact"===l.type&&this.openContact();
}}};


l.default=t;
}).call(this,a("543d").default,a("bc2e").default);
},
"475c":function c(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var n=t(a("9523")),r=t(a("66fd")),u=t(a("2684"));
function i(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function o(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?i(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):i(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
t(a("a8b1"));
var s={
state:{
baseUrl:u.default.BASE_URL,
deviceInfo:u.default.DEVICE_INFO,
scene_id:"",
isBgmPlay:!1,
setting:{
activity_home:{},
box_home:{},
shop_home:{},
topic_home:{},
login_page:{},
rule_page:{},
share_rule_page:{},
vip_page:{},
open_box:{},
box_room:{},
score:{},
miniapp_subscribe_ids:{},
user_center:{},
share:{},
market:{},
custom_service:{},
coupon_popup:{}}},


mutations:{
SET_IS_BGM_PLAY:function SET_IS_BGM_PLAY(e,l){
e.isBgmPlay=l;
},
SET_ADDRESS:function SET_ADDRESS(e,l){
e.address=l;
},
SET_SCENE_ID:function SET_SCENE_ID(e,l){
e.scene_id=l;
},
SET_SETTING:function SET_SETTING(e,l){
e.setting=o(o({},e.setting),l);
}},

actions:{
setIsBgmPlay:function setIsBgmPlay(e,l){
e.commit("SET_IS_BGM_PLAY",l);
},
setEnterScene:function setEnterScene(e,l){
e.commit("SET_SCENE_ID",l);
},
setAddress:function setAddress(e,l){
e.commit("SET_ADDRESS",l);
},
getSetting:function getSetting(e){
return setTimeout(function(){
r.default.prototype.$http("/miniapp/subscribe-ids").then(function(l){
e.commit("SET_SETTING",{
miniapp_subscribe_ids:l.data.ids});

});
},1e3),r.default.prototype.$http("/setting/base").then(function(l){
e.commit("SET_SETTING",l.data.setting);
});
}}};


l.default=s;
},
"4a4b":function a4b(e,l){
function a(l,t){
return e.exports=a=Object.setPrototypeOf?Object.setPrototypeOf.bind():function(e,l){
return e.__proto__=l,e;
},e.exports.__esModule=!0,e.exports.default=e.exports,a(l,t);
}
e.exports=a,e.exports.__esModule=!0,e.exports.default=e.exports;
},
"4ea4":function ea4(e,l){
e.exports=function(e){
return e&&e.__esModule?e:{
default:e};

},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"50c4":function c4(e,l,a){
(function(l){
e.exports={
navigateTo:function navigateTo(e){
this.setParameters(e.url),l.navigateTo(e);
},
redirectTo:function redirectTo(e){
this.setParameters(e.url),l.redirectTo(e);
},
reLaunch:function reLaunch(e){
this.setParameters(e.url),l.reLaunch(e);
},
switchTab:function switchTab(e){
l.switchTab(e);
},
navigateBack:function(e){
function l(){
return e.apply(this,arguments);
}
return l.toString=function(){
return e.toString();
},l;
}(function(){
l.switchTab(navigateBack);
}),
setParameters:function setParameters(e){
var a=e.split("?");
if(a.length<2)return!1;
var t={};
a=(a=a[1]).split("&");
for(var n=0;n<a.length;n++){
var r=a[n].split("=");
t[r[0]]=r[1];
}
try{
l.setStorageSync("pageParameters",JSON.stringify(t));
}catch(e){}
},
getParameters:function getParameters(){
try{
return JSON.parse(l.getStorageSync("pageParameters"));
}catch(e){
return null;
}
}};

}).call(this,a("543d").default);
},
"543d":function d(e,l,a){
"use strict";
(function(e,t){
var n=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.createApp=xl,l.createComponent=Dl,l.createPage=jl,l.createPlugin=Ll,
l.createSubpackageApp=Nl,l.default=void 0;
var r,u=n(a("278c")),i=n(a("9523")),o=n(a("b17c")),s=n(a("448a")),c=n(a("7037")),v=a("37dc"),b=n(a("66fd"));
function f(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function h(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?f(Object(a),!0).forEach(function(l){
(0,i.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):f(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var d="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",p=/^(?:[A-Za-z\d+/]{4})*?(?:[A-Za-z\d+/]{2}(?:==)?|[A-Za-z\d+/]{3}=?)?$/;
function g(){
var l,a=e.getStorageSync("uni_id_token")||"",t=a.split(".");
if(!a||3!==t.length)return{
uid:null,
role:[],
permission:[],
tokenExpired:0};

try{
l=JSON.parse(function(e){
return decodeURIComponent(r(e).split("").map(function(e){
return"%"+("00"+e.charCodeAt(0).toString(16)).slice(-2);
}).join(""));
}(t[1]));
}catch(e){
throw new Error("获取当前用户信息出错，详细错误信息为："+e.message);
}
return l.tokenExpired=1e3*l.exp,delete l.exp,delete l.iat,l;
}
r="function"!=typeof atob?function(e){
if(e=String(e).replace(/[\t\n\f\r ]+/g,""),!p.test(e))throw new Error("Failed to execute 'atob' on 'Window': The string to be decoded is not correctly encoded.");
var l;
e+="==".slice(2-(3&e.length));
for(var a,t,n="",r=0;r<e.length;){l=d.indexOf(e.charAt(r++))<<18|d.indexOf(e.charAt(r++))<<12|(a=d.indexOf(e.charAt(r++)))<<6|(t=d.indexOf(e.charAt(r++))),
n+=64===a?String.fromCharCode(l>>16&255):64===t?String.fromCharCode(l>>16&255,l>>8&255):String.fromCharCode(l>>16&255,l>>8&255,255&l);}
return n;
}:atob;
var m=Object.prototype.toString,y=Object.prototype.hasOwnProperty;
function w(e){
return"function"==typeof e;
}
function _(e){
return"string"==typeof e;
}
function S(e){
return"[object Object]"===m.call(e);
}
function O(e,l){
return y.call(e,l);
}
function x(){}
function k(e){
var l=Object.create(null);
return function(a){
return l[a]||(l[a]=e(a));
};
}
var A=/-(\w)/g,P=k(function(e){
return e.replace(A,function(e,l){
return l?l.toUpperCase():"";
});
});
function T(e){
var l={};
return S(e)&&Object.keys(e).sort().forEach(function(a){
l[a]=e[a];
}),Object.keys(l)?l:e;
}
var M=["invoke","success","fail","complete","returnValue"],E={},C={};
function j(e,l){
Object.keys(l).forEach(function(a){
-1!==M.indexOf(a)&&w(l[a])&&(e[a]=function(e,l){
var a=l?e?e.concat(l):Array.isArray(l)?l:[l]:e;
return a?function(e){
for(var l=[],a=0;a<e.length;a++){-1===l.indexOf(e[a])&&l.push(e[a]);}
return l;
}(a):a;
}(e[a],l[a]));
});
}
function D(e,l){
e&&l&&Object.keys(l).forEach(function(a){
-1!==M.indexOf(a)&&w(l[a])&&function(e,l){
var a=e.indexOf(l);
-1!==a&&e.splice(a,1);
}(e[a],l[a]);
});
}
function N(e){
return function(l){
return e(l)||l;
};
}
function L(e){
return!!e&&("object"===(0,c.default)(e)||"function"==typeof e)&&"function"==typeof e.then;
}
function I(e,l){
for(var a=!1,t=0;t<e.length;t++){
var n=e[t];
if(a)a=Promise.resolve(N(n));else{
var r=n(l);
if(L(r)&&(a=Promise.resolve(r)),!1===r)return{
then:function then(){}};

}
}
return a||{
then:function then(e){
return e(l);
}};

}
function Y(e){
var l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};
return["success","fail","complete"].forEach(function(a){
if(Array.isArray(e[a])){
var t=l[a];
l[a]=function(l){
I(e[a],l).then(function(e){
return w(t)&&t(e)||e;
});
};
}
}),l;
}
function R(e,l){
var a=[];
Array.isArray(E.returnValue)&&a.push.apply(a,(0,s.default)(E.returnValue));
var t=C[e];
return t&&Array.isArray(t.returnValue)&&a.push.apply(a,(0,s.default)(t.returnValue)),
a.forEach(function(e){
l=e(l)||l;
}),l;
}
function F(e){
var l=Object.create(null);
Object.keys(E).forEach(function(e){
"returnValue"!==e&&(l[e]=E[e].slice());
});
var a=C[e];
return a&&Object.keys(a).forEach(function(e){
"returnValue"!==e&&(l[e]=(l[e]||[]).concat(a[e]));
}),l;
}
function U(e,l,a){
for(var t=arguments.length,n=new Array(t>3?t-3:0),r=3;r<t;r++){n[r-3]=arguments[r];}
var u=F(e);
if(u&&Object.keys(u).length){
if(Array.isArray(u.invoke)){
var i=I(u.invoke,a);
return i.then(function(e){
return l.apply(void 0,[Y(u,e)].concat(n));
});
}
return l.apply(void 0,[Y(u,a)].concat(n));
}
return l.apply(void 0,[a].concat(n));
}
var V={
returnValue:function returnValue(e){
return L(e)?new Promise(function(l,a){
e.then(function(e){
e[0]?a(e[0]):l(e[1]);
});
}):e;
}},
H=/^\$|Window$|WindowStyle$|sendHostEvent|sendNativeEvent|restoreGlobal|requireGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale|invokePushCallback|getWindowInfo|getDeviceInfo|getAppBaseInfo|getSystemSetting|getAppAuthorizeSetting|initUTS|requireUTS|registerUTS/,G=/^create|Manager$/,J=["createBLEConnection"],W=["createBLEConnection","createPushMessage"],z=/^on|^off/;
function B(e){
return G.test(e)&&-1===J.indexOf(e);
}
function Z(e){
return H.test(e)&&-1===W.indexOf(e);
}
function X(e){
return e.then(function(e){
return[null,e];
}).catch(function(e){
return[e];
});
}
function Q(e,l){
return function(e){
return!(B(e)||Z(e)||function(e){
return z.test(e)&&"onPush"!==e;
}(e));
}(e)&&w(l)?function(){
for(var a=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},t=arguments.length,n=new Array(t>1?t-1:0),r=1;r<t;r++){n[r-1]=arguments[r];}
return w(a.success)||w(a.fail)||w(a.complete)?R(e,U.apply(void 0,[e,l,a].concat(n))):R(e,X(new Promise(function(t,r){
U.apply(void 0,[e,l,Object.assign({},a,{
success:t,
fail:r})].
concat(n));
})));
}:l;
}
Promise.prototype.finally||(Promise.prototype.finally=function(e){
var l=this.constructor;
return this.then(function(a){
return l.resolve(e()).then(function(){
return a;
});
},function(a){
return l.resolve(e()).then(function(){
throw a;
});
});
});
var K,q=!1,$=0,ee=0,le={};
K=ne(e.getSystemInfoSync().language)||"en",function(){
if("undefined"!=typeof __uniConfig&&__uniConfig.locales&&Object.keys(__uniConfig.locales).length){
var e=Object.keys(__uniConfig.locales);
e.length&&e.forEach(function(e){
var l=le[e],a=__uniConfig.locales[e];
l?Object.assign(l,a):le[e]=a;
});
}
}();
var ae=(0,v.initVueI18n)(K,{}),te=ae.t;
function ne(e,l){
if(e)return e=e.trim().replace(/_/g,"-"),l&&l[e]?e:"chinese"===(e=e.toLowerCase())?"zh-Hans":0===e.indexOf("zh")?e.indexOf("-hans")>-1?"zh-Hans":e.indexOf("-hant")>-1||function(e,l){
return!!["-tw","-hk","-mo","-cht"].find(function(l){
return-1!==e.indexOf(l);
});
}(e)?"zh-Hant":"zh-Hans":function(e,l){
return["en","fr","es"].find(function(l){
return 0===e.indexOf(l);
});
}(e)||void 0;
}
function re(){
if(w(getApp)){
var l=getApp({
allowDefault:!0});

if(l&&l.$vm)return l.$vm.$locale;
}
return ne(e.getSystemInfoSync().language)||"en";
}
ae.mixin={
beforeCreate:function beforeCreate(){
var e=this,l=ae.i18n.watchLocale(function(){
e.$forceUpdate();
});
this.$once("hook:beforeDestroy",function(){
l();
});
},
methods:{
$$t:function $$t(e,l){
return te(e,l);
}}},

ae.setLocale,ae.getLocale;
var ue=[];
void 0!==t&&(t.getLocale=re);
var ie,oe={
promiseInterceptor:V},
se=Object.freeze({
__proto__:null,
upx2px:function upx2px(l,a){
if(0===$&&function(){
var l=e.getSystemInfoSync(),a=l.platform,t=l.pixelRatio,n=l.windowWidth;
$=n,ee=t,q="ios"===a;
}(),0===(l=Number(l)))return 0;
var t=l/750*(a||$);
return t<0&&(t=-t),0===(t=Math.floor(t+1e-4))&&(t=1!==ee&&q?.5:1),
l<0?-t:t;
},
getLocale:re,
setLocale:function setLocale(e){
var l=!!w(getApp)&&getApp();
return!!l&&l.$vm.$locale!==e&&(l.$vm.$locale=e,ue.forEach(function(l){
return l({
locale:e});

}),!0);
},
onLocaleChange:function onLocaleChange(e){
-1===ue.indexOf(e)&&ue.push(e);
},
addInterceptor:function addInterceptor(e,l){
"string"==typeof e&&S(l)?j(C[e]||(C[e]={}),l):S(e)&&j(E,e);
},
removeInterceptor:function removeInterceptor(e,l){
"string"==typeof e?S(l)?D(C[e],l):delete C[e]:S(e)&&D(E,e);
},
interceptors:oe});

function ce(l){
(ie=ie||e.getStorageSync("__DC_STAT_UUID"))||(ie=Date.now()+""+Math.floor(1e7*Math.random()),
e.setStorage({
key:"__DC_STAT_UUID",
data:ie})),
l.deviceId=ie;
}
function ve(e){
if(e.safeArea){
var l=e.safeArea;
e.safeAreaInsets={
top:l.top,
left:l.left,
right:e.windowWidth-l.right,
bottom:e.screenHeight-l.bottom};

}
}
function be(e,l){
for(var a=e.deviceType||"phone",t={
ipad:"pad",
windows:"pc",
mac:"pc"},
n=Object.keys(t),r=l.toLocaleLowerCase(),u=0;u<n.length;u++){
var i=n[u];
if(-1!==r.indexOf(i)){
a=t[i];
break;
}
}
return a;
}
function fe(e){
var l=e;
return l&&(l=e.toLocaleLowerCase()),l;
}
function he(e){
return re?re():e;
}
function de(e){
var l=e.hostName||"WeChat";
return e.environment?l=e.environment:e.host&&e.host.env&&(l=e.host.env),
l;
}
var pe={
returnValue:function returnValue(e){
ce(e),ve(e),function(e){
var l,a=e.brand,t=void 0===a?"":a,n=e.model,r=void 0===n?"":n,u=e.system,i=void 0===u?"":u,o=e.language,s=void 0===o?"":o,c=e.theme,v=e.version,b=(e.platform,
e.fontSizeSetting),f=e.SDKVersion,h=e.pixelRatio,d=e.deviceOrientation,p="";
p=i.split(" ")[0]||"",l=i.split(" ")[1]||"";
var g=v,m=be(e,r),y=fe(t),w=de(e),_=d,S=h,O=f,x=s.replace(/_/g,"-"),k={
appId:"__UNI__F701208",
appName:"ch",
appVersion:"3.1.94",
appVersionCode:"100",
appLanguage:he(x),
uniCompileVersion:"3.7.11",
uniRuntimeVersion:"3.7.11",
uniPlatform:"mp-weixin",
deviceBrand:y,
deviceModel:r,
deviceType:m,
devicePixelRatio:S,
deviceOrientation:_,
osName:p.toLocaleLowerCase(),
osVersion:l,
hostTheme:c,
hostVersion:g,
hostLanguage:x,
hostName:w,
hostSDKVersion:O,
hostFontSizeSetting:b,
windowTop:0,
windowBottom:0,
osLanguage:void 0,
osTheme:void 0,
ua:void 0,
hostPackageName:void 0,
browserName:void 0,
browserVersion:void 0};

Object.assign(e,k,{});
}(e);
}},
ge={
redirectTo:{
name:function name(e){
return"back"===e.exists&&e.delta?"navigateBack":"redirectTo";
},
args:function args(e){
if("back"===e.exists&&e.url){
var l=function(e){
for(var l=getCurrentPages(),a=l.length;a--;){
var t=l[a];
if(t.$page&&t.$page.fullPath===e)return a;
}
return-1;
}(e.url);
if(-1!==l){
var a=getCurrentPages().length-1-l;
a>0&&(e.delta=a);
}
}
}},

previewImage:{
args:function args(e){
var l=parseInt(e.current);
if(!isNaN(l)){
var a=e.urls;
if(Array.isArray(a)){
var t=a.length;
if(t)return l<0?l=0:l>=t&&(l=t-1),l>0?(e.current=a[l],
e.urls=a.filter(function(e,t){
return!(t<l)||e!==a[l];
})):e.current=a[0],{
indicator:!1,
loop:!1};

}
}
}},

getSystemInfo:pe,
getSystemInfoSync:pe,
showActionSheet:{
args:function args(e){
"object"===(0,c.default)(e)&&(e.alertText=e.title);
}},

getAppBaseInfo:{
returnValue:function returnValue(e){
var l=e,a=l.version,t=l.language,n=l.SDKVersion,r=l.theme,u=de(e),i=t.replace("_","-");
e=T(Object.assign(e,{
appId:"__UNI__F701208",
appName:"ch",
appVersion:"3.1.94",
appVersionCode:"100",
appLanguage:he(i),
hostVersion:a,
hostLanguage:i,
hostName:u,
hostSDKVersion:n,
hostTheme:r}));

}},

getDeviceInfo:{
returnValue:function returnValue(e){
var l=e,a=l.brand,t=l.model,n=be(e,t),r=fe(a);
ce(e),e=T(Object.assign(e,{
deviceType:n,
deviceBrand:r,
deviceModel:t}));

}},

getWindowInfo:{
returnValue:function returnValue(e){
ve(e),e=T(Object.assign(e,{
windowTop:0,
windowBottom:0}));

}},

getAppAuthorizeSetting:{
returnValue:function returnValue(e){
var l=e.locationReducedAccuracy;
e.locationAccuracy="unsupported",!0===l?e.locationAccuracy="reduced":!1===l&&(e.locationAccuracy="full");
}},

compressImage:{
args:function args(e){
e.compressedHeight&&!e.compressHeight&&(e.compressHeight=e.compressedHeight),
e.compressedWidth&&!e.compressWidth&&(e.compressWidth=e.compressedWidth);
}}},

me=["success","fail","cancel","complete"];
function ye(e,l,a){
return function(t){
return l(_e(e,t,a));
};
}
function we(e,l){
var a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{},t=arguments.length>3&&void 0!==arguments[3]?arguments[3]:{},n=arguments.length>4&&void 0!==arguments[4]&&arguments[4];
if(S(l)){
var r=!0===n?l:{};
for(var u in w(a)&&(a=a(l,r)||{}),l){if(O(a,u)){
var i=a[u];
w(i)&&(i=i(l[u],l,r)),i?_(i)?r[i]=l[u]:S(i)&&(r[i.name?i.name:u]=i.value):console.warn("The '".concat(e,"' method of platform '微信小程序' does not support option '").concat(u,"'"));
}else-1!==me.indexOf(u)?w(l[u])&&(r[u]=ye(e,l[u],t)):n||(r[u]=l[u]);}
return r;
}
return w(l)&&(l=ye(e,l,t)),l;
}
function _e(e,l,a){
var t=arguments.length>3&&void 0!==arguments[3]&&arguments[3];
return w(ge.returnValue)&&(l=ge.returnValue(e,l)),we(e,l,a,{},t);
}
function Se(l,a){
if(O(ge,l)){
var t=ge[l];
return t?function(a,n){
var r=t;
w(t)&&(r=t(a));
var u=[a=we(l,a,r.args,r.returnValue)];
void 0!==n&&u.push(n),w(r.name)?l=r.name(a):_(r.name)&&(l=r.name);
var i=e[l].apply(e,u);
return Z(l)?_e(l,i,r.returnValue,B(l)):i;
}:function(){
console.error("Platform '微信小程序' does not support '".concat(l,"'."));
};
}
return a;
}
var Oe=Object.create(null);
["onTabBarMidButtonTap","subscribePush","unsubscribePush","onPush","offPush","share"].forEach(function(e){
Oe[e]=function(e){
return function(l){
var a=l.fail,t=l.complete,n={
errMsg:"".concat(e,":fail method '").concat(e,"' not supported")};

w(a)&&a(n),w(t)&&t(n);
};
}(e);
});
var xe={
oauth:["weixin"],
share:["weixin"],
payment:["wxpay"],
push:["weixin"]},
ke=Object.freeze({
__proto__:null,
getProvider:function getProvider(e){
var l=e.service,a=e.success,t=e.fail,n=e.complete,r=!1;
xe[l]?(r={
errMsg:"getProvider:ok",
service:l,
provider:xe[l]},
w(a)&&a(r)):(r={
errMsg:"getProvider:fail service not found"},
w(t)&&t(r)),w(n)&&n(r);
}}),
Ae=function(){
var e;
return function(){
return e||(e=new b.default()),e;
};
}();
function Pe(e,l,a){
return e[l].apply(e,a);
}
var Te,Me,Ee,Ce=Object.freeze({
__proto__:null,
$on:function $on(){
return Pe(Ae(),"$on",Array.prototype.slice.call(arguments));
},
$off:function $off(){
return Pe(Ae(),"$off",Array.prototype.slice.call(arguments));
},
$once:function $once(){
return Pe(Ae(),"$once",Array.prototype.slice.call(arguments));
},
$emit:function $emit(){
return Pe(Ae(),"$emit",Array.prototype.slice.call(arguments));
}});

function je(e){
return function(){
try{
return e.apply(e,arguments);
}catch(e){
console.error(e);
}
};
}
function De(e){
try{
return JSON.parse(e);
}catch(e){}
return e;
}
var Ne=[];
function Le(e,l){
Ne.forEach(function(a){
a(e,l);
}),Ne.length=0;
}
var Ie=[],Ye=e.getAppBaseInfo&&e.getAppBaseInfo();
Ye||(Ye=e.getSystemInfoSync());
var Re=Ye?Ye.host:null,Fe=Re&&"SAAASDK"===Re.env?e.miniapp.shareVideoMessage:e.shareVideoMessage,Ue=Object.freeze({
__proto__:null,
shareVideoMessage:Fe,
getPushClientId:function getPushClientId(e){
S(e)||(e={});
var l=function(e){
var l={};
for(var a in e){
var t=e[a];
w(t)&&(l[a]=je(t),delete e[a]);
}
return l;
}(e),a=l.success,t=l.fail,n=l.complete,r=w(a),u=w(t),i=w(n);
Promise.resolve().then(function(){
void 0===Ee&&(Ee=!1,Te="",Me="uniPush is not enabled"),Ne.push(function(e,l){
var o;
e?(o={
errMsg:"getPushClientId:ok",
cid:e},
r&&a(o)):(o={
errMsg:"getPushClientId:fail"+(l?" "+l:"")},
u&&t(o)),i&&n(o);
}),void 0!==Te&&Le(Te,Me);
});
},
onPushMessage:function onPushMessage(e){
-1===Ie.indexOf(e)&&Ie.push(e);
},
offPushMessage:function offPushMessage(e){
if(e){
var l=Ie.indexOf(e);
l>-1&&Ie.splice(l,1);
}else Ie.length=0;
},
invokePushCallback:function invokePushCallback(e){
if("enabled"===e.type)Ee=!0;else if("clientId"===e.type)Te=e.cid,Me=e.errMsg,
Le(Te,e.errMsg);else if("pushMsg"===e.type)for(var l={
type:"receive",
data:De(e.message)},
a=0;a<Ie.length&&((0,Ie[a])(l),!l.stopped);a++){;}else"click"===e.type&&Ie.forEach(function(l){
l({
type:"click",
data:De(e.message)});

});
}}),
Ve=["__route__","__wxExparserNodeId__","__wxWebviewId__"];
function He(e){
return Behavior(e);
}
function Ge(){
return!!this.route;
}
function Je(e){
this.triggerEvent("__l",e);
}
function We(e){
var l=e.$scope,a={};
Object.defineProperty(e,"$refs",{
get:function get(){
var e={};
return function e(l,a,t){
(l.selectAllComponents(a)||[]).forEach(function(l){
var n=l.dataset.ref;
t[n]=l.$vm||Ze(l),"scoped"===l.dataset.vueGeneric&&l.selectAllComponents(".scoped-ref").forEach(function(l){
e(l,a,t);
});
});
}(l,".vue-ref",e),(l.selectAllComponents(".vue-ref-in-for")||[]).forEach(function(l){
var a=l.dataset.ref;
e[a]||(e[a]=[]),e[a].push(l.$vm||Ze(l));
}),function(e,l){
var a=(0,o.default)(Set,(0,s.default)(Object.keys(e)));
return Object.keys(l).forEach(function(t){
var n=e[t],r=l[t];
Array.isArray(n)&&Array.isArray(r)&&n.length===r.length&&r.every(function(e){
return n.includes(e);
})||(e[t]=r,a.delete(t));
}),a.forEach(function(l){
delete e[l];
}),e;
}(a,e);
}});

}
function ze(e){
var l,a=e.detail||e.value,t=a.vuePid,n=a.vueOptions;
t&&(l=function e(l,a){
for(var t,n=l.$children,r=n.length-1;r>=0;r--){
var u=n[r];
if(u.$scope._$vueId===a)return u;
}
for(var i=n.length-1;i>=0;i--){if(t=e(n[i],a))return t;}
}(this.$vm,t)),l||(l=this.$vm),n.parent=l;
}
function Be(e){
return Object.defineProperty(e,"__v_isMPComponent",{
configurable:!0,
enumerable:!1,
value:!0}),
e;
}
function Ze(e){
return function(e){
return null!==e&&"object"===(0,c.default)(e);
}(e)&&Object.isExtensible(e)&&Object.defineProperty(e,"__ob__",{
configurable:!0,
enumerable:!1,
value:(0,i.default)({},"__v_skip",!0)}),
e;
}
var Xe=/_(.*)_worklet_factory_/,Qe=Page,Ke=Component,qe=/:/g,$e=k(function(e){
return P(e.replace(qe,"-"));
});
function el(e){
var l=e.triggerEvent,a=function a(e){
for(var a=arguments.length,t=new Array(a>1?a-1:0),n=1;n<a;n++){t[n-1]=arguments[n];}
if(this.$vm||this.dataset&&this.dataset.comType)e=$e(e);else{
var r=$e(e);
r!==e&&l.apply(this,[r].concat(t));
}
return l.apply(this,[e].concat(t));
};
try{
e.triggerEvent=a;
}catch(l){
e._triggerEvent=a;
}
}
function ll(e,l,a){
var t=l[e];
l[e]=function(){
if(Be(this),el(this),t){
for(var e=arguments.length,l=new Array(e),a=0;a<e;a++){l[a]=arguments[a];}
return t.apply(this,l);
}
};
}
function al(e,l,a){
l.forEach(function(l){
(function e(l,a){
if(!a)return!0;
if(b.default.options&&Array.isArray(b.default.options[l]))return!0;
if(w(a=a.default||a))return!!w(a.extendOptions[l])||!!(a.super&&a.super.options&&Array.isArray(a.super.options[l]));
if(w(a[l])||Array.isArray(a[l]))return!0;
var t=a.mixins;
return Array.isArray(t)?!!t.find(function(a){
return e(l,a);
}):void 0;
})(l,a)&&(e[l]=function(e){
return this.$vm&&this.$vm.__call_hook(l,e);
});
});
}
function tl(e,l){
var a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:[];
nl(l).forEach(function(l){
return rl(e,l,a);
});
}
function nl(e){
var l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:[];
return e&&Object.keys(e).forEach(function(a){
0===a.indexOf("on")&&w(e[a])&&l.push(a);
}),l;
}
function rl(e,l,a){
-1!==a.indexOf(l)||O(e,l)||(e[l]=function(e){
return this.$vm&&this.$vm.__call_hook(l,e);
});
}
function ul(e,l){
var a;
return[a=w(l=l.default||l)?l:e.extend(l),l=a.options];
}
function il(e,l){
if(Array.isArray(l)&&l.length){
var a=Object.create(null);
l.forEach(function(e){
a[e]=!0;
}),e.$scopedSlots=e.$slots=a;
}
}
function ol(e,l){
var a=(e=(e||"").split(",")).length;
1===a?l._$vueId=e[0]:2===a&&(l._$vueId=e[0],l._$vuePid=e[1]);
}
function sl(e,l){
var a=e.data||{},t=e.methods||{};
if("function"==typeof a)try{
a=a.call(l);
}catch(e){
Object({
VUE_APP_DARK_MODE:"false",
VUE_APP_NAME:"ch",
VUE_APP_PLATFORM:"mp-weixin",
NODE_ENV:"production",
BASE_URL:"/"}).
VUE_APP_DEBUG&&console.warn("根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。",a);
}else try{
a=JSON.parse(JSON.stringify(a));
}catch(e){}
return S(a)||(a={}),Object.keys(t).forEach(function(e){
-1!==l.__lifecycle_hooks__.indexOf(e)||O(a,e)||(a[e]=t[e]);
}),a;
}
Qe.__$wrappered||(Qe.__$wrappered=!0,Page=function Page(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};
return ll("onLoad",e),Qe(e);
},Page.after=Qe.after,Component=function Component(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};
return ll("created",e),Ke(e);
});
var cl=[String,Number,Boolean,Object,Array,null];
function vl(e){
return function(l,a){
this.$vm&&(this.$vm[e]=l);
};
}
function bl(e,l){
var a=e.behaviors,t=e.extends,n=e.mixins,r=e.props;
r||(e.props=r=[]);
var u=[];
return Array.isArray(a)&&a.forEach(function(e){
u.push(e.replace("uni://","wx".concat("://"))),"uni://form-field"===e&&(Array.isArray(r)?(r.push("name"),
r.push("value")):(r.name={
type:String,
default:""},
r.value={
type:[String,Number,Boolean,Array,Object,Date],
default:""}));

}),S(t)&&t.props&&u.push(l({
properties:hl(t.props,!0)})),
Array.isArray(n)&&n.forEach(function(e){
S(e)&&e.props&&u.push(l({
properties:hl(e.props,!0)}));

}),u;
}
function fl(e,l,a,t){
return Array.isArray(l)&&1===l.length?l[0]:l;
}
function hl(e){
var l=arguments.length>1&&void 0!==arguments[1]&&arguments[1],a=arguments.length>3?arguments[3]:void 0,t={};
return l||(t.vueId={
type:String,
value:""},
a.virtualHost&&(t.virtualHostStyle={
type:null,
value:""},
t.virtualHostClass={
type:null,
value:""}),
t.scopedSlotsCompiler={
type:String,
value:""},
t.vueSlots={
type:null,
value:[],
observer:function observer(e,l){
var a=Object.create(null);
e.forEach(function(e){
a[e]=!0;
}),this.setData({
$slots:a});

}}),
Array.isArray(e)?e.forEach(function(e){
t[e]={
type:null,
observer:vl(e)};

}):S(e)&&Object.keys(e).forEach(function(l){
var a=e[l];
if(S(a)){
var n=a.default;
w(n)&&(n=n()),a.type=fl(0,a.type),t[l]={
type:-1!==cl.indexOf(a.type)?a.type:null,
value:n,
observer:vl(l)};

}else{
var r=fl(0,a);
t[l]={
type:-1!==cl.indexOf(r)?r:null,
observer:vl(l)};

}
}),t;
}
function dl(e,l,a,t){
var n={};
return Array.isArray(l)&&l.length&&l.forEach(function(l,r){
"string"==typeof l?l?"$event"===l?n["$"+r]=a:"arguments"===l?n["$"+r]=a.detail&&a.detail.__args__||t:0===l.indexOf("$event.")?n["$"+r]=e.__get_value(l.replace("$event.",""),a):n["$"+r]=e.__get_value(l):n["$"+r]=e:n["$"+r]=function(e,l){
var a=e;
return l.forEach(function(l){
var t=l[0],n=l[2];
if(t||void 0!==n){
var r,u=l[1],i=l[3];
Number.isInteger(t)?r=t:t?"string"==typeof t&&t&&(r=0===t.indexOf("#s#")?t.substr(3):e.__get_value(t,a)):r=a,
Number.isInteger(r)?a=n:u?Array.isArray(r)?a=r.find(function(l){
return e.__get_value(u,l)===n;
}):S(r)?a=Object.keys(r).find(function(l){
return e.__get_value(u,r[l])===n;
}):console.error("v-for 暂不支持循环数据：",r):a=r[n],i&&(a=e.__get_value(i,a));
}
}),a;
}(e,l);
}),n;
}
function pl(e){
for(var l={},a=1;a<e.length;a++){
var t=e[a];
l[t[0]]=t[1];
}
return l;
}
function gl(e,l){
var a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:[],t=arguments.length>3&&void 0!==arguments[3]?arguments[3]:[],n=arguments.length>4?arguments[4]:void 0,r=arguments.length>5?arguments[5]:void 0,u=!1,i=S(l.detail)&&l.detail.__args__||[l.detail];
if(n&&(u=l.currentTarget&&l.currentTarget.dataset&&"wx"===l.currentTarget.dataset.comType,
!a.length))return u?[l]:i;
var o=dl(e,t,l,i),s=[];
return a.forEach(function(e){
"$event"===e?"__set_model"!==r||n?n&&!u?s.push(i[0]):s.push(l):s.push(l.target.value):Array.isArray(e)&&"o"===e[0]?s.push(pl(e)):"string"==typeof e&&O(o,e)?s.push(o[e]):s.push(e);
}),s;
}
function ml(e){
var l=this,a=((e=function(e){
try{
e.mp=JSON.parse(JSON.stringify(e));
}catch(e){}
return e.stopPropagation=x,e.preventDefault=x,e.target=e.target||{},O(e,"detail")||(e.detail={}),
O(e,"markerId")&&(e.detail="object"===(0,c.default)(e.detail)?e.detail:{},
e.detail.markerId=e.markerId),S(e.detail)&&(e.target=Object.assign({},e.target,e.detail)),
e;
}(e)).currentTarget||e.target).dataset;
if(!a)return console.warn("事件信息不存在");
var t=a.eventOpts||a["event-opts"];
if(!t)return console.warn("事件信息不存在");
var n=e.type,r=[];
return t.forEach(function(a){
var t=a[0],u=a[1],i="^"===t.charAt(0),o="~"===(t=i?t.slice(1):t).charAt(0);
t=o?t.slice(1):t,u&&function(e,l){
return e===l||"regionchange"===l&&("begin"===e||"end"===e);
}(n,t)&&u.forEach(function(a){
var t=a[0];
if(t){
var n=l.$vm;
if(n.$options.generic&&(n=function(e){
for(var l=e.$parent;l&&l.$parent&&(l.$options.generic||l.$parent.$options.generic||l.$scope._$vuePid);){l=l.$parent;}
return l&&l.$parent;
}(n)||n),"$emit"===t)return void n.$emit.apply(n,gl(l.$vm,e,a[1],a[2],i,t));
var u=n[t];
if(!w(u)){
var s="page"===l.$vm.mpType?"Page":"Component",c=l.route||l.is;
throw new Error("".concat(s,' "').concat(c,'" does not have a method "').concat(t,'"'));
}
if(o){
if(u.once)return;
u.once=!0;
}
var v=gl(l.$vm,e,a[1],a[2],i,t);
v=Array.isArray(v)?v:[],/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(u.toString())&&(v=v.concat([,,,,,,,,,,e])),
r.push(u.apply(n,v));
}
});
}),"input"===n&&1===r.length&&void 0!==r[0]?r[0]:void 0;
}
var yl={},wl=[],_l=["onShow","onHide","onError","onPageNotFound","onThemeChange","onUnhandledRejection"];
function Sl(l,a){
var t=a.mocks,n=a.initRefs;
(function(){
b.default.prototype.getOpenerEventChannel=function(){
return this.$scope.getOpenerEventChannel();
};
var e=b.default.prototype.__call_hook;
b.default.prototype.__call_hook=function(l,a){
return"onLoad"===l&&a&&a.__id__&&(this.__eventChannel__=function(e){
if(e){
var l=yl[e];
return delete yl[e],l;
}
return wl.shift();
}(a.__id__),delete a.__id__),e.call(this,l,a);
};
})(),function(){
var e={},l={};
b.default.prototype.$hasScopedSlotsParams=function(a){
var t=e[a];
return t||(l[a]=this,this.$on("hook:destroyed",function(){
delete l[a];
})),t;
},b.default.prototype.$getScopedSlotsParams=function(a,t,n){
var r=e[a];
if(r){
var u=r[t]||{};
return n?u[n]:u;
}
l[a]=this,this.$on("hook:destroyed",function(){
delete l[a];
});
},b.default.prototype.$setScopedSlotsParams=function(a,t){
var n=this.$options.propsData.vueId;
if(n){
var r=n.split(",")[0];
(e[r]=e[r]||{})[a]=t,l[r]&&l[r].$forceUpdate();
}
},b.default.mixin({
destroyed:function destroyed(){
var a=this.$options.propsData,t=a&&a.vueId;
t&&(delete e[t],delete l[t]);
}});

}(),l.$options.store&&(b.default.prototype.$store=l.$options.store),function(e){
e.prototype.uniIDHasRole=function(e){
return g().role.indexOf(e)>-1;
},e.prototype.uniIDHasPermission=function(e){
var l=g().permission;
return this.uniIDHasRole("admin")||l.indexOf(e)>-1;
},e.prototype.uniIDTokenValid=function(){
return g().tokenExpired>Date.now();
};
}(b.default),b.default.prototype.mpHost="mp-weixin",b.default.mixin({
beforeCreate:function beforeCreate(){
if(this.$options.mpType){
if(this.mpType=this.$options.mpType,this.$mp=(0,i.default)({
data:{}},
this.mpType,this.$options.mpInstance),this.$scope=this.$options.mpInstance,
delete this.$options.mpType,delete this.$options.mpInstance,"page"===this.mpType&&"function"==typeof getApp){
var e=getApp();
e.$vm&&e.$vm.$i18n&&(this._i18n=e.$vm.$i18n);
}
"app"!==this.mpType&&(n(this),function(e,l){
var a=e.$mp[e.mpType];
l.forEach(function(l){
O(a,l)&&(e[l]=a[l]);
});
}(this,t));
}
}});

var r={
onLaunch:function onLaunch(a){
this.$vm||(e.canIUse&&!e.canIUse("nextTick")&&console.error("当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上"),
this.$vm=l,this.$vm.$mp={
app:this},
this.$vm.$scope=this,this.$vm.globalData=this.globalData,this.$vm._isMounted=!0,
this.$vm.__call_hook("mounted",a),this.$vm.__call_hook("onLaunch",a));
}};

r.globalData=l.$options.globalData||{};
var u=l.$options.methods;
return u&&Object.keys(u).forEach(function(e){
r[e]=u[e];
}),function(e,l,a){
var t=e.observable({
locale:a||ae.getLocale()}),
n=[];
l.$watchLocale=function(e){
n.push(e);
},Object.defineProperty(l,"$locale",{
get:function get(){
return t.locale;
},
set:function set(e){
t.locale=e,n.forEach(function(l){
return l(e);
});
}});

}(b.default,l,ne(e.getSystemInfoSync().language)||"en"),al(r,_l),tl(r,l.$options),
r;
}
function Ol(e){
return Sl(e,{
mocks:Ve,
initRefs:We});

}
function xl(e){
return App(Ol(e)),e;
}
var kl=/[!'()*]/g,Al=function Al(e){
return"%"+e.charCodeAt(0).toString(16);
},Pl=/%2C/g,Tl=function Tl(e){
return encodeURIComponent(e).replace(kl,Al).replace(Pl,",");
};
function Ml(e){
var l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:Tl,a=e?Object.keys(e).map(function(a){
var t=e[a];
if(void 0===t)return"";
if(null===t)return l(a);
if(Array.isArray(t)){
var n=[];
return t.forEach(function(e){
void 0!==e&&(null===e?n.push(l(a)):n.push(l(a)+"="+l(e)));
}),n.join("&");
}
return l(a)+"="+l(t);
}).filter(function(e){
return e.length>0;
}).join("&"):null;
return a?"?".concat(a):"";
}
function El(e,l){
return function(e){
var l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},a=l.isPage,t=l.initRelation,n=arguments.length>2?arguments[2]:void 0,r=ul(b.default,e),i=(0,
u.default)(r,2),o=i[0],s=i[1],c=h({
multipleSlots:!0,
addGlobalClass:!0},
s.options||{});
s["mp-weixin"]&&s["mp-weixin"].options&&Object.assign(c,s["mp-weixin"].options);
var v={
options:c,
data:sl(s,b.default.prototype),
behaviors:bl(s,He),
properties:hl(s.props,!1,s.__file,c),
lifetimes:{
attached:function attached(){
var e=this.properties,l={
mpType:a.call(this)?"page":"component",
mpInstance:this,
propsData:e};

ol(e.vueId,this),t.call(this,{
vuePid:this._$vuePid,
vueOptions:l}),
this.$vm=new o(l),il(this.$vm,e.vueSlots),this.$vm.$mount();
},
ready:function ready(){
this.$vm&&(this.$vm._isMounted=!0,this.$vm.__call_hook("mounted"),this.$vm.__call_hook("onReady"));
},
detached:function detached(){
this.$vm&&this.$vm.$destroy();
}},

pageLifetimes:{
show:function show(e){
this.$vm&&this.$vm.__call_hook("onPageShow",e);
},
hide:function hide(){
this.$vm&&this.$vm.__call_hook("onPageHide");
},
resize:function resize(e){
this.$vm&&this.$vm.__call_hook("onPageResize",e);
}},

methods:{
__l:ze,
__e:ml}};


return s.externalClasses&&(v.externalClasses=s.externalClasses),Array.isArray(s.wxsCallMethods)&&s.wxsCallMethods.forEach(function(e){
v.methods[e]=function(l){
return this.$vm[e](l);
};
}),n?[v,s,o]:a?v:[v,o];
}(e,{
isPage:Ge,
initRelation:Je},
l);
}
var Cl=["onShow","onHide","onUnload"];
function jl(e){
return Component(function(e){
return function(e){
var l=El(e,!0),a=(0,u.default)(l,2),t=a[0],n=a[1];
return al(t.methods,Cl,n),t.methods.onLoad=function(e){
this.options=e;
var l=Object.assign({},e);
delete l.__id__,this.$page={
fullPath:"/"+(this.route||this.is)+Ml(l)},
this.$vm.$mp.query=e,this.$vm.__call_hook("onLoad",e);
},tl(t.methods,e,["onReady"]),function(e,l){
l&&Object.keys(l).forEach(function(a){
var t=a.match(Xe);
if(t){
var n=t[1];
e[a]=l[a],e[n]=l[n];
}
});
}(t.methods,n.methods),t;
}(e);
}(e));
}
function Dl(e){
return Component(El(e));
}
function Nl(l){
var a=Ol(l),t=getApp({
allowDefault:!0});

l.$scope=t;
var n=t.globalData;
if(n&&Object.keys(a.globalData).forEach(function(e){
O(n,e)||(n[e]=a.globalData[e]);
}),Object.keys(a).forEach(function(e){
O(t,e)||(t[e]=a[e]);
}),w(a.onShow)&&e.onAppShow&&e.onAppShow(function(){
for(var e=arguments.length,a=new Array(e),t=0;t<e;t++){a[t]=arguments[t];}
l.__call_hook("onShow",a);
}),w(a.onHide)&&e.onAppHide&&e.onAppHide(function(){
for(var e=arguments.length,a=new Array(e),t=0;t<e;t++){a[t]=arguments[t];}
l.__call_hook("onHide",a);
}),w(a.onLaunch)){
var r=e.getLaunchOptionsSync&&e.getLaunchOptionsSync();
l.__call_hook("onLaunch",r);
}
return l;
}
function Ll(l){
var a=Ol(l);
if(w(a.onShow)&&e.onAppShow&&e.onAppShow(function(){
for(var e=arguments.length,a=new Array(e),t=0;t<e;t++){a[t]=arguments[t];}
l.__call_hook("onShow",a);
}),w(a.onHide)&&e.onAppHide&&e.onAppHide(function(){
for(var e=arguments.length,a=new Array(e),t=0;t<e;t++){a[t]=arguments[t];}
l.__call_hook("onHide",a);
}),w(a.onLaunch)){
var t=e.getLaunchOptionsSync&&e.getLaunchOptionsSync();
l.__call_hook("onLaunch",t);
}
return l;
}
Cl.push.apply(Cl,["onPullDownRefresh","onReachBottom","onAddToFavorites","onShareTimeline","onShareAppMessage","onPageScroll","onResize","onTabItemTap"]),
["vibrate","preloadPage","unPreloadPage","loadSubPackage"].forEach(function(e){
ge[e]=!1;
}),[].forEach(function(l){
var a=ge[l]&&ge[l].name?ge[l].name:l;
e.canIUse(a)||(ge[l]=!1);
});
var Il={};
"undefined"!=typeof Proxy?Il=new Proxy({},{
get:function get(l,a){
return O(l,a)?l[a]:se[a]?se[a]:Ue[a]?Q(a,Ue[a]):ke[a]?Q(a,ke[a]):Oe[a]?Q(a,Oe[a]):Ce[a]?Ce[a]:Q(a,Se(a,e[a]));
},
set:function set(e,l,a){
return e[l]=a,!0;
}}):(
Object.keys(se).forEach(function(e){
Il[e]=se[e];
}),Object.keys(Oe).forEach(function(e){
Il[e]=Q(e,Oe[e]);
}),Object.keys(ke).forEach(function(e){
Il[e]=Q(e,Oe[e]);
}),Object.keys(Ce).forEach(function(e){
Il[e]=Ce[e];
}),Object.keys(Ue).forEach(function(e){
Il[e]=Q(e,Ue[e]);
}),Object.keys(e).forEach(function(l){
(O(e,l)||O(ge,l))&&(Il[l]=Q(l,Se(l,e[l])));
})),e.createApp=xl,e.createPage=jl,e.createComponent=Dl,e.createSubpackageApp=Nl,
e.createPlugin=Ll;
var Yl=Il;
l.default=Yl;
}).call(this,a("bc2e").default,a("c8ba"));
},
"5a43":function a43(e,l){
e.exports=function(e,l){
(null==l||l>e.length)&&(l=e.length);
for(var a=0,t=new Array(l);a<l;a++){t[a]=e[a];}
return t;
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"5a8e":function a8e(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,l.default={
setting:function setting(e){
return e.app.setting;
},
deviceInfo:function deviceInfo(e){
return e.app.deviceInfo;
},
baseUrl:function baseUrl(e){
return e.app.baseUrl;
},
token:function token(e){
return e.user.token;
},
userInfo:function userInfo(e){
return e.user.userInfo;
},
isBgmPlay:function isBgmPlay(e){
return e.app.isBgmPlay;
},
personalSettings:function personalSettings(e){
return e.user.personalSettings;
}};

},
"5bc3":function bc3(e,l,a){
var t=a("a395");
function n(e,l){
for(var a=0;a<l.length;a++){
var n=l[a];
n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),
Object.defineProperty(e,t(n.key),n);
}
}
e.exports=function(e,l,a){
return l&&n(e.prototype,l),a&&n(e,a),Object.defineProperty(e,"prototype",{
writable:!1}),
e;
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"5cff":function cff(e,l,a){
"use strict";
var t=a("4ea4"),n=t(a("9523")),r=t(a("7037")),u=t(a("0863"));
function i(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function o(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?i(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):i(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var s=(0,t(a("3cf8")).default)(a("9eae"));
e.exports={
emit:function emit(e){
var l=arguments.length>1&&void 0!==arguments[1]&&arguments[1],a=arguments.length>2&&void 0!==arguments[2]?arguments[2]:{};
"object"===(0,r.default)(l)&&(a=l,l=!1);
for(var t=e.split("."),n=s,i=0;i<t.length;i++){n=n[t[i]];}
var c=n[0],v=n[1];
return l&&(v=v.replace("{uuid}",l)),"PUT"===c&&(a="update"===t[1]?{
type:"update",
attributes:a}:
o({
type:t[t.length-1]},
a)),(0,u.default)({
url:v,
method:c,
data:a});

}};

},
"62e4":function e4(e,l){
e.exports=function(e){
return e.webpackPolyfill||(e.deprecate=function(){},e.paths=[],e.children||(e.children=[]),
Object.defineProperty(e,"loaded",{
enumerable:!0,
get:function get(){
return e.l;
}}),
Object.defineProperty(e,"id",{
enumerable:!0,
get:function get(){
return e.i;
}}),
e.webpackPolyfill=1),e;
};
},
6613:function _(e,l,a){
var t=a("5a43");
e.exports=function(e,l){
if(e){
if("string"==typeof e)return t(e,l);
var a=Object.prototype.toString.call(e).slice(8,-1);
return"Object"===a&&e.constructor&&(a=e.constructor.name),"Map"===a||"Set"===a?Array.from(e):"Arguments"===a||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(a)?t(e,l):void 0;
}
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"66fd":function fd(e,l,a){
"use strict";
a.r(l),function(e){
var a=Object.freeze({});
function t(e){
return null==e;
}
function n(e){
return null!=e;
}
function r(e){
return!0===e;
}
function u(e){
return"string"==typeof e||"number"==typeof e||"symbol"==typeof e||"boolean"==typeof e;
}
function i(e){
return null!==e&&"object"==typeof e;
}
var o=Object.prototype.toString;
function s(e){
return"[object Object]"===o.call(e);
}
function c(e){
var l=parseFloat(String(e));
return l>=0&&Math.floor(l)===l&&isFinite(e);
}
function v(e){
return n(e)&&"function"==typeof e.then&&"function"==typeof e.catch;
}
function b(e){
return null==e?"":Array.isArray(e)||s(e)&&e.toString===o?JSON.stringify(e,null,2):String(e);
}
function f(e){
var l=parseFloat(e);
return isNaN(l)?e:l;
}
function h(e,l){
for(var a=Object.create(null),t=e.split(","),n=0;n<t.length;n++){a[t[n]]=!0;}
return l?function(e){
return a[e.toLowerCase()];
}:function(e){
return a[e];
};
}
h("slot,component",!0);
var d=h("key,ref,slot,slot-scope,is");
function p(e,l){
if(e.length){
var a=e.indexOf(l);
if(a>-1)return e.splice(a,1);
}
}
var g=Object.prototype.hasOwnProperty;
function m(e,l){
return g.call(e,l);
}
function y(e){
var l=Object.create(null);
return function(a){
return l[a]||(l[a]=e(a));
};
}
var w=/-(\w)/g,_=y(function(e){
return e.replace(w,function(e,l){
return l?l.toUpperCase():"";
});
}),S=y(function(e){
return e.charAt(0).toUpperCase()+e.slice(1);
}),O=/\B([A-Z])/g,x=y(function(e){
return e.replace(O,"-$1").toLowerCase();
}),k=Function.prototype.bind?function(e,l){
return e.bind(l);
}:function(e,l){
function a(a){
var t=arguments.length;
return t?t>1?e.apply(l,arguments):e.call(l,a):e.call(l);
}
return a._length=e.length,a;
};
function A(e,l){
l=l||0;
for(var a=e.length-l,t=new Array(a);a--;){t[a]=e[a+l];}
return t;
}
function P(e,l){
for(var a in l){e[a]=l[a];}
return e;
}
function T(e){
for(var l={},a=0;a<e.length;a++){e[a]&&P(l,e[a]);}
return l;
}
function M(e,l,a){}
var E=function E(e,l,a){
return!1;
},C=function C(e){
return e;
};
function j(e,l){
if(e===l)return!0;
var a=i(e),t=i(l);
if(!a||!t)return!a&&!t&&String(e)===String(l);
try{
var n=Array.isArray(e),r=Array.isArray(l);
if(n&&r)return e.length===l.length&&e.every(function(e,a){
return j(e,l[a]);
});
if(e instanceof Date&&l instanceof Date)return e.getTime()===l.getTime();
if(n||r)return!1;
var u=Object.keys(e),o=Object.keys(l);
return u.length===o.length&&u.every(function(a){
return j(e[a],l[a]);
});
}catch(e){
return!1;
}
}
function D(e,l){
for(var a=0;a<e.length;a++){if(j(e[a],l))return a;}
return-1;
}
function N(e){
var l=!1;
return function(){
l||(l=!0,e.apply(this,arguments));
};
}
var L=["component","directive","filter"],I=["beforeCreate","created","beforeMount","mounted","beforeUpdate","updated","beforeDestroy","destroyed","activated","deactivated","errorCaptured","serverPrefetch"],Y={
optionMergeStrategies:Object.create(null),
silent:!1,
productionTip:!1,
devtools:!1,
performance:!1,
errorHandler:null,
warnHandler:null,
ignoredElements:[],
keyCodes:Object.create(null),
isReservedTag:E,
isReservedAttr:E,
isUnknownElement:E,
getTagNamespace:M,
parsePlatformTagName:C,
mustUseProp:E,
async:!0,
_lifecycleHooks:I};

function R(e){
var l=(e+"").charCodeAt(0);
return 36===l||95===l;
}
function F(e,l,a,t){
Object.defineProperty(e,l,{
value:a,
enumerable:!!t,
writable:!0,
configurable:!0});

}
var U,V=new RegExp("[^"+/a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/.source+".$_\\d]"),H=("__proto__"in{}),G="undefined"!=typeof window,J="undefined"!=typeof WXEnvironment&&!!WXEnvironment.platform,W=J&&WXEnvironment.platform.toLowerCase(),z=G&&window.navigator.userAgent.toLowerCase(),B=z&&/msie|trident/.test(z),Z=(z&&z.indexOf("msie 9.0"),
z&&z.indexOf("edge/"),z&&z.indexOf("android"),z&&/iphone|ipad|ipod|ios/.test(z)||"ios"===W),X=(z&&/chrome\/\d+/.test(z),
z&&/phantomjs/.test(z),z&&z.match(/firefox\/(\d+)/),{}.watch);
if(G)try{
var Q={};
Object.defineProperty(Q,"passive",{
get:function get(){}}),
window.addEventListener("test-passive",null,Q);
}catch(e){}
var K=function K(){
return void 0===U&&(U=!G&&!J&&void 0!==e&&e.process&&"server"===e.process.env.VUE_ENV),
U;
},q=G&&window.__VUE_DEVTOOLS_GLOBAL_HOOK__;
function $(e){
return"function"==typeof e&&/native code/.test(e.toString());
}
var ee,le="undefined"!=typeof Symbol&&$(Symbol)&&"undefined"!=typeof Reflect&&$(Reflect.ownKeys);
ee="undefined"!=typeof Set&&$(Set)?Set:function(){
function e(){
this.set=Object.create(null);
}
return e.prototype.has=function(e){
return!0===this.set[e];
},e.prototype.add=function(e){
this.set[e]=!0;
},e.prototype.clear=function(){
this.set=Object.create(null);
},e;
}();
var ae=M,te=0,ne=function ne(){
this.id=te++,this.subs=[];
};
function re(e){
ne.SharedObject.targetStack.push(e),ne.SharedObject.target=e,ne.target=e;
}
function ue(){
ne.SharedObject.targetStack.pop(),ne.SharedObject.target=ne.SharedObject.targetStack[ne.SharedObject.targetStack.length-1],
ne.target=ne.SharedObject.target;
}
ne.prototype.addSub=function(e){
this.subs.push(e);
},ne.prototype.removeSub=function(e){
p(this.subs,e);
},ne.prototype.depend=function(){
ne.SharedObject.target&&ne.SharedObject.target.addDep(this);
},ne.prototype.notify=function(){
for(var e=this.subs.slice(),l=0,a=e.length;l<a;l++){e[l].update();}
},(ne.SharedObject={}).target=null,ne.SharedObject.targetStack=[];
var ie=function ie(e,l,a,t,n,r,u,i){
this.tag=e,this.data=l,this.children=a,this.text=t,this.elm=n,this.ns=void 0,
this.context=r,this.fnContext=void 0,this.fnOptions=void 0,this.fnScopeId=void 0,
this.key=l&&l.key,this.componentOptions=u,this.componentInstance=void 0,
this.parent=void 0,this.raw=!1,this.isStatic=!1,this.isRootInsert=!0,
this.isComment=!1,this.isCloned=!1,this.isOnce=!1,this.asyncFactory=i,
this.asyncMeta=void 0,this.isAsyncPlaceholder=!1;
},oe={
child:{
configurable:!0}};


oe.child.get=function(){
return this.componentInstance;
},Object.defineProperties(ie.prototype,oe);
var se=function se(e){
void 0===e&&(e="");
var l=new ie();
return l.text=e,l.isComment=!0,l;
};
function ce(e){
return new ie(void 0,void 0,void 0,String(e));
}
var ve=Array.prototype,be=Object.create(ve);
["push","pop","shift","unshift","splice","sort","reverse"].forEach(function(e){
var l=ve[e];
F(be,e,function(){
for(var a=[],t=arguments.length;t--;){a[t]=arguments[t];}
var n,r=l.apply(this,a),u=this.__ob__;
switch(e){
case"push":
case"unshift":
n=a;
break;

case"splice":
n=a.slice(2);}

return n&&u.observeArray(n),u.dep.notify(),r;
});
});
var fe=Object.getOwnPropertyNames(be),he=!0;
function de(e){
he=e;
}
var pe=function pe(e){
this.value=e,this.dep=new ne(),this.vmCount=0,F(e,"__ob__",this),Array.isArray(e)?(H?e.push!==e.__proto__.push?ge(e,be,fe):function(e,l){
e.__proto__=l;
}(e,be):ge(e,be,fe),this.observeArray(e)):this.walk(e);
};
function ge(e,l,a){
for(var t=0,n=a.length;t<n;t++){
var r=a[t];
F(e,r,l[r]);
}
}
function me(e,l){
var a;
if(i(e)&&!(e instanceof ie))return m(e,"__ob__")&&e.__ob__ instanceof pe?a=e.__ob__:!he||K()||!Array.isArray(e)&&!s(e)||!Object.isExtensible(e)||e._isVue||e.__v_isMPComponent||(a=new pe(e)),
l&&a&&a.vmCount++,a;
}
function ye(e,l,a,t,n){
var r=new ne(),u=Object.getOwnPropertyDescriptor(e,l);
if(!u||!1!==u.configurable){
var i=u&&u.get,o=u&&u.set;
i&&!o||2!==arguments.length||(a=e[l]);
var s=!n&&me(a);
Object.defineProperty(e,l,{
enumerable:!0,
configurable:!0,
get:function get(){
var l=i?i.call(e):a;
return ne.SharedObject.target&&(r.depend(),s&&(s.dep.depend(),Array.isArray(l)&&Se(l))),
l;
},
set:function set(l){
var t=i?i.call(e):a;
l===t||l!=l&&t!=t||i&&!o||(o?o.call(e,l):a=l,s=!n&&me(l),
r.notify());
}});

}
}
function we(e,l,a){
if(Array.isArray(e)&&c(l))return e.length=Math.max(e.length,l),e.splice(l,1,a),
a;
if(l in e&&!(l in Object.prototype))return e[l]=a,a;
var t=e.__ob__;
return e._isVue||t&&t.vmCount?a:t?(ye(t.value,l,a),t.dep.notify(),
a):(e[l]=a,a);
}
function _e(e,l){
if(Array.isArray(e)&&c(l))e.splice(l,1);else{
var a=e.__ob__;
e._isVue||a&&a.vmCount||m(e,l)&&(delete e[l],a&&a.dep.notify());
}
}
function Se(e){
for(var l=void 0,a=0,t=e.length;a<t;a++){(l=e[a])&&l.__ob__&&l.__ob__.dep.depend(),
Array.isArray(l)&&Se(l);}
}
pe.prototype.walk=function(e){
for(var l=Object.keys(e),a=0;a<l.length;a++){ye(e,l[a]);}
},pe.prototype.observeArray=function(e){
for(var l=0,a=e.length;l<a;l++){me(e[l]);}
};
var Oe=Y.optionMergeStrategies;
function xe(e,l){
if(!l)return e;
for(var a,t,n,r=le?Reflect.ownKeys(l):Object.keys(l),u=0;u<r.length;u++){"__ob__"!==(a=r[u])&&(t=e[a],
n=l[a],m(e,a)?t!==n&&s(t)&&s(n)&&xe(t,n):we(e,a,n));}
return e;
}
function ke(e,l,a){
return a?function(){
var t="function"==typeof l?l.call(a,a):l,n="function"==typeof e?e.call(a,a):e;
return t?xe(t,n):n;
}:l?e?function(){
return xe("function"==typeof l?l.call(this,this):l,"function"==typeof e?e.call(this,this):e);
}:l:e;
}
function Ae(e,l){
var a=l?e?e.concat(l):Array.isArray(l)?l:[l]:e;
return a?function(e){
for(var l=[],a=0;a<e.length;a++){-1===l.indexOf(e[a])&&l.push(e[a]);}
return l;
}(a):a;
}
function Pe(e,l,a,t){
var n=Object.create(e||null);
return l?P(n,l):n;
}
Oe.data=function(e,l,a){
return a?ke(e,l,a):l&&"function"!=typeof l?e:ke(e,l);
},I.forEach(function(e){
Oe[e]=Ae;
}),L.forEach(function(e){
Oe[e+"s"]=Pe;
}),Oe.watch=function(e,l,a,t){
if(e===X&&(e=void 0),l===X&&(l=void 0),!l)return Object.create(e||null);
if(!e)return l;
var n={};
for(var r in P(n,e),l){
var u=n[r],i=l[r];
u&&!Array.isArray(u)&&(u=[u]),n[r]=u?u.concat(i):Array.isArray(i)?i:[i];
}
return n;
},Oe.props=Oe.methods=Oe.inject=Oe.computed=function(e,l,a,t){
if(!e)return l;
var n=Object.create(null);
return P(n,e),l&&P(n,l),n;
},Oe.provide=ke;
var Te=function Te(e,l){
return void 0===l?e:l;
};
function Me(e,l,a){
if("function"==typeof l&&(l=l.options),function(e,l){
var a=e.props;
if(a){
var t,n,r={};
if(Array.isArray(a))for(t=a.length;t--;){"string"==typeof(n=a[t])&&(r[_(n)]={
type:null});}else
if(s(a))for(var u in a){n=a[u],r[_(u)]=s(n)?n:{
type:n};}

e.props=r;
}
}(l),function(e,l){
var a=e.inject;
if(a){
var t=e.inject={};
if(Array.isArray(a))for(var n=0;n<a.length;n++){t[a[n]]={
from:a[n]};}else
if(s(a))for(var r in a){
var u=a[r];
t[r]=s(u)?P({
from:r},
u):{
from:u};

}
}
}(l),function(e){
var l=e.directives;
if(l)for(var a in l){
var t=l[a];
"function"==typeof t&&(l[a]={
bind:t,
update:t});

}
}(l),!l._base&&(l.extends&&(e=Me(e,l.extends,a)),l.mixins))for(var t=0,n=l.mixins.length;t<n;t++){e=Me(e,l.mixins[t],a);}
var r,u={};
for(r in e){i(r);}
for(r in l){m(e,r)||i(r);}
function i(t){
var n=Oe[t]||Te;
u[t]=n(e[t],l[t],a,t);
}
return u;
}
function Ee(e,l,a,t){
if("string"==typeof a){
var n=e[l];
if(m(n,a))return n[a];
var r=_(a);
if(m(n,r))return n[r];
var u=S(r);
return m(n,u)?n[u]:n[a]||n[r]||n[u];
}
}
function Ce(e,l,a,t){
var n=l[e],r=!m(a,e),u=a[e],i=Ne(Boolean,n.type);
if(i>-1)if(r&&!m(n,"default"))u=!1;else if(""===u||u===x(e)){
var o=Ne(String,n.type);
(o<0||i<o)&&(u=!0);
}
if(void 0===u){
u=function(e,l,a){
if(m(l,"default")){
var t=l.default;
return e&&e.$options.propsData&&void 0===e.$options.propsData[a]&&void 0!==e._props[a]?e._props[a]:"function"==typeof t&&"Function"!==je(l.type)?t.call(e):t;
}
}(t,n,e);
var s=he;
de(!0),me(u),de(s);
}
return u;
}
function je(e){
var l=e&&e.toString().match(/^\s*function (\w+)/);
return l?l[1]:"";
}
function De(e,l){
return je(e)===je(l);
}
function Ne(e,l){
if(!Array.isArray(l))return De(l,e)?0:-1;
for(var a=0,t=l.length;a<t;a++){if(De(l[a],e))return a;}
return-1;
}
function Le(e,l,a){
re();
try{
if(l)for(var t=l;t=t.$parent;){
var n=t.$options.errorCaptured;
if(n)for(var r=0;r<n.length;r++){try{
if(!1===n[r].call(t,e,l,a))return;
}catch(e){
Ye(e,t,"errorCaptured hook");
}}
}
Ye(e,l,a);
}finally{
ue();
}
}
function Ie(e,l,a,t,n){
var r;
try{
(r=a?e.apply(l,a):e.call(l))&&!r._isVue&&v(r)&&!r._handled&&(r.catch(function(e){
return Le(e,t,n+" (Promise/async)");
}),r._handled=!0);
}catch(e){
Le(e,t,n);
}
return r;
}
function Ye(e,l,a){
if(Y.errorHandler)try{
return Y.errorHandler.call(null,e,l,a);
}catch(l){
l!==e&&Re(l);
}
Re(e);
}
function Re(e,l,a){
if(!G&&!J||"undefined"==typeof console)throw e;
console.error(e);
}
var Fe,Ue=[],Ve=!1;
function He(){
Ve=!1;
var e=Ue.slice(0);
Ue.length=0;
for(var l=0;l<e.length;l++){e[l]();}
}
if("undefined"!=typeof Promise&&$(Promise)){
var Ge=Promise.resolve();
Fe=function Fe(){
Ge.then(He),Z&&setTimeout(M);
};
}else if(B||"undefined"==typeof MutationObserver||!$(MutationObserver)&&"[object MutationObserverConstructor]"!==MutationObserver.toString())Fe="undefined"!=typeof setImmediate&&$(setImmediate)?function(){
setImmediate(He);
}:function(){
setTimeout(He,0);
};else{
var Je=1,We=new MutationObserver(He),ze=document.createTextNode(String(Je));
We.observe(ze,{
characterData:!0}),
Fe=function Fe(){
Je=(Je+1)%2,ze.data=String(Je);
};
}
function Be(e,l){
var a;
if(Ue.push(function(){
if(e)try{
e.call(l);
}catch(e){
Le(e,l,"nextTick");
}else a&&a(l);
}),Ve||(Ve=!0,Fe()),!e&&"undefined"!=typeof Promise)return new Promise(function(e){
a=e;
});
}
var Ze=new ee();
function Xe(e){
(function e(l,a){
var t,n,r=Array.isArray(l);
if(!(!r&&!i(l)||Object.isFrozen(l)||l instanceof ie)){
if(l.__ob__){
var u=l.__ob__.dep.id;
if(a.has(u))return;
a.add(u);
}
if(r)for(t=l.length;t--;){e(l[t],a);}else for(t=(n=Object.keys(l)).length;t--;){e(l[n[t]],a);}
}
})(e,Ze),Ze.clear();
}
var Qe=y(function(e){
var l="&"===e.charAt(0),a="~"===(e=l?e.slice(1):e).charAt(0),t="!"===(e=a?e.slice(1):e).charAt(0);
return{
name:e=t?e.slice(1):e,
once:a,
capture:t,
passive:l};

});
function Ke(e,l){
function a(){
var e=arguments,t=a.fns;
if(!Array.isArray(t))return Ie(t,null,arguments,l,"v-on handler");
for(var n=t.slice(),r=0;r<n.length;r++){Ie(n[r],null,e,l,"v-on handler");}
}
return a.fns=e,a;
}
function qe(e,l,a,r){
var u=l.options.mpOptions&&l.options.mpOptions.properties;
if(t(u))return a;
var i=l.options.mpOptions.externalClasses||[],o=e.attrs,s=e.props;
if(n(o)||n(s))for(var c in u){
var v=x(c);
($e(a,s,c,v,!0)||$e(a,o,c,v,!1))&&a[c]&&-1!==i.indexOf(v)&&r[_(a[c])]&&(a[c]=r[_(a[c])]);
}
return a;
}
function $e(e,l,a,t,r){
if(n(l)){
if(m(l,a))return e[a]=l[a],r||delete l[a],!0;
if(m(l,t))return e[a]=l[t],r||delete l[t],!0;
}
return!1;
}
function el(e){
return u(e)?[ce(e)]:Array.isArray(e)?function e(l,a){
var i,o,s,c,v=[];
for(i=0;i<l.length;i++){t(o=l[i])||"boolean"==typeof o||(c=v[s=v.length-1],
Array.isArray(o)?o.length>0&&(ll((o=e(o,(a||"")+"_"+i))[0])&&ll(c)&&(v[s]=ce(c.text+o[0].text),
o.shift()),v.push.apply(v,o)):u(o)?ll(c)?v[s]=ce(c.text+o):""!==o&&v.push(ce(o)):ll(o)&&ll(c)?v[s]=ce(c.text+o.text):(r(l._isVList)&&n(o.tag)&&t(o.key)&&n(a)&&(o.key="__vlist"+a+"_"+i+"__"),
v.push(o)));}
return v;
}(e):void 0;
}
function ll(e){
return n(e)&&n(e.text)&&function(e){
return!1===e;
}(e.isComment);
}
function al(e){
var l=e.$options.provide;
l&&(e._provided="function"==typeof l?l.call(e):l);
}
function tl(e){
var l=nl(e.$options.inject,e);
l&&(de(!1),Object.keys(l).forEach(function(a){
ye(e,a,l[a]);
}),de(!0));
}
function nl(e,l){
if(e){
for(var a=Object.create(null),t=le?Reflect.ownKeys(e):Object.keys(e),n=0;n<t.length;n++){
var r=t[n];
if("__ob__"!==r){
for(var u=e[r].from,i=l;i;){
if(i._provided&&m(i._provided,u)){
a[r]=i._provided[u];
break;
}
i=i.$parent;
}
if(!i&&"default"in e[r]){
var o=e[r].default;
a[r]="function"==typeof o?o.call(l):o;
}
}
}
return a;
}
}
function rl(e,l){
if(!e||!e.length)return{};
for(var a={},t=0,n=e.length;t<n;t++){
var r=e[t],u=r.data;
if(u&&u.attrs&&u.attrs.slot&&delete u.attrs.slot,r.context!==l&&r.fnContext!==l||!u||null==u.slot)r.asyncMeta&&r.asyncMeta.data&&"page"===r.asyncMeta.data.slot?(a.page||(a.page=[])).push(r):(a.default||(a.default=[])).push(r);else{
var i=u.slot,o=a[i]||(a[i]=[]);
"template"===r.tag?o.push.apply(o,r.children||[]):o.push(r);
}
}
for(var s in a){a[s].every(ul)&&delete a[s];}
return a;
}
function ul(e){
return e.isComment&&!e.asyncFactory||" "===e.text;
}
function il(e,l,t){
var n,r=Object.keys(l).length>0,u=e?!!e.$stable:!r,i=e&&e.$key;
if(e){
if(e._normalized)return e._normalized;
if(u&&t&&t!==a&&i===t.$key&&!r&&!t.$hasNormal)return t;
for(var o in n={},e){e[o]&&"$"!==o[0]&&(n[o]=ol(l,o,e[o]));}
}else n={};
for(var s in l){s in n||(n[s]=sl(l,s));}
return e&&Object.isExtensible(e)&&(e._normalized=n),F(n,"$stable",u),F(n,"$key",i),
F(n,"$hasNormal",r),n;
}
function ol(e,l,a){
var t=function t(){
var e=arguments.length?a.apply(null,arguments):a({});
return(e=e&&"object"==typeof e&&!Array.isArray(e)?[e]:el(e))&&(0===e.length||1===e.length&&e[0].isComment)?void 0:e;
};
return a.proxy&&Object.defineProperty(e,l,{
get:t,
enumerable:!0,
configurable:!0}),
t;
}
function sl(e,l){
return function(){
return e[l];
};
}
function cl(e,l){
var a,t,r,u,o;
if(Array.isArray(e)||"string"==typeof e)for(a=new Array(e.length),t=0,
r=e.length;t<r;t++){a[t]=l(e[t],t,t,t);}else if("number"==typeof e)for(a=new Array(e),
t=0;t<e;t++){a[t]=l(t+1,t,t,t);}else if(i(e))if(le&&e[Symbol.iterator]){
a=[];
for(var s=e[Symbol.iterator](),c=s.next();!c.done;){a.push(l(c.value,a.length,t,t++)),
c=s.next();}
}else for(u=Object.keys(e),a=new Array(u.length),t=0,r=u.length;t<r;t++){o=u[t],
a[t]=l(e[o],o,t,t);}
return n(a)||(a=[]),a._isVList=!0,a;
}
function vl(e,l,a,t){
var n,r=this.$scopedSlots[e];
r?(a=a||{},t&&(a=P(P({},t),a)),n=r(a,this,a._i)||l):n=this.$slots[e]||l;
var u=a&&a.slot;
return u?this.$createElement("template",{
slot:u},
n):n;
}
function bl(e){
return Ee(this.$options,"filters",e)||C;
}
function fl(e,l){
return Array.isArray(e)?-1===e.indexOf(l):e!==l;
}
function hl(e,l,a,t,n){
var r=Y.keyCodes[l]||a;
return n&&t&&!Y.keyCodes[l]?fl(n,t):r?fl(r,e):t?x(t)!==l:void 0;
}
function dl(e,l,a,t,n){
if(a&&i(a)){
var r;
Array.isArray(a)&&(a=T(a));
var u=function u(_u2){
if("class"===_u2||"style"===_u2||d(_u2))r=e;else{
var i=e.attrs&&e.attrs.type;
r=t||Y.mustUseProp(l,i,_u2)?e.domProps||(e.domProps={}):e.attrs||(e.attrs={});
}
var o=_(_u2),s=x(_u2);
o in r||s in r||(r[_u2]=a[_u2],!n)||((e.on||(e.on={}))["update:"+_u2]=function(e){
a[_u2]=e;
});
};
for(var o in a){u(o);}
}
return e;
}
function pl(e,l){
var a=this._staticTrees||(this._staticTrees=[]),t=a[e];
return t&&!l||ml(t=a[e]=this.$options.staticRenderFns[e].call(this._renderProxy,null,this),"__static__"+e,!1),
t;
}
function gl(e,l,a){
return ml(e,"__once__"+l+(a?"_"+a:""),!0),e;
}
function ml(e,l,a){
if(Array.isArray(e))for(var t=0;t<e.length;t++){e[t]&&"string"!=typeof e[t]&&yl(e[t],l+"_"+t,a);}else yl(e,l,a);
}
function yl(e,l,a){
e.isStatic=!0,e.key=l,e.isOnce=a;
}
function wl(e,l){
if(l&&s(l)){
var a=e.on=e.on?P({},e.on):{};
for(var t in l){
var n=a[t],r=l[t];
a[t]=n?[].concat(n,r):r;
}
}
return e;
}
function _l(e,l,a,t){
l=l||{
$stable:!a};

for(var n=0;n<e.length;n++){
var r=e[n];
Array.isArray(r)?_l(r,l,a):r&&(r.proxy&&(r.fn.proxy=!0),l[r.key]=r.fn);
}
return t&&(l.$key=t),l;
}
function Sl(e,l){
for(var a=0;a<l.length;a+=2){
var t=l[a];
"string"==typeof t&&t&&(e[l[a]]=l[a+1]);
}
return e;
}
function Ol(e,l){
return"string"==typeof e?l+e:e;
}
function xl(e){
e._o=gl,e._n=f,e._s=b,e._l=cl,e._t=vl,e._q=j,e._i=D,e._m=pl,
e._f=bl,e._k=hl,e._b=dl,e._v=ce,e._e=se,e._u=_l,e._g=wl,e._d=Sl,
e._p=Ol;
}
function kl(e,l,t,n,u){
var i,o=this,s=u.options;
m(n,"_uid")?(i=Object.create(n))._original=n:(i=n,n=n._original);
var c=r(s._compiled),v=!c;
this.data=e,this.props=l,this.children=t,this.parent=n,this.listeners=e.on||a,
this.injections=nl(s.inject,n),this.slots=function(){
return o.$slots||il(e.scopedSlots,o.$slots=rl(t,n)),o.$slots;
},Object.defineProperty(this,"scopedSlots",{
enumerable:!0,
get:function get(){
return il(e.scopedSlots,this.slots());
}}),
c&&(this.$options=s,this.$slots=this.slots(),this.$scopedSlots=il(e.scopedSlots,this.$slots)),
s._scopeId?this._c=function(e,l,a,t){
var r=jl(i,e,l,a,t,v);
return r&&!Array.isArray(r)&&(r.fnScopeId=s._scopeId,r.fnContext=n),r;
}:this._c=function(e,l,a,t){
return jl(i,e,l,a,t,v);
};
}
function Al(e,l,a,t,n){
var r=function(e){
var l=new ie(e.tag,e.data,e.children&&e.children.slice(),e.text,e.elm,e.context,e.componentOptions,e.asyncFactory);
return l.ns=e.ns,l.isStatic=e.isStatic,l.key=e.key,l.isComment=e.isComment,
l.fnContext=e.fnContext,l.fnOptions=e.fnOptions,l.fnScopeId=e.fnScopeId,
l.asyncMeta=e.asyncMeta,l.isCloned=!0,l;
}(e);
return r.fnContext=a,r.fnOptions=t,l.slot&&((r.data||(r.data={})).slot=l.slot),
r;
}
function Pl(e,l){
for(var a in l){e[_(a)]=l[a];}
}
xl(kl.prototype);
var Tl={
init:function init(e,l){
if(e.componentInstance&&!e.componentInstance._isDestroyed&&e.data.keepAlive){
var a=e;
Tl.prepatch(a,a);
}else(e.componentInstance=function(e,l){
var a={
_isComponent:!0,
_parentVnode:e,
parent:l},
t=e.data.inlineTemplate;
return n(t)&&(a.render=t.render,a.staticRenderFns=t.staticRenderFns),new e.componentOptions.Ctor(a);
}(e,Vl)).$mount(l?e.elm:void 0,l);
},
prepatch:function prepatch(e,l){
var t=l.componentOptions;
!function(e,l,t,n,r){
var u=n.data.scopedSlots,i=e.$scopedSlots,o=!!(u&&!u.$stable||i!==a&&!i.$stable||u&&e.$scopedSlots.$key!==u.$key),s=!!(r||e.$options._renderChildren||o);
if(e.$options._parentVnode=n,e.$vnode=n,e._vnode&&(e._vnode.parent=n),
e.$options._renderChildren=r,e.$attrs=n.data.attrs||a,e.$listeners=t||a,
l&&e.$options.props){
de(!1);
for(var c=e._props,v=e.$options._propKeys||[],b=0;b<v.length;b++){
var f=v[b],h=e.$options.props;
c[f]=Ce(f,h,l,e);
}
de(!0),e.$options.propsData=l;
}
e._$updateProperties&&e._$updateProperties(e),t=t||a;
var d=e.$options._parentListeners;
e.$options._parentListeners=t,Ul(e,t,d),s&&(e.$slots=rl(r,n.context),
e.$forceUpdate());
}(l.componentInstance=e.componentInstance,t.propsData,t.listeners,l,t.children);
},
insert:function insert(e){
var l=e.context,a=e.componentInstance;
a._isMounted||(Jl(a,"onServiceCreated"),Jl(a,"onServiceAttached"),a._isMounted=!0,
Jl(a,"mounted")),e.data.keepAlive&&(l._isMounted?function(e){
e._inactive=!1,zl.push(e);
}(a):Gl(a,!0));
},
destroy:function destroy(e){
var l=e.componentInstance;
l._isDestroyed||(e.data.keepAlive?function e(l,a){
if(!(a&&(l._directInactive=!0,Hl(l))||l._inactive)){
l._inactive=!0;
for(var t=0;t<l.$children.length;t++){e(l.$children[t]);}
Jl(l,"deactivated");
}
}(l,!0):l.$destroy());
}},
Ml=Object.keys(Tl);
function El(e,l,u,o,s){
if(!t(e)){
var c=u.$options._base;
if(i(e)&&(e=c.extend(e)),"function"==typeof e){
var b;
if(t(e.cid)&&void 0===(e=function(e,l){
if(r(e.error)&&n(e.errorComp))return e.errorComp;
if(n(e.resolved))return e.resolved;
var a=Nl;
if(a&&n(e.owners)&&-1===e.owners.indexOf(a)&&e.owners.push(a),r(e.loading)&&n(e.loadingComp))return e.loadingComp;
if(a&&!n(e.owners)){
var u=e.owners=[a],o=!0,s=null,c=null;
a.$on("hook:destroyed",function(){
return p(u,a);
});
var b=function b(e){
for(var l=0,a=u.length;l<a;l++){u[l].$forceUpdate();}
e&&(u.length=0,null!==s&&(clearTimeout(s),s=null),null!==c&&(clearTimeout(c),
c=null));
},f=N(function(a){
e.resolved=Ll(a,l),o?u.length=0:b(!0);
}),h=N(function(l){
n(e.errorComp)&&(e.error=!0,b(!0));
}),d=e(f,h);
return i(d)&&(v(d)?t(e.resolved)&&d.then(f,h):v(d.component)&&(d.component.then(f,h),
n(d.error)&&(e.errorComp=Ll(d.error,l)),n(d.loading)&&(e.loadingComp=Ll(d.loading,l),
0===d.delay?e.loading=!0:s=setTimeout(function(){
s=null,t(e.resolved)&&t(e.error)&&(e.loading=!0,b(!1));
},d.delay||200)),n(d.timeout)&&(c=setTimeout(function(){
c=null,t(e.resolved)&&h(null);
},d.timeout)))),o=!1,e.loading?e.loadingComp:e.resolved;
}
}(b=e,c)))return function(e,l,a,t,n){
var r=se();
return r.asyncFactory=e,r.asyncMeta={
data:l,
context:a,
children:t,
tag:n},
r;
}(b,l,u,o,s);
l=l||{},ca(e),n(l.model)&&function(e,l){
var a=e.model&&e.model.prop||"value",t=e.model&&e.model.event||"input";
(l.attrs||(l.attrs={}))[a]=l.model.value;
var r=l.on||(l.on={}),u=r[t],i=l.model.callback;
n(u)?(Array.isArray(u)?-1===u.indexOf(i):u!==i)&&(r[t]=[i].concat(u)):r[t]=i;
}(e.options,l);
var f=function(e,l,a,r){
var u=l.options.props;
if(t(u))return qe(e,l,{},r);
var i={},o=e.attrs,s=e.props;
if(n(o)||n(s))for(var c in u){
var v=x(c);
$e(i,s,c,v,!0)||$e(i,o,c,v,!1);
}
return qe(e,l,i,r);
}(l,e,0,u);
if(r(e.options.functional))return function(e,l,t,r,u){
var i=e.options,o={},s=i.props;
if(n(s))for(var c in s){o[c]=Ce(c,s,l||a);}else n(t.attrs)&&Pl(o,t.attrs),
n(t.props)&&Pl(o,t.props);
var v=new kl(t,o,u,r,e),b=i.render.call(null,v._c,v);
if(b instanceof ie)return Al(b,t,v.parent,i);
if(Array.isArray(b)){
for(var f=el(b)||[],h=new Array(f.length),d=0;d<f.length;d++){h[d]=Al(f[d],t,v.parent,i);}
return h;
}
}(e,f,l,u,o);
var h=l.on;
if(l.on=l.nativeOn,r(e.options.abstract)){
var d=l.slot;
l={},d&&(l.slot=d);
}
!function(e){
for(var l=e.hook||(e.hook={}),a=0;a<Ml.length;a++){
var t=Ml[a],n=l[t],r=Tl[t];
n===r||n&&n._merged||(l[t]=n?Cl(r,n):r);
}
}(l);
var g=e.options.name||s;
return new ie("vue-component-"+e.cid+(g?"-"+g:""),l,void 0,void 0,void 0,u,{
Ctor:e,
propsData:f,
listeners:h,
tag:s,
children:o},
b);
}
}
}
function Cl(e,l){
var a=function a(_a4,t){
e(_a4,t),l(_a4,t);
};
return a._merged=!0,a;
}
function jl(e,l,a,o,s,c){
return(Array.isArray(a)||u(a))&&(s=o,o=a,a=void 0),r(c)&&(s=2),
function(e,l,a,u,o){
return n(a)&&n(a.__ob__)?se():(n(a)&&n(a.is)&&(l=a.is),l?(Array.isArray(u)&&"function"==typeof u[0]&&((a=a||{}).scopedSlots={
default:u[0]},
u.length=0),2===o?u=el(u):1===o&&(u=function(e){
for(var l=0;l<e.length;l++){if(Array.isArray(e[l]))return Array.prototype.concat.apply([],e);}
return e;
}(u)),"string"==typeof l?(c=e.$vnode&&e.$vnode.ns||Y.getTagNamespace(l),
s=Y.isReservedTag(l)?new ie(Y.parsePlatformTagName(l),a,u,void 0,void 0,e):a&&a.pre||!n(v=Ee(e.$options,"components",l))?new ie(l,a,u,void 0,void 0,e):El(v,a,e,u,l)):s=El(l,a,e,u),
Array.isArray(s)?s:n(s)?(n(c)&&function e(l,a,u){
if(l.ns=a,"foreignObject"===l.tag&&(a=void 0,u=!0),n(l.children))for(var i=0,o=l.children.length;i<o;i++){
var s=l.children[i];
n(s.tag)&&(t(s.ns)||r(u)&&"svg"!==s.tag)&&e(s,a,u);
}
}(s,c),n(a)&&function(e){
i(e.style)&&Xe(e.style),i(e.class)&&Xe(e.class);
}(a),s):se()):se());
var s,c,v;
}(e,l,a,o,s);
}
var Dl,Nl=null;
function Ll(e,l){
return(e.__esModule||le&&"Module"===e[Symbol.toStringTag])&&(e=e.default),
i(e)?l.extend(e):e;
}
function Il(e){
return e.isComment&&e.asyncFactory;
}
function Yl(e,l){
Dl.$on(e,l);
}
function Rl(e,l){
Dl.$off(e,l);
}
function Fl(e,l){
var a=Dl;
return function t(){
var n=l.apply(null,arguments);
null!==n&&a.$off(e,t);
};
}
function Ul(e,l,a){
Dl=e,function(e,l,a,n,u,i){
var o,s,c,v;
for(o in e){s=e[o],c=l[o],v=Qe(o),t(s)||(t(c)?(t(s.fns)&&(s=e[o]=Ke(s,i)),
r(v.once)&&(s=e[o]=u(v.name,s,v.capture)),a(v.name,s,v.capture,v.passive,v.params)):s!==c&&(c.fns=s,
e[o]=c));}
for(o in l){t(e[o])&&n((v=Qe(o)).name,l[o],v.capture);}
}(l,a||{},Yl,Rl,Fl,e),Dl=void 0;
}
var Vl=null;
function Hl(e){
for(;e&&(e=e.$parent);){if(e._inactive)return!0;}
return!1;
}
function Gl(e,l){
if(l){
if(e._directInactive=!1,Hl(e))return;
}else if(e._directInactive)return;
if(e._inactive||null===e._inactive){
e._inactive=!1;
for(var a=0;a<e.$children.length;a++){Gl(e.$children[a]);}
Jl(e,"activated");
}
}
function Jl(e,l){
re();
var a=e.$options[l],t=l+" hook";
if(a)for(var n=0,r=a.length;n<r;n++){Ie(a[n],e,null,e,t);}
e._hasHookEvent&&e.$emit("hook:"+l),ue();
}
var Wl=[],zl=[],Bl={},Zl=!1,Xl=!1,Ql=0,Kl=Date.now;
if(G&&!B){
var ql=window.performance;
ql&&"function"==typeof ql.now&&Kl()>document.createEvent("Event").timeStamp&&(Kl=function Kl(){
return ql.now();
});
}
function $l(){
var e,l;
for(Kl(),Xl=!0,Wl.sort(function(e,l){
return e.id-l.id;
}),Ql=0;Ql<Wl.length;Ql++){(e=Wl[Ql]).before&&e.before(),l=e.id,Bl[l]=null,
e.run();}
var a=zl.slice(),t=Wl.slice();
Ql=Wl.length=zl.length=0,Bl={},Zl=Xl=!1,function(e){
for(var l=0;l<e.length;l++){e[l]._inactive=!0,Gl(e[l],!0);}
}(a),function(e){
for(var l=e.length;l--;){
var a=e[l],t=a.vm;
t._watcher===a&&t._isMounted&&!t._isDestroyed&&Jl(t,"updated");
}
}(t),q&&Y.devtools&&q.emit("flush");
}
var ea=0,la=function la(e,l,a,t,n){
this.vm=e,n&&(e._watcher=this),e._watchers.push(this),t?(this.deep=!!t.deep,
this.user=!!t.user,this.lazy=!!t.lazy,this.sync=!!t.sync,this.before=t.before):this.deep=this.user=this.lazy=this.sync=!1,
this.cb=a,this.id=++ea,this.active=!0,this.dirty=this.lazy,this.deps=[],
this.newDeps=[],this.depIds=new ee(),this.newDepIds=new ee(),this.expression="",
"function"==typeof l?this.getter=l:(this.getter=function(e){
if(!V.test(e)){
var l=e.split(".");
return function(e){
for(var a=0;a<l.length;a++){
if(!e)return;
e=e[l[a]];
}
return e;
};
}
}(l),this.getter||(this.getter=M)),this.value=this.lazy?void 0:this.get();
};
la.prototype.get=function(){
var e;
re(this);
var l=this.vm;
try{
e=this.getter.call(l,l);
}catch(e){
if(!this.user)throw e;
Le(e,l,'getter for watcher "'+this.expression+'"');
}finally{
this.deep&&Xe(e),ue(),this.cleanupDeps();
}
return e;
},la.prototype.addDep=function(e){
var l=e.id;
this.newDepIds.has(l)||(this.newDepIds.add(l),this.newDeps.push(e),this.depIds.has(l)||e.addSub(this));
},la.prototype.cleanupDeps=function(){
for(var e=this.deps.length;e--;){
var l=this.deps[e];
this.newDepIds.has(l.id)||l.removeSub(this);
}
var a=this.depIds;
this.depIds=this.newDepIds,this.newDepIds=a,this.newDepIds.clear(),a=this.deps,
this.deps=this.newDeps,this.newDeps=a,this.newDeps.length=0;
},la.prototype.update=function(){
this.lazy?this.dirty=!0:this.sync?this.run():function(e){
var l=e.id;
if(null==Bl[l]){
if(Bl[l]=!0,Xl){
for(var a=Wl.length-1;a>Ql&&Wl[a].id>e.id;){a--;}
Wl.splice(a+1,0,e);
}else Wl.push(e);
Zl||(Zl=!0,Be($l));
}
}(this);
},la.prototype.run=function(){
if(this.active){
var e=this.get();
if(e!==this.value||i(e)||this.deep){
var l=this.value;
if(this.value=e,this.user)try{
this.cb.call(this.vm,e,l);
}catch(e){
Le(e,this.vm,'callback for watcher "'+this.expression+'"');
}else this.cb.call(this.vm,e,l);
}
}
},la.prototype.evaluate=function(){
this.value=this.get(),this.dirty=!1;
},la.prototype.depend=function(){
for(var e=this.deps.length;e--;){this.deps[e].depend();}
},la.prototype.teardown=function(){
if(this.active){
this.vm._isBeingDestroyed||p(this.vm._watchers,this);
for(var e=this.deps.length;e--;){this.deps[e].removeSub(this);}
this.active=!1;
}
};
var aa={
enumerable:!0,
configurable:!0,
get:M,
set:M};

function ta(e,l,a){
aa.get=function(){
return this[l][a];
},aa.set=function(e){
this[l][a]=e;
},Object.defineProperty(e,a,aa);
}
var na={
lazy:!0};

function ra(e,l,a){
var t=!K();
"function"==typeof a?(aa.get=t?ua(l):ia(a),aa.set=M):(aa.get=a.get?t&&!1!==a.cache?ua(l):ia(a.get):M,
aa.set=a.set||M),Object.defineProperty(e,l,aa);
}
function ua(e){
return function(){
var l=this._computedWatchers&&this._computedWatchers[e];
if(l)return l.dirty&&l.evaluate(),ne.SharedObject.target&&l.depend(),l.value;
};
}
function ia(e){
return function(){
return e.call(this,this);
};
}
function oa(e,l,a,t){
return s(a)&&(t=a,a=a.handler),"string"==typeof a&&(a=e[a]),e.$watch(l,a,t);
}
var sa=0;
function ca(e){
var l=e.options;
if(e.super){
var a=ca(e.super);
if(a!==e.superOptions){
e.superOptions=a;
var t=function(e){
var l,a=e.options,t=e.sealedOptions;
for(var n in a){a[n]!==t[n]&&(l||(l={}),l[n]=a[n]);}
return l;
}(e);
t&&P(e.extendOptions,t),(l=e.options=Me(a,e.extendOptions)).name&&(l.components[l.name]=e);
}
}
return l;
}
function va(e){
this._init(e);
}
function ba(e){
return e&&(e.Ctor.options.name||e.tag);
}
function fa(e,l){
return Array.isArray(e)?e.indexOf(l)>-1:"string"==typeof e?e.split(",").indexOf(l)>-1:!!function(e){
return"[object RegExp]"===o.call(e);
}(e)&&e.test(l);
}
function ha(e,l){
var a=e.cache,t=e.keys,n=e._vnode;
for(var r in a){
var u=a[r];
if(u){
var i=ba(u.componentOptions);
i&&!l(i)&&da(a,r,t,n);
}
}
}
function da(e,l,a,t){
var n=e[l];
!n||t&&n.tag===t.tag||n.componentInstance.$destroy(),e[l]=null,p(a,l);
}
(function(e){
e.prototype._init=function(e){
var l=this;
l._uid=sa++,l._isVue=!0,e&&e._isComponent?function(e,l){
var a=e.$options=Object.create(e.constructor.options),t=l._parentVnode;
a.parent=l.parent,a._parentVnode=t;
var n=t.componentOptions;
a.propsData=n.propsData,a._parentListeners=n.listeners,a._renderChildren=n.children,
a._componentTag=n.tag,l.render&&(a.render=l.render,a.staticRenderFns=l.staticRenderFns);
}(l,e):l.$options=Me(ca(l.constructor),e||{},l),l._renderProxy=l,l._self=l,
function(e){
var l=e.$options,a=l.parent;
if(a&&!l.abstract){
for(;a.$options.abstract&&a.$parent;){a=a.$parent;}
a.$children.push(e);
}
e.$parent=a,e.$root=a?a.$root:e,e.$children=[],e.$refs={},e._watcher=null,
e._inactive=null,e._directInactive=!1,e._isMounted=!1,e._isDestroyed=!1,
e._isBeingDestroyed=!1;
}(l),function(e){
e._events=Object.create(null),e._hasHookEvent=!1;
var l=e.$options._parentListeners;
l&&Ul(e,l);
}(l),function(e){
e._vnode=null,e._staticTrees=null;
var l=e.$options,t=e.$vnode=l._parentVnode,n=t&&t.context;
e.$slots=rl(l._renderChildren,n),e.$scopedSlots=a,e._c=function(l,a,t,n){
return jl(e,l,a,t,n,!1);
},e.$createElement=function(l,a,t,n){
return jl(e,l,a,t,n,!0);
};
var r=t&&t.data;
ye(e,"$attrs",r&&r.attrs||a,null,!0),ye(e,"$listeners",l._parentListeners||a,null,!0);
}(l),Jl(l,"beforeCreate"),!l._$fallback&&tl(l),function(e){
e._watchers=[];
var l=e.$options;
l.props&&function(e,l){
var a=e.$options.propsData||{},t=e._props={},n=e.$options._propKeys=[];
!e.$parent||de(!1);
var r=function r(_r){
n.push(_r);
var u=Ce(_r,l,a,e);
ye(t,_r,u),_r in e||ta(e,"_props",_r);
};
for(var u in l){r(u);}
de(!0);
}(e,l.props),l.methods&&function(e,l){
for(var a in e.$options.props,l){e[a]="function"!=typeof l[a]?M:k(l[a],e);}
}(e,l.methods),l.data?function(e){
var l=e.$options.data;
s(l=e._data="function"==typeof l?function(e,l){
re();
try{
return e.call(l,l);
}catch(e){
return Le(e,l,"data()"),{};
}finally{
ue();
}
}(l,e):l||{})||(l={});
for(var a=Object.keys(l),t=e.$options.props,n=(e.$options.methods,a.length);n--;){
var r=a[n];
t&&m(t,r)||R(r)||ta(e,"_data",r);
}
me(l,!0);
}(e):me(e._data={},!0),l.computed&&function(e,l){
var a=e._computedWatchers=Object.create(null),t=K();
for(var n in l){
var r=l[n],u="function"==typeof r?r:r.get;
t||(a[n]=new la(e,u||M,M,na)),n in e||ra(e,n,r);
}
}(e,l.computed),l.watch&&l.watch!==X&&function(e,l){
for(var a in l){
var t=l[a];
if(Array.isArray(t))for(var n=0;n<t.length;n++){oa(e,a,t[n]);}else oa(e,a,t);
}
}(e,l.watch);
}(l),!l._$fallback&&al(l),!l._$fallback&&Jl(l,"created"),l.$options.el&&l.$mount(l.$options.el);
};
})(va),function(e){
Object.defineProperty(e.prototype,"$data",{
get:function get(){
return this._data;
}}),
Object.defineProperty(e.prototype,"$props",{
get:function get(){
return this._props;
}}),
e.prototype.$set=we,e.prototype.$delete=_e,e.prototype.$watch=function(e,l,a){
if(s(l))return oa(this,e,l,a);
(a=a||{}).user=!0;
var t=new la(this,e,l,a);
if(a.immediate)try{
l.call(this,t.value);
}catch(e){
Le(e,this,'callback for immediate watcher "'+t.expression+'"');
}
return function(){
t.teardown();
};
};
}(va),function(e){
var l=/^hook:/;
e.prototype.$on=function(e,a){
var t=this;
if(Array.isArray(e))for(var n=0,r=e.length;n<r;n++){t.$on(e[n],a);}else(t._events[e]||(t._events[e]=[])).push(a),
l.test(e)&&(t._hasHookEvent=!0);
return t;
},e.prototype.$once=function(e,l){
var a=this;
function t(){
a.$off(e,t),l.apply(a,arguments);
}
return t.fn=l,a.$on(e,t),a;
},e.prototype.$off=function(e,l){
var a=this;
if(!arguments.length)return a._events=Object.create(null),a;
if(Array.isArray(e)){
for(var t=0,n=e.length;t<n;t++){a.$off(e[t],l);}
return a;
}
var r,u=a._events[e];
if(!u)return a;
if(!l)return a._events[e]=null,a;
for(var i=u.length;i--;){if((r=u[i])===l||r.fn===l){
u.splice(i,1);
break;
}}
return a;
},e.prototype.$emit=function(e){
var l=this,a=l._events[e];
if(a){
a=a.length>1?A(a):a;
for(var t=A(arguments,1),n='event handler for "'+e+'"',r=0,u=a.length;r<u;r++){Ie(a[r],l,t,l,n);}
}
return l;
};
}(va),function(e){
e.prototype._update=function(e,l){
var a=this,t=a.$el,n=a._vnode,r=function(e){
var l=Vl;
return Vl=e,function(){
Vl=l;
};
}(a);
a._vnode=e,a.$el=n?a.__patch__(n,e):a.__patch__(a.$el,e,l,!1),r(),
t&&(t.__vue__=null),a.$el&&(a.$el.__vue__=a),a.$vnode&&a.$parent&&a.$vnode===a.$parent._vnode&&(a.$parent.$el=a.$el);
},e.prototype.$forceUpdate=function(){
this._watcher&&this._watcher.update();
},e.prototype.$destroy=function(){
var e=this;
if(!e._isBeingDestroyed){
Jl(e,"beforeDestroy"),e._isBeingDestroyed=!0;
var l=e.$parent;
!l||l._isBeingDestroyed||e.$options.abstract||p(l.$children,e),e._watcher&&e._watcher.teardown();
for(var a=e._watchers.length;a--;){e._watchers[a].teardown();}
e._data.__ob__&&e._data.__ob__.vmCount--,e._isDestroyed=!0,e.__patch__(e._vnode,null),
Jl(e,"destroyed"),e.$off(),e.$el&&(e.$el.__vue__=null),e.$vnode&&(e.$vnode.parent=null);
}
};
}(va),function(e){
xl(e.prototype),e.prototype.$nextTick=function(e){
return Be(e,this);
},e.prototype._render=function(){
var e,l=this,a=l.$options,t=a.render,n=a._parentVnode;
n&&(l.$scopedSlots=il(n.data.scopedSlots,l.$slots,l.$scopedSlots)),l.$vnode=n;
try{
Nl=l,e=t.call(l._renderProxy,l.$createElement);
}catch(a){
Le(a,l,"render"),e=l._vnode;
}finally{
Nl=null;
}
return Array.isArray(e)&&1===e.length&&(e=e[0]),e instanceof ie||(e=se()),
e.parent=n,e;
};
}(va);
var pa=[String,RegExp,Array],ga={
KeepAlive:{
name:"keep-alive",
abstract:!0,
props:{
include:pa,
exclude:pa,
max:[String,Number]},

created:function created(){
this.cache=Object.create(null),this.keys=[];
},
destroyed:function destroyed(){
for(var e in this.cache){da(this.cache,e,this.keys);}
},
mounted:function mounted(){
var e=this;
this.$watch("include",function(l){
ha(e,function(e){
return fa(l,e);
});
}),this.$watch("exclude",function(l){
ha(e,function(e){
return!fa(l,e);
});
});
},
render:function render(){
var e=this.$slots.default,l=function(e){
if(Array.isArray(e))for(var l=0;l<e.length;l++){
var a=e[l];
if(n(a)&&(n(a.componentOptions)||Il(a)))return a;
}
}(e),a=l&&l.componentOptions;
if(a){
var t=ba(a),r=this.include,u=this.exclude;
if(r&&(!t||!fa(r,t))||u&&t&&fa(u,t))return l;
var i=this.cache,o=this.keys,s=null==l.key?a.Ctor.cid+(a.tag?"::"+a.tag:""):l.key;
i[s]?(l.componentInstance=i[s].componentInstance,p(o,s),o.push(s)):(i[s]=l,
o.push(s),this.max&&o.length>parseInt(this.max)&&da(i,o[0],o,this._vnode)),
l.data.keepAlive=!0;
}
return l||e&&e[0];
}}};


(function(e){
var l={
get:function get(){
return Y;
}};

Object.defineProperty(e,"config",l),e.util={
warn:ae,
extend:P,
mergeOptions:Me,
defineReactive:ye},
e.set=we,e.delete=_e,e.nextTick=Be,e.observable=function(e){
return me(e),e;
},e.options=Object.create(null),L.forEach(function(l){
e.options[l+"s"]=Object.create(null);
}),e.options._base=e,P(e.options.components,ga),function(e){
e.use=function(e){
var l=this._installedPlugins||(this._installedPlugins=[]);
if(l.indexOf(e)>-1)return this;
var a=A(arguments,1);
return a.unshift(this),"function"==typeof e.install?e.install.apply(e,a):"function"==typeof e&&e.apply(null,a),
l.push(e),this;
};
}(e),function(e){
e.mixin=function(e){
return this.options=Me(this.options,e),this;
};
}(e),function(e){
e.cid=0;
var l=1;
e.extend=function(e){
e=e||{};
var a=this,t=a.cid,n=e._Ctor||(e._Ctor={});
if(n[t])return n[t];
var r=e.name||a.options.name,u=function u(e){
this._init(e);
};
return(u.prototype=Object.create(a.prototype)).constructor=u,u.cid=l++,
u.options=Me(a.options,e),u.super=a,u.options.props&&function(e){
var l=e.options.props;
for(var a in l){ta(e.prototype,"_props",a);}
}(u),u.options.computed&&function(e){
var l=e.options.computed;
for(var a in l){ra(e.prototype,a,l[a]);}
}(u),u.extend=a.extend,u.mixin=a.mixin,u.use=a.use,L.forEach(function(e){
u[e]=a[e];
}),r&&(u.options.components[r]=u),u.superOptions=a.options,u.extendOptions=e,
u.sealedOptions=P({},u.options),n[t]=u,u;
};
}(e),function(e){
L.forEach(function(l){
e[l]=function(e,a){
return a?("component"===l&&s(a)&&(a.name=a.name||e,a=this.options._base.extend(a)),
"directive"===l&&"function"==typeof a&&(a={
bind:a,
update:a}),
this.options[l+"s"][e]=a,a):this.options[l+"s"][e];
};
});
}(e);
})(va),Object.defineProperty(va.prototype,"$isServer",{
get:K}),
Object.defineProperty(va.prototype,"$ssrContext",{
get:function get(){
return this.$vnode&&this.$vnode.ssrContext;
}}),
Object.defineProperty(va,"FunctionalRenderContext",{
value:kl}),
va.version="2.6.11";
var ma="[object Array]",ya="[object Object]";
function wa(e,l,a){
e[l]=a;
}
function _a(e){
return Object.prototype.toString.call(e);
}
function Sa(e){
if(e.__next_tick_callbacks&&e.__next_tick_callbacks.length){
if(Object({
VUE_APP_DARK_MODE:"false",
VUE_APP_NAME:"ch",
VUE_APP_PLATFORM:"mp-weixin",
NODE_ENV:"production",
BASE_URL:"/"}).
VUE_APP_DEBUG){
var l=e.$scope;
console.log("["+ +new Date()+"]["+(l.is||l.route)+"]["+e._uid+"]:flushCallbacks["+e.__next_tick_callbacks.length+"]");
}
var a=e.__next_tick_callbacks.slice(0);
e.__next_tick_callbacks.length=0;
for(var t=0;t<a.length;t++){a[t]();}
}
}
function Oa(e,l){
return l&&(l._isVue||l.__v_isMPComponent)?{}:l;
}
function xa(){}
var ka=y(function(e){
var l={},a=/:(.+)/;
return e.split(/;(?![^(]*\))/g).forEach(function(e){
if(e){
var t=e.split(a);
t.length>1&&(l[t[0].trim()]=t[1].trim());
}
}),l;
}),Aa=["createSelectorQuery","createIntersectionObserver","selectAllComponents","selectComponent"],Pa=["onLaunch","onShow","onHide","onUniNViewMessage","onPageNotFound","onThemeChange","onError","onUnhandledRejection","onInit","onLoad","onReady","onUnload","onPullDownRefresh","onReachBottom","onTabItemTap","onAddToFavorites","onShareTimeline","onShareAppMessage","onResize","onPageScroll","onNavigationBarButtonTap","onBackPress","onNavigationBarSearchInputChanged","onNavigationBarSearchInputConfirmed","onNavigationBarSearchInputClicked","onPageShow","onPageHide","onPageResize","onUploadDouyinVideo"];
va.prototype.__patch__=function(e,l){
var a=this;
if(null!==l&&("page"===this.mpType||"component"===this.mpType)){
var t=this.$scope,n=Object.create(null);
try{
n=function(e){
var l=Object.create(null);
[].concat(Object.keys(e._data||{}),Object.keys(e._computedWatchers||{})).reduce(function(l,a){
return l[a]=e[a],l;
},l);
var a=e.__composition_api_state__||e.__secret_vfa_state__,t=a&&a.rawBindings;
return t&&Object.keys(t).forEach(function(a){
l[a]=e[a];
}),Object.assign(l,e.$mp.data||{}),Array.isArray(e.$options.behaviors)&&-1!==e.$options.behaviors.indexOf("uni://form-field")&&(l.name=e.name,
l.value=e.value),JSON.parse(JSON.stringify(l,Oa));
}(this);
}catch(e){
console.error(e);
}
n.__webviewId__=t.data.__webviewId__;
var r=Object.create(null);
Object.keys(n).forEach(function(e){
r[e]=t.data[e];
});
var u=!1===this.$shouldDiffData?n:function(e,l){
var a={};
return function e(l,a){
if(l!==a){
var t=_a(l),n=_a(a);
if(t==ya&&n==ya){
if(Object.keys(l).length>=Object.keys(a).length)for(var r in a){
var u=l[r];
void 0===u?l[r]=null:e(u,a[r]);
}
}else t==ma&&n==ma&&l.length>=a.length&&a.forEach(function(a,t){
e(l[t],a);
});
}
}(e,l),function e(l,a,t,n){
if(l!==a){
var r=_a(l),u=_a(a);
if(r==ya){if(u!=ya||Object.keys(l).length<Object.keys(a).length)wa(n,t,l);else{
var i=function i(r){
var u=l[r],i=a[r],o=_a(u),s=_a(i);
if(o!=ma&&o!=ya)u!==a[r]&&function(e,l){
return"[object Null]"!==e&&"[object Undefined]"!==e||"[object Null]"!==l&&"[object Undefined]"!==l;
}(o,s)&&wa(n,(""==t?"":t+".")+r,u);else if(o==ma)s!=ma||u.length<i.length?wa(n,(""==t?"":t+".")+r,u):u.forEach(function(l,a){
e(l,i[a],(""==t?"":t+".")+r+"["+a+"]",n);
});else if(o==ya)if(s!=ya||Object.keys(u).length<Object.keys(i).length)wa(n,(""==t?"":t+".")+r,u);else for(var c in u){e(u[c],i[c],(""==t?"":t+".")+r+"."+c,n);}
};
for(var o in l){i(o);}
}}else r==ma?u!=ma||l.length<a.length?wa(n,t,l):l.forEach(function(l,r){
e(l,a[r],t+"["+r+"]",n);
}):wa(n,t,l);
}
}(e,l,"",a),a;
}(n,r);
Object.keys(u).length?(Object({
VUE_APP_DARK_MODE:"false",
VUE_APP_NAME:"ch",
VUE_APP_PLATFORM:"mp-weixin",
NODE_ENV:"production",
BASE_URL:"/"}).
VUE_APP_DEBUG&&console.log("["+ +new Date()+"]["+(t.is||t.route)+"]["+this._uid+"]差量更新",JSON.stringify(u)),
this.__next_tick_pending=!0,t.setData(u,function(){
a.__next_tick_pending=!1,Sa(a);
})):Sa(this);
}
},va.prototype.$mount=function(e,l){
return function(e,l,a){
return e.mpType?("app"===e.mpType&&(e.$options.render=xa),e.$options.render||(e.$options.render=xa),
!e._$fallback&&Jl(e,"beforeMount"),new la(e,function(){
e._update(e._render(),a);
},M,{
before:function before(){
e._isMounted&&!e._isDestroyed&&Jl(e,"beforeUpdate");
}},
!0),a=!1,e):e;
}(this,0,l);
},function(e){
var l=e.extend;
e.extend=function(e){
var a=(e=e||{}).methods;
return a&&Object.keys(a).forEach(function(l){
-1!==Pa.indexOf(l)&&(e[l]=a[l],delete a[l]);
}),l.call(this,e);
};
var a=e.config.optionMergeStrategies,t=a.created;
Pa.forEach(function(e){
a[e]=t;
}),e.prototype.__lifecycle_hooks__=Pa;
}(va),function(e){
e.config.errorHandler=function(l,a,t){
e.util.warn("Error in "+t+': "'+l.toString()+'"',a),console.error(l);
var n="function"==typeof getApp&&getApp();
n&&n.onError&&n.onError(l);
};
var l=e.prototype.$emit;
e.prototype.$emit=function(e){
if(this.$scope&&e){
var a=this.$scope._triggerEvent||this.$scope.triggerEvent;
if(a)try{
a.call(this.$scope,e,{
__args__:A(arguments,1)});

}catch(e){}
}
return l.apply(this,arguments);
},e.prototype.$nextTick=function(e){
return function(e,l){
if(!e.__next_tick_pending&&!function(e){
return Wl.find(function(l){
return e._watcher===l;
});
}(e)){
if(Object({
VUE_APP_DARK_MODE:"false",
VUE_APP_NAME:"ch",
VUE_APP_PLATFORM:"mp-weixin",
NODE_ENV:"production",
BASE_URL:"/"}).
VUE_APP_DEBUG){
var a=e.$scope;
console.log("["+ +new Date()+"]["+(a.is||a.route)+"]["+e._uid+"]:nextVueTick");
}
return Be(l,e);
}
if(Object({
VUE_APP_DARK_MODE:"false",
VUE_APP_NAME:"ch",
VUE_APP_PLATFORM:"mp-weixin",
NODE_ENV:"production",
BASE_URL:"/"}).
VUE_APP_DEBUG){
var t=e.$scope;
console.log("["+ +new Date()+"]["+(t.is||t.route)+"]["+e._uid+"]:nextMPTick");
}
var n;
if(e.__next_tick_callbacks||(e.__next_tick_callbacks=[]),e.__next_tick_callbacks.push(function(){
if(l)try{
l.call(e);
}catch(l){
Le(l,e,"nextTick");
}else n&&n(e);
}),!l&&"undefined"!=typeof Promise)return new Promise(function(e){
n=e;
});
}(this,e);
},Aa.forEach(function(l){
e.prototype[l]=function(e){
return this.$scope&&this.$scope[l]?this.$scope[l](e):"undefined"!=typeof my?"createSelectorQuery"===l?my.createSelectorQuery(e):"createIntersectionObserver"===l?my.createIntersectionObserver(e):void 0:void 0;
};
}),e.prototype.__init_provide=al,e.prototype.__init_injections=tl,e.prototype.__call_hook=function(e,l){
var a=this;
re();
var t,n=a.$options[e],r=e+" hook";
if(n)for(var u=0,i=n.length;u<i;u++){t=Ie(n[u],a,l?[l]:null,a,r);}
return a._hasHookEvent&&a.$emit("hook:"+e,l),ue(),t;
},e.prototype.__set_model=function(l,a,t,n){
Array.isArray(n)&&(-1!==n.indexOf("trim")&&(t=t.trim()),-1!==n.indexOf("number")&&(t=this._n(t))),
l||(l=this),e.set(l,a,t);
},e.prototype.__set_sync=function(l,a,t){
l||(l=this),e.set(l,a,t);
},e.prototype.__get_orig=function(e){
return s(e)&&e.$orig||e;
},e.prototype.__get_value=function(e,l){
return function e(l,a){
var t=a.split("."),n=t[0];
return 0===n.indexOf("__$n")&&(n=parseInt(n.replace("__$n",""))),1===t.length?l[n]:e(l[n],t.slice(1).join("."));
}(l||this,e);
},e.prototype.__get_class=function(e,l){
return function(e,l){
return n(e)||n(l)?function(e,l){
return e?l?e+" "+l:e:l||"";
}(e,function e(l){
return Array.isArray(l)?function(l){
for(var a,t="",r=0,u=l.length;r<u;r++){n(a=e(l[r]))&&""!==a&&(t&&(t+=" "),
t+=a);}
return t;
}(l):i(l)?function(e){
var l="";
for(var a in e){e[a]&&(l&&(l+=" "),l+=a);}
return l;
}(l):"string"==typeof l?l:"";
}(l)):"";
}(l,e);
},e.prototype.__get_style=function(e,l){
if(!e&&!l)return"";
var a=function(e){
return Array.isArray(e)?T(e):"string"==typeof e?ka(e):e;
}(e),t=l?P(l,a):a;
return Object.keys(t).map(function(e){
return x(e)+":"+t[e];
}).join(";");
},e.prototype.__map=function(e,l){
var a,t,n,r,u;
if(Array.isArray(e)){
for(a=new Array(e.length),t=0,n=e.length;t<n;t++){a[t]=l(e[t],t);}
return a;
}
if(i(e)){
for(r=Object.keys(e),a=Object.create(null),t=0,n=r.length;t<n;t++){a[u=r[t]]=l(e[u],u,t);}
return a;
}
if("number"==typeof e){
for(a=new Array(e),t=0,n=e;t<n;t++){a[t]=l(t,t);}
return a;
}
return[];
};
}(va),l.default=va;
}.call(this,a("c8ba"));
},
"6eb0":function eb0(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,l.default=[{
label:"北京市",
value:"11"},
{
label:"天津市",
value:"12"},
{
label:"河北省",
value:"13"},
{
label:"山西省",
value:"14"},
{
label:"内蒙古自治区",
value:"15"},
{
label:"辽宁省",
value:"21"},
{
label:"吉林省",
value:"22"},
{
label:"黑龙江省",
value:"23"},
{
label:"上海市",
value:"31"},
{
label:"江苏省",
value:"32"},
{
label:"浙江省",
value:"33"},
{
label:"安徽省",
value:"34"},
{
label:"福建省",
value:"35"},
{
label:"江西省",
value:"36"},
{
label:"山东省",
value:"37"},
{
label:"河南省",
value:"41"},
{
label:"湖北省",
value:"42"},
{
label:"湖南省",
value:"43"},
{
label:"广东省",
value:"44"},
{
label:"广西壮族自治区",
value:"45"},
{
label:"海南省",
value:"46"},
{
label:"重庆市",
value:"50"},
{
label:"四川省",
value:"51"},
{
label:"贵州省",
value:"52"},
{
label:"云南省",
value:"53"},
{
label:"西藏自治区",
value:"54"},
{
label:"陕西省",
value:"61"},
{
label:"甘肃省",
value:"62"},
{
label:"青海省",
value:"63"},
{
label:"宁夏回族自治区",
value:"64"},
{
label:"新疆维吾尔自治区",
value:"65"},
{
label:"台湾",
value:"66"},
{
label:"香港",
value:"67"},
{
label:"澳门",
value:"68"}];

},
"6f8f":function f8f(e,l){
e.exports=function(){
if("undefined"==typeof Reflect||!Reflect.construct)return!1;
if(Reflect.construct.sham)return!1;
if("function"==typeof Proxy)return!0;
try{
return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],function(){})),
!0;
}catch(e){
return!1;
}
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
7037:function _(e,l){
function a(l){
return e.exports=a="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){
return typeof e;
}:function(e){
return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e;
},e.exports.__esModule=!0,e.exports.default=e.exports,a(l);
}
e.exports=a,e.exports.__esModule=!0,e.exports.default=e.exports;
},
7258:function _(e,l){
e.exports={
show:["GET","/user/clock-in"],
clock_in:["PUT","/user/clock-in"],
update_image:["PUT","/user/clock-in/image"],
update_diary:["PUT","/user/clock-in/diary"],
update_image_share:["PUT","/user/clock-in/image-share"],
single_show:["GET","/clock-in/{uuid}"]};

},
7974:function _(e,l,a){
var t=a("970b"),n=a("5bc3"),r=a("a72c"),u=a("ec6a"),i=function(){
"use strict";
function e(l){
var a=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},n=arguments.length>2?arguments[2]:void 0;
t(this,e),this.cb=n,this.CssHandler=new r(a.tagStyle),this.data=l,this.DOM=[],
this._attrName="",this._attrValue="",this._attrs={},this._domain=a.domain,
this._protocol=a.domain&&a.domain.includes("://")?this._domain.split("://")[0]:"http",
this._i=0,this._sectionStart=0,this._state=this.Text,this._STACK=[],
this._tagName="",this._audioNum=0,this._imgNum=0,this._videoNum=0,this._useAnchor=a.useAnchor,
this._whiteSpace=!1;
}
return n(e,[{
key:"parse",
value:function value(){
this.CssHandler&&(this.data=this.CssHandler.getStyle(this.data)),u.highlight&&(this.data=this.data.replace(/<[pP][rR][eE]([\s\S]*?)>([\s\S]*?)<\/[pP][rR][eE][\s\S]*?>/g,function(e,l,a){
return"<pre"+l+">"+u.highlight(a,"<pre"+l+">")+"</pre>";
}));
for(var e=this.data.length;this._i<e;this._i++){this._state(this.data[this._i]);}
for(this._state==this.Text&&this.setText();this._STACK.length;){this.popNode(this._STACK.pop());}
if(this.DOM.length&&(this.DOM[0].PoweredBy="Parser"),!this.cb)return this.DOM;
this.cb(this.DOM);
}},
{
key:"setAttr",
value:function value(){
for(u.trustAttrs[this._attrName]&&("src"==this._attrName&&"/"==this._attrValue[0]&&("/"==this._attrValue[1]?this._attrValue=this._protocol+":"+this._attrValue:this._domain&&(this._attrValue=this._domain+this._attrValue)),
this._attrs[this._attrName]=this._attrValue?this._attrValue:"src"==this._attrName?"":"true"),
this._attrValue="";u.blankChar[this.data[this._i]];){this._i++;}
this.checkClose()?this.setNode():this._state=this.AttrName;
}},
{
key:"setText",
value:function value(){
var e=this.getSelection();
if(e){
if(!this._whiteSpace){
for(var l=[],a=e.length,t=!1;a--;){(!u.blankChar[e[a]]&&(t=!0)||!u.blankChar[l[0]])&&l.unshift(e[a]);}
if(!t)return;
e=l.join("");
}
var n,r,i;
for(a=e.indexOf("&");-1!=a&&-1!=(n=e.indexOf(";",a+2));){"#"==e[a+1]?(r=parseInt(("x"==e[a+2]?"0":"")+e.substring(a+2,n)),
isNaN(r)||(e=e.substring(0,a)+String.fromCharCode(r)+e.substring(n+1))):"nbsp"==(r=e.substring(a+1,n))?e=e.substring(0,a)+" "+e.substring(n+1):"lt"!=r&&"gt"!=r&&"amp"!=r&&"ensp"!=r&&"emsp"!=r&&(i=!0),
a=e.indexOf("&",a+2);}
var o=this._STACK.length?this._STACK[this._STACK.length-1].children:this.DOM;
o.length&&"text"==o[o.length-1].type?(o[o.length-1].text+=e,i&&(o[o.length-1].decode=!0)):o.push({
type:"text",
text:e,
decode:i});

}
}},
{
key:"setNode",
value:function value(){
var e=this._STACK.length?this._STACK[this._STACK.length-1].children:this.DOM,l={
name:this._tagName.toLowerCase(),
attrs:this._attrs};

if(u.LabelAttrsHandler(l,this),this._attrs={},">"==this.data[this._i]){
if(!u.selfClosingTags[this._tagName]){
if(u.ignoreTags[l.name]){
for(var a=this._i;this._i<this.data.length;){
for(-1==(this._i=this.data.indexOf("</",this._i+1))&&(this._i=this.data.length),
this._i+=2,this._sectionStart=this._i;!u.blankChar[this.data[this._i]]&&">"!=this.data[this._i]&&"/"!=this.data[this._i];){this._i++;}
if(this.data.substring(this._sectionStart,this._i).toLowerCase()==l.name){
if(this._i=this.data.indexOf(">",this._i),-1==this._i?this._i=this.data.length:this._sectionStart=this._i+1,
this._state=this.Text,"svg"==l.name){
var t=this.data.substring(a,this._i+1);
for(l.attrs.xmlns||(t=' xmlns="http://www.w3.org/2000/svg"'+t),this._i=a;"<"!=this.data[a];){a--;}
t=this.data.substring(a,this._i)+t,this._i=this._sectionStart-1,l.name="img",
l.attrs={
src:"data:image/svg+xml;utf8,"+t.replace(/#/g,"%23"),
ignore:"true"},
e.push(l);
}
break;
}
}
return;
}
this._STACK.push(l),l.children=[];
}
}else this._i++;
this._sectionStart=this._i+1,this._state=this.Text,u.ignoreTags[l.name]||(("pre"==l.name||l.attrs.style&&l.attrs.style.toLowerCase().includes("white-space")&&l.attrs.style.toLowerCase().includes("pre"))&&(this._whiteSpace=!0,
l.pre=!0),e.push(l));
}},
{
key:"popNode",
value:function value(e){
if(u.blockTags[e.name]?e.name="div":u.trustTags[e.name]||(e.name="span"),
e.pre){
this._whiteSpace=!1,e.pre=void 0;
for(var l=this._STACK.length;l--;){this._STACK[l].pre&&(this._whiteSpace=!0);}
}
if(e.c)if("ul"==e.name){
var a=1;
for(l=this._STACK.length;l--;){"ul"==this._STACK[l].name&&a++;}
if(1!=a)for(l=e.children.length;l--;){e.children[l].floor=a;}
}else if("ol"==e.name)for(var t=function t(e,l){
if("a"==l)return String.fromCharCode(97+(e-1)%26);
if("A"==l)return String.fromCharCode(65+(e-1)%26);
if("i"==l||"I"==l){
e=(e-1)%99+1;
var a=(["X","XX","XXX","XL","L","LX","LXX","LXXX","XC"][Math.floor(e/10)-1]||"")+(["I","II","III","IV","V","VI","VII","VIII","IX"][e%10-1]||"");
return"i"==l?a.toLowerCase():a;
}
return e;
},n=(l=0,1);i=e.children[l++];){"li"==i.name&&(i.type="ol",i.num=t(n++,e.attrs.type)+".");}
if("table"==e.name){
var r=function l(a){
if("th"==a.name||"td"==a.name)return e.attrs.border&&(a.attrs.style="border:"+e.attrs.border+"px solid gray;"+(a.attrs.style||"")),
void(e.attrs.hasOwnProperty("cellpadding")&&(a.attrs.style="padding:"+e.attrs.cellpadding+"px;"+(a.attrs.style||"")));
if("text"!=a.type)for(var t=0;t<(a.children||[]).length;t++){l(a.children[t]);}
};
if(e.attrs.border&&(e.attrs.style="border:"+e.attrs.border+"px solid gray;"+(e.attrs.style||"")),
e.attrs.hasOwnProperty("cellspacing")&&(e.attrs.style="border-spacing:"+e.attrs.cellspacing+"px;"+(e.attrs.style||"")),
e.attrs.border||e.attrs.hasOwnProperty("cellpadding"))for(l=0;l<e.children.length;l++){r(e.children[l]);}
}
if(1==e.children.length&&"div"==e.name&&"div"==e.children[0].name){
var i=e.children[0].attrs;
e.attrs.style=e.attrs.style||"",i.style=i.style||"",!(e.attrs.style.includes("padding")&&(e.attrs.style.includes("margin")||i.style.includes("margin"))&&e.attrs.style.includes("display")&&i.style.includes("display"))||e.attrs.id&&e.attrs.id||e.attrs.class&&i.class?e.attrs.style?i.style||(i.style=void 0):e.attrs.style=void 0:(i.style.includes("padding")&&(i.style="box-sizing:border-box;"+i.style),
e.attrs.style=e.attrs.style+";"+i.style,e.attrs.id=(i.id||"")+(e.attrs.id||""),
e.attrs.class=(i.class||"")+(e.attrs.class||""),e.children=i.children),
i=void 0;
}
this.CssHandler.pop&&this.CssHandler.pop(e);
}},
{
key:"checkClose",
value:function value(){
return">"==this.data[this._i]||"/"==this.data[this._i]&&">"==this.data[this._i+1];
}},
{
key:"getSelection",
value:function value(e){
for(var l=this._sectionStart==this._i?"":this.data.substring(this._sectionStart,this._i);e&&(u.blankChar[this.data[++this._i]]||(this._i--,
0));){;}
return this._sectionStart=this._i+1,l;
}},
{
key:"Text",
value:function value(e){
if("<"==e){
var l=this.data[this._i+1];
l>="a"&&l<="z"||l>="A"&&l<="Z"?(this.setText(),this._state=this.TagName):"/"==l?(this.setText(),
this._i++,(l=this.data[this._i+1])>="a"&&l<="z"||l>="A"&&l<="Z"?(this._sectionStart=this._i+1,
this._state=this.EndTag):this._state=this.Comment):"!"==l&&(this.setText(),
this._state=this.Comment);
}
}},
{
key:"Comment",
value:function value(){
if("--"==this.data.substring(this._i+1,this._i+3)||"[CDATA["==this.data.substring(this._i+1,this._i+7)){
if(this._i=this.data.indexOf("--\x3e",this._i+1),-1==this._i)return this._i=this.data.length;
this._i=this._i+2;
}else-1==(this._i=this.data.indexOf(">",this._i+1))&&(this._i=this.data.length);
this._sectionStart=this._i+1,this._state=this.Text;
}},
{
key:"TagName",
value:function value(e){
u.blankChar[e]?(this._tagName=this.getSelection(!0),this.checkClose()?this.setNode():this._state=this.AttrName):this.checkClose()&&(this._tagName=this.getSelection(),
this.setNode());
}},
{
key:"AttrName",
value:function value(e){
if(u.blankChar[e]){if(this._attrName=this.getSelection(!0).toLowerCase(),"="==this.data[this._i]){
for(;u.blankChar[this.data[++this._i]];){;}
this._sectionStart=this._i--,this._state=this.AttrValue;
}else this.setAttr();}else if("="==e){
for(this._attrName=this.getSelection().toLowerCase();u.blankChar[this.data[++this._i]];){;}
this._sectionStart=this._i--,this._state=this.AttrValue;
}else this.checkClose()&&(this._attrName=this.getSelection().toLowerCase(),
this.setAttr());
}},
{
key:"AttrValue",
value:function value(e){
if('"'==e||"'"==e){
if(this._sectionStart++,-1==(this._i=this.data.indexOf(e,this._i+1)))return this._i=this.data.length;
}else for(;!u.blankChar[this.data[this._i]]&&">"!=this.data[this._i];this._i++){;}
for(this._attrValue=this.getSelection();this._attrValue.includes("&quot;");){this._attrValue=this._attrValue.replace("&quot;","");}
this.setAttr();
}},
{
key:"EndTag",
value:function value(e){
if(u.blankChar[e]||">"==e||"/"==e){
for(var l,a=this.getSelection().toLowerCase(),t=!1,n=this._STACK.length;n--;){if(this._STACK[n].name==a){
t=!0;
break;
}}
if(t)for(;t;){(l=this._STACK.pop()).name==a&&(t=!1),this.popNode(l);}else"p"!=a&&"br"!=a||(this._STACK.length?this._STACK[this._STACK.length-1].children:this.DOM).push({
name:a,
attrs:{}});

this._i=this.data.indexOf(">",this._i),-1==this._i?this._i=this.data.length:this._state=this.Text;
}
}}]),
e;
}();
e.exports={
parseHtml:function parseHtml(e,l){
return new Promise(function(a){
return new i(e,l,a).parse();
});
},
parseHtmlSync:function parseHtmlSync(e,l){
return new i(e,l).parse();
}};

},
"7ec2":function ec2(e,l,a){
var t=a("7037").default;
function n(){
"use strict";
e.exports=n=function n(){
return l;
},e.exports.__esModule=!0,e.exports.default=e.exports;
var l={},a=Object.prototype,r=a.hasOwnProperty,u=Object.defineProperty||function(e,l,a){
e[l]=a.value;
},i="function"==typeof Symbol?Symbol:{},o=i.iterator||"@@iterator",s=i.asyncIterator||"@@asyncIterator",c=i.toStringTag||"@@toStringTag";
function v(e,l,a){
return Object.defineProperty(e,l,{
value:a,
enumerable:!0,
configurable:!0,
writable:!0}),
e[l];
}
try{
v({},"");
}catch(e){
v=function v(e,l,a){
return e[l]=a;
};
}
function b(e,l,a,t){
var n=l&&l.prototype instanceof d?l:d,r=Object.create(n.prototype),i=new T(t||[]);
return u(r,"_invoke",{
value:x(e,a,i)}),
r;
}
function f(e,l,a){
try{
return{
type:"normal",
arg:e.call(l,a)};

}catch(e){
return{
type:"throw",
arg:e};

}
}
l.wrap=b;
var h={};
function d(){}
function p(){}
function g(){}
var m={};
v(m,o,function(){
return this;
});
var y=Object.getPrototypeOf,w=y&&y(y(M([])));
w&&w!==a&&r.call(w,o)&&(m=w);
var _=g.prototype=d.prototype=Object.create(m);
function S(e){
["next","throw","return"].forEach(function(l){
v(e,l,function(e){
return this._invoke(l,e);
});
});
}
function O(e,l){
var a;
u(this,"_invoke",{
value:function value(n,u){
function i(){
return new l(function(a,i){
!function a(n,u,i,o){
var s=f(e[n],e,u);
if("throw"!==s.type){
var c=s.arg,v=c.value;
return v&&"object"==t(v)&&r.call(v,"__await")?l.resolve(v.__await).then(function(e){
a("next",e,i,o);
},function(e){
a("throw",e,i,o);
}):l.resolve(v).then(function(e){
c.value=e,i(c);
},function(e){
return a("throw",e,i,o);
});
}
o(s.arg);
}(n,u,a,i);
});
}
return a=a?a.then(i,i):i();
}});

}
function x(e,l,a){
var t="suspendedStart";
return function(n,r){
if("executing"===t)throw new Error("Generator is already running");
if("completed"===t){
if("throw"===n)throw r;
return{
value:void 0,
done:!0};

}
for(a.method=n,a.arg=r;;){
var u=a.delegate;
if(u){
var i=k(u,a);
if(i){
if(i===h)continue;
return i;
}
}
if("next"===a.method)a.sent=a._sent=a.arg;else if("throw"===a.method){
if("suspendedStart"===t)throw t="completed",a.arg;
a.dispatchException(a.arg);
}else"return"===a.method&&a.abrupt("return",a.arg);
t="executing";
var o=f(e,l,a);
if("normal"===o.type){
if(t=a.done?"completed":"suspendedYield",o.arg===h)continue;
return{
value:o.arg,
done:a.done};

}
"throw"===o.type&&(t="completed",a.method="throw",a.arg=o.arg);
}
};
}
function k(e,l){
var a=l.method,t=e.iterator[a];
if(void 0===t)return l.delegate=null,"throw"===a&&e.iterator.return&&(l.method="return",
l.arg=void 0,k(e,l),"throw"===l.method)||"return"!==a&&(l.method="throw",
l.arg=new TypeError("The iterator does not provide a '"+a+"' method")),h;
var n=f(t,e.iterator,l.arg);
if("throw"===n.type)return l.method="throw",l.arg=n.arg,l.delegate=null,
h;
var r=n.arg;
return r?r.done?(l[e.resultName]=r.value,l.next=e.nextLoc,"return"!==l.method&&(l.method="next",
l.arg=void 0),l.delegate=null,h):r:(l.method="throw",l.arg=new TypeError("iterator result is not an object"),
l.delegate=null,h);
}
function A(e){
var l={
tryLoc:e[0]};

1 in e&&(l.catchLoc=e[1]),2 in e&&(l.finallyLoc=e[2],l.afterLoc=e[3]),
this.tryEntries.push(l);
}
function P(e){
var l=e.completion||{};
l.type="normal",delete l.arg,e.completion=l;
}
function T(e){
this.tryEntries=[{
tryLoc:"root"}],
e.forEach(A,this),this.reset(!0);
}
function M(e){
if(e){
var l=e[o];
if(l)return l.call(e);
if("function"==typeof e.next)return e;
if(!isNaN(e.length)){
var a=-1,t=function l(){
for(;++a<e.length;){if(r.call(e,a))return l.value=e[a],l.done=!1,l;}
return l.value=void 0,l.done=!0,l;
};
return t.next=t;
}
}
return{
next:E};

}
function E(){
return{
value:void 0,
done:!0};

}
return p.prototype=g,u(_,"constructor",{
value:g,
configurable:!0}),
u(g,"constructor",{
value:p,
configurable:!0}),
p.displayName=v(g,c,"GeneratorFunction"),l.isGeneratorFunction=function(e){
var l="function"==typeof e&&e.constructor;
return!!l&&(l===p||"GeneratorFunction"===(l.displayName||l.name));
},l.mark=function(e){
return Object.setPrototypeOf?Object.setPrototypeOf(e,g):(e.__proto__=g,v(e,c,"GeneratorFunction")),
e.prototype=Object.create(_),e;
},l.awrap=function(e){
return{
__await:e};

},S(O.prototype),v(O.prototype,s,function(){
return this;
}),l.AsyncIterator=O,l.async=function(e,a,t,n,r){
void 0===r&&(r=Promise);
var u=new O(b(e,a,t,n),r);
return l.isGeneratorFunction(a)?u:u.next().then(function(e){
return e.done?e.value:u.next();
});
},S(_),v(_,c,"Generator"),v(_,o,function(){
return this;
}),v(_,"toString",function(){
return"[object Generator]";
}),l.keys=function(e){
var l=Object(e),a=[];
for(var t in l){a.push(t);}
return a.reverse(),function e(){
for(;a.length;){
var t=a.pop();
if(t in l)return e.value=t,e.done=!1,e;
}
return e.done=!0,e;
};
},l.values=M,T.prototype={
constructor:T,
reset:function reset(e){
if(this.prev=0,this.next=0,this.sent=this._sent=void 0,this.done=!1,
this.delegate=null,this.method="next",this.arg=void 0,this.tryEntries.forEach(P),
!e)for(var l in this){"t"===l.charAt(0)&&r.call(this,l)&&!isNaN(+l.slice(1))&&(this[l]=void 0);}
},
stop:function stop(){
this.done=!0;
var e=this.tryEntries[0].completion;
if("throw"===e.type)throw e.arg;
return this.rval;
},
dispatchException:function dispatchException(e){
if(this.done)throw e;
var l=this;
function a(a,t){
return u.type="throw",u.arg=e,l.next=a,t&&(l.method="next",l.arg=void 0),
!!t;
}
for(var t=this.tryEntries.length-1;t>=0;--t){
var n=this.tryEntries[t],u=n.completion;
if("root"===n.tryLoc)return a("end");
if(n.tryLoc<=this.prev){
var i=r.call(n,"catchLoc"),o=r.call(n,"finallyLoc");
if(i&&o){
if(this.prev<n.catchLoc)return a(n.catchLoc,!0);
if(this.prev<n.finallyLoc)return a(n.finallyLoc);
}else if(i){
if(this.prev<n.catchLoc)return a(n.catchLoc,!0);
}else{
if(!o)throw new Error("try statement without catch or finally");
if(this.prev<n.finallyLoc)return a(n.finallyLoc);
}
}
}
},
abrupt:function abrupt(e,l){
for(var a=this.tryEntries.length-1;a>=0;--a){
var t=this.tryEntries[a];
if(t.tryLoc<=this.prev&&r.call(t,"finallyLoc")&&this.prev<t.finallyLoc){
var n=t;
break;
}
}
n&&("break"===e||"continue"===e)&&n.tryLoc<=l&&l<=n.finallyLoc&&(n=null);
var u=n?n.completion:{};
return u.type=e,u.arg=l,n?(this.method="next",this.next=n.finallyLoc,
h):this.complete(u);
},
complete:function complete(e,l){
if("throw"===e.type)throw e.arg;
return"break"===e.type||"continue"===e.type?this.next=e.arg:"return"===e.type?(this.rval=this.arg=e.arg,
this.method="return",this.next="end"):"normal"===e.type&&l&&(this.next=l),
h;
},
finish:function finish(e){
for(var l=this.tryEntries.length-1;l>=0;--l){
var a=this.tryEntries[l];
if(a.finallyLoc===e)return this.complete(a.completion,a.afterLoc),P(a),h;
}
},
catch:function _catch(e){
for(var l=this.tryEntries.length-1;l>=0;--l){
var a=this.tryEntries[l];
if(a.tryLoc===e){
var t=a.completion;
if("throw"===t.type){
var n=t.arg;
P(a);
}
return n;
}
}
throw new Error("illegal catch attempt");
},
delegateYield:function delegateYield(e,l,a){
return this.delegate={
iterator:M(e),
resultName:l,
nextLoc:a},
"next"===this.method&&(this.arg=void 0),h;
}},
l;
}
e.exports=n,e.exports.__esModule=!0,e.exports.default=e.exports;
},
8094:function _(e,l){
e.exports={
info:["GET","/user"],
login:["POST","/login/with-miniapp"],
update:["PUT","/user"],
memberCard:{
check:["POST","/member-card/check"],
get_form:["POST","/member-card/get-form"],
pick:["POST","/member-card/pick"]},

coupon:{
list:["GET","/my-coupons"]}};


},
"82c0":function c0(e,l,a){
"use strict";
(function(l){
var t=a("4ea4"),n=t(a("9523")),r=t(a("0863"));
function u(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function i(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?u(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):u(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
e.exports={
pay:function pay(e){
if(console.log(e),"select_pending"===(e.pay_type||"wechat"))return this.selectPayMethod(e),
!1;
l.requestPayment(i(i({},e.pay_config),{},{
success:function success(){
e.success&&e.success();
},
fail:function fail(){
e.fail&&e.fail();
}}));

},
selectPayMethod:function selectPayMethod(e){
var a=this;
l.showActionSheet({
itemList:["支付宝支付","微信支付"],
success:function success(t){
var n=["alipay","wechat"][t.tapIndex];
l.showLoading({
title:"加载中~"}),
(0,r.default)("/orders/".concat(e.order.uuid,"/payment-config"),"POST",{
pay_type:n}).
then(function(t){
l.hideLoading(),e=i(i({},e),t.data),a.pay(e);
});
},
fail:function fail(e){
console.log(e.errMsg);
}});

}};

}).call(this,a("543d").default);
},
"8cf6":function cf6(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.isPC=function(){
for(var e=navigator.userAgent,l=["Android","iPhone","SymbianOS","Windows Phone","iPad","iPod"],a=!0,t=0;t<l.length-1;t++){if(e.indexOf(l[t])>0){
a=!1;
break;
}}
return a;
};
},
"8fc9":function fc9(e,l,a){
(function(e){
var t,n,r=a("7037");
!function(u,i){
"object"===r(l)&&void 0!==e?e.exports=i():void 0===(n="function"==typeof(t=i)?t.call(l,a,l,e):t)||(e.exports=n);
}(0,function(){
"use strict";
var l,a;
function t(){
return l.apply(null,arguments);
}
function n(e){
return e instanceof Array||"[object Array]"===Object.prototype.toString.call(e);
}
function u(e){
return null!=e&&"[object Object]"===Object.prototype.toString.call(e);
}
function i(e,l){
return Object.prototype.hasOwnProperty.call(e,l);
}
function o(e){
if(Object.getOwnPropertyNames)return 0===Object.getOwnPropertyNames(e).length;
var l;
for(l in e){if(i(e,l))return!1;}
return!0;
}
function s(e){
return void 0===e;
}
function c(e){
return"number"==typeof e||"[object Number]"===Object.prototype.toString.call(e);
}
function v(e){
return e instanceof Date||"[object Date]"===Object.prototype.toString.call(e);
}
function b(e,l){
var a,t=[];
for(a=0;a<e.length;++a){t.push(l(e[a],a));}
return t;
}
function f(e,l){
for(var a in l){i(l,a)&&(e[a]=l[a]);}
return i(l,"toString")&&(e.toString=l.toString),i(l,"valueOf")&&(e.valueOf=l.valueOf),
e;
}
function h(e,l,a,t){
return Al(e,l,a,t,!0).utc();
}
function d(e){
return null==e._pf&&(e._pf={
empty:!1,
unusedTokens:[],
unusedInput:[],
overflow:-2,
charsLeftOver:0,
nullInput:!1,
invalidEra:null,
invalidMonth:null,
invalidFormat:!1,
userInvalidated:!1,
iso:!1,
parsedDateParts:[],
era:null,
meridiem:null,
rfc2822:!1,
weekdayMismatch:!1}),
e._pf;
}
function p(e){
if(null==e._isValid){
var l=d(e),t=a.call(l.parsedDateParts,function(e){
return null!=e;
}),n=!isNaN(e._d.getTime())&&l.overflow<0&&!l.empty&&!l.invalidEra&&!l.invalidMonth&&!l.invalidWeekday&&!l.weekdayMismatch&&!l.nullInput&&!l.invalidFormat&&!l.userInvalidated&&(!l.meridiem||l.meridiem&&t);
if(e._strict&&(n=n&&0===l.charsLeftOver&&0===l.unusedTokens.length&&void 0===l.bigHour),
null!=Object.isFrozen&&Object.isFrozen(e))return n;
e._isValid=n;
}
return e._isValid;
}
function g(e){
var l=h(NaN);
return null!=e?f(d(l),e):d(l).userInvalidated=!0,l;
}
a=Array.prototype.some?Array.prototype.some:function(e){
var l,a=Object(this),t=a.length>>>0;
for(l=0;l<t;l++){if(l in a&&e.call(this,a[l],l,a))return!0;}
return!1;
};
var m=t.momentProperties=[],y=!1;
function w(e,l){
var a,t,n;
if(s(l._isAMomentObject)||(e._isAMomentObject=l._isAMomentObject),s(l._i)||(e._i=l._i),
s(l._f)||(e._f=l._f),s(l._l)||(e._l=l._l),s(l._strict)||(e._strict=l._strict),
s(l._tzm)||(e._tzm=l._tzm),s(l._isUTC)||(e._isUTC=l._isUTC),s(l._offset)||(e._offset=l._offset),
s(l._pf)||(e._pf=d(l)),s(l._locale)||(e._locale=l._locale),m.length>0)for(a=0;a<m.length;a++){s(n=l[t=m[a]])||(e[t]=n);}
return e;
}
function _(e){
w(this,e),this._d=new Date(null!=e._d?e._d.getTime():NaN),this.isValid()||(this._d=new Date(NaN)),
!1===y&&(y=!0,t.updateOffset(this),y=!1);
}
function S(e){
return e instanceof _||null!=e&&null!=e._isAMomentObject;
}
function O(e){
!1===t.suppressDeprecationWarnings&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+e);
}
function x(e,l){
var a=!0;
return f(function(){
if(null!=t.deprecationHandler&&t.deprecationHandler(null,e),a){
var n,u,o,s=[];
for(u=0;u<arguments.length;u++){
if(n="","object"===r(arguments[u])){
for(o in n+="\n["+u+"] ",arguments[0]){i(arguments[0],o)&&(n+=o+": "+arguments[0][o]+", ");}
n=n.slice(0,-2);
}else n=arguments[u];
s.push(n);
}
O(e+"\nArguments: "+Array.prototype.slice.call(s).join("")+"\n"+new Error().stack),
a=!1;
}
return l.apply(this,arguments);
},l);
}
var k,A={};
function P(e,l){
null!=t.deprecationHandler&&t.deprecationHandler(e,l),A[e]||(O(l),A[e]=!0);
}
function T(e){
return"undefined"!=typeof Function&&e instanceof Function||"[object Function]"===Object.prototype.toString.call(e);
}
function M(e,l){
var a,t=f({},e);
for(a in l){i(l,a)&&(u(e[a])&&u(l[a])?(t[a]={},f(t[a],e[a]),f(t[a],l[a])):null!=l[a]?t[a]=l[a]:delete t[a]);}
for(a in e){i(e,a)&&!i(l,a)&&u(e[a])&&(t[a]=f({},t[a]));}
return t;
}
function E(e){
null!=e&&this.set(e);
}
function C(e,l,a){
var t=""+Math.abs(e),n=l-t.length;
return(e>=0?a?"+":"":"-")+Math.pow(10,Math.max(0,n)).toString().substr(1)+t;
}
t.suppressDeprecationWarnings=!1,t.deprecationHandler=null,k=Object.keys?Object.keys:function(e){
var l,a=[];
for(l in e){i(e,l)&&a.push(l);}
return a;
};
var j=/(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|N{1,5}|YYYYYY|YYYYY|YYYY|YY|y{2,4}|yo?|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,D=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,N={},L={};
function I(e,l,a,t){
var n=t;
"string"==typeof t&&(n=function n(){
return this[t]();
}),e&&(L[e]=n),l&&(L[l[0]]=function(){
return C(n.apply(this,arguments),l[1],l[2]);
}),a&&(L[a]=function(){
return this.localeData().ordinal(n.apply(this,arguments),e);
});
}
function Y(e){
return e.match(/\[[\s\S]/)?e.replace(/^\[|\]$/g,""):e.replace(/\\/g,"");
}
function R(e,l){
return e.isValid()?(l=F(l,e.localeData()),N[l]=N[l]||function(e){
var l,a,t=e.match(j);
for(l=0,a=t.length;l<a;l++){L[t[l]]?t[l]=L[t[l]]:t[l]=Y(t[l]);}
return function(l){
var n,r="";
for(n=0;n<a;n++){r+=T(t[n])?t[n].call(l,e):t[n];}
return r;
};
}(l),N[l](e)):e.localeData().invalidDate();
}
function F(e,l){
var a=5;
function t(e){
return l.longDateFormat(e)||e;
}
for(D.lastIndex=0;a>=0&&D.test(e);){e=e.replace(D,t),D.lastIndex=0,
a-=1;}
return e;
}
var U={};
function V(e,l){
var a=e.toLowerCase();
U[a]=U[a+"s"]=U[l]=e;
}
function H(e){
return"string"==typeof e?U[e]||U[e.toLowerCase()]:void 0;
}
function G(e){
var l,a,t={};
for(a in e){i(e,a)&&(l=H(a))&&(t[l]=e[a]);}
return t;
}
var J={};
function W(e,l){
J[e]=l;
}
function z(e){
return e%4==0&&e%100!=0||e%400==0;
}
function B(e){
return e<0?Math.ceil(e)||0:Math.floor(e);
}
function Z(e){
var l=+e,a=0;
return 0!==l&&isFinite(l)&&(a=B(l)),a;
}
function X(e,l){
return function(a){
return null!=a?(K(this,e,a),t.updateOffset(this,l),this):Q(this,e);
};
}
function Q(e,l){
return e.isValid()?e._d["get"+(e._isUTC?"UTC":"")+l]():NaN;
}
function K(e,l,a){
e.isValid()&&!isNaN(a)&&("FullYear"===l&&z(e.year())&&1===e.month()&&29===e.date()?(a=Z(a),
e._d["set"+(e._isUTC?"UTC":"")+l](a,e.month(),Oe(a,e.month()))):e._d["set"+(e._isUTC?"UTC":"")+l](a));
}
var q,$=/\d/,ee=/\d\d/,le=/\d{3}/,ae=/\d{4}/,te=/[+-]?\d{6}/,ne=/\d\d?/,re=/\d\d\d\d?/,ue=/\d\d\d\d\d\d?/,ie=/\d{1,3}/,oe=/\d{1,4}/,se=/[+-]?\d{1,6}/,ce=/\d+/,ve=/[+-]?\d+/,be=/Z|[+-]\d\d:?\d\d/gi,fe=/Z|[+-]\d\d(?::?\d\d)?/gi,he=/[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;
function de(e,l,a){
q[e]=T(l)?l:function(e,t){
return e&&a?a:l;
};
}
function pe(e,l){
return i(q,e)?q[e](l._strict,l._locale):new RegExp(function(e){
return ge(e.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(e,l,a,t,n){
return l||a||t||n;
}));
}(e));
}
function ge(e){
return e.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&");
}
q={};
var me,ye={};
function we(e,l){
var a,t=l;
for("string"==typeof e&&(e=[e]),c(l)&&(t=function t(e,a){
a[l]=Z(e);
}),a=0;a<e.length;a++){ye[e[a]]=t;}
}
function _e(e,l){
we(e,function(e,a,t,n){
t._w=t._w||{},l(e,t._w,t,n);
});
}
function Se(e,l,a){
null!=l&&i(ye,e)&&ye[e](l,a._a,a,e);
}
function Oe(e,l){
if(isNaN(e)||isNaN(l))return NaN;
var a=function(e,l){
return(e%12+12)%12;
}(l);
return e+=(l-a)/12,1===a?z(e)?29:28:31-a%7%2;
}
me=Array.prototype.indexOf?Array.prototype.indexOf:function(e){
var l;
for(l=0;l<this.length;++l){if(this[l]===e)return l;}
return-1;
},I("M",["MM",2],"Mo",function(){
return this.month()+1;
}),I("MMM",0,0,function(e){
return this.localeData().monthsShort(this,e);
}),I("MMMM",0,0,function(e){
return this.localeData().months(this,e);
}),V("month","M"),W("month",8),de("M",ne),de("MM",ne,ee),de("MMM",function(e,l){
return l.monthsShortRegex(e);
}),de("MMMM",function(e,l){
return l.monthsRegex(e);
}),we(["M","MM"],function(e,l){
l[1]=Z(e)-1;
}),we(["MMM","MMMM"],function(e,l,a,t){
var n=a._locale.monthsParse(e,t,a._strict);
null!=n?l[1]=n:d(a).invalidMonth=e;
});
var xe="January_February_March_April_May_June_July_August_September_October_November_December".split("_"),ke="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),Ae=/D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,Pe=he,Te=he;
function Me(e,l,a){
var t,n,r,u=e.toLocaleLowerCase();
if(!this._monthsParse)for(this._monthsParse=[],this._longMonthsParse=[],
this._shortMonthsParse=[],t=0;t<12;++t){r=h([2e3,t]),this._shortMonthsParse[t]=this.monthsShort(r,"").toLocaleLowerCase(),
this._longMonthsParse[t]=this.months(r,"").toLocaleLowerCase();}
return a?"MMM"===l?-1!==(n=me.call(this._shortMonthsParse,u))?n:null:-1!==(n=me.call(this._longMonthsParse,u))?n:null:"MMM"===l?-1!==(n=me.call(this._shortMonthsParse,u))||-1!==(n=me.call(this._longMonthsParse,u))?n:null:-1!==(n=me.call(this._longMonthsParse,u))||-1!==(n=me.call(this._shortMonthsParse,u))?n:null;
}
function Ee(e,l){
var a;
if(!e.isValid())return e;
if("string"==typeof l)if(/^\d+$/.test(l))l=Z(l);else if(!c(l=e.localeData().monthsParse(l)))return e;
return a=Math.min(e.date(),Oe(e.year(),l)),e._d["set"+(e._isUTC?"UTC":"")+"Month"](l,a),
e;
}
function Ce(e){
return null!=e?(Ee(this,e),t.updateOffset(this,!0),this):Q(this,"Month");
}
function je(){
function e(e,l){
return l.length-e.length;
}
var l,a,t=[],n=[],r=[];
for(l=0;l<12;l++){a=h([2e3,l]),t.push(this.monthsShort(a,"")),n.push(this.months(a,"")),
r.push(this.months(a,"")),r.push(this.monthsShort(a,""));}
for(t.sort(e),n.sort(e),r.sort(e),l=0;l<12;l++){t[l]=ge(t[l]),n[l]=ge(n[l]);}
for(l=0;l<24;l++){r[l]=ge(r[l]);}
this._monthsRegex=new RegExp("^("+r.join("|")+")","i"),this._monthsShortRegex=this._monthsRegex,
this._monthsStrictRegex=new RegExp("^("+n.join("|")+")","i"),this._monthsShortStrictRegex=new RegExp("^("+t.join("|")+")","i");
}
function De(e){
return z(e)?366:365;
}
I("Y",0,0,function(){
var e=this.year();
return e<=9999?C(e,4):"+"+e;
}),I(0,["YY",2],0,function(){
return this.year()%100;
}),I(0,["YYYY",4],0,"year"),I(0,["YYYYY",5],0,"year"),I(0,["YYYYYY",6,!0],0,"year"),
V("year","y"),W("year",1),de("Y",ve),de("YY",ne,ee),de("YYYY",oe,ae),
de("YYYYY",se,te),de("YYYYYY",se,te),we(["YYYYY","YYYYYY"],0),we("YYYY",function(e,l){
l[0]=2===e.length?t.parseTwoDigitYear(e):Z(e);
}),we("YY",function(e,l){
l[0]=t.parseTwoDigitYear(e);
}),we("Y",function(e,l){
l[0]=parseInt(e,10);
}),t.parseTwoDigitYear=function(e){
return Z(e)+(Z(e)>68?1900:2e3);
};
var Ne=X("FullYear",!0);
function Le(e,l,a,t,n,r,u){
var i;
return e<100&&e>=0?(i=new Date(e+400,l,a,t,n,r,u),isFinite(i.getFullYear())&&i.setFullYear(e)):i=new Date(e,l,a,t,n,r,u),
i;
}
function Ie(e){
var l,a;
return e<100&&e>=0?((a=Array.prototype.slice.call(arguments))[0]=e+400,
l=new Date(Date.UTC.apply(null,a)),isFinite(l.getUTCFullYear())&&l.setUTCFullYear(e)):l=new Date(Date.UTC.apply(null,arguments)),
l;
}
function Ye(e,l,a){
var t=7+l-a;
return-(7+Ie(e,0,t).getUTCDay()-l)%7+t-1;
}
function Re(e,l,a,t,n){
var r,u,i=1+7*(l-1)+(7+a-t)%7+Ye(e,t,n);
return i<=0?u=De(r=e-1)+i:i>De(e)?(r=e+1,u=i-De(e)):(r=e,
u=i),{
year:r,
dayOfYear:u};

}
function Fe(e,l,a){
var t,n,r=Ye(e.year(),l,a),u=Math.floor((e.dayOfYear()-r-1)/7)+1;
return u<1?t=u+Ue(n=e.year()-1,l,a):u>Ue(e.year(),l,a)?(t=u-Ue(e.year(),l,a),
n=e.year()+1):(n=e.year(),t=u),{
week:t,
year:n};

}
function Ue(e,l,a){
var t=Ye(e,l,a),n=Ye(e+1,l,a);
return(De(e)-t+n)/7;
}
function Ve(e,l){
return e.slice(l,7).concat(e.slice(0,l));
}
I("w",["ww",2],"wo","week"),I("W",["WW",2],"Wo","isoWeek"),V("week","w"),
V("isoWeek","W"),W("week",5),W("isoWeek",5),de("w",ne),de("ww",ne,ee),
de("W",ne),de("WW",ne,ee),_e(["w","ww","W","WW"],function(e,l,a,t){
l[t.substr(0,1)]=Z(e);
}),I("d",0,"do","day"),I("dd",0,0,function(e){
return this.localeData().weekdaysMin(this,e);
}),I("ddd",0,0,function(e){
return this.localeData().weekdaysShort(this,e);
}),I("dddd",0,0,function(e){
return this.localeData().weekdays(this,e);
}),I("e",0,0,"weekday"),I("E",0,0,"isoWeekday"),V("day","d"),V("weekday","e"),
V("isoWeekday","E"),W("day",11),W("weekday",11),W("isoWeekday",11),de("d",ne),
de("e",ne),de("E",ne),de("dd",function(e,l){
return l.weekdaysMinRegex(e);
}),de("ddd",function(e,l){
return l.weekdaysShortRegex(e);
}),de("dddd",function(e,l){
return l.weekdaysRegex(e);
}),_e(["dd","ddd","dddd"],function(e,l,a,t){
var n=a._locale.weekdaysParse(e,t,a._strict);
null!=n?l.d=n:d(a).invalidWeekday=e;
}),_e(["d","e","E"],function(e,l,a,t){
l[t]=Z(e);
});
var He="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),Ge="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),Je="Su_Mo_Tu_We_Th_Fr_Sa".split("_"),We=he,ze=he,Be=he;
function Ze(e,l,a){
var t,n,r,u=e.toLocaleLowerCase();
if(!this._weekdaysParse)for(this._weekdaysParse=[],this._shortWeekdaysParse=[],
this._minWeekdaysParse=[],t=0;t<7;++t){r=h([2e3,1]).day(t),this._minWeekdaysParse[t]=this.weekdaysMin(r,"").toLocaleLowerCase(),
this._shortWeekdaysParse[t]=this.weekdaysShort(r,"").toLocaleLowerCase(),this._weekdaysParse[t]=this.weekdays(r,"").toLocaleLowerCase();}
return a?"dddd"===l?-1!==(n=me.call(this._weekdaysParse,u))?n:null:"ddd"===l?-1!==(n=me.call(this._shortWeekdaysParse,u))?n:null:-1!==(n=me.call(this._minWeekdaysParse,u))?n:null:"dddd"===l?-1!==(n=me.call(this._weekdaysParse,u))||-1!==(n=me.call(this._shortWeekdaysParse,u))||-1!==(n=me.call(this._minWeekdaysParse,u))?n:null:"ddd"===l?-1!==(n=me.call(this._shortWeekdaysParse,u))||-1!==(n=me.call(this._weekdaysParse,u))||-1!==(n=me.call(this._minWeekdaysParse,u))?n:null:-1!==(n=me.call(this._minWeekdaysParse,u))||-1!==(n=me.call(this._weekdaysParse,u))||-1!==(n=me.call(this._shortWeekdaysParse,u))?n:null;
}
function Xe(){
function e(e,l){
return l.length-e.length;
}
var l,a,t,n,r,u=[],i=[],o=[],s=[];
for(l=0;l<7;l++){a=h([2e3,1]).day(l),t=ge(this.weekdaysMin(a,"")),
n=ge(this.weekdaysShort(a,"")),r=ge(this.weekdays(a,"")),u.push(t),i.push(n),
o.push(r),s.push(t),s.push(n),s.push(r);}
u.sort(e),i.sort(e),o.sort(e),s.sort(e),this._weekdaysRegex=new RegExp("^("+s.join("|")+")","i"),
this._weekdaysShortRegex=this._weekdaysRegex,this._weekdaysMinRegex=this._weekdaysRegex,
this._weekdaysStrictRegex=new RegExp("^("+o.join("|")+")","i"),this._weekdaysShortStrictRegex=new RegExp("^("+i.join("|")+")","i"),
this._weekdaysMinStrictRegex=new RegExp("^("+u.join("|")+")","i");
}
function Qe(){
return this.hours()%12||12;
}
function Ke(e,l){
I(e,0,0,function(){
return this.localeData().meridiem(this.hours(),this.minutes(),l);
});
}
function qe(e,l){
return l._meridiemParse;
}
I("H",["HH",2],0,"hour"),I("h",["hh",2],0,Qe),I("k",["kk",2],0,function(){
return this.hours()||24;
}),I("hmm",0,0,function(){
return""+Qe.apply(this)+C(this.minutes(),2);
}),I("hmmss",0,0,function(){
return""+Qe.apply(this)+C(this.minutes(),2)+C(this.seconds(),2);
}),I("Hmm",0,0,function(){
return""+this.hours()+C(this.minutes(),2);
}),I("Hmmss",0,0,function(){
return""+this.hours()+C(this.minutes(),2)+C(this.seconds(),2);
}),Ke("a",!0),Ke("A",!1),V("hour","h"),W("hour",13),de("a",qe),de("A",qe),
de("H",ne),de("h",ne),de("k",ne),de("HH",ne,ee),de("hh",ne,ee),de("kk",ne,ee),
de("hmm",re),de("hmmss",ue),de("Hmm",re),de("Hmmss",ue),we(["H","HH"],3),
we(["k","kk"],function(e,l,a){
var t=Z(e);
l[3]=24===t?0:t;
}),we(["a","A"],function(e,l,a){
a._isPm=a._locale.isPM(e),a._meridiem=e;
}),we(["h","hh"],function(e,l,a){
l[3]=Z(e),d(a).bigHour=!0;
}),we("hmm",function(e,l,a){
var t=e.length-2;
l[3]=Z(e.substr(0,t)),l[4]=Z(e.substr(t)),d(a).bigHour=!0;
}),we("hmmss",function(e,l,a){
var t=e.length-4,n=e.length-2;
l[3]=Z(e.substr(0,t)),l[4]=Z(e.substr(t,2)),l[5]=Z(e.substr(n)),d(a).bigHour=!0;
}),we("Hmm",function(e,l,a){
var t=e.length-2;
l[3]=Z(e.substr(0,t)),l[4]=Z(e.substr(t));
}),we("Hmmss",function(e,l,a){
var t=e.length-4,n=e.length-2;
l[3]=Z(e.substr(0,t)),l[4]=Z(e.substr(t,2)),l[5]=Z(e.substr(n));
});
var $e,el=X("Hours",!0),ll={
calendar:{
sameDay:"[Today at] LT",
nextDay:"[Tomorrow at] LT",
nextWeek:"dddd [at] LT",
lastDay:"[Yesterday at] LT",
lastWeek:"[Last] dddd [at] LT",
sameElse:"L"},

longDateFormat:{
LTS:"h:mm:ss A",
LT:"h:mm A",
L:"MM/DD/YYYY",
LL:"MMMM D, YYYY",
LLL:"MMMM D, YYYY h:mm A",
LLLL:"dddd, MMMM D, YYYY h:mm A"},

invalidDate:"Invalid date",
ordinal:"%d",
dayOfMonthOrdinalParse:/\d{1,2}/,
relativeTime:{
future:"in %s",
past:"%s ago",
s:"a few seconds",
ss:"%d seconds",
m:"a minute",
mm:"%d minutes",
h:"an hour",
hh:"%d hours",
d:"a day",
dd:"%d days",
w:"a week",
ww:"%d weeks",
M:"a month",
MM:"%d months",
y:"a year",
yy:"%d years"},

months:xe,
monthsShort:ke,
week:{
dow:0,
doy:6},

weekdays:He,
weekdaysMin:Je,
weekdaysShort:Ge,
meridiemParse:/[ap]\.?m?\.?/i},
al={},tl={};
function nl(e,l){
var a,t=Math.min(e.length,l.length);
for(a=0;a<t;a+=1){if(e[a]!==l[a])return a;}
return t;
}
function rl(e){
return e?e.toLowerCase().replace("_","-"):e;
}
function ul(l){
var a=null;
if(void 0===al[l]&&void 0!==e&&e&&e.exports)try{
a=$e._abbr,function(){
var e=new Error("Cannot find module 'undefined'");
throw e.code="MODULE_NOT_FOUND",e;
}(),il(a);
}catch(e){
al[l]=null;
}
return al[l];
}
function il(e,l){
var a;
return e&&((a=s(l)?sl(e):ol(e,l))?$e=a:"undefined"!=typeof console&&console.warn&&console.warn("Locale "+e+" not found. Did you forget to load it?")),
$e._abbr;
}
function ol(e,l){
if(null!==l){
var a,t=ll;
if(l.abbr=e,null!=al[e])P("defineLocaleOverride","use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."),
t=al[e]._config;else if(null!=l.parentLocale)if(null!=al[l.parentLocale])t=al[l.parentLocale]._config;else{
if(null==(a=ul(l.parentLocale)))return tl[l.parentLocale]||(tl[l.parentLocale]=[]),
tl[l.parentLocale].push({
name:e,
config:l}),
null;
t=a._config;
}
return al[e]=new E(M(t,l)),tl[e]&&tl[e].forEach(function(e){
ol(e.name,e.config);
}),il(e),al[e];
}
return delete al[e],null;
}
function sl(e){
var l;
if(e&&e._locale&&e._locale._abbr&&(e=e._locale._abbr),!e)return $e;
if(!n(e)){
if(l=ul(e))return l;
e=[e];
}
return function(e){
for(var l,a,t,n,r=0;r<e.length;){
for(l=(n=rl(e[r]).split("-")).length,a=(a=rl(e[r+1]))?a.split("-"):null;l>0;){
if(t=ul(n.slice(0,l).join("-")))return t;
if(a&&a.length>=l&&nl(n,a)>=l-1)break;
l--;
}
r++;
}
return $e;
}(e);
}
function cl(e){
var l,a=e._a;
return a&&-2===d(e).overflow&&(l=a[1]<0||a[1]>11?1:a[2]<1||a[2]>Oe(a[0],a[1])?2:a[3]<0||a[3]>24||24===a[3]&&(0!==a[4]||0!==a[5]||0!==a[6])?3:a[4]<0||a[4]>59?4:a[5]<0||a[5]>59?5:a[6]<0||a[6]>999?6:-1,
d(e)._overflowDayOfYear&&(l<0||l>2)&&(l=2),d(e)._overflowWeeks&&-1===l&&(l=7),
d(e)._overflowWeekday&&-1===l&&(l=8),d(e).overflow=l),e;
}
var vl=/^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/,bl=/^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d|))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([+-]\d\d(?::?\d\d)?|\s*Z)?)?$/,fl=/Z|[+-]\d\d(?::?\d\d)?/,hl=[["YYYYYY-MM-DD",/[+-]\d{6}-\d\d-\d\d/],["YYYY-MM-DD",/\d{4}-\d\d-\d\d/],["GGGG-[W]WW-E",/\d{4}-W\d\d-\d/],["GGGG-[W]WW",/\d{4}-W\d\d/,!1],["YYYY-DDD",/\d{4}-\d{3}/],["YYYY-MM",/\d{4}-\d\d/,!1],["YYYYYYMMDD",/[+-]\d{10}/],["YYYYMMDD",/\d{8}/],["GGGG[W]WWE",/\d{4}W\d{3}/],["GGGG[W]WW",/\d{4}W\d{2}/,!1],["YYYYDDD",/\d{7}/],["YYYYMM",/\d{6}/,!1],["YYYY",/\d{4}/,!1]],dl=[["HH:mm:ss.SSSS",/\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss,SSSS",/\d\d:\d\d:\d\d,\d+/],["HH:mm:ss",/\d\d:\d\d:\d\d/],["HH:mm",/\d\d:\d\d/],["HHmmss.SSSS",/\d\d\d\d\d\d\.\d+/],["HHmmss,SSSS",/\d\d\d\d\d\d,\d+/],["HHmmss",/\d\d\d\d\d\d/],["HHmm",/\d\d\d\d/],["HH",/\d\d/]],pl=/^\/?Date\((-?\d+)/i,gl=/^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/,ml={
UT:0,
GMT:0,
EDT:-240,
EST:-300,
CDT:-300,
CST:-360,
MDT:-360,
MST:-420,
PDT:-420,
PST:-480};

function yl(e){
var l,a,t,n,r,u,i=e._i,o=vl.exec(i)||bl.exec(i);
if(o){
for(d(e).iso=!0,l=0,a=hl.length;l<a;l++){if(hl[l][1].exec(o[1])){
n=hl[l][0],t=!1!==hl[l][2];
break;
}}
if(null==n)return void(e._isValid=!1);
if(o[3]){
for(l=0,a=dl.length;l<a;l++){if(dl[l][1].exec(o[3])){
r=(o[2]||" ")+dl[l][0];
break;
}}
if(null==r)return void(e._isValid=!1);
}
if(!t&&null!=r)return void(e._isValid=!1);
if(o[4]){
if(!fl.exec(o[4]))return void(e._isValid=!1);
u="Z";
}
e._f=n+(r||"")+(u||""),xl(e);
}else e._isValid=!1;
}
function wl(e){
var l=parseInt(e,10);
return l<=49?2e3+l:l<=999?1900+l:l;
}
function _l(e){
var l,a=gl.exec(function(e){
return e.replace(/\([^)]*\)|[\n\t]/g," ").replace(/(\s\s+)/g," ").replace(/^\s\s*/,"").replace(/\s\s*$/,"");
}(e._i));
if(a){
if(l=function(e,l,a,t,n,r){
var u=[wl(e),ke.indexOf(l),parseInt(a,10),parseInt(t,10),parseInt(n,10)];
return r&&u.push(parseInt(r,10)),u;
}(a[4],a[3],a[2],a[5],a[6],a[7]),!function(e,l,a){
return!e||Ge.indexOf(e)===new Date(l[0],l[1],l[2]).getDay()||(d(a).weekdayMismatch=!0,
a._isValid=!1,!1);
}(a[1],l,e))return;
e._a=l,e._tzm=function(e,l,a){
if(e)return ml[e];
if(l)return 0;
var t=parseInt(a,10),n=t%100;
return(t-n)/100*60+n;
}(a[8],a[9],a[10]),e._d=Ie.apply(null,e._a),e._d.setUTCMinutes(e._d.getUTCMinutes()-e._tzm),
d(e).rfc2822=!0;
}else e._isValid=!1;
}
function Sl(e,l,a){
return null!=e?e:null!=l?l:a;
}
function Ol(e){
var l,a,n,r,u,i=[];
if(!e._d){
for(n=function(e){
var l=new Date(t.now());
return e._useUTC?[l.getUTCFullYear(),l.getUTCMonth(),l.getUTCDate()]:[l.getFullYear(),l.getMonth(),l.getDate()];
}(e),e._w&&null==e._a[2]&&null==e._a[1]&&function(e){
var l,a,t,n,r,u,i,o,s;
null!=(l=e._w).GG||null!=l.W||null!=l.E?(r=1,u=4,a=Sl(l.GG,e._a[0],Fe(Pl(),1,4).year),
t=Sl(l.W,1),((n=Sl(l.E,1))<1||n>7)&&(o=!0)):(r=e._locale._week.dow,
u=e._locale._week.doy,s=Fe(Pl(),r,u),a=Sl(l.gg,e._a[0],s.year),t=Sl(l.w,s.week),
null!=l.d?((n=l.d)<0||n>6)&&(o=!0):null!=l.e?(n=l.e+r,
(l.e<0||l.e>6)&&(o=!0)):n=r),t<1||t>Ue(a,r,u)?d(e)._overflowWeeks=!0:null!=o?d(e)._overflowWeekday=!0:(i=Re(a,t,n,r,u),
e._a[0]=i.year,e._dayOfYear=i.dayOfYear);
}(e),null!=e._dayOfYear&&(u=Sl(e._a[0],n[0]),(e._dayOfYear>De(u)||0===e._dayOfYear)&&(d(e)._overflowDayOfYear=!0),
a=Ie(u,0,e._dayOfYear),e._a[1]=a.getUTCMonth(),e._a[2]=a.getUTCDate()),
l=0;l<3&&null==e._a[l];++l){e._a[l]=i[l]=n[l];}
for(;l<7;l++){e._a[l]=i[l]=null==e._a[l]?2===l?1:0:e._a[l];}
24===e._a[3]&&0===e._a[4]&&0===e._a[5]&&0===e._a[6]&&(e._nextDay=!0,
e._a[3]=0),e._d=(e._useUTC?Ie:Le).apply(null,i),r=e._useUTC?e._d.getUTCDay():e._d.getDay(),
null!=e._tzm&&e._d.setUTCMinutes(e._d.getUTCMinutes()-e._tzm),e._nextDay&&(e._a[3]=24),
e._w&&void 0!==e._w.d&&e._w.d!==r&&(d(e).weekdayMismatch=!0);
}
}
function xl(e){
if(e._f!==t.ISO_8601){if(e._f!==t.RFC_2822){
e._a=[],d(e).empty=!0;
var l,a,n,r,u,i,o=""+e._i,s=o.length,c=0;
for(n=F(e._f,e._locale).match(j)||[],l=0;l<n.length;l++){r=n[l],
(a=(o.match(pe(r,e))||[])[0])&&((u=o.substr(0,o.indexOf(a))).length>0&&d(e).unusedInput.push(u),
o=o.slice(o.indexOf(a)+a.length),c+=a.length),L[r]?(a?d(e).empty=!1:d(e).unusedTokens.push(r),
Se(r,a,e)):e._strict&&!a&&d(e).unusedTokens.push(r);}
d(e).charsLeftOver=s-c,o.length>0&&d(e).unusedInput.push(o),e._a[3]<=12&&!0===d(e).bigHour&&e._a[3]>0&&(d(e).bigHour=void 0),
d(e).parsedDateParts=e._a.slice(0),d(e).meridiem=e._meridiem,e._a[3]=function(e,l,a){
var t;
return null==a?l:null!=e.meridiemHour?e.meridiemHour(l,a):null!=e.isPM?((t=e.isPM(a))&&l<12&&(l+=12),
t||12!==l||(l=0),l):l;
}(e._locale,e._a[3],e._meridiem),null!==(i=d(e).era)&&(e._a[0]=e._locale.erasConvertYear(i,e._a[0])),
Ol(e),cl(e);
}else _l(e);}else yl(e);
}
function kl(e){
var l=e._i,a=e._f;
return e._locale=e._locale||sl(e._l),null===l||void 0===a&&""===l?g({
nullInput:!0}):(
"string"==typeof l&&(e._i=l=e._locale.preparse(l)),S(l)?new _(cl(l)):(v(l)?e._d=l:n(a)?function(e){
var l,a,t,n,r,u,i=!1;
if(0===e._f.length)return d(e).invalidFormat=!0,void(e._d=new Date(NaN));
for(n=0;n<e._f.length;n++){r=0,u=!1,l=w({},e),null!=e._useUTC&&(l._useUTC=e._useUTC),
l._f=e._f[n],xl(l),p(l)&&(u=!0),r+=d(l).charsLeftOver,r+=10*d(l).unusedTokens.length,
d(l).score=r,i?r<t&&(t=r,a=l):(null==t||r<t||u)&&(t=r,
a=l,u&&(i=!0));}
f(e,a||l);
}(e):a?xl(e):function(e){
var l=e._i;
s(l)?e._d=new Date(t.now()):v(l)?e._d=new Date(l.valueOf()):"string"==typeof l?function(e){
var l=pl.exec(e._i);
null===l?(yl(e),!1===e._isValid&&(delete e._isValid,_l(e),!1===e._isValid&&(delete e._isValid,
e._strict?e._isValid=!1:t.createFromInputFallback(e)))):e._d=new Date(+l[1]);
}(e):n(l)?(e._a=b(l.slice(0),function(e){
return parseInt(e,10);
}),Ol(e)):u(l)?function(e){
if(!e._d){
var l=G(e._i),a=void 0===l.day?l.date:l.day;
e._a=b([l.year,l.month,a,l.hour,l.minute,l.second,l.millisecond],function(e){
return e&&parseInt(e,10);
}),Ol(e);
}
}(e):c(l)?e._d=new Date(l):t.createFromInputFallback(e);
}(e),p(e)||(e._d=null),e));
}
function Al(e,l,a,t,r){
var i={};
return!0!==l&&!1!==l||(t=l,l=void 0),!0!==a&&!1!==a||(t=a,
a=void 0),(u(e)&&o(e)||n(e)&&0===e.length)&&(e=void 0),i._isAMomentObject=!0,
i._useUTC=i._isUTC=r,i._l=a,i._i=e,i._f=l,i._strict=t,function(e){
var l=new _(cl(kl(e)));
return l._nextDay&&(l.add(1,"d"),l._nextDay=void 0),l;
}(i);
}
function Pl(e,l,a,t){
return Al(e,l,a,t,!1);
}
t.createFromInputFallback=x("value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.",function(e){
e._d=new Date(e._i+(e._useUTC?" UTC":""));
}),t.ISO_8601=function(){},t.RFC_2822=function(){};
var Tl=x("moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/",function(){
var e=Pl.apply(null,arguments);
return this.isValid()&&e.isValid()?e<this?this:e:g();
}),Ml=x("moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/",function(){
var e=Pl.apply(null,arguments);
return this.isValid()&&e.isValid()?e>this?this:e:g();
});
function El(e,l){
var a,t;
if(1===l.length&&n(l[0])&&(l=l[0]),!l.length)return Pl();
for(a=l[0],t=1;t<l.length;++t){l[t].isValid()&&!l[t][e](a)||(a=l[t]);}
return a;
}
var Cl=["year","quarter","month","week","day","hour","minute","second","millisecond"];
function jl(e){
var l=G(e),a=l.year||0,t=l.quarter||0,n=l.month||0,r=l.week||l.isoWeek||0,u=l.day||0,o=l.hour||0,s=l.minute||0,c=l.second||0,v=l.millisecond||0;
this._isValid=function(e){
var l,a,t=!1;
for(l in e){if(i(e,l)&&(-1===me.call(Cl,l)||null!=e[l]&&isNaN(e[l])))return!1;}
for(a=0;a<Cl.length;++a){if(e[Cl[a]]){
if(t)return!1;
parseFloat(e[Cl[a]])!==Z(e[Cl[a]])&&(t=!0);
}}
return!0;
}(l),this._milliseconds=+v+1e3*c+6e4*s+1e3*o*60*60,this._days=+u+7*r,
this._months=+n+3*t+12*a,this._data={},this._locale=sl(),this._bubble();
}
function Dl(e){
return e instanceof jl;
}
function Nl(e){
return e<0?-1*Math.round(-1*e):Math.round(e);
}
function Ll(e,l){
I(e,0,0,function(){
var e=this.utcOffset(),a="+";
return e<0&&(e=-e,a="-"),a+C(~~(e/60),2)+l+C(~~e%60,2);
});
}
Ll("Z",":"),Ll("ZZ",""),de("Z",fe),de("ZZ",fe),we(["Z","ZZ"],function(e,l,a){
a._useUTC=!0,a._tzm=Yl(fe,e);
});
var Il=/([\+\-]|\d\d)/gi;
function Yl(e,l){
var a,t,n=(l||"").match(e);
return null===n?null:0===(t=60*(a=((n[n.length-1]||[])+"").match(Il)||["-",0,0])[1]+Z(a[2]))?0:"+"===a[0]?t:-t;
}
function Rl(e,l){
var a,n;
return l._isUTC?(a=l.clone(),n=(S(e)||v(e)?e.valueOf():Pl(e).valueOf())-a.valueOf(),
a._d.setTime(a._d.valueOf()+n),t.updateOffset(a,!1),a):Pl(e).local();
}
function Fl(e){
return-Math.round(e._d.getTimezoneOffset());
}
function Ul(){
return!!this.isValid()&&this._isUTC&&0===this._offset;
}
t.updateOffset=function(){};
var Vl=/^(-|\+)?(?:(\d*)[. ])?(\d+):(\d+)(?::(\d+)(\.\d*)?)?$/,Hl=/^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;
function Gl(e,l){
var a,t,n,u=e,o=null;
return Dl(e)?u={
ms:e._milliseconds,
d:e._days,
M:e._months}:
c(e)||!isNaN(+e)?(u={},l?u[l]=+e:u.milliseconds=+e):(o=Vl.exec(e))?(a="-"===o[1]?-1:1,
u={
y:0,
d:Z(o[2])*a,
h:Z(o[3])*a,
m:Z(o[4])*a,
s:Z(o[5])*a,
ms:Z(Nl(1e3*o[6]))*a}):
(o=Hl.exec(e))?(a="-"===o[1]?-1:1,u={
y:Jl(o[2],a),
M:Jl(o[3],a),
w:Jl(o[4],a),
d:Jl(o[5],a),
h:Jl(o[6],a),
m:Jl(o[7],a),
s:Jl(o[8],a)}):
null==u?u={}:"object"===r(u)&&("from"in u||"to"in u)&&(n=function(e,l){
var a;
return e.isValid()&&l.isValid()?(l=Rl(l,e),e.isBefore(l)?a=Wl(e,l):((a=Wl(l,e)).milliseconds=-a.milliseconds,
a.months=-a.months),a):{
milliseconds:0,
months:0};

}(Pl(u.from),Pl(u.to)),(u={}).ms=n.milliseconds,u.M=n.months),t=new jl(u),
Dl(e)&&i(e,"_locale")&&(t._locale=e._locale),Dl(e)&&i(e,"_isValid")&&(t._isValid=e._isValid),
t;
}
function Jl(e,l){
var a=e&&parseFloat(e.replace(",","."));
return(isNaN(a)?0:a)*l;
}
function Wl(e,l){
var a={};
return a.months=l.month()-e.month()+12*(l.year()-e.year()),e.clone().add(a.months,"M").isAfter(l)&&--a.months,
a.milliseconds=+l-+e.clone().add(a.months,"M"),a;
}
function zl(e,l){
return function(a,t){
var n;
return null===t||isNaN(+t)||(P(l,"moment()."+l+"(period, number) is deprecated. Please use moment()."+l+"(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."),
n=a,a=t,t=n),Bl(this,Gl(a,t),e),this;
};
}
function Bl(e,l,a,n){
var r=l._milliseconds,u=Nl(l._days),i=Nl(l._months);
e.isValid()&&(n=null==n||n,i&&Ee(e,Q(e,"Month")+i*a),u&&K(e,"Date",Q(e,"Date")+u*a),
r&&e._d.setTime(e._d.valueOf()+r*a),n&&t.updateOffset(e,u||i));
}
Gl.fn=jl.prototype,Gl.invalid=function(){
return Gl(NaN);
};
var Zl=zl(1,"add"),Xl=zl(-1,"subtract");
function Ql(e){
return"string"==typeof e||e instanceof String;
}
function Kl(e){
return S(e)||v(e)||Ql(e)||c(e)||function(e){
var l=n(e),a=!1;
return l&&(a=0===e.filter(function(l){
return!c(l)&&Ql(e);
}).length),l&&a;
}(e)||function(e){
var l,a=u(e)&&!o(e),t=!1,n=["years","year","y","months","month","M","days","day","d","dates","date","D","hours","hour","h","minutes","minute","m","seconds","second","s","milliseconds","millisecond","ms"];
for(l=0;l<n.length;l+=1){t=t||i(e,n[l]);}
return a&&t;
}(e)||null==e;
}
function ql(e){
var l,a=u(e)&&!o(e),t=!1,n=["sameDay","nextDay","lastDay","nextWeek","lastWeek","sameElse"];
for(l=0;l<n.length;l+=1){t=t||i(e,n[l]);}
return a&&t;
}
function $l(e,l){
if(e.date()<l.date())return-$l(l,e);
var a,t=12*(l.year()-e.year())+(l.month()-e.month()),n=e.clone().add(t,"months");
return a=l-n<0?(l-n)/(n-e.clone().add(t-1,"months")):(l-n)/(e.clone().add(t+1,"months")-n),
-(t+a)||0;
}
function ea(e){
var l;
return void 0===e?this._locale._abbr:(null!=(l=sl(e))&&(this._locale=l),
this);
}
t.defaultFormat="YYYY-MM-DDTHH:mm:ssZ",t.defaultFormatUtc="YYYY-MM-DDTHH:mm:ss[Z]";
var la=x("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(e){
return void 0===e?this.localeData():this.locale(e);
});
function aa(){
return this._locale;
}
function ta(e,l){
return(e%l+l)%l;
}
function na(e,l,a){
return e<100&&e>=0?new Date(e+400,l,a)-126227808e5:new Date(e,l,a).valueOf();
}
function ra(e,l,a){
return e<100&&e>=0?Date.UTC(e+400,l,a)-126227808e5:Date.UTC(e,l,a);
}
function ua(e,l){
return l.erasAbbrRegex(e);
}
function ia(){
var e,l,a=[],t=[],n=[],r=[],u=this.eras();
for(e=0,l=u.length;e<l;++e){t.push(ge(u[e].name)),a.push(ge(u[e].abbr)),
n.push(ge(u[e].narrow)),r.push(ge(u[e].name)),r.push(ge(u[e].abbr)),r.push(ge(u[e].narrow));}
this._erasRegex=new RegExp("^("+r.join("|")+")","i"),this._erasNameRegex=new RegExp("^("+t.join("|")+")","i"),
this._erasAbbrRegex=new RegExp("^("+a.join("|")+")","i"),this._erasNarrowRegex=new RegExp("^("+n.join("|")+")","i");
}
function oa(e,l){
I(0,[e,e.length],0,l);
}
function sa(e,l,a,t,n){
var r;
return null==e?Fe(this,t,n).year:(l>(r=Ue(e,t,n))&&(l=r),ca.call(this,e,l,a,t,n));
}
function ca(e,l,a,t,n){
var r=Re(e,l,a,t,n),u=Ie(r.year,0,r.dayOfYear);
return this.year(u.getUTCFullYear()),this.month(u.getUTCMonth()),this.date(u.getUTCDate()),
this;
}
I("N",0,0,"eraAbbr"),I("NN",0,0,"eraAbbr"),I("NNN",0,0,"eraAbbr"),I("NNNN",0,0,"eraName"),
I("NNNNN",0,0,"eraNarrow"),I("y",["y",1],"yo","eraYear"),I("y",["yy",2],0,"eraYear"),
I("y",["yyy",3],0,"eraYear"),I("y",["yyyy",4],0,"eraYear"),de("N",ua),
de("NN",ua),de("NNN",ua),de("NNNN",function(e,l){
return l.erasNameRegex(e);
}),de("NNNNN",function(e,l){
return l.erasNarrowRegex(e);
}),we(["N","NN","NNN","NNNN","NNNNN"],function(e,l,a,t){
var n=a._locale.erasParse(e,t,a._strict);
n?d(a).era=n:d(a).invalidEra=e;
}),de("y",ce),de("yy",ce),de("yyy",ce),de("yyyy",ce),de("yo",function(e,l){
return l._eraYearOrdinalRegex||ce;
}),we(["y","yy","yyy","yyyy"],0),we(["yo"],function(e,l,a,t){
var n;
a._locale._eraYearOrdinalRegex&&(n=e.match(a._locale._eraYearOrdinalRegex)),
a._locale.eraYearOrdinalParse?l[0]=a._locale.eraYearOrdinalParse(e,n):l[0]=parseInt(e,10);
}),I(0,["gg",2],0,function(){
return this.weekYear()%100;
}),I(0,["GG",2],0,function(){
return this.isoWeekYear()%100;
}),oa("gggg","weekYear"),oa("ggggg","weekYear"),oa("GGGG","isoWeekYear"),
oa("GGGGG","isoWeekYear"),V("weekYear","gg"),V("isoWeekYear","GG"),W("weekYear",1),
W("isoWeekYear",1),de("G",ve),de("g",ve),de("GG",ne,ee),de("gg",ne,ee),
de("GGGG",oe,ae),de("gggg",oe,ae),de("GGGGG",se,te),de("ggggg",se,te),
_e(["gggg","ggggg","GGGG","GGGGG"],function(e,l,a,t){
l[t.substr(0,2)]=Z(e);
}),_e(["gg","GG"],function(e,l,a,n){
l[n]=t.parseTwoDigitYear(e);
}),I("Q",0,"Qo","quarter"),V("quarter","Q"),W("quarter",7),de("Q",$),
we("Q",function(e,l){
l[1]=3*(Z(e)-1);
}),I("D",["DD",2],"Do","date"),V("date","D"),W("date",9),de("D",ne),
de("DD",ne,ee),de("Do",function(e,l){
return e?l._dayOfMonthOrdinalParse||l._ordinalParse:l._dayOfMonthOrdinalParseLenient;
}),we(["D","DD"],2),we("Do",function(e,l){
l[2]=Z(e.match(ne)[0]);
});
var va=X("Date",!0);
I("DDD",["DDDD",3],"DDDo","dayOfYear"),V("dayOfYear","DDD"),W("dayOfYear",4),
de("DDD",ie),de("DDDD",le),we(["DDD","DDDD"],function(e,l,a){
a._dayOfYear=Z(e);
}),I("m",["mm",2],0,"minute"),V("minute","m"),W("minute",14),de("m",ne),
de("mm",ne,ee),we(["m","mm"],4);
var ba=X("Minutes",!1);
I("s",["ss",2],0,"second"),V("second","s"),W("second",15),de("s",ne),
de("ss",ne,ee),we(["s","ss"],5);
var fa,ha,da=X("Seconds",!1);
for(I("S",0,0,function(){
return~~(this.millisecond()/100);
}),I(0,["SS",2],0,function(){
return~~(this.millisecond()/10);
}),I(0,["SSS",3],0,"millisecond"),I(0,["SSSS",4],0,function(){
return 10*this.millisecond();
}),I(0,["SSSSS",5],0,function(){
return 100*this.millisecond();
}),I(0,["SSSSSS",6],0,function(){
return 1e3*this.millisecond();
}),I(0,["SSSSSSS",7],0,function(){
return 1e4*this.millisecond();
}),I(0,["SSSSSSSS",8],0,function(){
return 1e5*this.millisecond();
}),I(0,["SSSSSSSSS",9],0,function(){
return 1e6*this.millisecond();
}),V("millisecond","ms"),W("millisecond",16),de("S",ie,$),de("SS",ie,ee),
de("SSS",ie,le),fa="SSSS";fa.length<=9;fa+="S"){de(fa,ce);}
function pa(e,l){
l[6]=Z(1e3*("0."+e));
}
for(fa="S";fa.length<=9;fa+="S"){we(fa,pa);}
ha=X("Milliseconds",!1),I("z",0,0,"zoneAbbr"),I("zz",0,0,"zoneName");
var ga=_.prototype;
function ma(e){
return e;
}
ga.add=Zl,ga.calendar=function(e,l){
1===arguments.length&&(arguments[0]?Kl(arguments[0])?(e=arguments[0],
l=void 0):ql(arguments[0])&&(l=arguments[0],e=void 0):(e=void 0,
l=void 0));
var a=e||Pl(),n=Rl(a,this).startOf("day"),r=t.calendarFormat(this,n)||"sameElse",u=l&&(T(l[r])?l[r].call(this,a):l[r]);
return this.format(u||this.localeData().calendar(r,this,Pl(a)));
},ga.clone=function(){
return new _(this);
},ga.diff=function(e,l,a){
var t,n,r;
if(!this.isValid())return NaN;
if(!(t=Rl(e,this)).isValid())return NaN;
switch(n=6e4*(t.utcOffset()-this.utcOffset()),l=H(l)){
case"year":
r=$l(this,t)/12;
break;

case"month":
r=$l(this,t);
break;

case"quarter":
r=$l(this,t)/3;
break;

case"second":
r=(this-t)/1e3;
break;

case"minute":
r=(this-t)/6e4;
break;

case"hour":
r=(this-t)/36e5;
break;

case"day":
r=(this-t-n)/864e5;
break;

case"week":
r=(this-t-n)/6048e5;
break;

default:
r=this-t;}

return a?r:B(r);
},ga.endOf=function(e){
var l,a;
if(void 0===(e=H(e))||"millisecond"===e||!this.isValid())return this;
switch(a=this._isUTC?ra:na,e){
case"year":
l=a(this.year()+1,0,1)-1;
break;

case"quarter":
l=a(this.year(),this.month()-this.month()%3+3,1)-1;
break;

case"month":
l=a(this.year(),this.month()+1,1)-1;
break;

case"week":
l=a(this.year(),this.month(),this.date()-this.weekday()+7)-1;
break;

case"isoWeek":
l=a(this.year(),this.month(),this.date()-(this.isoWeekday()-1)+7)-1;
break;

case"day":
case"date":
l=a(this.year(),this.month(),this.date()+1)-1;
break;

case"hour":
l=this._d.valueOf(),l+=36e5-ta(l+(this._isUTC?0:6e4*this.utcOffset()),36e5)-1;
break;

case"minute":
l=this._d.valueOf(),l+=6e4-ta(l,6e4)-1;
break;

case"second":
l=this._d.valueOf(),l+=1e3-ta(l,1e3)-1;}

return this._d.setTime(l),t.updateOffset(this,!0),this;
},ga.format=function(e){
e||(e=this.isUtc()?t.defaultFormatUtc:t.defaultFormat);
var l=R(this,e);
return this.localeData().postformat(l);
},ga.from=function(e,l){
return this.isValid()&&(S(e)&&e.isValid()||Pl(e).isValid())?Gl({
to:this,
from:e}).
locale(this.locale()).humanize(!l):this.localeData().invalidDate();
},ga.fromNow=function(e){
return this.from(Pl(),e);
},ga.to=function(e,l){
return this.isValid()&&(S(e)&&e.isValid()||Pl(e).isValid())?Gl({
from:this,
to:e}).
locale(this.locale()).humanize(!l):this.localeData().invalidDate();
},ga.toNow=function(e){
return this.to(Pl(),e);
},ga.get=function(e){
return T(this[e=H(e)])?this[e]():this;
},ga.invalidAt=function(){
return d(this).overflow;
},ga.isAfter=function(e,l){
var a=S(e)?e:Pl(e);
return!(!this.isValid()||!a.isValid())&&("millisecond"===(l=H(l)||"millisecond")?this.valueOf()>a.valueOf():a.valueOf()<this.clone().startOf(l).valueOf());
},ga.isBefore=function(e,l){
var a=S(e)?e:Pl(e);
return!(!this.isValid()||!a.isValid())&&("millisecond"===(l=H(l)||"millisecond")?this.valueOf()<a.valueOf():this.clone().endOf(l).valueOf()<a.valueOf());
},ga.isBetween=function(e,l,a,t){
var n=S(e)?e:Pl(e),r=S(l)?l:Pl(l);
return!!(this.isValid()&&n.isValid()&&r.isValid())&&("("===(t=t||"()")[0]?this.isAfter(n,a):!this.isBefore(n,a))&&(")"===t[1]?this.isBefore(r,a):!this.isAfter(r,a));
},ga.isSame=function(e,l){
var a,t=S(e)?e:Pl(e);
return!(!this.isValid()||!t.isValid())&&("millisecond"===(l=H(l)||"millisecond")?this.valueOf()===t.valueOf():(a=t.valueOf(),
this.clone().startOf(l).valueOf()<=a&&a<=this.clone().endOf(l).valueOf()));
},ga.isSameOrAfter=function(e,l){
return this.isSame(e,l)||this.isAfter(e,l);
},ga.isSameOrBefore=function(e,l){
return this.isSame(e,l)||this.isBefore(e,l);
},ga.isValid=function(){
return p(this);
},ga.lang=la,ga.locale=ea,ga.localeData=aa,ga.max=Ml,ga.min=Tl,ga.parsingFlags=function(){
return f({},d(this));
},ga.set=function(e,l){
if("object"===r(e)){
var a,t=function(e){
var l,a=[];
for(l in e){i(e,l)&&a.push({
unit:l,
priority:J[l]});}

return a.sort(function(e,l){
return e.priority-l.priority;
}),a;
}(e=G(e));
for(a=0;a<t.length;a++){this[t[a].unit](e[t[a].unit]);}
}else if(T(this[e=H(e)]))return this[e](l);
return this;
},ga.startOf=function(e){
var l,a;
if(void 0===(e=H(e))||"millisecond"===e||!this.isValid())return this;
switch(a=this._isUTC?ra:na,e){
case"year":
l=a(this.year(),0,1);
break;

case"quarter":
l=a(this.year(),this.month()-this.month()%3,1);
break;

case"month":
l=a(this.year(),this.month(),1);
break;

case"week":
l=a(this.year(),this.month(),this.date()-this.weekday());
break;

case"isoWeek":
l=a(this.year(),this.month(),this.date()-(this.isoWeekday()-1));
break;

case"day":
case"date":
l=a(this.year(),this.month(),this.date());
break;

case"hour":
l=this._d.valueOf(),l-=ta(l+(this._isUTC?0:6e4*this.utcOffset()),36e5);
break;

case"minute":
l=this._d.valueOf(),l-=ta(l,6e4);
break;

case"second":
l=this._d.valueOf(),l-=ta(l,1e3);}

return this._d.setTime(l),t.updateOffset(this,!0),this;
},ga.subtract=Xl,ga.toArray=function(){
var e=this;
return[e.year(),e.month(),e.date(),e.hour(),e.minute(),e.second(),e.millisecond()];
},ga.toObject=function(){
var e=this;
return{
years:e.year(),
months:e.month(),
date:e.date(),
hours:e.hours(),
minutes:e.minutes(),
seconds:e.seconds(),
milliseconds:e.milliseconds()};

},ga.toDate=function(){
return new Date(this.valueOf());
},ga.toISOString=function(e){
if(!this.isValid())return null;
var l=!0!==e,a=l?this.clone().utc():this;
return a.year()<0||a.year()>9999?R(a,l?"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]":"YYYYYY-MM-DD[T]HH:mm:ss.SSSZ"):T(Date.prototype.toISOString)?l?this.toDate().toISOString():new Date(this.valueOf()+60*this.utcOffset()*1e3).toISOString().replace("Z",R(a,"Z")):R(a,l?"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]":"YYYY-MM-DD[T]HH:mm:ss.SSSZ");
},ga.inspect=function(){
if(!this.isValid())return"moment.invalid(/* "+this._i+" */)";
var e,l,a,t="moment",n="";
return this.isLocal()||(t=0===this.utcOffset()?"moment.utc":"moment.parseZone",
n="Z"),e="["+t+'("]',l=0<=this.year()&&this.year()<=9999?"YYYY":"YYYYYY",
a=n+'[")]',this.format(e+l+"-MM-DD[T]HH:mm:ss.SSS"+a);
},"undefined"!=typeof Symbol&&null!=Symbol.for&&(ga[Symbol.for("nodejs.util.inspect.custom")]=function(){
return"Moment<"+this.format()+">";
}),ga.toJSON=function(){
return this.isValid()?this.toISOString():null;
},ga.toString=function(){
return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ");
},ga.unix=function(){
return Math.floor(this.valueOf()/1e3);
},ga.valueOf=function(){
return this._d.valueOf()-6e4*(this._offset||0);
},ga.creationData=function(){
return{
input:this._i,
format:this._f,
locale:this._locale,
isUTC:this._isUTC,
strict:this._strict};

},ga.eraName=function(){
var e,l,a,t=this.localeData().eras();
for(e=0,l=t.length;e<l;++e){
if(a=this.clone().startOf("day").valueOf(),t[e].since<=a&&a<=t[e].until)return t[e].name;
if(t[e].until<=a&&a<=t[e].since)return t[e].name;
}
return"";
},ga.eraNarrow=function(){
var e,l,a,t=this.localeData().eras();
for(e=0,l=t.length;e<l;++e){
if(a=this.clone().startOf("day").valueOf(),t[e].since<=a&&a<=t[e].until)return t[e].narrow;
if(t[e].until<=a&&a<=t[e].since)return t[e].narrow;
}
return"";
},ga.eraAbbr=function(){
var e,l,a,t=this.localeData().eras();
for(e=0,l=t.length;e<l;++e){
if(a=this.clone().startOf("day").valueOf(),t[e].since<=a&&a<=t[e].until)return t[e].abbr;
if(t[e].until<=a&&a<=t[e].since)return t[e].abbr;
}
return"";
},ga.eraYear=function(){
var e,l,a,n,r=this.localeData().eras();
for(e=0,l=r.length;e<l;++e){if(a=r[e].since<=r[e].until?1:-1,
n=this.clone().startOf("day").valueOf(),r[e].since<=n&&n<=r[e].until||r[e].until<=n&&n<=r[e].since)return(this.year()-t(r[e].since).year())*a+r[e].offset;}
return this.year();
},ga.year=Ne,ga.isLeapYear=function(){
return z(this.year());
},ga.weekYear=function(e){
return sa.call(this,e,this.week(),this.weekday(),this.localeData()._week.dow,this.localeData()._week.doy);
},ga.isoWeekYear=function(e){
return sa.call(this,e,this.isoWeek(),this.isoWeekday(),1,4);
},ga.quarter=ga.quarters=function(e){
return null==e?Math.ceil((this.month()+1)/3):this.month(3*(e-1)+this.month()%3);
},ga.month=Ce,ga.daysInMonth=function(){
return Oe(this.year(),this.month());
},ga.week=ga.weeks=function(e){
var l=this.localeData().week(this);
return null==e?l:this.add(7*(e-l),"d");
},ga.isoWeek=ga.isoWeeks=function(e){
var l=Fe(this,1,4).week;
return null==e?l:this.add(7*(e-l),"d");
},ga.weeksInYear=function(){
var e=this.localeData()._week;
return Ue(this.year(),e.dow,e.doy);
},ga.weeksInWeekYear=function(){
var e=this.localeData()._week;
return Ue(this.weekYear(),e.dow,e.doy);
},ga.isoWeeksInYear=function(){
return Ue(this.year(),1,4);
},ga.isoWeeksInISOWeekYear=function(){
return Ue(this.isoWeekYear(),1,4);
},ga.date=va,ga.day=ga.days=function(e){
if(!this.isValid())return null!=e?this:NaN;
var l=this._isUTC?this._d.getUTCDay():this._d.getDay();
return null!=e?(e=function(e,l){
return"string"!=typeof e?e:isNaN(e)?"number"==typeof(e=l.weekdaysParse(e))?e:null:parseInt(e,10);
}(e,this.localeData()),this.add(e-l,"d")):l;
},ga.weekday=function(e){
if(!this.isValid())return null!=e?this:NaN;
var l=(this.day()+7-this.localeData()._week.dow)%7;
return null==e?l:this.add(e-l,"d");
},ga.isoWeekday=function(e){
if(!this.isValid())return null!=e?this:NaN;
if(null!=e){
var l=function(e,l){
return"string"==typeof e?l.weekdaysParse(e)%7||7:isNaN(e)?null:e;
}(e,this.localeData());
return this.day(this.day()%7?l:l-7);
}
return this.day()||7;
},ga.dayOfYear=function(e){
var l=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;
return null==e?l:this.add(e-l,"d");
},ga.hour=ga.hours=el,ga.minute=ga.minutes=ba,ga.second=ga.seconds=da,
ga.millisecond=ga.milliseconds=ha,ga.utcOffset=function(e,l,a){
var n,r=this._offset||0;
if(!this.isValid())return null!=e?this:NaN;
if(null!=e){
if("string"==typeof e){
if(null===(e=Yl(fe,e)))return this;
}else Math.abs(e)<16&&!a&&(e*=60);
return!this._isUTC&&l&&(n=Fl(this)),this._offset=e,this._isUTC=!0,
null!=n&&this.add(n,"m"),r!==e&&(!l||this._changeInProgress?Bl(this,Gl(e-r,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,
t.updateOffset(this,!0),this._changeInProgress=null)),this;
}
return this._isUTC?r:Fl(this);
},ga.utc=function(e){
return this.utcOffset(0,e);
},ga.local=function(e){
return this._isUTC&&(this.utcOffset(0,e),this._isUTC=!1,e&&this.subtract(Fl(this),"m")),
this;
},ga.parseZone=function(){
if(null!=this._tzm)this.utcOffset(this._tzm,!1,!0);else if("string"==typeof this._i){
var e=Yl(be,this._i);
null!=e?this.utcOffset(e):this.utcOffset(0,!0);
}
return this;
},ga.hasAlignedHourOffset=function(e){
return!!this.isValid()&&(e=e?Pl(e).utcOffset():0,(this.utcOffset()-e)%60==0);
},ga.isDST=function(){
return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset();
},ga.isLocal=function(){
return!!this.isValid()&&!this._isUTC;
},ga.isUtcOffset=function(){
return!!this.isValid()&&this._isUTC;
},ga.isUtc=Ul,ga.isUTC=Ul,ga.zoneAbbr=function(){
return this._isUTC?"UTC":"";
},ga.zoneName=function(){
return this._isUTC?"Coordinated Universal Time":"";
},ga.dates=x("dates accessor is deprecated. Use date instead.",va),ga.months=x("months accessor is deprecated. Use month instead",Ce),
ga.years=x("years accessor is deprecated. Use year instead",Ne),ga.zone=x("moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/",function(e,l){
return null!=e?("string"!=typeof e&&(e=-e),this.utcOffset(e,l),this):-this.utcOffset();
}),ga.isDSTShifted=x("isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information",function(){
if(!s(this._isDSTShifted))return this._isDSTShifted;
var e,l={};
return w(l,this),(l=kl(l))._a?(e=l._isUTC?h(l._a):Pl(l._a),this._isDSTShifted=this.isValid()&&function(e,l,a){
var t,n=Math.min(e.length,l.length),r=Math.abs(e.length-l.length),u=0;
for(t=0;t<n;t++){Z(e[t])!==Z(l[t])&&u++;}
return u+r;
}(l._a,e.toArray())>0):this._isDSTShifted=!1,this._isDSTShifted;
});
var ya=E.prototype;
function wa(e,l,a,t){
var n=sl(),r=h().set(t,l);
return n[a](r,e);
}
function _a(e,l,a){
if(c(e)&&(l=e,e=void 0),e=e||"",null!=l)return wa(e,l,a,"month");
var t,n=[];
for(t=0;t<12;t++){n[t]=wa(e,t,a,"month");}
return n;
}
function Sa(e,l,a,t){
"boolean"==typeof e?(c(l)&&(a=l,l=void 0),l=l||""):(a=l=e,
e=!1,c(l)&&(a=l,l=void 0),l=l||"");
var n,r=sl(),u=e?r._week.dow:0,i=[];
if(null!=a)return wa(l,(a+u)%7,t,"day");
for(n=0;n<7;n++){i[n]=wa(l,(n+u)%7,t,"day");}
return i;
}
ya.calendar=function(e,l,a){
var t=this._calendar[e]||this._calendar.sameElse;
return T(t)?t.call(l,a):t;
},ya.longDateFormat=function(e){
var l=this._longDateFormat[e],a=this._longDateFormat[e.toUpperCase()];
return l||!a?l:(this._longDateFormat[e]=a.match(j).map(function(e){
return"MMMM"===e||"MM"===e||"DD"===e||"dddd"===e?e.slice(1):e;
}).join(""),this._longDateFormat[e]);
},ya.invalidDate=function(){
return this._invalidDate;
},ya.ordinal=function(e){
return this._ordinal.replace("%d",e);
},ya.preparse=ma,ya.postformat=ma,ya.relativeTime=function(e,l,a,t){
var n=this._relativeTime[a];
return T(n)?n(e,l,a,t):n.replace(/%d/i,e);
},ya.pastFuture=function(e,l){
var a=this._relativeTime[e>0?"future":"past"];
return T(a)?a(l):a.replace(/%s/i,l);
},ya.set=function(e){
var l,a;
for(a in e){i(e,a)&&(T(l=e[a])?this[a]=l:this["_"+a]=l);}
this._config=e,this._dayOfMonthOrdinalParseLenient=new RegExp((this._dayOfMonthOrdinalParse.source||this._ordinalParse.source)+"|"+/\d{1,2}/.source);
},ya.eras=function(e,l){
var a,n,u,i=this._eras||sl("en")._eras;
for(a=0,n=i.length;a<n;++a){
switch(r(i[a].since)){
case"string":
u=t(i[a].since).startOf("day"),i[a].since=u.valueOf();}

switch(r(i[a].until)){
case"undefined":
i[a].until=1/0;
break;

case"string":
u=t(i[a].until).startOf("day").valueOf(),i[a].until=u.valueOf();}

}
return i;
},ya.erasParse=function(e,l,a){
var t,n,r,u,i,o=this.eras();
for(e=e.toUpperCase(),t=0,n=o.length;t<n;++t){if(r=o[t].name.toUpperCase(),
u=o[t].abbr.toUpperCase(),i=o[t].narrow.toUpperCase(),a)switch(l){
case"N":
case"NN":
case"NNN":
if(u===e)return o[t];
break;

case"NNNN":
if(r===e)return o[t];
break;

case"NNNNN":
if(i===e)return o[t];}else
if([r,u,i].indexOf(e)>=0)return o[t];}
},ya.erasConvertYear=function(e,l){
var a=e.since<=e.until?1:-1;
return void 0===l?t(e.since).year():t(e.since).year()+(l-e.offset)*a;
},ya.erasAbbrRegex=function(e){
return i(this,"_erasAbbrRegex")||ia.call(this),e?this._erasAbbrRegex:this._erasRegex;
},ya.erasNameRegex=function(e){
return i(this,"_erasNameRegex")||ia.call(this),e?this._erasNameRegex:this._erasRegex;
},ya.erasNarrowRegex=function(e){
return i(this,"_erasNarrowRegex")||ia.call(this),e?this._erasNarrowRegex:this._erasRegex;
},ya.months=function(e,l){
return e?n(this._months)?this._months[e.month()]:this._months[(this._months.isFormat||Ae).test(l)?"format":"standalone"][e.month()]:n(this._months)?this._months:this._months.standalone;
},ya.monthsShort=function(e,l){
return e?n(this._monthsShort)?this._monthsShort[e.month()]:this._monthsShort[Ae.test(l)?"format":"standalone"][e.month()]:n(this._monthsShort)?this._monthsShort:this._monthsShort.standalone;
},ya.monthsParse=function(e,l,a){
var t,n,r;
if(this._monthsParseExact)return Me.call(this,e,l,a);
for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),
t=0;t<12;t++){
if(n=h([2e3,t]),a&&!this._longMonthsParse[t]&&(this._longMonthsParse[t]=new RegExp("^"+this.months(n,"").replace(".","")+"$","i"),
this._shortMonthsParse[t]=new RegExp("^"+this.monthsShort(n,"").replace(".","")+"$","i")),
a||this._monthsParse[t]||(r="^"+this.months(n,"")+"|^"+this.monthsShort(n,""),
this._monthsParse[t]=new RegExp(r.replace(".",""),"i")),a&&"MMMM"===l&&this._longMonthsParse[t].test(e))return t;
if(a&&"MMM"===l&&this._shortMonthsParse[t].test(e))return t;
if(!a&&this._monthsParse[t].test(e))return t;
}
},ya.monthsRegex=function(e){
return this._monthsParseExact?(i(this,"_monthsRegex")||je.call(this),e?this._monthsStrictRegex:this._monthsRegex):(i(this,"_monthsRegex")||(this._monthsRegex=Te),
this._monthsStrictRegex&&e?this._monthsStrictRegex:this._monthsRegex);
},ya.monthsShortRegex=function(e){
return this._monthsParseExact?(i(this,"_monthsRegex")||je.call(this),e?this._monthsShortStrictRegex:this._monthsShortRegex):(i(this,"_monthsShortRegex")||(this._monthsShortRegex=Pe),
this._monthsShortStrictRegex&&e?this._monthsShortStrictRegex:this._monthsShortRegex);
},ya.week=function(e){
return Fe(e,this._week.dow,this._week.doy).week;
},ya.firstDayOfYear=function(){
return this._week.doy;
},ya.firstDayOfWeek=function(){
return this._week.dow;
},ya.weekdays=function(e,l){
var a=n(this._weekdays)?this._weekdays:this._weekdays[e&&!0!==e&&this._weekdays.isFormat.test(l)?"format":"standalone"];
return!0===e?Ve(a,this._week.dow):e?a[e.day()]:a;
},ya.weekdaysMin=function(e){
return!0===e?Ve(this._weekdaysMin,this._week.dow):e?this._weekdaysMin[e.day()]:this._weekdaysMin;
},ya.weekdaysShort=function(e){
return!0===e?Ve(this._weekdaysShort,this._week.dow):e?this._weekdaysShort[e.day()]:this._weekdaysShort;
},ya.weekdaysParse=function(e,l,a){
var t,n,r;
if(this._weekdaysParseExact)return Ze.call(this,e,l,a);
for(this._weekdaysParse||(this._weekdaysParse=[],this._minWeekdaysParse=[],
this._shortWeekdaysParse=[],this._fullWeekdaysParse=[]),t=0;t<7;t++){
if(n=h([2e3,1]).day(t),a&&!this._fullWeekdaysParse[t]&&(this._fullWeekdaysParse[t]=new RegExp("^"+this.weekdays(n,"").replace(".","\\.?")+"$","i"),
this._shortWeekdaysParse[t]=new RegExp("^"+this.weekdaysShort(n,"").replace(".","\\.?")+"$","i"),
this._minWeekdaysParse[t]=new RegExp("^"+this.weekdaysMin(n,"").replace(".","\\.?")+"$","i")),
this._weekdaysParse[t]||(r="^"+this.weekdays(n,"")+"|^"+this.weekdaysShort(n,"")+"|^"+this.weekdaysMin(n,""),
this._weekdaysParse[t]=new RegExp(r.replace(".",""),"i")),a&&"dddd"===l&&this._fullWeekdaysParse[t].test(e))return t;
if(a&&"ddd"===l&&this._shortWeekdaysParse[t].test(e))return t;
if(a&&"dd"===l&&this._minWeekdaysParse[t].test(e))return t;
if(!a&&this._weekdaysParse[t].test(e))return t;
}
},ya.weekdaysRegex=function(e){
return this._weekdaysParseExact?(i(this,"_weekdaysRegex")||Xe.call(this),e?this._weekdaysStrictRegex:this._weekdaysRegex):(i(this,"_weekdaysRegex")||(this._weekdaysRegex=We),
this._weekdaysStrictRegex&&e?this._weekdaysStrictRegex:this._weekdaysRegex);
},ya.weekdaysShortRegex=function(e){
return this._weekdaysParseExact?(i(this,"_weekdaysRegex")||Xe.call(this),e?this._weekdaysShortStrictRegex:this._weekdaysShortRegex):(i(this,"_weekdaysShortRegex")||(this._weekdaysShortRegex=ze),
this._weekdaysShortStrictRegex&&e?this._weekdaysShortStrictRegex:this._weekdaysShortRegex);
},ya.weekdaysMinRegex=function(e){
return this._weekdaysParseExact?(i(this,"_weekdaysRegex")||Xe.call(this),e?this._weekdaysMinStrictRegex:this._weekdaysMinRegex):(i(this,"_weekdaysMinRegex")||(this._weekdaysMinRegex=Be),
this._weekdaysMinStrictRegex&&e?this._weekdaysMinStrictRegex:this._weekdaysMinRegex);
},ya.isPM=function(e){
return"p"===(e+"").toLowerCase().charAt(0);
},ya.meridiem=function(e,l,a){
return e>11?a?"pm":"PM":a?"am":"AM";
},il("en",{
eras:[{
since:"0001-01-01",
until:1/0,
offset:1,
name:"Anno Domini",
narrow:"AD",
abbr:"AD"},
{
since:"0000-12-31",
until:-1/0,
offset:1,
name:"Before Christ",
narrow:"BC",
abbr:"BC"}],

dayOfMonthOrdinalParse:/\d{1,2}(th|st|nd|rd)/,
ordinal:function ordinal(e){
var l=e%10;
return e+(1===Z(e%100/10)?"th":1===l?"st":2===l?"nd":3===l?"rd":"th");
}}),
t.lang=x("moment.lang is deprecated. Use moment.locale instead.",il),t.langData=x("moment.langData is deprecated. Use moment.localeData instead.",sl);
var Oa=Math.abs;
function xa(e,l,a,t){
var n=Gl(l,a);
return e._milliseconds+=t*n._milliseconds,e._days+=t*n._days,e._months+=t*n._months,
e._bubble();
}
function ka(e){
return e<0?Math.floor(e):Math.ceil(e);
}
function Aa(e){
return 4800*e/146097;
}
function Pa(e){
return 146097*e/4800;
}
function Ta(e){
return function(){
return this.as(e);
};
}
var Ma=Ta("ms"),Ea=Ta("s"),Ca=Ta("m"),ja=Ta("h"),Da=Ta("d"),Na=Ta("w"),La=Ta("M"),Ia=Ta("Q"),Ya=Ta("y");
function Ra(e){
return function(){
return this.isValid()?this._data[e]:NaN;
};
}
var Fa=Ra("milliseconds"),Ua=Ra("seconds"),Va=Ra("minutes"),Ha=Ra("hours"),Ga=Ra("days"),Ja=Ra("months"),Wa=Ra("years"),za=Math.round,Ba={
ss:44,
s:45,
m:45,
h:22,
d:26,
w:null,
M:11};

function Za(e,l,a,t,n){
return n.relativeTime(l||1,!!a,e,t);
}
var Xa=Math.abs;
function Qa(e){
return(e>0)-(e<0)||+e;
}
function Ka(){
if(!this.isValid())return this.localeData().invalidDate();
var e,l,a,t,n,r,u,i,o=Xa(this._milliseconds)/1e3,s=Xa(this._days),c=Xa(this._months),v=this.asSeconds();
return v?(e=B(o/60),l=B(e/60),o%=60,e%=60,a=B(c/12),c%=12,
t=o?o.toFixed(3).replace(/\.?0+$/,""):"",n=v<0?"-":"",r=Qa(this._months)!==Qa(v)?"-":"",
u=Qa(this._days)!==Qa(v)?"-":"",i=Qa(this._milliseconds)!==Qa(v)?"-":"",
n+"P"+(a?r+a+"Y":"")+(c?r+c+"M":"")+(s?u+s+"D":"")+(l||e||o?"T":"")+(l?i+l+"H":"")+(e?i+e+"M":"")+(o?i+t+"S":"")):"P0D";
}
var qa=jl.prototype;
return qa.isValid=function(){
return this._isValid;
},qa.abs=function(){
var e=this._data;
return this._milliseconds=Oa(this._milliseconds),this._days=Oa(this._days),
this._months=Oa(this._months),e.milliseconds=Oa(e.milliseconds),e.seconds=Oa(e.seconds),
e.minutes=Oa(e.minutes),e.hours=Oa(e.hours),e.months=Oa(e.months),e.years=Oa(e.years),
this;
},qa.add=function(e,l){
return xa(this,e,l,1);
},qa.subtract=function(e,l){
return xa(this,e,l,-1);
},qa.as=function(e){
if(!this.isValid())return NaN;
var l,a,t=this._milliseconds;
if("month"===(e=H(e))||"quarter"===e||"year"===e)switch(l=this._days+t/864e5,
a=this._months+Aa(l),e){
case"month":
return a;

case"quarter":
return a/3;

case"year":
return a/12;}else
switch(l=this._days+Math.round(Pa(this._months)),e){
case"week":
return l/7+t/6048e5;

case"day":
return l+t/864e5;

case"hour":
return 24*l+t/36e5;

case"minute":
return 1440*l+t/6e4;

case"second":
return 86400*l+t/1e3;

case"millisecond":
return Math.floor(864e5*l)+t;

default:
throw new Error("Unknown unit "+e);}

},qa.asMilliseconds=Ma,qa.asSeconds=Ea,qa.asMinutes=Ca,qa.asHours=ja,
qa.asDays=Da,qa.asWeeks=Na,qa.asMonths=La,qa.asQuarters=Ia,qa.asYears=Ya,
qa.valueOf=function(){
return this.isValid()?this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*Z(this._months/12):NaN;
},qa._bubble=function(){
var e,l,a,t,n,r=this._milliseconds,u=this._days,i=this._months,o=this._data;
return r>=0&&u>=0&&i>=0||r<=0&&u<=0&&i<=0||(r+=864e5*ka(Pa(i)+u),
u=0,i=0),o.milliseconds=r%1e3,e=B(r/1e3),o.seconds=e%60,l=B(e/60),
o.minutes=l%60,a=B(l/60),o.hours=a%24,u+=B(a/24),i+=n=B(Aa(u)),
u-=ka(Pa(n)),t=B(i/12),i%=12,o.days=u,o.months=i,o.years=t,this;
},qa.clone=function(){
return Gl(this);
},qa.get=function(e){
return e=H(e),this.isValid()?this[e+"s"]():NaN;
},qa.milliseconds=Fa,qa.seconds=Ua,qa.minutes=Va,qa.hours=Ha,qa.days=Ga,
qa.weeks=function(){
return B(this.days()/7);
},qa.months=Ja,qa.years=Wa,qa.humanize=function(e,l){
if(!this.isValid())return this.localeData().invalidDate();
var a,t,n=!1,u=Ba;
return"object"===r(e)&&(l=e,e=!1),"boolean"==typeof e&&(n=e),"object"===r(l)&&(u=Object.assign({},Ba,l),
null!=l.s&&null==l.ss&&(u.ss=l.s-1)),t=function(e,l,a,t){
var n=Gl(e).abs(),r=za(n.as("s")),u=za(n.as("m")),i=za(n.as("h")),o=za(n.as("d")),s=za(n.as("M")),c=za(n.as("w")),v=za(n.as("y")),b=r<=a.ss&&["s",r]||r<a.s&&["ss",r]||u<=1&&["m"]||u<a.m&&["mm",u]||i<=1&&["h"]||i<a.h&&["hh",i]||o<=1&&["d"]||o<a.d&&["dd",o];
return null!=a.w&&(b=b||c<=1&&["w"]||c<a.w&&["ww",c]),(b=b||s<=1&&["M"]||s<a.M&&["MM",s]||v<=1&&["y"]||["yy",v])[2]=l,
b[3]=+e>0,b[4]=t,Za.apply(null,b);
}(this,!n,u,a=this.localeData()),n&&(t=a.pastFuture(+this,t)),a.postformat(t);
},qa.toISOString=Ka,qa.toString=Ka,qa.toJSON=Ka,qa.locale=ea,qa.localeData=aa,
qa.toIsoString=x("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",Ka),
qa.lang=la,I("X",0,0,"unix"),I("x",0,0,"valueOf"),de("x",ve),de("X",/[+-]?\d+(\.\d{1,3})?/),
we("X",function(e,l,a){
a._d=new Date(1e3*parseFloat(e));
}),we("x",function(e,l,a){
a._d=new Date(Z(e));
}),t.version="2.29.1",function(e){
l=e;
}(Pl),t.fn=ga,t.min=function(){
var e=[].slice.call(arguments,0);
return El("isBefore",e);
},t.max=function(){
var e=[].slice.call(arguments,0);
return El("isAfter",e);
},t.now=function(){
return Date.now?Date.now():+new Date();
},t.utc=h,t.unix=function(e){
return Pl(1e3*e);
},t.months=function(e,l){
return _a(e,l,"months");
},t.isDate=v,t.locale=il,t.invalid=g,t.duration=Gl,t.isMoment=S,
t.weekdays=function(e,l,a){
return Sa(e,l,a,"weekdays");
},t.parseZone=function(){
return Pl.apply(null,arguments).parseZone();
},t.localeData=sl,t.isDuration=Dl,t.monthsShort=function(e,l){
return _a(e,l,"monthsShort");
},t.weekdaysMin=function(e,l,a){
return Sa(e,l,a,"weekdaysMin");
},t.defineLocale=ol,t.updateLocale=function(e,l){
if(null!=l){
var a,t,n=ll;
null!=al[e]&&null!=al[e].parentLocale?al[e].set(M(al[e]._config,l)):(null!=(t=ul(e))&&(n=t._config),
l=M(n,l),null==t&&(l.abbr=e),(a=new E(l)).parentLocale=al[e],al[e]=a),
il(e);
}else null!=al[e]&&(null!=al[e].parentLocale?(al[e]=al[e].parentLocale,
e===il()&&il(e)):null!=al[e]&&delete al[e]);
return al[e];
},t.locales=function(){
return k(al);
},t.weekdaysShort=function(e,l,a){
return Sa(e,l,a,"weekdaysShort");
},t.normalizeUnits=H,t.relativeTimeRounding=function(e){
return void 0===e?za:"function"==typeof e&&(za=e,!0);
},t.relativeTimeThreshold=function(e,l){
return void 0!==Ba[e]&&(void 0===l?Ba[e]:(Ba[e]=l,"s"===e&&(Ba.ss=l-1),
!0));
},t.calendarFormat=function(e,l){
var a=e.diff(l,"days",!0);
return a<-6?"sameElse":a<-1?"lastWeek":a<0?"lastDay":a<1?"sameDay":a<2?"nextDay":a<7?"nextWeek":"sameElse";
},t.prototype=ga,t.HTML5_FMT={
DATETIME_LOCAL:"YYYY-MM-DDTHH:mm",
DATETIME_LOCAL_SECONDS:"YYYY-MM-DDTHH:mm:ss",
DATETIME_LOCAL_MS:"YYYY-MM-DDTHH:mm:ss.SSS",
DATE:"YYYY-MM-DD",
TIME:"HH:mm",
TIME_SECONDS:"HH:mm:ss",
TIME_MS:"HH:mm:ss.SSS",
WEEK:"GGGG-[W]WW",
MONTH:"YYYY-MM"},
t;
});
}).call(this,a("62e4")(e));
},
"921a":function a(l,_a5,t){
"use strict";
(function(a){
var n=t("4ea4"),r=n(t("2eee")),u=n(t("9523")),i=n(t("c973")),o=t("cbb4"),s=n(t("2684"));
function c(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function v(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?c(Object(a),!0).forEach(function(l){
(0,u.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):c(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
l.exports={
upload:function upload(l){
return(0,i.default)(r.default.mark(function t(){
return r.default.wrap(function(t){
for(;;){switch(t.prev=t.next){
case 0:
return t.abrupt("return",new Promise(function(t,n){
a.uploadFile({
url:s.default.BASE_URL+"/image",
filePath:l,
name:"image",
header:{
Authorization:(0,o.$getStorage)("token")},

success:function success(e){
try{
var l=JSON.parse(e.data);
t(l.data.image.url);
}catch(e){
n(e);
}
},
fail:function fail(l){
n(e);
}});

}));

case 1:
case"end":
return t.stop();}}

},t);
}))();
},
select:function select(e,l){
var t=this.upload;
a.chooseImage(v(v({},e),{},{
success:function success(e){
var n=[];
e.tempFilePaths.forEach(function(e){
n.push(t(e));
}),setTimeout(function(){
a.showLoading({
title:"图片处理中"});

},100),Promise.all(n).then(function(e){
setTimeout(function(){
a.hideLoading();
},100),l(e);
});
}}));

}};

}).call(this,t("543d").default);
},
"93db":function db(e,l){
e.exports={
list:["GET","/categories"]};

},
9523:function _(e,l,a){
var t=a("a395");
e.exports=function(e,l,a){
return(l=t(l))in e?Object.defineProperty(e,l,{
value:a,
enumerable:!0,
configurable:!0,
writable:!0}):
e[l]=a,e;
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"955d":function d(e,l,a){
"use strict";
(function(l){
var t=a("4ea4"),n=t(a("2eee")),r=t(a("7037")),u=t(a("9523")),i=t(a("c973")),o=t(a("b5e5")),s=t(a("3ce8"));
function c(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function v(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?c(Object(a),!0).forEach(function(l){
(0,u.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):c(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
function b(e){
var a=e.type,t=e.background,r=e.posterCanvasId,u=e.backgroundImage,s=e.reserve,c=e.textArray,v=e.drawArray,b=e.qrCodeArray,d=e.imagesArray,p=e.setCanvasWH,m=e.setCanvasToTempFilePath,y=e.setDraw,w=e.bgScale,_=e.Context,S=e._this,O=e.delayTimeScale,x=e.drawDelayTime;
return new Promise(function(){
var e=(0,i.default)(n.default.mark(function e(i,A){
var P,T,M,E,C,j;
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
if(e.prev=0,o.default.showLoading("正在准备海报数据"),_||(o.default.log("没有画布对象,创建画布对象"),
_=l.createCanvasContext(r,S||null)),!(t&&t.width&&t.height)){
e.next=7;
break;
}
P=t,e.next=10;
break;

case 7:
return e.next=9,k({
backgroundImage:u,
type:a});


case 9:
P=e.sent;

case 10:
if(w=w||.75,P.width=P.width*w,P.height=P.height*w,o.default.log("获取背景图信息对象成功:"+JSON.stringify(P)),
T={
bgObj:P,
type:a,
bgScale:w},
p&&"function"==typeof p&&p(T),!d){
e.next=24;
break;
}
return"function"==typeof d&&(d=d(T)),o.default.showLoading("正在生成需绘制图片的临时路径"),
o.default.log("准备设置图片"),e.next=22,g(d);

case 22:
d=e.sent,o.default.hideLoading();

case 24:
if(c&&("function"==typeof c&&(c=c(T)),c=h(_,c)),!b){
e.next=39;
break;
}
"function"==typeof b&&(b=b(T)),o.default.showLoading("正在生成需绘制图片的临时路径"),M=0;

case 29:
if(!(M<b.length)){
e.next=38;
break;
}
if(o.default.log(M),!b[M].image){
e.next=35;
break;
}
return e.next=34,o.default.downloadFile_PromiseFc(b[M].image);

case 34:
b[M].image=e.sent;

case 35:
M++,e.next=29;
break;

case 38:
o.default.hideLoading();

case 39:
if(!v){
e.next=69;
break;
}
if("function"==typeof v&&(v=v(T)),!o.default.isPromise(v)){
e.next=45;
break;
}
return e.next=44,v;

case 44:
v=e.sent;

case 45:
E=0;

case 46:
if(!(E<v.length)){
e.next=69;
break;
}
C=v[E],e.t0=C.type,e.next="image"===e.t0?51:"text"===e.t0?55:"qrcode"===e.t0?57:"custom"===e.t0?62:63;
break;

case 51:
return e.next=53,g(C);

case 53:
return C=e.sent,e.abrupt("break",65);

case 55:
return C=h(_,C),e.abrupt("break",65);

case 57:
if(!C.image){
e.next=61;
break;
}
return e.next=60,o.default.downloadFile_PromiseFc(C.image);

case 60:
C.image=e.sent;

case 61:
case 62:
return e.abrupt("break",65);

case 63:
return o.default.log("未识别的类型"),e.abrupt("break",65);

case 65:
v[E]=C;

case 66:
E++,e.next=46;
break;

case 69:
return e.next=71,f({
Context:_,
type:a,
posterCanvasId:r,
reserve:s,
drawArray:v,
textArray:c,
imagesArray:d,
bgObj:P,
qrCodeArray:b,
setCanvasToTempFilePath:m,
setDraw:y,
bgScale:w,
_this:S,
delayTimeScale:O,
drawDelayTime:x});


case 71:
j=e.sent,o.default.hideLoading(),i({
bgObj:P,
poster:j,
type:a}),
e.next=79;
break;

case 76:
e.prev=76,e.t1=e.catch(0),A(e.t1);

case 79:
case"end":
return e.stop();}}

},e,null,[[0,76]]);
}));
return function(l,a){
return e.apply(this,arguments);
};
}());
}
function f(e){
var a=e.Context,t=e.type,n=e.posterCanvasId,r=e.reserve,u=e.bgObj,i=e.drawArray,s=e.textArray,c=e.qrCodeArray,b=e.imagesArray,f=e.setCanvasToTempFilePath,h=e.setDraw,d=e.bgScale,p=e._this,g=e.delayTimeScale,m=e.drawDelayTime,w={
Context:a,
bgObj:u,
type:t,
bgScale:d};

return g=void 0!==g?g:15,m=void 0!==m?m:100,new Promise(function(e,d){
try{
if(o.default.showLoading("正在绘制海报"),o.default.log("背景对象:"+JSON.stringify(u)),
u&&u.path?(o.default.log("背景有图片路径"),a.drawImage(u.path,0,0,u.width,u.height)):(o.default.log("背景没有图片路径"),
u.backgroundColor?(o.default.log("背景有背景颜色:"+u.backgroundColor),a.setFillStyle(u.backgroundColor),
a.fillRect(0,0,u.width,u.height)):o.default.log("背景没有背景颜色")),o.default.showLoading("绘制图片"),
b&&b.length>0&&_(a,b),o.default.showLoading("绘制自定义内容"),h&&"function"==typeof h&&h(w),
o.default.showLoading("绘制文本"),s&&s.length>0&&y(a,s,u),o.default.showLoading("绘制二维码"),
c&&c.length>0)for(var S=0;S<c.length;S++){x(a,c[S]);}
if(o.default.showLoading("绘制可控层级序列"),i&&i.length>0)for(var O=0;O<i.length;O++){
var k=i[O];
switch(o.default.log("绘制可控层级序列, drawArrayItem:"+JSON.stringify(k)),k.type){
case"image":
o.default.log("绘制可控层级序列, 绘制图片"),_(a,k);
break;

case"text":
o.default.log("绘制可控层级序列, 绘制文本"),y(a,k,u);
break;

case"qrcode":
o.default.log("绘制可控层级序列, 绘制二维码"),x(a,k);
break;

case"custom":
o.default.log("绘制可控层级序列, 绘制自定义内容"),k.setDraw&&"function"==typeof k.setDraw&&k.setDraw(a);
break;

default:
o.default.log("未识别的类型");}

}
o.default.showLoading("绘制中"),setTimeout(function(){
a.draw("boolean"==typeof r&&r,function(){
o.default.showLoading("正在输出图片");
var a,r={};
f&&"function"==typeof f&&(r=f(u,t));
var h=v({
x:0,
y:0,
width:u.width,
height:u.height,
destWidth:2*u.width,
destHeight:2*u.height,
quality:.8},
r);
o.default.log("canvasToTempFilePath的data对象:"+JSON.stringify(h)),a=function a(){
l.canvasToTempFilePath(v(v({},h),{},{
canvasId:n,
success:function success(l){
o.default.hideLoading(),e(l);
},
fail:function fail(e){
o.default.hideLoading(),o.default.log("输出图片失败:"+JSON.stringify(e)),d("输出图片失败:"+JSON.stringify(e));
}}),
p||null);
};
var m=0;
c&&c.forEach(function(e){
e.text&&(m+=Number(e.text.length));
}),b&&b.forEach(function(){
m+=g;
}),s&&s.forEach(function(){
m+=g;
}),i&&i.forEach(function(e){
switch(e.type){
case"text":
e.text&&(m+=e.text.length);
break;

default:
m+=g;}

}),o.default.log("延时系数:"+g),o.default.log("总计延时:"+m),setTimeout(a,m);
});
},m);
}catch(e){
o.default.hideLoading(),d(e);
}
});
}
function h(e,l){
if(o.default.log("进入设置文字方法, texts:"+JSON.stringify(l)),l&&o.default.isArray(l)){
if(o.default.log("texts是数组"),l.length>0)for(var a=0;a<l.length;a++){o.default.log("字符串信息-初始化之前:"+JSON.stringify(l[a])),
l[a]=d(e,l[a]);}
}else o.default.log("texts是对象"),l=d(e,l);
return o.default.log("返回texts:"+JSON.stringify(l)),l;
}
function d(e,l){
if(o.default.log("进入设置文字方法, textItem:"+JSON.stringify(l)),l.text&&"string"==typeof l.text&&l.text.length>0){
l.alpha=void 0!==l.alpha?l.alpha:1,l.color=l.color||"black",l.size=void 0!==l.size?l.size:10,
l.textAlign=l.textAlign||"left",l.textBaseline=l.textBaseline||"middle",
l.dx=l.dx||0,l.dy=l.dy||0,l.size=Math.ceil(Number(l.size)),o.default.log("字符串信息-初始化默认值后:"+JSON.stringify(l));
var a=p(e,{
text:l.text,
size:l.size});

o.default.log("字符串信息-初始化时的文本长度:"+a);
var t={};
l.infoCallBack&&"function"==typeof l.infoCallBack&&(t=l.infoCallBack(a)),
l=v(v({},l),{},{
textLength:a},
t),o.default.log("字符串信息-infoCallBack后:"+JSON.stringify(l));
}
return l;
}
function p(e,l){
o.default.log("计算文字长度, obj:"+JSON.stringify(l));
var a,t=l.text,n=l.size;
e.setFontSize(n);
try{
a=e.measureText(t);
}catch(e){
a={};
}
if(o.default.log("measureText计算文字长度, textLength:"+JSON.stringify(a)),!(a=a&&a.width?a.width:0)){
for(var r=0,u=0;u<t.length;u++){
var i=t.substr(u,1);
/[a-zA-Z]/.test(i)?r+=.7:/[0-9]/.test(i)?r+=.55:/\./.test(i)?r+=.27:/-/.test(i)?r+=.325:/[\u4e00-\u9fa5]/.test(i)?r+=1:/\(|\)/.test(i)?r+=.373:/\s/.test(i)?r+=.25:/%/.test(i)?r+=.8:r+=1;
}
a=r*n;
}
return a;
}
function g(e){
return o.default.log("进入设置图片数据方法"),new Promise(function(){
var l=(0,i.default)(n.default.mark(function l(a,t){
var r;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
if(l.prev=0,!e||!o.default.isArray(e)){
l.next=14;
break;
}
o.default.log("images是一个数组"),r=0;

case 4:
if(!(r<e.length)){
l.next=12;
break;
}
return o.default.log("设置图片数据循环中:"+r),l.next=8,m(e[r]);

case 8:
e[r]=l.sent;

case 9:
r++,l.next=4;
break;

case 12:
l.next=18;
break;

case 14:
return o.default.log("images是一个对象"),l.next=17,m(e);

case 17:
e=l.sent;

case 18:
a(e),l.next=24;
break;

case 21:
l.prev=21,l.t0=l.catch(0),t(l.t0);

case 24:
case"end":
return l.stop();}}

},l,null,[[0,21]]);
}));
return function(e,a){
return l.apply(this,arguments);
};
}());
}
function m(e){
return new Promise(function(){
var l=(0,i.default)(n.default.mark(function l(a,t){
var r,u,i;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
if(!e.url){
l.next=17;
break;
}
return r=e.url,l.next=4,o.default.downloadFile_PromiseFc(r);

case 4:
return r=l.sent,e.url=r,u=e.infoCallBack&&"function"==typeof e.infoCallBack,
i={},l.next=10,o.default.getImageInfo_PromiseFc(r);

case 10:
i=l.sent,u&&(e=v(v({},e),e.infoCallBack(i))),e.dx=e.dx||0,e.dy=e.dy||0,
e.dWidth=e.dWidth||i.width,e.dHeight=e.dHeight||i.height,e=v(v({},e),{},{
imageInfo:i});


case 17:
a(e);

case 18:
case"end":
return l.stop();}}

},l);
}));
return function(e,a){
return l.apply(this,arguments);
};
}());
}
function y(e,l,a){
o.default.isArray(l)?o.default.log("遍历文本方法, 是数组"):(o.default.log("遍历文本方法, 不是数组"),
l=[l]),o.default.log("遍历文本方法, textArray:"+JSON.stringify(l));
var t=[];
if(l&&l.length>0)for(var n=0;n<l.length;n++){
var r=l[n];
if(r.text&&r.lineFeed){
var u=-1,i=a.width,s=r.size,c=r.dx;
if(r.lineFeed instanceof Object){
var b=r.lineFeed;
u=void 0!==b.lineNum&&"number"==typeof b.lineNum&&b.lineNum>=0?b.lineNum:u,
i=void 0!==b.maxWidth&&"number"==typeof b.maxWidth?b.maxWidth:i,s=void 0!==b.lineHeight&&"number"==typeof b.lineHeight?b.lineHeight:s,
c=void 0!==b.dx&&"number"==typeof b.dx?b.dx:c;
}
for(var f=r.text.split(""),h="",d=[],g=0,m=f.length;g<m;g++){p(e,{
text:h,
size:r.size})<=
i&&p(e,{
text:h+f[g],
size:r.size})<=
i?(h+=f[g],g==f.length-1&&d.push(h)):(d.push(h),h=f[g]);}
o.default.log("循环出的文本数组:"+JSON.stringify(d));
for(var y=u>=0&&u<d.length?u:d.length,_=0;_<y;_++){
var S=d[_];
_==y-1&&y<d.length&&(S=S.substring(0,S.length-1)+"...");
var O=v(v({},r),{},{
text:S,
dx:0===_?r.dx:c>=0?c:r.dx,
dy:r.dy+_*s,
textLength:p(e,{
text:S,
size:r.size})});


o.default.log("重新组成的文本对象:"+JSON.stringify(O)),t.push(O);
}
}else t.push(r);
}
o.default.log("绘制文本新数组:"+JSON.stringify(t)),function(e,l){
if(o.default.log("准备绘制文本方法, texts:"+JSON.stringify(l)),l&&o.default.isArray(l)){
if(o.default.log("准备绘制文本方法, 是数组"),l.length>0)for(var a=0;a<l.length;a++){w(e,l[a]);}
}else o.default.log("准备绘制文本方法, 不是数组"),w(e,l);
}(e,t);
}
function w(e,l){
if(o.default.log("进入绘制文本方法, textItem:"+JSON.stringify(l)),l&&o.default.isObject(l)&&l.text){
if(e.font=function(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};
if(e.font&&"string"==typeof e.font)return o.default.log(e.font),e.font;
var l="normal",a="normal",t="normal",n=e.size||10,r="sans-serif";
return n=Math.ceil(Number(n)),e.fontStyle&&"string"==typeof e.fontStyle&&(l=e.fontStyle.trim()),
e.fontVariant&&"string"==typeof e.fontVariant&&(a=e.fontVariant.trim()),
!e.fontWeight||"string"!=typeof e.fontWeight&&"number"!=typeof e.fontWeight||(t=e.fontWeight.trim()),
e.fontFamily&&"string"==typeof e.fontFamily&&(r=e.fontFamily.trim()),l+" "+a+" "+t+" "+n+"px "+r;
}(l),e.setFillStyle(l.color),e.setGlobalAlpha(l.alpha),e.setTextAlign(l.textAlign),
e.setTextBaseline(l.textBaseline),e.fillText(l.text,l.dx,l.dy),l.lineThrough&&l.lineThrough instanceof Object){
o.default.log("有删除线");
var a,t,n=l.lineThrough;
switch(n.alpha=void 0!==n.alpha?n.alpha:l.alpha,n.style=n.style||l.color,
n.width=void 0!==n.width?n.width:l.size/10,n.cap=void 0!==n.cap?n.cap:"butt",
o.default.log("删除线对象:"+JSON.stringify(n)),e.setGlobalAlpha(n.alpha),e.setStrokeStyle(n.style),
e.setLineWidth(n.width),e.setLineCap(n.cap),l.textAlign){
case"left":
a=l.dx;
break;

case"center":
a=l.dx-l.textLength/2;
break;

default:
a=l.dx-l.textLength;}

switch(l.textBaseline){
case"top":
t=l.dy+.5*l.size;
break;

case"middle":
t=l.dy;
break;

default:
t=l.dy-.5*l.size;}

e.beginPath(),e.moveTo(a,t),e.lineTo(a+l.textLength,t),e.stroke(),e.closePath(),
o.default.log("删除线完毕");
}
e.setGlobalAlpha(1),e.font="10px sans-serif";
}
}
function _(e,l){
if(o.default.log("进入图片绘制准备阶段:"+JSON.stringify(l)),l&&o.default.isArray(l)){
if(l.length>0)for(var a=0;a<l.length;a++){S(e,l[a]);}
}else S(e,l);
}
function S(e,l){
o.default.log("进入绘制图片方法, img:"+JSON.stringify(l)),l.url&&(l.circleSet?function(e,l){
o.default.log("进入绘制圆形图片方法, obj:"+JSON.stringify(l)),e.save();
var a,t,n,u=l.dx,i=l.dy,s=l.dWidth,c=l.dHeight,v=l.circleSet;
l.imageInfo,"object"===(0,r.default)(v)&&(a=v.x,t=v.y,n=v.r),n||(n=(s>c?c:s)/2),
a=a?u+a:(u||0)+n,t=t?i+t:(i||0)+n,e.beginPath(),e.arc(a,t,n,0,2*Math.PI,!1),
e.closePath(),e.fillStyle="#FFFFFF",e.fill(),e.clip(),O(e,l),o.default.log("默认图片绘制完毕"),
e.restore();
}(e,l):l.roundRectSet?function(e,l){
o.default.log("进入绘制矩形图片方法, obj:"+JSON.stringify(l)),e.save();
var a,t=l.dx,n=l.dy,u=l.dWidth,i=l.dHeight,s=l.roundRectSet;
l.imageInfo,"object"===(0,r.default)(s)&&(a=s.r),u<2*(a=a||.1*u)&&(a=u/2),
i<2*a&&(a=i/2),e.beginPath(),e.moveTo(t+a,n),e.arcTo(t+u,n,t+u,n+i,a),
e.arcTo(t+u,n+i,t,n+i,a),e.arcTo(t,n+i,t,n,a),e.arcTo(t,n,t+u,n,a),
e.closePath(),e.fillStyle="#FFFFFF",e.fill(),e.clip(),O(e,l),e.restore(),
o.default.log("进入绘制矩形图片方法, 绘制完毕");
}(e,l):O(e,l));
}
function O(e,l){
o.default.log("进入绘制默认图片方法, img:"+JSON.stringify(l)),l.url&&(o.default.log("进入绘制默认图片方法, 有url"),
l.dWidth&&l.dHeight&&l.sx&&l.sy&&l.sWidth&&l.sHeight?(o.default.log("进入绘制默认图片方法, 绘制第一种方案"),
e.drawImage(l.url,l.dx||0,l.dy||0,l.dWidth||!1,l.dHeight||!1,l.sx||!1,l.sy||!1,l.sWidth||!1,l.sHeight||!1)):l.dWidth&&l.dHeight?(o.default.log("进入绘制默认图片方法, 绘制第二种方案"),
e.drawImage(l.url,l.dx||0,l.dy||0,l.dWidth||!1,l.dHeight||!1)):(o.default.log("进入绘制默认图片方法, 绘制第三种方案"),
e.drawImage(l.url,l.dx||0,l.dy||0))),o.default.log("进入绘制默认图片方法, 绘制完毕");
}
function x(e,l){
o.default.showLoading("正在生成二维码");
for(var a=[],t={
text:l.text||"",
size:l.size||200,
background:l.background||"#ffffff",
foreground:l.foreground||"#000000",
pdground:l.pdground||"#000000",
correctLevel:l.correctLevel||3,
image:l.image||"",
imageSize:l.imageSize||40,
dx:l.dx||0,
dy:l.dy||0},
n=null,r=0,u=0,i=a.length;u<i;u++){if(r=u,a[u].text==t.text&&a[u].text.correctLevel==t.correctLevel){
n=a[u].obj;
break;
}}
r==i&&(n=new s.default(t.text,t.correctLevel),a.push({
text:t.text,
correctLevel:t.correctLevel,
obj:n}));

for(var c=function c(e){
var l=e.options;
return l.pdground&&(e.row>1&&e.row<5&&e.col>1&&e.col<5||e.row>e.count-6&&e.row<e.count-2&&e.col>1&&e.col<5||e.row>1&&e.row<5&&e.col>e.count-6&&e.col<e.count-2)?l.pdground:l.foreground;
},v=n.getModuleCount(),b=t.size,f=t.imageSize,h=(b/v).toPrecision(4),d=(b/v).toPrecision(4),p=0;p<v;p++){for(var g=0;g<v;g++){
var m=Math.ceil((g+1)*h)-Math.floor(g*h),y=Math.ceil((p+1)*h)-Math.floor(p*h),w=c({
row:p,
col:g,
count:v,
options:t});

e.setFillStyle(n.modules[p][g]?w:t.background),e.fillRect(t.dx+Math.round(g*h),t.dy+Math.round(p*d),m,y);
}}
if(t.image){
var _=t.dx+Number(((b-f)/2).toFixed(2)),S=t.dy+Number(((b-f)/2).toFixed(2));
(function(e,l,a,n,r,u,i,o,s){
e.setLineWidth(6),e.setFillStyle(t.background),e.setStrokeStyle(t.background),
e.beginPath(),e.moveTo(l+2,a),e.arcTo(l+n,a,l+n,a+2,2),e.arcTo(l+n,a+r,l+n-2,a+r,2),
e.arcTo(l,a+r,l,a+r-2,2),e.arcTo(l,a,l+2,a,2),e.closePath(),e.fill(),
e.stroke();
})(e,_,S,f,f),e.drawImage(t.image,_,S,f,f);
}
o.default.hideLoading();
}
function k(e){
e.backgroundImage;
var l=e.type;
return new Promise(function(){
var a=(0,i.default)(n.default.mark(function a(t,r){
var u,i,s,c,b,f,h,d;
return n.default.wrap(function(a){
for(;;){switch(a.prev=a.next){
case 0:
if(a.prev=0,o.default.showLoading("正在获取海报背景图"),u=A(l),o.default.log("获取的缓存:"+JSON.stringify(u)),
!(u&&u.path&&u.name)){
a.next=53;
break;
}
return o.default.log("海报有缓存, 准备获取后端背景图进行对比"),a.next=8,o.default.getPosterUrl(e);

case 8:
if(i=a.sent,o.default.log("准备对比name是否相同"),u.name!==o.default.fileNameInPath(i)){
a.next=45;
break;
}
return o.default.log("name相同, 判断该背景图是否存在于本地"),a.next=14,o.default.checkFile_PromiseFc(u.path);

case 14:
if(!(a.sent>=0)){
a.next=37;
break;
}
return o.default.log("海报save路径存在, 对比宽高信息, 存储并输出"),a.next=19,o.default.getImageInfo_PromiseFc(u.path);

case 19:
if(s=a.sent,c=v({},u),u.width&&u.height&&u.width===s.width&&u.height===s.height){
a.next=30;
break;
}
return o.default.log("宽高对比不通过， 重新获取"),a.next=25,E(e,i);

case 25:
b=a.sent,o.default.hideLoading(),t(b),a.next=35;
break;

case 30:
o.default.log("宽高对比通过, 再次存储, 并返回路径"),c=v(v({},u),{},{
width:s.width,
height:s.height}),
T(l,v({},c)),o.default.hideLoading(),t(c);

case 35:
a.next=43;
break;

case 37:
return o.default.log("海报save路径不存在, 重新获取海报"),a.next=40,E(e,i);

case 40:
f=a.sent,o.default.hideLoading(),t(f);

case 43:
a.next=51;
break;

case 45:
return o.default.log("name不相同, 重新获取海报"),a.next=48,E(e,i);

case 48:
h=a.sent,o.default.hideLoading(),t(h);

case 51:
a.next=59;
break;

case 53:
return o.default.log("海报背景图没有缓存, 准备获取海报背景图"),a.next=56,E(e);

case 56:
d=a.sent,o.default.hideLoading(),t(d);

case 59:
a.next=67;
break;

case 61:
a.prev=61,a.t0=a.catch(0),o.default.hideLoading(),o.default.showToast("获取分享用户背景图失败:"+JSON.stringify(a.t0)),
o.default.log(JSON.stringify(a.t0)),r(a.t0);

case 67:
case"end":
return a.stop();}}

},a,null,[[0,61]]);
}));
return function(e,l){
return a.apply(this,arguments);
};
}());
}
function A(e){
return o.default.getStorageSync(M(e));
}
function P(e){
var l=M(e),a=o.default.getStorageSync(l);
a&&a.path&&(o.default.removeSavedFile(a.path),o.default.removeStorageSync(l));
}
function T(e,l){
o.default.setStorage(M(e),l);
}
function M(e){
return"ShrePosterBackground_"+(e||"default");
}
function E(e,l){
e.backgroundImage;
var a=e.type;
return o.default.log("获取分享背景图, 尝试清空本地数据"),P(a),new Promise(function(){
var t=(0,i.default)(n.default.mark(function t(r,u){
var i,s,c,b,f,h,d,p;
return n.default.wrap(function(t){
for(;;){switch(t.prev=t.next){
case 0:
if(t.prev=0,o.default.showLoading("正在下载海报背景图"),!l){
t.next=24;
break;
}
return o.default.log("有从后端获取的背景图片路径"),o.default.log("尝试下载并保存背景图"),i=o.default.fileNameInPath(l),
t.next=8,o.default.downLoadAndSaveFile_PromiseFc(l);

case 8:
if(!(s=t.sent)){
t.next=20;
break;
}
return o.default.log("下载并保存背景图成功:"+s),t.next=13,o.default.getImageInfo_PromiseFc(s);

case 13:
c=t.sent,b={
path:s,
width:c.width,
height:c.height,
name:i},
T(a,v({},b)),o.default.hideLoading(),r(b),t.next=22;
break;

case 20:
o.default.hideLoading(),u("not find savedFilePath");

case 22:
t.next=48;
break;

case 24:
return o.default.log("没有从后端获取的背景图片路径, 尝试从后端获取背景图片路径"),t.next=27,o.default.getPosterUrl(e);

case 27:
return f=t.sent,o.default.log("尝试下载并保存背景图:"+f),t.next=31,o.default.downLoadAndSaveFile_PromiseFc(f);

case 31:
if(!(h=t.sent)){
t.next=46;
break;
}
return o.default.log("下载并保存背景图成功:"+h),t.next=36,o.default.getImageInfo_PromiseFc(h);

case 36:
d=t.sent,o.default.log("获取图片信息成功"),p={
path:h,
width:d.width,
height:d.height,
name:o.default.fileNameInPath(f)},
o.default.log("拼接背景图信息对象成功:"+JSON.stringify(p)),T(a,v({},p)),o.default.hideLoading(),
o.default.log("返回背景图信息对象"),r(v({},p)),t.next=48;
break;

case 46:
o.default.hideLoading(),u("not find savedFilePath");

case 48:
t.next=53;
break;

case 50:
t.prev=50,t.t0=t.catch(0),u(t.t0);

case 53:
case"end":
return t.stop();}}

},t,null,[[0,50]]);
}));
return function(e,l){
return t.apply(this,arguments);
};
}());
}
e.exports={
getSharePoster:function getSharePoster(e){
return new Promise(function(){
var l=(0,i.default)(n.default.mark(function l(a,t){
var r,u;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
return l.prev=0,l.next=3,b(e);

case 3:
r=l.sent,a(r),l.next=21;
break;

case 7:
return l.prev=7,l.t0=l.catch(0),P(e.type),l.prev=10,o.default.log("------------清除缓存后, 开始第二次尝试------------"),
l.next=14,b(e);

case 14:
u=l.sent,a(u),l.next=21;
break;

case 18:
l.prev=18,l.t1=l.catch(10),t(l.t1);

case 21:
case"end":
return l.stop();}}

},l,null,[[0,7],[10,18]]);
}));
return function(e,a){
return l.apply(this,arguments);
};
}());
},
setText:h,
setImage:g,
drawText:y,
drawImage:_,
drawQrCode:x};

}).call(this,a("543d").default);
},
"970b":function b(e,l){
e.exports=function(e,l){
if(!(e instanceof l))throw new TypeError("Cannot call a class as a function");
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"97cf":function cf(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,l.default=[[{
label:"市辖区",
value:"1101"}],
[{
label:"市辖区",
value:"1201"}],
[{
label:"石家庄市",
value:"1301"},
{
label:"唐山市",
value:"1302"},
{
label:"秦皇岛市",
value:"1303"},
{
label:"邯郸市",
value:"1304"},
{
label:"邢台市",
value:"1305"},
{
label:"保定市",
value:"1306"},
{
label:"张家口市",
value:"1307"},
{
label:"承德市",
value:"1308"},
{
label:"沧州市",
value:"1309"},
{
label:"廊坊市",
value:"1310"},
{
label:"衡水市",
value:"1311"}],
[{
label:"太原市",
value:"1401"},
{
label:"大同市",
value:"1402"},
{
label:"阳泉市",
value:"1403"},
{
label:"长治市",
value:"1404"},
{
label:"晋城市",
value:"1405"},
{
label:"朔州市",
value:"1406"},
{
label:"晋中市",
value:"1407"},
{
label:"运城市",
value:"1408"},
{
label:"忻州市",
value:"1409"},
{
label:"临汾市",
value:"1410"},
{
label:"吕梁市",
value:"1411"}],
[{
label:"呼和浩特市",
value:"1501"},
{
label:"包头市",
value:"1502"},
{
label:"乌海市",
value:"1503"},
{
label:"赤峰市",
value:"1504"},
{
label:"通辽市",
value:"1505"},
{
label:"鄂尔多斯市",
value:"1506"},
{
label:"呼伦贝尔市",
value:"1507"},
{
label:"巴彦淖尔市",
value:"1508"},
{
label:"乌兰察布市",
value:"1509"},
{
label:"兴安盟",
value:"1522"},
{
label:"锡林郭勒盟",
value:"1525"},
{
label:"阿拉善盟",
value:"1529"}],
[{
label:"沈阳市",
value:"2101"},
{
label:"大连市",
value:"2102"},
{
label:"鞍山市",
value:"2103"},
{
label:"抚顺市",
value:"2104"},
{
label:"本溪市",
value:"2105"},
{
label:"丹东市",
value:"2106"},
{
label:"锦州市",
value:"2107"},
{
label:"营口市",
value:"2108"},
{
label:"阜新市",
value:"2109"},
{
label:"辽阳市",
value:"2110"},
{
label:"盘锦市",
value:"2111"},
{
label:"铁岭市",
value:"2112"},
{
label:"朝阳市",
value:"2113"},
{
label:"葫芦岛市",
value:"2114"}],
[{
label:"长春市",
value:"2201"},
{
label:"吉林市",
value:"2202"},
{
label:"四平市",
value:"2203"},
{
label:"辽源市",
value:"2204"},
{
label:"通化市",
value:"2205"},
{
label:"白山市",
value:"2206"},
{
label:"松原市",
value:"2207"},
{
label:"白城市",
value:"2208"},
{
label:"延边朝鲜族自治州",
value:"2224"}],
[{
label:"哈尔滨市",
value:"2301"},
{
label:"齐齐哈尔市",
value:"2302"},
{
label:"鸡西市",
value:"2303"},
{
label:"鹤岗市",
value:"2304"},
{
label:"双鸭山市",
value:"2305"},
{
label:"大庆市",
value:"2306"},
{
label:"伊春市",
value:"2307"},
{
label:"佳木斯市",
value:"2308"},
{
label:"七台河市",
value:"2309"},
{
label:"牡丹江市",
value:"2310"},
{
label:"黑河市",
value:"2311"},
{
label:"绥化市",
value:"2312"},
{
label:"大兴安岭地区",
value:"2327"}],
[{
label:"市辖区",
value:"3101"}],
[{
label:"南京市",
value:"3201"},
{
label:"无锡市",
value:"3202"},
{
label:"徐州市",
value:"3203"},
{
label:"常州市",
value:"3204"},
{
label:"苏州市",
value:"3205"},
{
label:"南通市",
value:"3206"},
{
label:"连云港市",
value:"3207"},
{
label:"淮安市",
value:"3208"},
{
label:"盐城市",
value:"3209"},
{
label:"扬州市",
value:"3210"},
{
label:"镇江市",
value:"3211"},
{
label:"泰州市",
value:"3212"},
{
label:"宿迁市",
value:"3213"}],
[{
label:"杭州市",
value:"3301"},
{
label:"宁波市",
value:"3302"},
{
label:"温州市",
value:"3303"},
{
label:"嘉兴市",
value:"3304"},
{
label:"湖州市",
value:"3305"},
{
label:"绍兴市",
value:"3306"},
{
label:"金华市",
value:"3307"},
{
label:"衢州市",
value:"3308"},
{
label:"舟山市",
value:"3309"},
{
label:"台州市",
value:"3310"},
{
label:"丽水市",
value:"3311"}],
[{
label:"合肥市",
value:"3401"},
{
label:"芜湖市",
value:"3402"},
{
label:"蚌埠市",
value:"3403"},
{
label:"淮南市",
value:"3404"},
{
label:"马鞍山市",
value:"3405"},
{
label:"淮北市",
value:"3406"},
{
label:"铜陵市",
value:"3407"},
{
label:"安庆市",
value:"3408"},
{
label:"黄山市",
value:"3410"},
{
label:"滁州市",
value:"3411"},
{
label:"阜阳市",
value:"3412"},
{
label:"宿州市",
value:"3413"},
{
label:"六安市",
value:"3415"},
{
label:"亳州市",
value:"3416"},
{
label:"池州市",
value:"3417"},
{
label:"宣城市",
value:"3418"}],
[{
label:"福州市",
value:"3501"},
{
label:"厦门市",
value:"3502"},
{
label:"莆田市",
value:"3503"},
{
label:"三明市",
value:"3504"},
{
label:"泉州市",
value:"3505"},
{
label:"漳州市",
value:"3506"},
{
label:"南平市",
value:"3507"},
{
label:"龙岩市",
value:"3508"},
{
label:"宁德市",
value:"3509"}],
[{
label:"南昌市",
value:"3601"},
{
label:"景德镇市",
value:"3602"},
{
label:"萍乡市",
value:"3603"},
{
label:"九江市",
value:"3604"},
{
label:"新余市",
value:"3605"},
{
label:"鹰潭市",
value:"3606"},
{
label:"赣州市",
value:"3607"},
{
label:"吉安市",
value:"3608"},
{
label:"宜春市",
value:"3609"},
{
label:"抚州市",
value:"3610"},
{
label:"上饶市",
value:"3611"}],
[{
label:"济南市",
value:"3701"},
{
label:"青岛市",
value:"3702"},
{
label:"淄博市",
value:"3703"},
{
label:"枣庄市",
value:"3704"},
{
label:"东营市",
value:"3705"},
{
label:"烟台市",
value:"3706"},
{
label:"潍坊市",
value:"3707"},
{
label:"济宁市",
value:"3708"},
{
label:"泰安市",
value:"3709"},
{
label:"威海市",
value:"3710"},
{
label:"日照市",
value:"3711"},
{
label:"莱芜市",
value:"3712"},
{
label:"临沂市",
value:"3713"},
{
label:"德州市",
value:"3714"},
{
label:"聊城市",
value:"3715"},
{
label:"滨州市",
value:"3716"},
{
label:"菏泽市",
value:"3717"}],
[{
label:"郑州市",
value:"4101"},
{
label:"开封市",
value:"4102"},
{
label:"洛阳市",
value:"4103"},
{
label:"平顶山市",
value:"4104"},
{
label:"安阳市",
value:"4105"},
{
label:"鹤壁市",
value:"4106"},
{
label:"新乡市",
value:"4107"},
{
label:"焦作市",
value:"4108"},
{
label:"濮阳市",
value:"4109"},
{
label:"许昌市",
value:"4110"},
{
label:"漯河市",
value:"4111"},
{
label:"三门峡市",
value:"4112"},
{
label:"南阳市",
value:"4113"},
{
label:"商丘市",
value:"4114"},
{
label:"信阳市",
value:"4115"},
{
label:"周口市",
value:"4116"},
{
label:"驻马店市",
value:"4117"},
{
label:"省直辖县级行政区划",
value:"4190"}],
[{
label:"武汉市",
value:"4201"},
{
label:"黄石市",
value:"4202"},
{
label:"十堰市",
value:"4203"},
{
label:"宜昌市",
value:"4205"},
{
label:"襄阳市",
value:"4206"},
{
label:"鄂州市",
value:"4207"},
{
label:"荆门市",
value:"4208"},
{
label:"孝感市",
value:"4209"},
{
label:"荆州市",
value:"4210"},
{
label:"黄冈市",
value:"4211"},
{
label:"咸宁市",
value:"4212"},
{
label:"随州市",
value:"4213"},
{
label:"恩施土家族苗族自治州",
value:"4228"},
{
label:"省直辖县级行政区划",
value:"4290"}],
[{
label:"长沙市",
value:"4301"},
{
label:"株洲市",
value:"4302"},
{
label:"湘潭市",
value:"4303"},
{
label:"衡阳市",
value:"4304"},
{
label:"邵阳市",
value:"4305"},
{
label:"岳阳市",
value:"4306"},
{
label:"常德市",
value:"4307"},
{
label:"张家界市",
value:"4308"},
{
label:"益阳市",
value:"4309"},
{
label:"郴州市",
value:"4310"},
{
label:"永州市",
value:"4311"},
{
label:"怀化市",
value:"4312"},
{
label:"娄底市",
value:"4313"},
{
label:"湘西土家族苗族自治州",
value:"4331"}],
[{
label:"广州市",
value:"4401"},
{
label:"韶关市",
value:"4402"},
{
label:"深圳市",
value:"4403"},
{
label:"珠海市",
value:"4404"},
{
label:"汕头市",
value:"4405"},
{
label:"佛山市",
value:"4406"},
{
label:"江门市",
value:"4407"},
{
label:"湛江市",
value:"4408"},
{
label:"茂名市",
value:"4409"},
{
label:"肇庆市",
value:"4412"},
{
label:"惠州市",
value:"4413"},
{
label:"梅州市",
value:"4414"},
{
label:"汕尾市",
value:"4415"},
{
label:"河源市",
value:"4416"},
{
label:"阳江市",
value:"4417"},
{
label:"清远市",
value:"4418"},
{
label:"东莞市",
value:"4419"},
{
label:"中山市",
value:"4420"},
{
label:"潮州市",
value:"4451"},
{
label:"揭阳市",
value:"4452"},
{
label:"云浮市",
value:"4453"}],
[{
label:"南宁市",
value:"4501"},
{
label:"柳州市",
value:"4502"},
{
label:"桂林市",
value:"4503"},
{
label:"梧州市",
value:"4504"},
{
label:"北海市",
value:"4505"},
{
label:"防城港市",
value:"4506"},
{
label:"钦州市",
value:"4507"},
{
label:"贵港市",
value:"4508"},
{
label:"玉林市",
value:"4509"},
{
label:"百色市",
value:"4510"},
{
label:"贺州市",
value:"4511"},
{
label:"河池市",
value:"4512"},
{
label:"来宾市",
value:"4513"},
{
label:"崇左市",
value:"4514"}],
[{
label:"海口市",
value:"4601"},
{
label:"三亚市",
value:"4602"},
{
label:"三沙市",
value:"4603"},
{
label:"儋州市",
value:"4604"},
{
label:"省直辖县级行政区划",
value:"4690"}],
[{
label:"市辖区",
value:"5001"},
{
label:"县",
value:"5002"}],
[{
label:"成都市",
value:"5101"},
{
label:"自贡市",
value:"5103"},
{
label:"攀枝花市",
value:"5104"},
{
label:"泸州市",
value:"5105"},
{
label:"德阳市",
value:"5106"},
{
label:"绵阳市",
value:"5107"},
{
label:"广元市",
value:"5108"},
{
label:"遂宁市",
value:"5109"},
{
label:"内江市",
value:"5110"},
{
label:"乐山市",
value:"5111"},
{
label:"南充市",
value:"5113"},
{
label:"眉山市",
value:"5114"},
{
label:"宜宾市",
value:"5115"},
{
label:"广安市",
value:"5116"},
{
label:"达州市",
value:"5117"},
{
label:"雅安市",
value:"5118"},
{
label:"巴中市",
value:"5119"},
{
label:"资阳市",
value:"5120"},
{
label:"阿坝藏族羌族自治州",
value:"5132"},
{
label:"甘孜藏族自治州",
value:"5133"},
{
label:"凉山彝族自治州",
value:"5134"}],
[{
label:"贵阳市",
value:"5201"},
{
label:"六盘水市",
value:"5202"},
{
label:"遵义市",
value:"5203"},
{
label:"安顺市",
value:"5204"},
{
label:"毕节市",
value:"5205"},
{
label:"铜仁市",
value:"5206"},
{
label:"黔西南布依族苗族自治州",
value:"5223"},
{
label:"黔东南苗族侗族自治州",
value:"5226"},
{
label:"黔南布依族苗族自治州",
value:"5227"}],
[{
label:"昆明市",
value:"5301"},
{
label:"曲靖市",
value:"5303"},
{
label:"玉溪市",
value:"5304"},
{
label:"保山市",
value:"5305"},
{
label:"昭通市",
value:"5306"},
{
label:"丽江市",
value:"5307"},
{
label:"普洱市",
value:"5308"},
{
label:"临沧市",
value:"5309"},
{
label:"楚雄彝族自治州",
value:"5323"},
{
label:"红河哈尼族彝族自治州",
value:"5325"},
{
label:"文山壮族苗族自治州",
value:"5326"},
{
label:"西双版纳傣族自治州",
value:"5328"},
{
label:"大理白族自治州",
value:"5329"},
{
label:"德宏傣族景颇族自治州",
value:"5331"},
{
label:"怒江傈僳族自治州",
value:"5333"},
{
label:"迪庆藏族自治州",
value:"5334"}],
[{
label:"拉萨市",
value:"5401"},
{
label:"日喀则市",
value:"5402"},
{
label:"昌都市",
value:"5403"},
{
label:"林芝市",
value:"5404"},
{
label:"山南市",
value:"5405"},
{
label:"那曲地区",
value:"5424"},
{
label:"阿里地区",
value:"5425"}],
[{
label:"西安市",
value:"6101"},
{
label:"铜川市",
value:"6102"},
{
label:"宝鸡市",
value:"6103"},
{
label:"咸阳市",
value:"6104"},
{
label:"渭南市",
value:"6105"},
{
label:"延安市",
value:"6106"},
{
label:"汉中市",
value:"6107"},
{
label:"榆林市",
value:"6108"},
{
label:"安康市",
value:"6109"},
{
label:"商洛市",
value:"6110"}],
[{
label:"兰州市",
value:"6201"},
{
label:"嘉峪关市",
value:"6202"},
{
label:"金昌市",
value:"6203"},
{
label:"白银市",
value:"6204"},
{
label:"天水市",
value:"6205"},
{
label:"武威市",
value:"6206"},
{
label:"张掖市",
value:"6207"},
{
label:"平凉市",
value:"6208"},
{
label:"酒泉市",
value:"6209"},
{
label:"庆阳市",
value:"6210"},
{
label:"定西市",
value:"6211"},
{
label:"陇南市",
value:"6212"},
{
label:"临夏回族自治州",
value:"6229"},
{
label:"甘南藏族自治州",
value:"6230"}],
[{
label:"西宁市",
value:"6301"},
{
label:"海东市",
value:"6302"},
{
label:"海北藏族自治州",
value:"6322"},
{
label:"黄南藏族自治州",
value:"6323"},
{
label:"海南藏族自治州",
value:"6325"},
{
label:"果洛藏族自治州",
value:"6326"},
{
label:"玉树藏族自治州",
value:"6327"},
{
label:"海西蒙古族藏族自治州",
value:"6328"}],
[{
label:"银川市",
value:"6401"},
{
label:"石嘴山市",
value:"6402"},
{
label:"吴忠市",
value:"6403"},
{
label:"固原市",
value:"6404"},
{
label:"中卫市",
value:"6405"}],
[{
label:"乌鲁木齐市",
value:"6501"},
{
label:"克拉玛依市",
value:"6502"},
{
label:"吐鲁番市",
value:"6504"},
{
label:"哈密市",
value:"6505"},
{
label:"昌吉回族自治州",
value:"6523"},
{
label:"博尔塔拉蒙古自治州",
value:"6527"},
{
label:"巴音郭楞蒙古自治州",
value:"6528"},
{
label:"阿克苏地区",
value:"6529"},
{
label:"克孜勒苏柯尔克孜自治州",
value:"6530"},
{
label:"喀什地区",
value:"6531"},
{
label:"和田地区",
value:"6532"},
{
label:"伊犁哈萨克自治州",
value:"6540"},
{
label:"塔城地区",
value:"6542"},
{
label:"阿勒泰地区",
value:"6543"},
{
label:"自治区直辖县级行政区划",
value:"6590"}],
[{
label:"台北",
value:"6601"},
{
label:"高雄",
value:"6602"},
{
label:"基隆",
value:"6603"},
{
label:"台中",
value:"6604"},
{
label:"台南",
value:"6605"},
{
label:"新竹",
value:"6606"},
{
label:"嘉义",
value:"6607"},
{
label:"宜兰",
value:"6608"},
{
label:"桃园",
value:"6609"},
{
label:"苗栗",
value:"6610"},
{
label:"彰化",
value:"6611"},
{
label:"南投",
value:"6612"},
{
label:"云林",
value:"6613"},
{
label:"屏东",
value:"6614"},
{
label:"台东",
value:"6615"},
{
label:"花莲",
value:"6616"},
{
label:"澎湖",
value:"6617"}],
[{
label:"香港岛",
value:"6701"},
{
label:"九龙",
value:"6702"},
{
label:"新界",
value:"6703"}],
[{
label:"澳门半岛",
value:"6801"},
{
label:"氹仔岛",
value:"6802"},
{
label:"路环岛",
value:"6803"},
{
label:"路氹城",
value:"6804"}],
[{
label:"钓鱼岛",
value:"6901"}]];

},
"9b42":function b42(e,l){
e.exports=function(e,l){
var a=null==e?null:"undefined"!=typeof Symbol&&e[Symbol.iterator]||e["@@iterator"];
if(null!=a){
var t,n,r,u,i=[],o=!0,s=!1;
try{
if(r=(a=a.call(e)).next,0===l){
if(Object(a)!==a)return;
o=!1;
}else for(;!(o=(t=r.call(a)).done)&&(i.push(t.value),i.length!==l);o=!0){;}
}catch(e){
s=!0,n=e;
}finally{
try{
if(!o&&null!=a.return&&(u=a.return(),Object(u)!==u))return;
}finally{
if(s)throw n;
}
}
return i;
}
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
"9d4d":function d4d(e,l){
e.exports={
list:["GET","/shop/products"],
show:["GET","/shop/products/{uuid}"],
cart:{
index:["GET","/cart-items"],
store:["POST","/cart-items"]}};


},
"9eae":function eae(e,l,a){
var t={
"./address.js":"3e28",
"./app.js":"3d2e",
"./category.js":"93db",
"./clockIn.js":"7258",
"./core.js":"b93f",
"./groupon.js":"c58f",
"./order.js":"baa5",
"./product.js":"9d4d",
"./service.js":"a640",
"./trackRecord.js":"04fe",
"./user.js":"8094"};

function n(e){
var l=r(e);
return a(l);
}
function r(e){
if(!a.o(t,e)){
var l=new Error("Cannot find module '"+e+"'");
throw l.code="MODULE_NOT_FOUND",l;
}
return t[e];
}
n.keys=function(){
return Object.keys(t);
},n.resolve=r,e.exports=n,n.id="9eae";
},
a395:function a395(e,l,a){
var t=a("7037").default,n=a("e50d");
e.exports=function(e){
var l=n(e,"string");
return"symbol"===t(l)?l:String(l);
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
a640:function a640(e,l){
e.exports={
show:["GET","/service"],
order:{
show:["GET","/service-orders/{uuid}"],
list:["GET","/service-orders"],
preview:["POST","/service-preview-order"],
store:["POST","/service-orders"]}};


},
a72c:function a72c(e,l,a){
var t=a("970b"),n=a("5bc3"),r=a("ec6a"),u=function(){
"use strict";
function e(){
var l=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};
t(this,e),this.styles=Object.assign({},l);
}
return n(e,[{
key:"getStyle",
value:function value(e){
var l="";
return e=e.replace(/<[sS][tT][yY][lL][eE][\s\S]*?>([\s\S]*?)<\/[sS][tT][yY][lL][eE][\s\S]*?>/g,function(e,a){
return l+=a,"";
}),this.styles=new i(l,this.styles).parse(),e;
}},
{
key:"parseCss",
value:function value(e){
return new i(e,{},!0).parse();
}},
{
key:"match",
value:function value(e,l){
var a,t=(a=this.styles[e])?a+";":"";
if(l.class)for(var n=l.class.split(" "),r=0;r<n.length;r++){(a=this.styles["."+n[r]])&&(t+=a+";");}
return(a=this.styles["#"+l.id])&&(t+=a),t;
}}]),
e;
}();
e.exports=u;
var i=function(){
"use strict";
function e(l,a,n){
if(t(this,e),this.data=l,this.res=a,!n)for(var u in r.userAgentStyles){a[u]?a[u]=r.userAgentStyles[u]+";"+a[u]:a[u]=r.userAgentStyles[u];}
this._floor=0,this._i=0,this._list=[],this._comma=!1,this._sectionStart=0,
this._state=this.Space;
}
return n(e,[{
key:"parse",
value:function value(){
for(;this._i<this.data.length;this._i++){this._state(this.data[this._i]);}
return this.res;
}},
{
key:"Space",
value:function value(e){
"."==e||"#"==e||e>="a"&&e<="z"||e>="A"&&e<="Z"?(this._sectionStart=this._i,
this._state=this.StyleName):"/"==e&&"*"==this.data[this._i+1]?this.Comment():r.blankChar[e]||";"==e||(this._state=this.Ignore);
}},
{
key:"Comment",
value:function value(){
this._i=this.data.indexOf("*/",this._i)+1,this._i||(this._i=this.data.length),
this._state=this.Space;
}},
{
key:"Ignore",
value:function value(e){
"{"==e?this._floor++:"}"==e&&--this._floor<=0&&(this._list=[],this._state=this.Space);
}},
{
key:"StyleName",
value:function value(e){
r.blankChar[e]?(this._list.push(this.data.substring(this._sectionStart,this._i)),
this._state=this.NameSpace):"{"==e?(this._list.push(this.data.substring(this._sectionStart,this._i)),
this._floor=1,this._sectionStart=this._i+1,this.Content()):","==e?(this._list.push(this.data.substring(this._sectionStart,this._i)),
this._sectionStart=this._i+1,this._comma=!0):e>="a"&&e<="z"||e>="A"&&e<="Z"||e>="0"&&e<="9"||"."==e||"#"==e||"-"==e||"_"==e||(this._state=this.Ignore);
}},
{
key:"NameSpace",
value:function value(e){
"{"==e?(this._floor=1,this._sectionStart=this._i+1,this.Content()):","==e?(this._comma=!0,
this._sectionStart=this._i+1,this._state=this.StyleName):r.blankChar[e]||(this._comma?(this._state=this.StyleName,
this._sectionStart=this._i,this._i--,this._comma=!1):this._state=this.Ignore);
}},
{
key:"Content",
value:function value(){
this._i=this.data.indexOf("}",this._i),-1==this._i&&(this._i=this.data.length);
for(var e=this.data.substring(this._sectionStart,this._i).trim(),l=e.length,a=[e[--l]];--l>0;){r.blankChar[e[l]]&&r.blankChar[a[0]]||(";"!=e[l]&&":"!=e[l]||!r.blankChar[a[0]]?a.unshift(e[l]):a[0]=e[l]);}
for(a.unshift(e[0]),e=a.join(""),l=this._list.length;l--;){this.res[this._list[l]]=(this.res[this._list[l]]||"")+e;}
this._list=[],this._state=this.Space;
}}]),
e;
}();
},
a7b1:function a7b1(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.createAddress=function(e){
return(0,n.default)({
url:r,
method:"post",
data:e});

},l.deleteAddress=function(e){
return(0,n.default)({
url:"".concat(r,"/").concat(e),
method:"delete"});

},l.getAddressList=function(e){
return(0,n.default)({
url:r,
method:"get",
data:e});

},l.updateAddress=function(e,l){
return(0,n.default)({
url:"".concat(r,"/").concat(l),
method:"put",
data:{
type:"update",
attributes:e}});


};
var n=t(a("0863")),r="/addresses";
},
a8b1:function a8b1(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var n=t(a("2eee")),r=t(a("9523")),u=t(a("c973")),i=(t(a("0863")),a("cbb4")),o=a("c51d");
function s(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function c(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?s(Object(a),!0).forEach(function(l){
(0,r.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):s(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var v={
state:{
token:(0,i.$getStorage)("token")||"",
userInfo:(0,i.$getStorage)("userInfo")||{},
personalSettings:(0,i.$getStorage)("personalSettings")||{},
wechatInfo:(0,i.$getStorage)("wechatInfo")||{},
scoreRedpack:(0,i.$getStorage)("scoreRedpack")||[],
click_id:(0,i.$getStorage)("click_id")||"",
weixinadinfo:(0,i.$getStorage)("weixinadinfo")||""},

mutations:{
SET_TOKEN:function SET_TOKEN(e,l){
e.token=l,(0,i.$setStorage)("token",l);
},
SET_USER_INFO:function SET_USER_INFO(e,l){
e.userInfo=l,(0,i.$setStorage)("userInfo",l);
},
SET_WECHAT_INFO:function SET_WECHAT_INFO(e,l){
e.wechatInfo=l,(0,i.$setStorage)("wechatInfo",l);
},
SET_PERSONAL_SETTINGS:function SET_PERSONAL_SETTINGS(e,l){
e.personalSettings[l.key]=l.value;
},
SET_SCOREDPACK:function SET_SCOREDPACK(e,l){
e.scoreRedpack=l,(0,i.$setStorage)("scoreRedpack",l);
},
SET_CLICK_ID:function SET_CLICK_ID(e,l){
e.click_id=l,(0,i.$setStorage)("click_id",l);
},
SET_WX_INFO:function SET_WX_INFO(e,l){
e.weixinadinfo=l,(0,i.$setStorage)("weixinadinfo",l);
}},

actions:{
userSignIn:function userSignIn(e){
return(0,u.default)(n.default.mark(function l(){
var a;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
return l.next=2,(0,o.userSignIn)();

case 2:
return a=l.sent,l.next=5,e.dispatch("getUserInfo");

case 5:
return l.abrupt("return",a);

case 6:
case"end":
return l.stop();}}

},l);
}))();
},
setPersonalSettings:function setPersonalSettings(e,l){
e.commit("SET_PERSONAL_SETTINGS",l),(0,i.$setStorage)("personalSettings",e.state.personalSettings);
},
getUserInfo:function getUserInfo(e){
return(0,u.default)(n.default.mark(function l(){
var a;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
return l.next=2,(0,o.getUserInfo)().catch(function(l){
e.commit("SET_USER_INFO",""),e.commit("SET_TOKEN","");
});

case 2:
if(a=l.sent){
l.next=5;
break;
}
return l.abrupt("return");

case 5:
return e.commit("SET_USER_INFO",a.data.user),l.next=8,e.dispatch("getCountScoreRedpack");

case 8:
case"end":
return l.stop();}}

},l);
}))();
},
getCountScoreRedpack:function getCountScoreRedpack(e){
return(0,u.default)(n.default.mark(function l(){
var a;
return n.default.wrap(function(l){
for(;;){switch(l.prev=l.next){
case 0:
return l.next=2,(0,o.getCountScoreRedpack)().catch(function(l){
e.commit("SET_SCOREDPACK","");
});

case 2:
if(a=l.sent){
l.next=5;
break;
}
return l.abrupt("return");

case 5:
return e.commit("SET_SCOREDPACK",a.data.info),l.abrupt("return",a);

case 7:
case"end":
return l.stop();}}

},l);
}))();
},
logout:function logout(e){
(0,i.$setStorage)("token",""),e.commit("SET_USER_INFO",""),e.commit("SET_TOKEN",""),
e.commit("SET_SCOREDPACK","");
},
login:function login(e,l){
return(0,u.default)(n.default.mark(function a(){
var t,r,u,s;
return n.default.wrap(function(a){
for(;;){switch(a.prev=a.next){
case 0:
return t=l.type,r=l.params,u=(0,i.$getStorage)("inviter")||"",s=(0,
i.$getStorage)("invite_node")||"",a.abrupt("return",(0,o.userLogin)(t,c(c({},r),{},{
inviter:u,
invite_node:s})).
then(function(l){
return console.log("登陆成功"+l),l.data.token&&e.commit("SET_TOKEN",l.data.token),
l;
}));

case 4:
case"end":
return a.stop();}}

},a);
}))();
}}};


l.default=v;
},
b17c:function b17c(e,l,a){
var t=a("4a4b"),n=a("6f8f");
function r(l,a,u){
return n()?(e.exports=r=Reflect.construct.bind(),e.exports.__esModule=!0,
e.exports.default=e.exports):(e.exports=r=function r(e,l,a){
var n=[null];
n.push.apply(n,l);
var r=new(Function.bind.apply(e,n))();
return a&&t(r,a.prototype),r;
},e.exports.__esModule=!0,e.exports.default=e.exports),r.apply(null,arguments);
}
e.exports=r,e.exports.__esModule=!0,e.exports.default=e.exports;
},
b1a6:function b1a6(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAACRlBMVEUi9//n/v8h9v4AAAAh8vsAAAP///8h9f0g6vIh8/wg7vac+fsg7fUf5u4h8fke2+Mg8Pgf5Owf4ekg7PQe3uYd1Nsf6PAczNMf4+sd2N8d2eEe4OgZt70Yr7Ucz9Ye3eUbxs0Zub8av8YXp60byM8YsbcZusEYs7kEBQUd194d1t0bytEawskavsUcztUfio4c0dgXq7Eawcgc0tkXqrAbx84axMsYtbsBBQcXrbMfiY4ZvMMXqa8CExQJCgr29vYfiI0ehYoGLTEBDQ8BCAsegocRERERhIoPb3QIPUH9/f2Z9fe5ubmMl5gbbXEGMjYEHyEDGhwODg7+/v7o6OgceX4pKSkYJicDHB8CFxns7OzBwcEQe4ANYWVbW1tQUFAIQkYEIycVFRWV7/GK3d+np6cVnKIUlJoRf4V8fHwMXWELUlVFRUUpQkI/Pz8HNzocLS4THx8PGBj4+Pjy8vLW1tbR0dFur7EVn6Wbm5sTj5RWiouEhIQQeX4cc3h3d3cObHENZ2tpaWkaZWliYmILVloKTVBJSUkwMDAFKCru7u7g4ODa2tqG19mD0dPLy8vFxcVnpaeWlpYSio+Hh4cef4QdfYJOfX4Pcnhubm5AaGlfX185WltYWFgJSE0uS0s5OTk2NjYiIiIeHh6R6evl5eWO4+V9yMl2vb4Ul52RkZFakJGBgYFQgIF0dHQ8YWJUVFQyUVG0tLRinp9fmJlShIVJdndFb3AMWV7Hx8dxtrivr68WpKqfn5+Li4sgNDULERFEbW4l/367AAAdpElEQVR42uxaCXMVRRBep+dlZyd7vzMXj0QMj0QQUSCKCZpgOJWAF4iCgAegyOWBCCIWhxcKggeH4n0i4n1f/8zp7n0vL1apWCxUtOii6J2ZTdU3M9980937rIv+u3bDBfBnZhfAXwA/GuwC+DO0C+D/R+BHr3kFB52MsuS6CrZxdi5HzbLS1mXWqDUZRZK8CtE5mpxwXPMfdlvWtdboNSnJCZscIpaOQOcoHzvCl61RanYCXJLXOepTgS9Mp5paENh5izU6TXUJ4kocorcDT+JUvLLCXp+JZF1vjU6LlCaYLjlb+hZ5A1oIYpC2rdF7YCU7ByEz3zV3FLIC+e4VndEJPhTkfOa9S4opss0RsX+aR4o5JvBGJXivHPIae4JoHxF43UJ8ly5SxyhmwbKutkadCd9jhrvspCTaCIkkYr5LG50ajVIpRN0z4/dpHnYhTy1vHCqmHd1ojTJzGbPUBFuTYto6n7URdXdEitkcEP3D0SaVOk/oRawc9DEretZTIxQzayYizgvn7Tv+jUJqzRGMrimmYGc7zHdF/YUWJc6H2myFGf+G8MNO0rNUNjo7iiVJ0NQSDqv26HzovLMSFooz5Iwk5ytmfxBSZ7kssLOr5BDqHPFdZt3zQZuPAODeM8MeuIKuIw5kdKKYXpZciNRhxczy1rjXWefY5BCcgCXuP/OeI3Y+lzXFtHlECnIuHwjVUqTZBOdcKrfBbfM3wv2ZGWdAdkGOjPFLV5FUqrGCqDIRNUiEEyNNEcK5Don1IjjY+CFkMv/Ee6QKAbSJ9qSYwplQou68F6IrRTE6R2qL5nSuaXMINjY2Nn4OkNn69+wKIlp3V0lqJqGwr0YoZmj+8WM2ss55SJzthUcN+P0VyCzRf0sa6dfznZK9xHGMqT3JElRG75Q7W845+NnwayPatwCZNWca03Du6qiI+TSG0KtigM4txpJUv9k7x+Czqhd+IPDv/gaZgdxf5nwBg45tck0uMToONAl75NPMIo8PUWiGeIcKZwde/smcEaMXD+2A9Y1snwBAvxhJlKqFzSVlnBUqzRGCb7GvFQ8SLhVsckqR8pcmPXFW4IsLYYQN3Fsf0gAg49nmbwaAuYXh4WnPwAhbdI0Rl2G+24lUknNVSaCLO9HJqI0UU41/5SyDrm0DAFCpGsDeUjISzMXJwFcJ+NUVRLhkYnXLdlTq/woyuzwecBO+ew61pmRpj6bEtAvdfNkGccR32llLZXkvwFuPNLD9vBh6ryRmbuuFVV+8A3A8Af+oAbjgacjMaaEd64fMqS38NzcvN4SaIj1Vp5h2lOO4honkhKWqVAp0Pl62pufsD6xz8SKo7JzJSJ570JDjvvvm9ALse6/hY4DNCfijACsaZh7JQM+6bVv39MDby6rzHYRZa7QMvaw0Jvwq32v3rWSnXJulUhL7xzbJNNSmuCEDAG8vYCw3fbwY0G69xzQeGgR4gcFvAvje9Lz4AI1mnnqIX3/+QWxVbRHtmrRFXaQTRs28JVM1dpcmNqGLUgJviW0DCH/5zQkNDp9esXPZTfS8FuATwv4CwJs8/PDh11fsTGi25WSFobPtmsxpUhfFYXqsi87JN2Wx12Pai5xiFTYMkqnofJlEZ/AxQlxv9wB8TuAfB3iq4c+27FZc9ir6/o6g4CdJEjpt8KFJaqEUiSS4LDk8jVI6OWw4AGgPPjwS3UsVgN/mI/j1AHBsy4jB23cDGUOfNdsRjs8ww2G+20mhnpyKc4Ku3OnYrZuuSikB3wFklbV8cJn+R6jvAxbKjJnbs/WbsgrqwN81QdQLux2F6HyPjqcTtWZpUuPLIfopOXqrKU4JfK4H0Pjgss3bjcAAHkeh5Menb68OPv8A1GFfutXKVmt6Me1kd4n43hS4dVeq1B7vibZs23iZVmyzB1bsrgDUHdxjDAxOsFBmCD2LzJbtfUyztdS/bvaTdoGJ7PN5tKsRQtY8Y9ssOycrXkTtaJIr0gvMpsHgzOeeqgwf3O0JIaDPkP625Bn2zaODytCXNZwGyKzctSjTaTkO51JOTdhdVsyC0pzf5rHpj+tQ6POtzSmCtzbASw0N7x2hJX3zkYZliJ0Bf9i4P2kcAjjZcPsxQNt3wFxpFei5a6kRGi4a+JJremPQycIUKbDVzsWysZRbOaVgWDHt1MD/BLfehCp/CuFXTt1aVcAe+NSElNRaaCoJfYfpElvxItLnFAz04sBsWmx/MsF0xnk+Nsu5mPnu1uohgkpPtrYJf6BT+6AmF8EClsC1BC8BP3CHiYpPcMNUEO6nodN8Sc0c5P7eSNMpVR46u6qYusp3i7fEisvoRUueLtlcx/RrU0ya9jWwPXtysAp+oCOCyrt93OiwrjDgK8sfrl4DwP07mO+WcbZdX9NTAc8qyEv6ENvm4dqrS8q0NZ1Begm4Nwueb0jsoe2rCH5Pm2X1wzcMsre4JwOVY7WXbkqo1dOC1yhTuRQ7RPtxLdhSHTHxfVKT5ksqUcwCB5c6xTTwTliOWpLAX4vosdQ0B/oY/J7ZAA88Vx87MPjvkO8BX0cBgRc6xxG7y8Gl71Y/TtlKCtNj5wLbSrXcVzQ6fqCKbMuDBtozuL1tVQrda+2CQaZMEsZnyKZR9sFEkU6tpqeSUJhDhJhuVeGNG4Oz0J0TVcol7muGILOWF3/eaYS28hLc7AoMEMicZe+Fd95j7F+sqmLfUAtkJMeLOuSaXjffWq3E9+ASSmD1xAkamy3NOu26jZ5j9JuimyMJtIWXC3MD7O3PsJjrDckl+8twGP+TVJrgq7GSLtd2ylAl1/TEZP4KorxEMV0KLh3H7IKTcpV4r6G1WfsFNWhzfT0X1qwzT6/R5vdSaPwxjrMtcWRXk6R7x9Ms7C1c0zMwaTIYXDLf+dtmc55mlZ+STxn8pAzAYaP1lQRcfyFsN/K41DQ7Me+PN4BJsR6hYbbZltDViJ2FXdZqem5MA1F3QeDOdI+RuPZjSTGduC1HOp8m75fCoCHOCka3dM7cHoCeJWDsrkNFy78CFpvR12sLP+uOkPnOx1JzGh42Bw7X9HK0Je0B8b2bFdPLZYlAupB6xSzsBXP1/4jwgG3RfcFQ8rTufjhmWLOqiv2NpqRY5s2QAiWyGNDi5lvQCdVcIAZ5iWJqSyQMKmlUzPSrxP1wYN6L2zk8vu14BZZ0WdbkHtj41fHNlJmfPDBvMLmdtlkiDhRdTiXNKZ9Xq1ba/A3NIymyW1yaThN/LAymF5FYqYO/IlN5YDEBP3rw/Xd/haFms2TtbwCsX934wdfrcVKDiUxejKJCYAiqI8gVOHVVrPzRpGnUGtOqcVvHXOXZ2GwnBqULvnhobw8B/+zg+1he3QgrJ8xYM3eWWWWTlczHhPDRo7cBWeZJRzLfFetspATiizskUaVN2Uik1jLCtIsBMciLstU715ciVc5PRFCbPvvy7kay9zdCLwJHQfxoegY+n8/9+z85gXHz/R5Hk9lLiSvhpaSYdhPX9KKIIwRfc2wZWo6WzHdikA6KKk3wdj+sZ+CM3SxxVczbTc2Y0bPN//CbCrR1kWDrJChzqaYnbImiSYrpusT3UpdDRO8o2RgRj++mQ91+SZSmzt8Hm1Y31uxupAfDH7qic81dyKfjhJ7tUxgqcOonMKXmDDXkml7ZQ6emTfBxFsV2VEx7cisJaGEq8V14uTRXPuqFgyOwv8MnN7MyeBLdW7v74LPhF8w3woXjtcvC7hXRyfJ0/gFcJzFIFZlIXrOi0cilYn2oLR/lyEnzN2a7YH0d9k1Y6NiybOdiGCpbQ7D8nttN5t0HR4df+QAgc2drFqlg5wOSxKCUzEVxTuJjfIkssmNbIKe6Ih8pE3QrlKZXUsM+NVN5oR77W1xi2g27/G5YxZXABX3wO43XPlSNZ77L2lc/IVkxS5TQilxcIr5PuVQYF3dOJ8Wc2hbhnFP7sZDYAN/WYO3fBG8SdrxrZ81ZB7vxEdFX4JvaW6s3GUppS0rme8z63kzZU3ZSeyhwjdsKNqV+lBE6YwOPpUhhxpjagb0XNq+uYd8MsD2BO7MPK31fJK0DiwFeraH/ASDzmiw1k5JHM4g5qpUT1jzX9OLIo7sgNHwXhjc+x/uOpaLIT+vAqj9ot46WKYIgOrQtHeienryzhl3zYsQcMIEIhoMJERUVFTEfRDBhRPHgRREFPagYEAOIBxX04m9z6tUYz+OA1Odufx/V2zVvXr16u2fMp5853QUPOABiD442Zk378yWwhg+/sn/SsLal48scSMmantKeDXAyUrGiNI3HK6q/JKZtFBOG9HI9dZLrJHl0qmeO3H3z9sL9z5cfnWVW9mIvJ0wyTRPRovA7Hx89+vDly+XLlx81/19nibEDMYUT8AqPZ+GynocKqieDwNsJI3Tlkxgxszmzu/rkB4Tivy+Wyk5ws9r8/xJ+uvdTAfx77YamhquUNT0YJNzsia6JavwEcMx0Wj+GVhyAQZLrPdJJV9zmYDMj3n5o26/UkeNhLpYXY8ZAe2Uaz9efya8vTT5RS8KcOX1wzJkh/8nuhcHcWDggpqstmRLzcehk825cHyJzqPuxf17L2w/8SjvRef2rf9q6/K+Fm4S2GlqezH9aKI1lYA8AdlGWwyao4bI5BJHp9OmeTqhLv83Gv1PaMGYXkj4/5iHi9Z8f/EZ1EfHXdQwatkW9ZwAbkU1D9+SnTLDk8eit8AZZQ7CXcwbYbIem0MFfn+dBW+1p1cuX5/5UJtcflQ2w/rkUrgQ5c4C8ygmZoOQnzAJ3HPB4YZimkSLE1JFTipwHvtuvXRwcS3W/ddOz2/tvXH3QgPHxVr18/5cy+VjUk6JowfF9j09ufLzxxo39p77uv0hZL3KYBWbI1ugqEkrQnEq4GMCug6B6D1PoLNzMFUF2l7wYb/+AfcBEq15u/kuZDCLwShtqpsLcgydNSOCTUN4IADurwjNnBENgM3FgiUAvmD6TNjll8sD8H8uK6qEr2v9rfvlbyrnNGofgkCjeKw++iwDJxvcmGQpzJ+aCjmQiEHM4qZ9ALOvhYRsHzVDZ/aUcVJwxa34NAE9z7q1RznlkXQ0DhjX1KEZeqzwhpp0CDqzKQOAjnNeRMbAbxE6QIaQKjhBzeC36X5cQBP/f29zP/dT/+oA+uzJQkFNqRkJWhb0mEIFYpqQgxFQabnNX4L6NZ07r81nMoY5w5q2o86vOEBKvGoX4Vat632yTf+claxxYpIzDJoVtBWKiNgr43oMqPGtUErBX0yc4ukvnLgCvGY7mO+Jw3fsqTVZzfnUciTs0ZiM3SDsc2S3VL+dbJdi/qrHcLwGS50t7OeZny4CY5QjNlJsXNGh9X0eStq6jTEoBua/j7P/4btaqMTt2/skMNsBU4BlspvDX5SZ7SrNYiKeSm19aIGWR05mYqoqMUkImkc4r2revZxIUhd7kXHWNNuqXZsr+32djnja5f2tzX0/kNp6WxrgfxzvW9HiO5nmAlsSR0oq2LoYObUkoY7Qlywht5HDqvIoKafHEWV27+zLNtnHuJExP99fTY/Z5m/x+vGlT3qFEvdMJOQC7KJwDsM8HBoXJC9GGjybSH5WzF8w3JHBP6iUs29iu9flivEHWOZJX0jVDwpdrafqEaxFreoA94PsQ69LeLIh45QTkNWtqKqn8J6DQDdW7EEIXLiLnaGUjnQoqptC1lzjmIDhQKnfGXH/V5v61QXStgOiDBNrLaBywZkbAs3RewR2qp2xhGbaSKBvN8FXziu/P1LA5TSlJW5jWJVSqNggExw9/vwCkHRUfGqQuJRApTViKSdHhkXit4LQxDpM/NT7I5g1bzKmoZMYtGBCwZ1Nxc/vJq4IiuO8w+WqgkFCIBbh3zRb+mSd/Fs1VOohfU3jRGiTSGEQmG28BOnMzSdx34iRaKCcsy2hT0yb2TLOomjfQkWr2MjOnjtF2JzoJXTmOOUKLmEr61Xd4TFLzkaQJasuxn1xPWwm1YDDBA9hXjgOvmT2rRUxs1gzzKDFKJElDjsfTKfX7S4To8IYVpo34x7iCcHQpsj9WLUG9+xE0PTcjQ9Y9tgxzhwrEFBKIGedSKBPpIsN2yh4JOdW0ifQgcOXUOVlnUBm3QSHonM3wpcceJo9otmbmD4GR43VrgKu4guJIEVKqyKQW1KCsPcpvUp/eLqZPIX6WTZye0V06Z/GAeEQ5txd3JTplGbI3KVv4AyNmnFZIXtr9Y8dOEEr/NEiIdtKttUInMMCuhlMGhKT1wpGFVWtxKoFIA0eiQY/qXSgfEpPnUZxa0RVUmkxSoKJElL8YO7KL/Opn6FB1gXVJH7uz83oQy8IycMxqcsCjdxojZu2rSAohbRJpa2i8MK5wRggRZs/OyCA3oStiJv79MZaKfXqBgjw2XaKyp6Rs4V+EAcGwSPiEWBMjTY+APW6y9UxkSkJMPWtGLejeXjxNEnBOHdV0UNNvdVfvJkGwQEzhelYARehMaLpR4WQYkaKcfRxSwvkGPPFgCDMXOQL2bBkdkfJTF9YEtxMnUPmbKUBMUUzr6chGemYHZZNMQ/Zi6Aw/Hy0zYs8UkytIJciW8sv5KVZgnajHFewRnpHAGDECYk6bDMQsJk1LqN59qSOjjAzO6FiKPBGDECLVwQ0rk/yP1q/tUHmOytjjHGKaMhUuphgK4ybSy8ZPyHgoP87xdy1ADVRKzRQa+dxLYUwUFpX0a2HSDKeItC0uHaCyw3o3vJ0K0WRsgtRTPAppRC8LM4k5cEix5zwnTQ9FJywQ01W5IWrQ76e0yUVzibXJeQtmEFiNWzCRFBA/d9VAdTXKTCyCzmL2DowDJA4KVIrOMl7DjyGpmf9IeD+ZGkh0snMLAPuoJ5v4U9ObM3EcDaCKifNsJISd1NS7VKYOcdyREVpYtlfFLWJWmgsJuQE4LcVmXaLAZ+bjNmULf6SnwG1uZo+AmNnkQOWvli7SdJLZsIpyI2IXm/FeGpmIBvGNEHL2yi2dtn64NTm0Fv48kQjDEtjTXwLKUs4o2MIPjulC6+JLPaiw1Rj3CdL0MirFfjktbt4opo3God6nkq3IDaZ2yCoN46bV7Eudw/U+GVP4eBIKyvRLjfLPHJeZJVAHYrLzTY8vFIVxk+jXXH9FcGRNnLoyo7+1eHopaHy8okFMJcpJ3akHcnaMrOel7M9gxMyzghkCFSw/C2IBPKklrwssyf/o5cp2G7aBILFYgAd46LQsWfIhWTLs2IavJEibb+tL/6Hf2+4y6PUcRy9M7BfKpIYzs6NlVxjS3cRVkPcuAHl605Ih5si/vF1cFCjtk5AkwXiZfp9jlhj7RYFpAPjn7W0frXYVb2dxyGKE/xQLxRyQ8KuRHxdzi9KvuOSCTINghckJMXW2MoASzm/0NONL21n9PcTsv/v9n7PW8ajqIBk4NynQWdvNNW2kPlox+4wHldMxRBteKCPphHALRYiZvVYF3evbWGmynYaNIhx7dEv4zsnTiRN1yDwmIuxLIzk2XsQqfGTERkVHBnOhUTNizhhDbdrQzfq6pS2DdfeS0La6dinJtOlKNMfvuyWChHTdzzV+q+mk01jW+OKO6Ny/X3qKJ2ZUHEoym2SIhGIs2S0YjzktW9vd+ctpmZDNervkwgsomhlkCCZHf88SNFadj9/btMEbHuCfd7Oclnw7KmbbIpu02yNXQbYtI2Zz22PcXtG3PyiBwCHXmfGgEZrFQYCG7br3AHCeOsqwrHbDt3uV6DUNNs94IouT5v0+KslEgRk7Xsqc3R3ypiQjpkZGTKMsCPI4aikI2CdiFGk1LBU928MmI8fhc7cFIW378d2TB8PsEPDXr5fMI4PJCk4CeWMERCr85enJxmBMmRNlxuZXWgQZrr8qOoF3HwFYRlaa4PRWodbgwjG1Dq29fLvRqqyNC5BFt9ULkHEX2S+PY8XasKmipzdNkSEwYmJ53TPl7M+KqHC4WEAt88ZCOhegdFKWORg0l197lOK7Jw9ctJPR0/uS4WHGwL7mtJUvN4QeuO8yZA8hImaZqfgyDiff6BkvnCbNGso5AsBlc1sJIZvd8IKUrhja7Dl9zHCeMrCHZcxT9bXk0FUTzbKYwbKOERPRCotIcsyplOVuYJM7fd2xp1d99oZI23BNSZy3H6+eZFp3QqmfMvmmTFj6LaJCzaLHwfudA9g6jeQ4rDSrk4m3TNadLZ0VA6f9/PjBiFm2VSIkpKeLkUr4cM6SRSJt6k1ZPqOgBkn+f09vzhJRFoscSDDezgQ66a5nxJzagpammip2l2oi7gJtmWsjBToFQRlAjdumEhKxmtoEQJ6v12cV1NJoYqu0BhouG0+l1sPIAex0CixYq5pLOKpQ7LMuEhY1BkU6VwAgmuqVGMJlM1LJp+6HKhNCHYf2AMI3n+OTJo+zd5YfZpclBJwvsdoQCt7WifmbCptc08RVYB6kyhdWTy+70hODGdqU1qu7NkDh1h1xOX3vN4gIWU0n7FOuVKmovXm2oBMhYy0NXfT09hcGndDXzBC6DT+l7S6TpDMehEzSbSpF0q8uEzSoVTWbZQHkHG25VmD0H+IpF7CnhzHZnEn2m6oFECOelqRQbd/y5F/GBXt6LXt64h7LfWlqpErAGwP3gEKC374U/q+h3LWBIrvDsPZCPrEVWFIHpGG7VUAQPbEqL25cRk4WhRMQW01J4FZOKkkIMZuiIHYUlpydLDcfF0nRlYHOXH8cHo0U0IwfryjhWduGk/wlJziLkESNrnghco7wE2Lmbk6fzAqWS2r/rv4a8/K6tVT4YSIj7PVacjmtPVnQsjndPDqRN+c0K/PnNdsEaS3TAtTskjGwR4lY3hWBzoIgXID7mOaEr2PLIn3dVpJW6F4ZgZCr0vhUSl841aRWGkyq7VZonZ669on9bYByenPFHnZ94UDqG+eW1FvnePIb5sBqXTGwq1IxuSwQSZ3kCIc6BQm47TcaAMrdw6GA/cfwmgmRbYb2uZ2FfGC+pd2OzANI3lbMMetFSrdmjBWJ53dzlEOSfqsqM3ST66kQxAUeF/r6bWhrykl01woA3Dj22kNyPm2e2lkIIIuMWOUu5paMkBY5t0RiSgob1gvuG7cpJVnKj3dLx0HXBUlU7rPUALjanJyQoA5H722C80o19R0w9c/95YEC2NYwdGZMdqGeAgdSR3IBwIxjoBW67fbImuVshRT6pZyJv/6Yzz1mVhqlVHlG0NJUbUhIwT5oLZLfxFMvEDDbsrBVv75xSXW7YR5/6Flt29WFgT1fUfJNoEnkKs9BIlTLPbmy2yNVYmG/eywl+ZdsHqjNcG2kwKc3mAX1GiffNCYi5lcg1QpETVDkFgUhZnNZI6HM9jrHv4bj8KbIMxuorqDd43GkWnjf9gmiLk9Hawp4etdEoG0DSfT0Ug3AuSUvKKAXy9t1V+XE/h8E7DLvOkImfWor/ddYL08KE0jd2puFRrOahapQ2vqsn9bPb38H7Ok5xvd1Rf+lG84tqSkq1NCyaHG3KgEQSXZRZCbLApOQ0msk+eGyAASsulaDMOvHNUMhq2FY/yF+4Mq/PL0wMmKaW2wvfCdgB3IMhFOU9NOUONASzqdgyEtuuxVZWOOwNALsOLzXQvjq41qRPBzH20/0aAVZzHm2LncCASR5eoRB5OlxpGyx7QPL3G4rqTA1jJYW6mOkpzS7fa41gC3bfmakdvejdS7Hxdn/yC8PgEI6FoNq/hKtDsZGHz63hqVfF6I5TA83HsZlAijUdj3XSmBW+2ThpFKmXh9Roi76691I8TM9WjnFtFvSlsl3vYtWzBIof9xXCd1SWFqqFyry9KgKgr5yiurH57eSxEw13iggtL0+KikgdISYMP9d/MwFRc+enjuEGBk+WA4jplbMDAovZThc6BSubhsi+MvpkSXE/odjKoR6GXY1ZbMe1yOCVFN7Q2/wN/EzF2hrBBJhR2GcJ8RU+zJhItM6SdJvOM4ocTC8F7SRriMBu5/aSf81LquTVUoW2astDmhsMGF9cD/Wf54DesuMazubNYm80HFuKds9GiB601WGBOupQi2FLY4ut0ZiibYJqFPfvL4G8JD213cEYd+Gz+zHJs8m3ki+qT93B7a0+7sidXIuFakT5SyEhYBEJpfSC9Tm0u4NCLx/fgQ6yx4DRaDdY3jfEyJdu+0vfwK5UUHpEgNZAwAAAABJRU5ErkJggg==";
},
b4c8:function b4c8(e,l,a){
"use strict";
(function(e){
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.createAnimation=function(e,l){
if(l)return clearTimeout(l.timer),new s(e,l);
};
var n=t(a("9523")),r=t(a("970b")),u=t(a("5bc3"));
function i(e,l){
var a=Object.keys(e);
if(Object.getOwnPropertySymbols){
var t=Object.getOwnPropertySymbols(e);
l&&(t=t.filter(function(l){
return Object.getOwnPropertyDescriptor(e,l).enumerable;
})),a.push.apply(a,t);
}
return a;
}
function o(e){
for(var l=1;l<arguments.length;l++){
var a=null!=arguments[l]?arguments[l]:{};
l%2?i(Object(a),!0).forEach(function(l){
(0,n.default)(e,l,a[l]);
}):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(a)):i(Object(a)).forEach(function(l){
Object.defineProperty(e,l,Object.getOwnPropertyDescriptor(a,l));
});
}
return e;
}
var s=function(){
function l(a,t){
(0,r.default)(this,l),this.options=a,this.animation=e.createAnimation(a),
this.currentStepAnimates={},this.next=0,this.$=t;
}
return(0,u.default)(l,[{
key:"_nvuePushAnimates",
value:function value(e,l){
var a={};
if(a=this.currentStepAnimates[this.next]||{
styles:{},
config:{}},
c.includes(e)){
a.styles.transform||(a.styles.transform="");
var t="";
"rotate"===e&&(t="deg"),a.styles.transform+="".concat(e,"(").concat(l+t,") ");
}else a.styles[e]="".concat(l);
this.currentStepAnimates[this.next]=a;
}},
{
key:"_animateRun",
value:function value(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{},l=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{},a=this.$.$refs.ani.ref;
if(a)return new Promise(function(t,n){
nvueAnimation.transition(a,o({
styles:e},
l),function(e){
t();
});
});
}},
{
key:"_nvueNextAnimate",
value:function value(e){
var l=this,a=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,t=arguments.length>2?arguments[2]:void 0,n=e[a];
if(n){
var r=n.styles,u=n.config;
this._animateRun(r,u).then(function(){
a+=1,l._nvueNextAnimate(e,a,t);
});
}else this.currentStepAnimates={},"function"==typeof t&&t(),this.isEnd=!0;
}},
{
key:"step",
value:function value(){
var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:{};
return this.animation.step(e),this;
}},
{
key:"run",
value:function value(e){
this.$.animationData=this.animation.export(),this.$.timer=setTimeout(function(){
"function"==typeof e&&e();
},this.$.durationTime);
}}]),
l;
}(),c=["matrix","matrix3d","rotate","rotate3d","rotateX","rotateY","rotateZ","scale","scale3d","scaleX","scaleY","scaleZ","skew","skewX","skewY","translate","translate3d","translateX","translateY","translateZ"];
c.concat(["opacity","backgroundColor"],["width","height","left","right","top","bottom"]).forEach(function(e){
s.prototype[e]=function(){
var l;
return(l=this.animation)[e].apply(l,arguments),this;
};
});
}).call(this,a("543d").default);
},
b5e5:function b5e5(e,l,a){
(function(l){
var t=a("7037"),n=console.log,r={
log:function log(e){
n(e);
},
showLoading:function showLoading(e,a){
l.showLoading({
title:e,
mask:a||!1});

},
hideLoading:function hideLoading(){
l.hideLoading();
},
showToast:function showToast(e,a){
l.showToast({
title:e,
icon:a||"none"});

},
getPosterUrl:function getPosterUrl(e){
var l=e.backgroundImage,a=e.type;
return new Promise(function(e,t){
var n;
if(l)n=l;else switch(a){
case 1:
n="";
break;

default:
n="/static/1.jpg";}

n?e(n):t("背景图片路径不存在");
});
},
shareTypeListSheetArray:{
array:[0,1,2,3,4,5]},

isArray:function isArray(e){
return"[object Array]"===Object.prototype.toString.call(e);
},
isObject:function isObject(e){
return"[object Object]"===Object.prototype.toString.call(e);
},
isPromise:function isPromise(e){
return!!e&&("object"===t(e)||"function"==typeof e)&&"function"==typeof e.then;
},
getStorage:function getStorage(e,a,t){
l.getStorage({
key:e,
success:function success(e){
e.data&&""!=e.data?a&&a(e.data):t&&t();
},
fail:function fail(){
t&&t();
}});

},
setStorage:function setStorage(e,a){
n("设置缓存"),n("key："+e),n("data："+JSON.stringify(a)),l.setStorage({
key:e,
data:a});

},
setStorageSync:function setStorageSync(e,a){
l.setStorageSync(e,a);
},
getStorageSync:function getStorageSync(e){
return l.getStorageSync(e);
},
clearStorageSync:function clearStorageSync(){
l.clearStorageSync();
},
removeStorageSync:function removeStorageSync(e){
l.removeStorageSync(e);
},
getImageInfo:function getImageInfo(e,a,t){
e=u(e),l.getImageInfo({
src:e,
success:function success(e){
a&&"function"==typeof a&&a(e);
},
fail:function fail(e){
t&&"function"==typeof t&&t(e);
}});

},
downloadFile:function downloadFile(e,a){
e=u(e),l.downloadFile({
url:e,
success:function success(e){
a&&"function"==typeof a&&a(e);
}});

},
downloadFile_PromiseFc:function downloadFile_PromiseFc(e){
return new Promise(function(a,t){
"http"!==e.substring(0,4)?a(e):(e=u(e),n("url:"+e),l.downloadFile({
url:e,
success:function success(e){
e&&e.tempFilePath?a(e.tempFilePath):t("not find tempFilePath");
},
fail:function fail(e){
t(e);
}}));

});
},
saveFile:function saveFile(e){
l.saveFile({
tempFilePath:e,
success:function success(e){
n("保存成功:"+JSON.stringify(e));
}});

},
downLoadAndSaveFile_PromiseFc:function downLoadAndSaveFile_PromiseFc(e){
return new Promise(function(a,t){
n("准备下载并保存图片:"+e),"http"===e.substring(0,4)?(e=u(e),l.downloadFile({
url:e,
success:function success(e){
n("下载背景图成功："+JSON.stringify(e)),e&&e.tempFilePath?l.saveFile({
tempFilePath:e.tempFilePath,
success:function success(l){
n("保存背景图成功:"+JSON.stringify(l)),l&&l.savedFilePath?a(l.savedFilePath):a(e.tempFilePath);
},
fail:function fail(l){
a(e.tempFilePath);
}}):
t("not find tempFilePath");
},
fail:function fail(e){
t(e);
}})):
a(e);
});
},
checkFile_PromiseFc:function checkFile_PromiseFc(e){
return new Promise(function(a,t){
l.getSavedFileList({
success:function success(l){
var t=l.fileList.findIndex(function(l){
return l.filePath===e;
});
a(t);
},
fail:function fail(e){
t(e);
}});

});
},
removeSavedFile:function removeSavedFile(e){
l.getSavedFileList({
success:function success(a){
a.fileList.findIndex(function(l){
return l.filePath===e;
})>=0&&l.removeSavedFile({
filePath:e});

}});

},
fileNameInPath:function fileNameInPath(e){
var l=e.split("/");
return l[l.length-1];
},
getImageInfo_PromiseFc:function getImageInfo_PromiseFc(e){
return new Promise(function(a,t){
n("准备获取图片信息:"+e),e=u(e),l.getImageInfo({
src:e,
success:function success(e){
n("获取图片信息成功:"+JSON.stringify(e)),a(e);
},
fail:function fail(e){
n("获取图片信息失败:"+JSON.stringify(e)),t(e);
}});

});
},
previewImage:function previewImage(e){
"string"==typeof e&&(e=[e]),l.previewImage({
urls:e});

},
actionSheet:function actionSheet(e,l){
for(var a=[],t=0;t<e.array.length;t++){switch(e.array[t]){
case"sinaweibo":
a[t]="新浪微博";
break;

case"qq":
a[t]="QQ";
break;

case"weixin":
a[t]="微信";
break;

case"WXSceneSession":
a[t]="微信好友";
break;

case"WXSenceTimeline":
a[t]="微信朋友圈";
break;

case"WXSceneFavorite":
a[t]="微信收藏";
break;

case 0:
a[t]="图文链接";
break;

case 1:
a[t]="纯文字";
break;

case 2:
a[t]="纯图片";
break;

case 3:
a[t]="音乐";
break;

case 4:
a[t]="视频";
break;

case 5:
a[t]="小程序";}}

this.showActionSheet(a,l);
},
showActionSheet:function showActionSheet(e,a){
l.showActionSheet({
itemList:e,
success:function success(e){
a&&"function"==typeof a&&a(e.tapIndex);
}});

},
getProvider:function getProvider(e,a,t){
var n=this;
l.getProvider({
service:e,
success:function success(l){
if(t){
var r={};
r.array=l.provider,n.actionSheet(r,function(e){
a&&"function"==typeof a&&a(l.provider[e]);
});
}else if("payment"==e){
for(var u=l.provider,i=[],o=0;o<u.length;o++){"wxpay"==u[o]?i[o]={
name:"微信支付",
value:u[o],
img:"/static/image/wei.png"}:
"alipay"==u[o]&&(i[o]={
name:"支付宝支付",
value:u[o],
img:"/static/image/ali.png"});}

a&&"function"==typeof a&&a(i);
}else a&&"function"==typeof a&&a(l);
}});

}};

function u(e){
return"http"===e.substring(0,4)&&"http://store"!==e.substring(0,12)&&"http://tmp"!==e.substring(0,10)&&"https"!==e.substring(0,5)&&(e="https"+e.substring(4,e.length)),
e;
}
e.exports=r;
}).call(this,a("543d").default);
},
b93f:function b93f(e,l){
e.exports={
withdraw:{
store:["POST","/withdraw"],
list:["GET","/withdraw-records"]},

asset:{
show:["GET","/asset-balance"]},

visitor:{
store:["POST","/visitors"]},

couponCode:{
check_usable:["POST","/coupon-code-usable"]},

coupon:{
show:["GET","/coupons/{uuid}"],
pick:["POST","/coupons/{uuid}/pick"]},

message:{
index:["GET","/messages"],
destory:["DELETE","/messages/{uuid}"],
read:["PUT","/messages/{uuid}"]},

subscribe:{
send:["POST","/subscribe/send"]},

agent:{
record:{
index:["GET","/agent-records"]},

setting:{
show:["GET","/agent/setting"]}}};



},
baa5:function baa5(e,l){
e.exports={
list:["GET","/orders"],
store:["POST","/orders"],
show:["GET","/orders/{uuid}"],
payment_detail:["POST","/orders/{uuid}/payment"],
pay_config:["POST","/orders/{uuid}/payment-config"],
close:["PUT","/orders/{uuid}"],
complete:["PUT","/orders/{uuid}"],
destory:["DELETE","/orders/{uuid}"],
cancel_reason:{
list:["GET","/close-order-reasons"]},

preview:{
store:["POST","/preview-orders"]}};


},
bc2e:function bc2e(e,l,a){
"use strict";
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var t=["qy","env","error","version","lanDebug","cloud","serviceMarket","router","worklet"],n=["lanDebug","router","worklet"],r="undefined"!=typeof globalThis?globalThis:function(){
return this;
}(),u=["w","x"].join(""),i=r[u],o=i.getLaunchOptionsSync?i.getLaunchOptionsSync():null;
function s(e){
return(!o||1154!==o.scene||!n.includes(e))&&(t.indexOf(e)>-1||"function"==typeof i[e]);
}
r[u]=function(){
var e={};
for(var l in i){s(l)&&(e[l]=i[l]);}
return e;
}();
var c=r[u];
l.default=c;
},
bd8c:function bd8c(e,l,a){
(function(l){
var a=l.getSystemInfoSync();
e.exports=a;
}).call(this,a("543d").default);
},
c135:function c135(e,l){
e.exports=function(e){
if(Array.isArray(e))return e;
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
c240:function c240(e,l){
e.exports=function(){
throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
c50c:function c50c(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var n=t(a("66fd")),r=t(a("26cb")),u=t(a("a8b1")),i=t(a("475c")),o=t(a("5a8e"));
n.default.use(r.default);
var s=new r.default.Store({
modules:{
app:i.default,
user:u.default},

state:{},
mutations:{},
actions:{},
getters:o.default});

l.default=s;
},
c51d:function c51d(e,l,a){
"use strict";
var t=a("4ea4");
Object.defineProperty(l,"__esModule",{
value:!0}),
l.getCountScoreRedpack=function(){
return b.apply(this,arguments);
},l.getRedpackRecord=function(e){
return c.apply(this,arguments);
},l.getScorePack=function(e){
return v.apply(this,arguments);
},l.getScoreRecord=function(e){
return s.apply(this,arguments);
},l.getUserCoupons=function(e){
return(0,u.default)({
url:"/my-coupons",
method:"get",
data:e});

},l.getUserInfo=function(){
return i.apply(this,arguments);
},l.userLogin=function(e,l){
return(0,u.default)({
url:"/login/".concat(e),
method:"post",
data:l});

},l.userSignIn=function(){
return o.apply(this,arguments);
};
var n=t(a("2eee")),r=t(a("c973")),u=t(a("0863"));
function i(){
return(i=(0,r.default)(n.default.mark(function e(){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/user",
method:"get"});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function o(){
return(o=(0,r.default)(n.default.mark(function e(){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/sign-in",
method:"post"});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function s(){
return(s=(0,r.default)(n.default.mark(function e(l){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/asset-records/score",
method:"get",
data:l});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function c(){
return(c=(0,r.default)(n.default.mark(function e(l){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/asset-records/redpack",
method:"get",
data:l});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function v(){
return(v=(0,r.default)(n.default.mark(function e(l){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/get_score_redpack",
method:"get",
data:l});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
function b(){
return(b=(0,r.default)(n.default.mark(function e(){
return n.default.wrap(function(e){
for(;;){switch(e.prev=e.next){
case 0:
return e.next=2,(0,u.default)({
url:"/count_score_redpack",
method:"get"});


case 2:
return e.abrupt("return",e.sent);

case 3:
case"end":
return e.stop();}}

},e);
}))).apply(this,arguments);
}
},
c58f:function c58f(e,l){
e.exports={
order:{
store:["POST","/groupon-orders"]},

record:{
show:["GET","/groupon-records/{uuid}"],
list:["GET","/groupon-records"]},

preview_order:{
store:["POST","/preview-groupon-orders"]}};


},
c8ba:function c8ba(e,l){
var a;
a=function(){
return this;
}();
try{
a=a||new Function("return this")();
}catch(e){
"object"==typeof window&&(a=window);
}
e.exports=a;
},
c973:function c973(e,l){
function a(e,l,a,t,n,r,u){
try{
var i=e[r](u),o=i.value;
}catch(e){
return void a(e);
}
i.done?l(o):Promise.resolve(o).then(t,n);
}
e.exports=function(e){
return function(){
var l=this,t=arguments;
return new Promise(function(n,r){
var u=e.apply(l,t);
function i(e){
a(u,n,r,i,o,"next",e);
}
function o(e){
a(u,n,r,i,o,"throw",e);
}
i(void 0);
});
};
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
cbb4:function cbb4(e,l,a){
"use strict";
(function(e){
Object.defineProperty(l,"__esModule",{
value:!0}),
l.$getStorage=function(l){
try{
return e.getStorageSync(l);
}catch(e){
return null;
}
},l.$removeStorage=function(l){
e.removeStorage({
key:l});

},l.$setStorage=function(l,a){
try{
e.setStorageSync(l,a);
}catch(e){
throw new Error("setStorage Error");
}
};
}).call(this,a("543d").default);
},
ceea:function ceea(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAAArlBMVEUdHR8xMTMAAAByj51eeIEaGRh0k55ng45ddoADAwMUExQFBQZ1lKAcHB0MDAxzkZ1jfYhmgYsXFhdrh5MICAlphpFgeoUPDxAREhNtiZVkf4pfeYMuNjlhfIYbGhxAT1U8TFJvjJgkKSxSZm9MX2Y7SE0TGBo2REkrMTJXbnc2QUYdJShcdH5DVVsfIiIzPkEnLjEiJCVDUldUanJPY2stOj8yOj0YHiBbcntHWF9Is8fsAAAFc0lEQVR42uzYbY+aQBAH8O0sIwIHlqcCHlVBOdHzWevD9/9i3cEe7lnb9F4t1/DThDXR5O9kdlhlXz6xNvw/asO34ZugDf+P2vD/T/hWq9VqtVqtVqvVajXGwDMjR4hib8A+EduLLHjHijybfQaeAw85Hms427SgEpzS6Wi5HE3TU1DX32x0+U0DBCNffnuSPC9zAyqGyZpqQDV2Xpb+02/85QtUgobuXhOEU4mb89NT56lTEQtCrw8nqDSx+HZEdb10OlqK86TzyCUIHNOAiDWLjYheHOR+h4xXW+1Bdn+3RS8yAJxG7Vsbr7Y7V4TsdfSUnenaE8+3aza0WbGYArEa1Pgi+/R79jxcIeJxn/V6vezIvvfe0XK2TMR1YlTpm1N7xIKTbJxeyy+WZZ7xHhfPXo+uM1a61/XMMYIobs7ER9T5L/238n/DCb/5yo5Z9SW+T+ZbvGpGfBttXqvLfyz5TZonPOte1iskm9Flt21I+gHimcu+VuXHQ8Z5Jh6ca7jbbQYo2OlulnBhkSKyBgg8PGi8Jpe/L9bhfrhFwo6Xc8avNO3MmlB6E2JcawJ/UP5jkSPZziffuES8fY3qB6ZtgIWDvSY8KD/Ji4PL39HIvAHhTYDTEbczTXhQ/uE+5He0qxLVH/AtgImbY37Q3vC/0GozGwOmmAfwkmRft4jpRKv9IbhsjSaoLr0DMEyyzC0QcTUd/ym/dm+EaIHDlLINMPqJEDoRCvlodh9fe2CSIjoAhs1U8gDyhCwBIIhRKJcL7S+64+GRhugKBLV9EwGMEvIKxHBMRLSPk672yHlSlDYKrNBGIERMpQBglvhJ0oc3luPV7S+bXeY5VszhXk+SGQgBU8kAy6fwF5BY9+0/3h23SLzYodHqE7EAgyk0AHjxyRokhlG3/3kxGaUMiRcFBlR++OQFhAFTxwPY+OQVbgJEq27/AV45BtRefbJRvWNNgHXoh74fwQ3OSxOIUbW/6BSznCPcRCF9Zq34fxAKPw1JADULu2M03l5UnY3jLlrSO0IyVR0+BhiFgg43js1DDEASYMhtB25CMgIhZgrI4fVQl8PHJecrByTOivMyhhu9EeGpbXRiQc2cc76JQBJtOJ+bULN0orxtaMPqRArrFZynMUjilPPCg1qkE+UblkalTk5QwyHn6/fh15wPEWonnSgflXST0kn6kcqnOlF+k6LjgasLy4/0/FIXXOXHAzqY7V3h+SPT5tkV9uoPZhFA4ZKTPOf1+zmvy3P+5JJC/ZHYA3h1yVS6wy4OaIDEwMNCusNOXfIKyn+M2CJlty/MoOatSxPeMcu1B7VZ33X7XQDlPwOZA1D0SX53qpRZKPVR3icFCA5TywOIf7Zrd60NwlAYgMfBizgZ5EIyqWaK2kiNhdKa///XdmJH18+1FwMjvA+CaLw4BEMO5JVec9HPT+6/aVZytZLZ/H8NS4i20rP0Eit98Vtiydvc3omM9EZ6ySiZNhTEoaZfspX0HL3AceVSVyEs15+pTweuSOaGnjJKc+1DGsbEM67EKs0OCT2RHDRTdv7d9aw7I6eV0qqkJ0qtuHZHs/dkl6GDSnkVPfb7UVgRhE+iuFXs77kvldfG83c11/ts2uZM7WN6IN4rHs/bNIS99dxHwsVVuTcausuMuVeFdnzPvrh6KkSUR7noY7oR99NYVAQXnPCmft1uIq+xdMU2kbfxA6FFViZTWGgtjuW7jE4y10STdUqBrdWrmJYphYj81ZRuV9c7VzbHZ1GacGNax4Acy7qDuLHpsrADcqdoItm+HYQYxMAX39veUvDRxPNQaGLqwnWdK2qTLCQUuuw47tKD0AuPoAMAAAAAAAAAAAAAAAAAAAAAAPyrb9cvlwpzL8+XAAAAAElFTkSuQmCC";
},
d233:function d233(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALoAAACOCAMAAACxO+J4AAAAnFBMVEUdHR8xMTNyj51gd4JEUFdCTlQfICJjeoRlfYcoKy92kZl5k5w0PEEhIiRogIpieIJBTFN7lp5shI1pgo0lKCxccXtGUlhwjZpfdoAjJShthpBsiJVYa3RSZG07RUssMDR1kZ1OX2c9SE5HVV0xODxQYmouNDhVZ3A4QUYpLjJMXGQ2P0RddH5vi5lZbnhLWmF1jpc+SlByi5Jcb3a1FgjWAAAEKElEQVR42u3a23KiQBSF4TUbRTmpCChGwHg+R828/7tNNyoiZmYyd5ua/VUqF9gXf5GutkODH7Ul6d8g6ZL+TyRd0v+JpD9ACCGEEEII8T+yfv60UEfeyfEd5+Shdmafvp9Mff9zhnpZtXz/nVbW8N33WyvUR7j2fYfoAKyJHN9fh6gHc3DWt5xoD0yI6N3xzwMTNbDp63ClDS9ERNf4/gbcTSLHdyg3Q3MHl3LqYjQBZ6Op4zjvlAtMMxh7MCj37jj2dAS2ep/3cGWDjGiABVER/9kDT267FN5ygZa+9VjFpfi2C36sxHbse3mQAXCvEx7YtOnGse2E297AO53tInzcMKFEpLShXYL7jbftM6+9Qda3lc6QlONpBG11n/KaNx+T1rGVfgYuJrGtdBcIl0NKQ1ytKRcXi8+Q1lssurYSc1ko959nuz+DZhVNFt0soD0+m/XP9pnNF1Q4XXuomNJNggovSbfg46V8dKS7l05G30yj9QRVcyqkqNqmbFbIhJJKvDemwjDEEyulGFxsiCrxTSo5PYcPifhsCMwxKaVpYwZUMvYe4UsVTkM2s317X07WW1xl9KRRCtcScPm3aYMV0VN8i54EJpRQh+c24LILG3ho051es12qmOm1/1hMIBNN8DDIMCcqxUdU0dbhhSXcAXgY/IRFJUOqqFxaYMkm/c3Cqk3fFLjw+nzSEw/mfEjfMVVD0zc+6W8NABOD/qq9ANB845YODI70R8OTmQ/jk964p2N7oD9orUrjeWj0+0VK70i/MZybj/Ec02FF9CVjAjBPB2ZjenFsADVIR5hQRbR9Gh8EbNJfUjZvVDK+AEzTm0HQxLNRmjdP9dz5CFFJ73TYpHc6TVTtA9W8sA7j7KvxXHaOX6d4C0D9eKhfuqLTUcf0yR4mMquG6Y18Ld8dZ6/j22026V+khBFRnk6UjPCsxyddpfTwLBtTkU5v+5fxXB7EvKSM1sWj9QtpS5NvehMlbkBKsIC2GZPSWeGhySi91eo2TdyYO9KiEFeWcd3yFp83u3zS9y3lp4urFWlzFMxp3m4h50YtZQ8mJqnOWVqPk5jLyxPrHTRr2VJSLucx2j5Sk6bb8PK8IZGLshnReATAa3RVeMTmlmt5laKqVOLui/QGTOw/ukqD1dljzlqqLiM2dhiNX9IDE2lsqAFLTsdID+6H0TUMY4XGS3oG11A+uDzhfWH2DrFhpDCDSnoLZmIYhx7nF3qsXRwbLjIXI9yEGWbqShxP2Zx9/cYiiRMTIXY98/aHmMOC95EswJ55iTIAUz2x9fQ35gB6F85z5SGcezrdMJZL9Uunj7icHf2dBeyMm5jLo9FvOx3iPPwQ1WCSPxvNo0Oswudszkj/wSqNorROL8WWZXxeNxJCCCGEEEIIIYQQQgghhBBCCCGEEEIIwcMvDnRU2cnpP5QAAAAASUVORK5CYII=";
},
e2b2:function e2b2(e,l,a){
"use strict";
(function(e){
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0,a("8cf6");
var t={
data:function data(){
return{
position:[],
button:{},
btn:"[]",
swipeaction:{}};

},
watch:{
button:{
handler:function handler(e){
this.btn=JSON.stringify(e);
},
deep:!0},

show:function show(e){
this.autoClose||(this.button?this.button.show=e:this.init());
},
leftOptions:function leftOptions(){
this.init();
},
rightOptions:function rightOptions(){
this.init();
}},

created:function created(){
void 0!==this.swipeaction.children&&this.swipeaction.children.push(this);
},
mounted:function mounted(){
this.init();
},
beforeDestroy:function beforeDestroy(){
var e=this;
this.swipeaction.children.forEach(function(l,a){
l===e&&e.swipeaction.children.splice(a,1);
});
},
methods:{
init:function init(){
var e=this;
clearTimeout(this.swipetimer),this.swipetimer=setTimeout(function(){
e.getButtonSize();
},50);
},
closeSwipe:function closeSwipe(e){
this.autoClose&&this.swipeaction.closeOther(this);
},
change:function change(e){
this.$emit("change",e.open),this.button.show!==e.open&&(this.button.show=e.open);
},
appTouchStart:function appTouchStart(e){
var l=e.changedTouches[0].clientX;
this.clientX=l,this.timestamp=new Date().getTime();
},
appTouchEnd:function appTouchEnd(e,l,a,t){
var n=e.changedTouches[0].clientX,r=Math.abs(this.clientX-n),u=new Date().getTime()-this.timestamp;
r<40&&u<300&&this.$emit("click",{
content:a,
index:l,
position:t});

},
getButtonSize:function getButtonSize(){
var l=this;
e.createSelectorQuery().in(this).selectAll(".uni-swipe_button-group").boundingClientRect(function(e){
var a;
a=l.autoClose?"none":l.show,l.button={
data:e,
show:a};

}).exec();
}}};


l.default=t;
}).call(this,a("543d").default);
},
e50d:function e50d(e,l,a){
var t=a("7037").default;
e.exports=function(e,l){
if("object"!==t(e)||null===e)return e;
var a=e[Symbol.toPrimitive];
if(void 0!==a){
var n=a.call(e,l||"default");
if("object"!==t(n))return n;
throw new TypeError("@@toPrimitive must return a primitive value.");
}
return("string"===l?String:Number)(e);
},e.exports.__esModule=!0,e.exports.default=e.exports;
},
e608:function e608(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALoAAACOCAMAAACxO+J4AAACdlBMVEUi9//n/v8h9v6d+fwh8/sh9f0h8Pj///8g6fEh8vog6/MAAAAg7fUdHR8f4uog7/cf5Owg7vYe2eEe3OQf5+8e3uYf5u4d1dwZucB/f38e2+Md09oc0Nce4OgQe38cztUczNMbxcwbyM8avcQd2OAYsLYYr7UZt70d2N8d194YsrgbytEcy9IXp60Xq7EawcgawMcav8YZu8IZuL4XqrAc0tkXqK4bx84YtLofICEcxMsaw8oCBwgYtbv6+voJCQoyMjMawskXrbMpKiuR5Oea9Pfk5OQjJSYcICJeXl4kKCoNDQ4CAwOc+PuU6u1zc3QdHiD+/v5yr7JYgoQQeHxZWVksNzkcJyr39/eX7/KI1dgLUFMyQkMpMjTExMRAWVsdVlo5TVAcSU0eIyUi8/vn5+eM3eGqqqofk5mPj48Qen4PbXFOTk4eODsCDQ7Y2NjR0dG/v7+3t7hon6GCgoJSeXtLbG4OZWkPX2IvPUAcPD8uOzweMTQGLC4EIyUXFxgCExTe3t6Ax8p8wMMguL50s7VspagWoqgTjJEegod8fHwPdnpPc3UPcXUHOTsSEhMi9v6b9vnJycl5u70gsbevr68gpq1uqaxbiYuGhoYRgYYeaGxoaGhFY2VVVVUeTFAJSk02SEoeRUkdQkY5OTsFKCoEHiADGxzw8PDq6uoi4OiP4uUh09qF0dMfx87GxsYgrLIgoqiioqMgmqBmm56bm5sUlptgkZQSh4yIiIgffIEfcHUMWVwLU1cJREdCQkI/Pz82NjYGLzEdLTAcKi0i3uaCy86enp4UmJ0Uk5hilJcei5Fdi41jY2QHNjilpaUghIlHR0ntPLsCAAAdGklEQVR42uxZ+XMURRQeZ6d7e6bnzbE7e7Cbk8QkGKqi8QeMxnhEJEAUowiIBMGoKIcHiCKo4AGoIHiBVnkVild53/d9X6X+R06/19kMKALFpGqt4hXkMT1D6uvXX3/v9WvjhP+tHYd+BHYc+nHoR2XHoR+HflR2HHoCev1aUVjKWYGjnF0SLHasUMDHvPCNujU7CCz0guMMQnSm5cY/1LNRx2YTOpOhU3gty0QnuFHHxjRsgh8WcMzJcTMedGYI06hbc0oIzvK48iwX2WoixTzynNd11APHR5BuiOG2CCuLnWkid3xm1KtZE44Rz0MaEI6p5hS11OU+5SY5hs5FjTSdXICsP72IGtmQj4z6syjPKb6RiXQPELrf6OCoq0gTa6Qw6s9MHnEKN69ppOJ37CzSyNBiRn1KjJmAZRJ6jrNgoopPxamKUnW3T11CbIcI2keNZH7JUUCt7gA1MlcJjPqzsOoidI/qF48U3CkKjLqtNdKJp1F3jLH9kCoVvyaOphpW2k48d3BcNNZfQjVrTgfWchhGOvAslJ0ZnnotysGk6roQR8kWGx13iPWkkX4+jzmohNBNUUCe245rTJ6xD7Zs+eBoZMDPuSYmn6KNQl4kZhdpJtymvBo7NTCZdOn9Rsb2Te9R5H5f70K7ppFMvyGgrh8Sz1uUm6wSLPe5lLBiBUj5ee6ISW6iQ7Ppp+ugOIpWE1nf5qi9ytsC35gsXXeb+uVZUxaNLFo05SzZ33QktCxqsLQpOWqkaU3zcLhK+dULPFwDO5w0vmx/ScpLz89m1/6YzZ5/qZQvbT+8KOYDjLkrLAq3T0WuqC2CSRzhpJHOpCSl1l1Snr00G9vq/QPxz6VnS7mr9XB0Gc821sRpTjtOebWIb9xKHjUyX25MHbi3QcrN748o5I8A7FF+5P3NUm7wjrh2sW0qHANiUgNid6ZXlHNbkEIsyKVd89ofXS/7572cRZsN8Bf96+V5/fL6j+xD6n8OneUx/B1dqJHMq/go5AF1AgLCqlSIoQqlax13Srn6hizZDTBrFG4af1gt5Z0dh0gABapUuAgReshpJmGtIaBZJBg5x8H1TRF4wxdSDq7Njts8WP8LXFp7XDso5RcN/0oUxskneM4IvavPSR6SxetVzg46hKWWIzXg0aphOfe6gRrUq/v6rrlsDtxYGxi4bq4cXhX9+yHU1ARwieeO3pQ9Dk6px0PB6abEmvcC+o/pGPt5ixx+6pXshP0Ot2Qyl8DqxNArTw3LLT+zA7d1RN25okCXC5DnQQEXIhQ+jnJvXBxNcgaKZypWvkPKryjAZDd+CfB6JvPqGHy9NDn8lZR3lJNTdjyCTnQ3uea5X8utNjnHZVST4eeitSudfdp+uZR9KxII75ot58IVmdh2wm69cbWt6JPy8vYEdnbwoc5mZqLHyIMcPuVn+GrYa+tSLmhtTwO6Oz/O+jcvSvAilsL+MbhHQV8MQ9fL4d/Pn3i76Oa4NpjvEsoDunN+hdahhNVW2IoaaVXbHTVaJLqbBdqdDj/2WoCVddbPjtw0gNA+nivlZ7/Cvgza0/DJrcPyrAdQ6weWjmR1bVCOo+1UbRJH3Z0TXB+AkOeObgiIcfkxbULrWTSJY4V+/+Vy8E3MnV9LeH9g5EeINXDqQxvhYoJ+N3wbVmNC7b5uYOSPOXL/I+rbNwflsk8NUzgh7cKQwGmN5BM8JzYVdaS9ghqPGs5Uw2HXsUI/BeCZgVgKZ0vZHzN+MC7ROw3jMdh0QYZsObxrGOfsonfDUs6+Ol6gSzfD3oSQ49+EkLOAeF6sYMESdDo4pdPyXPmeAn51zNAfWgjw9TsrYv7uENtfkPLF7QrEs7Auo20rrLlKJVr17qdoR7wv9uz5CmBIEFhuU0THu3MeDnZ7yPP2iksvHUqzRZqibzCWRtV7JcQ2Z+6uaSpa2+/DcPXA6MzMuC2BtzCSP32gUJZ2nTUIsW1sIJ6LiKEjAvOCg6Oh5nn8hK/jkNNRpBjgc9DspoB9Fcy5eB/AwrdVbLUtg98yNdsGzyUW6a3vAG57fBNsCNwDhPwf3TmXAiuET+fWKiajqac4ypdOzaUAvWkzbM1sXQKwpgb+tYtmXTgBfeYo3Gtoe+t5gE3bLrgboIkluhZEHY6eiWYSlh7LVE9lh6nH1pKLxWVOa6RjpAB9LjwdA7w4Af5TWJ9J2C+wjD59DIE/nMmsh91NKuK27s7ZGP8G6s5NLeJmzBc85Lnj1jocJh5FWMgM/ODYoQ/39WGMEfze+Nd+P9R3TRJ6XIS1aOALts1U6zAI/Qp6gMQ1NSe4iJI3GGaoeY6HOmVeHrnUWMWEWjj52KHL/fAeQrxAgd+4l32iCq+kXQJPGo89FwNfN1OLztmyiSoVEvQabxij2xYScpEPcU6VEmpkqSNCCp1EGtmbAvTZsFxjvODx22LwC1XhlbRXx4ZqwCnDPiNXGmS6YBGUlTwUGiamNuJynOwhz5u78K3wtEYKKnlSgP7AICzOJMBT4ZW0nQCjCBztwrG+eXJ+QAVQA/Fcn5Yr1J0LCwElqBAXgbvjl0pM4M0pK+RZKtCnzIMnEBOB37bpnoOhLx59D4GTrYPZU+SOiCP0INSXWzWNZEQhRxe7VAx4mEHN4tRmNYewt02YqUB/BBZQ2id7WPtDDS2HPVPkSlsTplYHWDqv+roI66Yc1WnjcpwosHBpm+YrzjTmwnSint0P+5AyR2LXXAGDi6bgNvV1aegjeNFqYyItY3fObsP1MBvo4k4UtUa68ZKYanXMKBXobw7C2G8zjwj5e7Ng1tosQuclOtSV2pHghcingqWRqOMadBmjCkjkuUO3kbkSzqnakwr07AO7AZb8cHjgi5cD7J6XJeiW65OAi5pGYk6qdedc6lcE3YrYpujGLc1bO4qKXl5HGtAVlFs3Qt8lhwn8wx/Ogts30PcqJSW6c9S98COBrMjp7lxvAZejXEGed3fhVCMqAniYFvQmfv8Q3Hb3fyF/fR/Ap2IlfU9HPEtU6DDRZqvA+tMrGNhqo43szxGjIq2RoWFq7nih0si0oBvG9OcB1l92KOAzn+iDNW2GtUqvksswB1UcdJ7WyKjWdWTUO41wWViji5NpL7lI9zOnx69ShG5c9esQLHj035H/sAQu+iTGx3foqOtDnVvTSMtEJ7i+hxLI8+bT8am5U5GFN5xbZOqx3B6mCJ3K3SsBbrnwn8Avu+Qi2IggjPn0/cpaja5v+8MAswz3TrGRJB2CqS3cmVeYWUtFIM8DZzy/cjtd6Aa7dgxGtx6M/J5NMPQnV4qc+J5FAanhGcgSfk4X6no7deeCINBHbqofuWGFFvHctfBib3qK0Mm6lgE8fd4BId8J8FyLorDHzQR0UcIutC9II13szpnMjqdIGum6yHOvhMTOn+IxVfOe1I1buXxiqtDJ3l0Ig49PIH90AYzd7yA64Se/t3g8RBrpk0YWue7ORbgcp0/jasItZaWRrKEzp6YgZrTj11EhXehk38eBv+LVONyx3Fx4C8CzryVOcys113XzgoS82EIXSmdyTD5lzK9OSztSKNIaGbjYbOe+wZUEWWlDJ9v7LcxZt3jJksUXj8LYtQ9RYwjNfVCLI3UAOh1FAlbNoQjmPco2kaATB4+FHNMs85ip2FTCBg3PY35NHTqZ9yRAH/5Z9gan7lyEcZ+moT9YTFzimqgySHBTa6TnMMyknodC3nMGNkt7z0SNnNERsEmDbohnAXbuBLjSEzai4y46c77W9YnunI0u8qjmzXlMueYyN1V8VbGLh7pQzaw1H5H8CIOxSYL+xpq4PRMftefAmtM1zw/+3qTere5PsKCtiP2vzqqlaF+l7pwXFFH7VbFrmop2TsjV6jhBMDnQ/6bdSlqdCILw0HS3nbanM5OZScIkISZq3PddccVdUUFQPIgoooLghguCIi4H9erFDfWiB496EEHwoGd/k11fjc/lPM6lXibz3qvurv666qtvzu2OT55Bdn4y3n0OWALd5Z/PN1cNUEwPepabvZZpmDbL0hqR0uiAyRx31Nw1mtaomJPRbb969D9c9wFhfmWROw6HcO85FoT4P8FRpFVvPQVa0Jw6gaxqwASkHyJ2fLdHy5LOWYtaezQHHKrfsKAu18//vvPgVnz8CernA8D14/G3B79bdOf/BEcnYRKTsPQFMga3YOKCVYMVAPJkPFcDG0vCnWAt4tzqely/GhQYJX8+/ehQfGI76oqbN1H2bT8RH3p0mp3sBKr9KhBGgA5djoPTzkZRJzfMNUCh0nL2bjHaENzCYeDON0kmaFuoUOtxfdOzwFI/p3koX8frPoUiG1s0bFWQBJ/WxacII/XzwFE/2wTXMZTCD8jY1ICVayC5IjCXTQbyDEAuep0sGJVt3KqINZs/u12P63vQZXkV2tGzo/ff4yP34G6I8RcvQsxjGPeOxN/LaHZoZr9C72ZPBY4Ccc4sNFcPnglIP26pYNvLVqSCBALbckk+z85Amm7t2zpkJQ9/6TAuh67Fm84p5mQOnCBkiT7ujk8g4J/Gp3pvQjObGu/QajyM0qZkIGdOa94QXnXmeEGuz+kgP+xzcyBLEqT02kROKVII5KaGmFn0Dl0WbkdP3xzvDb4eOxLfWk+osf5WfIQymb3x5uloZnPv5t2iSOUbUC3bBQOwc51VjvuoVeVnIqGEkBTnDOSmFBTn5TJaBzdvW6sOWWXoPk55dedQfDjszPjoe2S54v3ReDsRpofuTI0v9G5S4InnZFdz9RCMhppBtZmdm5U1sGmXZ5IAZjJMKUVePBv9k2XdflTH1ZmKhU3TLsW7tgfXb/1qSdyKw8ddQSuwaSqqelMdDF1pLbi3UZTYtO0ZIxlMPnNiBTVOZwPIs9EsDeJrBg5WXdra5CTontL1Mr5Gsy45XZQ069fil/QNOqaQlrhcMQvd4qJuPnu1ui1pYpch/FWPjBAut6gFXRppJ0i0YTJXp5QauPeZZvZukGRsj+Pbc+j2ittxvP3MunV3aUU+BwQ9D+ROZ5Zk5FLP2MfMbm4sk48ubERBGKlMg5x0BXarnjeeJUC0b5C1Ku5a4bTZdQVCmOvb40MkBSDxwKF4+/X4IAlidoVbC9uVOsqTUcTOgYBhmbpCR4/wvJwBZrczv6eCMbNXONqbMxcjf8nW9l3NIs05b4MI6e60nbsO3Y+30CFFB9CW+P6hXTun3Q0ypLdzKk0R/q8RrCXlpDhfA+S26xdadL02egHX59LTbsiRXc4yUYMIVhN5KeuWfpHS4XF8Mt4SAhvhvyV8eEyahgvPU8XN/5zbiUv5xbRuTk4WS3AGuT5yLWkKGwkBjJRKEUYaa2jUbT+P4Cdb2LVRvVf2JXTUP+yK4y109NBRtSWOd31YFxQPhetKclKPE41dOHDMzrURO8TzKmCksopWR2QORUfZQ9FRbiSEaWSrh4Z+a+WkU6/n3orlt8PxCtf5op+Dfmd5iAnrOMlNKs0Ux7miBBIJS+EcgLwPXWDZXUKhZNZODO3NBYv7knbUaIampctmNOt1vRjISD68sP9v1/dfeCjFX+wc09OEcmSSGR2FSmSORht1dSJRLwEj5bA0BOS2cBHpOE0amYQwsm5Nr2aZ5r44PlupjuJ4X5BnypwZAG8UELwPUMzXtpCDdfncHBZtjCSnOId0t0nYqOloU+FOPneegQRpGXXd0xrdVr/bid3d8fSvF4O88ev0eHeXZnScCCAzEkPpE129l4najehnxSeYQ7dODTIZvmgWWw0Vda3FQ4JOvxpbut1dDYysz3MzhO+ypF117mhQ4B08GBR2R8/9IUuL0ql+uagwMtEC7NygCaCZ6ZHdTkb0YGPFRpB149kLaSRmOLSRCiOZZ1EL1nUJYxwstJ/6lL9B0vsbyakplVbKNtFV29FjwOOZYAD6K4CRvZko6toLOlMYSUVdZiMtFck4smRAKzRr7praXP9XUxSq0TU/fqyJwg+NXxqANYjzfD7YObfcI3YWoo8oqspTtal1AYzUVpKgzRQeyVhvoRY00gl1wVxv0QZf5/acygSt5YNyVY6I7hrBb/wBFQeWZWlkBOhnJQXNpEyaCuyczyMK8NEs+rqYvYxm3k/me9qbG1YSRqrezIW6Jjz3mhlNFtBnA8aTxFQqf9YEwldm5zh9MsAbMehjTNnSPmGnXzI/hYhqZdIgFJo/dBQzM4YmUkK1My2tjXSS1hQw0jf+1BSB5JxqJ6KQkIKMKfCcnouxNYcLyUlVbiz5XaMSx+yYMdK3TSSFkCkxXpJWp1W4cEOUCxZ4yNbqucS/P2pZvQlV8tBYxpAsxbK4hatA72dFpSBlfgutaAC5bea0OrnvNQhZOss94nzlWFIkLVpLrmf1xblkm/Yx0W4GVlT2WeXvOwarYhxvBqA3+uVaMIbkyAXmrXIE5H4jLY9qL1riKSWezCH1jlw6IYwUxXiGjZqRqcHzsWZBk5N8FqY8z20Y2ag2I3wl7yxLYAo8J3yrYK1uV0O+ML8tCMi7wMhiNCZobeQ9SiClDP/CaCmsFv1WWUemqy2scn9pitSU1Ng52CTJsSrFUomcd0K3ZXsOsE+PWg6rk0GnoxKHdxqMjGzeoF2drepw9tWlX8u6K3uu3jiXPBgDKz2LEs3SHCG01oCdG7UwhDKxjKMGoyXWIgVGOmPp6ExnzU1opKtmdhQpkRYvJ4BqLZ5k4al85uq+qgnWmzCWMVK3WhznBUtGvedn+NCRlvMcGammxh3PffXOzAJAvnaGDLZi59yGSYsaR8XsYTMSIh3NQOz4TOt68CX1TABUGGmqENJTGJmSDc9phbylz+2uEbe7lo4cMvL5GaK/wsj1qwytIoWQlUI7LQd5Q0otFvYNgeaCmnD9dxKg2FQCeqsbMFmHRbprmmQ63YIF9D3Jr0k1K4xEstu0kSbsIXbO0+rM7Yx1+KIYr20hzleT6Mf1o9ou9l6nVXNiq4KBOFTokddwvWe4u1thZBq+AhkgTAO03KBgFnpkqCCZu63lKP4WzfT0t1bOpwnQ3W0zZKREr76SeoGGz8OENRTttNKcs9SCApWxXwtgiG9IPJcxpf6zlyvdbR6HgQQhCZIgyFd8IHZg53Cuxjmao0GL/mjf/6F2SXX32/2flCjgJmkLuRKHnOEkrOxifH+NCH4++4w28FixS238wf91N2k0WqFU9vdXpOTj+FHkeOmG8Rz/47tUQSq3wSA1KdngXbTc7OpuaSXV1S2nSXQIpC5vLJAQkDmILGGk8FmCQmL7TjksXs5L90Cq8eec/9TVqOCrXYwlQ2UV04/4nsmEOoUZ9Mbbf97hJSSCSMBGkirCiCZKCP6ty+lO37edIBHpVmnCrkvfIjwwRDBVoA++BfeSSq623HPJIvS8kQ7MU1gQdGZInWM+4eKUR9ELNniLxZJOIC72y5gdMPvSAKrNsjUoMZ6evHjo0uMwkgj9IU8NKUz0D6kDawKf0JzDGYMi5tuanrbbD1Z2z/2VX6xaBYju0FhQgHmq0RuMrFFXr0TkbAuPDK5M//OjjcR/zdLF7spoOPtwdEuzc448Rd/wsV+Mk+AJp2aXDac6UigEppMSUOBselKI2FZL6gmyOzw4THBsO+vpkkw4GYXf8rzcHjPFRLTkZLS54/ywEQjDE/fIOkrp0WItgYC8ot4h3t0aTRk9VJ629radIUi3gscGJpWii/h563YTOhWfs09HRVFoWMw/6pxME8M+bwZ8kc43hrjHfs7q3PY7RSKI904QgB52QggcZV+xG5Hq8eDQ4U8a5QOpU4AynB/3o1owkEO6+1HnXlmdO7KNwdR7PjvJsbWAIMaNQyGkTR3GHlALVZcWI5E086OQ8NhAVudkUOcCuXZjTdfRlLtWVVeEGGITMNJVwY9We82pETtQgjEyLwRx0bT0BhGb6pAByPR+ezGAejWsPDwhTGh2RdoymShOa3qo39MgfAVfiBv9YKQDRwddwUizFDbKWKaO37jqJ93tmFBrdttTVfCr7zdFFKyfGwmPD5GWimnDRAWdMSgBSfSPCVoEy5/MMiYd+auVbNJtHSHq7YswVfXfjJH1qlMgMZ43kbSgstariZIuVkkNjw9Uf9Q5A/xtMPjJfGKRqOChpZuK70fGyGqVSwDoXncsfK3bhM65K61IJJU5HOsIhTCzdAfSmK46K0TZ7uE5EQcZWsdrpEsTPEXldoQEb6EjT7q1/XeCIQEmigtDZCD2GhEh7d4EIDRVTzrx+jR0HkB/DOcSQaU3eEoI/cnqZ3T3iqDyJXiKxnnMGJmEZlcQkAtats2437H1S8GukntNLze3Vcz61z5FwGh1nxNfuh4rIwT6NTwnYqsDo+a1olAgTfAUFYlk1aJhoMmOa+4F+iqh3zpvPUFTeyE0kqOqo1yOFrUSkRC609qnKL1w9VRjAk8JBDrnP+qcl6wedeyXmlStQQB3WvHSX/oJq3MrVufgumZSF8eRtApVFOE1JYxUs5dc/X2p7+cxgFwPw1TB80KtU0GX2UwjQfIrc+38wPNyNclHtFY70iCJ1EVh2k7qXA6AOG7Yy1hW30T9R6eB6qv6Gi6pBEy3329GwrMCo67mReap4odeh4YlGOgFKSyentF5w93X5lMLOu77maORDTcs4Pb7min3ee5QyHR+UGIENm1jX8KzAqX7o85ZiQzkgfzVV57JTQiyAYvvV0+I2q9okTA97yTtzrVLwKDVZaRiKVVe2DR2MhFqN5uBEPG8h6cFknfOa1ah1w2rc+/nCKmu9iNeesUf6KGnXUTJoetgY8iN0JpaeywXMUoUs1MlELHeXgoBuPke3jz58AZ4Zqi050I5Chip3jPLPvFJTDcWJS64YARYalgkZB3bNv20yulyvjT08vuwWgDIzXLfIWKx3Z6EQtXO4YmB6OMf5lkET1EE0hE2U3pKkODS6YQ/Y60q6Rbbyyfhf7ZcZpIatltJWlFWzQuQaMsvpZwyvrPp+oomhmcGogDpIgZLvwniRRXMof3OkBrWb8e0O4ftxjAjaR1IEC+1BgfSe2W8k5G1um4NChl157EiZnqhfXhuIKCeMWHV83fOzVnFGFkeGXdc1lggXMnIjwYmUTKzFqXBXbOhMjz7+NBUwLYXGm3G/VBRGlTDPpXw7ED9xku3aRoFdfLHHOrAGEHC12iSE0amzZQl9dneU65+De8WwC2G1QJBFJfLlwRUx9VJCSPq+YdLcnhyoHR/1LmYkAJcQHq3CaPo9XJnqVO+EJBLu+wJjcR8xXrvoplroTAupiqaCJNkOu1yK5zyp1d4diAdcJbUsZ7u6FFcrRzStn+PaenjcxMRizrsFCIo31gShGUuFAk3hbJlM0GDZtefBUI0vdC2yG4Y4BfCdsw81bhnjEwOgZ5eg2qRaAekPSkpJpkBUufmJGuI+rzMAND3A3Xwbjt8rgHU7nvfEfHrt/D8QJkHda6wBRhEKRLQjp07xYStXpPZKaPXp8uZpIHSsHW0Sd895aY/3KYC0dWrk06kGF0/3GhkzaSF50fAyIJpnvUvYex43wjah9ssopI1LFMWeJckuJuybxUK0LOpFxaMXyg1GUmtk8X0w0gh8uP+mkj4lUBS5xrF6tyxoCys7twatMdOAYIet04qAE3qHE0wjOpGGkWk2veaMHK3PZB9Z7a/7KhxXBJGIvxOYH6kogNFGSZ1tnRsD4wd6ESAkjItG6q4u0MlALB9vXhF3f3wFQPol2G7AIjGl/2HQGlfVwejIvidQOGiMBgwkBSKMFJvSsUNy7mQROqGL03OgNtnTprxvmcgf129CgnY7ObOWpn7N5eXJnFZMp6W8EvBprnWEzNaV1R85HjJniJ/v6RIImnfUebm884ICS7/KKxLpKmNS8dCxCp9e0tRYXzafxoE9z7c4PciOfaacrNlpRbt6WqJe7S1Ju6hC4fjCaCSqqkVGJE0q02CIK6375Qq12UgI3JxGT43hEL7/i9h30PadAjHrAAAAABJRU5ErkJggg==";
},
e738:function e738(e,l,a){
"use strict";
(function(e){
Object.defineProperty(l,"__esModule",{
value:!0}),
l.default=void 0;
var a={
data:function data(){
return{};
},
computed:{
share:function share(){
return{
title:"",
thumb:"",
desc:"",
path:"",
content:""};

}},

methods:{
getShareConfig:function getShareConfig(l){
l=l||!1;
var a=this.$store.getters.setting.share||{},t=this.$store.getters.userInfo||{};
!t.uuid&&l&&e.navigateTo({
url:"/pages/login/index"});

var n=this.share.path;
if(!n){
var r=getCurrentPages();
n=r[r.length-1].route;
var u=r[r.length-1].options,i="";
for(var o in u){"inviter"!==o&&(i+=o+"="+u[o]+"&");}
i&&(n+="?"+i);
}
t.uuid&&(-1!==n.indexOf("?")?n+="&inviter="+t.uuid:n+="?inviter="+t.uuid);
var s=this.$store.getters.setting.share.app_share_url+"?path="+n.replace("?","&"),c=this.share.title,v=this.share.thumb;
return c||(c=(c=a.share_title||"{name}邀请您来开盲盒啦~").replace("{name}",t.name||""),
v=v||a.share_image),{
title:c,
path:n,
app_url:s,
imageUrl:v,
desc:this.share.desc,
content:this.share.content,
success:function success(e){},
fail:function fail(e){}};

}},

onShareAppMessage:function onShareAppMessage(e){
return this.getShareConfig();
},
onShareTimeline:function onShareTimeline(e){
return this.getShareConfig();
}};

l.default=a;
}).call(this,a("543d").default);
},
eb95:function eb95(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAAAqFBMVEUdHR8xMTNyj50AAAMDAwcfHyIREhUaGh1vjJoNDRAGBglogo8PDxMLCw4XFxpieoYkJyoICAxedYE9S1JBUVkVFRdGV2A5Q0o2QEYhIyZtiZZshpRlfootMzduiphqhJFXbHZUaHJSZG5FU1xATVU0O0A8R05Zbnpccn0wO0MrLzQjLDMoLC8SFhtKWmIrNT0vNjo5SFAnMTggJy1PYGpOX2gZHyQcIyjCTm1kAAAEkUlEQVR42u3aaXPaMBAG4O2ufOADX4T7DFfCkQBp0v//z2pLFjGhndJPyO0+n5iBzLyWLWm1DnyrMQ5/Iw7P4U3A4W/E4f+d8IwxxhhjjDH2j+pCfW3cEdTWFFOoqxEidqCm1og4tB6gjg5YWFATaugJC/3Qg/pZotIiG2pnjYobCDCTpcGVR1T6goLf/tVdHSJUHnvwKV7vAFb6GyKxHlWjt1yUBiu4L2teJvGXoHV9fAeIUXkm2qK7sKC0S/VVxXB3uzUq72rwNzNEHMcAQ5RsomLdidTFWdkYpeESjLAaoDR46XTmKUpzgHcsREQ/yrjF1xFKbtsCIzS9pI1fzZykW66U9IxfpZuAFHH3dTSgHxFe+N4UtMfCGxG9ulg1yByPlNCBu3MScawGzBLKRcXDLyh36OOnWWyTIgype2yxn6L2TFK7SCpDwmiApeEh8EhpgAE6mQXghPTqo/RKyklfRwjwpsbezQQVfhzJM6Ngi8d+sYw/CDVxM1LCnou4p9yDrjGneyq8TfF094mqfUf0Fz2ABn2kOCUlAEjR9ygHhTlOXlX0NaIPxthgbtDqQTMR3T1Jxci+4HuDiBIoWO3iM22f1DZgjjXK+PMe2OIzOyyx+yDvgdIkOqWI5QYMD0ZMWIAuKv127ISfC4nV7zlE1ATF6kZ6G8ije8bU+UMsjV/iPJYHyhzAI1FGXw1R2+S/IWPK/BWejecQ6LGWkziEwtLHs0dPzWhTjFIsuUuoeiC1jcZ63PWiQwZUBprVQuXUuEylQ+4mKM1CkkIwyXaMuS4RXcQPoDTqqypTMawjYr/pWubieXZAW6jsJg48QLOFOBGkC4IrM8RUmFLGX4ldxK3eoq70xognkhKDJutZhPidiILfLqc9c2rhKymim8haZrOyoDR6XMW6/RcHBj7uitWXFfw+mw36Bzhbuhi9LHeYW4ZEhvYuO5ib+Ig4GF2VPm7ZgTKmpLm0W8iAOntFR38x3SYm7axa99FHzd9AoWlXxl5z13NDWjafdi6e+Xs7D54Ial4uNdoLGGaKFdF86xGJcx1/aKdYZdg7wiV+4U47Fkir2QC/SI16cKwhan3UMijEqPl41jGnlgdooTa021iaQiHDUhQvUBvb5myycf+cMCDSV+Ja1cmQOh4dUZuZUxE/VrJX+qpbL3B65ee2oFyG2qsp++zIH0ZP61m7dWyQ9DFRgYnsru5eKs/HY7ZYtFqtzKAjbC4gTTgLNUOJEnVTnkjxHPkr486wTiW7LOxzHyTUPTjp7AAN0hIwRljJrmdBRh/qFojzye+c3qQKrSmE5yVhGKrD9wgLaTlDOxfnWrshhaE5y+Uv//HATjE3DomMLeN/371UA9826yG5tXsZYWFPZFBn8haLczkzJbM6kzfo9XX4rWmr+g1eyvCRkQ2yP9i5KvyzgZ3JP/suw0eeYbvpbXqTIvyboJwhr7r/Qgcn+E6ibutk6WkyaJCo2QalbSYdIlHPgQdogyBRxydesUnUbY2vSETNNteqpoFvcG5n7JGDMcYYY4wxxhhjjDHGGGOMMcYYY4yx/8ZPrvROboNHRUMAAAAASUVORK5CYII=";
},
ec6a:function ec6a(e,l,a){
(function(l){
function a(e){
for(var l=Object.create(null),a=e.split(","),t=a.length;t--;){l[a[t]]=!0;}
return l;
}
var t=a("align,alt,app-id,appId,author,autoplay,border,cellpadding,cellspacing,class,color,colspan,controls,data-src,dir,face,height,href,id,ignore,loop,muted,name,path,poster,rowspan,size,span,src,start,style,type,unit-id,unitId,width,xmlns"),n=a("a,abbr,ad,audio,b,blockquote,br,code,col,colgroup,dd,del,dl,dt,div,em,fieldset,h1,h2,h3,h4,h5,h6,hr,i,img,ins,label,legend,li,ol,p,q,source,span,strong,sub,sup,table,tbody,td,tfoot,th,thead,tr,title,u,ul,video"),r=a("address,article,aside,body,center,cite,footer,header,html,nav,pre,section"),u=a("area,base,basefont,canvas,circle,command,ellipse,embed,frame,head,iframe,input,isindex,keygen,line,link,map,meta,param,path,polygon,rect,script,source,svg,textarea,track,use,wbr"),i=a("a,colgroup,fieldset,legend,sub,sup,table,tbody,td,tfoot,th,thead,tr"),o=a("area,base,basefont,br,col,circle,ellipse,embed,frame,hr,img,input,isindex,keygen,line,link,meta,param,path,polygon,rect,source,track,use,wbr"),s=a(" , ,\t,\r,\n,\f"),c={
a:"color:#366092;word-break:break-all;padding:1.5px 0 1.5px 0",
address:"font-style:italic",
blockquote:"background-color:#f6f6f6;border-left:3px solid #dbdbdb;color:#6c6c6c;padding:5px 0 5px 10px",
center:"text-align:center",
cite:"font-style:italic",
code:"padding:0 1px 0 1px;margin-left:2px;margin-right:2px;background-color:#f8f8f8;border-radius:3px",
dd:"margin-left:40px",
img:"max-width:100%",
mark:"background-color:yellow",
pre:"font-family:monospace;white-space:pre;overflow:scroll",
s:"text-decoration:line-through",
u:"text-decoration:underline"},
v=l.getSystemInfoSync().screenWidth/750,b=l.getSystemInfoSync().SDKVersion;
function f(){
for(var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:"",l=b.split("."),a=e.split(".");l.length!=a.length;){l.length<a.length?l.push("0"):a.push("0");}
for(var t=0;t<l.length;t++){if(l[t]!=a[t])return parseInt(l[t])>parseInt(a[t]);}
return!0;
}
function h(e){
for(var l=e._STACK.length;l--;){
if(i[e._STACK[l].name])return!1;
e._STACK[l].c=1;
}
return!0;
}
f("2.7.1")?(n.bdi=!0,n.bdo=!0,n.caption=!0,n.rt=!0,n.ruby=!0,u.rp=!0,
n.big=!0,n.small=!0,n.pre=!0,i.bdi=!0,i.bdo=!0,i.caption=!0,i.rt=!0,
i.ruby=!0,i.pre=!0,r.pre=void 0):(r.caption=!0,c.big="display:inline;font-size:1.2em",
c.small="display:inline;font-size:0.8em"),e.exports={
highlight:null,
LabelAttrsHandler:function LabelAttrsHandler(e,l){
switch(e.attrs.style=l.CssHandler.match(e.name,e.attrs,e)+(e.attrs.style||""),
e.name){
case"div":
case"p":
e.attrs.align&&(e.attrs.style="text-align:"+e.attrs.align+";"+e.attrs.style,
e.attrs.align=void 0);
break;

case"img":
e.attrs["data-src"]&&(e.attrs.src=e.attrs.src||e.attrs["data-src"],e.attrs["data-src"]=void 0),
e.attrs.src&&!e.attrs.ignore&&(h(l)?e.attrs.i=(l._imgNum++).toString():e.attrs.ignore="true");
break;

case"a":
case"ad":
h(l);
break;

case"font":
if(e.attrs.color&&(e.attrs.style="color:"+e.attrs.color+";"+e.attrs.style,
e.attrs.color=void 0),e.attrs.face&&(e.attrs.style="font-family:"+e.attrs.face+";"+e.attrs.style,
e.attrs.face=void 0),e.attrs.size){
var a=parseInt(e.attrs.size);
a<1?a=1:a>7&&(a=7),e.attrs.style="font-size:"+["xx-small","x-small","small","medium","large","x-large","xx-large"][a-1]+";"+e.attrs.style,
e.attrs.size=void 0;
}
break;

case"video":
case"audio":
e.attrs.id?l["_"+e.name+"Num"]++:e.attrs.id=e.name+ ++l["_"+e.name+"Num"],
"video"==e.name&&(e.attrs.style=e.attrs.style||"",e.attrs.width&&(e.attrs.style="width:"+parseFloat(e.attrs.width)+(e.attrs.width.includes("%")?"%":"px")+";"+e.attrs.style,
e.attrs.width=void 0),e.attrs.height&&(e.attrs.style="height:"+parseFloat(e.attrs.height)+(e.attrs.height.includes("%")?"%":"px")+";"+e.attrs.style,
e.attrs.height=void 0),l._videoNum>3&&(e.lazyLoad=!0)),e.attrs.source=[],
e.attrs.src&&e.attrs.source.push(e.attrs.src),e.attrs.controls||e.attrs.autoplay||console.warn("存在没有controls属性的 "+e.name+" 标签，可能导致无法播放",e),
h(l);
break;

case"source":
var t=l._STACK[l._STACK.length-1];
!t||"video"!=t.name&&"audio"!=t.name||(t.attrs.source.push(e.attrs.src),
t.attrs.src||(t.attrs.src=e.attrs.src));}

e.attrs.style.includes("url")&&(e.attrs.style=e.attrs.style.replace(/url\s*\(['"\s]*(.*?)['"]*\)/,function(e,a){
if(a&&"/"==a[0]){
if("/"==a[1])return"url("+l._protocol+":"+a+")";
if(l._domain)return"url("+l._domain+a+")";
}
return e;
})),e.attrs.style.includes("rpx")&&(e.attrs.style=e.attrs.style.replace(/[0-9.]*rpx/,function(e){
return parseFloat(e)*v+"px";
})),e.attrs.style||(e.attrs.style=void 0),l._useAnchor&&e.attrs.id&&h(l);
},
trustAttrs:t,
trustTags:n,
blockTags:r,
ignoreTags:u,
selfClosingTags:o,
blankChar:s,
userAgentStyles:c,
versionHigherThan:f,
makeMap:a,
rpx:v};

}).call(this,a("543d").default);
},
f0c5:function f0c5(e,l,a){
"use strict";
function t(e,l,a,t,n,r,u,i,o,s){
var c,v="function"==typeof e?e.options:e;
if(o){
v.components||(v.components={});
var b=Object.prototype.hasOwnProperty;
for(var f in o){b.call(o,f)&&!b.call(v.components,f)&&(v.components[f]=o[f]);}
}
if(s&&("function"==typeof s.beforeCreate&&(s.beforeCreate=[s.beforeCreate]),
(s.beforeCreate||(s.beforeCreate=[])).unshift(function(){
this[s.__module]=this;
}),(v.mixins||(v.mixins=[])).push(s)),l&&(v.render=l,v.staticRenderFns=a,
v._compiled=!0),t&&(v.functional=!0),r&&(v._scopeId="data-v-"+r),
u?(c=function c(e){
(e=e||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext)||"undefined"==typeof __VUE_SSR_CONTEXT__||(e=__VUE_SSR_CONTEXT__),
n&&n.call(this,e),e&&e._registeredComponents&&e._registeredComponents.add(u);
},v._ssrRegister=c):n&&(c=i?function(){
n.call(this,this.$root.$options.shadowRoot);
}:n),c)if(v.functional){
v._injectStyles=c;
var h=v.render;
v.render=function(e,l){
return c.call(l),h(e,l);
};
}else{
var d=v.beforeCreate;
v.beforeCreate=d?[].concat(d,c):[c];
}
return{
exports:e,
options:v};

}
a.d(l,"a",function(){
return t;
});
},
fdb9:function fdb9(e,l){
e.exports="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALwAAACOCAMAAAC8JZI/AAAAn1BMVEUdHR8xMTMAAAB1lKB3lqJeeIJvjZhzkp1phpBxj5pcdn94mKNykZxujJZ8nal6m6Z5maVgeoRlgIoUFBYaGhxtipVohI4ICAhngoxjfodhfIVriJMEBQUiJShbc3sXGBlKXGMxOj8tNTkODxALCww0PkMREhM/TlQkKCtXbnc4Q0hNYGhTaHAYHiBHWF86SE4qMjUoLDBPY2tDVFokLjLaDNrOAAAFOklEQVR42u3Z55aiMBgG4GwUCzg0UYp0BKQIWO7/2vYLTWXd8s/sbp6TGRhgznnJfEmQQd/+Yiz8H2LhWXgasPB/iIX/d8IzDMMwDMMwDMMwzH/kZKQOQpqdaOiP5HwO37f6FlHgJIrzMtQwxkcP/c7FX4tirkU2xrSEB0YAefBvuj/nRSKEK6kKL4oz/4x/1f0XfyF27hSFlyRREltKoP+s+3MerqEwvChKqX+QWmP3TztdaslZmEnSXU90WsJDqABp51uf34on3Q+d3lHyCONSknKEqAkvyzKEh8BhuZZlSZaWQ/cPnQ4H5VXqkXuKXFmmMjzEiTm51XW/HvCC3CodDMjcTll4QRDG8CiVgSALwtI/pQc4BT9ASzChIRJeoDe88M4OtxBwBWrCXxsolXITIrQ9duHnT4S5AA12Skzobfg5d0HofNM1hLwIfZIlKjFCkU6qWdPaAWrMp/YVGcB2N39WcH3oigpEP8LtfFIBa84qTdq5EfUuN372wNdaO8t4aBAr8EvFlozoz/Z8LhKb9IhBpKGeY846u6gfqaPakMiS3FCwyp6kYWX1XlbWdNa5YeKIBulB6gw39VELeZCdhqkcOJvZcgbtjIn+lpxqvHqHCRt9lis8mIE9Fn+2JKynjJdiSRaAzjD9fFY1f7arh+KPlxsIn2LcD8umnAlP82eN8cfHK0Lx/NlsvumLX1ttNpv1qa/swJ2/cigYr1DHsx+UbeQbhHdRRIoj5mZTOg3jFSFj1lvOltBmhALF763X6xp52A6t8fh4jUXFeEWoXL5zxdhdHyIoK3X5ht/PoJ+WbnowQqH124WOg0WFtcvLuVFMxXhFKNj01pv1kwYfdw5O/PEctMd2mP4/7mu9WKwX6wmomxQKY7V+Z2FjAn2esnjnoGNIeF+85VIyXhEqFl9fi68FtHY7aDC4To7DDnGjZLwiVH+9VUC8ZLd7ey7ERII+r9m9tdIxrnfvebSMV4ScUxPGeZ1W/rV0OfXQu2OsDPsGl5X+Lc3j8H46a9tku/W0jy+vU6e8MA89H58OA7NqIlLlCW2JHdM0Q6Th473iVk8M3V89c2vHxtiuTDNA6Pj5p7KWA8Eg/HU1Fe9XE0aOMdxRQ8mrj0f4dD/Ksnaj7n/QtOFDisLv9yT82Whl6f2I7dTY7429QVSRF19V2CciCL/fB1SF35Oazwy3ik82ts+XJqyMjpqfz2cPbwOfM1QjO7bhY6rCGyS8F5E4noZidSLAxCnNbnC+MAyawhtGG/4Rp1BfFC9PMlfDoKlsVHUS3uFM1Rwpyct6elVVusKrQ/hebJkWNLMVYiJCf0t45Fsjf/KGpqA+vOMqLUvhI0x4tIY3zUl4ECu9Zvr0W5gmXeHNLCDh7ac8PteqJkXj3CwIrx0peOPUciwTFEmb0nsUDgfc15fBTsqZMJAvVLwu61wKCyi1jsFxCBrzIE6ei6bmLVCeMbDpyA6CzAJ8iJPAv0fIi3TI6/O8D8XdFk1ehCh3LeDS8xFwlPMKcOHLKrqe9ZwsOyEykLU4g+M8Ocfl3Z+Hss8kzo1TBmnUJgxiBJKmVAZVNzCo+PA6LX1uwMf28D+q5qpwLYWjrdinpc8NsjsmHP9xJKTk/eRP1S43uJ5xlHKjmspin5Y+z/HQOGh+v4Ups9rSWuzT0uenSofmYp+WPv8sC23Ki/1V3cd3edetj/QX+7T03U5X7Db1xf6qKSB62VaMTdWzwJ8JypossHb0F1UMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAMwzAM84/4DvypsXKugUHXAAAAAElFTkSuQmCC";
}}]);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! (webpack)/buildin/global.js */ 3)))

/***/ }),
/* 12 */
/*!****************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/common/main.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(global.webpackJsonp = global.webpackJsonp || []).push([
['common/main'],
{
  '00a9': function a9(t, e, o) {
    'use strict';

    (function (t, e, n) {
      var a = o('4ea4');
      var r = a(o('9523'));
      o('18ba');
      var u = a(o('66fd'));
      var c = a(o('ed57'));
      var i = a(o('0863'));
      var l = a(o('c50c'));
      var f = a(o('1c67'));
      var d = a(o('bd8c'));
      var p = a(o('e738'));
      var s = a(o('452d'));
      var y = a(o('5cff'));
      function h(t, e) {
        var o = Object.keys(t);
        if (Object.getOwnPropertySymbols) {
          var n = Object.getOwnPropertySymbols(t);
          if (e) {
            n = n.filter(function (e) {
              return Object.getOwnPropertyDescriptor(t, e).enumerable;
            });
          }
          o.push.apply(o, n);
        }
        return o;
      }
      t.__webpack_require_UNI_MP_PLUGIN__ = o;
      u.default.mixin(p.default);
      u.default.mixin(s.default);
      u.default.component('tab-bar', function () {
        Promise.all([o.e('common/vendor'), o.e('components/TabBar/tabBar')]).
        then(
        function () {
          return resolve(o('8e61'));
        }.bind(null, o)).

        catch(o.oe);
      });
      u.default.prototype.$api = y.default;
      var v = o('921a');
      u.default.prototype.$upload = v;
      var m = o('50c4');
      u.default.prototype.$navigator = m;
      var b = o('2000');
      u.default.prototype.$visitor = b;
      u.default.prototype.$tool = f.default;
      u.default.prototype.$device = d.default;
      u.default.prototype.$store = l.default;
      u.default.prototype.$http = i.default;
      u.default.config.productionTip = false;
      u.default.prototype.$showPullRefresh = function () {
        e.showLoading({
          title: '刷新中' });

        setTimeout(function (t) {
          e.stopPullDownRefresh();
        }, 200);
        setTimeout(function (t) {
          e.hideLoading();
        }, 500);
        return l.default.dispatch('getSetting');
      };
      e.$showMsg = function () {
        if (arguments.length > 0 && void 0 !== arguments[0]) {
          var t = arguments[0];
        } else {
          var t = '请求出错';
        }
        if (arguments.length > 1 && void 0 !== arguments[1]) {
          var o = arguments[1];
        } else {
          var o = 2000;
        }
        e.showToast({
          title: t,
          icon: 'none',
          duration: o });

      };
      var g = e.$on;
      e.$on = function (t, o) {
        try {
          e.$off(t);
        } catch (t) {
          console.log('CatchClause', t);
          console.log('CatchClause', t);
        }
        g(t, o);
      };
      var $ = {
        click: 'https://cdn2.hquesoft.com/box/audio/click.mp3',
        check: 'https://cdn2.hquesoft.com/box/audio/check.mp3',
        yao: 'https://cdn2.hquesoft.com/box/audio/yao.mp3',
        open: 'https://cdn2.hquesoft.com/box/audio/open.mp3' };

      var O = {
        click: null,
        check: null,
        yao: null,
        open: null };

      setTimeout(function () {
        for (var t in $) {
          O[t] = e.createInnerAudioContext();
          O[t].src = $[t];
        }
      }, 800);
      u.default.prototype.$playAudio = function (t) {
        O[t] || (O[t] = e.createInnerAudioContext(), O[t].src = $[t] || t);
        O[t].play();
      };
      var P = null;
      u.default.prototype.$playBgm = function (t, o) {
        P || (P = e.createInnerAudioContext());
        if (t) {
          P.src = t;
          P.loop = true;
        }
        P.play();
        l.default.dispatch('setIsBgmPlay', true);
      };
      u.default.prototype.$stopBgm = function () {
        if (P) {
          P.stop();
        }
        l.default.dispatch('setIsBgmPlay', false);
      };
      u.default.prototype.$switchBgm = function () {
        l.default.getters.isBgmPlay ? u.default.prototype.$stopBgm() : u.default.prototype.$playBgm();
      };
      c.default.mpType = 'app';
      n(
      new u.default(
      function (t) {
        for (var e = 1; e < arguments.length; e++) {
          var o = null != arguments[e] ? arguments[e] : {};
          e % 2 ?
          h(Object(o), true).forEach(function (e) {
            (0, r.default)(t, e, o[e]);
          }) :
          Object.getOwnPropertyDescriptors ?
          Object.defineProperties(t, Object.getOwnPropertyDescriptors(o)) :
          h(Object(o)).forEach(function (e) {
            Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(o, e));
          });
        }
        return t;
      }(
      {
        store: l.default },

      c.default))).


      $mount();
    }).call(this, o('bc2e').default, o('543d').default, o('543d').createApp);
  },
  '120c': function c(t, e, o) {
    'use strict';

    var n = o('6656');
    o.n(n).a;
  },
  2131: function _(t, e, o) {
    'use strict';

    o.r(e);
    var n = o('ac63');
    var a = o.n(n);
    for (var r in n) {
      if (['default'].indexOf(r) < 0) {
        (function (t) {
          o.d(e, t, function () {
            return n[t];
          });
        })(r);
      }
    }
    e.default = a.a;
  },
  6656: function _(t, e, o) {},
  ac63: function ac63(t, e, o) {
    'use strict';

    var n = o('4ea4');
    Object.defineProperty(e, '__esModule', {
      value: true });

    e.default = void 0;
    var a = o('cbb4');
    var r = (o('2684'), n(o('3162')));
    var u = {
      onLaunch: function onLaunch(t) {
        var that = this;
        this.$store.dispatch('setEnterScene', t.scene);
        if (t.query && t.query.inviter) {
          (0, a.$setStorage)('inviter', t.query.inviter);
          (0, a.$setStorage)('invite_node', t.query.invite_node);
        }
        this.$store.dispatch('getSetting');
        setTimeout(function () {
          that.$store.dispatch('getSetting');
        }, 500);
        setTimeout(function () {
          r.default.checkUpdate();
        }, 2000);
      },
      onHide: function onHide() {
        console.log('App Hide');
      } };

    e.default = u;
  },
  ed57: function ed57(t, e, o) {
    'use strict';

    o.r(e);
    var n = o('2131');
    for (var a in n) {
      if (['default'].indexOf(a) < 0) {
        (function (t) {
          o.d(e, t, function () {
            return n[t];
          });
        })(a);
      }
    }
    o('120c');
    var r = o('f0c5');
    var u = Object(r.a)(n.default, void 0, void 0, false, null, null, null, false, void 0, void 0);
    e.default = u.exports;
  } },

[['00a9', 'common/runtime', 'common/vendor']]]);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! (webpack)/buildin/global.js */ 3)))

/***/ }),
/* 13 */,
/* 14 */,
/* 15 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 16 */
/*!********************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/index.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0;
var _pageLifetimes = __webpack_require__(/*! ./lifecycle/pageLifetimes */ 17);


var _clone = __webpack_require__(/*! ./methods/clone */ 18);
var _dataset = __webpack_require__(/*! ./methods/dataset */ 19);
var _escape = __webpack_require__(/*! ./methods/escape */ 20);
var _event = __webpack_require__(/*! ./methods/event */ 21);
var _getTabBar = __webpack_require__(/*! ./methods/getTabBar */ 22);
var _relation = __webpack_require__(/*! ./methods/relation */ 23);
var _selectComponent = __webpack_require__(/*! ./methods/selectComponent */ 24);

var _setData = __webpack_require__(/*! ./methods/setData */ 25);function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}var _default =

{

  install: function install(Vue, option) {
    Vue.mixin(_objectSpread({},
    _pageLifetimes.pageLifetimes, {
      methods: {
        clone: _clone.clone,
        handleDataset: _dataset.handleDataset,
        escape2Html: _escape.escape2Html,
        html2Escape: _escape.html2Escape,
        parseEventDynamicCode: _event.parseEventDynamicCode,
        getTabBar: _getTabBar.getTabBar,
        getRelationNodes: _relation.getRelationNodes,
        zpSelectComponent: _selectComponent.selectComponent,
        zpSelectAllComponents: _selectComponent.selectAllComponents,
        setData: _setData.setData } }));


  } };exports.default = _default;

/***/ }),
/* 17 */
/*!**************************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/lifecycle/pageLifetimes.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.pageLifetimes = void 0; /**
                                                                                                            * 组件pageLifetimes处理，需在页面生命周期里调用
                                                                                                            * @param {Object} node
                                                                                                            * @param {Object} lifeName
                                                                                                            */
function handlePageLifetime(node, lifeName) {
  node.$children.map(function (child) {
    if (typeof child[lifeName] == 'function') child[lifeName]();
    handlePageLifetime(child, lifeName);
  });
}

var pageLifetimes = {
  onLoad: function onLoad() {var _this = this;

    uni.onWindowResize(function (res) {
      handlePageLifetime(_this, "handlePageResize");
    });

  },
  onShow: function onShow() {
    handlePageLifetime(this, "handlePageShow");
  },
  onHide: function onHide() {
    handlePageLifetime(this, "handlePageHide");
  },
  onResize: function onResize() {



  } };exports.pageLifetimes = pageLifetimes;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 18 */
/*!****************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/clone.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.clone = clone; /**
                                                                                                   * 用于处理对props进行赋值的情况
                                                                                                   * //简单处理一下就行了
                                                                                                   *
                                                                                                   * @param {*} target
                                                                                                   * @returns
                                                                                                   */
function clone(target) {
  return JSON.parse(JSON.stringify(target));
}

/***/ }),
/* 19 */
/*!******************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/dataset.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.handleDataset = handleDataset; /**
                                                                                                                   * 用于处理dataset
                                                                                                                   * 自定义组件的事件里，是获取不到e.currentTarget.dataset的
                                                                                                                   * 因此收集data-参数，手动传进去
                                                                                                                   *
                                                                                                                   * @param {*} event
                                                                                                                   * @param {*} dataSet
                                                                                                                   */
function handleDataset(event) {var dataSet = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  if (event && !event.currentTarget) {
    if (dataSet.tagId) {
      event.currentTarget = {
        id: dataSet.tagId };

    } else {
      event.currentTarget = {
        dataset: dataSet };

    }
  }
}

/***/ }),
/* 20 */
/*!*****************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/escape.js ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.escape2Html = escape2Html;exports.html2Escape = html2Escape; /**
                                                                                                                                                 * 转义符换成普通字符
                                                                                                                                                 * @param {*} str
                                                                                                                                                 * @returns
                                                                                                                                                 */
function escape2Html(str) {
  if (!str) return str;
  var arrEntities = {
    'lt': '<',
    'gt': '>',
    'nbsp': ' ',
    'amp': '&',
    'quot': '"' };

  return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) {
    return arrEntities[t];
  });
}

/**
   * 普通字符转换成转义符
   * @param {*} sHtml
   * @returns
   */
function html2Escape(sHtml) {
  if (!sHtml) return sHtml;
  return sHtml.replace(/[<>&"]/g, function (c) {
    return {
      '<': '&lt;',
      '>': '&gt;',
      '&': '&amp;',
      '"': '&quot;' }[
    c];
  });
}

/***/ }),
/* 21 */
/*!****************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/event.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.parseEventDynamicCode = parseEventDynamicCode; /**
                                                                                                                                   * 解析事件里的动态函数名，这种没有()的函数名，在uniapp不被执行
                                                                                                                                   * 比如：<view bindtap="{{openId==undefined?'denglu':'hy_to'}}">立即</view>
                                                                                                                                   * @param {*} exp
                                                                                                                                   */
function parseEventDynamicCode(e, exp) {
  if (typeof this[exp] === 'function') {
    this[exp](e);
  }
}

/***/ }),
/* 22 */
/*!********************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/getTabBar.js ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.getTabBar = getTabBar; /**
                                                                                                           * 接管getTabBar函数，默认uni-app是没有这个函数的
                                                                                                           * 适用于使用custom-tab-bar自定义导航栏的小程序项目
                                                                                                           * 需注意：
                                                                                                           * 1.custom-tab-bar下面仍是小程序文件
                                                                                                           * 2.pages.json里面需使用条件编译区分好小程序和非小程序的tabBar配置
                                                                                                           */
function getTabBar() {
  return {
    setData: function setData(obj) {var _this$$mp, _this$$mp$page, _this$$mp2, _this$$mp2$page;
      if (typeof ((_this$$mp = this.$mp) === null || _this$$mp === void 0 ? void 0 : (_this$$mp$page = _this$$mp.page) === null || _this$$mp$page === void 0 ? void 0 : _this$$mp$page.getTabBar) === 'function' && ((_this$$mp2 =
      this.$mp) === null || _this$$mp2 === void 0 ? void 0 : (_this$$mp2$page = _this$$mp2.page) === null || _this$$mp2$page === void 0 ? void 0 : _this$$mp2$page.getTabBar())) {
        this.$mp.page.getTabBar().setData(obj);
      } else {
        console.log("当前平台不支持getTabBar()，已稍作处理，详细请参见相关文档。");
      }
    } };

}

/***/ }),
/* 23 */
/*!*******************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/relation.js ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.getRelationNodes = getRelationNodes; /**
                                                                                                                         * 组件间关系
                                                                                                                         * 注意：须与p-f-unicom配合使用！！！
                                                                                                                         * @param {*} name
                                                                                                                         * @returns
                                                                                                                         */
function getRelationNodes(name) {
  if (!this.$unicom) throw "this.getRelationNodes()需与p-f-unicom配合使用！";
  return this.$unicom('@' + name);
}

/***/ }),
/* 24 */
/*!**************************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/selectComponent.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.selectComponent = selectComponent;exports.selectAllComponents = selectAllComponents;function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(n);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var createTraverse = function createTraverse() {
  var stop = false;
  return function traverse(root, callback) {
    if (!stop && typeof callback === 'function') {
      var children = root.$children;
      for (var index = 0; !stop && index < children.length; index++) {
        var element = children[index];
        stop = callback(element) === true;
        traverse(element, callback);
      }
    }
  };
};

/**
    * 安全的JSON.stringify
    * @param {Object} node
    */
function safeStringify(node) {
  var cache = [];
  var str = JSON.stringify(node, function (key, value) {
    if (typeof value === 'object' && value !== null) {
      if (cache.indexOf(value) !== -1) {
        // 移除
        return;
      }
      // 收集所有的值
      cache.push(value);
    }
    return value;
  });
  cache = null; // 清空变量，便于垃圾回收机制回收
  return str;
}

var match = function match(node, selector) {var _vnode$context$$vnode, _vnode, _vnode$context;
  var vnode = node._vnode;

  //好家伙，在微信小程序里，node里面根本找不到class，因此这种方式没法搞了

  //关键之处！
  // console.log("attrs", (vnode.context.$vnode.data));
  vnode = (_vnode$context$$vnode = (_vnode = vnode) === null || _vnode === void 0 ? void 0 : (_vnode$context = _vnode.context) === null || _vnode$context === void 0 ? void 0 : _vnode$context.$vnode) !== null && _vnode$context$$vnode !== void 0 ? _vnode$context$$vnode : "";
  //console.log(vnode.data) -->  [Object] {"staticClass":"bar","attrs":{"_i":0}}  at selectComponent.js:72
  if (!vnode || !vnode.data) {
    return false;
  }

  var attrs = vnode.data.attrs || {};
  var staticClass = vnode.data.staticClass || '';

  var id = attrs.id || '';
  if (selector[0] === '#') {
    return selector.substr(1) === id;
  } else {
    staticClass = staticClass.trim().split(' ');
    selector = selector.substr(1).split('.');
    return selector.reduce(function (a, c) {return a && staticClass.includes(c);}, true);
  }
};

var selectorBuilder = function selectorBuilder(selector) {
  selector = selector.replace(/>>>/g, '>');
  selector = selector.split('>').map(function (s) {
    return s.trim().split(' ').join("').descendant('");
  }).join("').child('");

  // 替换掉new Function方式，因为小程序不支持new Function和eval
  //return new Function('Selector', 'node', 'all', `return new Selector(node, all).descendant('` + selector + `')`);
  return function (Selector, node, all) {
    return new Selector(node, all).descendant(selector);
  };
};var

Selector = /*#__PURE__*/function () {
  function Selector(node) {var all = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;_classCallCheck(this, Selector);
    this.nodes = [node];
    this.all = all;
  }_createClass(Selector, [{ key: "child", value: function child(

    selector) {
      var matches = [];
      if (this.all) {
        this.nodes.forEach(function (node) {var _matches;
          (_matches = matches).push.apply(_matches, _toConsumableArray(node.$children.filter(function (node) {return match(node, selector);})));
        });
      } else {
        if (this.nodes.length > 0) {
          var node = this.nodes[0].$children.find(function (node) {return match(node, selector);});
          matches = node ? [node] : [];
        }
      }
      this.nodes = matches;
      return this;
    } }, { key: "descendant", value: function descendant(

    selector) {var _this = this;
      var matches = [];
      this.nodes.forEach(function (root) {
        createTraverse()(root, function (node) {
          if (match(node, selector)) {
            matches.push(node);
            return !_this.all;
          }
        });
      });
      this.nodes = matches;
      return this;
    } }]);return Selector;}();

////////////////////////////////////////////selectComponent//////////////////////////////////////////////////
/**
 * 其他平台，如APP
 * @param {Object} selector
 */
function selectComponentOther(selector) {
  var selectors = selector.split(',').map(function (s) {return s.trim();});
  if (!selectors[0]) {
    return null;
  }
  var querySelector = selectorBuilder(selectors[0]);
  return querySelector(Selector, this, false, selector).nodes[0];
}


/**
   * 还是用这个微信小程序的实现吧
   * @param {Object} selector
   */
var selectComponentWeiXin2 = function selectComponentWeiXin2(selector) {var _this$$scope$selectCo;
  console.log(".$scope", this.$scope.selectComponent(selector));
  return ((_this$$scope$selectCo = this.$scope.selectComponent(selector)) === null || _this$$scope$selectCo === void 0 ? void 0 : _this$$scope$selectCo.data) || undefined;
};

/**
    * selectComponent
    * @param {Object} args
    */
function selectComponent(args) {
  // console.log(".$scope",this.$scope)

  //H5和小程序能正常使用这个函数
  //重写selectComponent函数，因为默认会多一层$vm
  return selectComponentWeiXin2.call(this, args);








}

////////////////////////////////////////////selectAllComponents//////////////////////////////////////////////////
/**
 * 其他平台，如APP
 * @param {Object} selector
 */
function selectAllComponentsOther(selector) {var _this2 = this;
  var selectors = selector.split(',').map(function (s) {return s.trim();});
  var selected = [];
  selectors.forEach(function (selector) {
    var querySelector = selectorBuilder(selector);
    selected = selected.concat(querySelector(Selector, _this2, true, selector).nodes);
  });
  return selected;
}


/**
   * 还是用这个微信小程序的实现吧
   * @param {Object} selector
   */
var selectAllComponentsWeiXin2 = function selectAllComponentsWeiXin2(selector) {
  var list = this.$scope.selectAllComponents(selector) || [];
  list = list.map(function (item) {return item.data;});
  return list;
};

/**
    * selectAllComponents
    * @param {Object} args
    */
function selectAllComponents(args) {

  //H5和小程序能正常使用这个函数
  //重写selectComponent函数，因为默认会多一层$vm
  return selectAllComponentsWeiXin2.call(this, args);






}

/***/ }),
/* 25 */
/*!******************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/methods/setData.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.setData = setData;var _set2 = _interopRequireDefault(__webpack_require__(/*! ../utils/_set */ 26));
var _debounce = _interopRequireDefault(__webpack_require__(/*! ../utils/debounce */ 27));function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

/**
                                                                                                                                                                    * 老setData polyfill
                                                                                                                                                                    * 用于转换后的uniapp的项目能直接使用this.setData()函数
                                                                                                                                                                    * @param {*} obj
                                                                                                                                                                    * @param {*} callback
                                                                                                                                                                    */
function oldSetData(obj, callback) {
  var that = this;
  var handleData = function handleData(tepData, tepKey, afterKey) {
    var tepData2 = tepData;
    tepKey = tepKey.split('.');
    tepKey.forEach(function (item) {
      if (tepData[item] === null || tepData[item] === undefined) {
        var reg = /^[0-9]+$/;
        tepData[item] = reg.test(afterKey) ? [] : {};
        tepData2 = tepData[item];
      } else {
        tepData2 = tepData[item];
      }
    });
    return tepData2;
  };
  var isFn = function isFn(value) {
    return typeof value == 'function' || false;
  };
  Object.keys(obj).forEach(function (key) {
    var val = obj[key];
    key = key.replace(/\]/g, '').replace(/\[/g, '.');
    var front, after;
    var index_after = key.lastIndexOf('.');
    if (index_after != -1) {
      after = key.slice(index_after + 1);
      front = handleData(that, key.slice(0, index_after), after);
    } else {
      after = key;
      front = that;
    }
    if (front.$data && front.$data[after] === undefined) {
      Object.defineProperty(front, after, {
        get: function get() {
          return front.$data[after];
        },
        set: function set(newValue) {
          front.$data[after] = newValue;
          that.hasOwnProperty("$forceUpdate") && that.$forceUpdate();
        },
        enumerable: true,
        configurable: true });

      front[after] = val;
    } else {
      that.$set(front, after, val);
    }
  });
  // this.$forceUpdate();
  isFn(callback) && this.$nextTick(callback);
}

/**
   * 变量名正则
   */
var variableNameReg = /^([^\x00-\xff]|[a-zA-Z_$])([^\x00-\xff]|[a-zA-Z0-9_$])*$/;


/**
                                                                                   * 2022-10-31 重写setData
                                                                                   * 2023-05-08 增加微信“简易双向绑定”支持
                                                                                   * 用于转换后的uniapp的项目能直接使用this.setData()函数
                                                                                   * @param {Object} obj
                                                                                   * @param {Object} callback
                                                                                   */
function setData(obj) {var _this = this;var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  Object.keys(obj).forEach(function (key) {
    (0, _set2.default)(_this, key, obj[key]);

    //处理微信“简易双向绑定”
    if (variableNameReg.test(key) && key.endsWith("Clone")) {
      var propName = key.replace(/Clone$/, "");
      if (_this.$options.propsData[propName]) {
        _this.$emit("update:".concat(propName), obj[key]);
      }
    }
  });

  (0, _debounce.default)(this, '$forceUpdate', 200);
  if (typeof callback == 'function') this.$nextTick(callback);
}

/***/ }),
/* 26 */
/*!*************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/utils/_set.js ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0; /**
                                                                                                      * lodash set
                                                                                                      * @param {*} obj
                                                                                                      * @param {*} path
                                                                                                      * @param {*} value
                                                                                                      * @returns
                                                                                                      */
function _set(obj, path, value) {
  if (Object(obj) !== obj) return obj; // When obj is not an object
  // If not yet an array, get the keys from the string-path
  if (!Array.isArray(path)) path = path.toString().match(/[^.[\]]+/g) || [];
  path.slice(0, -1).reduce(function (a, c, i) {return (// Iterate all of them except the last one
      Object(a[c]) === a[c] // Does the key exist and is its value an object?
      // Yes: then follow that path
      ?
      a[c]
      // No: create the key. Is the next key a potential array-index?
      :
      a[c] = Math.abs(path[i + 1]) >> 0 === +path[i + 1] ? [] // Yes: assign a new array object
      :
      {});}, // No: assign a new plain object
  obj)[path[path.length - 1]] = value; // Finally assign the value to the last key
  return obj; // Return the top-level object to allow chaining
}var _default =

_set;exports.default = _default;

/***/ }),
/* 27 */
/*!*****************************************************************************************************!*\
  !*** F:/小程序包解密/wxappUnpacker-master/wxa2265a5c8d674d31_uni/uni_modules/zp-mixins/utils/debounce.js ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = void 0; /**
                                                                                                      * 防抖
                                                                                                      * @param {Object} scope  //引用的this，发现不显式传this，拿不到。
                                                                                                      * @param {Object} fn
                                                                                                      * @param {Object} delay
                                                                                                      */
var t = null;
var debounce = function debounce(scope, fn, delay) {
  if (t !== null) {
    clearTimeout(t);
  }
  t = setTimeout(function () {
    scope[fn]();
  }, delay);
};var _default =

debounce;exports.default = _default;

/***/ })
]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map