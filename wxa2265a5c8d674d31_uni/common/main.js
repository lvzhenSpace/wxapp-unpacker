(global.webpackJsonp = global.webpackJsonp || []).push([
    ['common/main'],
    {
        '00a9': function (t, e, o) {
            'use strict';

            (function (t, e, n) {
                var a = o('4ea4');
                var r = a(o('9523'));
                o('18ba');
                var u = a(o('66fd'));
                var c = a(o('ed57'));
                var i = a(o('0863'));
                var l = a(o('c50c'));
                var f = a(o('1c67'));
                var d = a(o('bd8c'));
                var p = a(o('e738'));
                var s = a(o('452d'));
                var y = a(o('5cff'));
                function h(t, e) {
                    var o = Object.keys(t);
                    if (Object.getOwnPropertySymbols) {
                        var n = Object.getOwnPropertySymbols(t);
                        if (e) {
                            n = n.filter(function (e) {
                                return Object.getOwnPropertyDescriptor(t, e).enumerable;
                            });
                        }
                        o.push.apply(o, n);
                    }
                    return o;
                }
                t.__webpack_require_UNI_MP_PLUGIN__ = o;
                u.default.mixin(p.default);
                u.default.mixin(s.default);
                u.default.component('tab-bar', function () {
                    Promise.all([o.e('common/vendor'), o.e('components/TabBar/tabBar')])
                        .then(
                            function () {
                                return resolve(o('8e61'));
                            }.bind(null, o)
                        )
                        .catch(o.oe);
                });
                u.default.prototype.$api = y.default;
                var v = o('921a');
                u.default.prototype.$upload = v;
                var m = o('50c4');
                u.default.prototype.$navigator = m;
                var b = o('2000');
                u.default.prototype.$visitor = b;
                u.default.prototype.$tool = f.default;
                u.default.prototype.$device = d.default;
                u.default.prototype.$store = l.default;
                u.default.prototype.$http = i.default;
                u.default.config.productionTip = false;
                u.default.prototype.$showPullRefresh = function () {
                    e.showLoading({
                        title: '刷新中'
                    });
                    setTimeout(function (t) {
                        e.stopPullDownRefresh();
                    }, 200);
                    setTimeout(function (t) {
                        e.hideLoading();
                    }, 500);
                    return l.default.dispatch('getSetting');
                };
                e.$showMsg = function () {
                    if (arguments.length > 0 && void 0 !== arguments[0]) {
                        var t = arguments[0];
                    } else {
                        var t = '请求出错';
                    }
                    if (arguments.length > 1 && void 0 !== arguments[1]) {
                        var o = arguments[1];
                    } else {
                        var o = 2000;
                    }
                    e.showToast({
                        title: t,
                        icon: 'none',
                        duration: o
                    });
                };
                var g = e.$on;
                e.$on = function (t, o) {
                    try {
                        e.$off(t);
                    } catch (t) {
                        console.log('CatchClause', t);
                        console.log('CatchClause', t);
                    }
                    g(t, o);
                };
                var $ = {
                    click: 'https://cdn2.hquesoft.com/box/audio/click.mp3',
                    check: 'https://cdn2.hquesoft.com/box/audio/check.mp3',
                    yao: 'https://cdn2.hquesoft.com/box/audio/yao.mp3',
                    open: 'https://cdn2.hquesoft.com/box/audio/open.mp3'
                };
                var O = {
                    click: null,
                    check: null,
                    yao: null,
                    open: null
                };
                setTimeout(function () {
                    for (var t in $) {
                        O[t] = e.createInnerAudioContext();
                        O[t].src = $[t];
                    }
                }, 800);
                u.default.prototype.$playAudio = function (t) {
                    O[t] || ((O[t] = e.createInnerAudioContext()), (O[t].src = $[t] || t));
                    O[t].play();
                };
                var P = null;
                u.default.prototype.$playBgm = function (t, o) {
                    P || (P = e.createInnerAudioContext());
                    if (t) {
                        P.src = t;
                        P.loop = true;
                    }
                    P.play();
                    l.default.dispatch('setIsBgmPlay', true);
                };
                u.default.prototype.$stopBgm = function () {
                    if (P) {
                        P.stop();
                    }
                    l.default.dispatch('setIsBgmPlay', false);
                };
                u.default.prototype.$switchBgm = function () {
                    l.default.getters.isBgmPlay ? u.default.prototype.$stopBgm() : u.default.prototype.$playBgm();
                };
                c.default.mpType = 'app';
                n(
                    new u.default(
                        (function (t) {
                            for (var e = 1; e < arguments.length; e++) {
                                var o = null != arguments[e] ? arguments[e] : {};
                                e % 2
                                    ? h(Object(o), true).forEach(function (e) {
                                          (0, r.default)(t, e, o[e]);
                                      })
                                    : Object.getOwnPropertyDescriptors
                                    ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(o))
                                    : h(Object(o)).forEach(function (e) {
                                          Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(o, e));
                                      });
                            }
                            return t;
                        })(
                            {
                                store: l.default
                            },
                            c.default
                        )
                    )
                ).$mount();
            }.call(this, o('bc2e').default, o('543d').default, o('543d').createApp));
        },
        '120c': function (t, e, o) {
            'use strict';

            var n = o('6656');
            o.n(n).a;
        },
        2131: function (t, e, o) {
            'use strict';

            o.r(e);
            var n = o('ac63');
            var a = o.n(n);
            for (var r in n) {
                if (['default'].indexOf(r) < 0) {
                    (function (t) {
                        o.d(e, t, function () {
                            return n[t];
                        });
                    })(r);
                }
            }
            e.default = a.a;
        },
        6656: function (t, e, o) {},
        ac63: function (t, e, o) {
            'use strict';

            var n = o('4ea4');
            Object.defineProperty(e, '__esModule', {
                value: true
            });
            e.default = void 0;
            var a = o('cbb4');
            var r = (o('2684'), n(o('3162')));
            var u = {
                onLaunch: function (t) {
                    var that = this;
                    this.$store.dispatch('setEnterScene', t.scene);
                    if (t.query && t.query.inviter) {
                        (0, a.$setStorage)('inviter', t.query.inviter);
                        (0, a.$setStorage)('invite_node', t.query.invite_node);
                    }
                    this.$store.dispatch('getSetting');
                    setTimeout(function () {
                        that.$store.dispatch('getSetting');
                    }, 500);
                    setTimeout(function () {
                        r.default.checkUpdate();
                    }, 2000);
                },
                onHide: function () {
                    console.log('App Hide');
                }
            };
            e.default = u;
        },
        ed57: function (t, e, o) {
            'use strict';

            o.r(e);
            var n = o('2131');
            for (var a in n) {
                if (['default'].indexOf(a) < 0) {
                    (function (t) {
                        o.d(e, t, function () {
                            return n[t];
                        });
                    })(a);
                }
            }
            o('120c');
            var r = o('f0c5');
            var u = Object(r.a)(n.default, void 0, void 0, false, null, null, null, false, void 0, void 0);
            e.default = u.exports;
        }
    },
    [['00a9', 'common/runtime', 'common/vendor']]
]);
