(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/lotteryTicket/components/TicketItem" ], {
    "8ff5": function(e, t, n) {
        "use strict";
        n.r(t);
        var i = n("e555"), o = n.n(i);
        for (var s in i) [ "default" ].indexOf(s) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(s);
        t.default = o.a;
    },
    "9e92": function(e, t, n) {},
    a7dc: function(e, t, n) {
        "use strict";
        n.r(t);
        var i = n("e961"), o = n("8ff5");
        for (var s in o) [ "default" ].indexOf(s) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(s);
        n("fef5");
        var c = n("f0c5"), u = Object(c.a)(o.default, i.b, i.c, !1, null, "35dbbb07", null, !1, i.a, void 0);
        t.default = u.exports;
    },
    e555: function(e, t, n) {
        "use strict";
        (function(e) {
            var i = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = i(n("2eee")), s = i(n("c973")), c = {
                components: {},
                data: function() {
                    return {
                        isAddressListening: !1
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: {
                    sku: function() {
                        return this.info.sku;
                    }
                },
                onLoad: function() {
                    return (0, s.default)(o.default.mark(function e() {
                        return o.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                beforeDestroy: function() {
                    e.$off("selectAddress");
                },
                methods: {
                    toDetail: function() {
                        1 === this.info.status && e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    checkPrize: function() {
                        if (!this.sku) return e.showModal({
                            title: "无奖品",
                            content: "未查到任何奖品~"
                        }), !1;
                        "score" === this.sku.type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "redpack" === this.sku.type ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : "coupon" === this.sku.type ? e.navigateTo({
                            url: "/pages/couponDetail/index?uuid=" + this.sku.coupon.uuid
                        }) : "custom" === this.sku.type && e.navigateTo({
                            url: "/pages/orderDetail/index?uuid=" + this.info.order.uuid
                        });
                    },
                    getPrize: function() {
                        var t = this;
                        "custom" !== this.sku.type || this.sku.is_picked || e.showModal({
                            title: "领取实物奖品",
                            content: "是否填写邮寄地址发货？",
                            success: function(n) {
                                n.confirm && (e.showToast({
                                    title: "yes"
                                }), t.isAddressListening || (e.$once("selectAddress", function(n) {
                                    t.isAddressListening = !1, n.id && t.$http("/lottery-tickets/".concat(t.info.uuid, "/pick-prize"), "POST", {
                                        address_id: n.id
                                    }).then(function(n) {
                                        t.$emit("refresh"), e.showToast({
                                            title: "领取成功~",
                                            icon: "none"
                                        });
                                    });
                                }), t.isAddressListening = !0), e.navigateTo({
                                    url: "/pages/myAddress/index?select=true"
                                }));
                            }
                        });
                    }
                },
                filters: {}
            };
            t.default = c;
        }).call(this, n("543d").default);
    },
    e961: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return o;
        }), n.d(t, "a", function() {});
        var i = function() {
            this.$createElement;
            var e = (this._self._c, this.$tool.formatDate(this.info.created_at, "MM-dd hh:mm"));
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, o = [];
    },
    fef5: function(e, t, n) {
        "use strict";
        var i = n("9e92");
        n.n(i).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/lotteryTicket/components/TicketItem-create-component", {
    "pages/lotteryTicket/components/TicketItem-create-component": function(e, t, n) {
        n("543d").createComponent(n("a7dc"));
    }
}, [ [ "pages/lotteryTicket/components/TicketItem-create-component" ] ] ]);