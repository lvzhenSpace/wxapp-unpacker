(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/search/index" ], {
    "0ee8": function(t, e, i) {},
    "19cb": function(t, e, i) {
        "use strict";
        (function(t, e) {
            var n = i("4ea4");
            i("18ba"), n(i("66fd"));
            var a = n(i("b3e5"));
            t.__webpack_require_UNI_MP_PLUGIN__ = i, e(a.default);
        }).call(this, i("bc2e").default, i("543d").createPage);
    },
    "3c50": function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("ff4c"), a = i.n(n);
        for (var o in n) [ "default" ].indexOf(o) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(o);
        e.default = a.a;
    },
    6025: function(t, e, i) {
        "use strict";
        i.d(e, "b", function() {
            return a;
        }), i.d(e, "c", function() {
            return o;
        }), i.d(e, "a", function() {
            return n;
        });
        var n = {
            ProductList: function() {
                return i.e("components/ProductList/ProductList").then(i.bind(null, "0cf8"));
            },
            NoMore: function() {
                return i.e("components/NoMore/NoMore").then(i.bind(null, "0ffd"));
            },
            NoData: function() {
                return i.e("components/NoData/NoData").then(i.bind(null, "cafe"));
            }
        }, a = function() {
            this.$createElement;
            var t = (this._self._c, this.init && !this.list.length);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, o = [];
    },
    "949f": function(t, e, i) {
        "use strict";
        var n = i("0ee8");
        i.n(n).a;
    },
    b3e5: function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("6025"), a = i("3c50");
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(t) {
            i.d(e, t, function() {
                return a[t];
            });
        }(o);
        i("949f");
        var s = i("f0c5"), r = Object(s.a)(a.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        e.default = r.exports;
    },
    ff4c: function(t, e, i) {
        "use strict";
        (function(t) {
            var n = i("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var a = n(i("2eee")), o = n(i("c973")), s = n(i("448a")), r = {
                mixins: [ n(i("452d")).default ],
                components: {
                    ProductList: function() {
                        i.e("pages/search/components/list").then(function() {
                            return resolve(i("6f0a"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    SortBar: function() {
                        i.e("pages/search/components/sortBar").then(function() {
                            return resolve(i("2c55"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        sort: "",
                        key: "",
                        visible: !1,
                        category: [],
                        list: [],
                        page: 1,
                        per_page: 20,
                        total: 0,
                        activeCategory: "",
                        init: !1,
                        title: "全部商品",
                        action: "",
                        itemType: "all",
                        loading: !1
                    };
                },
                computed: {
                    isShowSortBar: function() {
                        return !("search" === this.action && !this.key);
                    },
                    guessList: function() {
                        return this.list.slice(0, 6);
                    }
                },
                watch: {
                    key: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    },
                    sort: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    },
                    itemType: function(t) {
                        this.page = 1, this.list = [], this.getProductList();
                    }
                },
                onLoad: function(e) {
                    var i = this;
                    this.activeCategory = e.category_id || 0, this.title = e.title, this.action = e.action, 
                    this.itemType = e.type || "all", this.activeCategory && !this.title ? this.$http("/categories/" + this.activeCategory).then(function(e) {
                        i.title = e.data.info.title, t.setNavigationBarTitle({
                            title: i.title
                        });
                    }) : t.setNavigationBarTitle({
                        title: this.title
                    }), this.getProductList(), this.$visitor.record("search");
                },
                methods: {
                    setSort: function(t) {
                        this.sort = t;
                    },
                    setType: function(t) {
                        this.itemType = t;
                    },
                    activeChange: function(t) {
                        var e = t.currentTarget.dataset.id;
                        this.activeCategory = e == this.activeCategory ? "" : e, this.visible = !1, this.getProductList();
                    },
                    handleChange: function(t) {
                        var e = t.currentTarget.dataset.index, i = this.category[e];
                        0 !== i.sub_categories.length ? this.category[e].openSubCategory = !this.category[e].openSubCategory : (this.activeCategory = i.id, 
                        this.visible = !1, this.getProductList());
                    },
                    handleClick: function(e) {
                        t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + e.uuid
                        });
                    },
                    getProductList: function() {
                        var e = this;
                        this.loading = !0, t.showLoading({
                            mask: !0
                        }), this.$http("/search", "POST", {
                            category_id: this.activeCategory,
                            page: this.page,
                            key: this.key,
                            sort: this.sort,
                            item_type: this.itemType
                        }).then(function(i) {
                            var n;
                            e.init = !0, t.hideLoading(), (n = e.list).push.apply(n, (0, s.default)(i.data.list)), 
                            e.loading = !1, e.page++;
                        });
                    },
                    showDrawer: function() {
                        this.visible = !0;
                    },
                    drawerClose: function() {
                        this.visible = !1;
                    }
                },
                onReachBottom: function() {
                    var t = this;
                    return (0, o.default)(a.default.mark(function e() {
                        return a.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                if (!t.loading) {
                                    e.next = 2;
                                    break;
                                }
                                return e.abrupt("return");

                              case 2:
                                t.getProductList();

                              case 3:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                }
            };
            e.default = r;
        }).call(this, i("543d").default);
    }
}, [ [ "19cb", "common/runtime", "common/vendor" ] ] ]);