(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/search/components/list" ], {
    2125: function(e, t, n) {
        "use strict";
        var a = n("6132");
        n.n(a).a;
    },
    6132: function(e, t, n) {},
    "6f0a": function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("e8a3"), i = n("fce5");
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(c);
        n("2125");
        var o = n("f0c5"), u = Object(o.a)(i.default, a.b, a.c, !1, null, "11b3eeb7", null, !1, a.a, void 0);
        t.default = u.exports;
    },
    "79da": function(e, t, n) {
        "use strict";
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = {
                name: "ProductList",
                props: {
                    list: {
                        type: Array
                    },
                    line: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    }
                },
                data: function() {
                    return {
                        theme: "default",
                        grid: "default-grid"
                    };
                },
                methods: {
                    toProductDetail: function(t) {
                        console.log("click ====>", t);
                        var n = "";
                        "product" == t.item_type ? n = "/pages/productDetail/index?uuid=" + t.uuid : "box" == t.item_type ? n = "/pages/boxDetail/index?uuid=" + t.uuid : "activity" == t.item_type && (n = "egg_lottery" === t.type ? "/pages/eggLottery/detail?uuid=".concat(t.uuid) : "/pages/".concat(t.type, "Activity/detail?uuid=").concat(t.uuid)), 
                        e.navigateTo({
                            url: n
                        });
                    }
                }
            };
            t.default = n;
        }).call(this, n("543d").default);
    },
    e8a3: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return c;
        }), n.d(t, "a", function() {
            return a;
        });
        var a = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, c = [];
    },
    fce5: function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("79da"), i = n.n(a);
        for (var c in a) [ "default" ].indexOf(c) < 0 && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(c);
        t.default = i.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/search/components/list-create-component", {
    "pages/search/components/list-create-component": function(e, t, n) {
        n("543d").createComponent(n("6f0a"));
    }
}, [ [ "pages/search/components/list-create-component" ] ] ]);