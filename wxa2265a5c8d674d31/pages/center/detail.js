(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/detail" ], {
    "0c4d": function(t, e, n) {
        "use strict";
        (function(t, e) {
            var r = n("4ea4");
            n("18ba"), r(n("66fd"));
            var i = r(n("5c27"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "0ecb": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return r;
        }), n.d(e, "c", function() {
            return i;
        }), n.d(e, "a", function() {});
        var r = function() {
            this.$createElement;
            var t = (this._self._c, 1 == this.userInfo.vip_status ? this.$tool.formatDate(this.userInfo.vip_end_at, "yyyy-MM-dd") : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, i = [];
    },
    "159b": function(t, e, n) {
        "use strict";
        n.r(e);
        var r = n("9a5f"), i = n.n(r);
        for (var o in r) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(o);
        e.default = i.a;
    },
    "5c27": function(t, e, n) {
        "use strict";
        n.r(e);
        var r = n("0ecb"), i = n("159b");
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(o);
        n("d157");
        var c = n("f0c5"), a = Object(c.a)(i.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        e.default = a.exports;
    },
    "88f7": function(t, e, n) {},
    "9a5f": function(t, e, n) {
        "use strict";
        (function(t) {
            var r = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = r(n("9523"));
            function o(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var r = Object.getOwnPropertySymbols(t);
                    e && (r = r.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, r);
                }
                return n;
            }
            function c(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? o(Object(n), !0).forEach(function(e) {
                        (0, i.default)(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : o(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            var a = {
                name: "center",
                components: {},
                data: function() {
                    return {
                        code: "",
                        init: !1,
                        isAnimated: !0
                    };
                },
                computed: c(c({}, (0, n("26cb").mapGetters)([ "deviceInfo", "userInfo", "token", "personalSettings" ])), {}, {
                    isLogin: function() {
                        return !!this.token;
                    },
                    userName: function() {
                        return this.token ? this.userInfo.name : "点击登录";
                    },
                    avatar: function() {
                        return this.token ? this.userInfo.headimg : "/static/toux.png";
                    },
                    config: function() {
                        return this.$store.getters.setting.user_center;
                    },
                    items: function() {
                        return this.config && this.config.items || [];
                    }
                }),
                onLoad: function() {
                    var e = this;
                    t.login({
                        success: function(t) {
                            e.code = t.code;
                        },
                        fail: function(t) {}
                    }), this.$visitor.record("center");
                },
                onShow: function() {
                    this.$store.dispatch("getUserInfo");
                },
                methods: {
                    toDetail: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    },
                    toDayTask: function() {
                        this.isAnimated = !1, t.navigateTo({
                            url: "/pages/dayTask/index"
                        });
                    },
                    toBuyVip: function() {
                        t.navigateTo({
                            url: "/pages/buyVip/index"
                        });
                    }
                }
            };
            e.default = a;
        }).call(this, n("543d").default);
    },
    d157: function(t, e, n) {
        "use strict";
        var r = n("88f7");
        n.n(r).a;
    }
}, [ [ "0c4d", "common/runtime", "common/vendor" ] ] ]);