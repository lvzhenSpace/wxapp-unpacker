(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/center/index" ], {
    "16fd": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("37bf"), i = n("82d5");
        for (var r in i) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("89af");
        var a = n("f0c5"), s = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = s.exports;
    },
    3296: function(e, t, n) {},
    "37bf": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            Banner: function() {
                return n.e("components/Banner/Banner").then(n.bind(null, "8003"));
            },
            FloatBtn: function() {
                return n.e("components/FloatBtn/FloatBtn").then(n.bind(null, "e690"));
            }
        }, i = function() {
            var e = this, t = (e.$createElement, e._self._c, e.config.is_show_vip && 1 == e.userInfo.vip_status ? e.$tool.formatDate(e.userInfo.vip_end_at, "yyyy-MM-dd") : null), n = e._f("0")(e.myLevel.my_level_score), o = e.level_title ? e._f("0")(e.myLevel.my_level_score) : null, i = e.config.banner && e.config.banner.length, r = e.config.banner && e.config.banner.length;
            e.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    f0: n,
                    f1: o,
                    g1: i,
                    g2: r
                }
            });
        }, r = [];
    },
    "82d5": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("d32b"), i = n.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    },
    "89af": function(e, t, n) {
        "use strict";
        var o = n("3296");
        n.n(o).a;
    },
    "9afa": function(e, t, n) {
        "use strict";
        (function(e, t) {
            var o = n("4ea4");
            n("18ba"), o(n("66fd"));
            var i = o(n("16fd"));
            e.__webpack_require_UNI_MP_PLUGIN__ = n, t(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    d32b: function(e, t, n) {
        "use strict";
        (function(e) {
            var o = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = o(n("9523"));
            function r(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function a(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? r(Object(n), !0).forEach(function(t) {
                        (0, i.default)(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            var s = {
                name: "center",
                components: {},
                data: function() {
                    return {
                        level_title: !0,
                        need_level_score: 0,
                        next_score: 0,
                        heightEle1: 0,
                        heightEle2: 0,
                        heightEle3: 0,
                        progress: 0,
                        myLevel: {},
                        version: "1.0.7",
                        init: !1,
                        orderStat: {},
                        count_score_redpack: 0,
                        myInfo: {},
                        vipShow: !1
                    };
                },
                computed: a(a({}, (0, n("26cb").mapGetters)([ "deviceInfo", "userInfo", "token", "personalSettings" ])), {}, {
                    isLogin: function() {
                        return !!this.token;
                    },
                    isscoreRedpack: function() {
                        return this.token && this.scoreRedpack ? this.count_score_redpack = this.scoreRedpack[0] : this.count_score_redpack = 0;
                    },
                    userName: function() {
                        return this.token ? this.userInfo.name : "点击登录";
                    },
                    avatar: function() {
                        return this.token ? this.userInfo.headimg : "/static/toux.png";
                    },
                    config: function() {
                        return this.$store.getters.setting.user_center;
                    },
                    items: function() {
                        return this.config && this.config.items || [];
                    },
                    isAnimated: function() {
                        return !0;
                    },
                    floatBtn: function() {
                        return {
                            is_show: this.config.is_show_float_btn,
                            link: this.config.float_btn_link,
                            image: this.config.float_btn_image,
                            is_animate: this.config.float_btn_is_animate
                        };
                    }
                }),
                onLoad: function() {
                    this.$visitor.record("center");
                },
                onShow: function() {
                    var t = this;
                    this.getMyInfo(), this.getLevelList(), this.isLogin && (this.$store.dispatch("getUserInfo").then(function(n) {
                        t.$http("/order-stat").then(function(e) {
                            t.orderStat = e.data.info;
                        }), t.userInfo && "微信用户" == t.userInfo.name && e.showModal({
                            title: "检测到您使用的是微信默认昵称!请先修改",
                            confirmText: "去修改",
                            showCancel: !1,
                            success: function(e) {
                                e.confirm && t.toProfilePage(), console.log("用户点击确定");
                            }
                        });
                    }), this.$store.dispatch("getCountScoreRedpack").then(function(e) {
                        t.count_score_redpack = e.data.info[0];
                    }));
                },
                methods: {
                    getLevelList: function() {
                        var e = this;
                        this.$http("/task/level_list").then(function(t) {
                            e.myLevel = t.data, e.need_level_score = t.data.next_task_level_info.need_level_score - t.data.my_level_score, 
                            e.progress = t.data.my_level_score / t.data.next_task_level_info.need_level_score * 100, 
                            e.next_score = t.data.next_task_level_info.need_level_score, 50 == t.data.my_task_level && 0 == t.data.next_task_level_info.length && (e.next_score = t.data.my_level_score, 
                            e.progress = 100, e.level_title = !1);
                        });
                    },
                    getMyInfo: function() {
                        var e = this;
                        this.$http("/task/level_list?no_list=1").then(function(t) {
                            e.myInfo = t.data.my_task_level_info, e.vipShow = !0;
                        }).catch(function(e) {
                            console.log(e);
                        });
                    },
                    toProfilePage: function() {
                        e.navigateTo({
                            url: "/pages/myProfile/index"
                        });
                    },
                    toDetail: function(t) {
                        e.navigateTo({
                            url: t
                        });
                    },
                    toDayTask: function() {
                        this.isAnimated = !1, e.navigateTo({
                            url: "/pages/dayTask/index"
                        });
                    },
                    toBuyVip: function() {
                        e.navigateTo({
                            url: "/pages/buyVip/index"
                        });
                    },
                    memberDetail: function() {
                        e.navigateTo({
                            url: "/member/pages/memberGrade/memberGrade"
                        });
                    }
                }
            };
            t.default = s;
        }).call(this, n("543d").default);
    }
}, [ [ "9afa", "common/runtime", "common/vendor" ] ] ]);