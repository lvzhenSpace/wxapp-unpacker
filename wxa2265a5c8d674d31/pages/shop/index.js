(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/shop/index" ], {
    "0439": function(e, n, t) {
        "use strict";
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0, n.default = {
            components: {},
            data: function() {
                return {
                    scrollTop: 0,
                    refreshCounter: 0,
                    getNextPageCounter: 0
                };
            },
            computed: {
                page: function() {
                    return this.$store.getters.setting.shop_home;
                }
            },
            watch: {},
            onLoad: function() {
                this.$visitor.record("box_index");
            },
            onPullDownRefresh: function() {
                var e = this;
                this.$showPullRefresh().then(function(n) {
                    e.refreshCounter++;
                });
            },
            onShow: function() {},
            methods: {},
            onReachBottom: function() {
                this.getNextPageCounter++;
            },
            onPageScroll: function(e) {
                this.scrollTop = e.scrollTop;
            }
        };
    },
    "202a": function(e, n, t) {
        "use strict";
        t.r(n);
        var o = t("0439"), r = t.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(c);
        n.default = r.a;
    },
    c0c8: function(e, n, t) {
        "use strict";
        t.d(n, "b", function() {
            return r;
        }), t.d(n, "c", function() {
            return c;
        }), t.d(n, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/PageRender/PageRender") ]).then(t.bind(null, "b4aa"));
            }
        }, r = function() {
            this.$createElement, this._self._c;
        }, c = [];
    },
    cafb: function(e, n, t) {
        "use strict";
        t.r(n);
        var o = t("c0c8"), r = t("202a");
        for (var c in r) [ "default" ].indexOf(c) < 0 && function(e) {
            t.d(n, e, function() {
                return r[e];
            });
        }(c);
        var u = t("f0c5"), a = Object(u.a)(r.default, o.b, o.c, !1, null, "428ccfc2", null, !1, o.a, void 0);
        n.default = a.exports;
    },
    e55c: function(e, n, t) {
        "use strict";
        (function(e, n) {
            var o = t("4ea4");
            t("18ba"), o(t("66fd"));
            var r = o(t("cafb"));
            e.__webpack_require_UNI_MP_PLUGIN__ = t, n(r.default);
        }).call(this, t("bc2e").default, t("543d").createPage);
    }
}, [ [ "e55c", "common/runtime", "common/vendor" ] ] ]);