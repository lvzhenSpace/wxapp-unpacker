(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myChip/index" ], {
    "173d": function(t, n, e) {
        "use strict";
        e.r(n);
        var a = e("5c2c"), u = e.n(a);
        for (var c in a) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return a[t];
            });
        }(c);
        n.default = u.a;
    },
    5082: function(t, n, e) {
        "use strict";
        var a = e("5f84");
        e.n(a).a;
    },
    "5c2c": function(t, n, e) {
        "use strict";
        (function(t) {
            var a = e("4ea4");
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var u = a(e("2eee")), c = a(e("c973")), r = {
                components: {},
                data: function() {
                    return {
                        list: []
                    };
                },
                computed: {
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    }
                },
                onLoad: function() {
                    var n = this;
                    return (0, c.default)(u.default.mark(function e() {
                        return u.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                t.showLoading({
                                    title: "加载中"
                                }), n.$http("/chip-assets").then(function(e) {
                                    n.list = e.data.list, t.hideLoading();
                                });

                              case 2:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                methods: {}
            };
            n.default = r;
        }).call(this, e("543d").default);
    },
    "5f84": function(t, n, e) {},
    af5c: function(t, n, e) {
        "use strict";
        e.r(n);
        var a = e("f909"), u = e("173d");
        for (var c in u) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(c);
        e("5082");
        var r = e("f0c5"), i = Object(r.a)(u.default, a.b, a.c, !1, null, null, null, !1, a.a, void 0);
        n.default = i.exports;
    },
    dfc6: function(t, n, e) {
        "use strict";
        (function(t, n) {
            var a = e("4ea4");
            e("18ba"), a(e("66fd"));
            var u = a(e("af5c"));
            t.__webpack_require_UNI_MP_PLUGIN__ = e, n(u.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    f909: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return u;
        }), e.d(n, "c", function() {
            return c;
        }), e.d(n, "a", function() {
            return a;
        });
        var a = {
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "cafe"));
            }
        }, u = function() {
            this.$createElement;
            var t = (this._self._c, this.list.length);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, c = [];
    }
}, [ [ "dfc6", "common/runtime", "common/vendor" ] ] ]);