(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/activityTicket/components/UserItem" ], {
    "2e20": function(e, t, n) {},
    "495f": function(e, t, n) {
        "use strict";
        (function(e) {
            var a = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = a(n("2eee")), i = a(n("c973")), r = {
                components: {},
                data: function() {
                    return {};
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: {},
                onLoad: function() {
                    return (0, i.default)(o.default.mark(function e() {
                        return o.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                methods: {
                    handleClick: function() {
                        "score" === this.info.node_reward.type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "redpack" === this.info.node_reward.type ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : "exclude_card" === this.info.node_reward.type || "show_card" === this.info.node_reward.type ? e.navigateTo({
                            url: "/pages/myCard/index"
                        }) : "coupon" === this.info.node_reward.type && e.navigateTo({
                            url: "/pages/myCoupons/index"
                        });
                    }
                },
                filters: {}
            };
            t.default = r;
        }).call(this, n("543d").default);
    },
    "6de0": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {
            return a;
        });
        var a = {
            SingleRewardDisplay: function() {
                return n.e("components/SingleRewardDisplay/SingleRewardDisplay").then(n.bind(null, "979f"));
            }
        }, o = function() {
            this.$createElement;
            var e = (this._self._c, this.info.user ? this.$tool.formatDate(this.info.created_at, "MM/dd hh:mm") : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, i = [];
    },
    "8c1b": function(e, t, n) {
        "use strict";
        var a = n("2e20");
        n.n(a).a;
    },
    a7f7: function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("6de0"), o = n("de6b");
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        n("8c1b");
        var r = n("f0c5"), c = Object(r.a)(o.default, a.b, a.c, !1, null, "261310e0", null, !1, a.a, void 0);
        t.default = c.exports;
    },
    de6b: function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("495f"), o = n.n(a);
        for (var i in a) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(i);
        t.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/activityTicket/components/UserItem-create-component", {
    "pages/activityTicket/components/UserItem-create-component": function(e, t, n) {
        n("543d").createComponent(n("a7f7"));
    }
}, [ [ "pages/activityTicket/components/UserItem-create-component" ] ] ]);