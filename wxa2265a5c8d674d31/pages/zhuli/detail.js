(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/zhuli/detail" ], {
    "084b": function(t, e, n) {
        "use strict";
        var i = n("17b8");
        n.n(i).a;
    },
    "17b8": function(t, e, n) {},
    "1b99": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("946c"), o = n("9938");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(u);
        n("084b");
        var r = n("f0c5"), c = Object(r.a)(o.default, i.b, i.c, !1, null, "0eb2e4ce", null, !1, i.a, void 0);
        e.default = c.exports;
    },
    "50c1": function(t, e, n) {
        "use strict";
        (function(t, e) {
            var i = n("4ea4");
            n("18ba"), i(n("66fd"));
            var o = i(n("1b99"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(o.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    8585: function(t, e, n) {
        "use strict";
        (function(t) {
            var i = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = i(n("9523"));
            function u(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(t);
                    e && (i = i.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, i);
                }
                return n;
            }
            function r(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? u(Object(n), !0).forEach(function(e) {
                        (0, o.default)(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : u(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            var c = {
                components: {
                    MyRecordList: function() {
                        n.e("pages/zhuli/components/MyRecordList").then(function() {
                            return resolve(n("590a"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        isInit: !1,
                        uuid: "",
                        isNotFound: !1,
                        info: {},
                        launchUuid: "",
                        launchRecord: {},
                        isSharePopup: !1,
                        isShowUserGroupCheck: !1,
                        userGroupId: "",
                        userGroupTitle: "",
                        isShowMyRecordList: !1
                    };
                },
                computed: r(r({}, (0, n("26cb").mapGetters)([ "userInfo" ])), {}, {
                    share: function() {
                        var t = this.info.title;
                        return this.launchRecord.user && (t = this.launchRecord.user.name + "邀请您来助力~"), 
                        {
                            path: "/pages/zhuli/detail?uuid=" + this.info.uuid + "&invite_node=zhuli-" + this.info.uuid + "&launchRecordUuid=" + this.launchUuid,
                            title: t,
                            thumb: this.info.thumb
                        };
                    },
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/zhuli/bg.png";
                    },
                    sku: function() {
                        return this.info.skus && this.info.skus[0] || {};
                    },
                    userList: function() {
                        for (var t = [], e = {
                            headimg: "https://cdn2.hquesoft.com/box/default-headimg"
                        }, n = 0; n < this.info.need_user_total; n++) {
                            var i = this.launchRecord.support_records && this.launchRecord.support_records[n] && this.launchRecord.support_records[n].user || e;
                            t.push(i);
                        }
                        return t;
                    },
                    posterInfo: function() {
                        var t = this.getShareConfig(!1), e = "快来帮我助力赢礼品吧~";
                        return this.launchRecord.user && (e = this.launchRecord.user.name + "邀请您来助力~"), 
                        {
                            title: e,
                            path: this.getShareConfig().path,
                            thumb: this.info.thumb,
                            app_url: t.app_url
                        };
                    }
                }),
                watch: {},
                onLoad: function(e) {
                    var n = this;
                    this.uuid = e.uuid, this.launchUuid = e.launchRecordUuid, t.showLoading({
                        title: "加载中"
                    }), this.initData().then(function(e) {
                        n.isInit = !0, t.hideLoading();
                    }).catch(function(t) {
                        404 == t.statusCode && (n.isNotFound = !0);
                    });
                },
                onShow: function() {
                    var t = this;
                    this.isInit && this.initData().then(function(e) {
                        t.isInit = !0;
                    }).catch(function(e) {
                        404 == e.statusCode && (t.isNotFound = !0);
                    });
                },
                methods: {
                    selectLaunchRecord: function(t) {
                        var e = this;
                        this.launchUuid = t.uuid, this.isShowMyRecordList = !1, this.$http("/zhuli/launch-records/" + this.launchUuid).then(function(t) {
                            e.launchRecord = t.data.info;
                        });
                    },
                    checkMyRecord: function() {
                        this.isShowMyRecordList = !0;
                    },
                    checkLaunchUserGroup: function() {
                        this.userGroupTitle = "发起助力", this.userGroupId = this.info.launch_user_group_id, 
                        this.isShowUserGroupCheck = !0;
                    },
                    checkSupportUserGroup: function() {
                        this.userGroupTitle = "给他人助力", this.userGroupId = this.info.support_user_group_id, 
                        this.isShowUserGroupCheck = !0;
                    },
                    sharePage: function() {
                        this.isSharePopup = !0;
                    },
                    initData: function() {
                        var e = this;
                        return this.launchUuid && this.$http("/zhuli/launch-records/" + this.launchUuid).then(function(t) {
                            e.launchRecord = t.data.info;
                        }), this.$http("/zhuli/activities/".concat(this.uuid)).then(function(n) {
                            e.info = n.data.info, e.prizeList = e.info.skus.map(function(t) {
                                return {
                                    prizeId: t.id,
                                    prizeName: t.title,
                                    prizeStock: t.stock,
                                    prizeWeight: t.id,
                                    prizeImage: t.thumb + "?x-oss-process=image/resize,w_200"
                                };
                            }), t.setNavigationBarTitle({
                                title: n.data.info.title
                            });
                        });
                    },
                    launchActivity: function() {
                        var e = this, n = "发起助力";
                        this.info.score_price && (n = "支付" + this.info.score_price + this.scoreAlias + "发起助力"), 
                        t.showModal({
                            title: n,
                            content: "发起后需要在" + this.info.time_limit + "小时内集齐" + this.info.need_user_total + "助力才算完成哦~",
                            confirmText: "立即发起",
                            success: function(n) {
                                n.confirm && (t.showLoading({
                                    title: "加载中~"
                                }), e.$http("/zhuli/launch-records", "POST", {
                                    activity_id: e.info.id
                                }).then(function(n) {
                                    t.hideLoading(), e.launchUuid = n.data.uuid, e.initData();
                                }));
                            }
                        });
                    },
                    createSupport: function() {
                        var e = this;
                        t.showLoading({
                            title: "助力中~"
                        }), this.$http("/zhuli/support-records", "POST", {
                            launch_record_id: this.launchRecord.id
                        }).then(function(n) {
                            t.hideLoading(), e.initData();
                        });
                    }
                }
            };
            e.default = c;
        }).call(this, n("543d").default);
    },
    "946c": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            CountDown: function() {
                return n.e("components/CountDown/CountDown").then(n.bind(null, "2fed"));
            },
            PageRender: function() {
                return Promise.all([ n.e("common/vendor"), n.e("components/PageRender/PageRender") ]).then(n.bind(null, "b4aa"));
            },
            UserGroupCheck: function() {
                return n.e("components/UserGroupCheck/UserGroupCheck").then(n.bind(null, "add4"));
            },
            SharePopup: function() {
                return Promise.all([ n.e("common/vendor"), n.e("components/SharePopup/SharePopup") ]).then(n.bind(null, "b677"));
            }
        }, o = function() {
            var t = this, e = (t.$createElement, t._self._c, t.launchRecord.id ? t.$tool.formatDate(t.launchRecord.launched_at, "MM-dd hh:mm") : null);
            t._isMounted || (t.e0 = function(e) {
                t.isShowMyRecordList = !1;
            }, t.e1 = function(e) {
                t.isShowUserGroupCheck = !1;
            }, t.e2 = function(e) {
                t.isSharePopup = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, u = [];
    },
    9938: function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("8585"), o = n.n(i);
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(u);
        e.default = o.a;
    }
}, [ [ "50c1", "common/runtime", "common/vendor" ] ] ]);