(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/ip/index" ], {
    "017d": function(t, n, e) {
        "use strict";
        e.r(n);
        var a = e("5a06"), i = e.n(a);
        for (var c in a) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return a[t];
            });
        }(c);
        n.default = i.a;
    },
    "3d95": function(t, n, e) {
        "use strict";
        e.r(n);
        var a = e("8e13"), i = e("017d");
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(c);
        e("6ccd"), e("7a9e");
        var r = e("f0c5"), u = Object(r.a)(i.default, a.b, a.c, !1, null, "8cf08886", null, !1, a.a, void 0);
        n.default = u.exports;
    },
    "5a06": function(t, n, e) {
        "use strict";
        (function(t) {
            var a = e("4ea4");
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var i = a(e("2eee")), c = a(e("c973")), r = {
                components: {},
                data: function() {
                    return {
                        ipList: [],
                        setting: {}
                    };
                },
                computed: {
                    banner: function() {
                        return this.setting.banner || [];
                    }
                },
                onLoad: function(t) {
                    var n = this;
                    return (0, c.default)(i.default.mark(function t() {
                        return i.default.wrap(function(t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                n.$http("/ip/categories").then(function(t) {
                                    n.ipList = t.data.list;
                                }), n.$http("/setting/ip_list").then(function(t) {
                                    n.setting = t.data.setting;
                                });

                              case 2:
                              case "end":
                                return t.stop();
                            }
                        }, t);
                    }))();
                },
                methods: {
                    toList: function(n) {
                        t.navigateTo({
                            url: "/pages/search/index?type=all&category_id=".concat(n.id, "&title=").concat(n.title)
                        });
                    }
                }
            };
            n.default = r;
        }).call(this, e("543d").default);
    },
    "6ccd": function(t, n, e) {
        "use strict";
        var a = e("a474");
        e.n(a).a;
    },
    "7a9e": function(t, n, e) {
        "use strict";
        var a = e("d5e3");
        e.n(a).a;
    },
    "8e13": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return c;
        }), e.d(n, "a", function() {
            return a;
        });
        var a = {
            Banner: function() {
                return e.e("components/Banner/Banner").then(e.bind(null, "8003"));
            },
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "cafe"));
            }
        }, i = function() {
            this.$createElement;
            var t = (this._self._c, this.banner.length), n = this.ipList.length;
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    g1: n
                }
            });
        }, c = [];
    },
    a474: function(t, n, e) {},
    ae81: function(t, n, e) {
        "use strict";
        (function(t, n) {
            var a = e("4ea4");
            e("18ba"), a(e("66fd"));
            var i = a(e("3d95"));
            t.__webpack_require_UNI_MP_PLUGIN__ = e, n(i.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    d5e3: function(t, n, e) {}
}, [ [ "ae81", "common/runtime", "common/vendor" ] ] ]);