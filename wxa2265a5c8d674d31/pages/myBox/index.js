(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/index" ], {
    "457e": function(t, e, s) {
        "use strict";
        (function(t) {
            var i = s("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = i(s("448a")), o = {
                components: {
                    PackageSku: function() {
                        Promise.all([ s.e("common/vendor"), s.e("pages/myBox/components/PackageSku") ]).then(function() {
                            return resolve(s("de82"));
                        }.bind(null, s)).catch(s.oe);
                    },
                    PayCard: function() {
                        Promise.all([ s.e("common/vendor"), s.e("pages/myBox/components/PayCard") ]).then(function() {
                            return resolve(s("00af"));
                        }.bind(null, s)).catch(s.oe);
                    }
                },
                data: function() {
                    return {
                        order: {},
                        showSelect: !0,
                        reasons: [],
                        visible: !1,
                        dataList: [],
                        current: 0,
                        currentUuid: "6056e83ef3251",
                        currentItem: {},
                        types: [ "pending", "resale_pending", "picked", "all" ],
                        typeTextList: [ "待处理", "提货中", "已提货", "全部" ],
                        boxList: [ "赏品", "宝箱" ],
                        boxIndex: 0,
                        boxShow: !1,
                        allBox: [],
                        arrBox: [ "暂无待处理的商品", "暂无提货中的商品", "暂无已提货的商品", "暂无商品" ],
                        isSelectMode: !1,
                        selectType: "deliver",
                        selectedIds: [],
                        isShowPay: !1,
                        isShowReturnSalePopup: !1,
                        isShowResalePopup: !1,
                        statusTotal: {},
                        isReturnSaleSuccess: !1,
                        boxTitle: !0,
                        popup: !1,
                        boxskus: [],
                        myInfo: {}
                    };
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order || {};
                    },
                    marketConfig: function() {
                        return this.$store.getters.setting.market || {};
                    },
                    exchangeConfig: function() {
                        return this.$store.getters.setting.exchange || {};
                    }
                },
                onLoad: function(e) {
                    t.showLoading({
                        title: "加载中"
                    }), this.getBoxList(), this.current = this.types.indexOf(e.status || "pending");
                },
                onShow: function() {
                    var t = this;
                    this.$http("/task/level_list?no_list=1").then(function(e) {
                        t.myInfo = e.data.my_task_level_info, e.data.my_task_level_info.level > 4 ? (t.types = [ "pending", "resale_pending", "picked", "all" ], 
                        t.typeTextList = [ "待处理", "提货中", "已提货", "全部" ]) : (t.types = [ "pending", "picked", "all" ], 
                        t.typeTextList = [ "待处理", "已提货", "全部" ]), t.initData();
                    }).catch(function(t) {
                        console.log(t);
                    });
                },
                onUnload: function() {},
                methods: {
                    handlePayCardClick: function() {
                        this.isShowPay = !1;
                    },
                    handleSaleCancel: function() {
                        this.isShowReturnSalePopup = !1;
                    },
                    getMyInfo: function() {
                        var t = this;
                        this.$http("/task/level_list?no_list=1").then(function(e) {
                            t.myInfo = e.data.my_task_level_info;
                        }).catch(function(t) {
                            console.log(t);
                        });
                    },
                    showaward: function(t) {
                        this.popup = !0, this.boxskus = this.allBox[t].skus, console.log(this.allBox), console.log(this.boxskus);
                    },
                    close: function() {
                        this.popup = !1;
                    },
                    getBoxList: function() {
                        var t = this;
                        this.$http("/shangdai/records?status=0").then(function(e) {
                            console.log(e), t.allBox = e.data.list;
                        });
                    },
                    boxchange: function(t) {
                        this.refresh(), this.boxIndex = t, 1 == t ? (this.boxShow = !0, this.showSelect = !1) : (this.boxShow = !1, 
                        this.showSelect = !0);
                    },
                    openBox: function(e) {
                        var s = this;
                        console.log(e), this.$http("/shangdai/open/".concat(e), "POST").then(function(e) {
                            t.$showMsg("恭喜你获得" + e.data.shangdai_sku.title), s.getBoxList();
                        }).catch(function(e) {
                            t.showToast({
                                title: "开启失败",
                                icon: "none",
                                duration: 2e3
                            });
                        });
                    },
                    hideResalePopup: function() {
                        this.isShowResalePopup = !1;
                    },
                    toPage: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    },
                    successDeliver: function(e) {
                        this.cancelSelect(), "custom_gateway" === e.deliver_type ? (this.refresh(), this.isShowPay = !1, 
                        t.showToast({
                            title: "提交成功",
                            icon: "none"
                        })) : (this.current = 2, this.refresh(), this.isShowPay = !1, t.showModal({
                            title: "发货成功",
                            content: "已成功提交发货请求，请注意查收快递哦~"
                        }));
                    },
                    selectAll: function() {
                        if (!this.dataList[0]) return !1;
                        this.selectedIds = [];
                        for (var t = 0; t < this.dataList[0].list.length; t++) {
                            var e = this.dataList[0].list[t];
                            "deliver" === this.selectType ? "virtual_asset" === e.sku_type_text || e.is_presell || this.selectedIds.push(e.id) : "return_sale" === this.selectType ? !this.orderConfig.is_ban_return_sale && "virtual_asset" != e.sku_type_text && e.is_return_saleable && this.selectedIds.push(e.id) : "resale" === this.selectType && "virtual_asset" != e.sku_type_text && this.selectedIds.push(e.id);
                        }
                    },
                    cancelSelect: function() {
                        this.isSelectMode = !1, this.selectedIds = [];
                    },
                    selectOrSubmit: function() {
                        if (this.isSelectMode) {
                            if (0 == this.selectedIds.length) return t.showModal({
                                title: "请选择物品",
                                content: "选择一件或多件物品后才能提交发货哦~"
                            }), !1;
                            this.isShowPay = !0;
                        } else this.isSelectMode = !0;
                    },
                    batchReturnSale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能分解哦~"
                        }), !1;
                        t.showModal({
                            title: "确认分解",
                            content: "确认要批量分解吗?",
                            success: function(s) {
                                console.log(e.selectedIds), s.confirm && (e.isReturnSaleSuccess = !0, t.showLoading({
                                    title: "分解中..."
                                }), e.$http("/asset/return-sale/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(s) {
                                    t.showToast({
                                        title: "分解成功"
                                    }), e.refresh(), e.isSelectMode = !1, e.isReturnSaleSuccess = !1;
                                }));
                            }
                        });
                    },
                    batchResale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能挂售哦~"
                        }), !1;
                        t.showModal({
                            title: "确认挂售到交易市场",
                            content: "统一以建议挂售价挂售到交易市场",
                            success: function(s) {
                                s.confirm && (t.showLoading({
                                    title: "挂售中..."
                                }), e.$http("/market/resale/batch/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(s) {
                                    t.hideLoading(), e.refresh(), e.isSelectMode = !1, t.showToast({
                                        title: "转售成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.navigateTo({
                                            url: "/pages/resale/index"
                                        });
                                    }, 1500);
                                }));
                            }
                        });
                    },
                    batchExchange: function() {
                        return 0 == this.selectedIds.length ? (t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能置换哦~"
                        }), !1) : this.selectedIds.length >= 10 ? (t.showModal({
                            title: "超出限制了",
                            content: "单次置换限制在10件以内~"
                        }), !1) : (this.setStorage("exchange_package_sku_ids", this.selectedIds), void t.navigateTo({
                            url: "/pages/exchange/productList"
                        }));
                    },
                    enterSelectMode: function(t) {
                        this.selectedIds = [], this.isSelectMode = !0, this.selectType = t;
                    },
                    checkItem: function(t) {
                        var e = this.selectedIds.indexOf(t.id);
                        e > -1 ? this.selectedIds.splice(e, 1) : this.selectedIds.push(t.id);
                    },
                    refresh: function() {
                        this.cleanData(), this.getOrderList(), this.getBoxList(), this.initBoxTotalData();
                    },
                    scrolltolower: function() {
                        this.dataList[this.current].page++, this.getOrderList(), this.getBoxList();
                    },
                    initData: function() {
                        var t = [];
                        this.types.forEach(function(e) {
                            t.push({
                                list: [],
                                type: e,
                                page: 1,
                                per_page: 50,
                                total: 0,
                                init: !1,
                                loading: !1
                            });
                        }), this.dataList = t, this.refresh(), console.log(t);
                    },
                    cleanData: function() {
                        this.dataList.forEach(function(t) {
                            t.page = 1, t.init = !1;
                        });
                    },
                    initBoxTotalData: function() {
                        var e = this;
                        this.$http("/status-total/package-sku").then(function(s) {
                            e.statusTotal = s.data.info, t.hideLoading(), console.log(e.statusTotal);
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    actions: function(e) {
                        switch (e.action) {
                          case "返售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowReturnSalePopup = !0;
                            break;

                          case "核销码":
                            t.navigateTo({
                                url: "/pages/orderCode/index?uuid=" + e.order.uuid
                            });
                            break;

                          case "查看订单":
                            t.navigateTo({
                                url: "/pages/orderDetail/index?uuid=" + e.order.pick_order.uuid
                            });
                            break;

                          case "转售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowResalePopup = !0;
                        }
                    },
                    currentChange: function(t) {
                        var e = t.currentTarget.dataset.current;
                        0 == e || 3 == e ? (this.boxTitle = !0, this.boxIndex = 0, this.boxShow = !1, this.showSelect = !0) : (this.boxTitle = !1, 
                        this.boxShow = !1), e !== this.current && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    currentChange2: function(t) {
                        var e = t.detail.current;
                        0 == e || 3 == e ? (this.boxTitle = !0, this.boxIndex = 0) : (this.boxIndex = 0, 
                        this.boxTitle = !1, this.boxShow = !1), e !== this.current && (this.current = e, 
                        this.dataList[this.current].init || this.getOrderList());
                    },
                    getOrderList: function() {
                        var t = this.dataList[this.current];
                        t.loading || (t.loading = !0, this.$http("/asset/package-skus", "GET", {
                            status: t.type,
                            page: t.page,
                            per_page: t.per_page
                        }).then(function(e) {
                            var s;
                            t.loading = !1, 1 === e.data.current_page ? t.list = e.data.list : (s = t.list).push.apply(s, (0, 
                            n.default)(e.data.list)), t.total = e.data.item_total, t.init = !0;
                        }));
                    }
                }
            };
            e.default = o;
        }).call(this, s("543d").default);
    },
    "57c6": function(t, e, s) {
        "use strict";
        var i = s("fcf0");
        s.n(i).a;
    },
    "70b5": function(t, e, s) {
        "use strict";
        s.d(e, "b", function() {
            return n;
        }), s.d(e, "c", function() {
            return o;
        }), s.d(e, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return s.e("components/NoData/NoData").then(s.bind(null, "cafe"));
            },
            ReturnSalePopup: function() {
                return s.e("components/ReturnSalePopup/ReturnSalePopup").then(s.bind(null, "f4ef"));
            },
            ResalePopup: function() {
                return s.e("components/ResalePopup/ResalePopup").then(s.bind(null, "cd4e"));
            }
        }, n = function() {
            var t = this, e = (t.$createElement, t._self._c, t.boxShow ? t.allBox.length : null), s = t.__map(t.types, function(e, s) {
                return {
                    $orig: t.__get_orig(e),
                    l0: t.boxShow ? null : t.__map(t.dataList[s].list, function(e, s) {
                        return {
                            $orig: t.__get_orig(e),
                            g0: t.selectedIds.indexOf(e.id)
                        };
                    }),
                    g1: t.boxShow ? null : t.dataList[s].list.length,
                    g2: t.boxShow ? null : t.dataList[s].init && !t.dataList[s].list.length
                };
            }), i = 0 == t.current && !t.orderConfig.is_hide_deliver_btn && t.showSelect && t.isSelectMode && "deliver" === t.selectType ? t.selectedIds.length : null, n = 0 == t.current && !t.orderConfig.is_hide_deliver_btn && t.showSelect && t.isSelectMode && "return_sale" === t.selectType ? t.selectedIds.length : null, o = 0 == t.current && !t.orderConfig.is_hide_deliver_btn && t.showSelect && t.isSelectMode && "resale" === t.selectType ? t.selectedIds.length : null, a = 0 == t.current && !t.orderConfig.is_hide_deliver_btn && t.showSelect && t.isSelectMode && "exchange" === t.selectType ? t.selectedIds.length : null;
            t._isMounted || (t.e0 = function(e) {
                t.isReturnSaleSuccess = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    g3: e,
                    l1: s,
                    g4: i,
                    g5: n,
                    g6: o,
                    g7: a
                }
            });
        }, o = [];
    },
    8448: function(t, e, s) {
        "use strict";
        s.r(e);
        var i = s("457e"), n = s.n(i);
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(t) {
            s.d(e, t, function() {
                return i[t];
            });
        }(o);
        e.default = n.a;
    },
    a30c: function(t, e, s) {
        "use strict";
        (function(t, e) {
            var i = s("4ea4");
            s("18ba"), i(s("66fd"));
            var n = i(s("e133"));
            t.__webpack_require_UNI_MP_PLUGIN__ = s, e(n.default);
        }).call(this, s("bc2e").default, s("543d").createPage);
    },
    e133: function(t, e, s) {
        "use strict";
        s.r(e);
        var i = s("70b5"), n = s("8448");
        for (var o in n) [ "default" ].indexOf(o) < 0 && function(t) {
            s.d(e, t, function() {
                return n[t];
            });
        }(o);
        s("57c6");
        var a = s("f0c5"), l = Object(a.a)(n.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = l.exports;
    },
    fcf0: function(t, e, s) {}
}, [ [ "a30c", "common/runtime", "common/vendor" ] ] ]);