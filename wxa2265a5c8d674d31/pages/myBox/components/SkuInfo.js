(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/components/SkuInfo" ], {
    "0140": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement;
            var t = (this._self._c, this.info.attrs && this.info.attrs.length), e = t ? this._f("productAttrsToString")(this.info.attrs) : null;
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    f0: e
                }
            });
        }, u = [];
    },
    "544d": function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("d62e"), u = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        e.default = u.a;
    },
    "79fc": function(t, e, n) {
        "use strict";
        var o = n("edf0");
        n.n(o).a;
    },
    d62e: function(t, e, n) {
        "use strict";
        (function(t) {
            var o = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var u = {
                mixins: [ o(n("452d")).default ],
                props: {
                    info: {
                        type: Object
                    },
                    myInfo: {
                        type: Object
                    },
                    disableClick: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                filters: {},
                computed: {
                    boxDepotConfig: function() {
                        return this.$store.getters.setting.box_depot || {};
                    }
                },
                methods: {
                    toProductDetail: function(e) {
                        if (this.disableClick) return !1;
                        var n = this.info;
                        "product" === n.product_type ? t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + n.product_uuid
                        }) : "box" === n.product_type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + n.product_uuid
                        });
                    }
                }
            };
            e.default = u;
        }).call(this, n("543d").default);
    },
    edf0: function(t, e, n) {},
    fccf: function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("0140"), u = n("544d");
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return u[t];
            });
        }(i);
        n("79fc");
        var c = n("f0c5"), a = Object(c.a)(u.default, o.b, o.c, !1, null, "e9ac1948", null, !1, o.a, void 0);
        e.default = a.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/myBox/components/SkuInfo-create-component", {
    "pages/myBox/components/SkuInfo-create-component": function(t, e, n) {
        n("543d").createComponent(n("fccf"));
    }
}, [ [ "pages/myBox/components/SkuInfo-create-component" ] ] ]);