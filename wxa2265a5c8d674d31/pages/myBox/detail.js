(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/detail" ], {
    "11fa": function(n, t, e) {
        "use strict";
        e.d(t, "b", function() {
            return o;
        }), e.d(t, "c", function() {
            return u;
        }), e.d(t, "a", function() {
            return i;
        });
        var i = {
            SkuItem: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/SkuItem/SkuItem") ]).then(e.bind(null, "3c3e"));
            },
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, o = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    1553: function(n, t, e) {
        "use strict";
        (function(n, t) {
            var i = e("4ea4");
            e("18ba"), i(e("66fd"));
            var o = i(e("ad4d"));
            n.__webpack_require_UNI_MP_PLUGIN__ = e, t(o.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    7334: function(n, t, e) {
        "use strict";
        e.r(t);
        var i = e("9ea7"), o = e.n(i);
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(u);
        t.default = o.a;
    },
    "897b": function(n, t, e) {},
    "9ea7": function(n, t, e) {
        "use strict";
        (function(n) {
            var i = e("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = {
                mixins: [ i(e("452d")).default ],
                components: {
                    IActionSheet: function() {
                        e.e("components/ActionSheet/index").then(function() {
                            return resolve(e("fbfb"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        deliverRecord: null,
                        info: {},
                        uuid: ""
                    };
                },
                filters: {
                    hidePhoneDetail: function(n) {
                        return n ? n.substring(0, 3) + "****" + n.substring(7, 11) : "";
                    }
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    }
                },
                onLoad: function(n) {
                    this.uuid = n.uuid;
                },
                onShow: function() {
                    this.getOrderInfo();
                },
                methods: {
                    checkChip: function() {
                        n.navigateTo({
                            url: "/pages/myChip/index"
                        });
                    },
                    checkRedpack: function() {
                        n.navigateTo({
                            url: "/pages/myRedpack/index"
                        });
                    },
                    checkScore: function() {
                        n.navigateTo({
                            url: "/pages/myScore/index"
                        });
                    },
                    openContact: function() {},
                    setCopyText: function(t) {
                        n.setClipboardData({
                            data: t,
                            success: function(t) {
                                n.showToast({
                                    title: "复制成功"
                                });
                            }
                        });
                    },
                    getOrderInfo: function() {
                        var t = this;
                        this.uuid, n.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$http("/asset/package-skus/".concat(this.uuid)).then(function(e) {
                            t.info = e.data.info, n.hideLoading();
                        }).catch(function(n) {});
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    payNow: function() {
                        n.navigateTo({
                            url: "/pages/payCenter/index?uuid=" + this.order.uuid
                        });
                    }
                }
            };
            t.default = o;
        }).call(this, e("543d").default);
    },
    ad4d: function(n, t, e) {
        "use strict";
        e.r(t);
        var i = e("11fa"), o = e("7334");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(u);
        e("d374");
        var a = e("f0c5"), c = Object(a.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        t.default = c.exports;
    },
    d374: function(n, t, e) {
        "use strict";
        var i = e("897b");
        e.n(i).a;
    }
}, [ [ "1553", "common/runtime", "common/vendor" ] ] ]);