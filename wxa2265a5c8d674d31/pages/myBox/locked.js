(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myBox/locked" ], {
    8102: function(t, e, i) {
        "use strict";
        var s = i("deef");
        i.n(s).a;
    },
    8222: function(t, e, i) {
        "use strict";
        i.r(e);
        var s = i("a02c"), n = i.n(s);
        for (var a in s) [ "default" ].indexOf(a) < 0 && function(t) {
            i.d(e, t, function() {
                return s[t];
            });
        }(a);
        e.default = n.a;
    },
    "894a": function(t, e, i) {
        "use strict";
        i.d(e, "b", function() {
            return n;
        }), i.d(e, "c", function() {
            return a;
        }), i.d(e, "a", function() {
            return s;
        });
        var s = {
            NoData: function() {
                return i.e("components/NoData/NoData").then(i.bind(null, "cafe"));
            }
        }, n = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__map(t.types, function(e, i) {
                return {
                    $orig: t.__get_orig(e),
                    l0: t.__map(t.dataList[i].list, function(e, i) {
                        return {
                            $orig: t.__get_orig(e),
                            g0: t.selectedIds.indexOf(e.id)
                        };
                    }),
                    g1: t.dataList[i].list.length,
                    g2: t.dataList[i].init && !t.dataList[i].list.length
                };
            }));
            t.$mp.data = Object.assign({}, {
                $root: {
                    l1: e
                }
            });
        }, a = [];
    },
    a02c: function(t, e, i) {
        "use strict";
        (function(t) {
            var s = i("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = s(i("448a")), a = {
                components: {
                    PackageSku: function() {
                        Promise.all([ i.e("common/vendor"), i.e("pages/myBox/components/PackageSku") ]).then(function() {
                            return resolve(i("de82"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        order: {},
                        list: [],
                        reasons: [],
                        visible: !1,
                        dataList: [],
                        current: 0,
                        currentUuid: "6056e83ef3251",
                        currentItem: {},
                        types: [ "locked" ],
                        typeTextList: [ "锁定中" ],
                        isSelectMode: !1,
                        selectType: "deliver",
                        selectedIds: [],
                        isShowPay: !1,
                        isShowReturnSalePopup: !1,
                        isShowResalePopup: !1,
                        statusTotal: {},
                        isReturnSaleSuccess: !1
                    };
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order || {};
                    },
                    marketConfig: function() {
                        return this.$store.getters.setting.market || {};
                    },
                    exchangeConfig: function() {
                        return this.$store.getters.setting.exchange || {};
                    }
                },
                onLoad: function(t) {
                    this.current = this.types.indexOf(t.status || "locked"), this.initData();
                },
                onShow: function() {
                    this.refresh();
                },
                onUnload: function() {},
                methods: {
                    hideResalePopup: function() {
                        this.isShowResalePopup = !1;
                    },
                    toPage: function(e) {
                        t.navigateTo({
                            url: e
                        });
                    },
                    successDeliver: function() {
                        this.cancelSelect(), this.current = 2, this.refresh(), this.isShowPay = !1, t.showModal({
                            title: "发货成功",
                            content: "已成功提交发货请求，请注意查收快递哦~"
                        });
                    },
                    selectAll: function() {
                        if (!this.dataList[0]) return !1;
                        this.selectedIds = [];
                        for (var t = 0; t < this.dataList[0].list.length; t++) {
                            var e = this.dataList[0].list[t];
                            "deliver" === this.selectType ? "virtual_asset" === e.sku_type_text || e.is_presell || this.selectedIds.push(e.id) : "return_sale" === this.selectType ? !this.orderConfig.is_ban_return_sale && "virtual_asset" != e.sku_type_text && e.is_return_saleable && this.selectedIds.push(e.id) : "resale" === this.selectType && "virtual_asset" != e.sku_type_text && this.selectedIds.push(e.id);
                        }
                    },
                    cancelSelect: function() {
                        this.isSelectMode = !1, this.selectedIds = [];
                    },
                    selectOrSubmit: function() {
                        if (this.isSelectMode) {
                            if (0 == this.selectedIds.length) return t.showModal({
                                title: "请选择物品",
                                content: "选择一件或多件物品后才能提交发货哦~"
                            }), !1;
                            this.isShowPay = !0;
                        } else this.isSelectMode = !0;
                    },
                    batchReturnSale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能回收哦~"
                        }), !1;
                        t.showModal({
                            title: "确认回收",
                            content: "确认要批量回收吗?",
                            success: function(i) {
                                i.confirm && (t.showLoading({
                                    title: "回收中..."
                                }), e.$http("/asset/return-sale/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(i) {
                                    e.isReturnSaleSuccess = 1, t.hideLoading(), e.refresh(), e.isSelectMode = !1;
                                }));
                            }
                        });
                    },
                    batchResale: function() {
                        var e = this;
                        if (0 == this.selectedIds.length) return t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能挂售哦~"
                        }), !1;
                        t.showModal({
                            title: "确认挂售到交易市场",
                            content: "统一以建议挂售价挂售到交易市场",
                            success: function(i) {
                                i.confirm && (t.showLoading({
                                    title: "挂售中..."
                                }), e.$http("/market/resale/batch/confirm", "post", {
                                    ids: e.selectedIds
                                }).then(function(i) {
                                    t.hideLoading(), e.refresh(), e.isSelectMode = !1, t.showToast({
                                        title: "转售成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.navigateTo({
                                            url: "/pages/resale/index"
                                        });
                                    }, 1500);
                                }));
                            }
                        });
                    },
                    batchExchange: function() {
                        return 0 == this.selectedIds.length ? (t.showModal({
                            title: "请选择物品",
                            content: "选择一件或多件物品后才能置换哦~"
                        }), !1) : this.selectedIds.length >= 10 ? (t.showModal({
                            title: "超出限制了",
                            content: "单次置换限制在10件以内~"
                        }), !1) : (this.setStorage("exchange_package_sku_ids", this.selectedIds), void t.navigateTo({
                            url: "/pages/exchange/productList"
                        }));
                    },
                    enterSelectMode: function(t) {
                        this.selectedIds = [], this.isSelectMode = !0, this.selectType = t;
                    },
                    checkItem: function(t) {
                        var e = this.selectedIds.indexOf(t.id);
                        e > -1 ? this.selectedIds.splice(e, 1) : this.selectedIds.push(t.id);
                    },
                    refresh: function() {
                        this.cleanData(), this.getOrderList(), this.initBoxTotalData();
                    },
                    scrolltolower: function() {
                        this.dataList[this.current].page++, this.getOrderList();
                    },
                    initData: function() {
                        var t = [];
                        this.types.forEach(function(e) {
                            t.push({
                                list: [],
                                type: e,
                                page: 1,
                                per_page: 50,
                                total: 0,
                                init: !1,
                                loading: !1
                            });
                        }), console.log(t), this.dataList = t;
                    },
                    cleanData: function() {
                        this.dataList.forEach(function(t) {
                            t.page = 1, t.init = !1;
                        });
                    },
                    initBoxTotalData: function() {
                        var t = this;
                        this.$http("/status-total/package-sku").then(function(e) {
                            t.statusTotal = e.data.info;
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    actions: function(e) {
                        switch (e.action) {
                          case "返售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowReturnSalePopup = !0;
                            break;

                          case "核销码":
                            t.navigateTo({
                                url: "/pages/orderCode/index?uuid=" + e.order.uuid
                            });
                            break;

                          case "查看订单":
                            t.navigateTo({
                                url: "/pages/orderDetail/index?uuid=" + e.order.pick_order.uuid
                            });
                            break;

                          case "转售":
                            this.currentUuid = e.order.uuid, this.currentItem = e.order, this.isShowResalePopup = !0;
                        }
                    },
                    currentChange: function(t) {
                        var e = t.currentTarget.dataset.current;
                        e !== this.current && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    currentChange2: function(t) {
                        var e = t.detail.current;
                        e !== this.current && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    getOrderList: function() {
                        var t = this.dataList[this.current];
                        t.loading || (t.loading = !0, this.$http("/asset/package-skus", "GET", {
                            status: t.type,
                            page: t.page,
                            per_page: t.per_page
                        }).then(function(e) {
                            var i;
                            t.loading = !1, 1 === e.data.current_page ? t.list = e.data.list : (i = t.list).push.apply(i, (0, 
                            n.default)(e.data.list)), t.total = e.data.item_total, t.init = !0;
                        }));
                    }
                }
            };
            e.default = a;
        }).call(this, i("543d").default);
    },
    a7fb: function(t, e, i) {
        "use strict";
        (function(t, e) {
            var s = i("4ea4");
            i("18ba"), s(i("66fd"));
            var n = s(i("fc9d"));
            t.__webpack_require_UNI_MP_PLUGIN__ = i, e(n.default);
        }).call(this, i("bc2e").default, i("543d").createPage);
    },
    deef: function(t, e, i) {},
    fc9d: function(t, e, i) {
        "use strict";
        i.r(e);
        var s = i("894a"), n = i("8222");
        for (var a in n) [ "default" ].indexOf(a) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(a);
        i("8102");
        var r = i("f0c5"), o = Object(r.a)(n.default, s.b, s.c, !1, null, null, null, !1, s.a, void 0);
        e.default = o.exports;
    }
}, [ [ "a7fb", "common/runtime", "common/vendor" ] ] ]);