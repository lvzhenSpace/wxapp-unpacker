(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/rule/index" ], {
    "070d": function(e, n, t) {
        "use strict";
        t.r(n);
        var o = t("22ee"), i = t("e5be");
        for (var r in i) [ "default" ].indexOf(r) < 0 && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(r);
        t("90e3");
        var a = t("f0c5"), u = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        n.default = u.exports;
    },
    "0a18": function(e, n, t) {},
    "22ee": function(e, n, t) {
        "use strict";
        t.d(n, "b", function() {
            return i;
        }), t.d(n, "c", function() {
            return r;
        }), t.d(n, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/PageRender/PageRender") ]).then(t.bind(null, "b4aa"));
            },
            SharePopup: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/SharePopup") ]).then(t.bind(null, "b677"));
            }
        }, i = function() {
            var e = this;
            e.$createElement, e._self._c, e._isMounted || (e.e0 = function(n) {
                e.showShare = !1;
            });
        }, r = [];
    },
    "22f2": function(e, n, t) {
        "use strict";
        (function(e, n) {
            var o = t("4ea4");
            t("18ba"), o(t("66fd"));
            var i = o(t("070d"));
            e.__webpack_require_UNI_MP_PLUGIN__ = t, n(i.default);
        }).call(this, t("bc2e").default, t("543d").createPage);
    },
    "90e3": function(e, n, t) {
        "use strict";
        var o = t("0a18");
        t.n(o).a;
    },
    "9b1f": function(e, n, t) {
        "use strict";
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var o = t("b5e5"), i = (t("2684"), {
                components: {
                    PosterTheme1: function() {
                        Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/components/posterTheme1") ]).then(function() {
                            return resolve(t("04ac"));
                        }.bind(null, t)).catch(t.oe);
                    },
                    SharePopup: function() {
                        Promise.all([ t.e("common/vendor"), t.e("components/SharePopup/SharePopup") ]).then(function() {
                            return resolve(t("b677"));
                        }.bind(null, t)).catch(t.oe);
                    }
                },
                name: "rule",
                data: function() {
                    return {
                        isInit: !1,
                        type: "",
                        page: {},
                        canvasStatus: !1,
                        sharerInfo: {},
                        all_money: 0,
                        userInfo: (0, o.getStorageSync)("userInfo") || {},
                        showShare: !1,
                        title: "初级推荐官",
                        direct_inviter: 0,
                        indirect_inviter: 0
                    };
                },
                computed: {
                    posterInfo: function() {
                        var e = this.$store.getters.userInfo || {}, n = this.getShareConfig(!1);
                        return {
                            path: "/pages/index/index?type=share&&inviter=" + e.uuid,
                            app_url: n.app_url,
                            thumb: "https://img121.7dun.com/yuanqimali/share/posterImg.png"
                        };
                    }
                },
                watch: {},
                created: function() {
                    e.loadFontFace({
                        family: "BebasNeue",
                        source: 'url("https://img121.7dun.com/newRecommend2/BebasNeue-1.otf")',
                        success: function() {
                            console.log("success");
                        }
                    });
                },
                onLoad: function(e) {
                    this.type = e.type, this.initData(), this.getDistribution();
                },
                methods: {
                    tograde: function() {
                        e.pageScrollTo({
                            scrollTop: 800,
                            duration: 300
                        });
                    },
                    initData: function() {
                        var e = this;
                        this.$http("/setting/" + this.type + "_rule").then(function(n) {
                            e.page = n.data.setting, e.isInit = !0;
                        });
                    },
                    close: function() {
                        this.showShare = !1, this.$emit("close");
                    },
                    saveImg: function() {
                        e.downloadFile({
                            url: "https://img121.7dun.com/member_grade/peipeiOnline.jpg",
                            success: function(n) {
                                e.showToast({
                                    title: "已保存到相册",
                                    icon: "none"
                                });
                            }
                        });
                    },
                    handlePosterImg: function() {
                        this.showShare = !0;
                    },
                    getDistribution: function() {
                        var e = this;
                        this.$http("/agent/brokerage").then(function(n) {
                            e.sharerInfo = n.data, e.all_money = n.data.all.money / 100, e.title = n.data.now_agent_level, 
                            e.direct_inviter = n.data.direct_inviter, e.indirect_inviter = n.data.indirect_inviter;
                        });
                    },
                    goinvite: function() {
                        e.navigateTo({
                            url: "/pages/myInvitees/index"
                        });
                    }
                }
            });
            n.default = i;
        }).call(this, t("543d").default);
    },
    e5be: function(e, n, t) {
        "use strict";
        t.r(n);
        var o = t("9b1f"), i = t.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(r);
        n.default = i.a;
    }
}, [ [ "22f2", "common/runtime", "common/vendor" ] ] ]);