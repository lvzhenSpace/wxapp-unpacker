(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myScore/index" ], {
    "10e1": function(t, e, n) {
        "use strict";
        n.r(e);
        var r = n("5b64"), a = n("c0b1");
        for (var u in a) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(u);
        n("582d");
        var o = n("f0c5"), i = Object(o.a)(a.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        e.default = i.exports;
    },
    "4d60": function(t, e, n) {
        "use strict";
        (function(t) {
            var r = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var a = r(n("2eee")), u = r(n("448a")), o = r(n("c973")), i = n("c51d"), c = {
                components: {},
                data: function() {
                    return {
                        list: [],
                        total: 0,
                        page: 1,
                        per_page: 20,
                        init: !1,
                        loading: !1
                    };
                },
                computed: {
                    navBar: function() {
                        return this.$store.getters.deviceInfo.customBar;
                    },
                    userInfo: function() {
                        return this.$store.getters.userInfo;
                    },
                    setting: function() {
                        return this.$store.getters.setting.score || {};
                    },
                    isBuyEnabled: function() {
                        return this.setting.is_buy_enabled;
                    }
                },
                onLoad: function() {
                    var e = this;
                    return (0, o.default)(a.default.mark(function n() {
                        var r, o;
                        return a.default.wrap(function(n) {
                            for (;;) switch (n.prev = n.next) {
                              case 0:
                                return e.$store.dispatch("getUserInfo"), t.showLoading({
                                    title: "加载中"
                                }), n.next = 4, e.getScoreRecord();

                              case 4:
                                o = n.sent, t.hideLoading(), e.init = !0, (r = e.list).push.apply(r, (0, u.default)(o.data.list)), 
                                e.total = o.data.item_total, t.setNavigationBarTitle({
                                    title: "我的" + e.scoreAlias
                                });

                              case 10:
                              case "end":
                                return n.stop();
                            }
                        }, n);
                    }))();
                },
                onReachBottom: function() {
                    var t = this;
                    return (0, o.default)(a.default.mark(function e() {
                        var n, r;
                        return a.default.wrap(function(e) {
                            for (;;) switch (e.prev = e.next) {
                              case 0:
                                if (!t.loading) {
                                    e.next = 2;
                                    break;
                                }
                                return e.abrupt("return");

                              case 2:
                                return t.loading = !0, t.page++, e.next = 6, t.getScoreRecord();

                              case 6:
                                r = e.sent, t.loading = !1, (n = t.list).push.apply(n, (0, u.default)(r.data.list));

                              case 9:
                              case "end":
                                return e.stop();
                            }
                        }, e);
                    }))();
                },
                methods: {
                    getScoreRecord: function() {
                        var t = this;
                        return (0, o.default)(a.default.mark(function e() {
                            return a.default.wrap(function(e) {
                                for (;;) switch (e.prev = e.next) {
                                  case 0:
                                    return e.next = 2, (0, i.getScoreRecord)({
                                        page: t.page,
                                        per_page: t.per_page
                                    });

                                  case 2:
                                    return e.abrupt("return", e.sent);

                                  case 3:
                                  case "end":
                                    return e.stop();
                                }
                            }, e);
                        }))();
                    },
                    scoreRedPack: function() {
                        t.navigateTo({
                            url: "/pages/scoreRedPack/index"
                        });
                    }
                }
            };
            e.default = c;
        }).call(this, n("543d").default);
    },
    "582d": function(t, e, n) {
        "use strict";
        var r = n("5871");
        n.n(r).a;
    },
    5871: function(t, e, n) {},
    "5b64": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return a;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {
            return r;
        });
        var r = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "cafe"));
            }
        }, a = function() {
            this.$createElement;
            var t = (this._self._c, !this.list.length && this.init);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, u = [];
    },
    b5d7: function(t, e, n) {
        "use strict";
        (function(t, e) {
            var r = n("4ea4");
            n("18ba"), r(n("66fd"));
            var a = r(n("10e1"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(a.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    c0b1: function(t, e, n) {
        "use strict";
        n.r(e);
        var r = n("4d60"), a = n.n(r);
        for (var u in r) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(e, t, function() {
                return r[t];
            });
        }(u);
        e.default = a.a;
    }
}, [ [ "b5d7", "common/runtime", "common/vendor" ] ] ]);