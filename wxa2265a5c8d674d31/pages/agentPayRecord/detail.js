(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/agentPayRecord/detail" ], {
    "2f20": function(e, t, n) {
        "use strict";
        (function(e) {
            var r = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = r(n("9523")), o = r(n("452d")), u = r(n("82c0")), c = n("26cb");
            function a(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var r = Object.getOwnPropertySymbols(e);
                    t && (r = r.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, r);
                }
                return n;
            }
            function s(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? a(Object(n), !0).forEach(function(t) {
                        (0, i.default)(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : a(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            var d = {
                mixins: [ o.default ],
                components: {
                    IActionSheet: function() {
                        n.e("components/ActionSheet/index").then(function() {
                            return resolve(n("fbfb"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        visible: !1,
                        info: {},
                        order: {},
                        skus: [],
                        user: {},
                        uuid: ""
                    };
                },
                computed: s(s({}, (0, c.mapGetters)([ "userInfo" ])), {}, {
                    share: function() {
                        return {
                            title: this.user.name + "请你代付一个订单"
                        };
                    }
                }),
                filters: {
                    hidePhoneDetail: function(e) {
                        return e ? e.substring(0, 3) + "****" + e.substring(7, 11) : "";
                    }
                },
                onLoad: function(e) {
                    var t = this;
                    this.uuid = e.uuid, this.$api.emit("order.cancel_reason.list").then(function(e) {
                        t.reasons = e.data.list;
                    });
                },
                onShow: function() {
                    this.initData();
                },
                methods: {
                    checkOrder: function() {
                        e.navigateTo({
                            url: "/pages/orderDetail/index?uuid=" + this.order.uuid
                        });
                    },
                    paySubmit: function() {
                        var t = this;
                        e.showLoading({
                            title: "提交中"
                        }), this.$http("/agent-pay-records/".concat(this.uuid, "/pay"), "POST").then(function(n) {
                            e.hideLoading(), u.default.pay({
                                pay_config: n.data.pay_config,
                                success: function() {
                                    t.initData();
                                },
                                fail: function() {
                                    e.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    });
                                }
                            });
                        });
                    },
                    setCopyText: function(t) {
                        e.setClipboardData({
                            data: t,
                            success: function(t) {
                                e.showToast({
                                    title: "复制成功"
                                });
                            }
                        });
                    },
                    initData: function() {
                        var t = this;
                        this.uuid, e.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$http("/agent-pay-records/".concat(this.uuid)).then(function(n) {
                            e.hideLoading(), t.order = n.data.info.order, t.info = n.data.info, t.skus = t.order.skus, 
                            t.user = n.data.info.user;
                        });
                    }
                }
            };
            t.default = d;
        }).call(this, n("543d").default);
    },
    "30e0": function(e, t, n) {
        "use strict";
        var r = n("6058");
        n.n(r).a;
    },
    "45de": function(e, t, n) {
        "use strict";
        (function(e, t) {
            var r = n("4ea4");
            n("18ba"), r(n("66fd"));
            var i = r(n("62eb"));
            e.__webpack_require_UNI_MP_PLUGIN__ = n, t(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "5f55": function(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("2f20"), i = n.n(r);
        for (var o in r) [ "default" ].indexOf(o) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(o);
        t.default = i.a;
    },
    6058: function(e, t, n) {},
    "62eb": function(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("d56c"), i = n("5f55");
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(o);
        n("30e0");
        var u = n("f0c5"), c = Object(u.a)(i.default, r.b, r.c, !1, null, null, null, !1, r.a, void 0);
        t.default = c.exports;
    },
    d56c: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return o;
        }), n.d(t, "a", function() {
            return r;
        });
        var r = {
            SkuItem: function() {
                return Promise.all([ n.e("common/vendor"), n.e("components/SkuItem/SkuItem") ]).then(n.bind(null, "3c3e"));
            },
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            },
            ReturnSalePopup: function() {
                return n.e("components/ReturnSalePopup/ReturnSalePopup").then(n.bind(null, "f4ef"));
            }
        }, i = function() {
            var e = this, t = (e.$createElement, e._self._c, e.order.uuid && e.order.cover_discount ? e._f("priceToFixed")(e.order.cover_discount) : null), n = e.order.uuid ? e._f("priceToFixed")(e.order.coupon_discount) : null, r = e.order.uuid ? e._f("priceToFixed")(e.order.redpack_discount) : null, i = e.order.uuid && 2 !== e.order.carriage_type ? e._f("priceToFixed")(e.order.carriage) : null;
            e._isMounted || (e.e0 = function(t) {
                e.isShowReturnSalePopup = !1;
            }), e.$mp.data = Object.assign({}, {
                $root: {
                    f0: t,
                    f1: n,
                    f2: r,
                    f3: i
                }
            });
        }, o = [];
    }
}, [ [ "45de", "common/runtime", "common/vendor" ] ] ]);