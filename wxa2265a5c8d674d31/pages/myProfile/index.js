(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myProfile/index" ], {
    "6ce7": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("ca9d"), i = n("f75a");
        for (var r in i) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("ddfc"), n("9bb5");
        var c = n("f0c5"), a = Object(c.a)(i.default, o.b, o.c, !1, null, "6768d372", null, !1, o.a, void 0);
        t.default = a.exports;
    },
    "81a8": function(e, t, n) {},
    "940f": function(e, t, n) {},
    "9bb5": function(e, t, n) {
        "use strict";
        var o = n("940f");
        n.n(o).a;
    },
    bd80: function(e, t, n) {
        "use strict";
        (function(e, o) {
            var i = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = i(n("2eee")), c = i(n("c973")), a = i(n("9523"));
            function s(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            var u = {
                components: {},
                data: function() {
                    return {
                        gender: [ "男", "女", "保密" ],
                        genderIndex: 0,
                        code: "",
                        isShowPhonePopup: !1,
                        form: {
                            headimg: "",
                            name: "",
                            phone: "",
                            city: "",
                            gender: "",
                            email: "",
                            birthday: ""
                        }
                    };
                },
                computed: function(e) {
                    for (var t = 1; t < arguments.length; t++) {
                        var n = null != arguments[t] ? arguments[t] : {};
                        t % 2 ? s(Object(n), !0).forEach(function(t) {
                            (0, a.default)(e, t, n[t]);
                        }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : s(Object(n)).forEach(function(t) {
                            Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                        });
                    }
                    return e;
                }({}, (0, n("26cb").mapGetters)([ "userInfo", "token" ])),
                watch: {
                    userInfo: function() {
                        this.initForm();
                    }
                },
                created: function() {
                    var t = this;
                    e.login({
                        success: function(e) {
                            t.code = e.code;
                        },
                        fail: function(e) {}
                    });
                },
                onLoad: function() {
                    var e = this;
                    return (0, c.default)(r.default.mark(function t() {
                        return r.default.wrap(function(t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                e.$store.dispatch("getUserInfo"), e.initForm();

                              case 2:
                              case "end":
                                return t.stop();
                            }
                        }, t);
                    }))();
                },
                methods: {
                    onChooseAvatar: function(t) {
                        console.log(t, "11");
                        var n = "wechat-" + (e.getSystemInfoSync().appName || "") + "-miniapp-" + (e.getSystemInfoSync().platform || ""), i = this;
                        o.uploadFile({
                            url: "https://api.we213.fuzhouweiyi.top/image",
                            filePath: t.detail.avatarUrl,
                            name: "image",
                            header: {
                                Authorization: this.token || "",
                                "Client-Type": n,
                                "Client-Name": "default",
                                "content-type": "multipart/form-data"
                            },
                            success: function(t) {
                                e.showToast({
                                    title: "上传成功~",
                                    icon: "none"
                                }), t.data, console.log("image=>", JSON.parse(t.data).data.image.url), i.form.headimg = JSON.parse(t.data).data.image.url;
                            },
                            fail: function(t) {
                                e.showToast({
                                    title: t,
                                    icon: "none"
                                }), console.log("fail:" + JSON.stringify(t));
                            }
                        });
                    },
                    bindInviter: function() {
                        var t = this;
                        e.showModal({
                            title: "请输入邀请码",
                            editable: !0,
                            success: function(n) {
                                if (n.confirm) {
                                    if (!n.content) return !1;
                                    t.$http("/user/bind-inviter", "POST", {
                                        invite_code: n.content
                                    }).then(function(n) {
                                        e.showToast({
                                            title: "绑定成功~",
                                            icon: "none"
                                        }), t.$store.dispatch("getUserInfo");
                                    });
                                }
                            }
                        });
                    },
                    handleDeleteAccount: function() {
                        var t = this;
                        e.showModal({
                            title: "确定要删除帐号吗?",
                            content: "删除帐号后所有帐号资料将无法找回，请谨慎操作!",
                            confirmText: "仍然删除",
                            cancelText: "暂不删除",
                            success: function(n) {
                                n.confirm && (e.showLoading({
                                    title: "申请中..."
                                }), t.$http("/user", "DELETE").then(function(t) {
                                    e.hideLoading(), e.showModal({
                                        title: "申请成功",
                                        content: "已为您申请删除帐号，待客服审核通过后帐号即会被删除~"
                                    });
                                }));
                            }
                        });
                    },
                    handleLogout: function() {
                        var t = this;
                        e.showModal({
                            title: "确定要退出登录吗?",
                            confirmText: "退出登录",
                            cancelText: "暂不",
                            success: function(n) {
                                n.confirm && (t.$store.dispatch("logout"), e.showToast({
                                    title: "已退出登陆，跳转中~",
                                    icon: "none"
                                }), setTimeout(function() {
                                    e.switchTab({
                                        url: "/pages/index/index"
                                    });
                                }, 1800));
                            }
                        });
                    },
                    startGetPhone: function() {
                        this.isShowPhonePopup = !0;
                    },
                    getPhoneNumberSuccess: function(t) {
                        var n = this;
                        this.$http("/phone-update/with-code", "POST", {
                            phone: t.phone,
                            phone_code: t.phone_code
                        }).then(function(t) {
                            n.$store.dispatch("getUserInfo"), e.showToast({
                                title: "手机号更新成功~",
                                icon: "none"
                            }), n.isShowPhonePopup = !1;
                        });
                    },
                    initForm: function() {
                        this.form.headimg = this.userInfo.headimg, this.form.name = this.userInfo.name, 
                        this.form.phone = this.userInfo.phone, this.form.city = this.userInfo.city, this.form.gender = this.userInfo.gender, 
                        this.form.email = this.userInfo.email, this.form.birthday = this.userInfo.birthday, 
                        this.genderIndex = "男" === this.userInfo.gender ? 0 : 1;
                    },
                    updateProfile: function(t) {
                        console.log(t), e.showLoading({
                            title: "更新中"
                        }), console.log(this.form), this.$http({
                            url: "/user",
                            method: "PUT",
                            data: this.form
                        }).then(function(t) {
                            e.hideLoading(), e.showToast({
                                title: "更新成功，跳转中~",
                                icon: "none"
                            }), setTimeout(function() {
                                e.switchTab({
                                    url: "/pages/index/index"
                                });
                            }, 1300);
                        }).catch(function(e) {});
                    },
                    selectBirthday: function(e) {
                        this.form.birthday = e.detail.value;
                    },
                    selectGender: function(e) {
                        this.genderIndex = e.detail.value, this.form.gender = this.gender[this.genderIndex];
                    },
                    getPhoneNumber: function(t) {
                        var n = this;
                        t.detail.encryptedData && (console.log("eeee", t), this.$http("/phone-update/with-miniapp", "POST", {
                            encrypt_data: t.detail.encryptedData,
                            iv: t.detail.iv,
                            code: this.code
                        }).then(function(t) {
                            n.$store.dispatch("getUserInfo"), e.showModal({
                                title: "手机号更新成功~",
                                icon: "none"
                            }), e.login({
                                success: function(e) {
                                    n.code = e.code;
                                },
                                fail: function(e) {}
                            });
                        }));
                    }
                },
                onShow: function() {},
                onPageScroll: function(e) {
                    this.scrollTop = e.scrollTop;
                }
            };
            t.default = u;
        }).call(this, n("543d").default, n("bc2e").default);
    },
    ca9d: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            GetPhonePopup: function() {
                return n.e("components/GetPhonePopup/GetPhonePopup").then(n.bind(null, "fc3f"));
            }
        }, i = function() {
            var e = this;
            e.$createElement, e._self._c, e._isMounted || (e.e0 = function(t) {
                e.isShowPhonePopup = !1;
            });
        }, r = [];
    },
    d2e9: function(e, t, n) {
        "use strict";
        (function(e, t) {
            var o = n("4ea4");
            n("18ba"), o(n("66fd"));
            var i = o(n("6ce7"));
            e.__webpack_require_UNI_MP_PLUGIN__ = n, t(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    ddfc: function(e, t, n) {
        "use strict";
        var o = n("81a8");
        n.n(o).a;
    },
    f75a: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("bd80"), i = n.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    }
}, [ [ "d2e9", "common/runtime", "common/vendor" ] ] ]);