(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/renyimen/detail" ], {
    "0876": function(t, i, n) {
        "use strict";
        var e = n("35db");
        n.n(e).a;
    },
    "35db": function(t, i, n) {},
    7341: function(t, i, n) {
        "use strict";
        (function(t) {
            var e = n("4ea4");
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var o = e(n("9523"));
            function u(t, i) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var e = Object.getOwnPropertySymbols(t);
                    i && (e = e.filter(function(i) {
                        return Object.getOwnPropertyDescriptor(t, i).enumerable;
                    })), n.push.apply(n, e);
                }
                return n;
            }
            function s(t) {
                for (var i = 1; i < arguments.length; i++) {
                    var n = null != arguments[i] ? arguments[i] : {};
                    i % 2 ? u(Object(n), !0).forEach(function(i) {
                        (0, o.default)(t, i, n[i]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : u(Object(n)).forEach(function(i) {
                        Object.defineProperty(t, i, Object.getOwnPropertyDescriptor(n, i));
                    });
                }
                return t;
            }
            var a = {
                components: {
                    OpenBoxPopup: function() {
                        n.e("pages/renyimen/components/OpenBoxPopup").then(function() {
                            return resolve(n("a929"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    PayCard: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/renyimen/components/PayCard") ]).then(function() {
                            return resolve(n("dbde"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    RecordList: function() {
                        n.e("pages/renyimen/components/RecordList").then(function() {
                            return resolve(n("88ea"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    imagepop: function() {
                        n.e("pages/renyimen/components/imagepop").then(function() {
                            return resolve(n("44a6"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    bangDan: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/renyimen/components/bangDan") ]).then(function() {
                            return resolve(n("6e75"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    wanfa: function() {
                        n.e("pages/renyimen/components/wanfa").then(function() {
                            return resolve(n("311b"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    jiangli: function() {
                        n.e("pages/renyimen/components/jiangli").then(function() {
                            return resolve(n("185a"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        noclick: !0,
                        huadong: !1,
                        movableX: 0,
                        movableXNew: 0,
                        swiperHeight: 0,
                        duocishu: null,
                        duocishuNum: 0,
                        limitNum: 0,
                        nowTeamTotal: null,
                        emitTotal: null,
                        limitCountNum: 0,
                        lianjiCiShu: 1,
                        emitWordTotal: 1,
                        current: 0,
                        types: [ "pending", "resale_pending" ],
                        typeTextList: [ "连击池", "秘宝池" ],
                        bgImgs: [ "url(https://img121.7dun.com/yuanqimali/fudai/rBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/srBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/ssrBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/urBg.png)", "url()" ],
                        uuid: "",
                        isInit: !1,
                        isPayPopup: !1,
                        payTotal: 1,
                        info: {
                            min_lucky_score: ""
                        },
                        order: {},
                        pageUuid: "",
                        isNotFound: !1,
                        isShowResultPopup: !1,
                        danmuSetting: {},
                        danmuList: [],
                        config: {},
                        detailImageList: [],
                        levelFilter: 0,
                        isTryMode: !1,
                        isShowRankList: !1,
                        wining_list: [],
                        isimagepop: !1,
                        is_doubleBoxCard: 0,
                        skuinfo: {},
                        skusList: [],
                        skus_ultimate: {},
                        openSkus: [],
                        isOpenPopup: !1,
                        istry: !1,
                        isOpen: !1
                    };
                },
                computed: s(s({}, (0, n("26cb").mapGetters)([ "userInfo" ])), {}, {
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/fudai/detail_bg.png";
                    },
                    boxImage: function() {
                        return this.info.image_3d || "https://cdn2.hquesoft.com/box/fudai/box.png";
                    },
                    skuLevel: function() {
                        return this.info.sku_level || [];
                    },
                    skus: function() {
                        var t = this;
                        return this.levelFilter ? this.info.skus.filter(function(i) {
                            return i.level === t.levelFilter;
                        }) : this.info.skus || [];
                    },
                    payInfo: function() {
                        return {
                            page_uuid: this.pageUuid,
                            title: this.info.title,
                            pay_total: this.payTotal,
                            total_list: this.info.total_list,
                            money_price: this.info.money_price,
                            score_price: this.info.score_price
                        };
                    },
                    titleBgColor: function() {
                        return " ";
                    },
                    titleColor: function() {
                        return "#ffffff";
                    },
                    setTitleText: function() {
                        t.setNavigationBarTitle({
                            title: this.info.title
                        });
                    },
                    discountTips: function() {
                        for (var t = this.info.total_list || [], i = t.length - 1; i >= 0; i--) if (t[i].is_discount) {
                            var n = t[i].total + "连开优惠";
                            return t[i].money_discount && (n += t[i].money_discount / 100 + "元"), t[i].score_discount && (n += t[i].score_discount + this.scoreAlias), 
                            n;
                        }
                        return !1;
                    },
                    width_value: function() {
                        return this.userInfo.lucky_score / this.info.min_lucky_score * 100 > 100 ? 100 : this.userInfo.lucky_score / this.info.min_lucky_score * 100 < 0 ? 0 : Math.trunc(this.userInfo.lucky_score / this.info.min_lucky_score * 100);
                    }
                }),
                watch: {},
                filters: {},
                onLoad: function(t) {
                    this.uuid = t.uuid;
                },
                onUnload: function() {},
                onPullDownRefresh: function() {
                    this.$showPullRefresh(), this.initData(), this.setTitleText();
                },
                onShow: function() {
                    var i = this;
                    this.$nextTick(function() {
                        i.setSwiperHeight(), i.current = 0, i.movableX = 0;
                    }), t.showLoading({
                        title: "加载中"
                    }), this.initData().then(function(n) {
                        i.isInit = !0, t.hideLoading(), i.$http("/renyi/change", "POST", {
                            renyi_id: i.info.id
                        }).then(function(t) {
                            i.pageUuid = t.data.page_uuid, i.fetchList();
                        });
                    }).catch(function(t) {
                        i.isNotFound = !0;
                    });
                },
                onReachBottom: function() {},
                methods: {
                    doudong: function() {
                        var t = this;
                        setTimeout(function() {
                            t.huadong || (t.movableXNew -= 15, setTimeout(function() {
                                t.huadong || (t.movableXNew += 30, setTimeout(function() {
                                    t.huadong || (t.movableXNew -= 30, setTimeout(function() {
                                        t.huadong || (t.movableXNew += 30, setTimeout(function() {
                                            t.huadong || (t.movableXNew = 10);
                                        }, 100));
                                    }, 100));
                                }, 100));
                            }, 100));
                        }, 100);
                    },
                    onMovableChange: function(t) {
                        "touch" === t.detail.source && (this.movableXNew = t.detail.x);
                    },
                    onTouchEndChange: function() {
                        var t = this;
                        this.movableX = this.movableXNew, this.$nextTick(function() {
                            t.movableX < 80 ? (t.movableX = 0, t.current = 0) : (t.movableX = 146, t.current = 1);
                        });
                    },
                    setSwiperHeight: function() {
                        var i = this;
                        setTimeout(function() {
                            0 == i.current ? t.createSelectorQuery().in(i).select("#content-wrap").boundingClientRect(function(t) {
                                i.swiperHeight = t.height;
                            }).exec(function(t) {
                                i.swiperHeight = t[0].height;
                            }) : t.createSelectorQuery().in(i).select("#content-wrap1").boundingClientRect(function(t) {
                                i.swiperHeight = t.height + 30;
                            }).exec(function(t) {
                                i.swiperHeight = t[0].height + 30;
                            });
                        }, 400);
                    },
                    handleRecordList: function() {
                        this.isShowRankList = !0;
                    },
                    handleRecordListClose: function() {
                        this.isShowRankList = !1;
                    },
                    handleMaxClick: function() {
                        t.vibrateShort({}), this.emitWordTotal - this.limitNum <= 0 || (this.lianjiCiShu = this.emitWordTotal - this.limitNum);
                    },
                    handleJianClick: function() {
                        t.vibrateShort({}), 1 != this.lianjiCiShu && 0 != this.lianjiCiShu ? this.lianjiCiShu = this.lianjiCiShu - 1 : t.showToast({
                            title: "不能再少了哦~",
                            icon: "none"
                        });
                    },
                    handleJiaClick: function() {
                        t.vibrateShort({}), this.lianjiCiShu != this.emitWordTotal - this.limitNum ? this.lianjiCiShu = this.lianjiCiShu + 1 : t.showToast({
                            title: "已达最大次数~",
                            icon: "none"
                        });
                    },
                    handleCountJian: function() {
                        t.vibrateShort({}), 1 != this.duocishuNum ? this.duocishuNum = this.duocishuNum - 1 : t.showToast({
                            title: "不能再少了哦~",
                            icon: "none"
                        });
                    },
                    handleCountJia: function() {
                        t.vibrateShort({}), this.duocishuNum != this.duocishu ? this.duocishuNum = this.duocishuNum + 1 : t.showToast({
                            title: "已达最大次数~",
                            icon: "none"
                        });
                    },
                    handleBaoPay: function() {
                        var i = this;
                        t.showLoading({
                            mask: !0
                        }), this.$http("/renyi/buy_word", "POST", {
                            renyi_id: this.info.id,
                            total: this.duocishuNum
                        }).then(function(n) {
                            t.hideLoading(), setTimeout(function() {
                                i.$playAudio("open"), setTimeout(function() {
                                    i.openSkus = n.data, i.isOpenPopup = !0, i.istry = !1, i.isOpen = !0;
                                }, 500);
                            }, 400);
                        });
                    },
                    goOpenBack: function() {
                        this.isOpenPopup = !1, this.istry = !0, this.isOpen = !1, this.limitCx();
                    },
                    limitCx: function() {
                        var t = this;
                        this.$http("/renyis/".concat(this.uuid, "/limit-cx"), "GET", {}).then(function(i) {
                            t.lianjiCiShu = 1, t.limitCountNum = i.data.info.count, t.limitNum = i.data.info.num, 
                            t.emitTotal = i.data.team.emit_total, t.nowTeamTotal = i.data.team.now_team_total, 
                            t.duocishuNum = i.data.info.use_count, t.duocishu = i.data.info.use_count;
                        });
                    },
                    currentTransition: function(t) {},
                    currentChange: function(t) {
                        var i = t.currentTarget.dataset.current;
                        i !== this.current && (this.current = i, 0 == this.current ? this.movableX = 0 : this.movableX = 146);
                    },
                    currentChange2: function(t) {
                        var i = this, n = t.detail.current;
                        this.$nextTick(function() {
                            i.setSwiperHeight();
                        }), n !== this.current && (this.current = n, 0 == this.current ? this.movableX = 0 : this.movableX = 146);
                    },
                    handleTry: function() {
                        t.showLoading({
                            title: "刷新中..."
                        }), this.initData().then(function(i) {
                            t.hideLoading(), t.showToast({
                                title: "刷新完成~",
                                icon: "none"
                            });
                        });
                    },
                    fetchList: function() {
                        if (this.isLoading) return !1;
                        this.isLoading = !0;
                    },
                    addClick: function() {
                        t.vibrateShort({}), 1 === this.payTotal ? this.payTotal = this.payTotal + 2 : 3 === this.payTotal ? this.payTotal = this.payTotal + 7 : t.showToast({
                            title: "最多十抽哦~",
                            icon: "none"
                        });
                    },
                    minusClick: function() {
                        t.vibrateShort({}), 3 === this.payTotal ? this.payTotal = this.payTotal - 2 : 10 === this.payTotal ? this.payTotal = this.payTotal - 7 : 20 === this.payTotal ? this.payTotal = this.payTotal - 10 : t.showToast({
                            title: "最少一抽哦~",
                            icon: "none"
                        });
                    },
                    useFreeTicket: function() {
                        var i = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/renyi/order/confirm", "POST", {
                            page_uuid: this.pageUuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(n) {
                            t.hideLoading();
                            var e = n.data;
                            e.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : i.paySuccess(e.order);
                        });
                    },
                    getDanmu: function() {},
                    showDetailImagePopup: function(t, i) {
                        t.odds = i, this.skuinfo = t, this.isimagepop = !0;
                    },
                    hideDetailImagePopup: function() {
                        this.$refs.detailPopup.close();
                    },
                    isimagepopfun: function() {
                        this.isimagepop = !1;
                    },
                    refresh: function() {
                        this.$store.dispatch("getUserInfo");
                    },
                    initData: function() {
                        var i = this;
                        return this.$http("/renyis/".concat(this.uuid), "GET", {}).then(function(n) {
                            t.hideLoading(), i.lianjiCiShu = 1, i.info = n.data.info, i.config = n.data.config, 
                            i.info.sku_level = i.info.sku_level.sort().reverse(), i.skusList = n.data.info.skus.filter(function(t) {
                                return 5 === t.level;
                            }), i.emitWordTotal = n.data.info.emit_word_total, i.emitTotal = n.data.info.emit_total, 
                            i.nowTeamTotal = n.data.info.now_team_total, "[]" != JSON.stringify(n.data.limit) && (i.limitCountNum = n.data.limit.count, 
                            i.limitNum = n.data.limit.num, i.duocishu = n.data.limit.use_count, i.duocishuNum = n.data.limit.use_count);
                            var e = 1;
                            i.skus_ultimate = i.skusList[0], setInterval(function() {
                                i.skus_ultimate = i.skusList[e], ++e >= i.skusList.length && (e = 0);
                            }, 3e3), i.getDanmu(), t.setNavigationBarTitle({
                                title: i.info.title
                            });
                        });
                    },
                    paySuccess: function(t) {
                        var i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], n = arguments.length > 2 ? arguments[2] : void 0;
                        this.isTryMode = i, this.order = t, this.is_doubleBoxCard = n, this.isPayPopup = !1, 
                        this.isShowResultPopup = !0, this.refresh(), this.initData();
                    },
                    goBack: function() {
                        this.isShowResultPopup = !1;
                    },
                    hidePayPopup: function() {
                        this.isPayPopup = !1;
                    },
                    pay: function(t) {
                        this.payTotal = t, this.isPayPopup = !0;
                    },
                    getLevelIcon: function(t) {
                        return this.skuLevel.find(function(i) {
                            return i.level === t;
                        }).icon;
                    }
                },
                onPageScroll: function(t) {}
            };
            i.default = a;
        }).call(this, n("543d").default);
    },
    "759c": function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("c04d"), o = n("e6b2");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(i, t, function() {
                return o[t];
            });
        }(u);
        n("0876");
        var s = n("f0c5"), a = Object(s.a)(o.default, e.b, e.c, !1, null, "0edfcd61", null, !1, e.a, void 0);
        i.default = a.exports;
    },
    ab6a: function(t, i, n) {
        "use strict";
        (function(t, i) {
            var e = n("4ea4");
            n("18ba"), e(n("66fd"));
            var o = e(n("759c"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, i(o.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    c04d: function(t, i, n) {
        "use strict";
        n.d(i, "b", function() {
            return o;
        }), n.d(i, "c", function() {
            return u;
        }), n.d(i, "a", function() {
            return e;
        });
        var e = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            },
            uniPopup: function() {
                return n.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(n.bind(null, "0fa2"));
            },
            BoxSkuPopup: function() {
                return n.e("components/BoxSkuPopup/BoxSkuPopup").then(n.bind(null, "f1cb"));
            },
            OpenBoxPopupTheme2: function() {
                return n.e("components/OpenBoxPopupTheme2/OpenBoxPopupTheme2").then(n.bind(null, "e39b"));
            },
            OpenBoxPopup: function() {
                return n.e("components/OpenBoxPopup/OpenBoxPopup").then(n.bind(null, "9fe0"));
            },
            FreeTicketFloatBtn: function() {
                return n.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(n.bind(null, "ef0dd"));
            },
            Danmus: function() {
                return n.e("components/Danmus/Danmus").then(n.bind(null, "4bc1"));
            }
        }, o = function() {
            var t = this, i = (t.$createElement, t._self._c, t.__map(t.info.sku_level, function(i, n) {
                return {
                    $orig: t.__get_orig(i),
                    g0: i.odds.toFixed(2)
                };
            })), n = t.info.team_skus.length, e = t.info.team_skus.length;
            t.$mp.data = Object.assign({}, {
                $root: {
                    l0: i,
                    g1: n,
                    g2: e
                }
            });
        }, u = [];
    },
    e6b2: function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("7341"), o = n.n(e);
        for (var u in e) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(i, t, function() {
                return e[t];
            });
        }(u);
        i.default = o.a;
    }
}, [ [ "ab6a", "common/runtime", "common/vendor" ] ] ]);