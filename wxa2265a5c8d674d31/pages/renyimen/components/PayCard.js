(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/renyimen/components/PayCard" ], {
    "145a": function(o, e, t) {
        "use strict";
        t.d(e, "b", function() {
            return i;
        }), t.d(e, "c", function() {
            return s;
        }), t.d(e, "a", function() {
            return n;
        });
        var n = {
            PriceDisplay: function() {
                return t.e("components/PriceDisplay/PriceDisplay").then(t.bind(null, "6b05"));
            },
            UsableCouponPopup: function() {
                return t.e("components/UsableCouponPopup/UsableCouponPopup").then(t.bind(null, "3858"));
            }
        }, i = function() {
            var o = this, e = (o.$createElement, o._self._c, o.order.coupon_discount ? o.$tool.formatPrice(o.order.coupon_discount) : null), t = o.order.coupon_discount ? null : o.usableCoupons.length, n = !o.order.coupon_discount && t ? o.usableCoupons.length : null, i = o.order.redpack ? o.$tool.formatPrice(o.order.redpack) : null, s = o.order.max_useable_score && o.form.is_use_score ? o._f("priceToFixed")(o.order.score_discount) : null;
            o.$mp.data = Object.assign({}, {
                $root: {
                    g0: e,
                    g1: t,
                    g2: n,
                    g3: i,
                    f0: s
                }
            });
        }, s = [];
    },
    "45da": function(o, e, t) {
        "use strict";
        t.r(e);
        var n = t("64d1"), i = t.n(n);
        for (var s in n) [ "default" ].indexOf(s) < 0 && function(o) {
            t.d(e, o, function() {
                return n[o];
            });
        }(s);
        e.default = i.a;
    },
    "55d2": function(o, e, t) {
        "use strict";
        var n = t("a099");
        t.n(n).a;
    },
    "64d1": function(o, e, t) {
        "use strict";
        (function(o) {
            var n = t("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = n(t("9523")), s = t("26cb"), u = n(t("82c0"));
            function r(o, e) {
                var t = Object.keys(o);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(o);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(o, e).enumerable;
                    })), t.push.apply(t, n);
                }
                return t;
            }
            function a(o) {
                for (var e = 1; e < arguments.length; e++) {
                    var t = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? r(Object(t), !0).forEach(function(e) {
                        (0, i.default)(o, e, t[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(t)) : r(Object(t)).forEach(function(e) {
                        Object.defineProperty(o, e, Object.getOwnPropertyDescriptor(t, e));
                    });
                }
                return o;
            }
            var c = {
                components: {},
                data: function() {
                    return {
                        payTotal: -1,
                        order: {},
                        price: 0,
                        form: {
                            is_use_redpack: 1,
                            is_use_score: 1,
                            is_doubleBoxCard: 0
                        },
                        total_prices: 0,
                        total_score_price: 0,
                        currentCoupon: {},
                        isCouponPopup: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        usableCouponsList: [],
                        unusableCouponsList: [],
                        isInit: !1,
                        isLoading: !1,
                        isSubmiting: !1,
                        isCheckUserStatement: !0,
                        doubleBoxCard: {},
                        count: 10,
                        buzaiBool: !1,
                        pingtaiBool: !1,
                        fahuoBoll: !1,
                        fahuoCurrent: 0,
                        fahuoList: [ "发货须知", "售后须知" ]
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: a(a({}, (0, s.mapGetters)([ "userInfo" ])), {}, {
                    isuserInfo: function() {
                        console.log(this.userInfo);
                    }
                }),
                watch: {
                    payTotal: function() {
                        this.initOrder();
                    }
                },
                created: function() {
                    this.payTotal = this.info.pay_total, console.log("info", this.info);
                    var e = this;
                    o.getStorage({
                        key: "fudai_scoreStorage",
                        success: function(o) {
                            e.form.is_use_score = o.data ? 1 : 0, setTimeout(function() {
                                e.initOrder();
                            }, 100);
                        }
                    }), o.getStorage({
                        key: "isCheckUserStatementBool",
                        success: function(o) {
                            e.isCheckUserStatement = o.data;
                        }
                    }), o.getStorage({
                        key: "isBuzaiBool",
                        success: function(o) {
                            e.buzaiBool = o.data;
                        }
                    });
                },
                methods: {
                    handleFaHuoClick: function() {
                        console.log("11"), this.fahuoBoll = !0;
                    },
                    handleHuoClose: function() {
                        this.fahuoBoll = !1;
                    },
                    handleHuoClick: function(o) {
                        o !== this.fahuoCurrent && (this.fahuoCurrent = o);
                    },
                    uncheck: function() {
                        this.isCheckUserStatement = !this.isCheckUserStatement;
                    },
                    handleDesClick: function() {
                        this.buzaiBool = !this.buzaiBool, o.setStorage({
                            key: "isBuzaiBool",
                            data: !0,
                            success: function() {}
                        });
                    },
                    handleTuiClick: function() {
                        this.pingtaiBool = !1;
                    },
                    handleXieyiClick: function() {
                        o.navigateTo({
                            url: "/pages/rule/userStatement"
                        });
                    },
                    load: function() {
                        this.count += 10, console.log(this.count), this.showPopup();
                    },
                    showPopup: function() {
                        for (var o = [], e = [], t = 0; t < this.count; t++) void 0 !== this.usableCoupons[t] && o.push(this.usableCoupons[t]), 
                        void 0 !== this.unusableCoupons[t] && e.push(this.unusableCoupons[t]);
                        this.usableCouponsList = o.filter(function(o, e, t) {
                            return t.findIndex(function(e) {
                                return e.id === o.id;
                            }) === e;
                        }), this.unusableCouponsList = e.filter(function(o, e, t) {
                            return t.findIndex(function(e) {
                                return e.id === o.id;
                            }) === e;
                        }), this.isCouponPopup = !0;
                    },
                    hidePopup: function() {
                        this.count = 10, this.usableCouponsList = [], this.unusableCouponsList = [], this.isCouponPopup = !1;
                    },
                    couponChange: function(o) {
                        o.id === this.currentCoupon.id || (this.currentCoupon = o, this.initOrder());
                    },
                    initOrder: function() {
                        var e = this;
                        o.showLoading(), this.$http("/renyi/order/preview", "POST", a({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(t) {
                            e.isInit = !0, e.order = t.data.order, e.unusableCoupons = t.data.order.coupons.unusable, 
                            e.usableCoupons = t.data.order.coupons.usable, console.log("order=>", t.data.order), 
                            o.hideLoading();
                        }).catch(function(o) {
                            e.isInit = !1, e.cancel();
                        }), this.$http("/user/cards").then(function(o) {
                            e.doubleBoxCard = o.data.double_box_card, console.log(o.data.double_box_card);
                        });
                    },
                    switchChange: function(o) {
                        null == o.detail.value[0] ? this.form.is_use_redpack = 0 : this.form.is_use_redpack = 1, 
                        this.initOrder();
                    },
                    scoreSwitchChange: function(e) {
                        console.log(e), this.form.is_use_score = !(e.detail.value.length > 1), null == e.detail.value[0] ? (this.form.is_use_score = 0, 
                        o.setStorage({
                            key: "fudai_scoreStorage",
                            data: !1,
                            success: function() {
                                console.log("success");
                            }
                        })) : (this.form.is_use_score = 1, o.setStorage({
                            key: "fudai_scoreStorage",
                            data: !0,
                            success: function() {
                                console.log("success");
                            }
                        })), console.log(this.form.is_use_score), this.initOrder();
                    },
                    doubleBoxCardswitchChange: function(o) {
                        1 === this.form.is_doubleBoxCard ? this.form.is_doubleBoxCard = 0 : this.form.is_doubleBoxCard = 1, 
                        this.initOrder();
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    createOrder: function() {
                        var e = this;
                        if (this.isLoading) return !1;
                        this.isLoading = !0, o.showLoading({
                            title: "提交中"
                        }), this.$http("/renyi/order/confirm", "POST", a({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(t) {
                            o.hideLoading(), e.isSubmiting = !1;
                            var n = t.data;
                            console.log(n), n.is_need_pay ? u.default.pay(a(a({}, n), {}, {
                                success: function() {
                                    e.$emit("success", n.order, !1, n.is_doubleBoxCard);
                                },
                                fail: function() {
                                    o.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.$http("/orders/".concat(n.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : e.$emit("success", n.order, !1, n.is_doubleBoxCard);
                        }).catch(function(o) {
                            e.isLoading = !1;
                        });
                    },
                    handleBtnClick: function() {
                        if (!this.isCheckUserStatement) return o.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1;
                    },
                    handleJixuClick: function() {
                        if (this.pingtaiBool = !1, this.isLoading) return !1;
                        this.createOrder();
                    },
                    submit: function() {
                        if (o.setStorage({
                            key: "isCheckUserStatementBool",
                            data: !0,
                            success: function() {}
                        }), 0 == this.buzaiBool) this.pingtaiBool = !0; else {
                            if (this.pingtaiBool = !1, this.isLoading) return !1;
                            this.createOrder();
                        }
                    }
                },
                onPageScroll: function(o) {}
            };
            e.default = c;
        }).call(this, t("543d").default);
    },
    a099: function(o, e, t) {},
    dbde: function(o, e, t) {
        "use strict";
        t.r(e);
        var n = t("145a"), i = t("45da");
        for (var s in i) [ "default" ].indexOf(s) < 0 && function(o) {
            t.d(e, o, function() {
                return i[o];
            });
        }(s);
        t("55d2");
        var u = t("f0c5"), r = Object(u.a)(i.default, n.b, n.c, !1, null, "6ac78b64", null, !1, n.a, void 0);
        e.default = r.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/renyimen/components/PayCard-create-component", {
    "pages/renyimen/components/PayCard-create-component": function(o, e, t) {
        t("543d").createComponent(t("dbde"));
    }
}, [ [ "pages/renyimen/components/PayCard-create-component" ] ] ]);