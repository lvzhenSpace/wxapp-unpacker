(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/renyimen/components/OpenBoxPopup" ], {
    "4cde": function(e, t, n) {
        "use strict";
        var o = n("af9a");
        n.n(o).a;
    },
    "8bf9": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, r = [];
    },
    a141: function(e, t, n) {
        "use strict";
        (function(e) {
            function n(e, t) {
                var n = "undefined" != typeof Symbol && e[Symbol.iterator] || e["@@iterator"];
                if (!n) {
                    if (Array.isArray(e) || (n = function(e, t) {
                        if (e) {
                            if ("string" == typeof e) return o(e, t);
                            var n = Object.prototype.toString.call(e).slice(8, -1);
                            return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(e) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? o(e, t) : void 0;
                        }
                    }(e)) || t && e && "number" == typeof e.length) {
                        n && (e = n);
                        var r = 0, i = function() {};
                        return {
                            s: i,
                            n: function() {
                                return r >= e.length ? {
                                    done: !0
                                } : {
                                    done: !1,
                                    value: e[r++]
                                };
                            },
                            e: function(e) {
                                throw e;
                            },
                            f: i
                        };
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }
                var a, s = !0, c = !1;
                return {
                    s: function() {
                        n = n.call(e);
                    },
                    n: function() {
                        var e = n.next();
                        return s = e.done, e;
                    },
                    e: function(e) {
                        c = !0, a = e;
                    },
                    f: function() {
                        try {
                            s || null == n.return || n.return();
                        } finally {
                            if (c) throw a;
                        }
                    }
                };
            }
            function o(e, t) {
                (null == t || t > e.length) && (t = e.length);
                for (var n = 0, o = new Array(t); n < t; n++) o[n] = e[n];
                return o;
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = {
                props: {
                    boxImg: String,
                    buttonTitle: String,
                    order: Object,
                    tryMode: Boolean,
                    tryInfo: Object,
                    info: Object,
                    isNavbarEnable: !1,
                    istry: Boolean,
                    payTotal: Number,
                    skus: Array,
                    isOpen: Boolean
                },
                data: function() {
                    return {
                        dataMsg: null,
                        showResult: !0,
                        status: 0,
                        isShowReturnSale: !1,
                        isReturnSaleSuccess: !1,
                        package: {},
                        defaultBoxImage: "https://cdn2.hquesoft.com/box/openbox.png",
                        isNotOpen: !1,
                        selectedIds: [],
                        total_money_price: 0,
                        total_score_price: 0,
                        total_score_score: 0,
                        tryskus: [],
                        myInfo: {}
                    };
                },
                mounted: function() {
                    this.initData(), this.getMyInfo();
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    },
                    rewardJikaTimes: function() {
                        return this.package.reward && this.package.reward.jika_times;
                    },
                    rewardLotteryTicket: function() {
                        return this.package.reward && this.package.reward.lottery_ticket;
                    }
                },
                methods: {
                    getMyInfo: function() {
                        var e = this;
                        this.$http("/task/level_list?no_list=1").then(function(t) {
                            e.myInfo = t.data.my_task_level_info;
                        }).catch(function(e) {
                            console.log(e);
                        });
                    },
                    viewFreeOrderDetail: function() {
                        this.info.money_price ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : e.navigateTo({
                            url: "/pages/myScore/index"
                        });
                    },
                    randomSort: function(e, t) {
                        return Math.random() > .5 ? -1 : 1;
                    },
                    initData: function() {
                        var e, t = n(this.skus);
                        try {
                            for (t.s(); !(e = t.n()).done; ) {
                                var o = e.value;
                                this.total_score_score += o.fj_score;
                            }
                        } catch (e) {
                            t.e(e);
                        } finally {
                            t.f();
                        }
                    },
                    lockInfo: function() {
                        this.$http("/yifanshang/rooms-lock-info", "GET", {
                            room_id: this.info.room.id
                        }).then(function(e) {});
                    },
                    onekeyrecycle_try: function() {
                        e.redirectTo({
                            url: "/pages/page/index?uuid=635180d9968bf"
                        });
                    },
                    onekeyrecycle: function() {
                        var t = this;
                        e.showLoading({
                            title: "分解中...",
                            success: function(o) {
                                var r, i = n(t.skus);
                                try {
                                    for (i.s(); !(r = i.n()).done; ) {
                                        var a = r.value;
                                        t.selectedIds.push(a.id);
                                    }
                                } catch (e) {
                                    i.e(e);
                                } finally {
                                    i.f();
                                }
                                t.$http("/asset/return-sale/confirm", "post", {
                                    ids: t.selectedIds
                                }).then(function(n) {
                                    t.isReturnSaleSuccess = 0, e.showToast({
                                        title: "分解成功"
                                    }), t.refresh(), t.isSelectMode = !1, t.$emit("refresh");
                                });
                            }
                        }), this.close();
                    },
                    startOpenAnimate: function() {
                        var e = this;
                        setTimeout(function() {
                            e.status = 1, setTimeout(function() {
                                e.isOpen = !0, e.$playAudio("open"), setTimeout(function() {
                                    e.showResult = !0;
                                }, 300);
                            }, 200);
                        }, 100);
                    },
                    handleRefresh: function() {
                        this.isNotOpen = !1, this.initData();
                    },
                    goLotteryDetail: function() {
                        e.navigateTo({
                            url: "/pages/lottery/detail?uuid=" + this.rewardLotteryTicket.uuid
                        });
                    },
                    goJikaDetail: function() {
                        e.navigateTo({
                            url: "/pages/jika/detail?uuid=" + this.rewardJikaTimes.uuid
                        });
                    },
                    returnSale: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    handleOk: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    goBack: function() {
                        e.navigateBack({
                            delta: 1
                        });
                    },
                    close: function() {
                        this.$emit("close"), this.$emit("refresh");
                    },
                    handleGuanBiClick: function() {
                        this.$emit("close"), this.$emit("refresh");
                    },
                    checkSku: function(t) {
                        "score" === t.sku_type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "coupon" === t.sku_type ? e.navigateTo({
                            url: "/pages/myCoupons/index"
                        }) : "redpack" === t.sku_type && e.navigateTo({
                            url: "/pages/myRedpack/index"
                        });
                    }
                }
            };
            t.default = r;
        }).call(this, n("543d").default);
    },
    a929: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("8bf9"), r = n("ec4a");
        for (var i in r) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(i);
        n("4cde");
        var a = n("f0c5"), s = Object(a.a)(r.default, o.b, o.c, !1, null, "52f2abe2", null, !1, o.a, void 0);
        t.default = s.exports;
    },
    af9a: function(e, t, n) {},
    ec4a: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("a141"), r = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        t.default = r.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/renyimen/components/OpenBoxPopup-create-component", {
    "pages/renyimen/components/OpenBoxPopup-create-component": function(e, t, n) {
        n("543d").createComponent(n("a929"));
    }
}, [ [ "pages/renyimen/components/OpenBoxPopup-create-component" ] ] ]);