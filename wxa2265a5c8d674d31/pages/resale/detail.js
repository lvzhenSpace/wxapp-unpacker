(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/resale/detail" ], {
    3616: function(t, e, n) {
        "use strict";
        (function(t) {
            var i = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var o = i(n("9523")), a = i(n("82c0"));
            function r(t, e) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(t);
                    e && (i = i.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable;
                    })), n.push.apply(n, i);
                }
                return n;
            }
            function s(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var n = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? r(Object(n), !0).forEach(function(e) {
                        (0, o.default)(t, e, n[e]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(n, e));
                    });
                }
                return t;
            }
            var c = {
                components: {},
                data: function() {
                    return {
                        uuid: "",
                        info: {
                            user: {}
                        },
                        user: {}
                    };
                },
                filters: {},
                computed: {
                    skus: function() {
                        return this.info.package_skus || [];
                    },
                    sku: function() {
                        return this.info.package_skus && this.info.package_skus[0] || {};
                    },
                    share: function() {
                        return {
                            title: this.info.user.name + "向你转售了一个商品，快来看看吧~"
                        };
                    },
                    detailImages: function() {
                        return this.sku.detail_images;
                    }
                },
                onLoad: function(t) {
                    this.uuid = t.uuid;
                },
                onShow: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        var t = this;
                        this.$http("/asset/resales/".concat(this.uuid)).then(function(e) {
                            t.info = e.data.info, t.user = e.data.info.user;
                        });
                    },
                    buy: function() {
                        var e = this;
                        t.showLoading({
                            title: "购买中"
                        }), this.$http("/asset/resale-order/confirm", "POST", {
                            resale_id: this.info.id
                        }).then(function(n) {
                            t.hideLoading();
                            var i = n.data;
                            n.data.is_need_pay ? a.default.pay(s(s({}, i), {}, {
                                success: function() {
                                    e.initData(), t.showToast({
                                        title: "购买成功，即将跳转~",
                                        icon: "none"
                                    }), setTimeout(function(e) {
                                        t.redirectTo({
                                            url: "/pages/myBox/index"
                                        });
                                    }, 1500);
                                },
                                fail: function() {
                                    t.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.$http("/orders/".concat(i.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : (t.showToast({
                                title: "购买成功~",
                                icon: "none"
                            }), e.initData());
                        });
                    },
                    cancel: function() {
                        var e = this;
                        t.showModal({
                            title: "提示",
                            content: "确认要取消此转售吗?",
                            success: function(n) {
                                n.confirm && (t.showLoading({
                                    title: "取消中"
                                }), e.$http("/asset/resales/".concat(e.uuid), "PUT", {
                                    type: "close"
                                }).then(function(n) {
                                    t.hideLoading(), t.showToast({
                                        title: "取消成功~",
                                        icon: "none"
                                    }), e.initData();
                                }));
                            }
                        });
                    },
                    toResaleIndex: function() {
                        t.navigateTo({
                            url: "/pages/resale/index"
                        });
                    }
                }
            };
            e.default = c;
        }).call(this, n("543d").default);
    },
    "519f": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            }
        }, o = function() {
            this.$createElement;
            var t = (this._self._c, this.info.id ? this.$tool.showShortTime(this.info.created_at) : null), e = this.info.id ? this.skus.length : null;
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    g1: e
                }
            });
        }, a = [];
    },
    7884: function(t, e, n) {
        "use strict";
        var i = n("9cbe");
        n.n(i).a;
    },
    "81d5": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("519f"), o = n("f671");
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        n("ea02"), n("7884");
        var r = n("f0c5"), s = Object(r.a)(o.default, i.b, i.c, !1, null, "cba957aa", null, !1, i.a, void 0);
        e.default = s.exports;
    },
    "9c32": function(t, e, n) {
        "use strict";
        (function(t, e) {
            var i = n("4ea4");
            n("18ba"), i(n("66fd"));
            var o = i(n("81d5"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(o.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "9cbe": function(t, e, n) {},
    "9dc4": function(t, e, n) {},
    ea02: function(t, e, n) {
        "use strict";
        var i = n("9dc4");
        n.n(i).a;
    },
    f671: function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("3616"), o = n.n(i);
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        e.default = o.a;
    }
}, [ [ "9c32", "common/runtime", "common/vendor" ] ] ]);