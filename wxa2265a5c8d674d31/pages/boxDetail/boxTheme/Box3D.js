(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/boxDetail/boxTheme/Box3D" ], {
    "272c": function(t, o, e) {},
    "3bd1": function(t, o, e) {
        "use strict";
        e.r(o);
        var n = e("93b7"), i = e.n(n);
        for (var c in n) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(o, t, function() {
                return n[t];
            });
        }(c);
        o.default = i.a;
    },
    "798f": function(t, o, e) {
        "use strict";
        e.r(o);
        var n = e("a386"), i = e("3bd1");
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(o, t, function() {
                return i[t];
            });
        }(c);
        e("99db");
        var s = e("f0c5"), a = Object(s.a)(i.default, n.b, n.c, !1, null, "a4bcb8da", null, !1, n.a, void 0);
        o.default = a.exports;
    },
    "93b7": function(t, o, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var e = {
                props: {
                    room: {
                        type: Object
                    },
                    info: {
                        type: Object
                    }
                },
                data: function() {
                    return {
                        selected: -1,
                        isShowClickTips: !1
                    };
                },
                created: function() {
                    this.isShowClickTips = !t.getStorageSync("disableShowClickTips");
                },
                computed: {
                    config: function() {
                        return this.$store.getters.setting.box_room;
                    },
                    boxBottomImage: function() {
                        return this.info.big_box_bottom || this.config.box_bottom || "https://cdn2.hquesoft.com/box/bigBox/3d-bg.png";
                    },
                    boxTopImage: function() {
                        return this.info.big_box_top || this.config.box_up || "https://cdn2.hquesoft.com/box/bigBox/3d-bg-up.png";
                    },
                    smallBoxImage: function() {
                        return this.info.small_box_image || this.config.small_box || "https://cdn2.hquesoft.com/box/bigBox/small-box.png";
                    }
                },
                methods: {
                    selectItem: function(o) {
                        this.isShowClickTips && (this.isShowClickTips = !1, t.setStorageSync("disableShowClickTips", !0)), 
                        this.selected !== o ? (this.selected = o, this.$playAudio("check")) : this.$emit("select", o);
                    }
                }
            };
            o.default = e;
        }).call(this, e("543d").default);
    },
    "99db": function(t, o, e) {
        "use strict";
        var n = e("272c");
        e.n(n).a;
    },
    a386: function(t, o, e) {
        "use strict";
        e.d(o, "b", function() {
            return n;
        }), e.d(o, "c", function() {
            return i;
        }), e.d(o, "a", function() {});
        var n = function() {
            this.$createElement;
            var t = (this._self._c, this.room.sku_status.length);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/boxDetail/boxTheme/Box3D-create-component", {
    "pages/boxDetail/boxTheme/Box3D-create-component": function(t, o, e) {
        e("543d").createComponent(e("798f"));
    }
}, [ [ "pages/boxDetail/boxTheme/Box3D-create-component" ] ] ]);