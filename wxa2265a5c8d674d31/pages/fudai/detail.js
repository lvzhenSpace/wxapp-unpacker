(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/fudai/detail" ], {
    "0ad4": function(t, i, n) {
        "use strict";
        var e = n("7f23");
        n.n(e).a;
    },
    "1e93": function(t, i, n) {
        "use strict";
        (function(t) {
            var e = n("4ea4");
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var o = e(n("9523"));
            function u(t, i) {
                var n = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var e = Object.getOwnPropertySymbols(t);
                    i && (e = e.filter(function(i) {
                        return Object.getOwnPropertyDescriptor(t, i).enumerable;
                    })), n.push.apply(n, e);
                }
                return n;
            }
            function s(t) {
                for (var i = 1; i < arguments.length; i++) {
                    var n = null != arguments[i] ? arguments[i] : {};
                    i % 2 ? u(Object(n), !0).forEach(function(i) {
                        (0, o.default)(t, i, n[i]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(n)) : u(Object(n)).forEach(function(i) {
                        Object.defineProperty(t, i, Object.getOwnPropertyDescriptor(n, i));
                    });
                }
                return t;
            }
            var a = {
                components: {
                    PayCard: function() {
                        Promise.all([ n.e("common/vendor"), n.e("pages/fudai/components/PayCard") ]).then(function() {
                            return resolve(n("4a6a"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    RecordList: function() {
                        n.e("pages/fudai/components/RecordList").then(function() {
                            return resolve(n("503f"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    imagepop: function() {
                        n.e("pages/fudai/components/imagepop").then(function() {
                            return resolve(n("7a28"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        bgImgs: [ "url(https://img121.7dun.com/yuanqimali/fudai/rBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/srBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/ssrBg.png)", "url(https://img121.7dun.com/yuanqimali/fudai/urBg.png)", "url()" ],
                        uuid: "",
                        isInit: !1,
                        isPayPopup: !1,
                        payTotal: 1,
                        info: {
                            min_lucky_score: ""
                        },
                        order: {},
                        pageUuid: "",
                        isNotFound: !1,
                        isShowResultPopup: !1,
                        danmuSetting: {},
                        danmuList: [],
                        config: {},
                        detailImageList: [],
                        levelFilter: 0,
                        isTryMode: !1,
                        isShowRankList: !1,
                        wining_list: [],
                        isimagepop: !1,
                        is_doubleBoxCard: 0,
                        skuinfo: {},
                        skusList: [],
                        skus_ultimate: {},
                        color_list: [ {
                            level: 0,
                            color: "#00aa00"
                        }, {
                            level: 1,
                            color: "#aa55ff"
                        }, {
                            level: 2,
                            color: "#ffff00"
                        }, {
                            level: 3,
                            color: "#ffff00"
                        }, {
                            level: 4,
                            color: "#ff0000"
                        } ]
                    };
                },
                computed: s(s({}, (0, n("26cb").mapGetters)([ "userInfo" ])), {}, {
                    bgImage: function() {
                        return "https://cdn2.hquesoft.com/box/fudai/detail_bg.png";
                    },
                    boxImage: function() {
                        return this.info.image_3d || "https://cdn2.hquesoft.com/box/fudai/box.png";
                    },
                    skuLevel: function() {
                        return this.info.sku_level || [];
                    },
                    skus: function() {
                        var t = this;
                        return this.levelFilter ? this.info.skus.filter(function(i) {
                            return i.level === t.levelFilter;
                        }) : this.info.skus || [];
                    },
                    payInfo: function() {
                        return {
                            page_uuid: this.pageUuid,
                            title: this.info.title,
                            pay_total: this.payTotal,
                            total_list: this.info.total_list,
                            money_price: this.info.money_price,
                            score_price: this.info.score_price
                        };
                    },
                    titleBgColor: function() {
                        return " ";
                    },
                    titleColor: function() {
                        return "#ffffff";
                    },
                    setTitleText: function() {
                        t.setNavigationBarTitle({
                            title: this.info.title
                        });
                    },
                    discountTips: function() {
                        for (var t = this.info.total_list || [], i = t.length - 1; i >= 0; i--) if (t[i].is_discount) {
                            var n = t[i].total + "连开优惠";
                            return t[i].money_discount && (n += t[i].money_discount / 100 + "元"), t[i].score_discount && (n += t[i].score_discount + this.scoreAlias), 
                            n;
                        }
                        return !1;
                    },
                    width_value: function() {
                        return this.userInfo.lucky_score / this.info.min_lucky_score * 100 > 100 ? 100 : this.userInfo.lucky_score / this.info.min_lucky_score * 100 < 0 ? 0 : Math.trunc(this.userInfo.lucky_score / this.info.min_lucky_score * 100);
                    }
                }),
                watch: {},
                filters: {},
                onLoad: function(i) {
                    this.uuid = i.uuid, t.loadFontFace({
                        family: "BigjianNew",
                        source: 'url("'.concat("https://img121.7dun.com/member_grade/BigjianNew.TTF", '")'),
                        success: function() {},
                        fail: function(t) {}
                    });
                },
                onUnload: function() {},
                created: function() {},
                onPullDownRefresh: function() {
                    this.$showPullRefresh(), this.initData(), this.setTitleText();
                },
                onShow: function() {
                    var i = this;
                    t.showLoading({
                        title: "加载中"
                    }), this.initData().then(function(n) {
                        i.isInit = !0, t.hideLoading(), i.$http("/fudai/change", "POST", {
                            fudai_id: i.info.id
                        }).then(function(t) {
                            i.pageUuid = t.data.page_uuid, i.fetchList();
                        });
                    }).catch(function(t) {
                        i.isNotFound = !0;
                    });
                },
                onReachBottom: function() {},
                methods: {
                    handleTry: function() {
                        t.showLoading({
                            title: "刷新中..."
                        }), this.initData().then(function(i) {
                            t.hideLoading(), t.showToast({
                                title: "刷新完成~",
                                icon: "none"
                            });
                        });
                    },
                    fetchList: function() {
                        var t = this;
                        return !this.isLoading && (this.isLoading = !0, this.$http("/fudais/".concat(this.info.uuid, "/records"), "GET", {
                            page: 1,
                            per_page: 100
                        }).then(function(i) {
                            t.isInit = !0;
                            for (var n = 0; n < i.data.list.length; n++) t.wining_list.length < 12 && 5 == i.data.list[n].level && t.wining_list.push(i.data.list[n]);
                        }).catch(function(i) {
                            t.isInit = !1;
                        }));
                    },
                    setLevelFilter: function(t) {
                        this.levelFilter === t ? this.levelFilter = 0 : this.levelFilter = t;
                    },
                    addClick: function() {
                        t.vibrateShort({}), 1 === this.payTotal ? this.payTotal = this.payTotal + 2 : 3 === this.payTotal ? this.payTotal = this.payTotal + 7 : t.showToast({
                            title: "最多十抽哦~",
                            icon: "none"
                        });
                    },
                    minusClick: function() {
                        t.vibrateShort({}), 3 === this.payTotal ? this.payTotal = this.payTotal - 2 : 10 === this.payTotal ? this.payTotal = this.payTotal - 7 : 20 === this.payTotal ? this.payTotal = this.payTotal - 10 : t.showToast({
                            title: "最少一抽哦~",
                            icon: "none"
                        });
                    },
                    gomybox: function() {
                        t.reLaunch({
                            url: "/pages/myBox/index"
                        });
                    },
                    useFreeTicket: function() {
                        var i = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/fudai/order/confirm", "POST", {
                            page_uuid: this.pageUuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(n) {
                            t.hideLoading();
                            var e = n.data;
                            e.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : i.paySuccess(e.order);
                        });
                    },
                    getDanmu: function() {
                        var t = this;
                        this.$http("/danmus/fudai_detail?node_id=".concat(this.info.id)).then(function(i) {
                            t.danmuSetting = i.data.setting, t.danmuList = i.data.list;
                        });
                    },
                    showDetailImagePopup: function(t, i) {
                        t.odds = i, this.skuinfo = t, this.isimagepop = !0;
                    },
                    hideDetailImagePopup: function() {
                        this.$refs.detailPopup.close();
                    },
                    isimagepopfun: function() {
                        this.isimagepop = !1;
                    },
                    refresh: function() {
                        this.$store.dispatch("getUserInfo");
                    },
                    initData: function() {
                        var i = this;
                        return this.$http("/fudais/".concat(this.uuid), "GET", {}).then(function(n) {
                            t.hideLoading(), i.info = n.data.info, i.config = n.data.config, i.info.sku_level = i.info.sku_level.sort().reverse(), 
                            i.skusList = n.data.info.skus.filter(function(t) {
                                return 5 === t.level;
                            });
                            var e = 1;
                            i.skus_ultimate = i.skusList[0], setInterval(function() {
                                i.skus_ultimate = i.skusList[e], ++e >= i.skusList.length && (e = 0);
                            }, 3e3), i.getDanmu(), t.setNavigationBarTitle({
                                title: i.info.title
                            });
                        });
                    },
                    paySuccess: function(t) {
                        var i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1], n = arguments.length > 2 ? arguments[2] : void 0;
                        this.isTryMode = i, this.order = t, this.is_doubleBoxCard = n, this.isPayPopup = !1, 
                        this.isShowResultPopup = !0, this.refresh();
                    },
                    goBack: function() {
                        this.isShowResultPopup = !1;
                    },
                    hidePayPopup: function() {
                        this.isPayPopup = !1;
                    },
                    pay: function(t) {
                        this.payTotal = t, this.isPayPopup = !0;
                    },
                    getLevelIcon: function(t) {
                        return this.skuLevel.find(function(i) {
                            return i.level === t;
                        }).icon;
                    },
                    getLevelTitle: function(t) {
                        return this.skuLevel.find(function(i) {
                            return i.level === t;
                        }).title;
                    }
                },
                onPageScroll: function(t) {}
            };
            i.default = a;
        }).call(this, n("543d").default);
    },
    "1fd6": function(t, i, n) {
        "use strict";
        (function(t, i) {
            var e = n("4ea4");
            n("18ba"), e(n("66fd"));
            var o = e(n("ccc7"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, i(o.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "2aca": function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("1e93"), o = n.n(e);
        for (var u in e) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(i, t, function() {
                return e[t];
            });
        }(u);
        i.default = o.a;
    },
    "760a": function(t, i, n) {
        "use strict";
        n.d(i, "b", function() {
            return o;
        }), n.d(i, "c", function() {
            return u;
        }), n.d(i, "a", function() {
            return e;
        });
        var e = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            },
            uniPopup: function() {
                return n.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(n.bind(null, "0fa2"));
            },
            BoxSkuPopup: function() {
                return n.e("components/BoxSkuPopup/BoxSkuPopup").then(n.bind(null, "f1cb"));
            },
            OpenBoxPopupTheme2: function() {
                return n.e("components/OpenBoxPopupTheme2/OpenBoxPopupTheme2").then(n.bind(null, "e39b"));
            },
            FreeTicketFloatBtn: function() {
                return n.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(n.bind(null, "ef0dd"));
            },
            Danmus: function() {
                return n.e("components/Danmus/Danmus").then(n.bind(null, "4bc1"));
            }
        }, o = function() {
            var t = this, i = (t.$createElement, t._self._c, t.__map(t.info.sku_level, function(i, n) {
                return {
                    $orig: t.__get_orig(i),
                    g0: i.odds.toFixed(2)
                };
            }));
            t._isMounted || (t.e0 = function(i) {
                t.isShowRankList = !0;
            }, t.e1 = function(i) {
                t.isShowRankList = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l0: i
                }
            });
        }, u = [];
    },
    "7f23": function(t, i, n) {},
    ccc7: function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("760a"), o = n("2aca");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            n.d(i, t, function() {
                return o[t];
            });
        }(u);
        n("0ad4");
        var s = n("f0c5"), a = Object(s.a)(o.default, e.b, e.c, !1, null, "20bda2e6", null, !1, e.a, void 0);
        i.default = a.exports;
    }
}, [ [ "1fd6", "common/runtime", "common/vendor" ] ] ]);