(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/fudai/components/PayCard" ], {
    "0bad": function(o, t, e) {},
    "3c59": function(o, t, e) {
        "use strict";
        (function(o) {
            var n = e("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = n(e("9523")), s = e("26cb"), u = n(e("82c0"));
            function r(o, t) {
                var e = Object.keys(o);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(o);
                    t && (n = n.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(o, t).enumerable;
                    })), e.push.apply(e, n);
                }
                return e;
            }
            function a(o) {
                for (var t = 1; t < arguments.length; t++) {
                    var e = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? r(Object(e), !0).forEach(function(t) {
                        (0, i.default)(o, t, e[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(e)) : r(Object(e)).forEach(function(t) {
                        Object.defineProperty(o, t, Object.getOwnPropertyDescriptor(e, t));
                    });
                }
                return o;
            }
            var c = {
                components: {},
                data: function() {
                    return {
                        payTotal: -1,
                        order: {},
                        price: 0,
                        form: {
                            is_use_redpack: 1,
                            is_use_score: 1,
                            is_doubleBoxCard: 0
                        },
                        total_prices: 0,
                        total_score_price: 0,
                        currentCoupon: {},
                        isCouponPopup: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        usableCouponsList: [],
                        unusableCouponsList: [],
                        isInit: !1,
                        isLoading: !1,
                        isSubmiting: !1,
                        isCheckUserStatement: !0,
                        doubleBoxCard: {},
                        count: 10,
                        buzaiBool: !1,
                        pingtaiBool: !1,
                        fahuoBoll: !1,
                        fahuoCurrent: 0,
                        fahuoList: [ "发货须知", "售后须知" ]
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: a(a({}, (0, s.mapGetters)([ "userInfo" ])), {}, {
                    isuserInfo: function() {}
                }),
                watch: {
                    payTotal: function() {
                        this.initOrder();
                    }
                },
                created: function() {
                    this.payTotal = this.info.pay_total;
                    var t = this;
                    o.getStorage({
                        key: "fudai_scoreStorage",
                        success: function(o) {
                            t.form.is_use_score = o.data ? 1 : 0, setTimeout(function() {
                                t.initOrder();
                            }, 100);
                        }
                    }), o.getStorage({
                        key: "isCheckUserStatementBool",
                        success: function(o) {
                            t.isCheckUserStatement = o.data;
                        }
                    }), o.getStorage({
                        key: "isBuzaiBool",
                        success: function(o) {
                            t.buzaiBool = o.data;
                        }
                    });
                },
                methods: {
                    handleFaHuoClick: function() {
                        console.log("11"), this.fahuoBoll = !0;
                    },
                    handleHuoClose: function() {
                        this.fahuoBoll = !1;
                    },
                    handleHuoClick: function(o) {
                        o !== this.fahuoCurrent && (this.fahuoCurrent = o);
                    },
                    uncheck: function() {
                        this.isCheckUserStatement = !this.isCheckUserStatement;
                    },
                    handleDesClick: function() {
                        this.buzaiBool = !this.buzaiBool, o.setStorage({
                            key: "isBuzaiBool",
                            data: !0,
                            success: function() {}
                        });
                    },
                    handleTuiClick: function() {
                        this.pingtaiBool = !1;
                    },
                    handleXieyiClick: function() {
                        o.navigateTo({
                            url: "/pages/rule/userStatement"
                        });
                    },
                    load: function() {
                        this.count += 10, this.showPopup();
                    },
                    showPopup: function() {
                        for (var o = [], t = [], e = 0; e < this.count; e++) void 0 !== this.usableCoupons[e] && o.push(this.usableCoupons[e]), 
                        void 0 !== this.unusableCoupons[e] && t.push(this.unusableCoupons[e]);
                        this.usableCouponsList = o.filter(function(o, t, e) {
                            return e.findIndex(function(t) {
                                return t.id === o.id;
                            }) === t;
                        }), this.unusableCouponsList = t.filter(function(o, t, e) {
                            return e.findIndex(function(t) {
                                return t.id === o.id;
                            }) === t;
                        }), this.isCouponPopup = !0;
                    },
                    hidePopup: function() {
                        this.count = 10, this.usableCouponsList = [], this.unusableCouponsList = [], this.isCouponPopup = !1;
                    },
                    couponChange: function(o) {
                        o.id === this.currentCoupon.id || (this.currentCoupon = o, this.initOrder());
                    },
                    initOrder: function() {
                        var t = this;
                        o.showLoading(), this.$http("/fudai/order/preview", "POST", a({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(e) {
                            t.isInit = !0, t.order = e.data.order, t.unusableCoupons = e.data.order.coupons.unusable, 
                            t.usableCoupons = e.data.order.coupons.usable, o.hideLoading();
                        }).catch(function(o) {
                            t.isInit = !1, t.cancel();
                        }), this.$http("/user/cards").then(function(o) {
                            t.doubleBoxCard = o.data.double_box_card;
                        });
                    },
                    switchChange: function(o) {
                        null == o.detail.value[0] ? this.form.is_use_redpack = 0 : this.form.is_use_redpack = 1, 
                        this.initOrder();
                    },
                    scoreSwitchChange: function(t) {
                        this.form.is_use_score = !(t.detail.value.length > 1), null == t.detail.value[0] ? (this.form.is_use_score = 0, 
                        o.setStorage({
                            key: "fudai_scoreStorage",
                            data: !1,
                            success: function() {
                                console.log("success");
                            }
                        })) : (this.form.is_use_score = 1, o.setStorage({
                            key: "fudai_scoreStorage",
                            data: !0,
                            success: function() {
                                console.log("success");
                            }
                        })), this.initOrder();
                    },
                    doubleBoxCardswitchChange: function(o) {
                        1 === this.form.is_doubleBoxCard ? this.form.is_doubleBoxCard = 0 : this.form.is_doubleBoxCard = 1, 
                        this.initOrder();
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    createOrder: function() {
                        var t = this;
                        if (this.isLoading) return !1;
                        this.isLoading = !0, o.showLoading({
                            title: "提交中"
                        }), this.$http("/fudai/order/confirm", "POST", a({
                            page_uuid: this.info.page_uuid,
                            total: this.payTotal,
                            coupon_id: this.currentCoupon.id
                        }, this.form)).then(function(e) {
                            o.hideLoading(), t.isSubmiting = !1;
                            var n = e.data;
                            n.is_need_pay ? u.default.pay(a(a({}, n), {}, {
                                success: function() {
                                    t.$emit("success", n.order, !1, n.is_doubleBoxCard);
                                },
                                fail: function() {
                                    o.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), t.$http("/orders/".concat(n.order.uuid), "PUT", {
                                        type: "close_and_delete"
                                    });
                                }
                            })) : t.$emit("success", n.order, !1, n.is_doubleBoxCard);
                        }).catch(function(o) {
                            t.isLoading = !1;
                        });
                    },
                    handleBtnClick: function() {
                        if (!this.isCheckUserStatement) return o.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1;
                    },
                    handleJixuClick: function() {
                        if (this.pingtaiBool = !1, this.isLoading) return !1;
                        this.createOrder();
                    },
                    submit: function() {
                        if (o.setStorage({
                            key: "isCheckUserStatementBool",
                            data: !0,
                            success: function() {}
                        }), 0 == this.buzaiBool) this.pingtaiBool = !0; else {
                            if (this.pingtaiBool = !1, this.isLoading) return !1;
                            this.createOrder();
                        }
                    }
                },
                onPageScroll: function(o) {}
            };
            t.default = c;
        }).call(this, e("543d").default);
    },
    4790: function(o, t, e) {
        "use strict";
        var n = e("0bad");
        e.n(n).a;
    },
    "4a6a": function(o, t, e) {
        "use strict";
        e.r(t);
        var n = e("cf4a"), i = e("5239");
        for (var s in i) [ "default" ].indexOf(s) < 0 && function(o) {
            e.d(t, o, function() {
                return i[o];
            });
        }(s);
        e("4790");
        var u = e("f0c5"), r = Object(u.a)(i.default, n.b, n.c, !1, null, "c4e9487c", null, !1, n.a, void 0);
        t.default = r.exports;
    },
    5239: function(o, t, e) {
        "use strict";
        e.r(t);
        var n = e("3c59"), i = e.n(n);
        for (var s in n) [ "default" ].indexOf(s) < 0 && function(o) {
            e.d(t, o, function() {
                return n[o];
            });
        }(s);
        t.default = i.a;
    },
    cf4a: function(o, t, e) {
        "use strict";
        e.d(t, "b", function() {
            return i;
        }), e.d(t, "c", function() {
            return s;
        }), e.d(t, "a", function() {
            return n;
        });
        var n = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            },
            UsableCouponPopup: function() {
                return e.e("components/UsableCouponPopup/UsableCouponPopup").then(e.bind(null, "3858"));
            }
        }, i = function() {
            var o = this, t = (o.$createElement, o._self._c, o.order.coupon_discount ? o.$tool.formatPrice(o.order.coupon_discount) : null), e = o.order.coupon_discount ? null : o.usableCoupons.length, n = !o.order.coupon_discount && e ? o.usableCoupons.length : null, i = o.order.redpack ? o.$tool.formatPrice(o.order.redpack) : null, s = o.order.max_useable_score && o.form.is_use_score ? o._f("priceToFixed")(o.order.score_discount) : null;
            o.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    g1: e,
                    g2: n,
                    g3: i,
                    f0: s
                }
            });
        }, s = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/fudai/components/PayCard-create-component", {
    "pages/fudai/components/PayCard-create-component": function(o, t, e) {
        e("543d").createComponent(e("4a6a"));
    }
}, [ [ "pages/fudai/components/PayCard-create-component" ] ] ]);