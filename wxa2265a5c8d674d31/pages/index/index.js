(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/index/index" ], {
    "49e4": function(t, n, o) {
        "use strict";
        (function(t, n) {
            var e = o("4ea4");
            o("18ba"), e(o("66fd"));
            var u = e(o("e860"));
            t.__webpack_require_UNI_MP_PLUGIN__ = o, n(u.default);
        }).call(this, o("bc2e").default, o("543d").createPage);
    },
    "9d3d": function(t, n, o) {
        "use strict";
        var e = o("c218");
        o.n(e).a;
    },
    aea9: function(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("f530"), u = o.n(e);
        for (var i in e) [ "default" ].indexOf(i) < 0 && function(t) {
            o.d(n, t, function() {
                return e[t];
            });
        }(i);
        n.default = u.a;
    },
    c218: function(t, n, o) {},
    e860: function(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("ed49"), u = o("aea9");
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(t) {
            o.d(n, t, function() {
                return u[t];
            });
        }(i);
        o("9d3d");
        var p = o("f0c5"), s = Object(p.a)(u.default, e.b, e.c, !1, null, "d05d154a", null, !1, e.a, void 0);
        n.default = s.exports;
    },
    ed49: function(t, n, o) {
        "use strict";
        o.d(n, "b", function() {
            return u;
        }), o.d(n, "c", function() {
            return i;
        }), o.d(n, "a", function() {
            return e;
        });
        var e = {
            HomeNavbar: function() {
                return o.e("components/HomeNavbar/HomeNavbar").then(o.bind(null, "3cce"));
            },
            PageRender: function() {
                return Promise.all([ o.e("common/vendor"), o.e("components/PageRender/PageRender") ]).then(o.bind(null, "b4aa"));
            },
            Popup: function() {
                return o.e("components/Popup/Popup").then(o.bind(null, "04f0"));
            },
            Danmus: function() {
                return o.e("components/Danmus/Danmus").then(o.bind(null, "4bc1"));
            },
            CouponPopup: function() {
                return o.e("components/CouponPopup/CouponPopup").then(o.bind(null, "5c64"));
            }
        }, u = function() {
            var t = this;
            t.$createElement, t._self._c, t._isMounted || (t.e0 = function(n) {
                t.isShowCouponPopupKey = !1;
            });
        }, i = [];
    },
    f530: function(t, n, o) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0, o("26cb");
            var e = {
                components: {},
                data: function() {
                    return {
                        isFixed: !1,
                        scrollTop: 0,
                        popupSetting: {
                            number: ""
                        },
                        isShowPopupKey: !0,
                        isShowCouponPopupKey: !1,
                        danmuSetting: {},
                        danmuList: [],
                        refreshCounter: 0,
                        getNextPageCounter: 0
                    };
                },
                computed: {
                    page: function() {
                        return this.$store.getters.setting.box_home;
                    },
                    isShowPopup: function() {
                        var n = t.getStorageSync("popup_" + this.popupSetting.number);
                        return this.popupSetting.is_enabled && this.isShowPopupKey && !n;
                    },
                    isShowCouponPopup: function() {
                        var n = t.getStorageSync("coupon_popup" + this.$store.getters.setting.coupon_popup.number);
                        return this.$store.getters.setting.coupon_popup.is_enabled && this.isShowCouponPopupKey && !n;
                    }
                },
                watch: {},
                onLoad: function(t) {
                    var n = this;
                    this.$http("/coupon/popup-list").then(function(t) {
                        0 !== t.data.list.length && (n.isShowCouponPopupKey = !0);
                    });
                    var o = t.gdt_vid, e = t.weixinadinfo;
                    if (o) {
                        var u = o.split(".")[0];
                        this.$store.commit("SET_CLICK_ID", u);
                    }
                    e && this.$store.commit("SET_WX_INFO", e), this.$visitor.record("box_index"), this.getPopup(), 
                    this.getDanmu();
                },
                onPullDownRefresh: function() {
                    this.getPopup(), this.getDanmu();
                },
                onReachBottom: function() {
                    this.getNextPageCounter++;
                },
                onShow: function() {},
                methods: {
                    getDanmu: function() {
                        var t = this;
                        this.$http("/danmus/home").then(function(n) {
                            t.danmuSetting = n.data.setting, t.danmuList = n.data.list;
                        });
                    },
                    getPopup: function() {
                        var t = this;
                        this.$http("/setting/popup", "GET").then(function(n) {
                            t.popupSetting = n.data.setting;
                        });
                    },
                    closePopup: function() {
                        t.setStorageSync("popup_" + this.popupSetting.number, 1), this.isShowPopupKey = !1;
                    },
                    closeCouponPopup: function() {
                        t.setStorageSync("coupon_popup" + this.$store.getters.setting.coupon_popup.number, 1), 
                        this.isShowCouponPopupKey = !1;
                    }
                },
                onPageScroll: function(n) {
                    this.scrollTop = n.scrollTop, n.scrollTop > 334 ? (this.isFixed = !0, t.$emit("onPageScroll", this.isFixed)) : (this.isFixed = !1, 
                    t.$emit("onPageScroll", this.isFixed));
                }
            };
            n.default = e;
        }).call(this, o("543d").default);
    }
}, [ [ "49e4", "common/runtime", "common/vendor" ] ] ]);