(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/login/index" ], {
    "00a3": function(e, t, n) {},
    "34c1": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return i;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            UserStatementFalse: function() {
                return n.e("components/UserStatementFalse/UserStatementFalse").then(n.bind(null, "3c88"));
            },
            GetPhonePopup: function() {
                return n.e("components/GetPhonePopup/GetPhonePopup").then(n.bind(null, "fc3f"));
            }
        }, i = function() {
            var e = this;
            e.$createElement, e._self._c, e._isMounted || (e.e0 = function(t) {
                e.isShowPhonePopup = !1;
            });
        }, a = [];
    },
    "3c8c": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("9cf3"), i = n.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = i.a;
    },
    "7ebf": function(e, t, n) {
        "use strict";
        (function(e, t) {
            var o = n("4ea4");
            n("18ba"), o(n("66fd"));
            var i = o(n("9421"));
            e.__webpack_require_UNI_MP_PLUGIN__ = n, t(i.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    9421: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("34c1"), i = n("3c8c");
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(a);
        n("d0a2");
        var c = n("f0c5"), r = Object(c.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        t.default = r.exports;
    },
    "9cf3": function(e, t, n) {
        "use strict";
        (function(e, o) {
            var i = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var a = i(n("9523")), c = n("cbb4");
            function r(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            var s = {
                name: "login",
                components: {
                    GetPhonePopup: function() {
                        n.e("components/GetPhonePopup/GetPhonePopupForWechat").then(function() {
                            return resolve(n("cb40"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        code: "",
                        params: {},
                        isShowPhonePopup: !1,
                        dataUuid: "",
                        isLoginLoading: !1,
                        isCheckUserStatement: !0,
                        isuser_info: !1
                    };
                },
                created: function() {
                    this.getCode();
                },
                onShow: function() {
                    var t = (0, c.$getStorage)("token");
                    t && (console.log("token====>" + t), setTimeout(function(t) {
                        e.navigateBack({
                            delta: 1
                        });
                    }, 100));
                },
                computed: {
                    logo: function() {
                        return this.$store.getters.setting.login_page.logo || "";
                    },
                    bgImage: function() {
                        return this.$store.getters.setting.login_page.bg_image || "";
                    }
                },
                methods: {
                    getCode: function() {
                        var t = this;
                        e.login({
                            success: function(e) {
                                console.log("logon ====>", e), t.code = e.code;
                            },
                            fail: function(e) {
                                console.log(e);
                            }
                        });
                    },
                    getUserInfoWithMiniapp: function() {
                        var t = this;
                        return this.isCheckUserStatement ? !this.isLoginLoading && (this.isLoginLoading = !0, 
                        void o.getUserProfile({
                            desc: "用于完善会员资料",
                            success: function(e) {
                                console.log(e), (0, c.$getStorage)("click_id") ? t.params = {
                                    code: t.code,
                                    encrypt_data: e.encryptedData,
                                    iv: e.iv,
                                    click_id: (0, c.$getStorage)("click_id")
                                } : t.params = {
                                    code: t.code,
                                    encrypt_data: e.encryptedData,
                                    iv: e.iv
                                }, t.submitLogin(t.params);
                            },
                            complete: function() {
                                t.isLoginLoading = !1;
                            }
                        })) : (e.showToast({
                            title: "请先阅读并同意《用户使用协议》",
                            icon: "none"
                        }), !1);
                    },
                    getPhoneNumberSuccess: function(e) {
                        this.params = function(e) {
                            for (var t = 1; t < arguments.length; t++) {
                                var n = null != arguments[t] ? arguments[t] : {};
                                t % 2 ? r(Object(n), !0).forEach(function(t) {
                                    (0, a.default)(e, t, n[t]);
                                }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : r(Object(n)).forEach(function(t) {
                                    Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                                });
                            }
                            return e;
                        }({
                            data_uuid: this.dataUuid
                        }, e), (0, c.$getStorage)("click_id") && (this.params.click_id = (0, c.$getStorage)("click_id")), 
                        (0, c.$getStorage)("weixinadinfo") && (this.params.weixinadinfo = (0, c.$getStorage)("weixinadinfo")), 
                        this.isuser_info = !0, this.submitLogin(this.params);
                    },
                    stopLogin: function() {
                        (0, c.$setStorage)("stop_login_time", new Date().getTime()), e.switchTab({
                            url: "/pages/index/index"
                        });
                    },
                    submitLogin: function(t) {
                        var n = this;
                        return e.showLoading({
                            title: "登录中"
                        }), (0, c.$getStorage)("click_id") && (t.click_id = (0, c.$getStorage)("click_id")), 
                        this.$store.dispatch("login", {
                            type: "with-wechat",
                            params: t
                        }).then(function(t) {
                            e.hideLoading(), t.data.is_need_phone_number || t.data.is_need_phone ? (n.isShowPhonePopup = !0, 
                            n.dataUuid = t.data.data_uuid, n.getCode()) : t.data.token && (n.$store.dispatch("getUserInfo"), 
                            t.data.is_first_login ? e.redirectTo({
                                url: "/pages/myProfile/index"
                            }) : e.navigateBack({
                                delta: 1
                            }));
                        });
                    }
                }
            };
            t.default = s;
        }).call(this, n("543d").default, n("bc2e").default);
    },
    d0a2: function(e, t, n) {
        "use strict";
        var o = n("00a3");
        n.n(o).a;
    }
}, [ [ "7ebf", "common/runtime", "common/vendor" ] ] ]);