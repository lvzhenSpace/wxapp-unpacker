(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myActivity/components/ActivityItem" ], {
    "61d6": function(t, i, n) {
        "use strict";
        var e = n("cde5");
        n.n(e).a;
    },
    bd06: function(t, i, n) {
        "use strict";
        (function(t) {
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var n = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    activity: function() {
                        return this.info.activity || {};
                    }
                },
                methods: {
                    toDetail: function() {
                        "seckill" == this.activity.type ? t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + this.activity.product_uuid + "&activityId=" + this.activity.id
                        }) : t.navigateTo({
                            url: "/pages/".concat(this.activity.type, "Activity/detail?uuid=").concat(this.activity.uuid)
                        });
                    }
                }
            };
            i.default = n;
        }).call(this, n("543d").default);
    },
    cde5: function(t, i, n) {},
    f0a5: function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("bd06"), c = n.n(e);
        for (var a in e) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(i, t, function() {
                return e[t];
            });
        }(a);
        i.default = c.a;
    },
    f280: function(t, i, n) {
        "use strict";
        n.d(i, "b", function() {
            return c;
        }), n.d(i, "c", function() {
            return a;
        }), n.d(i, "a", function() {
            return e;
        });
        var e = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            }
        }, c = function() {
            this.$createElement, this._self._c;
        }, a = [];
    },
    f708: function(t, i, n) {
        "use strict";
        n.r(i);
        var e = n("f280"), c = n("f0a5");
        for (var a in c) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(i, t, function() {
                return c[t];
            });
        }(a);
        n("61d6");
        var o = n("f0c5"), u = Object(o.a)(c.default, e.b, e.c, !1, null, "6f23ddd4", null, !1, e.a, void 0);
        i.default = u.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "pages/myActivity/components/ActivityItem-create-component", {
    "pages/myActivity/components/ActivityItem-create-component": function(t, i, n) {
        n("543d").createComponent(n("f708"));
    }
}, [ [ "pages/myActivity/components/ActivityItem-create-component" ] ] ]);