(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/code/index" ], {
    "13b0": function(e, t, o) {
        "use strict";
        (function(e, t) {
            var c = o("4ea4");
            o("18ba"), c(o("66fd"));
            var n = c(o("e7ff"));
            e.__webpack_require_UNI_MP_PLUGIN__ = o, t(n.default);
        }).call(this, o("bc2e").default, o("543d").createPage);
    },
    4562: function(e, t, o) {},
    "9aa4": function(e, t, o) {
        "use strict";
        var c = o("baf1");
        o.n(c).a;
    },
    baf1: function(e, t, o) {},
    bb50: function(e, t, o) {
        "use strict";
        o.r(t);
        var c = o("dc11"), n = o.n(c);
        for (var i in c) [ "default" ].indexOf(i) < 0 && function(e) {
            o.d(t, e, function() {
                return c[e];
            });
        }(i);
        t.default = n.a;
    },
    c5f9: function(e, t, o) {
        "use strict";
        o.d(t, "b", function() {
            return c;
        }), o.d(t, "c", function() {
            return n;
        }), o.d(t, "a", function() {});
        var c = function() {
            this.$createElement, this._self._c;
        }, n = [];
    },
    dc11: function(e, t, o) {
        "use strict";
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var o = {
                components: {},
                data: function() {
                    return {
                        code: "",
                        info: {}
                    };
                },
                computed: {},
                onLoad: function(e) {
                    this.code = e.code || "";
                },
                methods: {
                    useCode: function() {
                        var t = this;
                        e.showLoading({
                            title: "兑换中"
                        }), this.$http("/code/use", "POST", {
                            code: this.code
                        }).then(function(o) {
                            e.hideLoading(), e.showToast({
                                title: "兑换成功，跳转中~",
                                icon: "none"
                            }), setTimeout(function(o) {
                                "score" == t.info.code_type ? e.navigateTo({
                                    url: "/pages/myScore/index"
                                }) : "redpack" == t.info.code_type ? e.navigateTo({
                                    url: "/pages/myRedpack/index"
                                }) : "show_card" == t.info.code_type || "exclude_card" == t.info.code_type ? e.navigateTo({
                                    url: "/pages/myCard/index"
                                }) : "chip" == t.info.code_type ? e.navigateTo({
                                    url: "/pages/myChip/index"
                                }) : "product" == t.info.code_type && e.navigateTo({
                                    url: "/pages/myBox/index"
                                });
                            }, 1500);
                        });
                    },
                    submit: function() {
                        var t = this;
                        if (this.code.length < 4 || this.code.length > 10) return e.showToast({
                            title: "兑换码长度应在4到10个字符之间~",
                            icon: "none"
                        }), !1;
                        e.showLoading({
                            title: "校验中"
                        }), this.$http("/code/check", "POST", {
                            code: this.code
                        }).then(function(o) {
                            e.hideLoading();
                            var c = o.data;
                            if (t.info = c, "coupon" == c.code_type) e.navigateTo({
                                url: "/pages/couponDetail/index?uuid=" + c.coupon.uuid + "&code=" + t.code
                            }); else {
                                var n = "";
                                "score" === c.code_type ? n = "此兑换码可兑换".concat(c.score).concat(t.scoreAlias) : "redpack" === c.code_type ? n = "此兑换码可兑换".concat(c.redpack / 100, "元余额") : "exclude_card" === c.code_type ? n = "此兑换码可兑换".concat(c.prize_total, "张排除卡") : "show_card" === c.code_type ? n = "此兑换码可兑换".concat(c.prize_total, "张透视卡") : "chip" === c.code_type ? n = "此兑换码可兑换".concat(c.prize_total, '块"') + c.chip.title + '"' : "product" === c.code_type && (n = '兑换 "'.concat(c.product.title, '" 1件')), 
                                e.showModal({
                                    title: "兑换提示",
                                    content: n,
                                    confirmText: "立即兑换",
                                    success: function(e) {
                                        e.confirm && t.useCode();
                                    }
                                });
                            }
                        });
                    }
                }
            };
            t.default = o;
        }).call(this, o("543d").default);
    },
    e7ff: function(e, t, o) {
        "use strict";
        o.r(t);
        var c = o("c5f9"), n = o("bb50");
        for (var i in n) [ "default" ].indexOf(i) < 0 && function(e) {
            o.d(t, e, function() {
                return n[e];
            });
        }(i);
        o("f23a"), o("9aa4");
        var a = o("f0c5"), d = Object(a.a)(n.default, c.b, c.c, !1, null, "f920cd72", null, !1, c.a, void 0);
        t.default = d.exports;
    },
    f23a: function(e, t, o) {
        "use strict";
        var c = o("4562");
        o.n(c).a;
    }
}, [ [ "13b0", "common/runtime", "common/vendor" ] ] ]);