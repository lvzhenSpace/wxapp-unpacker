(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/openBox/index" ], {
    "0d5e": function(t, n, e) {
        "use strict";
        var o = e("0fb5");
        e.n(o).a;
    },
    "0fb5": function(t, n, e) {},
    "6dd6": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("c164"), i = e("dc38");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(u);
        e("0d5e");
        var a = e("f0c5"), r = Object(a.a)(i.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        n.default = r.exports;
    },
    9405: function(t, n, e) {
        "use strict";
        (function(t, n) {
            var o = e("4ea4");
            e("18ba"), o(e("66fd"));
            var i = o(e("6dd6"));
            t.__webpack_require_UNI_MP_PLUGIN__ = e, n(i.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    },
    b9f6: function(t, n, e) {
        "use strict";
        (function(t) {
            var o = e("4ea4");
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var i = o(e("2eee")), u = o(e("c973")), a = o(e("9523")), r = o(e("452d")), s = e("26cb");
            function c(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    n && (o = o.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), e.push.apply(e, o);
                }
                return e;
            }
            function d(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var e = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? c(Object(e), !0).forEach(function(n) {
                        (0, a.default)(t, n, e[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : c(Object(e)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(e, n));
                    });
                }
                return t;
            }
            e("1e0a");
            var f = {
                name: "openBox",
                mixins: [ r.default ],
                components: {
                    PayCard: function() {
                        Promise.all([ e.e("common/vendor"), e.e("pages/openBox/components/PayCard") ]).then(function() {
                            return resolve(e("7835"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        boxUuid: "",
                        roomUuid: "",
                        skuIndex: "",
                        pageUuid: "",
                        skuList: [],
                        boxInfo: {},
                        roomInfo: {},
                        isShowPayCard: !1,
                        isShowExcludeCard: !1,
                        isShowOpenCard: !1,
                        showBoxCard: {
                            score_price: 0,
                            my_balance: 0
                        },
                        excludeBoxCard: {
                            score_price: 0,
                            my_balance: 0
                        },
                        order: {},
                        yaoyiyaoFlag: !1,
                        isPay: !1,
                        isInit: !1,
                        isOpenPopup: !1,
                        animateStatus: 0,
                        visible: !1,
                        loading: !1,
                        tipsList: {},
                        tips: "",
                        isShowTips: !1,
                        isSharePopup: !1,
                        orderUuid: "",
                        danmuList: [],
                        danmuSetting: {},
                        buyTotal: 1,
                        remainTime: 0,
                        isShowSkuList: !1,
                        baseConfig: {},
                        isShowHiddenSkuRank: !1
                    };
                },
                computed: d(d({}, (0, s.mapGetters)([ "userInfo" ])), {}, {
                    defaultBoxImage: function() {
                        return this.isInit ? "https://cdn2.hquesoft.com/box/openbox.png" : "";
                    },
                    config: function() {
                        return this.$store.getters.setting.open_box;
                    },
                    payCardInfo: function() {
                        return {
                            box: this.boxInfo,
                            room: this.roomInfo,
                            sku_index: this.skuIndex,
                            page_uuid: this.pageUuid
                        };
                    },
                    deviceHeight: function() {
                        return this.$device.screenHeight;
                    },
                    isSmallDevice: function() {
                        return this.$device.screenHeight < 750;
                    },
                    instance: function() {
                        return this.boxInfo.instance;
                    },
                    posterInfo: function() {
                        return {
                            money_price: this.boxInfo.money_price,
                            score_price: this.boxInfo.score_price,
                            title: this.boxInfo.title,
                            path: this.getShareConfig().path,
                            thumb: this.boxInfo.thumb
                        };
                    },
                    freeTipLimit: function() {
                        return null === this.boxInfo.free_tips_limit ? 2 : this.boxInfo.free_tips_limit;
                    },
                    bgImage: function() {
                        return this.isInit ? this.boxInfo.room_bg_image || this.config.bg_image || "https://cdn2.hquesoft.com/box/bg.png" : "";
                    },
                    isDarkMode: function() {
                        return 2 === this.boxInfo.bg_color_mode;
                    },
                    boxTips: function() {
                        return this.boxInfo.tips || "官方正品，非质量问题不支持退换";
                    },
                    share: function() {
                        return {
                            title: this.boxInfo.title,
                            thumb: this.boxInfo.thumb,
                            desc: "",
                            path: "/pages/boxDetail/index?uuid=" + this.boxInfo.uuid + "&invite_node=box-" + this.boxInfo.uuid
                        };
                    }
                }),
                onLoad: function(n) {
                    var e = this;
                    return (0, u.default)(i.default.mark(function o() {
                        return i.default.wrap(function(o) {
                            for (;;) switch (o.prev = o.next) {
                              case 0:
                                e.roomUuid = n.roomUuid, e.skuIndex = parseInt(n.index), e.boxUuid = n.uuid, t.showLoading({
                                    title: "加载中",
                                    mask: !0
                                }), e.initRoom().then(function(t) {
                                    e.isInit = !0, "wholeRoom" === n.buyMode ? (e.changeone(), e.buyTotal = 0, e.isShowPayCard = !0) : n.buyMode && (e.buyTotal = parseInt(n.buyMode), 
                                    e.isShowPayCard = !0), e.getDanmu();
                                }), t.hideLoading(), t.$on("startShare", function() {
                                    e.isSharePopup = !0;
                                });

                              case 7:
                              case "end":
                                return o.stop();
                            }
                        }, o);
                    }))();
                },
                onShow: function() {
                    var n = this;
                    this.$store.dispatch("getUserInfo"), this.initData(), t.onAccelerometerChange(function(t) {
                        1 != n.boxInfo.open_mode || n.yaoyiyaoFlag || 0 === n.boxInfo.free_tips_limit || (Math.abs(t.x) > .9 && Math.abs(t.y) > .9 || Math.abs(t.x) > .9 && Math.abs(t.z) > .9) && (n.yaoyiyaoFlag || (n.$playAudio("yao"), 
                        n.getFreeTips(), n.yaoyiyaoFlag = !0, setTimeout(function() {
                            n.yaoyiyaoFlag = !1;
                        }, 2500)));
                    });
                },
                onUnload: function() {
                    t.stopAccelerometer();
                },
                onHide: function() {
                    t.stopAccelerometer();
                },
                methods: {
                    useFreeTicket: function() {
                        var n = this;
                        t.showLoading({
                            title: "提交中",
                            icon: "none"
                        }), this.$http("/box-order/confirm", "POST", {
                            room_id: this.payCardInfo.room.id,
                            sku_index: this.payCardInfo.sku_index,
                            page_uuid: this.payCardInfo.page_uuid,
                            total: 1,
                            is_use_free_ticket: 1
                        }).then(function(e) {
                            t.hideLoading();
                            var o = e.data;
                            o.is_need_pay ? t.showToast({
                                title: "兑换出错~",
                                icon: "none"
                            }) : n.paySuccess(o.order);
                        });
                    },
                    getDanmu: function() {
                        var t = this;
                        this.$http("/danmus/box_open?node_id=".concat(this.boxInfo.id)).then(function(n) {
                            t.danmuSetting = n.data.setting, t.danmuList = n.data.list;
                        });
                    },
                    showPreviewTips: function() {
                        t.showToast({
                            title: "此款正在预售中，暂不可购买哦~",
                            icon: "none"
                        });
                    },
                    isExcluded: function(t) {
                        return -1 !== (this.tipsList.exclude || []).findIndex(function(n) {
                            return n.sku_id === t.id;
                        });
                    },
                    isShowed: function(t) {
                        return -1 !== (this.tipsList.show || []).findIndex(function(n) {
                            return n.sku_id === t.id;
                        });
                    },
                    initRoom: function() {
                        var n = this;
                        return this.$http("/rooms/".concat(this.roomUuid, "/skus/").concat(this.skuIndex)).then(function(e) {
                            n.boxInfo = e.data.box, n.roomInfo = e.data.room, n.skuList = e.data.sku_list, n.pageUuid = e.data.page_uuid, 
                            n.baseConfig = e.data.config, t.setNavigationBarTitle({
                                title: n.boxInfo.title
                            });
                        });
                    },
                    initData: function() {
                        var t = this;
                        this.$http("/user/cards").then(function(n) {
                            t.showBoxCard = n.data.show_box_card, t.excludeBoxCard = n.data.exclude_box_card;
                        });
                    },
                    previewSmallBoxThumb: function(n) {
                        var e = this.skuList.map(function(t) {
                            return t.thumb;
                        });
                        t.previewImage({
                            urls: e,
                            current: n
                        });
                    },
                    showTips: function(t) {
                        this.tips = t, this.isShowTips = !0;
                    },
                    buyNow: function() {
                        this.buyTotal = 1, this.isShowPayCard = !0;
                    },
                    useCard: function(n) {
                        var e = this, o = "温馨提示", a = "确定要使用1张透视卡吗~";
                        "exclude_box" === n && (o = "温馨提示", a = "确定要使用1张排除卡吗~"), t.showModal({
                            title: o,
                            content: a,
                            success: function() {
                                var o = (0, u.default)(i.default.mark(function o(u) {
                                    return i.default.wrap(function(o) {
                                        for (;;) switch (o.prev = o.next) {
                                          case 0:
                                            u.confirm && (t.showLoading({
                                                title: "提交中"
                                            }), e.$http("/box/use-card", "POST", {
                                                card_type: n,
                                                page_uuid: e.pageUuid
                                            }).then(function(n) {
                                                e.showTips(n.data.tips), e.isShowExcludeCard = !1, e.isShowOpenCard = !1, e.tipsList = n.data.tips_list, 
                                                t.hideLoading(), e.initData();
                                            }));

                                          case 1:
                                          case "end":
                                            return o.stop();
                                        }
                                    }, o);
                                }));
                                return function(t) {
                                    return o.apply(this, arguments);
                                };
                            }()
                        });
                    },
                    getFreeTips: function() {
                        var t = this;
                        this.$http("/box/use-card", "POST", {
                            card_type: "free_tips",
                            page_uuid: this.pageUuid
                        }).then(function(n) {
                            t.showTips(n.data.tips), t.isShowExcludeCard = !1, t.isShowOpenCard = !1, t.tipsList = n.data.tips_list;
                        });
                    },
                    handleOk: function() {
                        t.redirectTo({
                            url: "/pages/orderList/index"
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    changeone: function(t) {
                        var n = this;
                        this.animateStatus = 1, this.$http("/box/change-sku", "POST", {
                            room_id: this.roomInfo.id,
                            sku_index: this.skuIndex
                        }).then(function(t) {
                            n.skuIndex = t.data.sku_index, n.roomUuid = t.data.room_uuid, n.pageUuid = t.data.page_uuid, 
                            n.initRoom(), n.tipsList = {};
                        }), setTimeout(function(t) {
                            n.animateStatus = 2;
                        }, 800);
                    },
                    paySuccess: function(n) {
                        if (2 === this.boxInfo.open_mode) return t.redirectTo({
                            url: "/pages/orderList/index"
                        }), !0;
                        this.isShowPayCard = !1, t.showToast({
                            title: "支付成功, 开盒中",
                            icon: "none"
                        }), this.order = n, this.isOpenPopup = !0;
                    },
                    goBack: function() {
                        t.navigateBack({
                            delta: 1
                        });
                    }
                }
            };
            n.default = f;
        }).call(this, e("543d").default);
    },
    c164: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            TextNavBar: function() {
                return e.e("components/TextNavBar/TextNavBar").then(e.bind(null, "9141"));
            },
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            },
            OpenBoxPopup: function() {
                return e.e("components/OpenBoxPopup/OpenBoxPopup").then(e.bind(null, "9fe0"));
            },
            SharePopup: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/SharePopup/SharePopup") ]).then(e.bind(null, "b677"));
            },
            Danmus: function() {
                return e.e("components/Danmus/Danmus").then(e.bind(null, "4bc1"));
            },
            uniPopup: function() {
                return e.e("uni_modules/uni-popup/components/uni-popup/uni-popup").then(e.bind(null, "0fa2"));
            },
            BoxSkuPopup: function() {
                return e.e("components/BoxSkuPopup/BoxSkuPopup").then(e.bind(null, "f1cb"));
            },
            FreeTicketFloatBtn: function() {
                return e.e("components/FreeTicketFloatBtn/FreeTicketFloatBtn").then(e.bind(null, "ef0dd"));
            },
            HiddenSkuRank: function() {
                return e.e("components/HiddenSkuRank/HiddenSkuRank").then(e.bind(null, "0818"));
            }
        }, i = function() {
            var t = this, n = (t.$createElement, t._self._c, t.boxInfo ? t._f("bigNumberDisplay")(t.userInfo.score) : null), e = t.boxInfo ? t.__map(t.skuList, function(n, e) {
                var o = t.__get_orig(n), i = t.isExcluded(n);
                return {
                    $orig: o,
                    m0: i,
                    m1: i ? null : t.isShowed(n)
                };
            }) : null;
            t._isMounted || (t.e0 = function(n) {
                t.isSharePopup = !0;
            }, t.e1 = function(n) {
                t.isShowHiddenSkuRank = !0;
            }, t.e2 = function(n) {
                t.isShowExcludeCard = !0;
            }, t.e3 = function(n) {
                t.isShowOpenCard = !0;
            }, t.e4 = function(n) {
                return t.$refs.detailPopup.open("bottom");
            }, t.e5 = function(n) {
                t.isShowTips = !1;
            }, t.e6 = function(n) {
                t.isShowExcludeCard = !1;
            }, t.e7 = function(n) {
                t.isShowOpenCard = !1;
            }, t.e8 = function(n) {
                t.isShowPayCard = !1;
            }, t.e9 = function(n) {
                t.isSharePopup = !1;
            }, t.e10 = function(n) {
                return t.$refs.detailPopup.close();
            }, t.e11 = function(n) {
                t.isShowHiddenSkuRank = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    f0: n,
                    l0: e
                }
            });
        }, u = [];
    },
    dc38: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("b9f6"), i = e.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(u);
        n.default = i.a;
    }
}, [ [ "9405", "common/runtime", "common/vendor" ] ] ]);