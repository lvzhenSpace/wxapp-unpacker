(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/orderList/index" ], {
    "41ff": function(t, e, i) {
        "use strict";
        var n = i("591c");
        i.n(n).a;
    },
    "591c": function(t, e, i) {},
    "5f04": function(t, e, i) {
        "use strict";
        (function(t) {
            var n = i("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var a = n(i("2eee")), s = n(i("c973")), r = n(i("448a")), o = n(i("82c0")), c = {
                components: {
                    OrderItem: function() {
                        Promise.all([ i.e("common/vendor"), i.e("pages/orderList/components/OrderItem") ]).then(function() {
                            return resolve(i("af3b"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    IActionSheet: function() {
                        i.e("components/ActionSheet/index").then(function() {
                            return resolve(i("fbfb"));
                        }.bind(null, i)).catch(i.oe);
                    },
                    PayCard: function() {
                        i.e("pages/orderList/components/PayCard").then(function() {
                            return resolve(i("ea74"));
                        }.bind(null, i)).catch(i.oe);
                    }
                },
                data: function() {
                    return {
                        order: {},
                        reasons: [],
                        list: [],
                        visible: !1,
                        dataList: [],
                        current: 0,
                        currentUuid: "6056e83ef3251",
                        types: [ "pay_pending", "deliver_pending", "delivered", "all" ],
                        typeTextList: [ "待付款", "待发货", "待收货", "全部" ],
                        arrList: [ "暂无待付款的订单", "暂无待发货的订单", "暂无待收货的订单", "暂无订单列表" ],
                        isSelectMode: !1,
                        selectedIds: [],
                        isShowPay: !1,
                        isShowReturnSalePopup: !1,
                        isRefresh: !1,
                        statusTotal: {}
                    };
                },
                computed: {
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    }
                },
                onLoad: function(e) {
                    var i = this;
                    this.current = this.types.indexOf(e.status || "pending"), this.initData(), this.$api.emit("order.cancel_reason.list").then(function(t) {
                        i.reasons = t.data.list;
                    }), t.$on("postCommentSuccess", function(t) {
                        i.initData(), i.getOrderList(), console.log("postCommentSuccess");
                    });
                },
                onShow: function() {
                    this.refresh();
                },
                onUnload: function() {
                    t.$off("postCommentSuccess", function(t) {
                        console.log("移除postCommentSuccess 自定义事件");
                    });
                },
                methods: {
                    initTotalData: function() {
                        var t = this;
                        this.$http("/order-stat").then(function(e) {
                            t.statusTotal = e.data.info;
                        });
                    },
                    successDeliver: function() {
                        this.cancelSelect(), this.current = 2, this.refresh(), this.isShowPay = !1, t.showModal({
                            title: "发货成功",
                            content: "已成功提交发货请求，请注意查收快递哦~"
                        });
                    },
                    cancelSelect: function() {
                        this.isSelectMode = !1, this.selectedIds = [];
                    },
                    selectOrSubmit: function() {
                        if (this.isSelectMode) {
                            if (0 == this.selectedIds.length) return t.showModal({
                                title: "请选择盒子",
                                content: "选择一个或多个盒子后才能提交发货哦~"
                            }), !1;
                            this.isShowPay = !0;
                        } else this.isSelectMode = !0;
                    },
                    checkItem: function(t) {
                        var e = this.selectedIds.indexOf(t.id);
                        e > -1 ? this.selectedIds.splice(e, 1) : this.selectedIds.push(t.id);
                    },
                    refresh: function() {
                        this.isRefresh = !0, this.cleanData(), this.getOrderList(), this.initTotalData();
                    },
                    scrolltolower: function() {
                        this.dataList[this.current].page++, this.getOrderList();
                    },
                    initData: function() {
                        console.log(666);
                        var t = [];
                        this.types.forEach(function(e) {
                            t.push({
                                list: [],
                                type: e,
                                page: 1,
                                per_page: 50,
                                total: 0,
                                init: !1,
                                loading: !1
                            });
                        }), this.dataList = t;
                    },
                    cleanData: function() {
                        this.dataList.forEach(function(t) {
                            t.page = 1, t.init = !1;
                        });
                    },
                    cancelOrder: function(e) {
                        var i = this;
                        this.visibleChange(), t.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.close", this.order.uuid).then(function(e) {
                            var n = i.dataList[i.current];
                            i.$api.emit("order.list", {
                                status: n.type,
                                page: 1,
                                per_page: n.per_page
                            }).then(function(e) {
                                var n;
                                t.hideLoading(), i.initData(), (n = i.dataList[i.current].list).push.apply(n, (0, 
                                r.default)(e.data.list)), i.dataList[i.current].total = e.data.item_total, i.dataList[i.current].init = !0, 
                                t.showToast({
                                    title: "已提交取消请求~",
                                    icon: "none"
                                });
                            });
                        }).catch(function(e) {
                            t.hideLoading();
                        });
                    },
                    visibleChange: function() {
                        this.visible = !this.visible;
                    },
                    payOrder: function(e) {
                        var i = this;
                        return (0, s.default)(a.default.mark(function n() {
                            var s;
                            return a.default.wrap(function(n) {
                                for (;;) switch (n.prev = n.next) {
                                  case 0:
                                    return t.showLoading({
                                        mask: !0
                                    }), n.next = 3, i.$api.emit("order.pay_config", e, {
                                        pay_type: "wechat",
                                        sub_type: "miniapp"
                                    });

                                  case 3:
                                    s = n.sent, t.hideLoading(), o.default.pay({
                                        pay_config: s.data.pay_config,
                                        success: function() {
                                            t.showToast({
                                                title: "支付成功",
                                                icon: "none"
                                            }), i.refresh();
                                        },
                                        faild: function() {
                                            t.showToast({
                                                title: "支付失败",
                                                icon: "none"
                                            });
                                        }
                                    });

                                  case 6:
                                  case "end":
                                    return n.stop();
                                }
                            }, n);
                        }))();
                    },
                    actions: function(e) {
                        var i = this;
                        switch (e.action) {
                          case "立即支付":
                            this.payOrder(e.order.uuid);
                            break;

                          case "取消订单":
                            this.order = e.order, this.visibleChange();
                            break;

                          case "返售":
                            this.currentUuid = e.order.uuid, this.isShowReturnSalePopup = !0;
                            break;

                          case "删除订单":
                            t.showModal({
                                title: "提示",
                                content: "确认要删除订单吗?",
                                success: function(n) {
                                    n.confirm && (t.showLoading({
                                        title: "加载中",
                                        mask: !0
                                    }), i.$api.emit("order.destory", e.order.uuid).then(function(t) {
                                        i.initData(), i.getOrderList();
                                    }));
                                }
                            });
                            break;

                          case "提醒发货":
                            break;

                          case "确认收货":
                            t.showModal({
                                title: "提示",
                                content: "订单确认收货?",
                                success: function(n) {
                                    n.confirm && (t.showLoading({
                                        title: "加载中",
                                        mask: !0
                                    }), i.$api.emit("order.complete", e.order.uuid).then(function(e) {
                                        t.hideLoading(), i.refresh();
                                    }));
                                }
                            });
                            break;

                          case "去评价":
                            t.navigateTo({
                                url: "/pages/writeComment/index?uuid=" + e.order.uuid
                            });
                            break;

                          case "核销码":
                            t.navigateTo({
                                url: "/pages/orderCode/index?uuid=" + e.order.uuid
                            });
                        }
                    },
                    currentChange: function(t) {
                        var e = t.currentTarget.dataset.current;
                        if (e !== this.current) {
                            this.current = e;
                            var i = this.dataList[this.current];
                            console.log(i), i.init || this.getOrderList();
                        }
                    },
                    currentChange2: function(t) {
                        var e = t.detail.current;
                        e !== this.current && (this.current = e, this.dataList[this.current].init || this.getOrderList());
                    },
                    getOrderList: function() {
                        var t = this.dataList[this.current];
                        t.loading || (t.loading = !0, this.$api.emit("order.list", {
                            status: t.type,
                            page: t.page,
                            per_page: t.per_page
                        }).then(function(e) {
                            var i;
                            t.loading = !1, 1 === e.data.current_page ? t.list = e.data.list : (i = t.list).push.apply(i, (0, 
                            r.default)(e.data.list)), t.total = e.data.item_total, t.init = !0;
                        }));
                    }
                }
            };
            e.default = c;
        }).call(this, i("543d").default);
    },
    "67c1": function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("948f"), a = i("c51c");
        for (var s in a) [ "default" ].indexOf(s) < 0 && function(t) {
            i.d(e, t, function() {
                return a[t];
            });
        }(s);
        i("41ff");
        var r = i("f0c5"), o = Object(r.a)(a.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        e.default = o.exports;
    },
    "948f": function(t, e, i) {
        "use strict";
        i.d(e, "b", function() {
            return a;
        }), i.d(e, "c", function() {
            return s;
        }), i.d(e, "a", function() {
            return n;
        });
        var n = {
            NoData: function() {
                return i.e("components/NoData/NoData").then(i.bind(null, "cafe"));
            },
            ReturnSalePopupOld: function() {
                return i.e("components/ReturnSalePopupOld/ReturnSalePopupOld").then(i.bind(null, "f428"));
            }
        }, a = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__map(t.types, function(e, i) {
                return {
                    $orig: t.__get_orig(e),
                    l0: t.__map(t.dataList[i].list, function(e, i) {
                        return {
                            $orig: t.__get_orig(e),
                            g0: t.selectedIds.indexOf(e.id)
                        };
                    }),
                    g1: t.dataList[i].list.length,
                    g2: t.dataList[i].init && !t.dataList[i].list.length
                };
            }));
            t._isMounted || (t.e0 = function(e) {
                t.isShowPay = !1;
            }, t.e1 = function(e) {
                t.isShowReturnSalePopup = !1;
            }), t.$mp.data = Object.assign({}, {
                $root: {
                    l1: e
                }
            });
        }, s = [];
    },
    c51c: function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("5f04"), a = i.n(n);
        for (var s in n) [ "default" ].indexOf(s) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(s);
        e.default = a.a;
    },
    ee45: function(t, e, i) {
        "use strict";
        (function(t, e) {
            var n = i("4ea4");
            i("18ba"), n(i("66fd"));
            var a = n(i("67c1"));
            t.__webpack_require_UNI_MP_PLUGIN__ = i, e(a.default);
        }).call(this, i("bc2e").default, i("543d").createPage);
    }
}, [ [ "ee45", "common/runtime", "common/vendor" ] ] ]);