(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/orderPreview/index" ], {
    3879: function(e, t, r) {
        "use strict";
        (function(e, t) {
            var n = r("4ea4");
            r("18ba"), n(r("66fd"));
            var i = n(r("3f31"));
            e.__webpack_require_UNI_MP_PLUGIN__ = r, t(i.default);
        }).call(this, r("bc2e").default, r("543d").createPage);
    },
    "3f31": function(e, t, r) {
        "use strict";
        r.r(t);
        var n = r("7627"), i = r("6f1e");
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(e) {
            r.d(t, e, function() {
                return i[e];
            });
        }(o);
        r("5b30");
        var s = r("f0c5"), u = Object(s.a)(i.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        t.default = u.exports;
    },
    "5b30": function(e, t, r) {
        "use strict";
        var n = r("8efa");
        r.n(n).a;
    },
    "6f1e": function(e, t, r) {
        "use strict";
        r.r(t);
        var n = r("dd60"), i = r.n(n);
        for (var o in n) [ "default" ].indexOf(o) < 0 && function(e) {
            r.d(t, e, function() {
                return n[e];
            });
        }(o);
        t.default = i.a;
    },
    7627: function(e, t, r) {
        "use strict";
        r.d(t, "b", function() {
            return i;
        }), r.d(t, "c", function() {
            return o;
        }), r.d(t, "a", function() {
            return n;
        });
        var n = {
            SelectAddress: function() {
                return r.e("components/SelectAddress/SelectAddress").then(r.bind(null, "8a38"));
            },
            SkuItem: function() {
                return Promise.all([ r.e("common/vendor"), r.e("components/SkuItem/SkuItem") ]).then(r.bind(null, "3c3e"));
            },
            PriceDisplay: function() {
                return r.e("components/PriceDisplay/PriceDisplay").then(r.bind(null, "6b05"));
            },
            UsableCouponPopup: function() {
                return r.e("components/UsableCouponPopup/UsableCouponPopup").then(r.bind(null, "3858"));
            },
            NoData: function() {
                return r.e("components/NoData/NoData").then(r.bind(null, "cafe"));
            }
        }, i = function() {
            var e = this, t = (e.$createElement, e._self._c, e.init && e.currentCoupon.id ? e._f("priceToFixed")(e.order.coupon_discount) : null), r = e.init && !e.currentCoupon.id ? e.usableCoupons.length : null, n = e.init && !e.currentCoupon.id && r ? e.usableCoupons.length : null, i = e.init && e.order.cover_discount ? e._f("priceToFixed")(e.order.cover_discount) : null, o = e.init ? e._f("priceToFixed")(e.order.redpack) : null, s = e.init && e.order.max_useable_score && e.order.score_discount ? e._f("priceToFixed")(e.order.score_discount) : null, u = e.init && 2 !== e.order.carriage_type ? e._f("priceToFixed")(e.order.carriage) : null, d = e.init ? e._f("priceToFixed")(e.order.price) : null;
            e._isMounted || (e.e0 = function(t) {
                e.couponsVisible = !0;
            }, e.e1 = function(t) {
                e.couponsVisible = !0;
            }, e.e2 = function(t) {
                e.couponsVisible = !1;
            }), e.$mp.data = Object.assign({}, {
                $root: {
                    f0: t,
                    g0: r,
                    g1: n,
                    f1: i,
                    f2: o,
                    f3: s,
                    f4: u,
                    f5: d
                }
            });
        }, o = [];
    },
    "8efa": function(e, t, r) {},
    dd60: function(e, t, r) {
        "use strict";
        (function(e) {
            var n = r("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var i = n(r("9523")), o = n(r("452d")), s = n(r("82c0")), u = r("26cb");
            function d(e, t) {
                var r = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), r.push.apply(r, n);
                }
                return r;
            }
            function c(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var r = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? d(Object(r), !0).forEach(function(t) {
                        (0, i.default)(e, t, r[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(r)) : d(Object(r)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(r, t));
                    });
                }
                return e;
            }
            var a = {
                mixins: [ o.default ],
                components: {
                    SplitLine: function() {
                        r.e("components/SplitLine/index").then(function() {
                            return resolve(r("2993"));
                        }.bind(null, r)).catch(r.oe);
                    },
                    IButton: function() {
                        r.e("components/Button/index").then(function() {
                            return resolve(r("93c4"));
                        }.bind(null, r)).catch(r.oe);
                    }
                },
                data: function() {
                    return {
                        address: {},
                        order: {
                            is_use_redpack: "unselect"
                        },
                        isNeedAddress: 0,
                        skus: [],
                        seckillId: "",
                        groupPriceUuid: "",
                        products: [],
                        remark: "",
                        init: !1,
                        couponsVisible: !1,
                        unusableCoupons: [],
                        usableCoupons: [],
                        currentCoupon: {},
                        current: 0,
                        animationData: {},
                        animationData2: {},
                        coverType: "",
                        _source: ""
                    };
                },
                computed: c(c({}, (0, u.mapGetters)([ "token" ])), {}, {
                    totalPrice: function() {
                        return this.order.price;
                    }
                }),
                onLoad: function(t) {
                    var r = this;
                    this.seckillId = t.seckillId, this.groupPriceUuid = t.groupPriceUuid, this.coverType = t.coverType, 
                    this.skus = JSON.parse(t.skus), this._source = t._source, e.$on("selectAddress", function(e) {
                        r.address = e, r.getOrderInfo();
                    });
                },
                onUnload: function() {
                    e.$off("selectAddress", function(e) {
                        console.log("移除 selectAddress 自定义事件");
                    });
                },
                onShow: function() {
                    this.token && !this.address.id && this.getOrderInfo();
                },
                methods: {
                    redpackSwitchChange: function(e) {
                        this.order.is_use_redpack = e.detail.value ? 1 : 0, this.getOrderInfo();
                    },
                    scoreSwitchChange: function(e) {
                        this.order.is_use_score = e.detail.value ? 1 : 0, this.getOrderInfo();
                    },
                    couponChange: function(e) {
                        e.id === this.currentCoupon.id ? (this.currentCoupon = {}, this.getOrderInfo()) : (this.currentCoupon = e, 
                        this.getOrderInfo());
                    },
                    createOrder: function() {
                        e.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.store", {
                            payment: "miniapp",
                            skus: this.skus,
                            coupon_id: this.currentCoupon && this.currentCoupon.id,
                            seckill_id: this.seckillId,
                            group_price_uuid: this.groupPriceUuid,
                            address_id: this.address && this.address.id,
                            remark: this.remark,
                            cover_type: this.coverType,
                            is_use_redpack: this.order.is_use_redpack,
                            is_use_score: this.order.is_use_score,
                            _source: this._source
                        }).then(function(t) {
                            var r = t.data, n = t.data.order;
                            if (!r.is_need_pay) return e.showToast({
                                title: "支付成功，正在跳转~",
                                icon: "none"
                            }), setTimeout(function() {
                                e.redirectTo({
                                    url: "/pages/orderDetail/index?uuid=" + n.uuid
                                });
                            }, 1500), !1;
                            s.default.pay(c(c({}, r), {}, {
                                pay_type: r.pay_type,
                                success: function() {
                                    e.showToast({
                                        title: "支付成功",
                                        icon: "none"
                                    }), setTimeout(function() {
                                        e.redirectTo({
                                            url: "/pages/orderList/index?status=deliver_pending"
                                        });
                                    }, 1500);
                                },
                                fail: function() {
                                    e.showToast({
                                        title: "支付失败",
                                        icon: "none"
                                    }), e.redirectTo({
                                        url: "/pages/orderDetail/index?uuid=" + n.uuid
                                    });
                                }
                            })), e.hideLoading();
                        }).catch(function(t) {
                            e.hideLoading();
                        });
                    },
                    payOrder: function() {
                        var t = this;
                        !this.isNeedAddress || this.address.id ? e.requestSubscribeMessage({
                            tmplIds: [ this.miniappSubscribeIds.order_delivered, this.miniappSubscribeIds.order_paid ],
                            complete: function(e) {
                                t.createOrder();
                            }
                        }) : e.showToast({
                            title: "未添加收货地址",
                            icon: "none"
                        });
                    },
                    getOrderInfo: function() {
                        var t = this;
                        e.showLoading({
                            title: "加载中",
                            mask: !0
                        }), this.$api.emit("order.preview.store", {
                            skus: this.skus,
                            address_id: this.address && this.address.id,
                            seckill_id: this.seckillId,
                            group_price_uuid: this.groupPriceUuid,
                            coupon_id: this.currentCoupon.id,
                            cover_type: this.coverType,
                            is_use_redpack: this.order.is_use_redpack,
                            is_use_score: this.order.is_use_score ? 1 : 0
                        }).then(function(r) {
                            e.hideLoading(), t.init = !0, t.order = r.data.order, t.products = r.data.order.skus, 
                            t.unusableCoupons = r.data.order.coupons.unusable, t.usableCoupons = r.data.order.coupons.usable, 
                            t.address = r.data.address, t.isNeedAddress = r.data.is_need_address, r.data.tips && e.showModal({
                                content: r.data.tips,
                                title: "温馨提示"
                            });
                        });
                    }
                }
            };
            t.default = a;
        }).call(this, r("543d").default);
    }
}, [ [ "3879", "common/runtime", "common/vendor" ] ] ]);