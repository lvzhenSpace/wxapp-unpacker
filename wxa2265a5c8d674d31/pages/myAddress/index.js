(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/myAddress/index" ], {
    "328f": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("b723"), s = n("620f");
        for (var a in s) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return s[t];
            });
        }(a);
        n("445e");
        var c = n("f0c5"), o = Object(c.a)(s.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = o.exports;
    },
    "445e": function(t, e, n) {
        "use strict";
        var i = n("e105");
        n.n(i).a;
    },
    6193: function(t, e, n) {
        "use strict";
        (function(t, e) {
            var i = n("4ea4");
            n("18ba"), i(n("66fd"));
            var s = i(n("328f"));
            t.__webpack_require_UNI_MP_PLUGIN__ = n, e(s.default);
        }).call(this, n("bc2e").default, n("543d").createPage);
    },
    "620f": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("fcc7"), s = n.n(i);
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        e.default = s.a;
    },
    b723: function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return s;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return n.e("components/NoData/NoData").then(n.bind(null, "cafe"));
            }
        }, s = function() {
            var t = this, e = (t.$createElement, t._self._c, t.__map(t.list, function(e, n) {
                return {
                    $orig: t.__get_orig(e),
                    f0: t._f("hidePhoneDetail")(e.phone)
                };
            })), n = !t.list.length && t.init;
            t.$mp.data = Object.assign({}, {
                $root: {
                    l0: e,
                    g0: n
                }
            });
        }, a = [];
    },
    e105: function(t, e, n) {},
    fcc7: function(t, e, n) {
        "use strict";
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = n("a7b1"), s = {
                components: {
                    NoData: function() {
                        n.e("components/NoData/NoData").then(function() {
                            return resolve(n("cafe"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                data: function() {
                    return {
                        list: [],
                        select: !1,
                        init: !1
                    };
                },
                filters: {
                    hidePhoneDetail: function(t) {
                        return t.substring(0, 3) + "****" + t.substring(7, 11);
                    }
                },
                onLoad: function(t) {
                    t.select && (this.select = !0);
                },
                onShow: function() {
                    var e = this;
                    t.showLoading({
                        title: "加载中"
                    }), (0, i.getAddressList)().then(function(n) {
                        e.list = n.data.addresses, e.init = !0, t.hideLoading();
                    });
                },
                methods: {
                    fetchData: function() {
                        var e = this;
                        (0, i.getAddressList)().then(function(n) {
                            e.list = n.data.addresses, e.init = !0, t.hideLoading();
                        });
                    },
                    selectAddress: function(e) {
                        this.select && (t.$emit("selectAddress", this.list[e.currentTarget.dataset.index]), 
                        t.navigateBack({
                            delta: 1
                        }));
                    },
                    handleEdit: function(t) {
                        var e = this.list[t];
                        this.$navigator.navigateTo({
                            url: "/pages/setAddress/index?uuid=".concat(e.uuid) + "&consignee=".concat(e.consignee) + "&phone=".concat(e.phone) + "&province=".concat(e.province) + "&city=".concat(e.city) + "&district=".concat(e.district) + "&address=".concat(e.address) + "&tag=".concat(e.tag) + "&is_default=".concat(e.is_default)
                        });
                    },
                    handleDelete: function(e) {
                        var n = this, s = e.currentTarget.dataset.index;
                        t.showModal({
                            title: "提示",
                            content: "删除该收货地址?",
                            success: function(e) {
                                e.confirm && (t.showLoading({
                                    title: "加载中"
                                }), (0, i.deleteAddress)(n.list[s].uuid).then(function(e) {
                                    n.list.splice(s, 1), t.hideLoading();
                                }));
                            }
                        });
                    },
                    handleNewAddress: function() {
                        t.navigateTo({
                            url: "/pages/setAddress/index"
                        });
                    },
                    $getAddress: function(t) {
                        console.log("main get address =======>"), my.authorize({
                            scopes: "scope.addressList",
                            success: function(e) {
                                my.tb.chooseAddress({
                                    addAddress: "show",
                                    searchAddress: "hide",
                                    locateAddress: "hide"
                                }, function(e) {
                                    t.success && t.success(e);
                                }, function(e) {
                                    t.fail && t.fail(e);
                                });
                            }
                        });
                    },
                    handleImportAddress: function() {
                        var e = this;
                        t.chooseAddress({
                            success: function(t) {
                                if (t.error) return !1;
                                (0, i.createAddress)({
                                    consignee: t.userName,
                                    phone: t.telNumber,
                                    province: t.provinceName,
                                    city: t.cityName,
                                    district: t.countyName,
                                    address: t.detailInfo,
                                    tag: "",
                                    is_default: 0
                                }).then(function(t) {
                                    e.fetchData();
                                });
                            }
                        });
                    }
                }
            };
            e.default = s;
        }).call(this, n("543d").default);
    }
}, [ [ "6193", "common/runtime", "common/vendor" ] ] ]);