(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/page/index" ], {
    1969: function(n, t, e) {
        "use strict";
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        uuid: "",
                        info: {},
                        refreshCounter: 1
                    };
                },
                computed: {
                    page: function() {
                        return this.info.content || {};
                    }
                },
                watch: {},
                onLoad: function(n) {
                    this.uuid = n.uuid, this.initData();
                },
                onShow: function() {
                    this.refreshCounter++;
                },
                methods: {
                    initData: function() {
                        var t = this;
                        n.showLoading({
                            title: "加载中"
                        }), this.$http("/pages/".concat(this.uuid)).then(function(e) {
                            console.log(e), t.info = e.data.info, n.hideLoading();
                        });
                    }
                },
                onPageScroll: function(n) {}
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    "4b00": function(n, t, e) {
        "use strict";
        e.r(t);
        var o = e("1969"), i = e.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(a);
        t.default = i.a;
    },
    "5da4": function(n, t, e) {
        "use strict";
        var o = e("5dd4");
        e.n(o).a;
    },
    "5dd4": function(n, t, e) {},
    "67d5": function(n, t, e) {
        "use strict";
        e.r(t);
        var o = e("8faf"), i = e("4b00");
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(n) {
            e.d(t, n, function() {
                return i[n];
            });
        }(a);
        e("5da4");
        var u = e("f0c5"), r = Object(u.a)(i.default, o.b, o.c, !1, null, "7a35360e", null, !1, o.a, void 0);
        t.default = r.exports;
    },
    "8faf": function(n, t, e) {
        "use strict";
        e.d(t, "b", function() {
            return i;
        }), e.d(t, "c", function() {
            return a;
        }), e.d(t, "a", function() {
            return o;
        });
        var o = {
            PageRender: function() {
                return Promise.all([ e.e("common/vendor"), e.e("components/PageRender/PageRender") ]).then(e.bind(null, "b4aa"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, a = [];
    },
    d66e: function(n, t, e) {
        "use strict";
        (function(n, t) {
            var o = e("4ea4");
            e("18ba"), o(e("66fd"));
            var i = o(e("67d5"));
            n.__webpack_require_UNI_MP_PLUGIN__ = e, t(i.default);
        }).call(this, e("bc2e").default, e("543d").createPage);
    }
}, [ [ "d66e", "common/runtime", "common/vendor" ] ] ]);