(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/Danmus/Danmus" ], {
    "4bc1": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("5d3f"), r = e("9fda");
        for (var a in r) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return r[t];
            });
        }(a);
        e("b0a3");
        var c = e("f0c5"), u = Object(c.a)(r.default, o.b, o.c, !1, null, "0937ce10", null, !1, o.a, void 0);
        n.default = u.exports;
    },
    "5d3f": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return r;
        }), e.d(n, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, r = [];
    },
    "9fda": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("c940"), r = e.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        n.default = r.a;
    },
    b0a3: function(t, n, e) {
        "use strict";
        var o = e("e8f2");
        e.n(o).a;
    },
    c940: function(t, n, e) {
        "use strict";
        (function(t) {
            var o = e("4ea4");
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var r = o(e("9523"));
            function a(t, n) {
                var e = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(t);
                    n && (o = o.filter(function(n) {
                        return Object.getOwnPropertyDescriptor(t, n).enumerable;
                    })), e.push.apply(e, o);
                }
                return e;
            }
            function c(t) {
                for (var n = 1; n < arguments.length; n++) {
                    var e = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? a(Object(e), !0).forEach(function(n) {
                        (0, r.default)(t, n, e[n]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(e)) : a(Object(e)).forEach(function(n) {
                        Object.defineProperty(t, n, Object.getOwnPropertyDescriptor(e, n));
                    });
                }
                return t;
            }
            var u = {
                name: "Danmus",
                props: {
                    list: Array,
                    uuid: String,
                    setting: Object
                },
                data: function() {
                    return {
                        danmus: [],
                        customBar: 64,
                        isShow: !1
                    };
                },
                watch: {
                    list: {
                        handler: function(t) {
                            t && this.start();
                        },
                        deep: !0
                    }
                },
                created: function() {
                    this.customBar = this.$store.getters.deviceInfo.customBar, this.start();
                },
                methods: {
                    start: function() {
                        var t = this;
                        this.danmus = [], this.isShow = !1;
                        for (var n = this.list, e = [], o = 0, r = 0; r < n.length; r++) {
                            var a = this.customBar + r % 4 * 42, u = 9 + 3 * Math.random(), i = 0 + 2 * Math.random() + r;
                            o = o > u + i ? o : u + i, e.push(c(c({}, n[r]), {}, {
                                style: "background: ".concat(n[r].bg_color, ";top:").concat(a, "px;animation-duration:").concat(u, "s;animation-delay:").concat(i, "s;")
                            }));
                        }
                        this.$forceUpdate(), setTimeout(function() {
                            t.danmus = e, t.isShow = !0;
                        }, 100), o -= 2.5, setTimeout(function() {
                            t.start(), t.$forceUpdate();
                        }, parseInt(1e3 * o));
                    },
                    clickItem: function(n) {
                        if (n.uuid === this.uuid) return !1;
                        "box" === n.type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + n.uuid
                        });
                    }
                }
            };
            n.default = u;
        }).call(this, e("543d").default);
    },
    e8f2: function(t, n, e) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/Danmus/Danmus-create-component", {
    "components/Danmus/Danmus-create-component": function(t, n, e) {
        e("543d").createComponent(e("4bc1"));
    }
}, [ [ "components/Danmus/Danmus-create-component" ] ] ]);