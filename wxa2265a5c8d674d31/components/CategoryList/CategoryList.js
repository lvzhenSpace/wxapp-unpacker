(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/CategoryList/CategoryList" ], {
    "4f8e4": function(t, e, n) {},
    5060: function(t, e, n) {
        "use strict";
        var o = n("4f8e4");
        n.n(o).a;
    },
    "77b7": function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("fd9c"), i = n("c221");
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(a);
        n("5060");
        var c = n("f0c5"), r = Object(c.a)(i.default, o.b, o.c, !1, null, "7a69af76", null, !1, o.a, void 0);
        e.default = r.exports;
    },
    c156: function(t, e, n) {
        "use strict";
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: {
                    refreshCounter: {
                        type: Number
                    },
                    module: {
                        type: Object
                    }
                },
                data: function() {
                    return {
                        list: []
                    };
                },
                watch: {
                    refreshCounter: function() {
                        this.initData();
                    }
                },
                mounted: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        var t = this;
                        if ("custom" === this.module.list_mode) {
                            var e = this.module.list || [ 0 ];
                            this.$http("/normal/categories", "GET", {
                                per_page: 10,
                                level_mode: "all",
                                ids: e
                            }).then(function(e) {
                                t.list = e.data.list;
                            });
                        } else this.$http("/normal/categories", "GET", {
                            per_page: 10
                        }).then(function(e) {
                            t.list = e.data.list;
                        });
                    },
                    handleClick: function(e) {
                        1 === e.level ? t.navigateTo({
                            url: "/pages/category/index?category_id=".concat(e.id, "&title=").concat(e.title)
                        }) : 2 === e.level && t.navigateTo({
                            url: "/pages/search/index?category_id=".concat(e.id, "&title=").concat(e.title)
                        });
                    },
                    more: function(e) {
                        t.navigateTo({
                            url: "/pages/ip/index"
                        });
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    c221: function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("c156"), i = n.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        e.default = i.a;
    },
    fd9c: function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return i;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/CategoryList/CategoryList-create-component", {
    "components/CategoryList/CategoryList-create-component": function(t, e, n) {
        n("543d").createComponent(n("77b7"));
    }
}, [ [ "components/CategoryList/CategoryList-create-component" ] ] ]);