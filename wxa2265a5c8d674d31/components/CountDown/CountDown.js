(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/CountDown/CountDown" ], {
    "2fed": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("541d"), o = n("f72b");
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(c);
        n("455c");
        var r = n("f0c5"), u = Object(r.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = u.exports;
    },
    "455c": function(t, e, n) {
        "use strict";
        var i = n("6b7f");
        n.n(i).a;
    },
    "541d": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var i = function() {
            this.$createElement, this._self._c;
        }, o = [];
    },
    "6b7f": function(t, e, n) {},
    "6d87": function(t, e, n) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var i = {
            name: "CountDown",
            components: {
                DefaultTheme: function() {
                    n.e("components/CountDown/theme/Default").then(function() {
                        return resolve(n("e9b9"));
                    }.bind(null, n)).catch(n.oe);
                },
                ProductDetailTheme1: function() {
                    n.e("components/CountDown/theme/ProductDetailTheme1").then(function() {
                        return resolve(n("1b07"));
                    }.bind(null, n)).catch(n.oe);
                },
                ZhuliTheme: function() {
                    n.e("components/CountDown/theme/ZhuliTheme").then(function() {
                        return resolve(n("fbfc"));
                    }.bind(null, n)).catch(n.oe);
                }
            },
            props: {
                start: {
                    type: String
                },
                end: {
                    type: String
                },
                endText: {
                    type: String,
                    default: "活动已结束"
                },
                theme: {
                    type: String,
                    default: "default"
                }
            },
            data: function() {
                return {
                    status: "pending",
                    timer: null,
                    startSecondTime: 0,
                    endSecondTime: 0,
                    timeObj: {
                        day: 0,
                        hour: 0,
                        minute: 0,
                        second: 0
                    }
                };
            },
            computed: {},
            mounted: function() {
                this.calcTime();
            },
            watch: {
                start: {
                    handler: function(t) {
                        var e = this;
                        clearInterval(this.timer), t && (this.startSecondTime = this.getSecondTime(t), this.endSecondTime = this.getSecondTime(this.end), 
                        this.calcTime(), this.timer = setInterval(function() {
                            e.calcTime();
                        }, 1e3));
                    },
                    immediate: !0
                },
                status: {
                    handler: function(t) {
                        this.$emit("change", t);
                    },
                    immediate: !0
                }
            },
            destroyed: function() {
                clearInterval(this.timer);
            },
            methods: {
                getSecondTime: function(t) {
                    if ("string" == typeof t) var e = t.split(/[- :]/), n = new Date(e[0], e[1] - 1, e[2], e[3], e[4], e[5]); else n = new Date();
                    return n.getTime() / 1e3;
                },
                calcTime: function() {
                    var t = this.getSecondTime();
                    if (this.endSecondTime < t) return this.status = "expired", void clearInterval(this.timer);
                    if (this.startSecondTime < t) {
                        this.status = "working";
                        var e = this.endSecondTime - t;
                        this.timeObj = this.formatTimeDiff(e);
                    } else if (this.startSecondTime > t) {
                        this.status = "pending";
                        var n = this.startSecondTime - t;
                        this.timeObj = this.formatTimeDiff(n);
                    }
                },
                formatTimeDiff: function(t) {
                    var e = parseInt(t / 86400);
                    return {
                        day: e,
                        hour: parseInt(t / 3600) - 24 * e,
                        minute: parseInt(t % 3600 / 60),
                        second: parseInt(t % 60)
                    };
                }
            }
        };
        e.default = i;
    },
    f72b: function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("6d87"), o = n.n(i);
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(c);
        e.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/CountDown/CountDown-create-component", {
    "components/CountDown/CountDown-create-component": function(t, e, n) {
        n("543d").createComponent(n("2fed"));
    }
}, [ [ "components/CountDown/CountDown-create-component" ] ] ]);