(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ActivityList/ActivityList" ], {
    "4d0f": function(t, i, e) {
        "use strict";
        e.d(i, "b", function() {
            return n;
        }), e.d(i, "c", function() {
            return a;
        }), e.d(i, "a", function() {});
        var n = function() {
            this.$createElement;
            var t = (this._self._c, 1 == this.homeNum ? "wrap" === this.wrapMode && this.list.length > 0 : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, a = [];
    },
    "5f72": function(t, i, e) {
        "use strict";
        (function(t) {
            var n = e("4ea4");
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var a = n(e("2eee")), u = n(e("c973")), c = {
                props: {
                    ids: {
                        type: Array
                    },
                    module: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    modulesArr: {
                        type: Array
                    },
                    homeNum: {
                        type: Number,
                        default: 0
                    },
                    numberType: Number,
                    refreshCounter: Number,
                    getNextPageCounter: Number
                },
                components: {
                    Row1: function() {
                        e.e("components/ActivityItem/Row1").then(function() {
                            return resolve(e("7111"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    Grid1: function() {
                        e.e("components/ActivityItem/Grid1").then(function() {
                            return resolve(e("850c"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    Grid2: function() {
                        e.e("components/ActivityItem/Grid2").then(function() {
                            return resolve(e("fcdb"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    Grid3: function() {
                        e.e("components/ActivityItem/Grid3").then(function() {
                            return resolve(e("2dcd"));
                        }.bind(null, e)).catch(e.oe);
                    },
                    Row1Seckill: function() {
                        e.e("components/ActivityItem/Row1Seckill").then(function() {
                            return resolve(e("fd81"));
                        }.bind(null, e)).catch(e.oe);
                    }
                },
                data: function() {
                    return {
                        result: [],
                        page: 1,
                        list: [],
                        peilaArr: [],
                        yifanArr: [],
                        titleList: [ "全部", "无限赏", "一番赏" ],
                        current: 0,
                        type: "all"
                    };
                },
                mounted: function() {
                    this.initData(), this.oldInitData();
                },
                computed: {
                    isScroll: function() {
                        return "scroll" == this.module.display;
                    },
                    grid: function() {
                        return this.module.grid || "grid3";
                    },
                    wrapMode: function() {
                        return this.module.wrap_mode || "wrap";
                    },
                    url: function() {
                        return {
                            yifanshang: "/yifanshangs",
                            lottery: "/lotteries",
                            seckill: "/seckills",
                            egg_lottery: "/egg-lotteries",
                            jika: "/jikas",
                            fudai: "/fudais",
                            rotate_lottery: "/rotate-lotteries",
                            box: "/boxes",
                            zhuli: "/zhuli/activities"
                        }[this.module.activity_type];
                    },
                    activityType: function() {
                        return this.module.activity_type || "";
                    }
                },
                watch: {
                    numberType: function(t, i) {
                        0 == t && this.activityTypes(), 1 == t ? this.fudaiApi() : 2 == t ? this.yifanshangApi() : 3 == t ? this.peilaApi() : 4 == t && this.renyimenApi();
                    },
                    ids: function() {
                        this.initData(), this.oldInitData();
                    },
                    refreshCounter: function() {
                        this.oldInitData(), this.initData();
                    },
                    getNextPageCounter: function(t) {
                        "all" === this.module.list_content && this.getNextPage();
                    }
                },
                methods: {
                    yifanshangApi: function() {
                        var t = this;
                        t.list = [], t.modulesArr.forEach(function(i) {
                            i.activity_type && "yifanshang" == i.activity_type && t.$http("/yifanshangs", "GET", {
                                per_page: 100,
                                ids: i.list
                            }).then(function(i) {
                                t.yifanArr = i.data.list.concat(i.data.list), i.data.list.forEach(function(i) {
                                    0 == i.yfs_type && t.list.push(i);
                                });
                            });
                        });
                    },
                    peilaApi: function() {
                        var t = this;
                        t.list = [], t.modulesArr.forEach(function(i) {
                            i.activity_type && "yifanshang" == i.activity_type && t.$http("/yifanshangs", "GET", {
                                per_page: 100,
                                ids: i.list
                            }).then(function(i) {
                                t.peilaArr = i.data.list.concat(i.data.list), i.data.list.forEach(function(i) {
                                    1 == i.yfs_type && t.list.push(i);
                                });
                            });
                        });
                    },
                    renyimenApi: function() {
                        var t = this;
                        t.list = [], t.modulesArr.forEach(function(i) {
                            i.activity_type && "renyi" == i.activity_type && t.$http("/renyis", "GET", {
                                per_page: 100,
                                ids: i.list
                            }).then(function(i) {
                                t.list = i.data.list;
                            });
                        });
                    },
                    fudaiApi: function() {
                        var t = this;
                        t.list = [], t.modulesArr.forEach(function(i) {
                            i.activity_type && "fudai" == i.activity_type && t.$http("/fudais", "GET", {
                                per_page: 100,
                                ids: i.list
                            }).then(function(i) {
                                t.list = i.data.list;
                            });
                        });
                    },
                    init1: function() {
                        var t = this;
                        return (0, u.default)(a.default.mark(function i() {
                            return a.default.wrap(function(i) {
                                for (;;) switch (i.prev = i.next) {
                                  case 0:
                                    return i.abrupt("return", new Promise(function(i, e) {
                                        t.modulesArr.forEach(function(e) {
                                            "fudai" == e.activity_type && t.$http("/fudais", "GET", {
                                                per_page: 100,
                                                ids: e.list
                                            }).then(function(t) {
                                                i(t.data.list);
                                            });
                                        });
                                    }));

                                  case 1:
                                  case "end":
                                    return i.stop();
                                }
                            }, i);
                        }))();
                    },
                    init2: function() {
                        var t = this;
                        return (0, u.default)(a.default.mark(function i() {
                            var e;
                            return a.default.wrap(function(i) {
                                for (;;) switch (i.prev = i.next) {
                                  case 0:
                                    return e = [], i.abrupt("return", new Promise(function(i, n) {
                                        t.modulesArr.forEach(function(t) {
                                            "yifanshang" == t.activity_type && e.push(t.list);
                                        }), t.$http("/yifanshangs", "GET", {
                                            per_page: 100,
                                            ids: e.join(",")
                                        }).then(function(t) {
                                            i(t.data.list);
                                        });
                                    }));

                                  case 2:
                                  case "end":
                                    return i.stop();
                                }
                            }, i);
                        }))();
                    },
                    init3: function() {
                        var t = this;
                        return new Promise(function(i, e) {
                            t.modulesArr.forEach(function(e) {
                                "renyi" == e.activity_type && t.$http("/renyis", "GET", {
                                    per_page: 100,
                                    ids: e.list
                                }).then(function(t) {
                                    i(t.data.list);
                                });
                            });
                        });
                    },
                    activityTypes: function() {
                        var t = this;
                        this.list = [], Promise.all([ this.init1(), this.init3(), this.init2() ]).then(function(i) {
                            i.forEach(function(i) {
                                i.forEach(function(i) {
                                    t.list.push(i);
                                });
                            });
                        }).catch(function(t) {
                            console.log(t, "eerr");
                        });
                    },
                    initData: function() {
                        this.activityTypes();
                    },
                    oldInitData: function() {
                        var t = this;
                        "all" === this.module.list_content && "fudai" != this.module.activity_type ? (this.page = 1, 
                        this.$http(this.url, "GET", {
                            page: this.page,
                            per_page: 100
                        }).then(function(i) {
                            t.list = i.data.list;
                        })) : this.ids && this.ids.length > 0 && "fudai" != this.module.activity_type && this.$http(this.url, "GET", {
                            per_page: 100,
                            ids: this.ids
                        }).then(function(i) {
                            t.list = i.data.list;
                        });
                    },
                    clickItem: function(i) {
                        var e = "";
                        console.log(i), "yifanshang" === i.activity_type && 0 == i.yfs_type ? e = "/pages/yifanshang/detail?uuid=".concat(i.uuid) : "yifanshang" === i.activity_type && 1 == i.yfs_type ? e = "/pages/malishang/detail?uuid=".concat(i.uuid) : "lottery" === i.activity_type ? e = "/pages/lottery/detail?uuid=".concat(i.uuid) : "egg_lottery" === i.activity_type ? e = "/pages/eggLottery/detail?uuid=".concat(i.uuid) : "seckill" === i.activity_type ? e = "/pages/seckill/detail?uuid=".concat(i.uuid) : "jika" === i.activity_type ? e = "/pages/jika/detail?uuid=".concat(i.uuid) : "fudai" === i.activity_type && 0 == i.is_lord ? e = "/pages/fudai/detail?uuid=".concat(i.uuid) : "fudai" === i.activity_type && 1 == i.is_lord ? e = "/pages/wuxianshang/detail?uuid=".concat(i.uuid) : "renyi" === i.activity_type ? e = "/pages/renyimen/detail?uuid=".concat(i.uuid) : "rotate_lottery" === i.activity_type ? e = "/pages/rotateLottery/detail?uuid=".concat(i.uuid) : "box" === i.activity_type ? e = "/pages/boxDetail/index?uuid=".concat(i.uuid) : "zhuli" === i.activity_type && (e = "/pages/zhuli/detail?uuid=".concat(i.uuid)), 
                        t.navigateTo({
                            url: e
                        });
                    },
                    getNextPage: function() {
                        var t = this;
                        this.page++, this.$http(this.url, "GET", {
                            page: this.page,
                            per_page: 100
                        }).then(function(i) {
                            t.list = t.list.concat(i.data.list);
                        });
                    }
                }
            };
            i.default = c;
        }).call(this, e("543d").default);
    },
    "6c275": function(t, i, e) {},
    "7e17": function(t, i, e) {
        "use strict";
        var n = e("6c275");
        e.n(n).a;
    },
    bd49: function(t, i, e) {
        "use strict";
        e.r(i);
        var n = e("5f72"), a = e.n(n);
        for (var u in n) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(i, t, function() {
                return n[t];
            });
        }(u);
        i.default = a.a;
    },
    eec7: function(t, i, e) {
        "use strict";
        e.r(i);
        var n = e("4d0f"), a = e("bd49");
        for (var u in a) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(i, t, function() {
                return a[t];
            });
        }(u);
        e("7e17");
        var c = e("f0c5"), s = Object(c.a)(a.default, n.b, n.c, !1, null, "1bd09a99", null, !1, n.a, void 0);
        i.default = s.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ActivityList/ActivityList-create-component", {
    "components/ActivityList/ActivityList-create-component": function(t, i, e) {
        e("543d").createComponent(e("eec7"));
    }
}, [ [ "components/ActivityList/ActivityList-create-component" ] ] ]);