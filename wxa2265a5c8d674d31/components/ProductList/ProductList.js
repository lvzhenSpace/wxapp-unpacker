(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ProductList/ProductList" ], {
    "0cf8": function(t, n, e) {
        "use strict";
        e.r(n);
        var i = e("2323"), o = e("96b5");
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(u);
        e("3d01");
        var c = e("f0c5"), r = Object(c.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        n.default = r.exports;
    },
    2323: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {
            return i;
        });
        var i = {
            ProductItem: function() {
                return e.e("components/ProductItem/ProductItem").then(e.bind(null, "9c3e"));
            }
        }, o = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    2386: function(t, n, e) {},
    "3d01": function(t, n, e) {
        "use strict";
        var i = e("2386");
        e.n(i).a;
    },
    "96b5": function(t, n, e) {
        "use strict";
        e.r(n);
        var i = e("c636"), o = e.n(i);
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(u);
        n.default = o.a;
    },
    c636: function(t, n, e) {
        "use strict";
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var i = {
            props: {
                ids: {
                    type: Array
                },
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                refreshCounter: Number,
                getNextPageCounter: Number
            },
            data: function() {
                return {
                    page: 1,
                    list: []
                };
            },
            mounted: function() {
                this.initData();
            },
            computed: {
                isScroll: function() {
                    return "scroll" == this.module.theme;
                },
                grid: function() {
                    return this.module.grid || "grid3";
                },
                wrapMode: function() {
                    return this.module.wrap_mode || "wrap";
                }
            },
            watch: {
                ids: function() {
                    this.initData();
                },
                refreshCounter: function() {
                    this.initData();
                },
                getNextPageCounter: function(t) {
                    "all" === this.module.list_content && this.getNextPage();
                }
            },
            methods: {
                initData: function() {
                    var t = this;
                    "all" === this.module.list_content ? (this.page = 1, this.$http("/shop/products", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(n) {
                        t.list = n.data.list;
                    })) : this.ids && this.ids.length > 0 && this.$http("/shop/products", "GET", {
                        per_page: 80,
                        ids: this.ids
                    }).then(function(n) {
                        t.list = n.data.list;
                    });
                },
                clickItem: function() {
                    this.$playAudio("click");
                },
                getNextPage: function() {
                    var t = this;
                    this.page++, this.$http("/shop/products", "GET", {
                        page: this.page,
                        per_page: 12
                    }).then(function(n) {
                        t.list = t.list.concat(n.data.list);
                    });
                }
            }
        };
        n.default = i;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ProductList/ProductList-create-component", {
    "components/ProductList/ProductList-create-component": function(t, n, e) {
        e("543d").createComponent(e("0cf8"));
    }
}, [ [ "components/ProductList/ProductList-create-component" ] ] ]);