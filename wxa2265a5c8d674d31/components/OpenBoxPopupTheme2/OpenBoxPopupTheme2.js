(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/OpenBoxPopupTheme2/OpenBoxPopupTheme2" ], {
    "6f84": function(e, t, n) {
        "use strict";
        var o = n("d2f1");
        n.n(o).a;
    },
    "6fe8": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return r;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            ReturnSalePopup: function() {
                return n.e("components/ReturnSalePopup/ReturnSalePopup").then(n.bind(null, "f4ef"));
            }
        }, r = function() {
            var e = this;
            e.$createElement, e._self._c, e._isMounted || (e.e0 = function(t) {
                e.isShowReturnSale = !1;
            }, e.e1 = function(t) {
                e.isReturnSaleSuccess = !0;
            });
        }, i = [];
    },
    8343: function(e, t, n) {
        "use strict";
        (function(e) {
            function n(e, t) {
                var n = "undefined" != typeof Symbol && e[Symbol.iterator] || e["@@iterator"];
                if (!n) {
                    if (Array.isArray(e) || (n = function(e, t) {
                        if (e) {
                            if ("string" == typeof e) return o(e, t);
                            var n = Object.prototype.toString.call(e).slice(8, -1);
                            return "Object" === n && e.constructor && (n = e.constructor.name), "Map" === n || "Set" === n ? Array.from(e) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? o(e, t) : void 0;
                        }
                    }(e)) || t && e && "number" == typeof e.length) {
                        n && (e = n);
                        var r = 0, i = function() {};
                        return {
                            s: i,
                            n: function() {
                                return r >= e.length ? {
                                    done: !0
                                } : {
                                    done: !1,
                                    value: e[r++]
                                };
                            },
                            e: function(e) {
                                throw e;
                            },
                            f: i
                        };
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
                }
                var a, s = !0, u = !1;
                return {
                    s: function() {
                        n = n.call(e);
                    },
                    n: function() {
                        var e = n.next();
                        return s = e.done, e;
                    },
                    e: function(e) {
                        u = !0, a = e;
                    },
                    f: function() {
                        try {
                            s || null == n.return || n.return();
                        } finally {
                            if (u) throw a;
                        }
                    }
                };
            }
            function o(e, t) {
                (null == t || t > e.length) && (t = e.length);
                for (var n = 0, o = new Array(t); n < t; n++) o[n] = e[n];
                return o;
            }
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = {
                props: {
                    boxImg: String,
                    buttonTitle: String,
                    order: Object,
                    tryMode: Boolean,
                    tryInfo: Object,
                    info: Object,
                    isNavbarEnable: !1,
                    levelskus: Array,
                    sku_level: Array,
                    doubleBoxCard: Number,
                    shangbaoType: String
                },
                data: function() {
                    return {
                        bkImgs: [ "url(https://img121.7dun.com/20230207NewImg/huanxiang/wuxianR.png)", "url(https://img121.7dun.com/20230207NewImg/huanxiang/wuxianSr.png)", "url(https://img121.7dun.com/20230207NewImg/huanxiang/wuxianSsr.png)", "url(https://img121.7dun.com/20230207NewImg/huanxiang/wuxianUr.png)", "url()" ],
                        isOpen: !1,
                        showResult: !1,
                        status: 0,
                        isShowReturnSale: !1,
                        isReturnSaleSuccess: !1,
                        package: {},
                        defaultBoxImage: "https://cdn2.hquesoft.com/box/openbox.png",
                        isNotOpen: !1,
                        selectedIds: [],
                        total_money_price: 0,
                        total_score_price: 0,
                        total_score_score: 0,
                        myInfo: {}
                    };
                },
                mounted: function() {
                    this.initData(), this.getMyInfo();
                },
                computed: {
                    skus: function() {
                        return this.package.skus || [];
                    },
                    orderConfig: function() {
                        return this.$store.getters.setting.order;
                    },
                    rewardJikaTimes: function() {
                        return this.package.reward && this.package.reward.jika_times;
                    },
                    rewardLotteryTicket: function() {
                        return this.package.reward && this.package.reward.lottery_ticket;
                    }
                },
                methods: {
                    getMyInfo: function() {
                        var e = this;
                        this.$http("/task/level_list?no_list=1").then(function(t) {
                            e.myInfo = t.data.my_task_level_info;
                        }).catch(function(e) {
                            console.log(e);
                        });
                    },
                    viewFreeOrderDetail: function() {
                        this.info.money_price ? e.navigateTo({
                            url: "/pages/myRedpack/index"
                        }) : e.navigateTo({
                            url: "/pages/myScore/index"
                        });
                    },
                    initData: function() {
                        var t = this;
                        this.tryMode ? (e.showLoading(), this.$http("/try/packages/".concat(this.tryInfo.package_uuid)).then(function(n) {
                            t.package = n.data, t.startOpenAnimate(), e.hideLoading();
                        })) : this.$http("/asset/package?order_id=".concat(this.order.id)).then(function(e) {
                            t.package = e.data;
                            for (var o = 0; o < t.levelskus.length; o++) for (var r = 0; r < t.package.skus.length; r++) if (t.levelskus[o].title == t.package.skus[r].title) {
                                t.package.skus[r].level = t.levelskus[o].level;
                                for (var i = 0; i < t.sku_level.length; i++) t.sku_level[i].level == t.package.skus[r].level && (t.package.skus[r].level_icon = t.sku_level[i].icon, 
                                t.package.skus[r].level_title = t.sku_level[i].title);
                            }
                            console.log(t.package.skus, "666"), t.startOpenAnimate();
                            var a, s = n(t.package.skus);
                            try {
                                for (s.s(); !(a = s.n()).done; ) {
                                    var u = a.value;
                                    if (t.total_score_score += u.fj_score * u.total, t.total_money_price += u.money_price * u.total, 
                                    t.total_score_price += u.score_price * u.total, u.money_price > 1e4) for (var c = 0; c < u.total; c++) {
                                        var l = {
                                            order_id: e.data.id,
                                            thumb: u.thumb,
                                            title: u.title,
                                            money_price: t.money_price_title(u.title)
                                        };
                                        t.getScorePack(u.money_price, JSON.stringify(l));
                                    }
                                }
                            } catch (e) {
                                s.e(e);
                            } finally {
                                s.f();
                            }
                            console.log("this.levelskus=>", t.levelskus), 1 == t.doubleBoxCard && t.usecard(t.total_money_price, t.total_score_price);
                        }).catch(function(e) {
                            t.isNotOpen = !0;
                        });
                    },
                    money_price_title: function(e) {
                        for (var t = 0; t < this.levelskus.length; t++) if (this.levelskus[t].title == e) return this.levelskus[t].money_price;
                        return 0;
                    },
                    getScorePack: function(t, n) {
                        "renyi" != this.shangbaoType && (e.showToast({
                            title: "恭喜你获取到一个赏包",
                            icon: "none"
                        }), this.$http("/create_score_redpack", "post", {
                            worth: t / 100,
                            description: n
                        }).then(function(e) {}));
                    },
                    usecard: function(e, t) {
                        this.$http("/fudai/use-card", "post", {
                            card_type: "double_box",
                            money_price: e,
                            score_price: t
                        }).then(function(e) {});
                    },
                    onekeyrecycle: function() {
                        var t = this;
                        e.showLoading({
                            title: "分解中...",
                            success: function(o) {
                                var r, i = n(t.skus);
                                try {
                                    for (i.s(); !(r = i.n()).done; ) {
                                        var a = r.value;
                                        t.selectedIds.push(a.id);
                                    }
                                } catch (e) {
                                    i.e(e);
                                } finally {
                                    i.f();
                                }
                                t.$http("/asset/return-sale/confirm", "post", {
                                    ids: t.selectedIds
                                }).then(function(n) {
                                    t.isReturnSaleSuccess = 0, e.showToast({
                                        title: "分解成功"
                                    }), t.refresh(), t.isSelectMode = !1, t.$emit("refresh");
                                });
                            }
                        }), this.close();
                    },
                    startOpenAnimate: function() {
                        var e = this;
                        setTimeout(function() {
                            e.status = 1, setTimeout(function() {
                                e.isOpen = !0, e.$playAudio("open"), setTimeout(function() {
                                    e.showResult = !0;
                                }, 300);
                            }, 200);
                        }, 100);
                    },
                    handleRefresh: function() {
                        this.isNotOpen = !1, this.initData();
                    },
                    goLotteryDetail: function() {
                        e.navigateTo({
                            url: "/pages/lottery/detail?uuid=" + this.rewardLotteryTicket.uuid
                        });
                    },
                    goJikaDetail: function() {
                        e.navigateTo({
                            url: "/pages/jika/detail?uuid=" + this.rewardJikaTimes.uuid
                        });
                    },
                    returnSale: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    handleOk: function() {
                        e.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    goBack: function() {
                        e.navigateBack({
                            delta: 1
                        });
                    },
                    close: function() {
                        this.$emit("close"), this.$emit("refresh");
                    },
                    checkSku: function(t) {
                        "score" === t.sku_type ? e.navigateTo({
                            url: "/pages/myScore/index"
                        }) : "coupon" === t.sku_type ? e.navigateTo({
                            url: "/pages/myCoupons/index"
                        }) : "redpack" === t.sku_type && e.navigateTo({
                            url: "/pages/myRedpack/index"
                        });
                    }
                }
            };
            t.default = r;
        }).call(this, n("543d").default);
    },
    d2f1: function(e, t, n) {},
    d4b0: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("8343"), r = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        t.default = r.a;
    },
    e39b: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("6fe8"), r = n("d4b0");
        for (var i in r) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(i);
        n("6f84");
        var a = n("f0c5"), s = Object(a.a)(r.default, o.b, o.c, !1, null, "645b880e", null, !1, o.a, void 0);
        t.default = s.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/OpenBoxPopupTheme2/OpenBoxPopupTheme2-create-component", {
    "components/OpenBoxPopupTheme2/OpenBoxPopupTheme2-create-component": function(e, t, n) {
        n("543d").createComponent(n("e39b"));
    }
}, [ [ "components/OpenBoxPopupTheme2/OpenBoxPopupTheme2-create-component" ] ] ]);