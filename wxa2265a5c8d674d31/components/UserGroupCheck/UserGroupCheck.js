(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/UserGroupCheck/UserGroupCheck" ], {
    "0054": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("a23a"), i = n.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = i.a;
    },
    "5c2b": function(e, t, n) {
        "use strict";
        var o = n("e890");
        n.n(o).a;
    },
    a23a: function(e, t, n) {
        "use strict";
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = {
                components: {},
                data: function() {
                    return {
                        info: {},
                        isPassed: 0,
                        options: {},
                        isInit: !1
                    };
                },
                props: {
                    userGroupId: {
                        type: Number
                    },
                    title: {
                        type: String
                    }
                },
                computed: {},
                watch: {},
                created: function() {
                    this.initOrder();
                },
                methods: {
                    toCheck: function(t) {
                        var n = {
                            phone: "/pages/myProfile/index",
                            vip: "/pages/buyVip/index",
                            birthday: "/pages/myProfile/index",
                            score: "/pages/myScore/index",
                            register_time: "/pages/center/detail",
                            level_score: "/pages/center/detail",
                            invitee_total: "/pages/myInvitees/index",
                            psy_money_total: "/pages/index/index"
                        }[t];
                        e.navigateTo({
                            url: n
                        });
                    },
                    initOrder: function() {
                        var t = this;
                        if (!this.userGroupId) return !1;
                        e.showLoading(), this.$http("/user-group/check", "POST", {
                            id: this.userGroupId
                        }).then(function(n) {
                            console.log(n), t.isInit = !0, t.options = n.data.options, t.isPassed = n.data.is_passed, 
                            e.hideLoading();
                        });
                    },
                    cancel: function() {
                        this.$emit("close");
                    },
                    toPage: function(t) {
                        e.navigateTo({
                            url: t
                        });
                    }
                }
            };
            t.default = n;
        }).call(this, n("543d").default);
    },
    add4: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("e952"), i = n("0054");
        for (var r in i) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return i[e];
            });
        }(r);
        n("5c2b");
        var a = n("f0c5"), c = Object(a.a)(i.default, o.b, o.c, !1, null, "36aa1e83", null, !1, o.a, void 0);
        t.default = c.exports;
    },
    e890: function(e, t, n) {},
    e952: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return i;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, i = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/UserGroupCheck/UserGroupCheck-create-component", {
    "components/UserGroupCheck/UserGroupCheck-create-component": function(e, t, n) {
        n("543d").createComponent(n("add4"));
    }
}, [ [ "components/UserGroupCheck/UserGroupCheck-create-component" ] ] ]);