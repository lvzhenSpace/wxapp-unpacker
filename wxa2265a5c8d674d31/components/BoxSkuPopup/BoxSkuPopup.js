(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxSkuPopup/BoxSkuPopup" ], {
    "3fac": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return i;
        }), e.d(n, "a", function() {
            return u;
        });
        var u = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, o = function() {
            this.$createElement;
            var t = (this._self._c, this.isShowHeaderTab ? this.skuList.length : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, i = [];
    },
    "62d0": function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        current: 0
                    };
                },
                props: {
                    skuList: {
                        type: Array
                    },
                    detailImageList: {
                        type: Array
                    },
                    setting: {}
                },
                computed: {
                    isShowDetail: function() {
                        return this.detailImageList && this.detailImageList.length;
                    },
                    isShowSkuList: function() {
                        return this.skuList && this.skuList.length;
                    },
                    isShowHeaderTab: function() {
                        return this.isShowDetail && this.isShowSkuList;
                    }
                },
                watch: {},
                onLoad: function(t) {},
                created: function() {},
                methods: {
                    currentChange: function(t) {
                        var n = t.currentTarget.dataset.current;
                        this.current = n;
                    },
                    currentChange2: function(t) {
                        var n = t.detail.current;
                        this.current = n;
                    },
                    close: function() {
                        this.$emit("close");
                    },
                    previewSkuThumb: function(n) {
                        t.previewImage({
                            urls: this.skuList.map(function(t) {
                                return t.thumb;
                            }),
                            current: n
                        });
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    "882d": function(t, n, e) {},
    "8fe4": function(t, n, e) {
        "use strict";
        var u = e("882d");
        e.n(u).a;
    },
    af83: function(t, n, e) {
        "use strict";
        e.r(n);
        var u = e("62d0"), o = e.n(u);
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(i);
        n.default = o.a;
    },
    f1cb: function(t, n, e) {
        "use strict";
        e.r(n);
        var u = e("3fac"), o = e("af83");
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(i);
        e("8fe4");
        var r = e("f0c5"), c = Object(r.a)(o.default, u.b, u.c, !1, null, "2371eb5a", null, !1, u.a, void 0);
        n.default = c.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxSkuPopup/BoxSkuPopup-create-component", {
    "components/BoxSkuPopup/BoxSkuPopup-create-component": function(t, n, e) {
        e("543d").createComponent(e("f1cb"));
    }
}, [ [ "components/BoxSkuPopup/BoxSkuPopup-create-component" ] ] ]);