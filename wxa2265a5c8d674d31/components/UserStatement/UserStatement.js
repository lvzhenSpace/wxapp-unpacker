(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/UserStatement/UserStatement" ], {
    "56d9": function(t, e, n) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var c = {
            props: {
                value: Boolean
            },
            data: function() {
                return {
                    isCheck: !0
                };
            },
            mounted: function() {
                this.$emit("input", this.isCheck);
            },
            watch: {
                isCheck: function(t) {
                    this.$emit("input", this.isCheck);
                }
            },
            computed: {},
            methods: {
                uncheck: function() {
                    this.isCheck = !this.isCheck;
                },
                checkChange: function(t) {}
            }
        };
        e.default = c;
    },
    "667c": function(t, e, n) {},
    "6c30": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return c;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var c = function() {
            this.$createElement, this._self._c;
        }, o = [];
    },
    "72e8": function(t, e, n) {
        "use strict";
        n.r(e);
        var c = n("6c30"), o = n("9eb0");
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        n("8f69");
        var u = n("f0c5"), a = Object(u.a)(o.default, c.b, c.c, !1, null, "6ffc7640", null, !1, c.a, void 0);
        e.default = a.exports;
    },
    "8f69": function(t, e, n) {
        "use strict";
        var c = n("667c");
        n.n(c).a;
    },
    "9eb0": function(t, e, n) {
        "use strict";
        n.r(e);
        var c = n("56d9"), o = n.n(c);
        for (var i in c) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return c[t];
            });
        }(i);
        e.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/UserStatement/UserStatement-create-component", {
    "components/UserStatement/UserStatement-create-component": function(t, e, n) {
        n("543d").createComponent(n("72e8"));
    }
}, [ [ "components/UserStatement/UserStatement-create-component" ] ] ]);