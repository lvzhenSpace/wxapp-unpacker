(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/HiddenSkuRank/HiddenSkuRank" ], {
    "0818": function(t, n, e) {
        "use strict";
        e.r(n);
        var i = e("28d0"), a = e("2e11");
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(t) {
            e.d(n, t, function() {
                return a[t];
            });
        }(o);
        e("27d9");
        var c = e("f0c5"), u = Object(c.a)(a.default, i.b, i.c, !1, null, "3fa442c3", null, !1, i.a, void 0);
        n.default = u.exports;
    },
    "27d9": function(t, n, e) {
        "use strict";
        var i = e("d7d2");
        e.n(i).a;
    },
    "28d0": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return a;
        }), e.d(n, "c", function() {
            return o;
        }), e.d(n, "a", function() {
            return i;
        });
        var i = {
            NoData: function() {
                return e.e("components/NoData/NoData").then(e.bind(null, "cafe"));
            }
        }, a = function() {
            var t = this, n = (t.$createElement, t._self._c, t.__map(t.list, function(n, e) {
                return {
                    $orig: t.__get_orig(n),
                    g0: t.$tool.formatDate(n.created_at, "MM/dd hh:mm")
                };
            })), e = !t.list.length && t.isInit;
            t.$mp.data = Object.assign({}, {
                $root: {
                    l0: n,
                    g1: e
                }
            });
        }, o = [];
    },
    "2e11": function(t, n, e) {
        "use strict";
        e.r(n);
        var i = e("8b1f"), a = e.n(i);
        for (var o in i) [ "default" ].indexOf(o) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(o);
        n.default = a.a;
    },
    "8b1f": function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                components: {},
                data: function() {
                    return {
                        isInit: !1,
                        list: [],
                        total: 0,
                        page: 1,
                        perPage: 20,
                        tag: "all"
                    };
                },
                props: {
                    info: {
                        type: Object
                    }
                },
                computed: {},
                watch: {},
                created: function() {
                    this.initData();
                },
                methods: {
                    initData: function() {
                        t.showLoading({
                            title: "加载中"
                        }), this.fetchList().then(function(n) {
                            t.hideLoading();
                        });
                    },
                    fetchList: function() {
                        var t = this;
                        return !this.isLoading && (this.isLoading = !0, this.$http("/boxes/".concat(this.info.uuid, "/records"), "GET", {
                            page: this.page,
                            per_page: this.perPage
                        }).then(function(n) {
                            t.isInit = !0, t.list = t.list.concat(n.data.list), t.isLoading = !1, t.page++, 
                            t.total = n.data.item_total;
                        }).catch(function(n) {
                            t.isInit = !1;
                        }));
                    },
                    cancel: function() {
                        this.$emit("close");
                    }
                },
                onPageScroll: function(t) {}
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    d7d2: function(t, n, e) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/HiddenSkuRank/HiddenSkuRank-create-component", {
    "components/HiddenSkuRank/HiddenSkuRank-create-component": function(t, n, e) {
        e("543d").createComponent(e("0818"));
    }
}, [ [ "components/HiddenSkuRank/HiddenSkuRank-create-component" ] ] ]);