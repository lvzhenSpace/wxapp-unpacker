(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SharePopup/SharePopup" ], {
    "04d4": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, r = [];
    },
    "0cb5": function(e, t, n) {
        "use strict";
        var o = n("eeec");
        n.n(o).a;
    },
    "8d2d": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("c00b"), r = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(i);
        t.default = r.a;
    },
    b677: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("04d4"), r = n("8d2d");
        for (var i in r) [ "default" ].indexOf(i) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(i);
        n("0cb5");
        var c = n("f0c5"), s = Object(c.a)(r.default, o.b, o.c, !1, null, "50ae3779", null, !1, o.a, void 0);
        t.default = s.exports;
    },
    c00b: function(e, t, n) {
        "use strict";
        (function(e) {
            var o = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = o(n("9523")), i = n("2684");
            function c(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function s(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? c(Object(n), !0).forEach(function(t) {
                        (0, r.default)(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : c(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            var a = {
                components: {
                    PosterTheme1: function() {
                        Promise.all([ n.e("common/vendor"), n.e("components/SharePopup/components/posterTheme1") ]).then(function() {
                            return resolve(n("04ac"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    poster: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    }
                },
                computed: {
                    calcInfo: function() {
                        return s(s({}, this.info), {}, {
                            qrcode: i.BASE_URL + "/miniapp/qrcode?path=" + encodeURIComponent(this.info.path)
                        });
                    },
                    isCanShareToTimeLine: function() {
                        return !1;
                    },
                    isCanShareWithPoster: function() {
                        return !0;
                    }
                },
                data: function() {
                    return {
                        image: "",
                        isShowPoster: !1,
                        isAskShareType: !0
                    };
                },
                methods: {
                    shareToWechat: function() {
                        return this.close(), 0 == this.poster && this.$http("/task/add_share", "POST").then(function(e) {}).catch(function(e) {
                            console.log(e);
                        }), !1;
                    },
                    shareToTimeLine: function() {
                        var t = this;
                        e.share({
                            provider: "weixin",
                            scene: "WXSceneTimeline",
                            type: 0,
                            href: this.info.app_url,
                            title: this.info.title,
                            imageUrl: this.info.thumb,
                            summary: this.info.desc || this.info.title,
                            success: function(e) {
                                t.close();
                            },
                            fail: function(t) {
                                console.log("fail:" + JSON.stringify(t)), e.showToast({
                                    title: "分享未成功~"
                                });
                            }
                        });
                    },
                    generatePoster: function() {
                        this.isAskShareType = !1, this.isShowPoster = !0;
                    },
                    getPosterUrl: function(e) {
                        this.image = e;
                    },
                    close: function() {
                        this.$emit("close");
                    },
                    saveImg: function() {
                        e.saveImageToPhotosAlbum({
                            filePath: this.image,
                            success: function(t) {
                                e.showToast({
                                    title: "已保存到相册",
                                    icon: "none"
                                });
                            }
                        });
                    }
                }
            };
            t.default = a;
        }).call(this, n("543d").default);
    },
    eeec: function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SharePopup/SharePopup-create-component", {
    "components/SharePopup/SharePopup-create-component": function(e, t, n) {
        n("543d").createComponent(n("b677"));
    }
}, [ [ "components/SharePopup/SharePopup-create-component" ] ] ]);