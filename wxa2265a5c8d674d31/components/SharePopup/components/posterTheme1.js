(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SharePopup/components/posterTheme1" ], {
    "04ac": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("1818"), r = n("756e");
        for (var a in r) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(a);
        n("23a3");
        var i = n("f0c5"), c = Object(i.a)(r.default, o.b, o.c, !1, null, "11f5f41f", null, !1, o.a, void 0);
        t.default = c.exports;
    },
    1818: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, r = [];
    },
    "23a3": function(e, t, n) {
        "use strict";
        var o = n("be30");
        n.n(o).a;
    },
    "756e": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("78d9"), r = n.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = r.a;
    },
    "78d9": function(e, t, n) {
        "use strict";
        var o = n("4ea4");
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var r = o(n("2eee")), a = o(n("c973")), i = o(n("b5e5")), c = n("955d"), s = {
            data: function() {
                return {
                    canvasId: "default_PosterCanvasId",
                    poster: null,
                    posterPath: null
                };
            },
            components: {},
            computed: {
                displayPrice: function() {
                    var e = "";
                    return this.info && this.info.money_price && (e = this.info.money_price / 100 + "元"), 
                    this.info && this.info.score_price && (e += (e ? "+" : "") + this.info.score_price + this.scoreAlias), 
                    e || (e = ""), e;
                }
            },
            props: {
                info: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            watch: {
                info: function() {
                    this.generatePoster();
                }
            },
            mounted: function() {
                this.generatePoster();
            },
            methods: {
                generatePoster: function() {
                    var e = this;
                    return (0, a.default)(r.default.mark(function t() {
                        var n;
                        return r.default.wrap(function(t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                return t.prev = 0, t.next = 3, (0, c.getSharePoster)({
                                    _this: e,
                                    type: "testShareType",
                                    posterCanvasId: e.canvasId,
                                    delayTimeScale: 20,
                                    background: {
                                        height: 1e3,
                                        width: 696,
                                        backgroundColor: "#ffffff"
                                    },
                                    drawArray: function(t) {
                                        var n = t.bgObj;
                                        return t.type, t.bgScale, n.width, n.width, n.height, new Promise(function(t, o) {
                                            t([ {
                                                type: "image",
                                                url: e.info.thumb,
                                                dx: 0,
                                                dy: 0,
                                                infoCallBack: function(e) {
                                                    return {
                                                        dWidth: n.width,
                                                        dHeight: n.width + 225,
                                                        mode: "aspectFit"
                                                    };
                                                }
                                            }, {
                                                type: "image",
                                                url: e.info.qrcode,
                                                dx: 180,
                                                dy: n.width - 95,
                                                infoCallBack: function(e) {
                                                    return n.width, e.height, {
                                                        dWidth: 156,
                                                        dHeight: 130,
                                                        roundRectSet: {
                                                            r: 70
                                                        }
                                                    };
                                                }
                                            } ]);
                                        });
                                    },
                                    setCanvasWH: function(t) {
                                        var n = t.bgObj;
                                        t.type, t.bgScale, e.poster = n;
                                    }
                                });

                              case 3:
                                n = t.sent, e.poster.finalPath = n.poster.tempFilePath, e.posterPath = n.poster.tempFilePath, 
                                e.$emit("getPosterUrl", e.posterPath), t.next = 13;
                                break;

                              case 9:
                                t.prev = 9, t.t0 = t.catch(0), i.default.hideLoading(), i.default.showToast(JSON.stringify(t.t0));

                              case 13:
                              case "end":
                                return t.stop();
                            }
                        }, t, null, [ [ 0, 9 ] ]);
                    }))();
                }
            }
        };
        t.default = s;
    },
    be30: function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SharePopup/components/posterTheme1-create-component", {
    "components/SharePopup/components/posterTheme1-create-component": function(e, t, n) {
        n("543d").createComponent(n("04ac"));
    }
}, [ [ "components/SharePopup/components/posterTheme1-create-component" ] ] ]);