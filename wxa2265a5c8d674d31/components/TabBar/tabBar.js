(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/TabBar/tabBar" ], {
    "0c6d": function(e, t, n) {
        "use strict";
        var a = n("b35a");
        n.n(a).a;
    },
    2721: function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("e0af"), c = n.n(a);
        for (var o in a) [ "default" ].indexOf(o) < 0 && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(o);
        t.default = c.a;
    },
    4572: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return a;
        }), n.d(t, "c", function() {
            return c;
        }), n.d(t, "a", function() {});
        var a = function() {
            this.$createElement, this._self._c;
        }, c = [];
    },
    "8e61": function(e, t, n) {
        "use strict";
        n.r(t);
        var a = n("4572"), c = n("2721");
        for (var o in c) [ "default" ].indexOf(o) < 0 && function(e) {
            n.d(t, e, function() {
                return c[e];
            });
        }(o);
        n("0c6d");
        var r = n("f0c5"), u = Object(r.a)(c.default, a.b, a.c, !1, null, "ee16a54e", null, !1, a.a, void 0);
        t.default = u.exports;
    },
    b35a: function(e, t, n) {},
    e0af: function(e, t, n) {
        "use strict";
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var a = {
                components: {},
                data: function() {
                    return {
                        list: [ {
                            selected: 0,
                            pagePath: "/pages/index/index",
                            text: "首页",
                            bgOn: "url(" + n("0cca") + ")",
                            bg: "url(" + n("ceea") + ")"
                        }, {
                            selected: 1,
                            pagePath: "/pages/activity/index",
                            text: "福利",
                            bgOn: "url(" + n("35ce") + ")",
                            bg: "url(" + n("fdb9") + ")"
                        }, {
                            selected: 2,
                            pagePath: "/pages/myBox/index",
                            text: "盒柜",
                            bgOn: "url(" + n("b1a6") + ")",
                            bg: "url(" + n("eb95") + ")"
                        }, {
                            selected: 3,
                            pagePath: "/pages/center/index",
                            text: "我的",
                            bgOn: "url(" + n("e608") + ")",
                            bg: "url(" + n("d233") + ")"
                        } ]
                    };
                },
                props: {
                    isselected: String
                },
                computed: {},
                watch: {},
                onLoad: function(e) {},
                created: function() {},
                methods: {
                    link: function(t, n) {
                        this.isselected != n && e.reLaunch({
                            url: t
                        });
                    }
                },
                onPageScroll: function(e) {}
            };
            t.default = a;
        }).call(this, n("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/TabBar/tabBar-create-component", {
    "components/TabBar/tabBar-create-component": function(e, t, n) {
        n("543d").createComponent(n("8e61"));
    }
}, [ [ "components/TabBar/tabBar-create-component" ] ] ]);