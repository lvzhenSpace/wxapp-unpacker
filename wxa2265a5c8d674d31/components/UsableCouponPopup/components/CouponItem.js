(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/UsableCouponPopup/components/CouponItem" ], {
    "1b2a": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return o;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    "73a6": function(t, n, e) {
        "use strict";
        var o = e("4ea4");
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var u = o(e("8fc9")), a = {
            name: "CouponItem",
            props: {
                backgroundColor: {
                    type: String,
                    default: "#ffffff"
                },
                coupon: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                isSelected: {
                    type: Boolean,
                    default: function() {
                        return !1;
                    }
                },
                active: {
                    type: Number,
                    default: 1
                },
                activeText: {
                    type: String,
                    default: "立即使用"
                },
                unActiveText: {
                    type: String,
                    default: "已使用"
                }
            },
            computed: {
                baseCoupon: function() {
                    return this.coupon.base_coupon || {};
                },
                style: function() {
                    return "background-color:" + this.backgroundColor;
                },
                validDateStr: function() {
                    var t = this.coupon.usable_start_at, n = this.coupon.usable_end_at;
                    if (!n) return "";
                    var e = (0, u.default)(t), o = (0, u.default)(n), a = new Date();
                    if (e > a || o < a) return e.format("YYYY.MM.DD") + " - " + o.format("MM.DD");
                    var c = o.diff(a, "days");
                    return c < 1 ? o.diff(a, "h") + 1 + "小时后过期" : c + 1 + "天后过期";
                }
            },
            methods: {
                click: function() {
                    this.$emit("click", this.coupon);
                }
            }
        };
        n.default = a;
    },
    "8adf": function(t, n, e) {
        "use strict";
        var o = e("e243");
        e.n(o).a;
    },
    d810: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("73a6"), u = e.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(a);
        n.default = u.a;
    },
    df52: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("1b2a"), u = e("d810");
        for (var a in u) [ "default" ].indexOf(a) < 0 && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(a);
        e("8adf");
        var c = e("f0c5"), r = Object(c.a)(u.default, o.b, o.c, !1, null, "453c9b9c", null, !1, o.a, void 0);
        n.default = r.exports;
    },
    e243: function(t, n, e) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/UsableCouponPopup/components/CouponItem-create-component", {
    "components/UsableCouponPopup/components/CouponItem-create-component": function(t, n, e) {
        e("543d").createComponent(e("df52"));
    }
}, [ [ "components/UsableCouponPopup/components/CouponItem-create-component" ] ] ]);