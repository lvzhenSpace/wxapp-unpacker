(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/ProductItem/ProductItem" ], {
    "2abb": function(t, n, e) {
        "use strict";
        var o = e("a67d");
        e.n(o).a;
    },
    "9c3e": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("b4bd"), u = e("e633");
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(t) {
            e.d(n, t, function() {
                return u[t];
            });
        }(i);
        e("2abb");
        var c = e("f0c5"), r = Object(c.a)(u.default, o.b, o.c, !1, null, "cba160fe", null, !1, o.a, void 0);
        n.default = r.exports;
    },
    a67d: function(t, n, e) {},
    b4bd: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return u;
        }), e.d(n, "c", function() {
            return i;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, u = function() {
            this.$createElement;
            var t = (this._self._c, this.isShowTags ? this.info.tags && this.info.tags.length : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t
                }
            });
        }, i = [];
    },
    d795: function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    grid: {
                        type: String,
                        default: function() {
                            return "grid2";
                        }
                    },
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    },
                    isShowTags: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    e633: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("d795"), u = e.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(i);
        n.default = u.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/ProductItem/ProductItem-create-component", {
    "components/ProductItem/ProductItem-create-component": function(t, n, e) {
        e("543d").createComponent(e("9c3e"));
    }
}, [ [ "components/ProductItem/ProductItem-create-component" ] ] ]);