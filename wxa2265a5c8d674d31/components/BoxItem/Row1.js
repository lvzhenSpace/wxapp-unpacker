(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Row1" ], {
    1646: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    3473: function(t, n, e) {},
    "61e8": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("1646"), i = e("caf8");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(u);
        e("e42b");
        var c = e("f0c5"), a = Object(c.a)(i.default, o.b, o.c, !1, null, "690329cd", null, !1, o.a, void 0);
        n.default = a.exports;
    },
    "868a": function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    caf8: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("868a"), i = e.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(u);
        n.default = i.a;
    },
    e42b: function(t, n, e) {
        "use strict";
        var o = e("3473");
        e.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Row1-create-component", {
    "components/BoxItem/Row1-create-component": function(t, n, e) {
        e("543d").createComponent(e("61e8"));
    }
}, [ [ "components/BoxItem/Row1-create-component" ] ] ]);