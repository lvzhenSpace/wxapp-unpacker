(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Grid1" ], {
    "06d8": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("b81c"), i = e.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(c);
        n.default = i.a;
    },
    2483: function(t, n, e) {
        "use strict";
        var o = e("eee3");
        e.n(o).a;
    },
    "666d": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("c67c"), i = e("06d8");
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(c);
        e("2483");
        var u = e("f0c5"), r = Object(u.a)(i.default, o.b, o.c, !1, null, "632e4d3e", null, !1, o.a, void 0);
        n.default = r.exports;
    },
    b81c: function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    c67c: function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return c;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, c = [];
    },
    eee3: function(t, n, e) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Grid1-create-component", {
    "components/BoxItem/Grid1-create-component": function(t, n, e) {
        e("543d").createComponent(e("666d"));
    }
}, [ [ "components/BoxItem/Grid1-create-component" ] ] ]);