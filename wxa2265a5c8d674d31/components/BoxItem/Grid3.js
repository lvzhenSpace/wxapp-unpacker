(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Grid3" ], {
    "0a9a": function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    },
    "122d": function(t, n, e) {},
    "78bf": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("0a9a"), i = e.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(u);
        n.default = i.a;
    },
    "859a": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return u;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    bf34: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("859a"), i = e("78bf");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(u);
        e("e141");
        var a = e("f0c5"), r = Object(a.a)(i.default, o.b, o.c, !1, null, "2f53a721", null, !1, o.a, void 0);
        n.default = r.exports;
    },
    e141: function(t, n, e) {
        "use strict";
        var o = e("122d");
        e.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Grid3-create-component", {
    "components/BoxItem/Grid3-create-component": function(t, n, e) {
        e("543d").createComponent(e("bf34"));
    }
}, [ [ "components/BoxItem/Grid3-create-component" ] ] ]);