(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/BoxItem/Grid2" ], {
    "06d3": function(t, n, e) {
        "use strict";
        var o = e("b3a1");
        e.n(o).a;
    },
    "23c3b": function(t, n, e) {
        "use strict";
        e.d(n, "b", function() {
            return i;
        }), e.d(n, "c", function() {
            return c;
        }), e.d(n, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return e.e("components/PriceDisplay/PriceDisplay").then(e.bind(null, "6b05"));
            }
        }, i = function() {
            this.$createElement, this._self._c;
        }, c = [];
    },
    "5fa9": function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("23c3b"), i = e("e457");
        for (var c in i) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return i[t];
            });
        }(c);
        e("06d3");
        var u = e("f0c5"), a = Object(u.a)(i.default, o.b, o.c, !1, null, "493c82cb", null, !1, o.a, void 0);
        n.default = a.exports;
    },
    b3a1: function(t, n, e) {},
    e457: function(t, n, e) {
        "use strict";
        e.r(n);
        var o = e("fcbb"), i = e.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(c);
        n.default = i.a;
    },
    fcbb: function(t, n, e) {
        "use strict";
        (function(t) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: {
                    info: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    tag: {
                        type: String
                    },
                    theme: {
                        type: String,
                        default: function() {
                            return "default-theme";
                        }
                    }
                },
                data: function() {
                    return {};
                },
                computed: {
                    tagString: function() {
                        return this.info && this.info.tags && this.info.tags[0] || " ";
                    }
                },
                methods: {
                    toDetail: function() {
                        t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + this.info.uuid
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, e("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/BoxItem/Grid2-create-component", {
    "components/BoxItem/Grid2-create-component": function(t, n, e) {
        e("543d").createComponent(e("5fa9"));
    }
}, [ [ "components/BoxItem/Grid2-create-component" ] ] ]);