(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/CouponList/CouponList" ], {
    "040b": function(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("986c"), i = o("5ca1");
        for (var u in i) [ "default" ].indexOf(u) < 0 && function(t) {
            o.d(n, t, function() {
                return i[t];
            });
        }(u);
        o("6a09");
        var c = o("f0c5"), s = Object(c.a)(i.default, e.b, e.c, !1, null, null, null, !1, e.a, void 0);
        n.default = s.exports;
    },
    "0768": function(t, n, o) {},
    "5ca1": function(t, n, o) {
        "use strict";
        o.r(n);
        var e = o("e233"), i = o.n(e);
        for (var u in e) [ "default" ].indexOf(u) < 0 && function(t) {
            o.d(n, t, function() {
                return e[t];
            });
        }(u);
        n.default = i.a;
    },
    "6a09": function(t, n, o) {
        "use strict";
        var e = o("0768");
        o.n(e).a;
    },
    "986c": function(t, n, o) {
        "use strict";
        o.d(n, "b", function() {
            return e;
        }), o.d(n, "c", function() {
            return i;
        }), o.d(n, "a", function() {});
        var e = function() {
            this.$createElement, this._self._c;
        }, i = [];
    },
    e233: function(t, n, o) {
        "use strict";
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var e = {
            props: {
                ids: {
                    type: Array
                },
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                },
                refreshCounter: Number
            },
            components: {
                CouponItem: function() {
                    o.e("components/CouponList/components/CouponItem").then(function() {
                        return resolve(o("60a7"));
                    }.bind(null, o)).catch(o.oe);
                }
            },
            data: function() {
                return {
                    list: [],
                    isInit: !1
                };
            },
            mounted: function() {
                this.initData();
            },
            computed: {
                isScroll: function() {
                    return "scroll" == this.module.display;
                },
                grid: function() {
                    return this.module.grid || "grid1";
                }
            },
            watch: {
                ids: function() {
                    this.initData();
                },
                refreshCounter: function() {
                    this.initData();
                }
            },
            methods: {
                initData: function() {
                    var t = this;
                    this.ids && this.ids.length > 0 && this.$http("/coupons", "GET", {
                        per_page: 100,
                        ids: this.ids
                    }).then(function(n) {
                        t.list = n.data.list, t.isInit = !0;
                    });
                },
                clickItem: function() {
                    this.$playAudio("click");
                }
            }
        };
        n.default = e;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/CouponList/CouponList-create-component", {
    "components/CouponList/CouponList-create-component": function(t, n, o) {
        o("543d").createComponent(o("040b"));
    }
}, [ [ "components/CouponList/CouponList-create-component" ] ] ]);