(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SelectAddress/SelectAddress" ], {
    "09a0": function(e, t, n) {
        "use strict";
        var s = n("9bcc");
        n.n(s).a;
    },
    1376: function(e, t, n) {
        "use strict";
        n.r(t);
        var s = n("2a9e"), o = n.n(s);
        for (var d in s) [ "default" ].indexOf(d) < 0 && function(e) {
            n.d(t, e, function() {
                return s[e];
            });
        }(d);
        t.default = o.a;
    },
    "1a85": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return s;
        }), n.d(t, "c", function() {
            return o;
        }), n.d(t, "a", function() {});
        var s = function() {
            this.$createElement;
            var e = (this._self._c, this.address && this.address.id && this.isShowPhone ? this._f("hidePhoneDetail")(this.address && this.address.phone) : null);
            this.$mp.data = Object.assign({}, {
                $root: {
                    f0: e
                }
            });
        }, o = [];
    },
    "2a9e": function(e, t, n) {
        "use strict";
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = {
                props: {
                    value: {
                        type: Object
                    },
                    isShowPhone: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                data: function() {
                    return {
                        address: {}
                    };
                },
                filters: {
                    hidePhoneDetail: function(e) {
                        return e ? e.substring(0, 3) + "****" + e.substring(7, 11) : "";
                    }
                },
                watch: {
                    address: function() {
                        this.$emit("input", this.address);
                    },
                    value: function(e) {
                        this.address = e;
                    }
                },
                mounted: function(t) {
                    var n = this;
                    this.address = this.value || {}, e.$on("selectAddress", function(e) {
                        n.address = e;
                    });
                },
                beforeDestroy: function() {
                    e.$off("selectAddress", function(e) {
                        console.log("移除 selectAddress 自定义事件");
                    });
                },
                methods: {
                    handleSelectAddress: function() {
                        e.navigateTo({
                            url: "/pages/myAddress/index?select=true"
                        });
                    }
                }
            };
            t.default = n;
        }).call(this, n("543d").default);
    },
    "8a38": function(e, t, n) {
        "use strict";
        n.r(t);
        var s = n("1a85"), o = n("1376");
        for (var d in o) [ "default" ].indexOf(d) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(d);
        n("09a0");
        var c = n("f0c5"), a = Object(c.a)(o.default, s.b, s.c, !1, null, "6f060a36", null, !1, s.a, void 0);
        t.default = a.exports;
    },
    "9bcc": function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SelectAddress/SelectAddress-create-component", {
    "components/SelectAddress/SelectAddress-create-component": function(e, t, n) {
        n("543d").createComponent(n("8a38"));
    }
}, [ [ "components/SelectAddress/SelectAddress-create-component" ] ] ]);