(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/slider-verify/slider-verify" ], {
    7465: function(t, e, i) {
        "use strict";
        i.d(e, "b", function() {
            return n;
        }), i.d(e, "c", function() {
            return o;
        }), i.d(e, "a", function() {});
        var n = function() {
            this.$createElement, this._self._c;
        }, o = [];
    },
    bc5d: function(t, e, i) {
        "use strict";
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = {
                name: "slider-verify",
                props: {
                    isShow: !0,
                    session_id: {
                        type: Number,
                        default: 0
                    },
                    activity_uuid: {
                        type: String
                    },
                    inviter_user_uuid: {
                        type: String
                    }
                },
                data: function() {
                    return {
                        x: 0,
                        oldx: 0,
                        img: "1",
                        left: 0,
                        top: 0
                    };
                },
                watch: {
                    isShow: function(t, e) {
                        t && this.refreshVerify();
                    }
                },
                mounted: function() {
                    this.refreshVerify();
                },
                methods: {
                    refreshVerify: function() {
                        var e = Math.random().toFixed(2);
                        this.left = t.upx2px(560) * e > t.upx2px(280) ? t.upx2px(280) : t.upx2px(560) * e + t.upx2px(150), 
                        this.top = t.upx2px(190) * e, e <= .2 && (this.img = 1), e > .2 && e <= .4 && (this.img = 2), 
                        e > .4 && e <= .6 && (this.img = 3), e > .6 && e <= .8 && (this.img = 4), e > .8 && e <= 1 && (this.img = 5), 
                        this.resetMove();
                    },
                    startMove: function(t) {
                        this.oldx = t.detail.x;
                    },
                    endTouchMove: function() {
                        var e = this;
                        Math.abs(e.oldx - e.left) <= 5 ? (t.showToast({
                            title: "验证成功",
                            duration: 1500,
                            success: function() {
                                e.$emit("touchSliderResult", !0);
                            }
                        }), this.acceptInvite()) : e.refreshVerify();
                    },
                    acceptInvite: function() {
                        var e = this;
                        t.showLoading({
                            title: "助力中"
                        }), this.$http("/lotteries/".concat(this.activity_uuid, "/accept-invite"), "POST", {
                            inviter: this.inviter_user_uuid
                        }).then(function(i) {
                            e.isAccepted = 1, t.hideLoading(), t.showToast({
                                title: "助力成功~",
                                icon: "none"
                            });
                        });
                    },
                    resetMove: function() {
                        var t = this;
                        this.x = 1, this.oldx = 1, setTimeout(function() {
                            t.x = 0, t.oldx = 0;
                        }, 300);
                    },
                    closeSlider: function() {
                        this.$emit("touchSliderResult", !1);
                    }
                }
            };
            e.default = i;
        }).call(this, i("543d").default);
    },
    cd5d: function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("bc5d"), o = i.n(n);
        for (var r in n) [ "default" ].indexOf(r) < 0 && function(t) {
            i.d(e, t, function() {
                return n[t];
            });
        }(r);
        e.default = o.a;
    },
    ded3: function(t, e, i) {
        "use strict";
        i.r(e);
        var n = i("7465"), o = i("cd5d");
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(t) {
            i.d(e, t, function() {
                return o[t];
            });
        }(r);
        i("e3e9");
        var s = i("f0c5"), c = Object(s.a)(o.default, n.b, n.c, !1, null, null, null, !1, n.a, void 0);
        e.default = c.exports;
    },
    e3e9: function(t, e, i) {
        "use strict";
        var n = i("fdfb");
        i.n(n).a;
    },
    fdfb: function(t, e, i) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/slider-verify/slider-verify-create-component", {
    "components/slider-verify/slider-verify-create-component": function(t, e, i) {
        i("543d").createComponent(i("ded3"));
    }
}, [ [ "components/slider-verify/slider-verify-create-component" ] ] ]);