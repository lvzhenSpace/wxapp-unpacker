(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/SkuItem/SkuItem" ], {
    "0fd6": function(t, e, n) {},
    "142c": function(t, e, n) {
        "use strict";
        var o = n("0fd6");
        n.n(o).a;
    },
    "155d": function(t, e, n) {
        "use strict";
        (function(t) {
            var o = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var u = {
                mixins: [ o(n("452d")).default ],
                props: {
                    info: {
                        type: Object
                    },
                    disableClick: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    }
                },
                filters: {},
                methods: {
                    toProductDetail: function(e) {
                        if (this.disableClick) return !1;
                        var n = this.info;
                        "product" === n.product_type ? t.navigateTo({
                            url: "/pages/productDetail/index?uuid=" + n.product_uuid
                        }) : "box" === n.product_type && t.navigateTo({
                            url: "/pages/boxDetail/index?uuid=" + n.product_uuid
                        });
                    }
                }
            };
            e.default = u;
        }).call(this, n("543d").default);
    },
    "3c3e": function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("8f3d"), u = n("e253");
        for (var i in u) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return u[t];
            });
        }(i);
        n("142c");
        var c = n("f0c5"), r = Object(c.a)(u.default, o.b, o.c, !1, null, "d9427bde", null, !1, o.a, void 0);
        e.default = r.exports;
    },
    "8f3d": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return u;
        }), n.d(e, "c", function() {
            return i;
        }), n.d(e, "a", function() {
            return o;
        });
        var o = {
            PriceDisplay: function() {
                return n.e("components/PriceDisplay/PriceDisplay").then(n.bind(null, "6b05"));
            }
        }, u = function() {
            this.$createElement;
            var t = (this._self._c, this.info.attrs && this.info.attrs.length), e = t ? this._f("productAttrsToString")(this.info.attrs) : null;
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: t,
                    f0: e
                }
            });
        }, i = [];
    },
    e253: function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("155d"), u = n.n(o);
        for (var i in o) [ "default" ].indexOf(i) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(i);
        e.default = u.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/SkuItem/SkuItem-create-component", {
    "components/SkuItem/SkuItem-create-component": function(t, e, n) {
        n("543d").createComponent(n("3c3e"));
    }
}, [ [ "components/SkuItem/SkuItem-create-component" ] ] ]);