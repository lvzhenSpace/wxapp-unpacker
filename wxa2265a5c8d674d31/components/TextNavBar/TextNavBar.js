(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/TextNavBar/TextNavBar" ], {
    "4f45": function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return a;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, a = [];
    },
    "507e": function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("a180"), a = n.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(c);
        e.default = a.a;
    },
    "7a21": function(t, e, n) {
        "use strict";
        var o = n("b76e");
        n.n(o).a;
    },
    9141: function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("4f45"), a = n("507e");
        for (var c in a) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return a[t];
            });
        }(c);
        n("7a21");
        var r = n("f0c5"), i = Object(r.a)(a.default, o.b, o.c, !1, null, null, null, !1, o.a, void 0);
        e.default = i.exports;
    },
    a180: function(t, e, n) {
        "use strict";
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: {
                    title: String,
                    darkMode: Boolean,
                    bgColor: String,
                    titleColor: String
                },
                data: function() {
                    return {
                        customBar: 64,
                        contentStyle: 64,
                        titleStyle: 64
                    };
                },
                computed: {
                    deviceInfo: function() {
                        return this.$store.getters.deviceInfo;
                    }
                },
                created: function() {
                    this.customBar = this.deviceInfo.customBar, this.contentStyle = "height:".concat(this.deviceInfo.customBar, "px;padding-top:").concat(this.deviceInfo.statusBar, "px;background:").concat(this.bgColor, ";"), 
                    this.titleStyle = "color:".concat(this.titleColor);
                },
                methods: {
                    goBack: function() {
                        console.log("goback", getCurrentPages().length), 1 == getCurrentPages().length ? t.switchTab({
                            url: "/pages/index/index"
                        }) : t.navigateBack();
                    }
                }
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    b76e: function(t, e, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/TextNavBar/TextNavBar-create-component", {
    "components/TextNavBar/TextNavBar-create-component": function(t, e, n) {
        n("543d").createComponent(n("9141"));
    }
}, [ [ "components/TextNavBar/TextNavBar-create-component" ] ] ]);