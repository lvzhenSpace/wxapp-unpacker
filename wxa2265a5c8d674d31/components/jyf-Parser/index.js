(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/jyf-Parser/index" ], {
    "4c68": function(t, e, n) {
        "use strict";
        (function(t, i) {
            var o = n("4ea4");
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var r = o(n("7037")), a = n("7974").parseHtmlSync, s = getApp().parserCache = {}, l = n("a72c"), c = n("ec6a");
            function d(t) {
                if (0 != t.indexOf("http")) return t;
                for (var e = "", n = 0; n < t.length && (e += Math.random() >= .5 ? t[n].toUpperCase() : t[n].toLowerCase(), 
                "/" != t[n] || "/" == t[n - 1] || "/" == t[n + 1]); n++) ;
                return e += t.substring(n + 1);
            }
            var u = {
                name: "parser",
                data: function() {
                    return {
                        showAnimation: "",
                        controls: {},
                        nodes: []
                    };
                },
                components: {
                    trees: function() {
                        n.e("components/jyf-Parser/trees").then(function() {
                            return resolve(n("6943"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                props: {
                    html: {
                        type: null,
                        default: null
                    },
                    autocopy: {
                        type: Boolean,
                        default: !0
                    },
                    autopause: {
                        type: Boolean,
                        default: !0
                    },
                    autopreview: {
                        type: Boolean,
                        default: !0
                    },
                    autosetTitle: {
                        type: Boolean,
                        default: !0
                    },
                    domain: {
                        type: String,
                        default: null
                    },
                    imgMode: {
                        type: String,
                        default: "default"
                    },
                    lazyLoad: {
                        type: Boolean,
                        default: !1
                    },
                    selectable: {
                        type: Boolean,
                        default: !1
                    },
                    tagStyle: {
                        type: Object,
                        default: function() {
                            return {};
                        }
                    },
                    showWithAnimation: {
                        type: Boolean,
                        default: !1
                    },
                    useAnchor: {
                        type: Boolean,
                        default: !1
                    },
                    useCache: {
                        type: Boolean,
                        default: !1
                    }
                },
                watch: {
                    html: function(t) {
                        this.setContent(t, void 0, !0);
                    }
                },
                mounted: function() {
                    this.imgList = [], this.imgList.each = function(t) {
                        for (var e = 0; e < this.length; e++) {
                            var n = t(this[e], e, this);
                            n && (this.includes(n) ? this[e] = d(n) : this[e] = n);
                        }
                    }, this.setContent(this.html, void 0, !0);
                },
                methods: {
                    setContent: function(e, n, o) {
                        var u = this;
                        if ("object" == (0, r.default)(n)) for (var h in n) this[h = h.replace(/-(\w)/g, function(t, e) {
                            return e.toUpperCase();
                        })] = n[h];
                        if (this.showWithAnimation && (this.showAnimation = "transition:400ms ease 0ms;transition-property:transform,opacity;transform-origin:50% 50% 0;-webkit-transition:400ms ease 0ms;-webkit-transform:;-webkit-transition-property:transform,opacity;-webkit-transform-origin:50% 50% 0;opacity: 1"), 
                        e) if ("string" == typeof e) {
                            if (this.useCache) {
                                var f = function(t) {
                                    for (var e = t.length, n = 5381; e--; ) n += (n << 5) + t.charCodeAt(e);
                                    return n;
                                }(e);
                                s[f] ? this.nodes = s[f] : (this.nodes = a(e, this), s[f] = this.nodes);
                            } else this.nodes = a(e, this);
                            this.$emit("parse", this.nodes);
                        } else if ("[object Array]" == Object.prototype.toString.call(e)) {
                            if (this.nodes = o ? [] : e, e.length && "Parser" != e[0].PoweredBy) {
                                var m = {
                                    _imgNum: 0,
                                    _videoNum: 0,
                                    _audioNum: 0,
                                    _domain: this.domain,
                                    _protocol: this.domain ? this.domain.includes("://") ? this.domain.split("://")[0] : "http" : void 0,
                                    _STACK: [],
                                    CssHandler: new l(this.tagStyle)
                                };
                                m.CssHandler.getStyle(""), function t(e) {
                                    for (var n, i = 0; n = e[i++]; ) if ("text" != n.type) {
                                        for (var o in n.attrs = n.attrs || {}, n.attrs) c.trustAttrs[o] ? "string" != typeof n.attrs[o] && (n.attrs[o] = n.attrs[o].toString()) : n.attrs[o] = void 0;
                                        c.LabelAttrsHandler(n, m), c.blockTags[n.name] ? n.name = "div" : c.trustTags[n.name] || (n.name = "span"), 
                                        n.children && n.children.length ? (m._STACK.push(n), t(n.children), m._STACK.pop()) : n.children = void 0;
                                    }
                                }(e), this.nodes = e;
                            }
                        } else {
                            if ("object" != (0, r.default)(e) || !e.nodes) return this.$emit("error", {
                                source: "parse",
                                errMsg: "传入的nodes数组格式不正确！应该传入的类型是array，实际传入的类型是：" + (0, r.default)(e.nodes)
                            });
                            this.nodes = e.nodes, console.warn("Parser 类型错误：object 类型已废弃，请直接将 html 设置为 object.nodes （array 类型）");
                        } else {
                            if (o) return;
                            this.nodes = [];
                        }
                        this.$nextTick(function() {
                            u.imgList.length = 0, u.videoContexts = [], function e(n) {
                                for (var o = function() {
                                    var o = n[r];
                                    if ("trees" == o.$options.name) for (a = !1, s = o.nodes.length; l = o.nodes[--s]; ) l.c || ("img" == l.name ? (l.attrs.src && l.attrs.i && (-1 == u.imgList.indexOf(l.attrs.src) ? u.imgList[l.attrs.i] = l.attrs.src : u.imgList[l.attrs.i] = d(l.attrs.src)), 
                                    a || (a = !0, u.lazyLoad && t.createIntersectionObserver ? (o._observer && o._observer.disconnect(), 
                                    o._observer = t.createIntersectionObserver(o), o._observer.relativeToViewport({
                                        top: 1e3,
                                        bottom: 1e3
                                    }).observe("._img", function(t) {
                                        o.imgLoad = !0, o._observer.disconnect(), o._observer = null;
                                    })) : o.imgLoad = !0)) : "video" == l.name ? ((c = t.createVideoContext(l.attrs.id, o)).id = l.attrs.id, 
                                    u.videoContexts.push(c)) : "audio" == l.name && l.attrs.autoplay ? i.createAudioContext(l.attrs.id, o).play() : "title" == l.name && u.autosetTitle && "text" == l.children[0].type && l.children[0].text && t.setNavigationBarTitle({
                                        title: l.children[0].text
                                    }));
                                    o.$children.length && e(o.$children);
                                }, r = n.length; r--; ) {
                                    var a, s, l, c;
                                    o();
                                }
                            }(u.$children), t.createSelectorQuery().in(u).select("._contain").boundingClientRect(function(t) {
                                u.$emit("ready", t);
                            }).exec();
                        });
                    },
                    getText: function() {
                        var t = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0], e = "", n = function n(i) {
                            if ("text" == i.type) return e += i.text;
                            if (t && (("p" == i.name || "div" == i.name || "tr" == i.name || "li" == i.name || /h[1-6]/.test(i.name)) && e && "\n" != e[e.length - 1] || "br" == i.name) && (e += "\n"), 
                            i.children) for (var o = 0; o < i.children.length; o++) n(i.children[o]);
                            t && ("p" == i.name || "div" == i.name || "tr" == i.name || "li" == i.name || /h[1-6]/.test(i.name)) && e && "\n" != e[e.length - 1] ? e += "\n" : t && "td" == i.name && (e += "\t");
                        }, i = this.nodes && this.nodes.length ? this.nodes : this.html[0] && (this.html[0].name || this.html[0].type) ? this.html : [];
                        if (!i.length) return "";
                        for (var o = 0; o < this.data.html.length; o++) n(this.data.html[o]);
                        return e;
                    },
                    navigateTo: function(e) {
                        var n = this, i = function(i, o) {
                            var r = t.createSelectorQuery().in(o || n);
                            r.select(i).boundingClientRect(), r.selectViewport().scrollOffset(), r.exec(function(n) {
                                if (!n || !n[0]) return e.fail ? e.fail({
                                    errMsg: "Label Not Found"
                                }) : null;
                                t.pageScrollTo({
                                    scrollTop: n[1].scrollTop + n[0].top,
                                    success: e.success,
                                    fail: e.fail
                                });
                            });
                        };
                        e.id ? i("._contain >>> #" + e.id + ", ._contain >>> ." + e.id) : i("._contain");
                    },
                    getVideoContext: function(t) {
                        if (!t) return this.videoContexts;
                        for (var e = this.videoContexts.length; e--; ) if (this.videoContexts[e].id == t) return this.videoContexts[e];
                        return null;
                    }
                }
            };
            e.default = u;
        }).call(this, n("543d").default, n("bc2e").default);
    },
    "83b0": function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("c1e1"), o = n("ed18");
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(r);
        n("e833");
        var a = n("f0c5"), s = Object(a.a)(o.default, i.b, i.c, !1, null, null, null, !1, i.a, void 0);
        e.default = s.exports;
    },
    c1e1: function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return i;
        }), n.d(e, "c", function() {
            return o;
        }), n.d(e, "a", function() {});
        var i = function() {
            var t = this, e = (t.$createElement, t._self._c, !(t.html && t.html.length && (t.html[0].name || t.html[0].type) || t.nodes.length)), n = t.nodes.length, i = n ? null : t.html && t.html.length && (t.html[0].name || t.html[0].type);
            t.$mp.data = Object.assign({}, {
                $root: {
                    g0: e,
                    g1: n,
                    g2: i
                }
            });
        }, o = [];
    },
    d3c9: function(t, e, n) {},
    e833: function(t, e, n) {
        "use strict";
        var i = n("d3c9");
        n.n(i).a;
    },
    ed18: function(t, e, n) {
        "use strict";
        n.r(e);
        var i = n("4c68"), o = n.n(i);
        for (var r in i) [ "default" ].indexOf(r) < 0 && function(t) {
            n.d(e, t, function() {
                return i[t];
            });
        }(r);
        e.default = o.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/jyf-Parser/index-create-component", {
    "components/jyf-Parser/index-create-component": function(t, e, n) {
        n("543d").createComponent(n("83b0"));
    }
}, [ [ "components/jyf-Parser/index-create-component" ] ] ]);