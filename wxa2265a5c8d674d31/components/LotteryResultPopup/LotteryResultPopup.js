(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/LotteryResultPopup/LotteryResultPopup" ], {
    "3c0d": function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("f327"), u = n("4405");
        for (var c in u) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return u[t];
            });
        }(c);
        n("c73e");
        var a = n("f0c5"), r = Object(a.a)(u.default, o.b, o.c, !1, null, "50143e10", null, !1, o.a, void 0);
        e.default = r.exports;
    },
    4405: function(t, e, n) {
        "use strict";
        n.r(e);
        var o = n("5701"), u = n.n(o);
        for (var c in o) [ "default" ].indexOf(c) < 0 && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(c);
        e.default = u.a;
    },
    "550e": function(t, e, n) {},
    5701: function(t, e, n) {
        "use strict";
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                components: {},
                data: function() {
                    return {};
                },
                props: {
                    skus: {
                        type: Array
                    }
                },
                computed: {
                    sku: function() {
                        return this.skus[0] || {};
                    }
                },
                watch: {},
                onLoad: function(t) {},
                created: function() {},
                methods: {
                    toBoxDetail: function() {
                        t.navigateTo({
                            url: "/pages/myBox/detail?uuid=" + this.sku.uuid
                        });
                    },
                    toMyBox: function() {
                        t.navigateTo({
                            url: "/pages/myBox/index"
                        });
                    },
                    cancel: function() {
                        this.$emit("cancel");
                    }
                },
                onPageScroll: function(t) {}
            };
            e.default = n;
        }).call(this, n("543d").default);
    },
    c73e: function(t, e, n) {
        "use strict";
        var o = n("550e");
        n.n(o).a;
    },
    f327: function(t, e, n) {
        "use strict";
        n.d(e, "b", function() {
            return o;
        }), n.d(e, "c", function() {
            return u;
        }), n.d(e, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, u = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/LotteryResultPopup/LotteryResultPopup-create-component", {
    "components/LotteryResultPopup/LotteryResultPopup-create-component": function(t, e, n) {
        n("543d").createComponent(n("3c0d"));
    }
}, [ [ "components/LotteryResultPopup/LotteryResultPopup-create-component" ] ] ]);