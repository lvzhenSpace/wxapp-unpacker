(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/AreaSelect/AreaSelect" ], {
    "1aef": function(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("73e1"), a = n("62fc");
        for (var c in a) [ "default" ].indexOf(c) < 0 && function(e) {
            n.d(t, e, function() {
                return a[e];
            });
        }(c);
        n("6b6a");
        var o = n("f0c5"), l = Object(o.a)(a.default, r.b, r.c, !1, null, "5e66b08e", null, !1, r.a, void 0);
        t.default = l.exports;
    },
    "416f": function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var r = {
            data: function() {
                return {
                    cityPickerValueDefault: [ 0, 0, 1 ],
                    labelArr: []
                };
            },
            props: {
                value: {
                    type: Array
                }
            },
            components: {
                simpleAddress: function() {
                    Promise.all([ n.e("common/vendor"), n.e("components/AreaSelect/simple-address") ]).then(function() {
                        return resolve(n("39db"));
                    }.bind(null, n)).catch(n.oe);
                }
            },
            computed: {
                pickerText: function() {
                    return this.labelArr && this.labelArr.join("-");
                }
            },
            mounted: function() {
                this.value && (this.labelArr = this.value);
            },
            watch: {
                labelArr: function() {
                    this.cityPickerValueDefault = this.$refs.simpleAddress.queryIndex(this.labelArr, "label").index;
                }
            },
            methods: {
                openAddres: function() {
                    this.$refs.simpleAddress.open();
                },
                onConfirm: function(e) {
                    this.labelArr = e.labelArr, this.$emit("change", e);
                }
            }
        };
        t.default = r;
    },
    "62fc": function(e, t, n) {
        "use strict";
        n.r(t);
        var r = n("416f"), a = n.n(r);
        for (var c in r) [ "default" ].indexOf(c) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(c);
        t.default = a.a;
    },
    "6b6a": function(e, t, n) {
        "use strict";
        var r = n("a3a4");
        n.n(r).a;
    },
    "73e1": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return r;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {});
        var r = function() {
            this.$createElement, this._self._c;
        }, a = [];
    },
    a3a4: function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/AreaSelect/AreaSelect-create-component", {
    "components/AreaSelect/AreaSelect-create-component": function(e, t, n) {
        n("543d").createComponent(n("1aef"));
    }
}, [ [ "components/AreaSelect/AreaSelect-create-component" ] ] ]);