(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/PageRender" ], {
    3592: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("e9ea"), r = n.n(o);
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(a);
        t.default = r.a;
    },
    "7db2": function(e, t, n) {
        "use strict";
        var o = n("c53e");
        n.n(o).a;
    },
    "8abf": function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return r;
        }), n.d(t, "c", function() {
            return a;
        }), n.d(t, "a", function() {
            return o;
        });
        var o = {
            FloatBtn: function() {
                return n.e("components/FloatBtn/FloatBtn").then(n.bind(null, "e690"));
            },
            SharePopup: function() {
                return Promise.all([ n.e("common/vendor"), n.e("components/SharePopup/SharePopup") ]).then(n.bind(null, "b677"));
            }
        }, r = function() {
            this.$createElement, this._self._c;
        }, a = [];
    },
    b4aa: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("8abf"), r = n("3592");
        for (var a in r) [ "default" ].indexOf(a) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(a);
        n("7db2");
        var i = n("f0c5"), u = Object(i.a)(r.default, o.b, o.c, !1, null, "4b3a103f", null, !1, o.a, void 0);
        t.default = u.exports;
    },
    c53e: function(e, t, n) {},
    e9ea: function(e, t, n) {
        "use strict";
        (function(e) {
            var o = n("4ea4");
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var r = o(n("9523")), a = n("26cb");
            function i(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var o = Object.getOwnPropertySymbols(e);
                    t && (o = o.filter(function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable;
                    })), n.push.apply(n, o);
                }
                return n;
            }
            function u(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? i(Object(n), !0).forEach(function(t) {
                        (0, r.default)(e, t, n[t]);
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : i(Object(n)).forEach(function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
                    });
                }
                return e;
            }
            var c = {
                components: {
                    DefaultTheme: function() {
                        n.e("components/PageRender/themes/DefaultTheme").then(function() {
                            return resolve(n("ee86"));
                        }.bind(null, n)).catch(n.oe);
                    },
                    HomepageTheme: function() {
                        Promise.all([ n.e("common/vendor"), n.e("components/PageRender/themes/HomepageTheme") ]).then(function() {
                            return resolve(n("3b65"));
                        }.bind(null, n)).catch(n.oe);
                    }
                },
                props: {
                    page: {
                        type: Object,
                        default: function() {
                            return {
                                title: "",
                                modules: [],
                                isSharePopup: !1
                            };
                        }
                    },
                    homeNum: Number,
                    theme: {
                        type: String
                    },
                    fullPageMode: {
                        type: Boolean,
                        default: function() {
                            return !0;
                        }
                    },
                    isFixed: {
                        type: Boolean,
                        default: function() {
                            return !1;
                        }
                    },
                    refreshCounter: Number,
                    getNextPageCounter: Number
                },
                data: function() {
                    return {
                        isSharePopup: !1
                    };
                },
                computed: u(u({}, (0, a.mapGetters)([ "userInfo" ])), {}, {
                    renderTheme: function() {
                        return this.page && this.page.base && this.page.base.theme || this.theme || "default";
                    },
                    title: function() {
                        return this.page && this.page.title || "未设置";
                    },
                    navColor: function() {
                        return this.page && this.page.nav_color || "#FFFFFF";
                    },
                    isShowFloatBtn: function() {
                        return this.page && this.page.is_float_btn;
                    },
                    floatBtn: function() {
                        return this.page && this.page.float_btn || {};
                    },
                    posterInfo: function() {
                        return {
                            money_price: "",
                            score_price: "",
                            title: this.title,
                            path: this.getShareConfig().path,
                            thumb: "https://img121.7dun.com/yuanqimali/share/posterImg.png"
                        };
                    }
                }),
                mounted: function() {
                    this.setNav();
                },
                watch: {
                    title: function() {
                        this.setNav();
                    },
                    navColor: function() {
                        this.setNav();
                    }
                },
                methods: {
                    setNav: function() {
                        if (!this.fullPageMode) return !1;
                        e.setNavigationBarTitle({
                            title: this.title
                        }), e.setNavigationBarColor({
                            backgroundColor: this.navColor,
                            frontColor: "#ffffff"
                        });
                    },
                    invite: function() {
                        "invite" == this.floatBtn.link.url && (this.isSharePopup = !0);
                    },
                    hideSharePopup: function() {
                        this.isSharePopup = !1;
                    }
                }
            };
            t.default = c;
        }).call(this, n("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/PageRender-create-component", {
    "components/PageRender/PageRender-create-component": function(e, t, n) {
        n("543d").createComponent(n("b4aa"));
    }
}, [ [ "components/PageRender/PageRender-create-component" ] ] ]);