(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/themes/HomepageTheme" ], {
    "042a": function(e, n, t) {},
    "3b65": function(e, n, t) {
        "use strict";
        t.r(n);
        var i = t("de53"), o = t("db0d");
        for (var a in o) [ "default" ].indexOf(a) < 0 && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(a);
        t("a9f9");
        var u = t("f0c5"), s = Object(u.a)(o.default, i.b, i.c, !1, null, "02f310a9", null, !1, i.a, void 0);
        n.default = s.exports;
    },
    a9f9: function(e, n, t) {
        "use strict";
        var i = t("042a");
        t.n(i).a;
    },
    d23d: function(e, n, t) {
        "use strict";
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0, t("b5e5");
            var i = {
                components: {
                    ImageList: function() {
                        t.e("components/PageRender/modules/ImageList").then(function() {
                            return resolve(t("a3da"));
                        }.bind(null, t)).catch(t.oe);
                    },
                    PureImageList: function() {
                        t.e("components/PageRender/modules/PureImageList").then(function() {
                            return resolve(t("12fe"));
                        }.bind(null, t)).catch(t.oe);
                    },
                    VideoItem: function() {
                        t.e("components/PageRender/modules/Video").then(function() {
                            return resolve(t("d556"));
                        }.bind(null, t)).catch(t.oe);
                    }
                },
                props: {
                    homeNum: {
                        type: Number,
                        default: 0
                    },
                    refreshCount: {
                        type: Number
                    },
                    page: {
                        type: Object,
                        default: function() {
                            return {
                                modules: []
                            };
                        }
                    },
                    refreshCounter: Number,
                    getNextPageCounter: Number
                },
                data: function() {
                    return {
                        typeTop: 148,
                        androidBool: !1,
                        iosBool: !1,
                        selectTop: 0,
                        isFixed: !1,
                        topheight: 0,
                        numberType: 0,
                        homeList: [ {
                            selected: 0,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/yuanqimali/home/quanbu.png",
                            selectedIconPath: "https://img121.7dun.com/yuanqimali/home/quanbu-on.png",
                            activity_type: "fudai"
                        }, {
                            selected: 2,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/yuanqimali/home/yifanshang.png",
                            selectedIconPath: "https://img121.7dun.com/yuanqimali/home/yifanshang-on.png",
                            activity_type: "yifanshang"
                        }, {
                            selected: 3,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/yuanqimali/home/malishang.png",
                            selectedIconPath: "https://img121.7dun.com/yuanqimali/home/malishang-on.png",
                            activity_type: "yifanshang"
                        }, {
                            selected: 1,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/yuanqimali/home/wuxianshang.png",
                            selectedIconPath: "https://img121.7dun.com/yuanqimali/home/wuxianshang-on.png",
                            activity_type: "fudai"
                        }, {
                            selected: 4,
                            pagePath: "/pages/index/index",
                            iconPath: "https://img121.7dun.com/yuanqimali/home/lianjishang.png",
                            selectedIconPath: "https://img121.7dun.com/yuanqimali/home/lianjishang-on.png",
                            activity_type: "renyi"
                        } ]
                    };
                },
                computed: {
                    deviceInfo: function() {
                        return this.$store.getters.deviceInfo;
                    },
                    pageModules: function() {
                        return this.page.modules || [];
                    },
                    swiperList: function() {
                        return this.pageModules[0] && this.pageModules[0].list || [];
                    },
                    swiperModule: function() {
                        return this.pageModules[0] || {
                            style: {}
                        };
                    },
                    modules: function() {
                        return this.pageModules.slice(1);
                    },
                    title: function() {
                        return this.page.title || "未设置";
                    },
                    floatBtn: function() {
                        return this.page && this.page.float_btn || {};
                    }
                },
                mounted: function() {
                    var n = this;
                    switch (this.$nextTick(function() {
                        n.setSwiperHeight();
                    }), e.getSystemInfoSync().platform) {
                      case "android":
                        console.log("运行Android上"), this.androidBool = !0, this.typeTop = 148;
                        break;

                      case "ios":
                        console.log("运行iOS上"), this.iosBool = !0, this.typeTop = 174;
                        break;

                      default:
                        console.log("运行在开发者工具上");
                    }
                    var t = this;
                    e.$on("onPageScroll", function(e) {
                        t.isFixed = e;
                    }), this.topheight = this.deviceInfo.customBar;
                },
                watch: {
                    title: function(e) {}
                },
                methods: {
                    setSwiperHeight: function() {
                        var n = this;
                        setTimeout(function() {
                            e.createSelectorQuery().in(n).select("#title_id").boundingClientRect(function(e) {
                                null !== e.top && (1 == n.iosBool ? n.selectTop = e.top + 5 : n.selectTop = 148);
                            }).exec();
                        }, 600);
                    },
                    goShareActivity: function() {
                        "invite" == this.floatBtn.link.url || e.navigateTo({
                            url: this.floatBtn.link.path
                        });
                    },
                    hanldeActivityTypeClick: function(e) {
                        this.numberType = e;
                    }
                }
            };
            n.default = i;
        }).call(this, t("543d").default);
    },
    db0d: function(e, n, t) {
        "use strict";
        t.r(n);
        var i = t("d23d"), o = t.n(i);
        for (var a in i) [ "default" ].indexOf(a) < 0 && function(e) {
            t.d(n, e, function() {
                return i[e];
            });
        }(a);
        n.default = o.a;
    },
    de53: function(e, n, t) {
        "use strict";
        t.d(n, "b", function() {
            return o;
        }), t.d(n, "c", function() {
            return a;
        }), t.d(n, "a", function() {
            return i;
        });
        var i = {
            Banner: function() {
                return t.e("components/Banner/Banner").then(t.bind(null, "8003"));
            },
            cardTitle: function() {
                return t.e("components/cardTitle/cardTitle").then(t.bind(null, "9c6f"));
            },
            BoxList: function() {
                return t.e("components/BoxList/BoxList").then(t.bind(null, "a640a"));
            },
            ProductList: function() {
                return t.e("components/ProductList/ProductList").then(t.bind(null, "0cf8"));
            },
            ActivityList: function() {
                return Promise.all([ t.e("common/vendor"), t.e("components/ActivityList/ActivityList") ]).then(t.bind(null, "eec7"));
            },
            CouponList: function() {
                return t.e("components/CouponList/CouponList").then(t.bind(null, "040b"));
            },
            IPList: function() {
                return t.e("components/IPList/IPList").then(t.bind(null, "3ff6"));
            },
            CategoryList: function() {
                return t.e("components/CategoryList/CategoryList").then(t.bind(null, "77b7"));
            },
            SigninCard: function() {
                return t.e("components/SigninCard/SigninCard").then(t.bind(null, "4003"));
            },
            HTML: function() {
                return t.e("components/HTML/HTML").then(t.bind(null, "b320"));
            }
        }, o = function() {
            this.$createElement;
            var e = (this._self._c, this.swiperList.length);
            this.$mp.data = Object.assign({}, {
                $root: {
                    g0: e
                }
            });
        }, a = [];
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/themes/HomepageTheme-create-component", {
    "components/PageRender/themes/HomepageTheme-create-component": function(e, n, t) {
        t("543d").createComponent(t("3b65"));
    }
}, [ [ "components/PageRender/themes/HomepageTheme-create-component" ] ] ]);