(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/modules/ImageList" ], {
    "0fe3": function(e, t, n) {
        "use strict";
        var o = n("ed81");
        n.n(o).a;
    },
    "28e3": function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = {
            props: {
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            data: function() {
                return {};
            },
            computed: {
                list: function() {
                    return this.module.list || [];
                },
                style: function() {
                    return this.module.style || {};
                },
                perRow: function() {
                    return {
                        grid1: 1,
                        grid2: 2,
                        grid3: 3,
                        grid4: 4,
                        grid5: 5,
                        scroll: 1
                    }[this.module.grid];
                },
                imageWidth: function() {
                    return (750 - 2 * (this.style.margin || 0) - (this.module.spacing || 0) * (this.perRow - 1)) / this.perRow;
                }
            },
            mounted: function() {},
            watch: {},
            methods: {}
        };
        t.default = o;
    },
    a3da: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("b427f"), u = n("cff7");
        for (var r in u) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return u[e];
            });
        }(r);
        n("0fe3");
        var i = n("f0c5"), c = Object(i.a)(u.default, o.b, o.c, !1, null, "4fbdc8a4", null, !1, o.a, void 0);
        t.default = c.exports;
    },
    b427f: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return u;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, u = [];
    },
    cff7: function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("28e3"), u = n.n(o);
        for (var r in o) [ "default" ].indexOf(r) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(r);
        t.default = u.a;
    },
    ed81: function(e, t, n) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/modules/ImageList-create-component", {
    "components/PageRender/modules/ImageList-create-component": function(e, t, n) {
        n("543d").createComponent(n("a3da"));
    }
}, [ [ "components/PageRender/modules/ImageList-create-component" ] ] ]);