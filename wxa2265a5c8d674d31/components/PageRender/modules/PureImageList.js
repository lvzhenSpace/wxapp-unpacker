(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/PageRender/modules/PureImageList" ], {
    "12fe": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("d425"), r = n("3b22");
        for (var u in r) [ "default" ].indexOf(u) < 0 && function(e) {
            n.d(t, e, function() {
                return r[e];
            });
        }(u);
        n("27f4f");
        var i = n("f0c5"), c = Object(i.a)(r.default, o.b, o.c, !1, null, "5ed1ef5c", null, !1, o.a, void 0);
        t.default = c.exports;
    },
    "27f4f": function(e, t, n) {
        "use strict";
        var o = n("d97c");
        n.n(o).a;
    },
    "3b22": function(e, t, n) {
        "use strict";
        n.r(t);
        var o = n("e93b"), r = n.n(o);
        for (var u in o) [ "default" ].indexOf(u) < 0 && function(e) {
            n.d(t, e, function() {
                return o[e];
            });
        }(u);
        t.default = r.a;
    },
    d425: function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return o;
        }), n.d(t, "c", function() {
            return r;
        }), n.d(t, "a", function() {});
        var o = function() {
            this.$createElement, this._self._c;
        }, r = [];
    },
    d97c: function(e, t, n) {},
    e93b: function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = {
            props: {
                module: {
                    type: Object,
                    default: function() {
                        return {};
                    }
                }
            },
            data: function() {
                return {};
            },
            computed: {
                list: function() {
                    return this.module.images || [];
                },
                style: function() {
                    return this.module.style || {};
                },
                perRow: function() {
                    return {
                        grid1: 1,
                        grid2: 2,
                        grid3: 3,
                        grid4: 4,
                        grid5: 5,
                        scroll: 1
                    }[this.module.grid || "grid1"];
                },
                imageWidth: function() {
                    return (750 - 2 * (this.style.margin || 0) - (this.module.spacing || 0) * (this.perRow - 1)) / this.perRow;
                }
            },
            mounted: function() {},
            watch: {},
            methods: {}
        };
        t.default = o;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/PageRender/modules/PureImageList-create-component", {
    "components/PageRender/modules/PureImageList-create-component": function(e, t, n) {
        n("543d").createComponent(n("12fe"));
    }
}, [ [ "components/PageRender/modules/PureImageList-create-component" ] ] ]);